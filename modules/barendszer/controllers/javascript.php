<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    admin
 * @author     mberta
 * @copyright  (c) 2008
 */
abstract class Javascript_Controller extends Controller {

   	// Template view name
	public $main_template = "javascript_view";
	
	public $auth;
	public $auto_render = TRUE;
	
	public $js_type = TRUE;
	
	public $content = "";

	public function __construct()
	{
		parent::__construct();
		$this->session = Session::instance();
			
		$this->auth = Auth::instance();
		$this->_checkLogin();		
		

		$this->template = new View($this->main_template);
	
		
		if ($this->auto_render == TRUE)
		{
			Event::add('system.post_controller', array($this, '_render'));
		}
	}

	
	/**
	 * Render the loaded template.
	 */
	public function _render()
	{
		if ($this->auto_render == TRUE /*&& request::is_ajax()*/)
		{
			$this->template->content = $this->content;
			$this->template->js_type = $this->js_type;
			$this->template->render(TRUE);
		}
	}

	
	private function _checkLogin(){
		
		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
			url::redirect('login');
		}
	}
	

} // End Template_Controller