<?php
class Felhasznalok_Controller extends Admin_Controller{

	
	
	function index() {
	
//		$yaml = Spyc_Core::YAMLLoad('spyc.yaml');
//
//		print_r ($yaml);
		
//		$yaml = new Spyc();
//		$yy = $yaml->YAMLLoad('spyc.yaml');
//		
//		print_r ($yy);
		
        $this->setCurrentModule("barendszer");
		
        
        $grid = new Bgrid("users","username","asc",20,0); 
        $grid->init();
		$grid->setDataModel();
        
        
        $this->template->module_name = "Felhasználók kezelése";
	    $this->template->module_group_title = "Felhasználók";
	    $this->template->module_short_description = "itt böngészheti és módosíthatja a felhasználók adatait";        
		
	    $rendered = $grid->render();
	    
	    	    
        $this->template->module_content = appmodules::getMenu("barendszer","felhasznalok").$rendered;		
		
		
	}
	
	
	
}

?>