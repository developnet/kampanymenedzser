<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    admin
 * @author     mberta
 * @copyright  (c) 2008
 */
abstract class Ajax_Controller extends Controller {

   	// Template view name
	public $main_template = "ajax_view";
	
	public $auth;
	public $auto_render = TRUE;
	public $req_auth = TRUE;
	public $req_ajax = TRUE;
	public $output_json_file = TRUE;
	
	public $jsonarray = array();

	public function __construct(){
		parent::__construct();
		$this->session = Session::instance();

		if($this->req_auth){
			$this->auth = Auth::instance();
			$this->_checkLogin();			
		}
		
		

		$this->template = new View($this->main_template);
		$this->template->output_json_file = $this->output_json_file;
		
		if ($this->auto_render == TRUE)
		{
			Event::add('system.post_controller', array($this, '_render'));
		}
	}

	
	/**
	 * Render the loaded template.
	 */
	public function _render()
	{
		
		$this->template->output_json_file = $this->output_json_file;
		
		if ($this->auto_render == TRUE){
			
			if($this->req_ajax &&  request::is_ajax()){
				$this->template->json = json_encode($this->jsonarray);
				$this->template->render(TRUE);				
			}elseif(!$this->req_ajax){
				$this->template->json = json_encode($this->jsonarray);
				$this->template->render(TRUE);				
			}
			

			
		}
	}

	
	private function _checkLogin(){
		
		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
			url::redirect('login');
		}
	}
	

} // End Template_Controller