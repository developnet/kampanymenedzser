<?php
class Beallitasok_Controller extends Admin_Controller {

	function index() {
	
        $this->setCurrentModule("barendszer");
		

        $this->template->module_name = "Beállítások kezelése";
	    $this->template->module_group_title = "Beállítások";
	    $this->template->module_short_description = "rövid leírás";        
		
        
        $grid = new Bgrid("cikkek","cim","asc",2,0); 
        $grid->init();
		$grid->setDataModel(new Vdata_Model());
        
        
		
	    $rendered = $grid->render();
	    
	    	    
			
		
        $this->template->module_content = appmodules::getMenu("barendszer","beallitasok").$rendered;	

        
        
        
		
	}
	
	
	
}
?>