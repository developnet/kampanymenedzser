-- phpMyAdmin SQL Dump
-- version 3.0.1.1
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2009. jan. 04. 18:26
-- Szerver verzió: 5.0.45
-- PHP verzió: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Adatbázis: `core`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_blogcategories`
--

CREATE TABLE IF NOT EXISTS `b_blogcategories` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `b_blogcategories`
--


-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_blogcomments`
--

CREATE TABLE IF NOT EXISTS `b_blogcomments` (
  `id` int(11) NOT NULL auto_increment,
  `datetime` datetime NOT NULL,
  `name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `text` text collate utf8_unicode_ci NOT NULL,
  `blogpost_id` int(11) NOT NULL,
  `status` varchar(1) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

--
-- A tábla adatainak kiíratása `b_blogcomments`
--


-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_blogposts`
--

CREATE TABLE IF NOT EXISTS `b_blogposts` (
  `id` mediumint(9) NOT NULL auto_increment,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `title` varchar(255) collate utf8_unicode_ci NOT NULL,
  `text` text collate utf8_unicode_ci NOT NULL,
  `status` varchar(1) collate utf8_unicode_ci NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `b_blogposts`
--

INSERT INTO `b_blogposts` (`id`, `date`, `time`, `title`, `text`, `status`) VALUES
(1, '0000-00-00', '00:00:00', 'Title', 'text', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_codedicts`
--

CREATE TABLE IF NOT EXISTS `b_codedicts` (
  `id` bigint(20) NOT NULL auto_increment,
  `codetype` int(11) default NULL,
  `code` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `codedicts_textfk_1` (`codetype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- A tábla adatainak kiíratása `b_codedicts`
--

INSERT INTO `b_codedicts` (`id`, `codetype`, `code`, `value`, `description`) VALUES
(1, NULL, '1', '', 'User státusz '),
(2, 1, '1', 'aktív', ''),
(3, 1, '0', 'inaktív', ''),
(4, NULL, '2', '', 'Entitás státusz '),
(5, 2, 'draft', 'Szerkesztés alatt', ''),
(6, 2, 'published', 'Publikált', ''),
(7, 2, 'deleted', 'Törölt', ''),
(8, NULL, '3', '', 'Oldal típus '),
(9, 3, 'normal', 'Normál', ''),
(10, 3, 'hidden', 'Rejtett', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_entities`
--

CREATE TABLE IF NOT EXISTS `b_entities` (
  `id` int(11) NOT NULL auto_increment,
  `entity` varchar(20) NOT NULL COMMENT 'Entitás neve',
  `entity_type` enum('vertical','horizontal') NOT NULL COMMENT 'vertical / horizontal',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Entitások tulajdonnságait tartalmazza' AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `b_entities`
--

INSERT INTO `b_entities` (`id`, `entity`, `entity_type`, `name`, `description`) VALUES
(1, 'users', 'horizontal', 'Felhasználók', ''),
(2, 'cikkek', 'vertical', 'Cikkek', ''),
(3, 'pages', 'horizontal', 'Oldalak', 'Oldalak');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_entity_datas`
--

CREATE TABLE IF NOT EXISTS `b_entity_datas` (
  `id` int(11) NOT NULL auto_increment,
  `entity_instance_id` int(11) NOT NULL,
  `field` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  `value_text` mediumtext NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='Entitás adatok' AUTO_INCREMENT=9 ;

--
-- A tábla adatainak kiíratása `b_entity_datas`
--

INSERT INTO `b_entity_datas` (`id`, `entity_instance_id`, `field`, `value`, `value_text`) VALUES
(1, 1, 'cim', '', 'Ez a cucc címe sdf sdf'),
(2, 1, 'szoveg', '', 'ez a szö<span style="font-weight: bold;">veg sdf sdf sdf sdf sdf</span><br>'),
(3, 3, 'cim', '', 'a valami2'),
(4, 3, 'szoveg', '', 'valami fgdfg2<br>'),
(5, 4, 'cim', '', 'Abbbb'),
(6, 4, 'szoveg', '', 'sdfasdsa<br>'),
(7, 5, 'cim', '', 'dddd'),
(8, 5, 'szoveg', '', 'asdasd sad as asd s<br>');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_entity_instances`
--

CREATE TABLE IF NOT EXISTS `b_entity_instances` (
  `id` int(11) NOT NULL auto_increment,
  `entity_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Entitás példányokat tartalmazó tábla (vertical entity type)' AUTO_INCREMENT=6 ;

--
-- A tábla adatainak kiíratása `b_entity_instances`
--

INSERT INTO `b_entity_instances` (`id`, `entity_id`) VALUES
(1, 2),
(3, 2),
(4, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_metadatas`
--

CREATE TABLE IF NOT EXISTS `b_metadatas` (
  `id` bigint(20) NOT NULL auto_increment,
  `entity` varchar(50) NOT NULL,
  `field` varchar(50) NOT NULL,
  `text_id` bigint(20) NOT NULL,
  `length` int(11) NOT NULL,
  `type` enum('string','number','date','codedict','text','html','popup','email') NOT NULL,
  `filter` tinyint(4) NOT NULL,
  `grid` tinyint(4) NOT NULL,
  `form` tinyint(4) NOT NULL,
  `mod` tinyint(4) NOT NULL,
  `required` int(11) NOT NULL,
  `codedict_codetype` varchar(50) NOT NULL,
  `foreign_entity` varchar(50) NOT NULL,
  `foreign_field` varchar(50) NOT NULL,
  `ord` int(11) NOT NULL,
  `description` bigint(20) NOT NULL,
  `long_description` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `entity` (`entity`,`field`),
  KEY `metadata_textfk_1` (`text_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=20 ;

--
-- A tábla adatainak kiíratása `b_metadatas`
--

INSERT INTO `b_metadatas` (`id`, `entity`, `field`, `text_id`, `length`, `type`, `filter`, `grid`, `form`, `mod`, `required`, `codedict_codetype`, `foreign_entity`, `foreign_field`, `ord`, `description`, `long_description`) VALUES
(1, 'users', 'id', 1, 20, 'number', 0, 1, 0, 0, 0, '', '', '', 1, 1, 12),
(2, 'users', 'username', 2, 0, 'string', 1, 1, 1, 1, 1, '', '', '', 3, 7, 12),
(3, 'users', 'email', 3, 0, 'email', 1, 1, 1, 1, 1, '', '', '', 2, 8, 12),
(6, 'users', 'status', 4, 20, 'codedict', 1, 1, 1, 1, 1, '1', '', '', 5, 9, 12),
(8, 'users', 'datum', 5, 0, 'date', 1, 0, 1, 1, 1, '', '', '', 6, 10, 12),
(11, 'users', 'note', 6, 0, 'html', 0, 0, 1, 1, 1, '', '', '', 7, 11, 12),
(12, 'cikkek', 'cim', 13, 0, 'string', 1, 1, 1, 1, 1, '', '', '', 2, 1, 1),
(13, 'cikkek', 'szoveg', 14, 0, 'html', 0, 0, 1, 1, 1, '', '', '', 3, 1, 1),
(14, 'cikkek', 'id', 1, 20, 'number', 0, 1, 0, 0, 0, '', '', '', 1, 1, 1),
(15, 'pages', 'type', 16, 0, 'codedict', 0, 1, 1, 1, 1, '3', '', '', 2, 16, 16),
(16, 'pages', 'template', 17, 255, 'string', 1, 1, 1, 1, 1, '', '', '', 3, 17, 17),
(17, 'pages', 'name', 18, 255, 'string', 1, 1, 1, 1, 1, '', '', '', 1, 18, 18),
(18, 'pages', 'status', 4, 255, 'codedict', 1, 1, 1, 1, 1, '2', '', '', 4, 4, 4),
(19, 'pages', 'content', 19, 0, 'html', 0, 0, 1, 1, 1, '', '', '', 5, 19, 19);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_pages`
--

CREATE TABLE IF NOT EXISTS `b_pages` (
  `id` bigint(20) NOT NULL auto_increment,
  `parent_id` bigint(20) NOT NULL default '0',
  `type` varchar(255) NOT NULL default 'normal',
  `name` varchar(255) NOT NULL,
  `template` varchar(255) default NULL,
  `content` text NOT NULL,
  `status` varchar(255) NOT NULL default 'draft',
  `ord` int(11) NOT NULL default '0',
  `mod_date` timestamp NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `mod_user` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- A tábla adatainak kiíratása `b_pages`
--

INSERT INTO `b_pages` (`id`, `parent_id`, `type`, `name`, `template`, `content`, `status`, `ord`, `mod_date`, `mod_user`) VALUES
(1, 0, 'normal', 'Kezdőoldal', 'sablon01', '', 'draft', 1, '2009-01-04 11:09:13', NULL),
(2, 1, 'normal', 'Kapcsolat', 'sablon01', '', 'draft', 1, '2009-01-04 11:57:00', NULL),
(3, 0, 'normal', 'Aloldal', 'sablon02', '', 'draft', 2, '2009-01-04 10:17:13', NULL),
(4, 0, 'normal', 'Blog', NULL, '', 'draft', 3, '2009-01-04 14:49:13', NULL),
(11, 3, 'normal', 'Új oldal', NULL, '', 'draft', 2, '2009-01-04 11:57:00', NULL),
(5, 0, 'hidden', 'Új oldalasasdd 666', NULL, '', 'draft', 5, '2009-01-04 14:49:14', NULL),
(6, 4, 'normal', 'Új oldal', NULL, '', 'draft', 2, '2009-01-04 14:49:12', NULL),
(7, 4, 'normal', 'Aloldal 25', NULL, '', 'draft', 4, '2009-01-04 14:52:58', NULL),
(8, 4, 'normal', 'skjd as kd', NULL, '', 'draft', 1, '2009-01-04 14:49:12', NULL),
(9, 0, 'hidden', 'Új oldal', NULL, '', 'draft', 4, '2009-01-04 14:49:14', NULL),
(10, 3, 'normal', 'Új oldal', NULL, '', 'draft', 1, '2009-01-04 11:57:00', NULL),
(12, 11, 'normal', 'Új oldal', NULL, '', 'draft', 1, '2009-01-04 11:57:00', NULL),
(13, 4, 'normal', 'Új oldal', NULL, '', 'draft', 3, '2009-01-04 14:52:56', NULL),
(14, 3, 'normal', 'Új oldal', NULL, '', 'draft', 4, '2009-01-04 14:52:43', NULL),
(15, 3, 'normal', 'Új oldal', NULL, '', 'draft', 3, '2009-01-04 14:52:43', NULL),
(16, 3, 'normal', 'Új oldald', NULL, '', 'deleted', 5, '2009-01-04 14:52:49', NULL),
(17, 11, 'normal', 'Új oldal', NULL, '', 'draft', 2, '2009-01-04 14:52:49', NULL),
(19, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 6, '2009-01-04 14:50:00', NULL),
(20, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 7, '2009-01-04 14:50:02', NULL),
(21, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 8, '2009-01-04 14:50:04', NULL),
(22, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 9, '2009-01-04 14:50:06', NULL),
(23, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 10, '2009-01-04 14:50:08', NULL),
(24, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 11, '2009-01-04 14:50:11', NULL),
(25, 0, 'normal', 'Új oldal', NULL, '', 'deleted', 12, '2009-01-04 14:50:13', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_roles`
--

CREATE TABLE IF NOT EXISTS `b_roles` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- A tábla adatainak kiíratása `b_roles`
--

INSERT INTO `b_roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_roles_users`
--

CREATE TABLE IF NOT EXISTS `b_roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `b_roles_users`
--

INSERT INTO `b_roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_texts_hu_HU`
--

CREATE TABLE IF NOT EXISTS `b_texts_hu_HU` (
  `id` bigint(20) NOT NULL auto_increment,
  `text` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=20 ;

--
-- A tábla adatainak kiíratása `b_texts_hu_HU`
--

INSERT INTO `b_texts_hu_HU` (`id`, `text`) VALUES
(1, '#'),
(2, 'Felhasználónév'),
(3, 'E-mail'),
(4, 'Státusz'),
(5, 'Valami dátum'),
(6, 'Megjegyzés'),
(7, 'A felhasználónév leírása.'),
(8, 'Az e-mail az érvényes legyen ám.'),
(9, 'A státusz mondja meg, hogy a felhasználó beléphet-e.'),
(10, 'Dátum típusú mező.'),
(11, 'Ez egy text ipusu mező.'),
(12, '<h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p><h2>Hósszú leírás</h2><p>Valami leírás he?</p><p>Valami leírás he?</p><p>Valami leírás he?</p>'),
(13, 'Cím'),
(14, 'Szöveg'),
(16, 'Típus'),
(17, 'Sablon'),
(18, 'Megnevezés'),
(19, 'Tartalom');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_users`
--

CREATE TABLE IF NOT EXISTS `b_users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL default '',
  `password` char(50) NOT NULL,
  `logins` int(10) unsigned NOT NULL default '0',
  `last_login` int(10) unsigned default NULL,
  `status` varchar(1) NOT NULL,
  `datum` date NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `b_users`
--

INSERT INTO `b_users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `status`, `datum`, `note`) VALUES
(1, 'bertamarton@gmail.com', 'admin', '8f3e4ab7554e27a48cf6c83f45e6689898deb0e0fe78a93352', 21, 1231079006, '1', '2008-12-13', 'a<br>');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `b_user_tokens`
--

CREATE TABLE IF NOT EXISTS `b_user_tokens` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- A tábla adatainak kiíratása `b_user_tokens`
--


-- --------------------------------------------------------

--
-- Tábla szerkezet: `wwwsqldesigner`
--

CREATE TABLE IF NOT EXISTS `wwwsqldesigner` (
  `keyword` varchar(30) NOT NULL default '',
  `data` text,
  `dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- A tábla adatainak kiíratása `wwwsqldesigner`
--

INSERT INTO `wwwsqldesigner` (`keyword`, `data`, `dt`) VALUES
('core', '<?xml version="1.0" encoding="utf-8" ?><sql><datatypes db="mysql">\n\n	<group label="Numeric" color="rgb(238,238,170)">\n\n		<type label="Integer" length="0" sql="INTEGER" re="INT" quote=""/>\n\n		<type label="Decimal" length="1" sql="DECIMAL" re="DEC" quote=""/>\n\n		<type label="Single precision" length="0" sql="FLOAT" quote=""/>\n\n		<type label="Double precision" length="0" sql="DOUBLE" re="DOUBLE" quote=""/>\n\n	</group>\n\n\n\n	<group label="Character" color="rgb(255,200,200)">\n\n		<type label="Char" length="1" sql="CHAR" quote="''"/>\n\n		<type label="Varchar" length="1" sql="VARCHAR" quote="''"/>\n\n		<type label="Text" length="0" sql="MEDIUMTEXT" re="TEXT" quote="''"/>\n\n		<type label="Binary" length="1" sql="BINARY" quote="''"/>\n\n		<type label="Varbinary" length="1" sql="VARBINARY" quote="''"/>\n\n		<type label="BLOB" length="0" sql="BLOB" re="BLOB" quote="''"/>\n\n	</group>\n\n\n\n	<group label="Date &amp; Time" color="rgb(200,255,200)">\n\n		<type label="Date" length="0" sql="DATE" quote="''"/>\n\n		<type label="Time" length="0" sql="TIME" quote="''"/>\n\n		<type label="Datetime" length="0" sql="DATETIME" quote="''"/>\n\n		<type label="Year" length="0" sql="YEAR" quote=""/>\n\n		<type label="Timestamp" length="0" sql="TIMESTAMP" quote="''"/>\n\n	</group>\n\n	\n\n	<group label="Miscellaneous" color="rgb(200,200,255)">\n\n		<type label="ENUM" length="1" sql="ENUM" quote=""/>\n\n		<type label="SET" length="1" sql="SET" quote=""/>\n\n	</group>\n\n</datatypes><table x="178" y="271" name="b_roles_users"><row name="user_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_users" row="id" /></row><row name="role_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_roles" row="id" /></row><key type="INDEX" name="fk_role_id"><part>role_id</part></key><key type="PRIMARY" name="PRIMARY"><part>user_id</part><part>role_id</part></key><comment>InnoDB free: 4096 kB; (`user_id`) REFER `core/b_users`(`id`) ON DELETE CASCADE; </comment></table><table x="14" y="14" name="b_users"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="email" null="1" autoincrement="0"><datatype>VARCHAR(127)</datatype></row><row name="username" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="password" null="1" autoincrement="0"><datatype>CHAR(50)</datatype></row><row name="logins" null="1" autoincrement="0"><datatype>INTEGER</datatype><default>0</default></row><row name="last_login" null="0" autoincrement="0"><datatype>INTEGER</datatype></row><row name="status" null="1" autoincrement="0"><datatype>VARCHAR(1)</datatype></row><row name="datum" null="1" autoincrement="0"><datatype>DATE</datatype></row><row name="note" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="UNIQUE" name="uniq_email"><part>email</part></key><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_username"><part>username</part></key><comment>InnoDB free: 4096 kB</comment></table><table x="16" y="272" name="b_roles"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="name" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="description" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_name"><part>name</part></key><comment>InnoDB free: 4096 kB</comment></table><table x="181" y="15" name="b_user_tokens"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="user_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_users" row="id" /></row><row name="user_agent" null="1" autoincrement="0"><datatype>VARCHAR(40)</datatype></row><row name="token" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="created" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="expires" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><key type="INDEX" name="fk_user_id"><part>user_id</part></key><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_token"><part>token</part></key><comment>InnoDB free: 4096 kB; (`user_id`) REFER `core/b_users`(`id`) ON DELETE CASCADE</comment></table><table x="456" y="302" name="b_codedicts"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="codetype" null="0" autoincrement="0"><datatype>INTEGER</datatype></row><row name="code" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="value" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><row name="description" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="INDEX" name="codedicts_textfk_1"><part>codetype</part></key><comment>szÃ³tÃ¡r tÃ¡bla, </comment></table><table x="707" y="162" name="b_metadatas"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="entity_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entity" row="id" /></row><row name="field" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="text_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_texts_hu_HU" row="id" /></row><row name="length" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="type" null="1" autoincrement="0"><datatype>ENUM(''STRING'',''NUMBER'',''DATE'',''CODEDICT'',''TEXT'',''HTML'',''POPUP'',''EMAIL'')</datatype></row><row name="filter" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="grid" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="form" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="mod" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="required" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="codedict_codetype" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="foreign_entity" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="foreign_field" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="ord" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="description" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="INDEX" name="metadata_textfk_1"></key><key type="INDEX" name="entity"><part>entity</part><part>field</part></key><comment>EntitÃ¡sok meta adatai</comment></table><table x="449" y="159" name="b_texts_hu_HU"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="text" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><comment>szÃ¼vegek, labelek hu_HU locÃ¡lhoz</comment></table><table x="21" y="660" name="wwwsqldesigner"><row name="keyword" null="1" autoincrement="0"><datatype>VARCHAR(30)</datatype></row><row name="data" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><row name="dt" null="1" autoincrement="0"><datatype>TIMESTAMP</datatype><default>CURRENT_TIMESTAMP</default></row><key type="PRIMARY" name="PRIMARY"><part>keyword</part></key></table><table x="927" y="159" name="b_entity"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity" null="0" autoincrement="0"><datatype>VARCHAR(30)</datatype><comment>EntitÃ¡s neve</comment></row><row name="entity_type" null="0" autoincrement="0"><datatype>VARCHAR(20)</datatype><comment>vertical / horizontal</comment></row><row name="description" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡sok tulajdonnsÃ¡gait tartalmazza</comment></table><table x="1098" y="62" name="b_entity_instances"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entity" row="id" /></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡s pÃ©ldÃ¡nyokat tartalmazÃ³ tÃ¡bla (vertical entity type)</comment></table><table x="1328" y="130" name="b_entity_data"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="instance_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entity_instances" row="id" /></row><row name="value" null="0" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><row name="value_text" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡s adatok</comment></table></sql>', '2008-12-03 08:15:52'),
('core v1.2', '<?xml version="1.0" encoding="utf-8" ?><sql><datatypes db="mysql">\n\n	<group label="Numeric" color="rgb(238,238,170)">\n\n		<type label="Integer" length="0" sql="INTEGER" re="INT" quote=""/>\n\n		<type label="Decimal" length="1" sql="DECIMAL" re="DEC" quote=""/>\n\n		<type label="Single precision" length="0" sql="FLOAT" quote=""/>\n\n		<type label="Double precision" length="0" sql="DOUBLE" re="DOUBLE" quote=""/>\n\n	</group>\n\n\n\n	<group label="Character" color="rgb(255,200,200)">\n\n		<type label="Char" length="1" sql="CHAR" quote="''"/>\n\n		<type label="Varchar" length="1" sql="VARCHAR" quote="''"/>\n\n		<type label="Text" length="0" sql="MEDIUMTEXT" re="TEXT" quote="''"/>\n\n		<type label="Binary" length="1" sql="BINARY" quote="''"/>\n\n		<type label="Varbinary" length="1" sql="VARBINARY" quote="''"/>\n\n		<type label="BLOB" length="0" sql="BLOB" re="BLOB" quote="''"/>\n\n	</group>\n\n\n\n	<group label="Date &amp; Time" color="rgb(200,255,200)">\n\n		<type label="Date" length="0" sql="DATE" quote="''"/>\n\n		<type label="Time" length="0" sql="TIME" quote="''"/>\n\n		<type label="Datetime" length="0" sql="DATETIME" quote="''"/>\n\n		<type label="Year" length="0" sql="YEAR" quote=""/>\n\n		<type label="Timestamp" length="0" sql="TIMESTAMP" quote="''"/>\n\n	</group>\n\n	\n\n	<group label="Miscellaneous" color="rgb(200,200,255)">\n\n		<type label="ENUM" length="1" sql="ENUM" quote=""/>\n\n		<type label="SET" length="1" sql="SET" quote=""/>\n\n	</group>\n\n</datatypes><table x="191" y="203" name="b_roles_users"><row name="user_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_users" row="id" /></row><row name="role_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_roles" row="id" /></row><key type="INDEX" name="fk_role_id"><part>role_id</part></key><key type="PRIMARY" name="PRIMARY"><part>user_id</part><part>role_id</part></key><comment>InnoDB free: 4096 kB; (`user_id`) REFER `core/b_users`(`id`) ON DELETE CASCADE; </comment></table><table x="13" y="31" name="b_users"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="email" null="1" autoincrement="0"><datatype>VARCHAR(127)</datatype></row><row name="username" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="password" null="1" autoincrement="0"><datatype>CHAR(50)</datatype></row><row name="logins" null="1" autoincrement="0"><datatype>INTEGER</datatype><default>0</default></row><row name="last_login" null="0" autoincrement="0"><datatype>INTEGER</datatype></row><row name="status" null="1" autoincrement="0"><datatype>VARCHAR(1)</datatype></row><row name="datum" null="1" autoincrement="0"><datatype>DATE</datatype></row><row name="note" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="UNIQUE" name="uniq_email"><part>email</part></key><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_username"><part>username</part></key><comment>InnoDB free: 4096 kB</comment></table><table x="173" y="305" name="b_roles"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="name" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="description" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_name"><part>name</part></key><comment>InnoDB free: 4096 kB</comment></table><table x="192" y="7" name="b_user_tokens"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="user_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_users" row="id" /></row><row name="user_agent" null="1" autoincrement="0"><datatype>VARCHAR(40)</datatype></row><row name="token" null="1" autoincrement="0"><datatype>VARCHAR(32)</datatype></row><row name="created" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="expires" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><key type="INDEX" name="fk_user_id"><part>user_id</part></key><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="UNIQUE" name="uniq_token"><part>token</part></key><comment>InnoDB free: 4096 kB; (`user_id`) REFER `core/b_users`(`id`) ON DELETE CASCADE</comment></table><table x="377" y="475" name="b_codedicts"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="codetype" null="0" autoincrement="0"><datatype>INTEGER</datatype></row><row name="code" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="value" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><row name="description" null="1" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="INDEX" name="codedicts_textfk_1"><part>codetype</part></key><comment>szÃ³tÃ¡r tÃ¡bla, </comment></table><table x="584" y="53" name="b_fields"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entities" row="id" /></row><row name="field" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="text_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_texts_hu_HU" row="id" /></row><row name="length" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="type" null="1" autoincrement="0"><datatype>ENUM(''STRING'',''NUMBER'',''DATE'',''CODEDICT'',''TEXT'',''HTML'',''POPUP'',''EMAIL'')</datatype></row><row name="filter" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="grid" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="form" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="mod" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="required" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="codedict_codetype" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="foreign_entity" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="foreign_field" null="1" autoincrement="0"><datatype>VARCHAR(50)</datatype></row><row name="ord" null="1" autoincrement="0"><datatype>INTEGER</datatype></row><row name="description" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><key type="INDEX" name="metadata_textfk_1"></key><key type="INDEX" name="entity"><part>field</part></key><comment>EntitÃ¡sok mezÅ‘inek meta adatai</comment></table><table x="354" y="634" name="b_texts_hu_HU"><row name="id" null="1" autoincrement="1"><datatype>INTEGER</datatype></row><row name="text" null="1" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name="PRIMARY"><part>id</part></key><comment>szÃ¼vegek, labelek hu_HU locÃ¡lhoz</comment></table><table x="21" y="660" name="wwwsqldesigner"><row name="keyword" null="1" autoincrement="0"><datatype>VARCHAR(30)</datatype></row><row name="data" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><row name="dt" null="1" autoincrement="0"><datatype>TIMESTAMP</datatype><default>CURRENT_TIMESTAMP</default></row><key type="PRIMARY" name="PRIMARY"><part>keyword</part></key></table><table x="812" y="351" name="b_entities"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity" null="0" autoincrement="0"><datatype>VARCHAR(30)</datatype><comment>EntitÃ¡s neve</comment></row><row name="entity_type" null="0" autoincrement="0"><datatype>VARCHAR(20)</datatype><comment>vertical / horizontal</comment></row><row name="description" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><row name="text_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_texts_hu_HU" row="id" /></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡sok tulajdonnsÃ¡gait tartalmazza</comment></table><table x="1160" y="327" name="b_entity_instances"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="entity_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entities" row="id" /></row><row name="status" null="0" autoincrement="0"><datatype>VARCHAR(1)</datatype></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡s pÃ©ldÃ¡nyokat tartalmazÃ³ tÃ¡bla (vertical entity type)</comment></table><table x="1049" y="29" name="b_entity_data"><row name="id" null="0" autoincrement="1"><datatype>INTEGER</datatype></row><row name="field_id" null="1" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_fields" row="id" /></row><row name="entity_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entities" row="id" /></row><row name="instance_id" null="0" autoincrement="0"><datatype>INTEGER</datatype><relation table="b_entity_instances" row="id" /></row><row name="value" null="0" autoincrement="0"><datatype>VARCHAR(255)</datatype></row><row name="value_text" null="0" autoincrement="0"><datatype>MEDIUMTEXT</datatype></row><key type="PRIMARY" name=""><part>id</part></key><comment>EntitÃ¡s adatok</comment></table></sql>', '2008-12-03 08:33:28');

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `b_roles_users`
--
ALTER TABLE `b_roles_users`
  ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `b_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `b_roles` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `b_user_tokens`
--
ALTER TABLE `b_user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `b_users` (`id`) ON DELETE CASCADE;
