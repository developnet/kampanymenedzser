CREATE TABLE `b_roles_users` (
`user_id` INTEGER ,
`role_id` INTEGER ,
KEY (`role_id`),
PRIMARY KEY (`user_id`, `role_id`)
) COMMENT 'Admin userek és jogosultásgok összerendelése';

CREATE TABLE `b_users` (
`id` INTEGER AUTO_INCREMENT ,
`email` VARCHAR(127) ,
`username` VARCHAR(32) ,
`password` CHAR(50) ,
`logins` INTEGER DEFAULT 0 ,
`last_login` INTEGER NOT NULL ,
`status` VARCHAR(1) ,
`datum` DATE ,
`note` MEDIUMTEXT ,
UNIQUE KEY (`email`),
PRIMARY KEY (`id`),
UNIQUE KEY (`username`)
) COMMENT 'Admin usereket tartalmazó tábla';

CREATE TABLE `b_roles` (
`id` INTEGER AUTO_INCREMENT ,
`name` VARCHAR(32) ,
`description` VARCHAR(255) ,
PRIMARY KEY (`id`),
UNIQUE KEY (`name`)
) COMMENT 'Admin jogosultásgok';

CREATE TABLE `b_user_tokens` (
`id` INTEGER AUTO_INCREMENT ,
`user_id` INTEGER ,
`user_agent` VARCHAR(40) ,
`token` VARCHAR(32) ,
`created` INTEGER ,
`expires` INTEGER ,
KEY (`user_id`),
PRIMARY KEY (`id`),
UNIQUE KEY (`token`)
) COMMENT 'InnoDB free: 4096 kB; (`user_id`) REFER `core/b_users`(`id`) ON DELETE CASCADE';

CREATE TABLE `b_codedicts` (
`id` INTEGER AUTO_INCREMENT ,
`codetype` INTEGER NOT NULL ,
`code` INTEGER ,
`value` VARCHAR(255) ,
`description` VARCHAR(255) ,
PRIMARY KEY (`id`),
KEY (`codetype`)
) COMMENT 'szótár tábla, ';

INSERT INTO `b_codedicts` (`id`, `codetype`, `code`, `value`, `description`) VALUES
(1, NULL, 1, '', 'Státusz '),
(2, 1, 1, 'aktív', ''),
(3, 1, 0, 'inaktív', '');


CREATE TABLE `b_metadatas` (
`id` INTEGER AUTO_INCREMENT ,
`entity` VARCHAR(50) ,
`entity_id` INTEGER NOT NULL ,
`field` VARCHAR(50) ,
`text_id` INTEGER ,
`length` INTEGER ,
`type` ENUM('STRING','NUMBER','DATE','CODEDICT','TEXT','HTML','POPUP','EMAIL') ,
`filter` INTEGER ,
`grid` INTEGER ,
`form` INTEGER ,
`mod` INTEGER ,
`required` INTEGER ,
`codedict_codetype` VARCHAR(50) ,
`foreign_entity` VARCHAR(50) ,
`foreign_field` VARCHAR(50) ,
`ord` INTEGER ,
`description` MEDIUMTEXT ,
PRIMARY KEY (`id`),
KEY (),
KEY (`entity`, `field`)
) COMMENT 'Entitások meta adatai';

CREATE TABLE `b_texts_hu_HU` (
`id` INTEGER AUTO_INCREMENT ,
`text` MEDIUMTEXT ,
PRIMARY KEY (`id`)
) COMMENT 'szövegek, labelek hu_HU locálhoz';

CREATE TABLE `wwwsqldesigner` (
`keyword` VARCHAR(30) ,
`data` MEDIUMTEXT NOT NULL ,
`dt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`keyword`)
);

CREATE TABLE `b_entity` (
`id` INTEGER NOT NULL AUTO_INCREMENT ,
`entity` VARCHAR(30) NOT NULL COMMENT 'Entitás neve' ,
`entity_type` VARCHAR(20) NOT NULL COMMENT 'vertical / horizontal' ,
`description` MEDIUMTEXT NOT NULL ,
PRIMARY KEY (`id`)
) COMMENT 'Entitások tulajdonnságait tartalmazza';

CREATE TABLE `b_entity_instances` (
`id` INTEGER NOT NULL AUTO_INCREMENT ,
`entity_id` INTEGER NOT NULL ,
PRIMARY KEY (`id`)
) COMMENT 'Entitás példányokat tartalmazó tábla (vertical entity type)';

CREATE TABLE `b_entity_data` (
`id` INTEGER NOT NULL AUTO_INCREMENT ,
`instance_id` INTEGER NOT NULL ,
`value` VARCHAR(255) NOT NULL ,
`value_text` MEDIUMTEXT NOT NULL ,
PRIMARY KEY (`id`)
) COMMENT 'Entitás adatok';

ALTER TABLE `b_roles_users` ADD FOREIGN KEY (user_id) REFERENCES `b_users` (`id`);
ALTER TABLE `b_roles_users` ADD FOREIGN KEY (role_id) REFERENCES `b_roles` (`id`);
ALTER TABLE `b_user_tokens` ADD FOREIGN KEY (user_id) REFERENCES `b_users` (`id`);
ALTER TABLE `b_metadatas` ADD FOREIGN KEY (entity_id) REFERENCES `b_entity` (`id`);
ALTER TABLE `b_metadatas` ADD FOREIGN KEY (text_id) REFERENCES `b_texts_hu_HU` (`id`);
ALTER TABLE `b_entity_instances` ADD FOREIGN KEY (entity_id) REFERENCES `b_entity` (`id`);
ALTER TABLE `b_entity_data` ADD FOREIGN KEY (instance_id) REFERENCES `b_entity_instances` (`id`);
