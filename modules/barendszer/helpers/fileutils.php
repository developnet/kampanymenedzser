<?php defined('SYSPATH') or die('No direct script access.');

class fileutils_Core {
	
	public static function getCsvRowNumber($filename){
		
		$rows = file($filename);
		$size = sizeof($rows);
		
		unset($rows);
 
		return $size;
	}
	
	/*
	 * a sorok 0-tól számozódnak
	 * offset = ha nagyobb nulla akkor az offset < sorszámig eldobja sorokat
	 * limit = max ennyi sort nyomat be, ha 0 akkor nincs limit
	 */
	public static function getCsvContent($filename, $offset = 0, $limit = 0, $separator = ";"){
		ini_set("auto_detect_line_endings", true);
		$array = file($filename);
		$csvData = array();
		$i = 0;
		$j = 0;
		
		foreach ($array as $key => $a){
			
			if($offset > 0 && $offset > $i){
				$i++;
				continue;
			}
			
			if($limit != 0 && ($limit) == $j ){
				break;
			}
			
			$a = string::replaceChars($a);
			$row = explode($separator,$a);

			foreach ($row as $key => $r){
				$row[$key] = trim(trim($r),'"');
			}
			
			$csvData[] = $row;
			$j++;
			$i++;
			unset($row);
		}
		unset($array);
		
		return $csvData;
	}

	/**
	 * Makes directory, returns TRUE if exists or made
	 *
	 * @param string $pathname The directory path.
	 * @return boolean returns TRUE if exists or made or FALSE on failure.
	 */

	function makedir($pathname, $mode = 0755){
		is_dir(dirname($pathname)) || $this->makedir(dirname($pathname), $mode);
		return is_dir($pathname) || @mkdir($pathname, $mode);
	}

	function readDirectory($dir) {
		$listDir = array();
		if($handler = opendir($dir)) {
			while (($sub = readdir($handler)) !== FALSE) {
				if ($sub != "." && $sub != ".." && $sub != "Thumb.db") {
					if(is_file($dir."/".$sub)) {
						$listDir[] = $sub;
					}elseif(is_dir($dir."/".$sub)){
						$listDir[$sub] = $this->ReadFolderDirectory($dir."/".$sub);
					}
				}
			}
			closedir($handler);
		}
		return $listDir;
	}

	 
	 
	 
	 
	 
	public function permission($filename){
		$perms = fileperms($filename);

		if     (($perms & 0xC000) == 0xC000) { $info = 's'; }
		elseif (($perms & 0xA000) == 0xA000) { $info = 'l'; }
		elseif (($perms & 0x8000) == 0x8000) { $info = '-'; }
		elseif (($perms & 0x6000) == 0x6000) { $info = 'b'; }
		elseif (($perms & 0x4000) == 0x4000) { $info = 'd'; }
		elseif (($perms & 0x2000) == 0x2000) { $info = 'c'; }
		elseif (($perms & 0x1000) == 0x1000) { $info = 'p'; }
		else                                 { $info = 'u'; }

		// владелец
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));

		// группа
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));

		// все
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));

		return $info;
	}

	public function dir_list($dir, $justfiles = true){
		if ($dir[strlen($dir)-1] != '/') $dir .= '/';

		if (!is_dir($dir)) return array();

		$dir_handle  = opendir($dir);
		$dir_objects = array();
		while ($object = readdir($dir_handle))
		if (!in_array($object, array('.','..')))
		{
			$filename    = $dir . $object;
			$file_object = array(
                                            'name' => $object,
                                            'size' => filesize($filename),
                                            'perm' => fileutils::permission($filename),
                                            'type' => filetype($filename),
                                            'time' => date("Y-m-d H:i:s", filemtime($filename))
			);

			if($justfiles){
				if($file_object['type'] == 'file'){
					$dir_objects[] = $file_object;
				}
			}else {
				$dir_objects[] = $file_object;
			}

		}

		return $dir_objects;
	}
	 
	 
	/**
	 * Recursively delete a directory
	 *
	 * @param string $dir Directory name
	 * @param boolean $deleteRootToo Delete specified top-level directory as well
	 */
	public function unlinkRecursive($dir, $deleteRootToo){
		if(!$dh = @opendir($dir))
		{
			return;
		}
		while (false !== ($obj = readdir($dh)))
		{
			if($obj == '.' || $obj == '..')
			{
				continue;
			}

			if (!@unlink($dir . '/' . $obj))
			{
				unlinkRecursive($dir.'/'.$obj, true);
			}
		}

		closedir($dh);

		if ($deleteRootToo)
		{
			@rmdir($dir);
		}

		return;
	}
	 

	function file_get_contents_utf8($fn) {
		$content = file_get_contents($fn);
		return mb_convert_encoding($content, 'UTF-8',  mb_detect_encoding($content, 'UTF-8, ISO-8859-1, ISO-8859-2', true));
	}

	function convert_file_to_utf8($fn){
		file_put_contents($fn, fileutils::file_get_contents_utf8($fn));
	}


}
?>