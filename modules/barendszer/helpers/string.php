<?php defined('SYSPATH') or die('No direct script access.');

class string_Core {
	
	public static function trimString($string){
		return trim(trim($string),"\"");
	}
	
	public static function trimName($string,$length = 10, $end = '..'){
		if(strlen($string) > $length) {
			//utf8_encode(substr(utf8_decode($string),0,14));
			return mb_substr($string, 0, $length-2, 'utf-8').$end;//
			//return utf8_encode(substr(utf8_decode($string), 0, $length-2)).$end;
		}else{
			return $string;
		}
	}
	
	public static function replaceInLink($link){
		
		$thisa = array(
		"&amp;"
		);

		$thata = array(
		"&"
		);		

		return str_replace($thisa, $thata, $link);	
	}
	
	public static function replaceInMessageId($id){
		$id = trim($id);
		$thisa = array(
		"&lt;","&g=", "&gt=","&gt;"
		);

		$thata = array(
		"<",">",">",">"
		);		

		return str_replace($thisa, $thata, $id);	
	}
	
	
	
	public static function trim_kmuser($str)
	{
		$str = trim($str, '/');
		$str = trim($str, '<');
		$str = trim($str, '>');
		
		return trim($str);
	}	
	
	/**
	 * Trim Slashes
	 *
	 * Removes any leading/traling slashes from a string:
	 *
	 * /this/that/theother/
	 *
	 * becomes:
	 *
	 * this/that/theother
	 *
	 * @param	string
	 * @return	string
	 */
	public static function trim_slashes($str)
	{
		return trim($str, '/');
	}

	
	
	
	/**
	 * Strip Slashes
	 *
	 * Removes slashes contained in a string or in an array
	 *
	 * @param	mixed	string or array
	 * @return	mixed	string or array
	 */
	public static function strip_slashes($str)
	{
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = strip_slashes($val);
			}
		}
		else
		{
			$str = stripslashes($str);
		}

		return $str;
	}

	/**
	 * Strip Quotes
	 *
	 * Removes single and double quotes from a string
	 *
	 * @param	string
	 * @return	string
	 */
	public static function strip_quotes($str)
	{
		return str_replace(array('"', "'"), '', $str);
	}

	/**
	 * Quotes to Entities
	 *
	 * Converts single and double quotes to entities
	 *
	 \	 * @param	string
	 * @return	string
	 */
	function quotes_to_entities($str)
	{
		return str_replace(array("\'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $str);
	}

	/**
	 * Reduce Double Slashes
	 *
	 * Converts double slashes in a string to a single slash,
	 * except those found in http://
	 *
	 * http://www.some-site.com//index.php
	 *
	 * becomes:
	 *
	 * http://www.some-site.com/index.php
	 *
	 * @param	string
	 * @return	string
	 */
	public static function reduce_double_slashes($str)
	{
		return preg_replace("#([^:])//+#", "\\1/", $str);
	}

	/**
	 * Reduce Multiples
	 *
	 * Reduces multiple instances of a particular character.  Example:
	 *
	 * Fred, Bill,, Joe, Jimmy
	 *
	 * becomes:
	 *
	 * Fred, Bill, Joe, Jimmy
	 *
	 * @param	string
	 * @param	string	the character you wish to reduce
	 * @param	bool	TRUE/FALSE - whether to trim the character from the beginning/end
	 * @return	string
	 */
	public static function reduce_multiples($str, $character = ',', $trim = FALSE)
	{
		$str = preg_replace('#'.preg_quote($character, '#').'{2,}#', $character, $str);

		if ($trim === TRUE)
		{
			$str = trim($str, $character);
		}

		return $str;
	}

	/**
	 * Create a Random String
	 *
	 * Useful for generating passwords or hashes.
	 *
	 * @param	string 	type of random string.  Options: alunum, numeric, nozero, unique
	 * @param	integer	number of characters
	 * @return	string
	 * e8d4a1d58d 490a91dabe 0b8b6a98b5 3d
	 * 08c83791ef ec436899cf 85e95333ce d6
	 * 145017a23d 2b2cc5632d 356a98c1f5 1e
	 */
	public static function random_string($type = 'alnum', $len = 8)
	{
		switch($type)
		{
			case 'alnum'	:
			case 'numeric'	:
			case 'nozero'	:

				switch ($type)
				{
					case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					break;
					case 'numeric'	:	$pool = '0123456789';
					break;
					case 'nozero'	:	$pool = '123456789';
					break;
				}

				$str = '';
				for ($i=0; $i < $len; $i++)
				{
					$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
				}
				return $str;
				break;
			case 'unique' : return md5(time().uniqid(mt_rand()));
			break;
		}
	}

	/**
	 * Alternator
	 *
	 * Allows strings to be alternated.  See docs...
	 *
	 * @param	string (as many parameters as needed)
	 * @return	string
	 */
	public static function alternator()
	{
		static $i;

		if (func_num_args() == 0)
		{
			$i = 0;
			return '';
		}
		$args = func_get_args();
		return $args[($i++ % count($args))];
	}

	/**
	 * Repeater function
	 *
	 * @param	string
	 * @param	integer	number of repeats
	 * @return	string
	 */
	public static function repeater($data, $num = 1)
	{
		return (($num > 0) ? str_repeat($data, $num) : '');
	}


	public static function create_reference_string($string){

		$string = strtolower($string);

		$mit=array("(é)","(á)","(ű)","(ő)","(ú)","(ö)","(ü)","(ó)","(í)","(É)","(Á)","(Ű)","(Ő)","(Ú)","(Ö)","(Ü)","(Ó)","(Í)","( )");
		$mire=array("e","a","u","o","u","o","u","o","i","E","A","U","O","U","O","U","O","I","_");

		$string = str_replace("&Aacute;","A",$string);
		$string = str_replace("&aacute;","a",$string);
		$string = str_replace("&Eacute;","E",$string);
		$string = str_replace("&eacute;","e",$string);
		$string = str_replace("&Iacute;","I",$string);
		$string = str_replace("&iacute;","i",$string);
		$string = str_replace("&Oacute;","O",$string);
		$string = str_replace("&oacute;","o",$string);
		$string = str_replace("&#336;","O",$string);
		$string = str_replace("&#337;","o",$string);
		$string = str_replace("&Uacute;","U",$string);
		$string = str_replace("&uacute;","u",$string);
		$string = str_replace("&Uuml;","U",$string);
		$string = str_replace("&uuml;","u",$string);
		$string = str_replace("&#368;","U",$string);
		$string = str_replace("&#369;","u",$string);
		$string = str_replace("&Auml;","A",$string);
		$string = str_replace("&auml;","a",$string);
		$string = str_replace("&Ouml;","O",$string);
		$string = str_replace("&ouml;","o",$string);
		$string = str_replace("&Uuml;","U",$string);
		$string = str_replace("&uuml;","u",$string);
		$string = str_replace("&szlig;","s",$string);
		$string = str_replace("&otilde;","o",$string);
		$string = str_replace("&#245;","o",$string);
		$string = str_replace("!","",$string);
		$string = str_replace("?","",$string);
		$string = str_replace(".","",$string);
		$string = str_replace(":","",$string);
		$string = str_replace("-","",$string);
		$string = str_replace(",","",$string);
		$string = str_replace(";","",$string);
		$string = str_replace("(","",$string);
		$string = str_replace(")","",$string);
		$string = str_replace("'","",$string);
		$string = str_replace("/","",$string);
		$string = str_replace("*","",$string);
		$string = str_replace("¯","",$string);
		$string = str_replace("%","",$string);
		$string = str_replace("+","",$string);
		$string = str_replace("-","",$string);
		$string = str_replace("¯","",$string);
		$string = str_replace("=","",$string);
		$string = str_replace("@","",$string);
		$string = str_replace("<","",$string);
		$string = str_replace(">","",$string);
		$string = str_replace("[","",$string);
		$string = str_replace("]","",$string);
		$string = str_replace("{","",$string);
		$string = str_replace("}","",$string);
		$string = str_replace("#","",$string);
		$string = str_replace("ä","",$string);
		$string = str_replace("đ","",$string);
		$string = str_replace("ł","",$string);
		$string = str_replace("Ł","",$string);
		$string = str_replace("$","",$string);
		$string = str_replace("ß","",$string);
		$string = str_replace("`","",$string);
		$string = str_replace("|","",$string);
		$string = str_replace("Ä","",$string);
		$string = str_replace("®","",$string);
		$string = str_replace("™","",$string);
		$string = str_replace("€","",$string);
		$string = str_replace("Í","",$string);
		$string = str_replace("÷","",$string);
		$string = str_replace("×","",$string);
		$string = str_replace("˘","",$string);
		$string = str_replace("ˆ","",$string);
		$string = str_replace("ˇ","",$string);
		$string = str_replace("˚","",$string);
		$string = str_replace("˛","",$string);
		$string = str_replace("`","",$string);
		$string = str_replace("˙","",$string);
		$string = str_replace("˝","",$string);

		return preg_replace($mit,$mire,$string);
	}


	/* * Add a new variable to the query string
	 * or overwrites the value, if already exist */
	public static function add_to_query_string($param, $new_value) {
		$parameters = input::instance()->get();
		$parameters[$param] = $new_value;
		return url::base().url::current().'?'.http_build_query($parameters);
	}
	/* *
	 Remove a variable from the query string */
	public static function remove_from_query_string($param) {
		$parameters = input::instance()->get();
		unset($parameters[$param]);
		return url::base().url::current().'?'.http_build_query($parameters);
	}


	public static function replaceChars($string, $reverse = FALSE){
		$thisa = array(
		"Û","Û","Ô","Õ","ô","û","õ"
		);

		$thata = array(
		"Ű","Ű","Ő","Ő","ő","ű","ő"
		);		

		if(!$reverse){
			return str_replace($thisa, $thata, $string);	
		}else{
			return str_replace($thata, $thisa, $string);
		}
		
		
	}	
	
	/**
	 * szóközök mentén szétvág, kisbetüvé rak és minde szó elsőbetüje nagy
	 *
	 * @param unknown_type $string
	 */
	public static function ucfirst($string){
		
		$string = mb_convert_case($string, MB_CASE_TITLE, "utf-8");
		$string = strtolower($string);
		$names = explode(" ",strtolower($string));

		$_name = "";

		$i = 1;
		foreach($names as $name){

			$_name .= ucfirst($name);

			if($i != sizeof($names)){
				$_name .= " ";
			}
			$i++;


		}
			
		////ha a form iso-8859-2 es kódolásu volt akkor még maradhat benne kalapos
		//$_name = str_replace("û","ű",$_name);
		//$_name = str_replace("õ","ő",$_name);
		
		$_name = string::replaceChars($_name);
		return $_name;
	}

	/**
	 * Finds last character boundary prior to $maxLength in a utf-8
	 * Quoted (Printable) Encoded string $encodedText
	 * Original written by Colin Brown.
	 * @param string $encodedText utf8 QP text
	 * @param integer $maxLength finds last character boundary prior to this length
	 * @access private
	 * @return integer
	 */
	public static function UTF8CharBoundary($encodedText, $maxLength) {

		$foundSplitPos = False;
		$lookBack = 3;

		while (!$foundSplitPos)
		{
			$lastChunk = substr($encodedText, $maxLength - $lookBack, $lookBack);
			$encodedCharPos = strpos($lastChunk,'=');

			if ($encodedCharPos !== False) {
				// Found start of encoded character byte within last $lookBack chars
				// Check the encoded byte value (the 2 chars after the '=')
				$hex = substr($encodedText,$maxLength-$lookBack+$encodedCharPos+1,2);
				$dec = hexdec($hex);
				// construct a binary representation of this byte
				$byte = str_pad(decbin($dec), 8,'0', STR_PAD_LEFT);
				// First two character tell the type of character byte
				$isFirstOfMulti = strpos($byte,'11') === 0;
				$isMiddleOfMulti = strpos($byte,'10') === 0;
				$isSingle = strpos($byte,'0') === 0;
				if ($isSingle)
				{
					// Single byte character.
						
					// If the encoded char was found at pos 0, it will fit
					// otherwise reduce maxLength to start of the encoded char
						
					$maxLength = $encodedCharPos == 0 ?
					$maxLength :
					$maxLength - ($lookBack - $encodedCharPos);
					$foundSplitPos = True;
				}
				elseif ($isFirstOfMulti)
				{
					// First byte of a multi byte character
						
					// Reduce maxLength to split at start of character
					$maxLength = $maxLength - ($lookBack - $encodedCharPos);
					$foundSplitPos = True;
				}
				elseif ($isMiddleOfMulti)
				{
					// Middle byte of a multi byte character, look further back
					$lookBack += 3;
				}
			} else {
				// no encoded character found
				$foundSplitPos = True;
			}
		}
		return $maxLength;
	}


	public static function checkDomain($domainName){
		if(substr_count($domainName, ".") != 2 && substr_count($domainName, ".") != 1) {
			return false;
		}
		 

		$parts = explode(".", $domainName);

		if(sizeof($parts) == 2){

				/*** now we have 2 parts ***/
			if(!in_array($parts[1], string::getTldArray())  &&  !in_array(strtoupper($parts[1]), string::getCountryArray())){
				return false;
			}			
			
			
		}else if(sizeof($parts) == 3){///3 parts

			if(!in_array($parts[2], string::getTldArray())  &&  !in_array(strtoupper($parts[2]), string::getCountryArray())){
				return false;
			}			
			
		}
		

		return string::checkChars($domainName);
	}


	public static function getTldArray(){
		return array("name", "mobi", "pro", "edu", "int", "com", "net", "org", "museum", "csiro", "mil", "gov", "biz", "info", "root", "arpa", "aero", "cat", "jobs", "travel");
	}

	public static function getCountryArray(){
		return array("AF","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CY","CZ","DK","DJ","DM","DO","TP","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","FX","GF","PF","TF","GA","GM","GE","DE","GH","GI","GR","GL","GD","GP","GU","GT","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IL","IT","JM","JP","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","KN","LC","VC","WS","SM","ST","SA","SN","SC","SL","SG","SK","SI","SB","SO","ZA","GS","ES","LK","SH","PM","SD","SR","SJ","SZ","SE","CH","SY","TW","TJ","TZ","TH","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","YU","ZM","ZW");
	}

	public static function checkChars($domainName){
		if(!preg_match("/^(?:[^\W_]((?:[^\W_]|-){0,61}[^\W_])?\.)+[a-zA-Z]{2,6}\.?$/", $domainName)) {
			return false;
		}else{
			return true;
		}
	}

	
	public static function processFieldTag($tag, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep){
		
		$tag = substr($tag, $startSepSize,strlen($tag)-($endSepSize+2));
		
		if(strpos($tag, $fldUresSep) === false){
			$f = $tag;
			$uresErtek = "";		
		}else{
			list($f,$ures) = explode($fldUresSep, $tag);
			
			if(strpos($ures, $fldEmpSep) === false){
				$uresErtek = "";	
			}else{
				list($temp,$uresErtek) = explode($fldEmpSep, $ures);				
			}
						
		}		

		$return = array();
		$return['reference'] = $f;
		$return['empty'] = $uresErtek;

		return $return;		
	}	
	
	
	public static function translateUTF8ToWindowsCP1252($string) {
		$utf8 = array(
        '‚Ç¨', // ‚Ç¨
        '‚Äô', // ‚Äô
        '¬£', // ¬£
        '√Ä', // √Ä
        '√Å', // √Å
        '√Ç', // √Ç
        '√É', // √É
        '√Ñ', // √Ñ
        '√Ö', // √Ö
        '√Ü', // √Ü
        '√á', // √á
        '√à', // √à
        '√â', // √â
        '√ä', // √ä
        '√ã', // √ã
        '√å', // √å
        '√ç', // √ç
        '√é', // √é
        '√è', // √è
        '√ê', // √ê
        '√ë', // √ë
        '√í', // √í
        '√ì', // √ì
        '√î', // √î
        '√ï', // √ï
        '√ñ', // √ñ
        '√ó', // √ó
        '√ò', // √ò
        '√ô', // √ô
        '√ö', // √ö
        '√õ', // √õ
        '√ú', // √ú
        '√ù', // √ù
        '√û', // √û
        '√ü', // √ü
        '√†', // √†
        '√°', // √°
        '√¢', // √¢
        '√£', // √£
        '√§', // √§
        '√•', // √•
        '√¶', // √¶
        '√ß', // √ß
        '√®', // √®
        '√©', // √©
        '√™', // √™
        '√´', // √´
        '√¨', // √¨
        '√≠', // √≠
        '√Æ', // √Æ
        '√Ø', // √Ø
        '√∞', // √∞
        '√±', // √±
        '√≤', // √≤
        '√≥', // √≥
        '√¥', // √¥
        '√µ', // √µ
        '√∂', // √∂
        '√∑', // √∑
        '√∏', // √∏
        '√π', // √π
        '√∫', // √∫
        '√ª', // √ª
        '√º', // √º
        '√Ω', // √Ω
        '√æ', // √æ
        '√ø', // √ø
		);

		$cp1252 = array(
		chr(128), // ‚Ç¨
		chr(146), // ‚Äô
		chr(163), // ¬£
		chr(192), // √Ä
		chr(193), // √Å
		chr(194), // √Ç
		chr(195), // √É
		chr(196), // √Ñ
		chr(197), // √Ö
		chr(198), // √Ü
		chr(199), // √á
		chr(200), // √à
		chr(201), // √â
		chr(202), // √ä
		chr(203), // √ã
		chr(204), // √å
		chr(205), // √ç
		chr(206), // √é
		chr(207), // √è
		chr(208), // √ê
		chr(209), // √ë
		chr(210), // √í
		chr(211), // √ì
		chr(212), // √î
		chr(213), // √ï
		chr(214), // √ñ
		chr(215), // √ó
		chr(216), // √ò
		chr(217), // √ô
		chr(218), // √ö
		chr(219), // √õ
		chr(220), // √ú
		chr(221), // √ù
		chr(222), // √û
		chr(223), // √ü
		chr(224), // √†
		chr(225), // √°
		chr(226), // √¢
		chr(227), // √£
		chr(228), // √§
		chr(229), // √•
		chr(230), // √¶
		chr(231), // √ß
		chr(232), // √®
		chr(233), // √©
		chr(234), // √™
		chr(235), // √´
		chr(236), // √¨
		chr(237), // √≠
		chr(238), // √Æ
		chr(239), // √Ø
		chr(240), // √∞
		chr(241), // √±
		chr(242), // √≤
		chr(243), // √≥
		chr(244), // √¥
		chr(245), // √µ
		chr(246), // √∂
		chr(247), // √∑
		chr(248), // √∏
		chr(249), // √π
		chr(250), // √∫
		chr(251), // √ª
		chr(252), // √º
		chr(253), // √Ω
		chr(254), // √æ
		chr(255), // √ø
		);

		return str_replace($utf8, $cp1252, $string);
	}	
	
	
} // end of string helper
?>
