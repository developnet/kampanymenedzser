<?php defined('SYSPATH') or die('No direct script access.');

class arrayutils_Core {
	
	static private $sortfield = null;
    
	static private $sortorder = 1;
    
    static private function sort_callback(&$a, &$b) {
    	
    	$aa = strtoupper($a[self::$sortfield]);
    	$bb = strtoupper($b[self::$sortfield]);
    	
        if($aa == $bb) return 0;
        return ($aa < $bb)? -self::$sortorder : self::$sortorder;
        
    }
    
    static function sort(&$v, $field, $ord = "asc") {
        self::$sortfield = $field;
        self::$sortorder = ($ord=="asc") ? 1 : -1;
        
        usort($v, array('arrayutils', 'sort_callback'));
        
    }
    
    
    
	
}
?>