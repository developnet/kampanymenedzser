<?php
class appmodules_Core {
	
	public static function getMenu($module,$current) {
		
		$temp = new View(Kohana::config('admin.theme')."/"."module_menu");
	    
		$temp->module_side_title = Kohana::config($module.'.modulename');		
		
		$appm = Kohana::config($module.'.modulemenu');
		
		$modulemenus = array();
		
		foreach ($appm as $key => $am){
			
			$menu = array(
				"class" => "",
				"link" => $key,
				"name" => $am
				);
				
			if($current==$key)
				$menu['class']="current";	
				
			$modulemenus[] = $menu;			
			
		}
		
		
		$temp->modulemenus = $modulemenus;		

		return $temp->render(FALSE,FALSE);
	}
	
	
	public static function getMessage($type="warning", $text){
		return '<div class="'.$type.'">'.$text.'</div>';
	}
}
?>