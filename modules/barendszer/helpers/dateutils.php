<?php defined('SYSPATH') or die('No direct script access.');

class dateutils_Core {
	
	
    public static function formatDate($date) {
		$_a = preg_split('[ ]', $date);//
    	$a = preg_split('[-]', $_a[0]);
		if(isset($a[0]) && isset($a[1]) && isset($a[2])){
	    	return $a[0].". ".KOHANA::lang("barendszer.".$a[1])." ".$a[2].".";
	    				
		}else{
			return "";
		}
    		
////
    }
    

    public static function formatDateWithTime($date) {
    	$do = preg_split('[ ]', $date);
    	$da = dateutils::formatDate($do[0]);
    	
    	if(isset($do[1]))
    		return $da." ".$do[1];
    	else
    		return $da;
    	 
    }
    
    
    //calculates the nearest weekday (e.g. Sunday) to a given date.
	//Author John Borda January 2008 www.bordaline.co.uk
	//debugging help from Jake Arkinstall www.JakeArkinstall.com 
	//version 1.0.0
    public static function nrweekday($day = "2009-01-01")
    {
    	list($year,$month,$day) = explode("-",$day);
    	$day1 = mktime(0,0,0,$month,$day,$year);
        $day1 = ($day1 -259200); //subtract 3 days
        $checkday = date("l", $day1);
        // $checkday1 = $day1;
        while($checkday != "Monday"){ //change this day of week to suit your app.
        	$day1 = $day1 + 86400; 
        	$checkday = date("l", $day1);
        	//echo $checkday;
        }

    return date('Y-m-d',$day1);
    }    
	
}
?>