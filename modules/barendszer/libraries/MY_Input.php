<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Input library.
 *
 * $Id: Input.php 3250 2008-08-02 11:58:29Z armen $
 *
 * @package    Core
 * @author     Kohana Team
 * @copyright  (c) 2007-2008 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Input extends Input_Core {

	protected $raw_data = array();

	public function __construct()
	{
		
				
			// Use XSS clean?
			$this->use_xss_clean = (bool) KOHANA::config('core.global_xss_filtering');
	
			// Only run this once!
			if (self::$instance === NULL AND $this->use_xss_clean){
				// Disable the xss cleaner and copy the data
				$this->use_xss_clean = FALSE;
				
				$this->raw_data['post'] = $this->clean_input_data($_POST);
				$this->raw_data['get'] = $this->clean_input_data($_GET);
				$this->raw_data['cookie'] = $this->clean_input_data($_COOKIE);
	
				// Don't forget to turn it back on!
				$this->use_xss_clean = TRUE;
			}			
			
		


		parent::__construct();
		

	}

	/**
	 * Fetch raw global data
	 *
	 * @param   string   global data name
	 * @param   string   key to find
	 * @param   mixed    default value
	 * @return  mixed
	 */
	public function raw($type = 'post', $key = array(), $default = NULL)
	{
		if (isset($this->raw_data[$type]))
			return $this->search_array($this->raw_data[$type], $key, $default);
	}

} // End Input
