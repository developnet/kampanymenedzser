<?php defined('SYSPATH') or die('No direct script access.');

$lang = array
(
	'asc' => 'desc',
	'username' => 'Felhasználónév',
	'password' => 'Jelszó',
	'tologin' => 'Bejelentkezés',
	'badlogin' => 'Hibás felhasználónév vagy jelszó!',
	'logout' => 'Kijelentkezés',
 	
	'00' => '00',
	'01' => 'jan.',
	'02' => 'feb.',
	'03' => 'márc.',
	'04' => 'ápr.',
	'05' => 'máj.',
	'06' => 'jún.',
	'07' => 'júl.',
	'08' => 'aug.',
	'09' => 'szep.',
	'10' => 'okt.',
	'11' => 'nov.',
	'12' => 'dec.'

);
/*
 * 	'01' => 'január',
	'02' => 'február',
	'03' => 'március',
	'04' => 'április',
	'05' => 'május',
	'06' => 'június',
	'07' => 'július',
	'08' => 'augusztus',
	'09' => 'szeptmeber',
	'10' => 'október',
	'11' => 'november',
	'12' => 'deember'
 */
?>