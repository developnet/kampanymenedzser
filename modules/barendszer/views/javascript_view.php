<?php
header("Cache-Control: must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 
header("Pragma: no-cache"); 
header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")-2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT"); 
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

if($js_type)
	header('content-type: text/javascript');

echo $content;
?>