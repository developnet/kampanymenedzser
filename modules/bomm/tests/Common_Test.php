<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Example Test.
 *
 * $Id: Example_Test.php 3769 2008-12-15 00:48:56Z zombor $
 *
 * @package    Unit_Test
 * @author     Kohana Team
 * @copyright  (c) 2007-2008 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Common_Test extends Unit_Test_Case {

	// Disable this Test class?
	const DISABLED = FALSE;

	public $setup_has_run = FALSE;

	public function setup()
	{
		$this->setup_has_run = TRUE;
	}


	public function replace_in_link_test(){
		
		$link = 'http://kampanymenedzser.hu/{{nev}}/{{ceg}}/{{nincsilyen,ures=Na mi a helyzet?}}/{{akarmi}}';
		$eredmeny1 = 'http://kampanymenedzser.hu/Márton/DevelopNet Kft./Na mi a helyzet?/';
		$eredmeny2 = 'http://kampanymenedzser.hu/TcOhcnRvbg==/RGV2ZWxvcE5ldCBLZnQu/TmEgbWkgYSBoZWx5emV0Pw==/';
   
		
		$member = ORM::factory('omm_list_member', 4016);
		
		$this
			->assert_equal($eredmeny1, replace::replaceFields($member,$link))
			->assert_equal($eredmeny2, replace::replaceFields($member,$link,TRUE));

		//->assert_not_equal($var, 6)
		//->assert_same($var, '5')
		//->assert_not_same($var, 5);
	}


}
