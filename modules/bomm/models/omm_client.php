<?php
class Omm_client_Model extends ORM {

	protected $db = "own";
	
	protected $sorting = array('name' => 'asc');
	
	protected $has_many = array("omm_list","omm_campaign","omm_letter_template",'omm_riport');
	
	protected $has_and_belongs_to_many = array('users');
	
	public function getDB(){
		return $this->db;
	}
	
	public function getCommonLetters(){
		return ORM::factory("omm_common_letter")->where('status','active')->orderby('name','asc')->find_all();
	}
	
	public function getTags(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
				
		$sql = "SELECT tag.* FROM ".$tp.".omm_tags tag WHERE tag.status='active' ORDER BY tag.name";
		
		//$members = $this->db->from("omm_list_members")->where(array('omm_list_id!=' => 0, 'id !=' => $this->id, 'email' => $this->email))->orderby("reg_date","desc")->get();
		
		return $this->db->query($sql);
	}	
	
	public function membersNumberSelected($memberStatus,$memids){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	

		$sql = "SELECT COUNT(id) as ossz FROM ".$tp."omm_list_members m WHERE m.status = '".$memberStatus."' ";

			$idin = "";
			foreach($memids as $mid){
				$idin .= $mid.',';
			}
			
			$idin .= '0';
			
			$sql .= " AND m.id IN (".$idin.") ";		
		
			$query = $this->db->query($sql);
			return $query[0]->ossz;			
	}
	
	public function membersNumber($fields,$memberStatus = "active",$listfilter = "all",$groupfilter = "all",$tagfilter = "all",$productfilter = "all",$searchfield = "none", $searchkeyword = "none",$searchfields = array(),$rdate_from = "" ,$rdate_to = ""){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	

		if($groupfilter != "all"){
			
			$groups = explode("_", $groupfilter);//
			
			$allm = 0;
			foreach($groups as $g){
				$group = ORM::factory("omm_list_group")->find($g);
				$members = $group->getMemberCount($fields,$memberStatus, $listfilter,$tagfilter,$productfilter,$searchfield, $searchkeyword,$searchfields);
				$allm = $allm + $members;	
			}
			
			return $allm;
		}else{
			
			$sql = "SELECT COUNT(m.id) as ossz FROM ".$tp."omm_list_members m  ";
			if($searchfield != "none"){
				
				foreach ($searchfields as $f) {
					$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (m.id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
				}	
				
			}			
			
			$sql .= " WHERE m.status = '".$memberStatus."' ";
			$sql .= " AND m.omm_client_id='".$this->id."' ";

			if($rdate_from != ""){
				$sql .= " AND m.reg_date >= '".$rdate_from." 00:00:00' ";	
			}

			if($rdate_to != ""){
				$sql .= " AND m.reg_date <= '".$rdate_to." 23:59:59' ";	
			}			
			
			$sql .= Omm_client_Model::getFilterSql($listfilter,$tagfilter,$productfilter,$productfilter);
			$sql .= Omm_client_Model::getSearchSql($searchfield,$searchkeyword,$searchfields);
			
			$query = $this->db->query($sql);
			return $query[0]->ossz;
		}	
	}	
	
	
	
	
	public static function getFilterSql($listfilter,$tagfilter,$productfilter,$productfilter){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		$sql = "";
			
			if($listfilter != "all"){
				$sql .= "AND ( 1=2 ";
				$lists = explode("_", $listfilter);//
				
				foreach($lists as $list){
					$sql .= " OR EXISTS (SELECT id FROM ".$tp."omm_list_member_connect WHERE omm_list_id = ".$list." AND omm_list_member_id = m.id) ";	
				}
				
				$sql .= ")";
			}
			
			
			if($tagfilter != "all"){
				$sql .= "AND ( 1=2 ";
				$tags = explode("_", $tagfilter);//
				
				foreach($tags as $t){
					$sql .= " OR EXISTS (SELECT id FROM ".$tp."omm_tag_objects WHERE object_type = 'member' AND object_id = m.id AND tag_id=".$t." ) ";	
				}
				
				$sql .= ")";
			}			
			
			if($productfilter != "all"){
				$sql .= "AND ( 1=2 ";
				$prods = explode("_", $productfilter);//
				
				foreach($prods as $prod){
					$sql .= " OR EXISTS (SELECT id FROM ".$tp."omm_product_member_connects  WHERE omm_product_id = ".$prod." AND omm_list_member_id = m.id) ";	
				}
				
				$sql .= ")";
			}				
		
			return $sql;
	}
	//
	public static function getKeywordArray($searchkeyword){
		
		$ors = explode(" ",string::trimString($searchkeyword));
		$return = array();
		foreach($ors as $or){
			
			$ands = array();
			
			$keywords = explode(",",string::trimString($or));
			
			foreach($keywords as $keyword){
				if($keyword != "")
					$ands[] = strtolower($keyword);	
			}
			
			if(!empty($ands))
				$return[] = $ands;
		}
		
		return $return;
	}
	
	public static function getSearchSqlForField($field,$keywordarray){
		$sql = "";
		$sql .= " ( 1=2  ";
		foreach($keywordarray as $or){
			$sql .= " OR ( 1=1 ";
			foreach($or as $keyword){
				$sql .= "AND IFNULL(LOWER(".$field->reference.".value), LOWER(".$field->reference.".value_text)) LIKE '%".$keyword."%' ";
			}			
			$sql .= " ) ";			
		}
		$sql .= " ) ";
		
		return $sql;
	}
	
	public static function getSearchSqlForNormalField($field,$keywordarray){
		$sql = "";
		$sql .= " ( 1=2 ";
		foreach($keywordarray as $or){
			$sql .= " OR ( 1=1 ";
			foreach($or as $keyword){
				$sql .= "AND m.$field LIKE '%".$keyword."%' ";
			}			
			$sql .= " ) ";			
		}
		$sql .= " ) ";
		
		return $sql;
	}	
	
	public static function getKeywordFieldSql($keyword, $field){
		
		$keyword = str_replace("*", "%", $keyword);
		
		if(is_object($field)){
			$sql = " IFNULL(LOWER(".$field->reference.".value), LOWER(".$field->reference.".value_text)) LIKE '".$keyword."' ";		
		}else{
			$sql = " IFNULL(LOWER(".$field.".value), LOWER(".$field.".value_text)) LIKE '".$keyword."' ";
		}
	
		
		return $sql;
	}
	
	public static function getSearchSql($searchfield,$searchkeyword,$fields){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		if(!is_array($searchkeyword)){
			$keywordarray = Omm_client_Model::getKeywordArray($searchkeyword);	
		}else{
			$keywordarray = $searchkeyword;
		}
		
		
		
		
		$sql = "";
		
			if($searchfield == "all"){
				$sql .= " AND (1=2 ";
				foreach($fields as $field){////egy mező
					$sql .= " OR ( 1=2 ";
					foreach($keywordarray as $or){
						$sql .= " OR ( 1=2 ";
						foreach($or as $keyword){
							$sql .= ' OR '.Omm_client_Model::getKeywordFieldSql($keyword, $field);
						}
						$sql .= " ) ";
					}
					$sql .= " ) ";	
				}

					$sql .= " OR ( 1=2 ";
					foreach($keywordarray as $or){
						$sql .= " OR ( 1=2 ";
						foreach($or as $keyword){
							$sql .= " OR  m.email LIKE '%".$keyword."%'";
						}
						$sql .= " ) ";
					}
					$sql .= " ) ";				
				
				
	
				
				
				$sql .= " ) ";	
				
			//// searchfield = all	
			}elseif($searchfield == 'detail'){
				
				
				$sql .= " AND (1=1 ";
				foreach($keywordarray as $key => $val){
					
					if($val != "")
						$sql .= ' AND '.Omm_client_Model::getKeywordFieldSql($val, $key);
					
				}
				$sql .= " ) ";	
				
				
			}			
		
			//echo $searchfield;
		return $sql;
	}
	
	public function getMembersForSubscribersTableSelected($fields,$memberStatus,$orderby,$order,$offset,$rowperpage,$memids,$searchfields = array(),$export = false){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	

			$sql = "select ";
			$sql .= " m.id as id,";
			$sql .= " m.email as email,";
			$sql .= " m.status,";
			
			foreach ($searchfields as $f) {
				if($f->grid == 1 || $export){
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";	
				}
			}
			
			$sql .= " m.reg_date,";
			$sql .= " m.activation_date,";
			$sql .= " m.unsubscribe_date";
			
			
			$sql .= " FROM   ".$tp."omm_list_members m";
	
			
			$sql .= " WHERE 1=1 ";
			
			$sql .= " AND m.status='".$memberStatus."' ";	

			$idin = "";
			foreach($memids as $mid){
				
				$idin .= $mid.',';
				
			}
			$idin .= '0';
			
			$sql .= " AND m.id IN (".$idin.") ";
			
			$sql .= " order by $orderby $order ";
			

			$sql .= " LIMIT $offset,$rowperpage";
			
			//echo $sql;
			
			//echo $sql;
			return $this->db->query($sql);				
			
		
	}
	
	public function getMembersForSubscribersTable($fields, $memberStatus,$orderby,$order,$offset,$rowperpage,$searchfield = "none", $searchkeyword = "none",$listfilter = "all",$groupfilter = "all",$tagfilter = "all",$productfilter = "all",$searchfields = array(),$rdate_from = "",$rdate_to = "",$export = false){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		//echo $groupfilter;		
		if($groupfilter != "all"){
			$groups = explode("_", $groupfilter);
			
			//$group->getMemberCount($fields,$memberStatus = "active",$memberid = NULL);			
			
			$group = ORM::factory("omm_list_group")->find($groups[0]);
			$fields = $this->getAllFields($group->omm_list_id);
			$members = $group->getMembers($fields,$memberStatus,$orderby,$order,$offset,$rowperpage, true, $listfilter,$tagfilter,$productfilter,$searchfield, $searchkeyword,$searchfields,$rdate_from,$rdate_to,$export);
			
			return $members;
			
		}else{
			
		
			$sql = "select ";
			$sql .= " m.id as id,";
			$sql .= " m.email as email,";
			/*
			foreach ($fields as $f) {
				if($f->grid == 1){
					
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
					//$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";	
				}
			}
			*/
			foreach ($searchfields as $f) {
				if($f->grid == 1  || $export){
					
					$sql .= " IFNULL(".$f->reference.".value,".$f->reference.".value_text) as ".$f->reference.", ";
					//$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";	
				}
			}			
			
			$sql .= " m.status,"; //
			$sql .= " m.reg_date,"; //
			$sql .= " m.activation_date,";
			$sql .= " m.unsubscribe_date";
			
			
			$sql .= " FROM   ".$tp."omm_list_members m";
	
			if($searchfield != "none" || 1==1){
				
				foreach ($searchfields as $f) {
					$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (m.id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
				}	
				
			}		
			
			$sql .= " WHERE 1=1 ";
			
			$sql .= " AND m.status='".$memberStatus."' ";
			$sql .= " AND m.omm_client_id='".$this->id."' ";
			
			if($rdate_from != ""){
				$sql .= " AND m.reg_date >= '".$rdate_from." 00:00:00' ";	
			}

			if($rdate_to != ""){
				$sql .= " AND m.reg_date <= '".$rdate_to." 23:59:59' ";	
			}			
			
			
			$sql .= Omm_client_Model::getFilterSql($listfilter,$tagfilter,$productfilter,$productfilter);
			$sql .= Omm_client_Model::getSearchSql($searchfield,$searchkeyword,$searchfields);
			
			$sql .= " group by m.id ";
			
			if($memberStatus == 'unsubscribed' && $orderby == 'reg_date')//
				$sql .= " order by unsubscribe_date $order ";
			else
				$sql .= " order by $orderby $order ";

			
			$sql .= " LIMIT $offset,$rowperpage";
			
			//echo $sql;
			
			//echo $sql;
			return $this->db->query($sql);			
			
		}//////groupfilter = all
		
		

	}	

	
	
	
	public function getLastTestMember(){
		$m = ORM::factory("omm_list_member")->where('omm_client_id', 0)->orderby('reg_date', 'desc')->find();
		
		if($m->loaded){
			return $m;
		}else{
			return null;
		}
	}
	
	/**
	 * globális mezők
	 *
	 */
	public function getFields(){
		return ORM::factory('omm_list_field')->where('omm_list_id', 0)->find_all();
	
	}

	public function getAllFields($listid){////
		return ORM::factory('omm_list_field')->in('omm_list_id',array(0,$listid))->find_all();
	
	}	
	
	public function getAllAllFields(){////
		
		$lists = ORM::factory('omm_list')->where('omm_client_id', $this->id)->find_all();
		
		$listarray = array();
		$listarray[] = 0;
		foreach($lists as $l){
			$listarray[] = $l->id;
		}
		
		
		return ORM::factory('omm_list_field')->in('omm_list_id',$listarray)->find_all();
	}		
	
	
	public function gridFields(){
		
		return ORM::factory('omm_list_field')->where('omm_list_id', 0)->where("grid",1)->find_all();
			
	}	
	
	public function getMembersForImportByEmail($email = "",$fields = NULL){
		
		if($fields == NULL)
			$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
			
			
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.code as code,";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
		}
		$sql .= " IFNULL(m.activation_date, m.reg_date) as regdatum, ";
		$sql .= " m.status as statusz, ";
		$sql .= " m.mod_date as mod_date ";
		$sql .= " FROM   ".$tp."omm_list_members m";
		$sql .= " WHERE 1=1 ";
		
		if($email != ""){
			$sql .= " AND m.email = '".$email."' ";	
		}
		
		$sql .= " order by m.reg_date";
		//echo $sql;
		
		return $this->db->query($sql);
	}		
	
	public function getNameField(){
		
		$fullname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',0)->where('type', 'fullname')->count_all();
		
		if($fullname){
			$fullname = ORM::factory('omm_list_field')->where('omm_list_id',0)->where('type', 'fullname')->find();
			return $fullname->reference;
		}else{
			
			$firstname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',0)->where('type', 'firstname')->count_all();
			$lastname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',0)->where('type', 'lastname')->count_all();
			
			if($firstname && $lastname){
				return "bothnames";
			}elseif($firstname && !$lastname){
				return $this->searchReferenceByType("firstname");
			}elseif(!$firstname && $lastname){
				return $this->searchReferenceByType("firstname");
			}else{
				return $this->searchReferenceByType("email");
			}
		}
	}	
	
	public function searchReferenceByType($type){
		$fullname = ORM::factory('omm_list_field')->where('omm_list_id',0)->where('type', $type)->find();
		return $fullname->reference;
	}	
	
	public function campaigns($status = "active"){
		return ORM::factory("omm_campaign")->where("omm_client_id",$this->id)->where('status',$status)->orderby('name','asc')->find_all();
	}
	
	public function tempaltes(){
		return $this->omm_letter_template;						
	}		
	
	public function products($orderby="",$order="", $status = 'active'){
		if($orderby == "" && $order==""){
			return ORM::factory("omm_product")->where("omm_client_id",$this->id)->where('status',$status)->orderby("name")->find_all();
		}elseif($orderby != "" && $order==""){
			return ORM::factory("omm_product")->where("omm_client_id",$this->id)->where('status',$status)->orderby($orderby)->find_all();
		}else{
			return ORM::factory("omm_product")->where("omm_client_id",$this->id)->where('status',$status)->orderby($orderby,$order)->find_all();
		}
	}
	
	public function lists($orderby="",$order="", $status = 'active'){
		if($orderby == "" && $order==""){
			return ORM::factory("omm_list")->where("omm_client_id",$this->id)->where('status',$status)->orderby("name")->find_all();
		}elseif($orderby != "" && $order==""){
			return ORM::factory("omm_list")->where("omm_client_id",$this->id)->where('status',$status)->orderby($orderby)->find_all();
		}else{
			return ORM::factory("omm_list")->where("omm_client_id",$this->id)->where('status',$status)->orderby($orderby,$order)->find_all();
		}
	}
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}
	
	
	public function getAllSubscribedMember($status = 'active'){
		$tablename = Kohana::config("database.default");
		$table_prefix = $tablename['table_prefix'];
				
		$result = $this->db->query("SELECT count(*) as records_found FROM ".$table_prefix."omm_list_member_connect m, ".$table_prefix."omm_lists l,".$table_prefix."omm_clients c WHERE c.id=".$this->id." AND l.omm_client_id=c.id AND m.omm_list_id = l.id AND m.status='active' AND l.status='".$status."'");
		return $result[0]->records_found;
	}
	 
	
	public function riports($status = 'active'){
		return ORM::factory("omm_riport")->where("omm_client_id",$this->id)->where('status',$status)->orderby("name")->find_all();
	
	}
	
	public function deleteClient(){
	
		foreach ($this->lists() as $l){
			$l->deleteList();
		}

		foreach ($this->campaigns() as $c){
			$c->deleteCampaign();
		}		
		
		$this->status = 'deleted';
		$this->saveObject();
		
	}
	
	public function getMemberForImportById($member_id = "",$fields = NULL){
		
		if($fields == NULL)
			$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
			
			
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.code as code,";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
		}
		$sql .= " IFNULL(m.activation_date, m.reg_date) as regdatum, ";
		$sql .= " m.status as statusz, ";
		$sql .= " m.mod_date as mod_date ";
		$sql .= " FROM   ".$tp."omm_list_members m ";
		$sql .= " WHERE m.id = ".$member_id." ";
	
		$sql .= " order by m.reg_date";
		//echo $sql;
		
		return $this->db->query($sql);
	}	
	
	
}
?>