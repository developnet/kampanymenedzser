<?php
class Omm_list_connection_Model extends ORM {

	protected $db = "own";
	
	protected $belongs_to = array("list" => "omm_list");
	
	protected $has_one = array('other_list' 	 => 'omm_list');	
	
	public function relList(){
		return $this->other_list;
	}
	
}
?>