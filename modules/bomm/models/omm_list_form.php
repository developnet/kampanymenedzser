<?php
class Omm_list_form_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('name' => 'asc');
	
	protected $belongs_to = array("omm_list");

	protected $has_many = array("omm_list_form_field");
	
	public function getSubscribedMemberCount(){
		return ORM::factory('omm_list_member')->where('omm_list_id',$this->omm_list_id)->where('omm_list_form_id', $this->id)->where('status','active')->count_all();
	}
	
	public function deleteForm(){
		$this->deleteFields();
		$this->delete();
	}
	
	public function deleteFields(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$this->db->query("DELETE FROM ".$tp."omm_list_form_fields WHERE omm_list_form_id=".$this->id." ");
		
	}
	
	public function fields(){ 
		return $this->omm_list_form_field;
	}
	
	
	public function getHtml($raw = false, $action = null, $values = null, $km_user_code = null){
		$html = "";
		
		if($this->form_template == 'form_layout_4'){
			$formTempl = new View(Kohana::config('admin.theme')."/forms/form_template_modern");
		}else{
			$formTempl = new View(Kohana::config('admin.theme')."/forms/form_template");	
		}
		
		
        $formLayout = new View(Kohana::config('admin.theme')."/forms/".$this->form_template);	
        
        $formStyle = new View(Kohana::config('admin.theme')."/forms/".$this->form_style);
        
		$formTempl->assets = Kohana::config('core.assetspath').'formassets/';

        $formTempl->style = $formStyle->render(FALSE,FALSE);	
        $formTempl->layout = $formLayout->render(FALSE,FALSE);
        
        if(is_null($action)){

        	if($km_user_code != null){
        		$formTempl->formAction = url::site().'api/subscribe?a='.$km_user_code;	
        	}else{
        		$formTempl->formAction = url::site().'api/subscribe?a='.$_SESSION['km_user_code'];		
        	}
        	
        }else{
        	$formTempl->formAction = $action;
        }
        
	    if(!is_null($values)){
        	$formTempl->values = $values;	
        }        
        
        $formTempl->formCode = $this->code;
        $formTempl->listCode = $this->omm_list->code;
        $formTempl->fields = $this->fields();
        $formTempl->submitButtonLabel = $this->submit_button;
        
       		
		
       	if($raw){
       		
       		if(is_null($action)){
       			$formTempl->formAction = "";	
       		}
       		
       		$html = $formTempl->render(FALSE,FALSE);	
       		return $html;
       	}else{
       		$html = $formTempl->render(FALSE,FALSE);	
       		return html::specialchars($html);	
       	}
       	
		
		//return $html;
	}
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}
}
?>

