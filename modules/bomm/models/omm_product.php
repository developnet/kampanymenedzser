<?php
class Omm_product_Model extends ORM {
	
	protected $db = "own";
	
	protected $belongs_to = array("omm_client");


	public function getMembersForProductTable($fields, $memberStatus,$orderby,$order,$offset,$rowperpage,$showing = "all", $param = "all",$searchfield = "none", $searchkeyword = "none"){
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			if($f->grid == 1){
				$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";	
			}
		}
		
		$sql .= " c.order_date as order_date, ";
		$sql .= " c.purchase_date as purchase_date, ";
		$sql .= " c.failed_date as failed_date ";
		
		$sql .= " FROM   ".$tp."omm_list_members m, ".$tp."omm_product_member_connects c ";

		if($searchfield != "none"){
			
			foreach ($fields as $f) {
				$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (c.omm_list_member_id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
			}	
			
		}		
		
		$sql .= " WHERE c.omm_product_id=".$this->id." AND c.omm_list_member_id = m.id ";
		
		if($showing != "all"){

		}else{
			$sql .= " AND c.status='".$memberStatus."' ";
		}
		
		if($searchfield != "none" && $searchkeyword != "none" &&  $searchkeyword != ""  &&  $searchfield != "email"  &&  $searchfield != "order_date"  ){
			 $sql .= "AND IFNULL(".$searchfield.".value, ".$searchfield.".value_text) LIKE '%".$searchkeyword."%' ";
		}elseif($searchfield == "email"){
			$sql .= "AND m.email LIKE '%".$searchkeyword."%' ";
		}elseif($searchfield == "order_date"){
			$sql .= "AND c.order_date LIKE '%".$searchkeyword."%' ";
		}		
		
		$sql .= " order by $orderby $order ";
		
		if($searchfield != "none"){
			//$sql .= " LIMIT 200";	
		}else{
			$sql .= " LIMIT $offset,$rowperpage";
		}
		
		//echo $sql;
		
		//echo $sql;
		
		return $this->db->query($sql);
	}		
	
	
	public function saveObject(){
		
		if(isset($_SESSION['auth_user']))
			$this->mod_user_id = $_SESSION['auth_user']->id;
			
		$this->save();
	}	
	
	
	public function membersNumber($memberStatus = "purchased"){
		return $this->db->from("omm_product_member_connects")->where("omm_product_id",$this->id)->where('status',$memberStatus)->count_records();	
	}	
	
	public function created(){
		return dateutils::formatDate($this->created_date);
	}	
	
	public function getProductDate($member_id,$status){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
				
		if($status == 'purchased'){
			$datefield = 'purchase_date';
		}elseif($status == 'ordered'){
			$datefield = 'order_date';
		}elseif($status == 'failed'){
			$datefield = 'failed_date';
		}
		//
		
		$p = $this->db->query("SELECT ".$datefield." AS date 
									FROM ".$tp."omm_product_member_connects
									WHERE omm_list_member_id = ".$member_id." AND status='".$status."' AND omm_product_id = ".$this->id." ");		
		if(isset($p[0])){
			return dateutils::formatDateWithTime($p[0]->date);	
		}else{
			return 'nincs dátum';
		}
		
	}	
	
	public function createIfNotExist($name,$client_id = 0){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		$name = 
		string::strip_quotes($name);
		$result = $this->db->query("SELECT id FROM ".$tp."omm_products WHERE UPPER(name)='".strtoupper($name)."' LIMIT 1");
		
		if(isset($result[0])){

			return $result[0]->id;		
			//$tago->create($tag,'active');
			//$tago->saveObject();
		
		}else{
			
			$product = ORM::factory("omm_product");	

		    $product->code = string::random_string('unique');
		    if($client_id != 0 || !isset($_SESSION['selected_client'])){
		    	$product->omm_client_id = $client_id;
		    }else{
		    	$product->omm_client_id = $_SESSION['selected_client'];	
		    }
		    
		    $product->created_date = date("Y-m-d");	 			
	        $product->name = $name;
        	$product->saveObject();
	        				
			return $product->id;			
			
		}		
		
	}
	
	
	public function deleteProduct(){
		////self
		$this->status = "deleted";
		$this->saveObject();
		
	}	
	
}
?>