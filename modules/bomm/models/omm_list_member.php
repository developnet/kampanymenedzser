<?php
class Omm_list_member_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('reg_date' => 'desc');
	
	protected $belongs_to = array("omm_client");
	
	protected $has_many = array("omm_list_member_data");
	
	
	public static function _getTags($asString = false, $justTag = NULL,$memberid,$db){
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		if($justTag == NULL){//
			$sql = "SELECT tag.* FROM ".$tp.".omm_tags tag, ".$tp.".omm_tag_objects o WHERE o.tag_id = tag.id AND o.object_id = ".$memberid." AND o.object_type = 'member' AND tag.status='active'";	
		}else{
			$sql = "SELECT tag.* FROM ".$tp.".omm_tags tag, ".$tp.".omm_tag_objects o WHERE o.tag_id=".$justTag." AND o.tag_id = tag.id AND o.object_id = ".$memberid." AND o.object_type = 'member' AND tag.status='active'";
		}		
		
		
		//$members = $this->db->from("omm_list_members")->where(array('omm_list_id!=' => 0, 'id !=' => $this->id, 'email' => $this->email))->orderby("reg_date","desc")->get();
		$result = $db->query($sql);
		if($asString){
			$s = "";
			
			foreach($result as $t){
				$s .= $t->name.",";
			}
			
			return trim($s, ",");
			
		}else{
			return $result;	
		}		
		
	}
	
	
	public function getTags($asString = false, $justTag = NULL){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		if($justTag == NULL){//
			$sql = "SELECT tag.* FROM ".$tp.".omm_tags tag, ".$tp.".omm_tag_objects o WHERE o.tag_id = tag.id AND o.object_id = ".$this->id." AND o.object_type = 'member' AND tag.status='active'";	
		}else{
			$sql = "SELECT tag.* FROM ".$tp.".omm_tags tag, ".$tp.".omm_tag_objects o WHERE o.tag_id=".$justTag." AND o.tag_id = tag.id AND o.object_id = ".$this->id." AND o.object_type = 'member' AND tag.status='active'";
		}		
		
		
		//$members = $this->db->from("omm_list_members")->where(array('omm_list_id!=' => 0, 'id !=' => $this->id, 'email' => $this->email))->orderby("reg_date","desc")->get();
		$result = $this->db->query($sql);
		if($asString){
			$s = "";
			
			foreach($result as $t){
				$s .= $t->name.",";
			}
			
			return trim($s, ",");
			
		}else{
			return $result;	
		}
		
		
	}
	
	public function delTagById($tag_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		$this->db->query("DELETE FROM ".$tp.".omm_tag_objects WHERE tag_id=".$tag_id." AND object_id = ".$this->id." AND object_type='member' LIMIT 1");
	}
	
	public function delTagsById($tags = array()){
		
		foreach($tags as $r){
			$this->delTagById($r['id']);
		}		
		
	}
	
	public function delFromList($list_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$this->db->query("DELETE FROM ".$tp.".omm_list_member_connect WHERE omm_list_id=".$list_id." AND omm_list_member_id = ".$this->id."  LIMIT 1");
	}
	
	public static function delTagsByJustId($db,$member_id, $tags){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
					
		foreach($tags as $r){
			$db->query("DELETE FROM ".$tp.".omm_tag_objects WHERE tag_id=".$r['id']." AND object_id = ".$member_id." AND object_type='member' LIMIT 1");
		}			
	}
	
	public static function addTagsJusIds($db,$member_id, $tags){
		
		foreach($tags as $tag){
			
			if(isset($tag['id']) && $tag['id'] != ''){
				
				if(Omm_list_member_Model::hasTagJustIds($db,$member_id, $tag['id']) == 0 ){
					$db->insert("omm_tag_objects", array("tag_id" => $tag['id'], "object_id" => $member_id, "object_type" => 'member', "tagged_by" => $_SESSION['auth_user']->id));	
				}
				
			
			}elseif($tag['name'] != ""){//
				$tago = ORM::factory("omm_tag");		
				$tago->create($tag['name'],'active');
				$tago->saveObject();
				$db->insert("omm_tag_objects", array("tag_id" => $tago->id, "object_id" => $member_id, "object_type" => 'member', "tagged_by" => $_SESSION['auth_user']->id));
			}
		}		
	}
	
	public static function hasTagJustIds($db,$member_id, $tag_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$result = $db->query("SELECT COUNT(*) as record_found FROM ".$tp."omm_tag_objects WHERE object_id='".$member_id."' AND object_type='member' AND tag_id='".$tag_id."' ");
		return $result[0]->record_found;		
	}
	
	
	
	/**
	 * Hozzáadja és törli a cimkéket
	 *
	 * @param unknown_type $tags
	 */
	public function addTags($tags = array(), $delete_others = true){
		
		$tagids = array();
		foreach($tags as $tag){
			if(isset($tag['id']) && $tag['id'] != ''){
				$tagids[] = intval($tag['id']);						
			}
		}
		
		if($delete_others){
			$rec_tags = $this->getTags();
			foreach($rec_tags as $r){
				if(!in_array($r->id,$tagids)){
					$this->delTagById($r->id);
				}
			}			
		}

		if(isset($_SESSION['auth_user'])){
			$user = $_SESSION['auth_user']->id;
		}else{
			$user = 0;
		}
		
		
		foreach($tags as $tag){
			
			if(isset($tag['id']) && $tag['id'] != ''){
				
				if($this->hasTag($tag['id']) == 0 ){
					$this->db->insert("omm_tag_objects", array("tag_id" => $tag['id'], "object_id" => $this->id, "object_type" => 'member', "tagged_by" => $user));	
				}
				
			
			}elseif($tag['name'] != ""){//
				$tago = ORM::factory("omm_tag");		
				$tago->create($tag['name'],'active');
				$tago->saveObject();
				$this->db->insert("omm_tag_objects", array("tag_id" => $tago->id, "object_id" => $this->id, "object_type" => 'member', "tagged_by" => $user));
			}
		}
	}

	public function hasList($list_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$result = $this->db->query("SELECT COUNT(*) as record_found FROM ".$tp."omm_list_member_connect WHERE omm_list_member_id='".$this->id."' AND omm_list_id='".$list_id."'  ");
		return $result[0]->record_found;
	}	
	
	public function hasTag($tag_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$result = $this->db->query("SELECT COUNT(*) as record_found FROM ".$tp."omm_tag_objects WHERE object_id='".$this->id."' AND object_type='member' AND tag_id='".$tag_id."' ");
		return $result[0]->record_found;
	}	
	
	public function addProduct($product_id,$product_status, $date = null){
		
		if($date == NULL){
			$date = date('Y-m-d H:i:s'); 
		}
		
		if($product_status == "ordered"){//
			$this->db->insert("omm_product_member_connects", array("omm_product_id" => $product_id, "omm_list_member_id" => $this->id, "status" => $product_status, 'order_date' => $date));	
		}elseif($product_status == "purchased"){
			$this->db->insert("omm_product_member_connects", array("omm_product_id" => $product_id, "omm_list_member_id" => $this->id, "status" => $product_status, 'purchase_date' => $date));
		}elseif($product_status == "failed"){
			$this->db->insert("omm_product_member_connects", array("omm_product_id" => $product_id, "omm_list_member_id" => $this->id, "status" => $product_status, 'failed_date' => $date));
		}
		
	}
	
	public function addList($list_id, $reg_date){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		////////

		$sql = "SELECT count(id)as ossz FROM ".$tp."omm_list_member_connect WHERE omm_list_id=".$list_id." AND omm_list_member_id = ".$this->id." ";
		
		$res = $this->db->query($sql);
		
		if($res[0]->ossz <= 0){
			$this->db->insert("omm_list_member_connect", array("omm_list_id" => $list_id, "omm_list_member_id" => $this->id, "reg_date" => $reg_date, 'status' => 'active' ));	
		}		
		
			
	}
	
	public function  getProducts(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//////
				
		$sql = "SELECT
					p.name as product_name,
					p.id as product_id,
					c.order_date,
					c.id as connect_id,
					c.purchase_date,
					c.failed_date,
					c.status
					FROM 
					".$tp."omm_product_member_connects c, ".$tp."omm_products p 
					WHERE 
					c.omm_product_id = p.id
					AND
					c.omm_list_member_id = ".$this->id." AND p.status != 'deleted'
					ORDER BY c.order_date DESC,c.purchase_date DESC,c.failed_date DESC
					";
		
		return $this->db->query($sql);
	}
	
	/**
	 * Feliratkozó levelei
	 *
	 */
	public function getLetters(){
				
		$sql = "SELECT 
					job.id as job_id,
					job.type as job_type,
					job.status as job_status,
					job.sent_time as job_sent_time,
					job.prepared_time as job_prepared_time,
					job.created_time as job_created_time,
					letter.id as letter_id,
					letter.name as letter_name,
					letter.type as letter_type,
					letter.timing_type as letter_timing_type,
					letter.timing_event_type as letter_timing_event_type,
					letter.timing_event_value as letter_timing_event_value,
					letter.timing_date as letter_timing_date,
					letter.timing_hour as letter_timing_hour,
					cletter.id as cletter_id,
					cletter.name as cletter_name,
					cletter.type as cletter_type,
					cletter.note as cletter_note,
					camp.id as campaign_id,
					camp.name as campaign_name,
					(SELECT count(id) FROM omm_letter_link_clicks WHERE omm_letter_id = letter.id AND omm_list_member_id = ".$this->id." ) as click_count,
					(SELECT count(id) FROM omm_letter_open_log WHERE omm_letter_id = letter.id AND omm_list_member_id = ".$this->id." ) as open_count,
					(SELECT count(id) FROM omm_letter_bounces WHERE omm_letter_id = letter.id AND omm_list_member_id = ".$this->id." ) as bounce_count,					
					(SELECT count(id) FROM omm_common_letter_link_clicks WHERE omm_job_id =job.id AND omm_letter_id = cletter.id AND omm_list_member_id = ".$this->id." ) as common_click_count,
					(SELECT count(id) FROM omm_common_letter_open_log WHERE omm_job_id =job.id AND omm_letter_id = cletter.id AND omm_list_member_id = ".$this->id." ) as common_open_count,
					0 as common_bounce_count	
				FROM omm_jobs job
				LEFT JOIN omm_letters letter ON (letter.id = job.omm_letter_id)
				LEFT JOIN omm_common_letters cletter ON (cletter.id = job.omm_common_letter_id)
				LEFT JOIN omm_campaigns camp ON (camp.id = letter.omm_campaign_id)				
				WHERE 
					job.omm_list_member_id=".$this->id."
				AND
					job.type != 'test'
				AND
					job.status = 'sent'
				ORDER BY job.sent_time DESC
		";
		
		return $this->db->query($sql);
	}
	
	/**
	 * Nem csak other, hanem a user összes listája
	 *
	 * @return unknown
	 */	
	public function getLists($list_id = null){
		
		
		$client_id = $this->omm_client_id;
		
		if($list_id != NULL){
			
			$sql = "SELECT 
						list.id as list_id,
						connect.id as connect_id,
						list.name as list_name,
						list.status as list_status,
						list.note as list_note,
						connect.reg_date as reg_date,
						connect.unsubscribe_date as unsubscribe_date
					FROM 
						omm_list_member_connect connect,
						omm_lists list
					WHERE 
						connect.omm_list_member_id=".$this->id."
					AND
						connect.omm_list_id = list.id
					AND
						list.id = ".$list_id."						
					AND
						list.status != 'deleted'
					ORDER BY connect.reg_date DESC
			";			
			
		}else{

			$sql = "SELECT 
						list.id as list_id,
						connect.id as connect_id,
						list.name as list_name,
						list.status as list_status,
						list.note as list_note,
						connect.reg_date as reg_date,
						connect.unsubscribe_date as unsubscribe_date
					FROM 
						omm_list_member_connect connect,
						omm_lists list
					WHERE 
						connect.omm_list_member_id=".$this->id."
					AND
						connect.omm_list_id = list.id
					AND
						list.status != 'deleted'
					ORDER BY connect.reg_date DESC
			";			
			
		}
		

		
		return $this->db->query($sql);
	}	
	
	/**
	 * @deprecated 
	 */
	public function getOtherLists(){
		return array();
		
		$client_id = $this->omm_list->omm_client_id;
		$members = $this->db->from("omm_list_members")->where(array('omm_list_id!=' => 0, 'id !=' => $this->id, 'email' => $this->email))->orderby("reg_date","desc")->get();
		
		$lists = array();
		
		foreach($members as $m){
			
			$mm = ORM::factory("omm_list_member")->find($m->id);
			
			$l = ORM::factory("omm_list")->find($m->omm_list_id);
			
			if($l->omm_client_id != $client_id){
				continue;
			}
			
			$mo = $mm->getData($l->fields());
			
			$nameref = $l->getNameField();
			
			if($nameref == "bothnames"){
			
				$first = $l->searchReferenceByType("firstname");
				$last = $l->searchReferenceByType("lastname");
				
				$name = $mo->$last." ".$mo->$first;
			}else{
				
				if($nameref != ""){
					$name = $mo->$nameref;	
				}else{
					$name = $mo->email;
				}
				
				
			}			
			
			if($name == "" || $name == " "){
				$name = "(nincs név)";
			}
			$listrow = array(
				"list_id" => $l->id,
				"list_name" => $l->name,
				"member_status" => $m->status,
				"member_reg_date" => $m->reg_date,
				"member_unsub_date" => $m->unsubscribe_date,
				"member_name" => $name,
				"member_id" => $m->id
			);
			
			$lists[] = $listrow;
			
		}
		
		return $lists;
	}
	
	public function getMainDataForList($fields,$list_id){
			
		
	
	}
	
	public function getData($fields,$nolist = FALSE){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		
		
		if(sizeof($fields) == 0 || $fields[0]->omm_list_id == 0 || $nolist){ //globál adatok

			$sql = "select ";
			$sql .= " m.id as id,";
			$sql .= " m.email as email,";
			$sql .= " m.reg_date as reg_date, ";
			$sql .= " m.mod_date as mod_date, ";			
			$sql .= " m.reg_date as regdatum, ";			
			
			foreach ($fields as $f) {
				
				if($f->type == "singleselectdropdown" || $f->type == "singleselectradio"){
					$sql .= " (SELECT vv.value FROM ".$tp."omm_list_member_datas dd, ".$tp."omm_list_field_values vv where dd.omm_list_id = ".$f->omm_list_id." AND vv.omm_list_field_id = dd.omm_list_field_id AND vv.code = IFNULL(dd.value,dd.value_text) AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";		
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_id = ".$f->omm_list_id." AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference."_code, ";				
				}else{
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_id = ".$f->omm_list_id." AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";		
				}
									
			}

			$sql .= " m.status as status, ";
			$sql .= " m.code as code ";
			$sql .= " from   ".$tp."omm_list_members m";
			$sql .= " where m.id=".$this->id." ";
			$sql .= " LIMIT 1";			
			
		}else{ // listához tartozó adatok

			$sql = "select ";
			$sql .= " m.id as id,";
			$sql .= " m.email as email,";
			
			$sql .= " c.code as code,";
			foreach ($fields as $f) {
				
				if($f->type == "singleselectdropdown" || $f->type == "singleselectradio"){
					$sql .= " (SELECT vv.value FROM ".$tp."omm_list_member_datas dd, ".$tp."omm_list_field_values vv where dd.omm_list_id = ".$f->omm_list_id." AND vv.omm_list_field_id = dd.omm_list_field_id AND vv.code = IFNULL(dd.value,dd.value_text) AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";		
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_id = ".$f->omm_list_id." AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference."_code, ";				
				}else{
					$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_id = ".$f->omm_list_id." AND dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";		
				}
									
			}
			
			$sql .= " c.manual as manual, ";
			$sql .= " c.reg_date as reg_date, ";
			$sql .= " c.reg_date as regdatum, ";
			$sql .= " c.status as status ";
			$sql .= " from   ".$tp."omm_list_members m, omm_list_member_connect c";
			$sql .= " where m.id=".$this->id." AND c.omm_list_member_id = m.id AND c.omm_list_id = ".$fields[0]->omm_list_id." ";
			$sql .= " LIMIT 1";
				
			
		}
		
		//echo $sql;
		
		$r = $this->db->query($sql);
		
		
		
		
		return $r[0];
	}
	
	public function getFullData($fields,$nolist = FALSE){
		$member = $this->getData($fields,$nolist);
		
		foreach($fields as $f){
			
			if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){
				$ref = $f->reference."_code";
				$value = $f->getValueFromCode($ref);
				$member->$ref = $value;				
			}

			if($f->type == "multiselect"){
				$ref = $f->reference;
				$value = "";
				foreach($f->getValues() as $v){
					if( strpos($member->$ref , $v->code ) !== false ){
						
						if($v->value != "" && $v->value != " ")
							$value = $value.", ".$v->value;
							
					}
				}
				$value = trim($value, ','); 
				$member->$ref = $value;
			}			
		}
		
		return $member;
	}
	

	
	public function saveData($post,$fields,$update_empty = true){
		
        foreach ($fields as $f) {
			$ref = $f->reference;//
        	if(!$update_empty){
        		
        		if(isset($post->$ref) && $post->$ref == ""){
        			continue;
        		}
        		
        	}
        	
       		$data = ORM::factory("omm_list_member_data")->where("omm_list_field_id",$f->id)->where("omm_list_member_id",$this->id)->find();

       		if(!$data->loaded){
       			$data->omm_list_id = $f->omm_list_id;
       			$data->omm_list_field_id = $f->id;
       			$data->omm_list_member_id = $this->id;
        	}
        	$data->omm_list_id = $f->omm_list_id;
        	
	       	
        	$data->field_reference = $ref;

        	if(isset($post->$ref)){
        	    if(($f->type == "multiselect" || $f->type == "singleselectdropdown" || $f->type == "singleselectradio") && is_array($post->$ref)){
	        		$data->value = NULL;
	
	        		$value = "";
	        		
	        		foreach ($post->$ref as $d){
	        			$value .= $d;
	        			
	        			if($f->type == "multiselect"){
	        				$value .= ",";
	        			}
	        		}
	
	        		$data->value_text = $value;
	        	}elseif($f->type == "multitext"){
	        		$data->value = NULL;
	        		$data->value_text = $post->$ref;
	        	}else{
	        		
	        		if($f->type == "fullname" || $f->type == "firstname" || $f->type == "lastname" ){///kezdő nagybetü többi kicsi
	        			$data->value = string::ucfirst($post->$ref);
	        			//$data->value = $post->$ref;
	        		}else{
	        			$data->value = $post->$ref;
	        		}
	        			
	        		
	        	}        		
        	}else{
        		
        		if(!$data->loaded){
	        		 $data->value = NULL;
	        		 $data->value_text = "";        			
        		}
        		
        	}
	       		
        	$data->save();
        }
		$this->saveObject();
	}
	
	public function getTestData(){
		$data = ORM::factory("omm_list_member_data")->where('omm_list_member_id', $this->id)->where('field_reference','testfield_data')->find();
		if($data->loaded){
			return json_decode($data->value_text);
		}else{
			return NULL;
		}
	}
	
	/**
	 * TEST usert csinál...csak új orm példányon!!
	 *
	 * @param unknown_type $email
	 * @return unknown
	 */
	
	public function createTestUser($email,$listid,$regdate, $_data = ""){
		
		$code1 = string::random_string('unique');

		$this->reg_date = $regdate;
		$this->activation_date = $regdate;
		
		
		$this->code = $code1;
		$this->omm_client_id = $listid;
		$this->manual = 1;
		$this->email = $email;
		$this->status = 'test';
		$this->mod_user_id = 1;
		$this->save();
		
		if( $_data != ""){
			
			$data = ORM::factory("omm_list_member_data");
			
			$data->omm_list_member_id = $this->id;
			$data->omm_list_field_id = 0;
			$data->field_reference = 'testfield_data';
			
			$data->value = NULL;
			$data->value_text = $_data; 
			
			$data->save();
		}
		
		return $this->id;
	}		
	
	
	public function getMemberStat(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$ret = array('clickedratio' => 0, 'openedratio' => 0, 'allsent' => 0, 'allhtml' => 0, 'alllinks' => 0 , 'opened' => 0, 'clicked' => 0);		
		
		$ret['allsent'] = $this->getSentLetterCount();

		$ret['allhtml'] = $this->db->from("omm_jobs")->
								 where('omm_list_member_id',$this->id)->
								 where('status','sent')->
								 where('email_type','html')->
								 in("type",array('dayjob','common'))->
								 count_records();		
		
								 
								 
		$openres = $this->db->query("SELECT COUNT(DISTINCT omm_letter_id) AS `records_found` 
									FROM ".$tp."omm_letter_open_log
									WHERE omm_list_member_id = ".$this->id."");
		
		$openres_common = $this->db->query("SELECT COUNT(DISTINCT omm_job_id) AS `records_found` 
									FROM ".$tp."omm_common_letter_open_log
									WHERE omm_list_member_id = ".$this->id."");		
		
		//13378
		
		$ret['opened'] = $openres[0]->records_found + $openres_common[0]->records_found;
		
		if($ret['allhtml'] != 0){
			$ret['openedratio'] =  ( $ret['opened'] / $ret['allhtml']) * 100; 	
		}else{
			$ret['openedratio'] = 0;
		}		

		
		$ret['alllinks'] = $this->db->from("omm_jobs")->
								 where('omm_list_member_id',$this->id)->
								 where('status','sent')->
								 where('email_link_count > ',0)->
								 in("type",array('dayjob','common'))->
								 count_records();		
		
		
		$clickedres = $this->db->query("SELECT COUNT(DISTINCT omm_letter_id) AS `records_found` 
											FROM ".$tp."omm_letter_link_clicks
											WHERE omm_list_member_id = ".$this->id."
											AND omm_letter_link_id IS NOT NULL
											");
		
		$clickedres_common = $this->db->query("SELECT COUNT(DISTINCT omm_job_id) AS `records_found` 
											FROM ".$tp."omm_common_letter_link_clicks
											WHERE omm_list_member_id = ".$this->id."
											AND omm_letter_link_id IS NOT NULL
											");		
		
		$ret['clicked'] = $clickedres[0]->records_found + $clickedres_common[0]->records_found;
		
		if($ret['alllinks'] != 0){
			$ret['clickedratio'] =  ($ret['clicked'] / $ret['alllinks']) * 100; 	
		}else{
			$ret['clickedratio'] = 0;
		}
								 
		
		
		
		return $ret;
		
	}
	
	public function getSentLetterCount(){
		return $this->db->from("omm_jobs")->
							where('omm_list_member_id',$this->id)->
							where('status','sent')->
							in("type",array('dayjob','common'))->
							count_records();
	}
	

	
	public function getClickStat(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$ret = array('opened' => 0, 'all' => 0);
		
		$ret['all'] = $this->db->from("omm_jobs")->
								 where('omm_list_member_id',$this->id)->
								 where('status','sent')->
								 where('email_link_count > ',0)->
								 in("type",array('dayjob','sendnow'))->
								 count_records();


		
		$sql = "SELECT COUNT(DISTINCT j.id) AS `records_found` 
				FROM ".$tp."omm_jobs j,  ".$tp."omm_letter_link_redirections l
				WHERE j.id = l.omm_job_id
				AND l.url_name != 'unsubscribe'
				AND l.clicked IS NOT NULL
				AND j.omm_list_member_id = ".$this->id." 
				AND j.status = 'sent' 
				AND j.email_link_count > 0 
				AND j.type IN ('dayjob','sendnow')";
		
		$res = $this->db->query($sql);
		
		$opened = $res[0]->records_found;
		
		if($opened != 0){
			$ret['clicked'] =  ($ret['all'] / $opened) * 100; 	
		}else{
			$ret['clicked'] = 0;
		}
				
		return $ret;
		return $ret;
	}	
	
	
	
	public function saveObject(){
		$this->mod_date = date('Y-m-d H:i:s');
		if(isset($_SESSION)){
			$this->mod_user_id = $_SESSION['auth_user']->id;	
		}
		
		$this->save();
	}	
	
//	public function getDataObject($fields){
//
//		$data = array();
//		$data->id = $this->id;
//		$data['id'] = $this->id;
//		
//		$data->email = $this->email;
//		$data['email'] = $this->email;
//		
//		$data->reg_date = $this->reg_date;
//		$data['reg_date'] = $this->reg_date;
//		
//		$dataOrm = $this->omm_list_member_data;
//
//		$dataOrm = array();
//		
//		foreach ($fields as $f){
//			$freference = $f->reference;	
//			$data->$freference = NULL;
//			$data[$freference] = NULL;								
//		}
//		
//		foreach ($dataOrm as $d){
//			
//			$freference = $d->field_reference;
//			
//			if($d->value == "value_text"){
//				$data->$freference = $d->value_text;	
//				$data[$freference] = $d->value_text;	
//			}else{
//				$data->$freference = $d->value;
//				$data[$freference] = $d->value;
//			}
//			
//		}
//		
//		return $data;
//	}	
	
	//arrayutils::sort($records, $orderField,$order);
}
?>