<?php
require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

class Omm_letter_template_Model extends ORM {
	
	protected $db = "own";
	 
	public function replaceUrl($url){
		$this->html = str_replace("{{kepek_url}}",$url,$this->html);
	}

	
	public function trySetHtml($html){
		if($html != ""){
			$dom = new simple_html_dom();
	    	$dom->load($html, true);		
			
	    	$ret = $dom->find('body', 0);
	    	
	    	if($ret == null){
	    		return 0;
	    	}else{
	    		$html = str_replace("=iso-8859-2", "=utf-8",$html);
	    		$html = str_replace("=iso-8859-1", "=utf-8",$html);
	    		$html = str_replace("=ISO-8859-1", "=utf-8",$html);
	    		$html = str_replace("=ISO-8859-2", "=utf-8",$html);
	    		
	    		//$html = mb_convert_case($html, MB_CASE_TITLE, "utf-8");
	    		
	    		$mit=array("(õ)","(û)","(Õ)","(Û)","(Í)");
				$mire=array("ő","ű","Ő","Ű","Í");
	    		
				$html = preg_replace($mit,$mire,$html);
	    		//öüóõúéáûí - ÖÜÓÚÉÁÛÍ
	    		
	    		$this->html = $html;
	    		
	    		//$this->realhtml_content = $this->getRealHtml();
	    		
	    		return 1;
	    	}			
		}else{
			return 1;
		}

		
	}	
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}
	
}
?>