<?php

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

class Omm_job_Model extends ORM {

	protected $db = "own";

	protected $belongs_to = array('omm_letter','omm_common_letter','omm_list_member','omm_day_job');

	private $sepStart;
	private $sepEnd;
	private $unsubLink;
	private $actLink;

	private $modLink;

	private $startSepSize;
	private $endSepSize;

	private $fldEmp;
	private $fldEmpSep;
	private $fldUresSep;

	private $redirectlink;
	private $openlink;
	private $activateLink;

	private $datamodeLink;


	private $redirectTable;
	private $openTable;

	private $linksTable;

	public function __construct(){
		parent::__construct();
		$this->init();
	}

	private $unsubscribeLink;

	/**
	 * létrehozz a job-ot az alaphivatkozásokkal
	 *
	 * @param  $type = dayjob, sendnow, common, test (?)
	 * @param  $omm_letter_id
	 * @param  $omm_common_letter_id
	 * @param  $omm_list_member_id
	 * @param  $omm_day_job_id
	 */
	public function create($type, $omm_letter_id, $omm_common_letter_id, $omm_list_member_id, $omm_day_job_id ){

		$this->type = $type;
		$this->omm_letter_id = $omm_letter_id;
		$this->omm_common_letter_id = $omm_common_letter_id;
		$this->omm_list_member_id = $omm_list_member_id;
		$this->omm_day_job_id = $omm_day_job_id;

		$this->status = 'created';
		$this->created_time = date('Y-m-d H:i:s');
		$this->save();
	}

	private function init(){
		$this->sepStart = Kohana::config('core.fieldSeparatorStart');
		$this->sepEnd = Kohana::config('core.fieldSeparatorEnd');
		$this->unsubLink = Kohana::config('core.unsubscribe_link_name');
		$this->actLink = Kohana::config('core.activation_link_name');
		$this->modLink = Kohana::config('core.datamod_link_name');


		$this->fldEmp = Kohana::config('core.fieldEmpty');
		$this->fldEmpSep = Kohana::config('core.fieldEmptySeparator');
		$this->fldUresSep = Kohana::config('core.fieldUresSeparator');

		$this->startSepSize = strlen($this->sepStart);
		$this->endSepSize = strlen($this->sepEnd);
	}

	/**
	 * Előkészíti a levelet, legenerálja a tartalma, beírja a címett, feladó értékeket
	 *
	 */
	public function prepare($dom, $km_user = "",$list_id = NULL,$listcode = ""){

		if(isset($_SESSION['km_user_code'])){
			$km_user_code = $_SESSION['km_user_code'];
		}else{

			$dbcore = new Database("");
			$acc = $dbcore->query("SELECT code FROM omm_accounts WHERE km_user = '".$km_user."' ");

			if(sizeof($acc) > 0){
				$km_user_code = $acc[0]->code;
			}else{
				$km_user_code = "xx";
			}
				
		}

		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		
		if($km_user_code == "VvcGCrNx"){
			$url_base = 'https://hireso.garbaroyal.hu/';			
		}else{
			$url_base = url::base();
		}
		
		
		if($this->type == "dayjob"){
			$mail = $this->omm_letter;
			$this->redirectlink = $url_base.'api/redirect?a='.$km_user_code.'&url=';
			$this->openlink = $url_base.'api/openletter?a='.$km_user_code.'&code=';

			$this->linksTable = "omm_letter_links";
			$this->openTable = "omm_letter_open";

			//$list = $this->omm_list_member->omm_list;
			$client = $this->omm_list_member->omm_client;
			$member = $this->omm_list_member;
			
			if($list_id != NULL){
				$fields = $client->getAllFields($list_id);
			
			}else{
				$fields = $client->getAllAllFields();
				
			}
			
			
			$nameref = $client->getNameField();

		}else if($this->type == "sendnow"){
			$mail = $this->omm_letter;
			$this->redirectlink = $url_base.'api/redirect?a='.$km_user_code.'&url=';
			$this->openlink = $url_base.'api/openletter?a='.$km_user_code.'&code=';

			$this->linksTable = "omm_letter_links";
			$this->openTable = "omm_letter_open";

			$list = $this->omm_list_member->omm_list;
			$client = $list->omm_client;
			$member = $this->omm_list_member;
			$fields = $list->fields();
			$nameref = $list->getNameField();

		}else if($this->type == "common"){
			$mail = $this->omm_common_letter;
			$this->redirectlink = $url_base.'api/redirectcom?a='.$km_user_code.'&url=';
			$this->openlink = $url_base.'api/opencommonletter?a='.$km_user_code.'&code=';

			$this->linksTable = "omm_common_letter_links";
			$this->openTable = "omm_common_letter_open";


			//$list = $this->omm_list_member->omm_list;
			$client = $this->omm_list_member->omm_client;
			$member = $this->omm_list_member;
			
			$fields = $client->getAllAllFields();
			
			$nameref = $client->getNameField();

		}else if($this->type == "test"){
			$mail = $this->omm_letter;
			$this->redirectlink = $url_base.'api/redirect?a='.$km_user_code.'&url=';
			$this->openlink = $url_base.'api/openletter?a='.$km_user_code.'&code=';
			$this->openTable = "omm_letter_open_log";

			$list = null;
			//$client = $list->omm_client;
			$member = $this->omm_list_member;
			$fields = array();
			$nameref = "";
		}

		$link_count = 0;
		//////feladó adatai
		$this->email_type = $mail->type;
		$this->email_sender_name = $mail->sender_name;
		$this->email_sender_email = $mail->sender_email;

		if($mail->sender_replyto != NULL && $mail->sender_replyto != ""){
			$this->email_sender_replyto = $mail->sender_replyto;
		}



		if($member->loaded){

			$mo = $member->getFullData($fields, TRUE);
			if($nameref == "bothnames"){

				$first = $client->searchReferenceByType("firstname");
				$last = $client->searchReferenceByType("lastname");

				$name = $mo->$last." ".$mo->$first;
			}elseif($nameref != ""){

				try {
					$name = $mo->$nameref;
				}catch(Exception $e){
					$name = "";
				}

			}else{
				$name = "";
			}

			//$name = "";

			////címzett adatai
			$this->email_recipient_name = $name;
			$this->email_recipient_email = $member->email;

			$html = $mail->html_content;

			$text = $mail->text_content;
			$sub = $mail->subject;

					$runForTest = false;
			
					if($this->type == "test"){
						$testData = $member->getTestData();
						
						if($testData != NULL){
							
							foreach ($testData as $key => $td){
								$mo->$key = $td;
							}
		
							$runForTest = true;
						}
							
					}				
				
				if($this->type != "test" || ($this->type == "test" && $runForTest)) {//////tesztnél nem helyettesítünk, de már helyettesítünk
						
	
					////mezők értékeinek behelyettesítése + leiratkozó link
	
					///{{[^{{]+?}}/gi
					$pattern = '/'.$this->sepStart.'[^'.$this->sepStart.']+?'.$this->sepEnd.'/i';
	
					if($this->type != "test"){
	
						/////////leiratkozo link redirect
						//$realunsub = url::base().'api/unsubscribe?code='.$member->code;
						$realunsub = $url_base.'api/unsubscribe';
		
		
		
		
						$linkres = $this->db->query("SELECT code
					                    			   FROM ".$tp.$this->linksTable." 
													  WHERE omm_letter_id = ".$mail->id." 
													    AND url_name = 'unsubscribe' LIMIT 1");				
		
						if(sizeof($linkres) != 0){
							$unsublink_code = $linkres[0]->code;
						}else{
							$unsublink_code = string::random_string('unique');
		
							$this->db->query("INSERT INTO ".$tp.$this->linksTable."
												SET omm_letter_id = ".$mail->id.",
													code = '".$unsublink_code."',
													url_name = 'unsubscribe',
													url = '".$realunsub."'
												");					
		
						}
		
						$this->unsubscribeLink = $this->redirectlink.$unsublink_code."&m=".$member->code;
						/////////leiratkozo link redirect
		
						$this->activateLink = $url_base.'api/activate?a='.$km_user_code.'&code='.$member->code.'&lcode='.$listcode;
		
						$this->datamodeLink = $url_base.'api/modify?a='.$km_user_code.'&code='.$member->code;
							
						
					}
					
	
	
					////html content
					$matches = array();
					preg_match_all($pattern, $html, $matches);
	
					foreach($matches as $match){
						foreach($match as $m){
							$this->replaceMatch($m,$mo,$html);
						}
					}
					////////////html
	
					////text content
					$matches = array();
					preg_match_all($pattern, $text, $matches);
	
					foreach($matches as $match){
						foreach($match as $m){
							$this->replaceMatch($m,$mo,$text);
						}
					}
					////////////text
	
					//////////subject
					$matches = array();
					preg_match_all($pattern, $sub, $matches);
	
					foreach($matches as $match){
						foreach($match as $m){
							$this->replaceMatch($m,$mo,$sub);
						}
					}
					//////////subject
	
					//////////email_sender_name
					$sender_name = $this->email_sender_name;
					$matches = array();
					preg_match_all($pattern, $sender_name, $matches);
	
					foreach($matches as $match){
						foreach($match as $m){
							$this->replaceMatch($m,$mo,$sender_name);
						}
					}
					//////////email_sender_name
	
	
					//////////email_sender_email
					$sender_email = $this->email_sender_email;
					$matches = array();
					preg_match_all($pattern, $sender_email, $matches);
	
					foreach($matches as $match){
						foreach($match as $m){
							$this->replaceMatch($m,$mo,$sender_email);
						}
					}
					//////////email_sender_email
	
					$this->email_sender_name = $sender_name;
					$this->email_sender_email = $sender_email;
	
				}///////teszt check behelyettesítés
							
			
			/////////////HTML feldolgozás, linkek
			if($mail->type == "html"){/////////csak akkor ha html
				
				
				$dom->load($html, true);
				
				if($this->type != "test") { //////tesztnél nem cserélünk linket

					if($this->type == "common") {///omm_job_id
						
						$maillinkek = $this->db->query("SELECT *
										                  FROM ".$tp.$this->linksTable." 
													     WHERE omm_letter_id = ".$mail->id." 
													       AND omm_job_id = ".$this->id."
													     ORDER BY link_number");						
						
					}else{
						
						$maillinkek = $this->db->query("SELECT *
										                  FROM ".$tp.$this->linksTable." 
													     WHERE omm_letter_id = ".$mail->id." 
													     ORDER BY link_number");						
						
					}


					$dontupdateMails = array();

					////linkek
					$linknum = 1;
					foreach($dom->find('a') as $element){

						$link_url = $element->href;
						$link_name = $element->plaintext;
						if($link_name == ""){
							$link_name = $element->innertext;
						}

						//&amp;
						$link_url = string::replaceInLink($link_url);
							
						if($link_url == $this->sepStart.$this->unsubLink.$this->sepEnd || $link_url == $this->unsubscribeLink){//ha leiratkozó link
							////akkor nem csinálunk semmit
						}elseif($link_url == $this->sepStart.$this->actLink.$this->sepEnd || $link_url == $this->activateLink){///ha aktivációs link
							////akkor nem csinálunk semmit
						}elseif($link_url == $this->sepStart.$this->modLink.$this->sepEnd || $link_url == $this->datamodeLink){///ha adatmódosító link
							////akkor nem csinálunk semmit
						}elseif(strpos($link_url, "@") > 0 || strpos($link_url, "mailto") > 0){///ha e-mail link
							////akkor nem csinálunk semmit
						}else{

							///////////sima link


							try{
								$resultlink = null;

								foreach ($maillinkek as $link){

									if($link_url == $link->url && $link_name == $link->url_name && $linknum == $link->link_number){
										$resultlink = $link;
										break;
									}

								}


								if($resultlink != null){

									$this->db->query("UPDATE ".$tp.$this->linksTable." SET link_number = ".$linknum." WHERE id = ".$link->id." ");
									$dontupdateMails[] = $link->id;

									$link_code = $link->code;

								}else{


									$link_code = string::random_string('unique');
									$job_id = $this->id;
									$member_id = $member->id;
									
									if($this->type == "common") {///omm_job_id
										
										$this->db->query("INSERT INTO ".$tp.$this->linksTable."
															SET omm_letter_id = ".$mail->id.",
																omm_job_id = ".$this->id.",
																link_number = ".$linknum.",
																code = '".$link_code."',
																url_name = '".$link_name."',
																url = '".$link_url."'
															");										
										
									}else{

										$this->db->query("INSERT INTO ".$tp.$this->linksTable."
															SET omm_letter_id = ".$mail->id.",
																link_number = ".$linknum.",
																code = '".$link_code."',
																url_name = '".$link_name."',
																url = '".$link_url."'
															");										
										
									}
									
									
								}

								$element->href = $this->redirectlink.$link_code."&m=".$member->code;
								$link_count++;
								$linknum++;
							}catch (Exception $e){
								KOHANA::log("error",$e);
							}
							///////sima link

						}
					}

					/////update régi linkek

					foreach ($maillinkek as $link){
						if(!in_array($link->id,$dontupdateMails) && $link->url_name != "unsubscribe"){
							$this->db->query("UPDATE ".$tp.$this->linksTable." SET omm_letter_id = 0 WHERE id = ".$link->id." ");
						}
					}


				}/////linkcsere


				if($this->type != "test") {//////tesztnél nem csinálunk megnyitás képet
					foreach($dom->find('body') as $element){
							
						////megnyitás elleőrző képet hozzárakjuk a BODY-hoz...annak ugy is kell benne lennie
						try{
							$inner = $element->innertext;

							if($this->type =='common'){///omm_job_id
								
								$openres = $this->db->query("SELECT code
								                			   FROM ".$tp.$this->openTable." 
															  WHERE omm_letter_id = ".$mail->id." 
															    AND omm_job_id = ".$this->id."	
															  	LIMIT 1");								
								
							}else{

								$openres = $this->db->query("SELECT code
								                			   FROM ".$tp.$this->openTable." 
															  WHERE 
															  	omm_letter_id = ".$mail->id." LIMIT 1");								
								
							}
							

							if(sizeof($openres) != 0){
								$code = $openres[0]->code;
							}else{
								$code = string::random_string('unique');

								if($this->type =='common'){///omm_job_id								
									
									$this->db->query("INSERT INTO ".$tp.$this->openTable."
															SET omm_letter_id = ".$mail->id.", omm_job_id = ".$this->id.", code = '".$code."'
															");
																		
									
								}else{	
									$this->db->query("INSERT INTO ".$tp.$this->openTable."
															SET omm_letter_id = ".$mail->id.",  
																code = '".$code."'
															");
								}
							}

							$src = $this->openlink.$code."&m=".$member->code;

							$element->innertext = $inner.'<img src="'.$src.'" width="1" height="1" alt="" />';

							unset($inner);

						}catch (Exception $e){
							KOHANA::log("error",$e);
					
						}
						/////////megnyitás elleőrző kép

							
					}
				}
				$html = $dom->save();

				/////////linkek

			}

			//print_r($mo);
			
			


			$this->email_subject =  $sub;
			
			if($this->type == 'test'){
				$this->email_subject = '[TESZT] '.$sub;
			}
			
			$this->email_content_html =  $html;
			$this->email_content_text =  $text;
			$this->email_link_count = $link_count;
			$this->status = 'prepared';
			$this->prepared_time = date('Y-m-d H:i:s');
			$this->save();
		}else{
			KOHANA::log("error","nincs member vagy nem aktív");
			//nincs member vagy nem aktív
		}


		if(isset($list)) unset($list);
		if(isset($client)) unset($client);
		if(isset($member)) unset($member);
		if(isset($html)) unset($html);
		if(isset($fields)) unset($fields);
		if(isset($mo)) unset($mo);

	}

	private function replaceMatch($m,$mo,&$text){

		if($m == $this->sepStart.$this->unsubLink.$this->sepEnd && ($this->type != "test")) {////leiratkozo link

			$text = str_ireplace($m, $this->unsubscribeLink, $text);

		}elseif($m == $this->sepStart.$this->actLink.$this->sepEnd && ($this->type != "test")){////ha aktivációs link

			$text = str_ireplace($m, $this->activateLink, $text);

		}elseif($m == $this->sepStart.$this->modLink.$this->sepEnd && ($this->type != "test")){////ha adatmódosító link

			$text = str_ireplace($m, $this->datamodeLink, $text);

		}elseif($m == $this->sepStart.'code'.$this->sepEnd && ($this->type != "test")){////ha code

			$text = str_ireplace($m, $mo->code, $text);

		}else{/////ha sime mező hivatkozás

			
			
			$ref = string::processFieldTag($m, $this->startSepSize, $this->endSepSize, $this->fldUresSep, $this->fldEmpSep);

			$reference = $ref['reference'];
			$empty = $ref['empty'];

			if(!isset($mo->$reference) || $mo->$reference == NULL || $mo->$reference == "" ){
				$text = str_ireplace($m, $empty, $text);
			}else{
				$text = str_ireplace($m, $mo->$reference, $text);
			}
			
			
			
		}

	}



	/**
	 * csak indokolt esetben futtatunk önállóan egy job-ot
	 *csk COMMON
	 */
	public function execute(){
		$start = time();

		$swift = jobs::getSwift(false, true);
		jobs::sendJobs(array($this),$this->email_type,$swift,$this->db);
		$swift->disconnect();
		$end = time();
		$this->exec_time = $end-$start;
		$this->save();
	}



}
?>