<?php 
class Omm_message_Model extends ORM {
	
	protected $db = "default";
	
	protected $sorting = array('date' => 'desc', 'time' => 'desc');
	
	public function saveObject(){
		
		if(isset($_SESSION['auth_user'])){
			$this->mod_user_id = $_SESSION['auth_user']->id;	
		}else{
			$this->mod_user_id = 0;
		}
		
		$this->save();
	}
	
	
	public function create($recipient,$type,$subject,$preview,$body = ""){
		
		$this->recipient = $recipient;
		$this->type = $type;
		$this->subject = $subject;
		$this->preview = $preview;
		$this->body = $body;
		
		$this->date = date("Y-m-d");
		$this->time = date("H:i:s");
		
		$this->saveObject();
	}
	
}
?>