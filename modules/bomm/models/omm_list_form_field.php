<?php
class Omm_list_form_field_Model extends ORM {

	protected $db = "own";
	
	protected $sorting = array('ord' => 'asc');
	
	protected $belongs_to = array("omm_list_form");
	
	protected $has_one = array('omm_list_field' => 'omm_list_field');
	
	
	
	public function saveObj(){
		
		$isthereany = (bool) ORM::factory('omm_list_form_field')->where('omm_list_form_id',$this->omm_list_form_id)->where('omm_list_field_id', $this->omm_list_field_id)->count_all();
		
		if(!$isthereany){
			$this->save();
		}
		
		
	}
	
	public function getName(){
		if($this->field_reference == 'email'){
			return "E-mail cím";	
		}else{
			return $this->omm_list_field->name;			
		}

	}
	
	
}
?>