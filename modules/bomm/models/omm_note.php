<?php 
class Omm_note_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('slug' => 'asc', 'created' => 'asc');
	
	
	public static function getTagsForMember($member_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
				
		$sql = "SELECT tag.* FROM ".$tp.".omm_notes tag, ".$tp.".omm_note_objects o WHERE o.tag_id = tag.id AND o.object_id = ".$member_id." AND o.object_type = 'member' AND tag.status='active' ORDER by tag.name";
		
		//$members = $this->db->from("omm_list_members")->where(array('omm_list_id!=' => 0, 'id !=' => $this->id, 'email' => $this->email))->orderby("reg_date","desc")->get();
		$db = new Database("own");
		return $db->query($sql);
	}	
	
	public function saveObject(){
		
		if(isset($_SESSION['auth_user'])){
			$this->mod_user_id = $_SESSION['auth_user']->id;	
		}else{
			$this->mod_user_id = 0;
		}
		
		$this->save();
	}
	
	public function searchTagId($t){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$result = $this->db->query("SELECT id FROM ".$tp."omm_notes WHERE UPPER(name)='".strtoupper($t)."' LIMIT 1");

		if(isset($result[0])){
			return $result[0]->id;		
		}else{
			return 0;			
		}		
	}
	
	public function createIfNotExist($name,$status,$color1 = 'dddddd',$color2 = '666666'){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$result = $this->db->query("SELECT id FROM ".$tp."omm_notes WHERE UPPER(name)='".strtoupper($name)."' LIMIT 1");
		
		if(isset($result[0])){

			return $result[0]->id;		
			//$tago->create($tag,'active');
			//$tago->saveObject();
		
		}else{
			
			$tago = ORM::factory("omm_note");		
			$tago->create($name,$status);
			$tago->saveObject();
			return $tago->id;			
			
		}
		
	}
	
	public function create($name,$status,$color1 = 'dddddd',$color2 = '666666'){
		//
		$this->slug = $this->getSlug($name);
		$this->name = $name;
		$this->status = $status; //active,hidden,deleted 
		
		if($color1 != ''){
			$this->color1 = $color1;	
		}
		
		if($color2 != ''){
			$this->color2 = $color2;	
		}
		
		
		$this->created = date("Y-m-d H:i:s");
		$this->saveObject();
	}
	
	private function getSlug($name){
		
		$slug = string::create_reference_string($name);
		
		$i = 1;
		
		while($this->isSlugUnique($slug) > 0 ){

			$slug = $slug."_".$i;
			$i++;
			
		}
		return $slug;
	}
	
	public function isSlugUnique($slug){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		
		$result = $this->db->query("SELECT COUNT(*) as record_found FROM ".$tp."omm_notes WHERE slug='".$slug."' ");
		return $result[0]->record_found;
	}	
	
}
?>