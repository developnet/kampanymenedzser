<?php
require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

class Omm_common_letter_Model extends ORM {
	
	protected $db = "own";
	
	protected $belongs_to = array("omm_list");
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}		

	public function getRealHtml(){
		return $this->html_content; 
	}
	
	public function setHtmlContent($html){
		
		/*
		$typo = $this->getDetail("typo");
        $typoCssView = new View(Kohana::config('admin.theme')."/typos/".$typo);		
        $css = $typoCssView->render(FALSE,FALSE);	
        
		$em = new Emogrifier($html,$css);
		$em->emogrify();
		*/
		
		$html = str_ireplace("%7B%7B", "{{", $html);
		$html = str_ireplace("%7D%7D", "}}", $html);
		$this->html_content = $html;
	}	
	
	public function trySetHtml($html){
		$dom = new simple_html_dom();
    	$dom->load($html, true);		
		
    	$ret = $dom->find('body', 0);
    	
    	if($ret == null){
    		return 0;
    	}else{
    		$html = str_replace("=iso-8859-2", "=utf-8",$html);
    		$html = str_replace("=iso-8859-1", "=utf-8",$html);
    		$html = str_replace("=ISO-8859-1", "=utf-8",$html);
    		$html = str_replace("=ISO-8859-2", "=utf-8",$html);
    		
    		//$html = mb_convert_case($html, MB_CASE_TITLE, "utf-8");
    		
    		$mit=array("(õ)","(û)","(Õ)","(Û)","(Í)");
			$mire=array("ő","ű","Ő","Ű","Í");
    		
			$html = preg_replace($mit,$mire,$html);
    		//öüóõúéáûí - ÖÜÓÚÉÁÛÍ
    		
    		$this->html_content = $html;
    		
    		//$this->realhtml_content = $this->getRealHtml();
    		
    		return 1;
    	}
		
	}	

	public function extractText($html){
		$dom = new simple_html_dom();
    	$dom->load($html, true);
		
    	return trim($dom->plaintext);
	}	
	
	public function duplicate(){
		/////1 levél adatai

		$new = ORM::factory('omm_common_letter');

		$new->name = "".$this->name."_másolat";
		$new->subject = $this->subject;
		$new->sender_name = $this->sender_name;
		$new->sender_email = $this->sender_email;
		$new->html_content = $this->html_content;
		$new->text_content = $this->text_content;
		$new->note = $this->note;
		
		$new->saveObject();

	
		return $new;
	}	

	public function deleteLetter(){
		$this->status = 'deleted';
		$this->saveObject();

	}	
	
}
?>