<?php
class Omm_list_field_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('ord' => 'asc');
	
	protected $belongs_to = array("omm_list");
	
	protected $has_many = array("omm_list_field_value","omm_list_group_condition");
	
	private $conds = "";
	

	public function getConditions($group_id, $level = "1"){
		$this->conds = ORM::factory("omm_list_group_condition")->where("omm_list_group_id",$group_id)->where("omm_list_field_id",$this->id)->where('level',$level)->orderby('ord')->find_all();
		return $this->conds;
	}

	
	public function isReferenceUnique(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		
		
		$result = $this->db->query("SELECT COUNT(*) as record_found FROM ".$tp."omm_list_fields WHERE omm_list_id=".$this->omm_list_id." AND reference='".$this->reference."' ");

		
		return $result[0]->record_found;
	}
	
	public function getValues(){
		return $this->omm_list_field_value;
	}

	public function getValueFromCode($code){
		$result = $this->db->from('omm_list_field_values')->where('omm_list_field_id' , $this->id)->where('code', $code)->get();
		
		if(sizeof($result)>0){
			return $result[0]->value;
		}else{
			return '-';
		}
	}

	public function getCodeFromValue($value){
		$result = $this->db->from('omm_list_field_values')->where('omm_list_field_id' , $this->id)->where('value', $value)->get();
		
		if(sizeof($result)>0){
			return $result[0]->code;
		}else{
			return '-';
		}
	}	
	
	public function type(){
		$result = meta::getCodeDictRow(2,$this->type);
		return $result[0]->value;
	}

	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}	
	
	public function getOrder(){
		$tablename = Kohana::config("database.default");
		$table_prefix = $tablename['table_prefix'];		
		
		$result = $this->db->query("SELECT MAX(ord) as max_ord FROM ".$table_prefix."omm_list_fields WHERE omm_list_id=".$this->omm_list_id." ");
		
		return $result[0]->max_ord+1;
	}
	
	public function deleteOptions(){
		$tablename = Kohana::config("database.default");
		$table_prefix = $tablename['table_prefix'];		
		
		$this->db->query("DELETE FROM ".$table_prefix."omm_list_field_values WHERE omm_list_field_id=".$this->id." ");
	}
	
	/**
	 * Ha már van hozzá adat akkor nem törölhető
	 * Enter description here...
	 * @return unknown_type
	 */
	public function isReadOnly(){
		
		
	}
	
}
?>