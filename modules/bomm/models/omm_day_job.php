<?php
require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

class Omm_day_job_Model extends ORM {

	protected $db = "own";

	public $ABSOLUTE_TYPE = 'absolute';
	public $RELATIVE_TYPE = 'relative';
	public $SENDNOW_TYPE = 'sendnow';

	protected $has_many = array("omm_job","omm_day_job_detail");

	protected $sorting = array('created_time' => 'desc');

	private $tp = "";


	private function addDetail($type,$value){
		if($this->tp == ""){
			$tablename = Kohana::config("database.default");
			$this->tp = $tablename['table_prefix'];
		}

		$this->db->query("INSERT INTO ".$this->tp."omm_day_job_details SET omm_day_job_id= ".$this->id.", type='".$type."', value ='".$value."' ");
	}

	public function createAbsHourly($day,$hour){
		$dayjob = ORM::factory('omm_day_job')->where("day",$day)->where('type','absolute')->where('hour',$hour)->find();
			
		if(!$dayjob->loaded){
			$dayjob->type = 'absolute';
			$dayjob->status = '_created';
			$dayjob->day = $day;
			$dayjob->hour = $hour;
			$dayjob->created_time = date('Y-m-d H:i:s');
			$dayjob->save();
			return $dayjob;
		}else{
			return $dayjob;
		}
	}

	public function createAbsDaily($day){
		$dayjob = ORM::factory('omm_day_job')->where("day",$day)->where('type','absolute')->where('hour IS NULL')->find();
			
		if(!$dayjob->loaded){
			$dayjob->type = 'absolute';
			$dayjob->status = '_created';
			$dayjob->day = $day;
			//$dayjob->hour = $hour;
			$dayjob->created_time = date('Y-m-d H:i:s');
			$dayjob->save();
			return $dayjob;
		}else{
			return $dayjob;
		}
	}

	public function createRelDaily($day){
		$dayjob = ORM::factory('omm_day_job')->where("day",$day)->where('type','relative')->where('hour IS NULL')->find();
			
		if(!$dayjob->loaded){
			$dayjob->type = 'relative';
			$dayjob->status = '_created';
			$dayjob->day = $day;
			//$dayjob->hour = $hour;
			$dayjob->created_time = date('Y-m-d H:i:s');
			$dayjob->save();
			return $dayjob;
		}else{
			return $dayjob;
		}
	}

	public function create($type, $job_id = 0, $letter_id = 0){
		$status = 'created';

		$day = date('Y-m-d');
		$hour = date('H');
			
		$dayjob = ORM::factory('omm_day_job')->where("day",$day)->where("type",$type)->where("hour",$hour)->find();
			
		if(!$dayjob->loaded){
			$this->type = $type;
			$this->status = '_created';
			$this->day = $day;
			$this->hour = $hour;
			$this->created_time = date('Y-m-d H:i:s');
			$this->save();
			return $this;
		}else{
			return $dayjob;
		}
			


	}


	public function createJobs($limit,$letter_id = 0){
		switch($this->type){

			case $this->ABSOLUTE_TYPE:
				return $this->absoluteCreate($limit);
				break;
			case $this->RELATIVE_TYPE:
				return $this->relativeCreate($limit);
				break;
			case $this->SENDNOW_TYPE:
				return $this->sendnowCreate($limit, $letter_id);
				break;
		}

	}

	public function prepareJobs($limit, $km_user = ""){
		return $this->prepare($limit,$km_user);
	}

	public function sendJobs($limit, $km_user = ""){
		return $this->send($limit,$km_user);
	}

	private function send($limit, $km_user = ""){

		//jobs::runLostSql($this->db); /////biztonsági log fájlok sqleinek futtatása (ha volt kiküldés ami nem került db-be, lásd jobs helper)

		if($this->tp == ""){
			$tablename = Kohana::config("database.default");
			$this->tp = $tablename['table_prefix'];
		}

		$today_jobs_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		count_records();

		$today_sent_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		in('status',array('sent','error'))->
		count_records();

		$breaklimit = $limit;
		$i = 0;
		$limit_szamolo = 0;
		$all = 0;


		//		$job_ids = $this->db->select('omm_jobs.id as id')->
		//								from('omm_jobs')->
		//								where("omm_day_job_id",$this->id)->
		//								where('status','prepared')->
		//								get();

		$sql = "SELECT id,omm_letter_id FROM ".$this->tp."omm_jobs
				 WHERE omm_day_job_id = ".$this->id."
				   AND status = 'prepared' ORDER BY omm_letter_id";

		$jobs = $this->db->query($sql);


		$jobs_html = array();
		$jobs_text = array();

		$last_letter_id = 0;

		
		$swift = jobs::getSwift(true,false,"",$km_user);

		foreach ($jobs as $j){

			if($i >= $breaklimit || $limit_szamolo >= $breaklimit){

				if(sizeof($jobs_html) >0){
					$count = jobs::sendJobs($jobs_html,'html',$swift, $this->db, true, $km_user);
					$i = $i + $count;
				}

				if(sizeof($jobs_text)>0){
					$count = jobs::sendJobs($jobs_text,'text',$swift, $this->db, true,  $km_user);
					$i = $i + $count;
				}

				$jobs_html = array();
				$jobs_text = array();

				break;
			}

			if($last_letter_id != 0 && $last_letter_id != $j->omm_letter_id){

				if(sizeof($jobs_html) >0){
					$count = jobs::sendJobs($jobs_html,'html',$swift, $this->db, true,  $km_user);
					$i = $i + $count;
				}

				if(sizeof($jobs_text)>0){
					$count = jobs::sendJobs($jobs_text,'text',$swift, $this->db, true,  $km_user);
					$i = $i + $count;
				}
					
				$jobs_html = array();
				$jobs_text = array();

			}


			$jo = ORM::factory('omm_job')->find($j->id);

			if($jo->email_type == 'html'){
				$jobs_html[] = $jo;
			}elseif($jo->email_type == 'text'){
				$jobs_text[] = $jo;
			}

			$limit_szamolo++;
			$last_letter_id = $j->omm_letter_id;
			
			
			//echo $jo->id."<br/>";

		}///////foreach jobs

		if(sizeof($jobs_html) >0){
			$count = jobs::sendJobs($jobs_html,'html',$swift, $this->db, true,  $km_user);
			$i = $i + $count;
		}

		if(sizeof($jobs_text)>0){
			$count = jobs::sendJobs($jobs_text,'text',$swift, $this->db, true,  $km_user);
			$i = $i + $count;
		}


		$swift->disconnect();

		$now_sent = $i;

		$ret = array($this->type => array(
				     'today_jobs_all' => $today_jobs_all, 
				     'today_sent'=> $today_sent_all, 
				     'now_sent' => $now_sent));

		$ret[$this->type]['status'] = 'WORKING';

		$today_sent_all = $today_sent_all+$i;

		if($today_jobs_all <= $today_sent_all){
			$this->status = 'sent';
			$this->sent_time = date('Y-m-d H:i:s');
			$this->save();
			$ret[$this->type]['status'] = 'READY';
		}

		return $ret;
	}

	private function prepare($breaklimit, $km_user = ""){
		$today_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		count_records();

		$prepared_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		in('status',array('sent','error','prepared'))->
		count_records();

		$i = 0;
		$all = 0;
			
		$job_ids = $this->db->select('omm_jobs.id as id')->
		from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		where('status','created')->
		get();

		$all = sizeof($job_ids);

		KOHANA::log('debug',"Before cycle memory: ".memory_get_usage());

		$dom = new simple_html_dom();
		foreach($job_ids as $ji){
			if($i>=$breaklimit) break;
			$j = ORM::factory('omm_job')->find($ji->id);

			$j->prepare($dom,$km_user);

			KOHANA::log('debug',"After $i memory: ".memory_get_usage());
			unset($j);
			KOHANA::log('debug',"After $i unset memory: ".memory_get_usage());
			$i++;
			Kohana::log_save();
		}
		unset($dom);

		KOHANA::log('debug',"After cycle memory: ".memory_get_usage());

		$ret = array( $this->type => array(
					  'today_all' => $today_all,
		 			  'today_prepared' => $prepared_all,
					  'now_selected' => $all, 
					  'now_prepared' => $i));
		$ret[$this->type]['status'] = 'WORKING';

		$prepared_all = $prepared_all + $i;

		if($today_all <= $prepared_all){
			$this->status = 'prepared';
			$this->prepared_time = date('Y-m-d H:i:s');
			$this->save();
			$ret[$this->type]['status'] = 'READY';
		}

		return $ret;
	}


	private function sendnowCreate($limit,$letter_id){
		$ret = array();
		$ret['create'] = array();
		$letter = ORM::factory('omm_letter')->find($letter_id);

		if($letter->loaded){


			$ret['create']['status'] = 'OK';
			$ret['create']['today_all'] = $letter->getRecipientsNumber();
			$ret['create']['today_created'] = $this->db->from('omm_jobs')->where("omm_day_job_id",$this->id)->count_records();

			$recipients = $letter->getRecipientsForJob($this->id,$letter->id);

			$ret['create']['now_selected'] = sizeof($recipients);

			$i = 0;
			$all = 0;

			foreach ($recipients as $r){
				if($i>=$limit) break;

				$job = ORM::factory('omm_job');
				$job->create('dayjob', $letter->id, null, $r->id, $this->id);
				$i++;
			}

			$ret['create']['now_created'] = $i;
			$ret['create']['status'] = 'WORKING';

			if($ret['create']['today_all'] <= $ret['create']['today_created']){
				$ret['create']['status'] = 'READY';
				$this->status = 'created';
				$this->created_time = date('Y-m-d H:i:s');
				$this->save();
			}


		}else{
			$ret['create']['status'] = 'ERROR1';
		}

		return $ret;
	}




	private function absoluteCreate($limit){
		$today_all = 0;

		if($this->hour == NULL){//////daily

			$letters = ORM::factory('omm_letter')->
			where('status','active')->
			where('timing_type','absolute')->
			where('timing_date',$this->day)->
			where('timing_hour IS NULL')->
			find_all();

		}else{//////hourly

			$letters = ORM::factory('omm_letter')->
			where('status','active')->
			where('timing_type','absolute')->
			where('timing_date',$this->day)->
			where('timing_hour',$this->hour)->
			find_all();

		}
			


			
		foreach($letters as $l){
			$today_all = $today_all + $l->getRecipientsNumber();
		}


		$created_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		count_records();
			


		$breaklimit = $limit;

		$i = 0;
		$all = 0;

		//$jobs = array();



		foreach($letters as $l){

			if($l->isSent()){
				continue;
			}

			$recipients = $l->getRecipientsForJob($this->id,$l->id);

			$html = $l->html_content;

			$all = $all + sizeof($recipients);

			foreach($recipients as $r){
				if($i>=$breaklimit) break;

				$job = ORM::factory('omm_job');
				$job->create('dayjob', $l->id, null, $r->id, $this->id);
					
				unset($job);
				$i++;
			}


			if($i>=$breaklimit) break;

			if($i>0){
				$this->addDetail('absolute_letter',$l->id);
			}

		}

		$created_all = $created_all + $i;

		$ret = array($this->type => array(
					'today_all'=>$today_all,
					'today_created'=>$created_all,
					'now_selected' => $all, 
					'now_created' => $i));		

		if($today_all <= $created_all){
			$this->status = 'created';
			$this->created_time = date('Y-m-d H:i:s');
			$this->save();
		}


		return $ret;
	}


	private function relativeCreate($limit){
		$today_all = 0;

		$letters = ORM::factory('omm_letter')->
		where('status','active')->
		where('timing_type','relative')->
		find_all();

			
		foreach($letters as $l){
			$today_all = $today_all + $l->getRecipientsNumber(TRUE, array(), array(), TRUE);
		}


		$created_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		count_records();
			


		$breaklimit = $limit;

		$i = 0;
		$all = 0;

		foreach($letters as $l){
			$recipients = $l->getRecipientsForJob($this->id,$l->id);

			$html = $l->html_content;

			$all = $all + sizeof($recipients);

			foreach($recipients as $r){
				if($i>=$breaklimit) break;

				$job = ORM::factory('omm_job');
				$job->create('dayjob', $l->id, null, $r->id, $this->id);
				$i++;
			}

			if($i>=$breaklimit) break;

			if($i>0){
				$this->addDetail('relative_letter',$l->id);
			}

		}

		$created_all = $created_all + $i;


		$ret = array($this->type => array(
					'today_all'=>$today_all,
					'today_created'=>$created_all,
					'now_selected' => $all, 
					'now_created' => $i));

		if($today_all <= $created_all){
			$this->status = 'created';
			$this->created_time = date('Y-m-d H:i:s');
			$this->save();
		}


		return $ret;
	}


	public function jobsForStat($stat = array('sent')){
		return $today_sent_all = $this->db->from('omm_jobs')->
		where("omm_day_job_id",$this->id)->
		in('status',$stat)->
		count_records();
	}


	public function createSystemMessages($user){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT DISTINCT (omm_letter_id) FROM ".$tp."omm_jobs WHERE omm_day_job_id=".$this->id."  ";

		$res = $this->db->query($sql);

		if($this->type == "absolute"){////absoulute dayjob
			foreach($res as $r){
				$letter = ORM::factory('omm_letter')->find($r->omm_letter_id);
				meta::createLetterSystemMessage($letter);
			}
		}else{
				
			if(sizeof($res) > 0){
				$preview = "A mai napon kiküldött relatív levelek:";

				$body = "";

				foreach($res as $r){
					$letter = ORM::factory('omm_letter')->find($r->omm_letter_id);
						
					$mm = $letter->getSystemMessage();
						
					$body .=$mm['preview']."<br/>";
					$body .=$mm['body']."<br/>";
						
				}
					
				$recipient = $user;
				
				meta::createMessage($recipient,"system","Relatív levelek",$preview,$body);
					
			}
				
		}


	}

}
?>