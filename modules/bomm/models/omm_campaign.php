<?php
class Omm_campaign_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('name' => 'asc');
	
	//protected $sorting = array('created_date' => 'desc');	
	
	protected $has_many = array("omm_letter");
	
	protected $belongs_to = array('omm_client');
	
	public function created(){
		return dateutils::formatDate($this->created_date);
	}	
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}
	
	public function letterNum(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status!=','deleted')->count_records();	
	}

	public function sentLetterNum(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
				
		$sql = "SELECT l.id,l.name,l.omm_campaign_id,j.id, j.day, j.hour FROM ".$tp."omm_day_job_details d, ".$tp."omm_day_jobs j, ".$tp."omm_letters l  
						 WHERE j.status = 'sent' 
						   AND j.id=d.omm_day_job_id 
						   AND d.type = 'absolute_letter' 
						   AND d.value = l.id
						   AND l.omm_campaign_id = ".$this->id."
						   ORDER BY j.day DESC
						   ";

		
		$res = $this->db->query($sql);
		
		$rel = $this->relative();
		
		return sizeof($res)+$rel;
	}

	public function draftLetterNum(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status','draft')->count_records();	
	}

	public function readyLetterNum(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status','active')->count_records();	
	}
	
	public function drafts($sort = "name", $order = "asc"){
		
		if($sort == "time"){
			return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->where('status','draft')->orderby("timing_date",$order)->orderby("timing_hour",$order)->orderby("timing_event_value",$order)->orderby("name","asc")->find_all();	
		}else{
			return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->where('status','draft')->orderby($sort,$order)->find_all();
		}
		
		
	}
	
	public function relativeLetters($sort = "name", $order = "asc"){
		
		if($sort == "time"){
			return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->where('status','active')->where('timing_type','relative')->orderby("timing_event_value",$order)->orderby("name","asc")->find_all();	
		}else{
			return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->where('status','active')->where('timing_type','relative')->orderby($sort,$order)->find_all();
		}
		
		
	}
	
	public function relative(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','relative')->count_records();	
	}

	public function absoluteLetters($sort = "name", $order = "asc"){////abszolúlt
		
		if($sort == "time"){
			return ORM::factory("omm_letter")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','absolute')->orderby("timing_date",$order)->orderby("timing_hour",$order)->orderby("name","asc")->find_all();	
		}else{
			return ORM::factory("omm_letter")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','absolute')->orderby($sort,$order)->find_all();
		}
		
		

		
	}	
	
	public function absolute(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','absolute')->count_records();	
	}

	public function sendnow(){
		return $this->db->from("omm_letters")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','sendnow')->count_records();	
	}	

	public function sendnowLetters($sort = "name", $order = "asc"){////azonnalu
		return ORM::factory("omm_letter")->where("omm_campaign_id",$this->id)->where('status','active')->where('timing_type','sendnow')->orderby($sort,$order)->find_all();	
	}		
	
	public function letters(){
		return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->where('status','active')->find_all();
	}	

	public function allLetters(){
		return ORM::factory("omm_letter")->where('omm_campaign_id',$this->id)->find_all();
	}		
	
	public function deleteCampaign(){
		
		foreach ($this->allLetters() as $letter){
			$letter->deleteLetter();
		}
		
		
		$this->status = 'deleted';
		$this->saveObject();
	}
	
}
?>