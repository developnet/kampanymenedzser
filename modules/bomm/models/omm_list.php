<?php
class Omm_list_Model extends ORM {
	
	protected $db = "own";
	
	protected $has_many = array(
							"omm_list_field",
							"omm_list_group",
							"omm_list_member",
							"omm_list_form",
							"omm_list_connection",
							"omm_list_notification"
							);
	
	protected $belongs_to = array("omm_client");
	
							
	protected $has_one = array(
							'verifycation_email' 	 => 'omm_common_letter', 
							'subscribe_conf_email' 	 => 'omm_common_letter',
							'unsubscribe_conf_email' => 'omm_common_letter', 
							'datamod_conf_email' 	 => 'omm_common_letter'
							);
	
	private $notifmail = array();						
							
	public function getDataModForm(){
		
		if(is_null($this->datamod_form_id)){
			return null;
		}else{
			return ORM::factory("omm_list_form")->find($this->datamod_form_id);
		}
		
	}
	
	public function getNotifMail($type){
		
		if(!isset($this->notifmail[$type])){
			
			$notif = ORM::factory("omm_list_notification")->where("type",$type)->where("omm_list_id",$this->id)->find();
			
			if($notif->loaded){
				$this->notifmail[$type] = $notif->value; 
			}else{
				$this->notifmail[$type] = "";
			}
						
		}
		
		return $this->notifmail[$type];
	}							
							
	public function clearNotifications(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		$sql = "DELETE FROM ".$tp."omm_list_notifications WHERE omm_list_id=".$this->id." ";
		
		$this->db->query($sql);					
	}	

	
	public function notifications(){
		return $this->omm_list_notification;						
	}	
							
	public function checkUnsubProcess(){
  		
  		
  		if($this->unsubscribe_landing_page == NULL) return false;
	
  		
	
  		if($this->second_unsubscribe_page == NULL) return false;
	
  		
	
  		if($this->error_unsubscribe_page == NULL) return false;
			
		return true;
	}

	
	public function checkSubProcess(){
		
		if($this->initial_conf_page == NULL) return false;
		
		if($this->type == "double" && $this->second_conf_page == NULL) return false;
		
		if($this->second_subscribe_page == NULL) return false;
		
		if($this->error_subscribe_page == NULL) return false;
	
		return true;
	}
	
	public function deleteList(){
//		$tablename = Kohana::config("database.default");
//		$tp = $tablename['table_prefix'];
//		
//		/////connections	
//		$data = $this->db->where('omm_list_id', $this->id)->delete('omm_list_connections');
//		/////connections
//		
//		//fields
//		$sql_ids = "SELECT id FROM ".$tp."omm_list_fields WHERE omm_list_id = ".$this->id."";
//		$ids = $this->db->query($sql_ids);
//		
//		$idins = array();
//		foreach ($ids as $id){
//			$idins[] = $id->id;
//		}
//		
//		if(sizeof($idins)>0)		
//			$data = $this->db->in('omm_list_field_id', $idins)->delete('omm_list_field_values');
//		
//		$data = $this->db->where('omm_list_id', $this->id)->delete('omm_list_fields');
//		//fields
//		
//		
//		////forms
//		$sql_ids = "SELECT id FROM ".$tp."omm_list_forms WHERE omm_list_id = ".$this->id."";
//		$ids = $this->db->query($sql_ids);
//		
//		$idins = array();
//		foreach ($ids as $id){
//			$idins[] = $id->id;
//		}
//		
//		if(sizeof($idins)>0)
//			$data = $this->db->in('omm_list_form_id', $idins)->delete('omm_list_form_fields');
//		
//		$data = $this->db->where('omm_list_id', $this->id)->delete('omm_list_forms');
//		////forms
//
//		////groups
//		$sql_ids = "SELECT id FROM ".$tp."omm_list_forms WHERE omm_list_id = ".$this->id."";
//		$ids = $this->db->query($sql_ids);
//		
//		$idins = array();
//		foreach ($ids as $id){
//			$idins[] = $id->id;
//		}		
//		
//		if(sizeof($idins)>0)
//			$data = $this->db->where('omm_list_group_id', $this->id)->delete('omm_list_group_conditions');
//		
//		$data = $this->db->where('omm_list_id', $this->id)->delete('omm_list_groups');
//		////groups
//		
//		/////members
//		$this->deleteAll();
//		
//		try{
//			$sql = "DELETE FROM ".$tp."omm_common_letters WHERE id = ".$this->verifycation_email_id." ";
//			$this->db->query($sql);		
//			$sql = "DELETE FROM ".$tp."omm_common_letter_link_redirections WHERE omm_letter_id = ".$this->verifycation_email_id." ";
//			$this->db->query($sql);					
//			$sql = "DELETE FROM ".$tp."omm_common_letter_open_log WHERE omm_letter_id = ".$this->verifycation_email_id." ";
//			$this->db->query($sql);		
//		}catch (Exception $e){
//			
//		}
//			
//		try{			
//			$sql = "DELETE FROM ".$tp."omm_common_letters WHERE id = ".$this->subscribe_conf_email_id." ";
//			$this->db->query($sql);
//			$sql = "DELETE FROM ".$tp."omm_common_letter_link_redirections WHERE omm_letter_id = ".$this->subscribe_conf_email_id." ";
//			$this->db->query($sql);					
//			$sql = "DELETE FROM ".$tp."omm_common_letter_open_log WHERE omm_letter_id = ".$this->subscribe_conf_email_id." ";
//			$this->db->query($sql);		
//		}catch (Exception $e){
//			
//		}
//			
//		try{			
//			$sql = "DELETE FROM ".$tp."omm_common_letters WHERE id = ".$this->datamod_conf_email_id." ";
//			$this->db->query($sql);			
//			$sql = "DELETE FROM ".$tp."omm_common_letter_link_redirections WHERE omm_letter_id = ".$this->datamod_conf_email_id." ";
//			$this->db->query($sql);					
//			$sql = "DELETE FROM ".$tp."omm_common_letter_open_log WHERE omm_letter_id = ".$this->datamod_conf_email_id." ";
//			$this->db->query($sql);					
//		}catch (Exception $e){
//			
//		}
			
		
		////self
		$this->status = "deleted";
		$this->saveObject();
		
	}
	public function deleteAll(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		//$return = array();
		
		/*
		$sql = "DELETE FROM ".$tp."omm_list_connections WHERE omm_list_id=".$this->id." ";
		$this->db->query($sql);		
		
		
		
		$sql_ids = "SELECT id FROM ".$tp."omm_list_members WHERE omm_list_id = ".$this->id."";
		$ids = $this->db->query($sql_ids);
		
		$idins = array();
		foreach ($ids as $id){
			$idins[] = $id->id;
		}
		
		if(sizeof($idins)>0){
			
//			$data = $this->db->in('omm_list_member_id', $idins)->delete('omm_letter_link_clicks');
//			$data = $this->db->in('omm_list_member_id', $idins)->delete('omm_common_letter_link_clicks');
	
//			$data = $this->db->in('omm_list_member_id', $idins)->delete('omm_letter_open_log');
//			$data = $this->db->in('omm_list_member_id', $idins)->delete('omm_common_letter_open_log');

			
			$data = $this->db->in('omm_list_member_id', $idins)->delete('omm_list_member_datas');
			$data = $this->db->in('id', $idins)->delete('omm_list_members');			
			
			return count($data);
		}else{
			return 0;
		}

		*/
		
	}

	public function deleteConnections($type){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		
		$sql = "DELETE FROM ".$tp."omm_list_connections WHERE omm_list_id=".$this->id." AND type='".$type."' ";
		
		$this->db->query($sql);
	}
	
	public function connections($type){
		return ORM::factory("omm_list_connection")->where("type",$type)->where("omm_list_id",$this->id)->find_all();
	}

	public function connectedLists($type){
		
		if(($type == "subscribe" && $this->subscribe_connection_type == "all_lists") || 
			($type == "unsubscribe" && $this->unsubscribe_connection_type == "all_lists")
		){
			
			return ORM::factory('omm_list')->notin('id', $this->id)->find_all();
			
		}else{
			
			$res = $this->db->select("omm_list_connections.other_list_id as id")->from("omm_list_connections")->where("omm_list_id",$this->id)->where("type",$type)->get();
		
			$ids = array();
			
			foreach($res as $r){
				$ids[] = $r->id;
			}
	
			if(sizeof($ids)>0){
				return ORM::factory('omm_list')->in('id', $ids)->find_all();	
			}else{
				return array();
			}			
			
		}

	}	
	
	public function activateMembersByEmail($email){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		$this->db->query("UPDATE ".$tp."omm_list_members SET status='active', activation_date = NOW() WHERE omm_list_id=".$this->id." AND email='".$email."' AND status='prereg' LIMIT 1");
	}
	
	public function unsubscribeMembersByEmail($email){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];	
		$this->db->query("UPDATE ".$tp."omm_list_members SET status='unsubscribed', unsubscribe_date = NOW() WHERE omm_list_id=".$this->id." AND email='".$email."' LIMIT 1");		
	}
	
	public function getAllFields(){////
		return ORM::factory('omm_list_field')->in('omm_list_id',array(0,$this->id))->find_all();
	}		
	
	
	public function fields($globalfields = false){
		if($globalfields){
			
			return ORM::factory('omm_list_field')->where('omm_list_id', 0)->find_all();
			
		}else{
			return $this->omm_list_field;	
		}
								
	}							

	
	
	public function gridFields($globalfields = false){
		
		if($globalfields){
			
			return ORM::factory('omm_list_field')->where('omm_list_id', 0)->where("grid",1)->find_all();
			
		}else{
			return ORM::factory('omm_list_field')->where('omm_list_id', $this->id)->where("grid",1)->find_all();	
		}		
		
		
	}
	
	public function groups(){
		return $this->omm_list_group;						
	}		

	public function groupNumber(){
		return $this->omm_list_group->count();	
	}
	
	public function forms(){
		return $this->omm_list_form;						
	}		

	//Deprecated
	public function members($memberStatus,$orderby,$order){
		
		$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
		$members = ORM::factory("omm_list_member")->where("omm_list_id",$this->id)->where("status",$memberStatus)->find_all();

		$mo = array();
		
		foreach ($members as $m){
			
			$mo[] = $m->getDataObject($fields);
			
		}
		
		/*arrayutils::sort($mo, $orderby,$order);
		
		$page = array();
		
		$rowperpage = 10;
		$offset = 0;
		
		if(sizeof($mo)>$rowperpage){
			$pages = array_chunk($mo, $rowperpage);
			
			if($offset!=0){
				$pageindex = $offset/$rowperpage;
			}else{
				$pageindex = 0;
			}

			$page = $pages[$pageindex];
		}else{
			$page = $mo;
		}*/
		
		return $mo;		
		
		
	}

	public function getNameField(){
		
		$fullname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',$this->id)->where('type', 'fullname')->count_all();
		
		if($fullname){
			$fullname = ORM::factory('omm_list_field')->where('omm_list_id',$this->id)->where('type', 'fullname')->find();
			return $fullname->reference;
		}else{
			
			$firstname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',$this->id)->where('type', 'firstname')->count_all();
			$lastname = (bool) ORM::factory('omm_list_field')->where('omm_list_id',$this->id)->where('type', 'lastname')->count_all();
			
			if($firstname && $lastname){
				return "bothnames";
			}elseif($firstname && !$lastname){
				return $this->searchReferenceByType("firstname");
			}elseif(!$firstname && $lastname){
				return $this->searchReferenceByType("firstname");
			}else{
				return $this->searchReferenceByType("email");
			}
		}
	}
	
	public function searchReferenceByType($type){
		$fullname = ORM::factory('omm_list_field')->where('omm_list_id',$this->id)->where('type', $type)->find();
		return $fullname->reference;
	}

	public function getMembersForListTable($memberStatus,$orderby,$order,$offset,$rowperpage,$showing = "all", $param = "all",$searchfield = "none", $searchkeyword = "none"){
		
		$stat = $this->getMemberStat($showing,$param);
		
		$from = $stat['from'];
		$to = $stat['to'];
		
		$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		$sql .= " c.unsubscribe_date as unsubscribe_date,";
		foreach ($fields as $f) {
			if($f->grid == 1){
				$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";	
			}
		}
		
		if($memberStatus == "unsubscribed"){
			$sql .= " c.unsubscribe_date as reg_date ";	
		}else{
			$sql .= " c.reg_date as reg_date ";
		}
		
		
		$sql .= " FROM   ".$tp."omm_list_member_connect c, ".$tp."omm_list_members m "; //

		if($searchfield != "none"){
			
			foreach ($fields as $f) {
				$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (m.id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
			}	
			
		}		
		
		$sql .= " WHERE c.omm_list_id=".$this->id." AND c.omm_list_member_id = m.id ";
		
		if($showing != "all"){
			if($memberStatus == "active"){
				$sql .= " AND c.reg_date BETWEEN '".$from."' AND '".$to."'";		
			}elseif($memberStatus == "unsubscribed"){
				$sql .= " AND c.unsubscribe_date BETWEEN '".$from."' AND '".$to."' ";
			}
		}else{
			$sql .= " AND m.status='".$memberStatus."'";//
		}
		
		if($searchfield != "none" && $searchkeyword != "none" &&  $searchkeyword != ""  &&  $searchfield != "email"  &&  $searchfield != "regdatum"  ){
			 $sql .= "AND IFNULL(".$searchfield.".value, ".$searchfield.".value_text) LIKE '%".$searchkeyword."%' ";
		}elseif($searchfield == "email"){
			$sql .= "AND m.email LIKE '%".$searchkeyword."%' ";
		}elseif($searchfield == "regdatum"){
			$sql .= "AND c.activation_date LIKE '%".$searchkeyword."%' ";
		}		
		
		$sql .= " order by $orderby $order ";
		
		if($searchfield != "none"){
			//$sql .= " LIMIT 200";	
		}else{
			$sql .= " LIMIT $offset,$rowperpage";
		}
		
		
		
		//echo $sql;
		
		return $this->db->query($sql);
	}	
	
	public function getDataSelect($memberStatus,$orderby,$order,$offset,$rowperpage,$showing = "all", $param = "all"){
		
		$stat = $this->getMemberStat($showing,$param);
		
		$from = $stat['from'];
		$to = $stat['to'];
		
		$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
		}
		$sql .= " IFNULL(m.activation_date, m.reg_date) as reg_date ";
		$sql .= " from  ".$tp."omm_list_members m,  ".$tp."omm_list_member_connect c";
		$sql .= " where c.omm_list_id=".$this->id." AND m.status='".$memberStatus."' AND c.omm_list_member_id = m.id";
		$sql .= " AND c.reg_date BETWEEN '".$from."' AND '".$to."' ";
		$sql .= " order by $orderby $order ";
		$sql .= " LIMIT $offset,$rowperpage";
		//echo $sql;
		
		return $this->db->query($sql);
	}

	
	public function getMembersForImportByEmail($email = "",$fields = NULL){
		
		if($fields == NULL)
			$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
			
			
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.code as code,";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
		}
		$sql .= " IFNULL(m.activation_date, m.reg_date) as regdatum, ";
		$sql .= " m.status as statusz, ";
		$sql .= " m.mod_date as mod_date ";
		$sql .= " FROM   ".$tp."omm_list_members m,  ".$tp."omm_list_member_connect c";
		$sql .= " WHERE c.omm_list_member_id = m.id AND c.omm_list_id=".$this->id." ";
		
		if($email != ""){
			$sql .= " AND m.email = '".$email."' ";	
		}
		
		$sql .= " order by m.reg_date";
		//echo $sql;
		
		return $this->db->query($sql);
	}	
	
	
	public function getMembersForImport($from = "", $to = "",$fields = null){
		
		if($fields == null)
			$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		foreach ($fields as $f) {
			$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.omm_list_field_id=".$f->id." LIMIT 1) as ".$f->reference.", ";
		}
		$sql .= " IFNULL(m.activation_date, m.reg_date) as regdatum, ";
		$sql .= " c.reg_date as list_reg_date, ";
		$sql .= " m.status as statusz, ";
		$sql .= " m.mod_date as mod_date ";
		$sql .= " FROM   ".$tp."omm_list_members m,  ".$tp."omm_list_member_connect c";
		$sql .= " WHERE c.omm_list_member_id = m.id AND c.omm_list_id=".$this->id." ";
		
		if($from != ""){
			$sql .= " AND IFNULL(m.mod_date, m.reg_date) >= '".$from."' ";	
		}

		if($to != ""){
			$sql .= " AND IFNULL(m.mod_date, m.reg_date) <= '".$to."' ";	
		}		
		
		$sql .= " order by m.reg_date";
		//echo $sql;
		
		return $this->db->query($sql);
	}	
	
	
	public function addTestMember(){
		$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		
		$code1 = string::random_string('unique');
		$email = string::random_string()."@".string::random_string().".hu";
		$reg_date =  date("Y-m-d H:i:s"); 

		$member = ORM::factory("omm_list_member");
		
		$member->email = $email;
		$member->reg_date = $reg_date;
		$member->activation_code = $code1;
		$member->status = "active";
		$member->omm_list_id = $this->id;
		
		$member->save();
		
		
		foreach($fields as $f){
			
			$data = ORM::factory("omm_list_member_data");
			$data->omm_list_field_id = $f->id;
			$data->omm_list_member_id = $member->id;
			$data->field_reference = $f->reference;
			$data->value = string::random_string();
			$data->save();
		}
	}
	
	public function getMemberStat($showing,$param){
		$stat = array();
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		
		if($showing == "month"){
			$from = $param."-01 00:00:00"; 
			$to = date( "Y-m-d", strtotime( $from." +1 month" ));				
			$to = date( "Y-m-d", strtotime( $to." -1 day" ))." 23:59:59";		
			

		}elseif($showing == "year"){	

			$from = $param."-01-01 00:00:00"; 
			$to = date( "Y-m-d", strtotime( $from." +12 month" ));				
			$to = date( "Y-m-d", strtotime( $to." -1 day" ))." 23:59:59";			
			
		}elseif($showing == "all"){
			$from = "1000-01-01 00:00:00"; 
			$to = "9999-12-31 23:59:59";
			
		}elseif($showing == "week"){
			$from = $param." 00:00:00"; 
			$to = $to = date( "Y-m-d", strtotime( $from." +7 day" ))." 23:59:59";
			
		}elseif($showing == "day"){
			$from = $param." 00:00:00"; 
			$to = $param." 23:59:59";
			
		}

			$stat['from'] = $from;
			$stat['to'] = $to;		
		
			$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE reg_date BETWEEN '".$from."' AND '".$to."' AND omm_list_id=".$this->id." ";
			$subscribed = $this->db->query($sql);
			$stat['active'] = $subscribed[0]->ossz;		
	
			$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE unsubscribe_date IS NOT NULL AND unsubscribe_date BETWEEN '".$from."' AND '".$to."' AND omm_list_id=".$this->id." ";
			$unsubscribed = $this->db->query($sql);
			$stat['unsubscribed'] = $unsubscribed[0]->ossz;		
		
		
		$stat['deleted'] = 0;
		
		return $stat;
	}
	
	public function membersNumber($memberStatus = "active"){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
				
		$sql = "SELECT count(c.id) as ossz FROM ".$tp."omm_list_member_connect c, ".$tp."omm_list_members m  WHERE  m.id = c.omm_list_member_id AND m.status='".$memberStatus."' AND c.omm_list_id=".$this->id." and m.omm_client_id =".$this->omm_client->id."  ";
		$count = $this->db->query($sql);

		return $count[0]->ossz;
	}
	
	
	public function created(){
		return dateutils::formatDate($this->created_date);
	}
	
	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}

	public function testCondition($letterId,$sign){
		
		$res = $this->db->select("omm_letter_recip_conditions.allinlist as all")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$letterId)->where("sign",$sign)->where('value',$this->id)->get();
		
		if(sizeof($res)>0){
			return $res[0]->all;
		}
		
	}

	public function _subscribe($email){

		if(!$this->_unique_email($email)){
				
			$member = ORM::factory("omm_list_member");
			
			$member->omm_list_id = $this->id;
			$member->manual = 0;
			$member->reg_date = date("Y-m-d H:i:s");

			$member->status = 'active';

			$member->email = $post->email;
			$member->activation_date = date("Y-m-d H:i:s");
			$member->unsubscribe_date = null;

			$member->save();			
		}		
	
	}
	
	public function _unique_email($email){
		////van-e ilyen aktív user?
		return  (bool) ORM::factory('omm_list_member')->where('omm_list_id',$this->id)->where('email',$email)->where('status','active')->count_all();
	}	
	
}


?>