<?php
class Omm_list_group_condition_Model extends ORM {
	
	protected $db = "own";
	
	protected $belongs_to = array("group" => "omm_list_group");
	
	
	public function saveObject(){
		
		$tablename = Kohana::config("database.default");
		$table_prefix = $tablename['table_prefix'];		
		
		$result = $this->db->query("SELECT MAX(ord) as max_ord FROM ".$table_prefix."omm_list_group_conditions WHERE omm_list_group_id=".$this->omm_list_group_id." AND omm_list_field_id=".$this->omm_list_field_id." ");
		
		$this->ord = $result[0]->max_ord+1;
		
		$this->save();
	}
}
?>