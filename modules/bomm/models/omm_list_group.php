<?php
class Omm_list_group_Model extends ORM {

	protected $db = "own";

	protected $sorting = array('name' => 'asc');

	protected $belongs_to = array("omm_list");
	
	protected $has_many = array("omm_list_group_condition");

	public function members(){
		return 0;
	}

	public function conditions(){
		return $this->omm_list_group_condition;
	}

	public function getFixedConditions($field, $level = 1){
		return ORM::factory("omm_list_group_condition")->where("field_reference",$field)->where("omm_list_group_id",$this->id)->where('level', $level)->find_all();
	}


	public function saveObject(){
		$this->mod_user_id = $_SESSION['auth_user']->id;
		$this->save();
	}

	public function deleteGroup(){

		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "DELETE FROM ".$tp."omm_list_group_conditions WHERE omm_list_group_id = ".$this->id." " ;

		$this->db->query($sql);

		$this->delete();

	}

	public function getMemberCount($fields,$memberStatus = "active", $listfilter = "all" ,$tagfilter = "all", $productfilter = "all",$searchfield = "none", $searchkeyword = "none", $searchfields = array()){
		
		
		if(sizeof($this->conditions()) == 0) return 0;

		//$fields = ORM::factory("omm_list_field")->where("omm_list_id",$this->id)->find_all();
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = " SELECT COUNT(*) as records_found ";

		$sql .= " FROM ".$tp."omm_list_member_connect list, ".$tp."omm_list_members m";
				

		foreach ($fields as $f) {
			$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (m.id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
		}

		$sql .= " WHERE  (list.omm_list_id=".$this->omm_list_id." and m.status='".$memberStatus."' and list.omm_list_member_id = m.id) AND ";
		


		$condlevels = $this->getConditionLevels();
		foreach($condlevels as $lev){
			$level = $lev->level;
			$emailconds = $this->getFixedConditions("email",$level);
			$regdateconds = $this->getFixedConditions("reg_date",$level);
			$fieldcondCount = $this->getFieldConditionNumberOnLevel($level);

			foreach ($fields as $f){
				$conditions = $f->getConditions($this->id,$level);
				if(sizeof($conditions)>0){
					$sql .= " ( ";
					foreach ($conditions as $c){

						if($c->type == "ispresent"){
							$sql .= "(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) IS NOT NULL AND IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) != '') OR ";
						}elseif($c->type == "notpresent"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) IS NULL OR IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) = '' OR ";
						}elseif($c->type == "equal"){

							if($f->type == "multiselect")
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) LIKE '%".$c->value."%' OR ";
							else
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) LIKE '".$c->value."' OR ";

						}elseif($c->type == "notequal"){

							if($f->type == "multiselect"){
								$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) NOT LIKE '%".$c->value."%' OR ";
							}else{
								$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) NOT LIKE '".$c->value."' OR ";
							}

						}elseif($c->type == "biggerthan"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) > '".$c->value."' OR ";
						}elseif($c->type == "smallerthan"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) < '".$c->value."' OR ";
						}elseif($c->type == "checkedatleast"){
							//q82ikgWE,q82ikgWE,q82ikgWE,
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) > '".$chars."' OR ";
						}elseif($c->type == "checkedless"){
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) < '".$chars."' OR ";
						}elseif($c->type == "checkedequal"){
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) = '".$chars."' OR ";
						}
		     
		     
					}
					$sql .= " (1=2) ) AND ";

				}
			}
			$sql .= " (1=1) AND ";
				
			if(sizeof($emailconds)>0){
				$sql .= " ( ";
				foreach ($emailconds as $c){

					if($c->type == "ispresent"){
						$sql .= "m.email IS NOT NULL OR ";
					}elseif($c->type == "notpresent"){
						$sql .= "m.email IS NULL OR ";
					}elseif($c->type == "equal"){
						$sql .= "m.email LIKE '".$c->value."' OR ";
					}elseif($c->type == "notequal"){
						$sql .= "m.email NOT LIKE '".$c->value."' OR ";
					}elseif($c->type == "biggerthan"){
						$sql .= "m.email > '".$c->value."' OR ";
					}elseif($c->type == "smallerthan"){
						$sql .= "m.email < '".$c->value."' OR ";
					}

				}
				$sql .= " (1=2) ) AND ";
			}
			$sql .= " (1=1) AND ";

			if(sizeof($regdateconds)>0){
				$sql .= " ( ";
				foreach ($regdateconds as $c){

					if($c->type == "ispresent"){
						$sql .= "m.activation_date IS NOT NULL OR ";
					}elseif($c->type == "notpresent"){
						$sql .= "m.activation_date IS NULL OR ";
					}elseif($c->type == "equal"){
						$sql .= "m.activation_date = '".$c->value."' OR ";
					}elseif($c->type == "notequal"){
						$sql .= "m.activation_date != '".$c->value."' OR ";
					}elseif($c->type == "biggerthan"){
						$sql .= "m.activation_date > '".$c->value."' OR ";
					}elseif($c->type == "smallerthan"){
						$sql .= "m.activation_date < '".$c->value."' OR ";
					}

				}
				 
				$sql .= " (1=2) ) AND ";
			}

			$sql .= " (1=1) AND ";


		}////LEVEL



		$sql .= " (1=1)  ";

		$sql .= Omm_client_Model::getFilterSql($listfilter,$tagfilter,$productfilter,$productfilter);
		$sql .= Omm_client_Model::getSearchSql($searchfield,$searchkeyword,$searchfields);
		
			
		$sql .= "  ";

		$result = $this->db->query($sql);

		return $result[0]->records_found;
	}

	public function getConditionLevels(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$sql = "SELECT distinct(level) as level FROM ".$tp."omm_list_group_conditions WHERE omm_list_group_id = ".$this->id." order by level asc";
		$res = $this->db->query($sql);

		return $res;
	}

	public function getFieldConditionNumberOnLevel($level){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$sql = "SELECT distinct(field_reference) FROM ".$tp."omm_list_group_conditions WHERE omm_list_group_id = ".$this->id." AND level = ".$level." AND field_reference NOT IN ('email','reg_date')  ";
		$res = $this->db->query($sql);

		$ossz = sizeof($res);

		return $ossz;
	}

	// ezt majd át kell rakni a list-het///
	public function getMembers($fields,$memberStatus = "active",$orderby = "reg_date",$order = "desc",$offset = 0,$rowperpage = 30, $limit = true, $listfilter = "all" ,$tagfilter = "all", $productfilter = "all",$searchfield = "none", $searchkeyword = "none",$searchfields = array(),$rdate_from = "", $rdate_to="",$export = false){
		if(sizeof($this->conditions()) == 0) return array();
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = " SELECT 	m.id, ";
		$sql .= " m.email, ";
		$sql .= " m.reg_date, ";
		$sql .= " m.status,"; //		

		foreach ($fields as $f) {
			
			if($f->grid || $export)
			$sql .= " IFNULL(".$f->reference.".value, ".$f->reference.".value_text) as ".$f->reference.", ";
			
			
		}

		$sql .= " list.reg_date ";
		$sql .= " FROM ".$tp."omm_list_members m,  ".$tp."omm_list_member_connect list";

		foreach ($fields as $f) {
			$sql .= " LEFT JOIN (".$tp."omm_list_member_datas ".$f->reference.") ON (list.omm_list_member_id = ".$f->reference.".omm_list_member_id AND ".$f->reference.".omm_list_field_id = ".$f->id." ) ";
		}

		$sql .= " WHERE  (list.omm_list_id=".$this->omm_list_id." and m.status='".$memberStatus."' and list.omm_list_member_id = m.id) AND ";

		$sql .= "  ";

		$condlevels = $this->getConditionLevels();
		
		foreach($condlevels as $lev){
			$level = $lev->level;
			$emailconds = $this->getFixedConditions("email",$level);
			$regdateconds = $this->getFixedConditions("reg_date",$level);
			$fieldcondCount = $this->getFieldConditionNumberOnLevel($level);

			foreach ($fields as $f){
				$conditions = $f->getConditions($this->id,$level);
				if(sizeof($conditions)>0){
					$sql .= " ( ";
					foreach ($conditions as $c){

						if($c->type == "ispresent"){
							$sql .= "(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) IS NOT NULL AND IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) != '') OR ";
						}elseif($c->type == "notpresent"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) IS NULL OR IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) = '' OR ";
						}elseif($c->type == "equal"){

							if($f->type == "multiselect")
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) LIKE '%".$c->value."%' OR ";
							else
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) LIKE '".$c->value."' OR ";

						}elseif($c->type == "notequal"){

							if($f->type == "multiselect"){
								$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) NOT LIKE '%".$c->value."%' OR ";
							}else{
								$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) NOT LIKE '".$c->value."' OR ";
							}


						}elseif($c->type == "biggerthan"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) > '".$c->value."' OR ";
						}elseif($c->type == "smallerthan"){
							$sql .= "IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text) < '".$c->value."' OR ";
						}elseif($c->type == "checkedatleast"){
							//q82ikgWE,q82ikgWE,q82ikgWE,
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) > '".$chars."' OR ";
						}elseif($c->type == "checkedless"){
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) < '".$chars."' OR ";
						}elseif($c->type == "checkedequal"){
							$chars = ($c->value*8) + $c->value;
							$sql .= "CHAR_LENGTH(IFNULL(".$c->field_reference.".value, ".$c->field_reference.".value_text)) = '".$chars."' OR ";
						}

		     
					}
					$sql .= " (1=2) ) AND ";

				}
			}
	   
			$sql .= " (1=1) AND ";
				
			if(sizeof($emailconds)>0){
				$sql .= " ( ";
				foreach ($emailconds as $c){

					if($c->type == "ispresent"){
						$sql .= "m.email IS NOT NULL OR ";
					}elseif($c->type == "notpresent"){
						$sql .= "m.email IS NULL OR ";
					}elseif($c->type == "equal"){
						$sql .= "m.email LIKE '".$c->value."' OR ";
					}elseif($c->type == "notequal"){
						$sql .= "m.email NOT LIKE '".$c->value."' OR ";
					}elseif($c->type == "biggerthan"){
						$sql .= "m.email > '".$c->value."' OR ";
					}elseif($c->type == "smallerthan"){
						$sql .= "m.email < '".$c->value."' OR ";
					}

				}
				$sql .= " (1=2) ) AND ";
			}

			$sql .= " (1=1) AND ";

			if(sizeof($regdateconds)>0){
				$sql .= " ( ";
				foreach ($regdateconds as $c){

					if($c->type == "ispresent"){
						$sql .= "m.activation_date IS NOT NULL OR ";
					}elseif($c->type == "notpresent"){
						$sql .= "m.activation_date IS NULL OR ";
					}elseif($c->type == "equal"){
						$sql .= "m.activation_date = '".$c->value."' OR ";
					}elseif($c->type == "notequal"){
						$sql .= "m.activation_date != '".$c->value."' OR ";
					}elseif($c->type == "biggerthan"){
						$sql .= "m.activation_date > '".$c->value."' OR ";
					}elseif($c->type == "smallerthan"){
						$sql .= "m.activation_date < '".$c->value."' OR ";
					}

				}
				 
				$sql .= " (1=2) ) AND ";
			}

			$sql .= " (1=1) AND ";


		}////LEVEL


		$sql .= " (1=1)  ";

		
			if($rdate_from != ""){
				$sql .= " AND m.reg_date >= '".$rdate_from." 00:00:00' ";	
			}

			if($rdate_to != ""){
				$sql .= " AND m.reg_date <= '".$rdate_to." 23:59:59' ";	
			}			
		
		$sql .= Omm_client_Model::getFilterSql('all',$tagfilter,$productfilter,$productfilter);
		$sql .= Omm_client_Model::getSearchSql($searchfield,$searchkeyword,$searchfields);
		
		 //
		
		if($orderby == "reg_date") $orderby = "m.reg_date";
		
		//		
		if($memberStatus == 'unsubscribed' && $orderby == 'reg_date')
			$sql .= " order by unsubscribe_date $order ";
		else
			$sql .= " order by $orderby $order ";
		
		if($limit)
		$sql .= " LIMIT $offset,$rowperpage";
		
		//echo $sql;
		
		$result = $this->db->query($sql);

		return $result;

	}

}
?>