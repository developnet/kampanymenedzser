<?php
class Omm_list_member_connect_Model extends ORM {
	
	protected $db = "own";
	
	protected $sorting = array('reg_date' => 'desc');
	
	protected $belongs_to = array("omm_list");
	
	protected $has_and_belongs_to_many = array('omm_list_groups');
	

	
	
	public function saveObject(){
		$this->mod_date = date('Y-m-d H:i:s');
		if(isset($_SESSION)){
			$this->mod_user_id = $_SESSION['auth_user']->id;	
		}
		
		$this->save();
	}		
	
	
}
?>