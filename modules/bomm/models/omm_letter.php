<?php
require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";
require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."emogrifier.php";

class Omm_letter_Model extends ORM {

	protected $db = "own";

	protected $sorting = array('created_date' => 'desc', 'name' => 'asc');

	protected $has_many = array("omm_letter_recip_condition", "omm_letter_detail");

	protected $belongs_to = array('omm_campaign');

	private $checkOk = "Tick";
	private $checkFailed = "Off";

	public function created(){
		return dateutils::formatDate($this->created_date);
	}
	
	public function getSendingRatio(){

		$sql = "SELECT count(id) as ossz FROM omm_jobs WHERE omm_letter_id = '".$this->id."' AND status = 'sent' AND type='dayjob' ";
		$res = $this->db->query($sql);		
		$sent_count = $res[0]->ossz;		

		$sql = "SELECT count(id) as ossz FROM omm_jobs WHERE omm_letter_id = '".$this->id."' AND status = 'prepared'  AND type='dayjob'";
		$res = $this->db->query($sql);		
		$prepared_count = $res[0]->ossz;

		$ossz = $sent_count + $prepared_count;
		
		return round((($sent_count / $ossz)*100),2);
	}
	
	public function getSentTime(){
		
		$sql = "SELECT max(sent_time) as max FROM omm_jobs WHERE omm_letter_id = '".$this->id."' AND status = 'sent'  AND type='dayjob'";
		$ends = $this->db->query($sql);		
		$end = $ends[0]->max;	

		return dateutils::formatDateWithTime($end);
		
	}
	
	public function getFieldsInLetter(){
		$return = array();
		
		$sepStart = Kohana::config('core.fieldSeparatorStart');
		$sepEnd = Kohana::config('core.fieldSeparatorEnd');
		$unsubLink = Kohana::config('core.unsubscribe_link_name');
		$actLink = Kohana::config('core.activation_link_name');
		$modLink = Kohana::config('core.datamod_link_name');
			
		$pattern = '/'.$sepStart.'[^'.$sepStart.']+?'.$sepEnd.'/i';

		$fldEmp = Kohana::config('core.fieldEmpty');
		$fldEmpSep = Kohana::config('core.fieldEmptySeparator');
		$fldUresSep = Kohana::config('core.fieldUresSeparator');
			
		$startSepSize = strlen($sepStart);
		$endSepSize = strlen($sepEnd);

		
			$matches = array();  //////html
		preg_match_all($pattern, $this->html_content, $matches);
		
		$_fields = array("email");
		
		$ok = 0;
		$error = array();
		$i = 0;
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m != $sepStart.$unsubLink.$sepEnd && $m != $sepStart.$actLink.$sepEnd && $m != $sepStart.$modLink.$sepEnd){////leiratkozo link

					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);

					$reference = $ref['reference'];
					$empty = $ref['empty'];

					if(!in_array($ref['reference'],$_fields)){
						$return[] = $ref;	
						$_fields[] = $ref['reference'];
					}					
					
					

				}
			}
		}
			

		$matches = array();
		preg_match_all($pattern, $this->text_content, $matches);

		$ok = 0;
		$i = 0;
		$error_text = array();
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m != $sepStart.$unsubLink.$sepEnd && $m != $sepStart.$actLink.$sepEnd && $m != $sepStart.$modLink.$sepEnd){////leiratkozo link
					
					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);

					$reference = $ref['reference'];
					$empty = $ref['empty'];

					if(!in_array($ref['reference'],$_fields)){
						$return[] = $ref;	
						$_fields[] = $ref['reference'];
					}										
				}
			}
		}

		$matches = array();
		preg_match_all($pattern, $this->subject, $matches);

		$ok = 0;
		$i = 0;
		$error_subject = array();
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				
				if($m != $sepStart.$unsubLink.$sepEnd && $m != $sepStart.$actLink.$sepEnd && $m != $sepStart.$modLink.$sepEnd){////leiratkozo link
					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);

					$reference = $ref['reference'];
					$empty = $ref['empty'];

					if(!in_array($ref['reference'],$_fields)){
						$return[] = $ref;	
						$_fields[] = $ref['reference'];
					}										
				}
			}
		}		
		
		
		
		return $return;
	}
	
	public function setHtmlContent($html){
		
		/*
		$typo = $this->getDetail("typo");
        $typoCssView = new View(Kohana::config('admin.theme')."/typos/".$typo);		
        $css = $typoCssView->render(FALSE,FALSE);	
        
		$em = new Emogrifier($html,$css);
		$em->emogrify();
		*/
		
		$html = str_ireplace("%7B%7B", "{{", $html);
		$html = str_ireplace("%7D%7D", "}}", $html);
		$this->html_content = $html;
	}
	
	public function getDetail($key){
		return "none";
		
		/*
		$detail = $this->db->from("omm_letter_details")->where("omm_letter_id", $this->id)->where("key", $key)->get();
		
		if(sizeof($detail) <= 0){
			return "none";
		}else{
			return $detail[0]->value;
		}
		*/
	}

	public function setDetail($key, $value){
		if($this->getDetail($key) == "none"){
			$this->db->insert("omm_letter_details", array("omm_letter_id" => $this->id, "key" => $key, "value" => $value, "mod_user_id" => $_SESSION['auth_user']->id));	
		}else{
			$status = $db->update("omm_letter_details", array("key" => $key, "value" => $value, "mod_user_id" => $_SESSION['auth_user']->id), array("omm_letter_id" => $this->id, "key", "key" => $key));			
		}
	}
	
	public function saveObject(){
		if(isset($_SESSION)){
			$this->mod_user_id = $_SESSION['auth_user']->id;	
		}else{
			$this->mod_user_id = 0;
		}
		
		$this->save();
	}

	public function contentCheck($boolean = false){

		if($this->type == "html" && $this->html_content != ""){

			if($boolean) return true;
			else return $this->checkOk;

		}else if($this->type == "text" && $this->text_content != ""){

			if($boolean) return true;
			else return $this->checkOk;

		}else{

			if($boolean) return false;
			else return $this->checkFailed;

		}


	}

	public function getTimingDate(){

		$d = dateutils::formatDate($this->timing_date);


		if($this->timing_hour != NULL){
			$d .= " ".$this->timing_hour." óra";
		}

		return $d;
	}

	/**
	 * ellenőrzi-e, hogy el lett-e kezdve a kiküldés, (inaktivvá tehető-e még vagy nem)
	 */
	public function timingCheckForHour($boolean = false){
		if($this->timing_type == "relative") {
			if($boolean) return true;
			else return $this->checkOk;
		}

		$timing_date = strtotime($this->timing_date);
		$now =  strtotime(date("Y-m-d"));

		if($timing_date < $now)	{
			if($boolean) return false;
			else return $this->checkFailed;
		}
		//echo $timing_date." - ".$now."<br/>";

		if($this->timing_hour != NULL){

			$now_hour =  strtotime(date("Y-m-d H").":00:00");
			$timing_hour = strtotime($this->timing_date." ".$this->timing_hour.":00:00");

			//echo $timing_hour." - ".$now_hour;

			if($timing_date == $now && $timing_hour <= $now_hour){
				if($boolean) return false;
				else return $this->checkFailed;
			}

		}else{

			$now_hour =  strtotime(date("Y-m-d H").":00:00");
			$limit_hour = strtotime(date("Y-m-d")." 01:59:00");
			if($timing_date <= $now && $now_hour >= $limit_hour)	{
				if($boolean) return false;
				else return $this->checkFailed;
			}

		}


		if($boolean) return true;
		else return $this->checkOk;
	}

	public function timingCheck($boolean = false){
		if($this->timing_type == "" ){

			if($boolean) return false;
			else return $this->checkFailed;

		}else if($this->timing_type == "relative" || $this->timing_type == "absolute" || $this->timing_type == "sendnow"){

			if($boolean) return true;
			else return $this->checkOk;

		}

	}

	public function recipCheck($boolean = false){
		$cond = $this->db->from("omm_letter_recip_conditions")->where("omm_letter_id",$this->id)->count_records();

		if($cond > 0){

			if($boolean) return true;
			else return $this->checkOk;

		}else{

			if($boolean) return false;
			else return $this->checkFailed;

		}

	}


	public function testCheck($boolean = false){

		$res = $this->db->from("omm_jobs")->where('omm_letter_id',$this->id)->where('type','test')->where('status','sent')->count_records();

		if($res>0){

			if($boolean) return true;
			else return $this->checkOk;

		}else{

			if($boolean) return false;
			else return $this->checkFailed;

		}

	}

	public function fieldsCheck(){
		//text_content
		//html_content

		////feltételben szereplő listák leválogatása
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$this->id)->where("sign","plus")->get();
		$ids = array();
		foreach ($res as $r){
			$ids[] = $r->id;
		}


		$_fields = $this->omm_campaign->omm_client->getAllAllFields();
			
		$fields = array();
		$fields[] = "regdatum";
		$fields[] = "email";
		$fields[] = "code";

		foreach ($_fields as $f){
			$fields[] = $f->reference;
		}


		
		$sepStart = Kohana::config('core.fieldSeparatorStart');
		$sepEnd = Kohana::config('core.fieldSeparatorEnd');
		$unsubLink = Kohana::config('core.unsubscribe_link_name');
		$actLink = Kohana::config('core.activation_link_name');
		$modLink = Kohana::config('core.datamod_link_name');
			
		$pattern = '/'.$sepStart.'[^'.$sepStart.']+?'.$sepEnd.'/i';

		$fldEmp = Kohana::config('core.fieldEmpty');
		$fldEmpSep = Kohana::config('core.fieldEmptySeparator');
		$fldUresSep = Kohana::config('core.fieldUresSeparator');
			
		$startSepSize = strlen($sepStart);
		$endSepSize = strlen($sepEnd);
			
			
			
		$matches = array();  //////html
		preg_match_all($pattern, $this->html_content, $matches);

		$ok = 0;
		$error = array();
		$i = 0;
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m == $sepStart.$unsubLink.$sepEnd){////leiratkozo link

					$ok++;

				}elseif($m == $sepStart.$actLink.$sepEnd){////ha aktivációs link

					$ok++;

				}elseif($m == $sepStart.$modLink.$sepEnd){////ha adatmódosító link

					$ok++;

				}else{/////ha sime mező hivatkozás

					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);


					$reference = $ref['reference'];
					$empty = $ref['empty'];
					if(in_array($reference,$fields )){
						$ok++;
					}else{
						$error[]=$m;
					}

				}
			}
		}
			

		$matches = array();
		preg_match_all($pattern, $this->text_content, $matches);

		$ok = 0;
		$i = 0;
		$error_text = array();
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m == $sepStart.$unsubLink.$sepEnd){////leiratkozo link

					$ok++;

				}elseif($m == $sepStart.$actLink.$sepEnd){////ha aktivációs link

					$ok++;

				}elseif($m == $sepStart.$modLink.$sepEnd){////ha adatmódosító link

					$ok++;

				}else{/////ha sime mező hivatkozás

					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);


					$reference = $ref['reference'];
					$empty = $ref['empty'];
					if(in_array($reference,$fields )){
						$ok++;
					}else{
						$error_text[]=$m;
					}

				}
			}
		}

		$matches = array();
		preg_match_all($pattern, $this->subject, $matches);

		$ok = 0;
		$i = 0;
		$error_subject = array();
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m == $sepStart.$unsubLink.$sepEnd){////leiratkozo link

					$ok++;

				}elseif($m == $sepStart.$actLink.$sepEnd){////ha aktivációs link

					$ok++;

				}elseif($m == $sepStart.$modLink.$sepEnd){////ha adatmódosító link

					$ok++;

				}else{/////ha sime mező hivatkozás

					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);


					$reference = $ref['reference'];
					$empty = $ref['empty'];
					if(in_array($reference,$fields )){
						$ok++;
					}else{
						$error_subject[]=$m;
					}

				}
			}
		}

		$ret = array();

		$ret['html'] = $error;
		$ret['text'] = $error_text;
		$ret['subject'] = $error_subject;
		return $ret;

	}

	public function isReady(){
		$ret = $this->fieldsCheck();
		return ($this->contentCheck(true) && $this->recipCheck(true)  && $this->testCheck(true)  &&	$this->timingCheckForHour(true) && sizeof($ret['html']) == 0 && sizeof($ret['text'] == 0));
	}

	public function getTests(){
		$res = $this->db->from("omm_jobs")->where('omm_letter_id',$this->id)->where('type','test')->orderby('created_time', 'desc')->get();
		return $res;
	}

	public function type(){

		if($this->timing_type == 'relative') return "Relatív";
		else if($this->timing_type == 'absolute') return "Abszolút";
		else if($this->timing_type == 'sendnow') return "Azonnali";
		else return "Nincs megadva";

	}

	public function timing(){

		if($this->timing_type == 'relative') return $this->timing_event_value.' nappal a feliratkozás után.';
		else if($this->timing_type == 'absolute') return dateutils::formatDate($this->timing_date);
		else if($this->timing_type == 'sendnow') return "-";
		else return "Nincs megadva";

	}

	public function sendings(){
		return 'Küldések:';
	}

	public function sentNum(){
		return $this->db->from("omm_jobs")->where("omm_letter_id",$this->id)->where('type!=','common')->where('type!=','test')->where('status','sent')->count_records();
	}

	public function opened(){
		//return $this->db->from("omm_letter_open_log")->where("omm_letter_id",$this->id)->where('status','1')->count_records();
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "
				SELECT count(*) as count 
				  FROM ".$tp."omm_letter_open_log open, ".$tp."omm_jobs job
				 WHERE open.omm_letter_id = ".$this->id."
				   AND open.status = 1
				   AND job.id = open.omm_job_id
				   AND job.type != 'test'; 
		";		

		$opened = $this->db->query($sql);

		return $opened[0]->count;
	}

	public function clicked(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		//$clicked =  $this->db->select("DISTINCT omm_letter_link_redirections.omm_job_id")->from("omm_letter_link_redirections")->where("omm_letter_id",$this->id)->where('clicked IS NOT',NULL)->where('url_name !=','unsubscribe')->get();

		$sql = "
				SELECT DISTINCT red.omm_job_id 
				  FROM ".$tp."omm_letter_link_redirections red, ".$tp."omm_jobs job
				 WHERE red.omm_letter_id = ".$this->id."
				   AND red.clicked IS NOT NULL
				   AND red.url_name != 'unsubscribe'
				   AND job.id = red.omm_job_id
				   AND job.type != 'test'; 
		";

		$clicked = $this->db->query($sql);

		return sizeof($clicked);
	}

	public function unsubscribed(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		//return  $this->db->from("omm_common_letter_link_redirections")->where("omm_letter_id",$this->id)->where('clicked IS NOT',NULL)->where('url_name','unsubscribe')->count_records();

		$sql = "
				SELECT count(*) as count 
				  FROM ".$tp."omm_letter_link_redirections red, ".$tp."omm_jobs job
				 WHERE red.omm_letter_id = ".$this->id."
				   AND red.clicked IS NOT NULL
				   AND red.url_name = 'unsubscribe'
				   AND job.id = red.omm_job_id
				   AND job.type != 'test'; 
		";

		$unsubscribed = $this->db->query($sql);

		return $unsubscribed[0]->count;
	}

	public function productsIncludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product")->where("omm_letter_id",$this->id)->where("sign","plus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_product")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}	
	
	public function productsExcludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product")->where("omm_letter_id",$this->id)->where("sign","minus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_product")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}		
	
	public function tagsExcludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","tag")->where("omm_letter_id",$this->id)->where("sign","minus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_tag")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}	
	
	public function tagsIncludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","tag")->where("omm_letter_id",$this->id)->where("sign","plus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_tag")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}	
	
	public function listsIncludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$this->id)->where("sign","plus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_list")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}


	public function listsExcludes(){

		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$this->id)->where("sign","minus")->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		if(sizeof($ids) > 0){
			return ORM::factory("omm_list")->in('id', $ids)->find_all();
		}else {
			return array();
		}

	}


	public function deleteGroupConditionInclude($gid){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'group', 'sign' => 'plus', 'value' => $gid));
	}

	public function deleteGroupConditionExclude($gid){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'group', 'sign' => 'minus', 'value' => $gid));
	}


	public function deleteProductCondition($id,$sign){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'product', 'sign' => $sign, 'value' => $id));
	}		
	
	public function deleteTagCondition($id,$sign){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'tag', 'sign' => $sign, 'value' => $id));
	}	
	
	public function deleteListConditionInclude($lid){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'list', 'sign' => 'plus', 'value' => $lid));
	}

	public function deleteListConditionExclude($lid){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'list', 'sign' => 'minus', 'value' => $lid));
	}

	public function emailConditions(){
		return $this->db->from("omm_letter_recip_conditions")->where("type","email")->where("omm_letter_id",$this->id)->where("sign","minus")->get();
	}

	public function deleteLetter(){
		/*
		 $tablename = Kohana::config("database.default");
		 $tp = $tablename['table_prefix'];

		 $sql = "DELETE FROM ".$tp."omm_letter_link_clicks WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_letter_links WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_letter_open_log WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_letter_open WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_letter_recip_conditions WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_jobs WHERE omm_letter_id = ".$this->id." ";
		 $this->db->query($sql);

		 $sql = "DELETE FROM ".$tp."omm_day_job_details WHERE value = ".$this->id." AND type IN ('absolute_letter','relative_letter','sendnow_letter') ";
		 $this->db->query($sql);

		 $this->delete();
		 */

		$this->status = 'deleted';
		$this->saveObject();

	}

	public function deleteEmailCondition($id){
		return $this->db->delete('omm_letter_recip_conditions', array('id' => $id));
	}

	public function clearProductStatsForProduct($sign,$pid){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'product_stat', 'sign' => $sign, 'allinlist' => $pid));
	}	
	
	public function clearProductStats($sign){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'product_stat', 'sign' => $sign,));
	}
	
	public function clearProducts($sign){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'product', 'sign' => $sign));
	}
	
	public function clearIncludeLists(){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'list', 'sign' => 'plus'));
	}

	public function clearExcludeLists(){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'list', 'sign' => 'minus'));
	}

	public function clearIncludeGroups(){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'group', 'sign' => 'plus'));
	}

	public function clearExcludeGroups(){
		return $this->db->delete('omm_letter_recip_conditions', array('omm_letter_id' => $this->id, 'type' => 'group', 'sign' => 'minus'));
	}
	
	public function testProductCondition($sign, $pid, $stat){
		if($stat == 'all'){
			$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product")->where("omm_letter_id",$this->id)->where("sign",$sign)->where('value',$pid)->where('allinlist',1)->get();
		}else{
			$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product_stat")->where("omm_letter_id",$this->id)->where("sign",$sign)->where('value',$stat)->where('allinlist',$pid)->get();			
		}
		
		if(sizeof($res) == 0) return false;
		else return true;		
		
	}
	
	public function addProductStat($stat,$pid,$sign){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product_stat")->where("omm_letter_id",$this->id)->where("sign",$sign)->where('value',$stat)->where('allinlist',$pid)->get();

		if(sizeof($res) == 0)
			return $this->db->insert('omm_letter_recip_conditions', array('value' => $stat, 'allinlist' => $pid, 'type' => 'product_stat', 'omm_letter_id' => $this->id,'sign' => $sign));
		else return false;
	}	
	
	public function addEmailCondition($email){

		$res = $this->db->from("omm_letter_recip_conditions")->where('value',$email)->where('type','email')->where('omm_letter_id',$this->id)->get();
		
		if(sizeof($res) == 0)
			return $this->db->insert('omm_letter_recip_conditions', array('value' => $email, 'type' => 'email', 'omm_letter_id' => $this->id,'sign' => 'minus'));
		else return false;
	}

	public function addGroupForInclude($id){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","group")->where("omm_letter_id",$this->id)->where("sign",'plus')->where('value',$id)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $id, 'type' => 'group', 'omm_letter_id' => $this->id,'sign' => 'plus'));
		else
		return false;
	}

	public function addGroupForExclude($id){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","group")->where("omm_letter_id",$this->id)->where("sign",'minus')->where('value',$id)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $id, 'type' => 'group', 'omm_letter_id' => $this->id,'sign' => 'minus'));
		else
		return false;
	}

	public function addProductsForInclude($tagId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product")->where("omm_letter_id",$this->id)->where("sign",'plus')->where('value',$tagId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $tagId, 'type' => 'product', 'omm_letter_id' => $this->id,'sign' => 'plus', 'allinlist' => $allin));
		else
		return false;
	}	
	
	public function addProductsForExclude($tagId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","product")->where("omm_letter_id",$this->id)->where("sign",'minus')->where('value',$tagId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $tagId, 'type' => 'product', 'omm_letter_id' => $this->id,'sign' => 'minus', 'allinlist' => $allin));
		else
		return false;
	}	
	
	public function addTagsForExclude($tagId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","tag")->where("omm_letter_id",$this->id)->where("sign",'minus')->where('value',$tagId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $tagId, 'type' => 'tag', 'omm_letter_id' => $this->id,'sign' => 'minus', 'allinlist' => $allin));
		else
		return false;
	}
	
	public function addTagsForInclude($tagId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","tag")->where("omm_letter_id",$this->id)->where("sign",'plus')->where('value',$tagId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $tagId, 'type' => 'tag', 'omm_letter_id' => $this->id,'sign' => 'plus', 'allinlist' => $allin));
		else
		return false;
	}


	public function addListForInclude($listId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$this->id)->where("sign",'plus')->where('value',$listId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $listId, 'type' => 'list', 'omm_letter_id' => $this->id,'sign' => 'plus', 'allinlist' => $allin));
		else
		return false;
	}

	public function addListForExclude($listId, $allin = '1'){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","list")->where("omm_letter_id",$this->id)->where("sign",'minus')->where('value',$listId)->get();

		if(sizeof($res) == 0)
		return $this->db->insert('omm_letter_recip_conditions', array('value' => $listId, 'type' => 'list', 'omm_letter_id' => $this->id,'sign' => 'minus', 'allinlist' => $allin));
		else
		return false;

	}


	public function groupCondArray($sign){
		$res = $this->db->select("omm_letter_recip_conditions.value as id")->from("omm_letter_recip_conditions")->where("type","group")->where("omm_letter_id",$this->id)->where("sign",$sign)->get();

		$ids = array();

		foreach ($res as $r){
			$ids[] = $r->id;
		}
		return $ids;
	}

	public function getRecipients($count = TRUE, $idsInclude = array(), $idsExclude = array(), $relative = FALSE, $debug = FALSE){
		return $this->getRecipientsNumberFilter($count, $idsInclude, $idsExclude , $relative , $debug);
	}


	public function getRecipientsForJob($job_id,$letter_id){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$ids = array();
		///////akik már ebben a dayjob-ban és ebben a levélben szerepelnek
		$res = $this->db->query("
				SELECT j.omm_list_member_id as id FROM ".$tp."omm_jobs j 
							WHERE j.omm_day_job_id = ".$job_id." AND j.omm_letter_id = ".$letter_id." ");

		foreach($res as $r){
			$ids[] = $r->id;
		}

		return $this->getRecipientsNumber(FALSE, array(), $ids, TRUE);
	}

	
	public function getRecipientsNumberFilter($count = TRUE, $idsInclude = array(), $idsExclude = array(), $relative = FALSE, $debug = FALSE){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		
		$client = $this->omm_campaign->omm_client;	
		
		$minus_member_ids = array(); ///groupokból
		$plus_member_ids = array(); //groupokból
		
		////////minuszos e-mailek leválogatása
		if($debug) echo '<h1>minus listákból jövő mailok</h1>';
		$minus_list_ids = $this->db->query("
				SELECT value AS list_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'list'
									     AND sign = 'minus' 
									     AND allinlist = 1;  
		");			
		
		
		
		if($debug) echo '<h1>minus csoportokból jövő mailok</h1>';
		/////////////minus csoportok tagjai
		$plusGroupIds = $this->groupCondArray('minus');

		foreach ($plusGroupIds as $gid){
			$group = ORM::factory('omm_list_group')->find($gid);
			if($group->loaded){
				$_fields = $client->getAllFields($group->omm_list_id);
				$members = $group->getMembers($_fields,'active','reg_date','desc',0,0,false, "all" , "all",  "all", "none",  "none",$_fields);
				foreach ($members as $m){
					$minus_member_ids[] = $m->id;
				}
			}
		}
		/////////////		
		
		if($debug) echo '<h1>minus termékek jövő mailok</h1>';
		$minus_product_ids = $this->db->query("
				SELECT value AS product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'product'
									     AND sign = 'minus' 
									     AND allinlist = 1;  
		");		
		
		
		if($debug) echo '<h1>minus termékek stattal jövő mailok</h1>';
		$minus_product_ids_with_stat = $this->db->query("
				SELECT value AS status, allinlist as product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'product_stat'
									     AND sign = 'minus' 
									     
		");			
		
		
		///// minusos címkék
		$minus_tag_ids = $this->db->query("
				SELECT c.value as tag_id FROM ".$tp."omm_letter_recip_conditions c   
							WHERE c.type = 'tag'  AND c.sign = 'minus'
							  AND c.omm_letter_id = ".$this->id."		
		");				
		
		
		////////////email kivételek
		$minus_emails = $this->db->query("
				SELECT c.value as email FROM ".$tp."omm_letter_recip_conditions c   
							WHERE c.type = 'email'  AND c.sign = 'minus'
							  AND c.omm_letter_id = ".$this->id."		
		");		
		
		
		/////////////**********/////////////////
		
		////////plus listák tagjai
		$plus_list_ids = $this->db->query("
				SELECT value AS list_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'list'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");
		
		////////plus státuszos termékek tagjai
		$plus_product_ids_with_stat = $this->db->query("
				SELECT value AS status, allinlist as product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'product_stat'  
									   	 AND sign = 'plus' 
									     
		");

		////////plus teljes termékek tagjai
		$plus_product_ids = $this->db->query("
				SELECT value AS product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'product'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");			
		
		
		////////plus címkék tagjai
		$plus_tag_ids = $this->db->query("
				SELECT value AS tag_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'tag'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");		
		
		
		
		if($debug) echo '<h1>plus csoportokból jövő mailok - az előző 4 szürővel</h1>';
		/////////////plus csoportok tagjai
		$plusGroupIds = $this->groupCondArray('plus');
		foreach ($plusGroupIds as $gid){
			$group = ORM::factory('omm_list_group')->find($gid);
			if($group->loaded){
				$_fields = $client->getAllFields($group->omm_list_id);
				$members = $group->getMembers($_fields,'active','reg_date','desc',0,0,false, "all" , "all",  "all", "none",  "none",$_fields);
				foreach ($members as $m){
					$plus_member_ids[] = $m->id;
				}
			}
		}
		/////////////
		
		foreach($idsInclude as $id){
			$plus_member_ids[] = $id;	
		}

		foreach($idsExclude as $id){
			$minus_member_ids[] = $id;	
		}		
		
		/*
		-$minus_member_ids
		-$plus_member_ids

		-$minus_list_ids
		-$minus_product_ids-
		-$minus_product_ids_with_stat-
		-$minus_tag_ids

		-$minus_emails
		
		-$plus_list_ids
		-$plus_product_ids
		-$plus_product_ids_with_stat
		-$plus_tag_ids		
		*/
		
		if(
			sizeof($plus_member_ids) == 0 && 
			sizeof($plus_list_ids) == 0 && 
			sizeof($plus_product_ids) == 0 && 
			sizeof($plus_product_ids_with_stat) == 0 &&
			sizeof($plus_tag_ids) == 0
			
		){
			if($count) return 0;
			else return array();
		}
		
		$sql = "select m.* 
				from ";
		if(sizeof($plus_list_ids) > 0)
			$sql .= "omm_list_member_connect lc,"; 
		if(sizeof($plus_tag_ids) > 0)
			$sql .= "omm_tag_objects tc,";					 
		if(sizeof($plus_product_ids) > 0)
			$sql .= "omm_product_member_connects pc,";

			
			$sql .= " omm_list_members m ";
			
			$sql .= " where ";
				
			
		
		if(sizeof($plus_tag_ids) > 0){
			$sql .= " m.id = tc.object_id AND ";
			
			$sql .= " tc.tag_id IN(0 ";

			foreach($plus_tag_ids as $t){
				$sql .= ",".$t->tag_id."";	
			}
			
			$sql .= ") AND";
			
		}
								 
		if(sizeof($plus_product_ids) > 0){
			$sql .= " m.id = pc.omm_list_member_id AND ";
			
			$sql .= " pc.omm_product_id IN(0 ";

			foreach($plus_product_ids as $p){
				$sql .= ",".$p->product_id."";	
			}
			
			$sql .= ") AND";
		}
			
		
		if(sizeof($plus_product_ids_with_stat) > 0){
			$sql .= " ( 1=2 ";
			$i = 2;
			foreach($plus_product_ids_with_stat as $p){
				$sql .= " OR ((SELECT count(id) FROM omm_product_member_connects WHERE omm_product_id = ".$p->product_id." AND status = '".$p->status."' AND omm_list_member_id = m.id ) > 0)  ";
			}
			$sql .= " ) AND ";
		}			
		
		
		if(sizeof($minus_list_ids) > 0){
			$sql .= " (SELECT count(id) FROM omm_list_member_connect WHERE m.id=omm_list_member_id AND  ";

			$sql .= " omm_list_id IN(-1 ";

			foreach($minus_list_ids as $t){
				$sql .= ",".$t->list_id."";	
			}
			
			$sql .= ")) <= 0 AND ";
		}			
		
		if(sizeof($minus_product_ids) > 0){//
			$sql .= " (SELECT count(id) FROM omm_product_member_connects WHERE m.id=omm_list_member_id AND  ";

			$sql .= " omm_product_id IN(-1 ";

			foreach($minus_product_ids as $t){//
				$sql .= ",".$t->product_id."";	
			}
			
			$sql .= ")) <= 0 AND ";
		}			
		
		if(sizeof($minus_product_ids_with_stat) > 0){

			foreach($minus_product_ids_with_stat as $t){//
				$sql .= " (SELECT count(id) FROM omm_product_member_connects WHERE m.id=omm_list_member_id AND status='".$t->status."' AND omm_product_id=".$t->product_id." ";
				$sql .= ") <= 0 AND ";	
			}
			
			
		}			
		
		if(sizeof($minus_tag_ids) > 0){//
			$sql .= " (SELECT count(id) FROM omm_tag_objects WHERE m.id=object_id AND object_type='member' AND  ";

			$sql .= " tag_id IN(-1 ";

			foreach($minus_tag_ids as $t){//
				$sql .= ",".$t->tag_id."";	
			}
			
			$sql .= ")) <= 0 AND ";
		}			
		
		
		
		
		if(sizeof($minus_emails) > 0){ 
			$sql .= " m.email NOT IN ('x@x.hu'";
			foreach($minus_emails as $e){
				$sql .=",'".$e->email."'";
			}
			$sql .= " ) AND ";
		}

		
		if(sizeof($plus_member_ids) > 0){ 
			$sql .= " m.id  IN (-1";
			foreach($plus_member_ids as $i){
				$sql .=",".$i."";
			}
			$sql .= " ) AND ";
		}

		if(sizeof($minus_member_ids) > 0){ 
			$sql .= " m.id  NOT IN (-1";
			foreach($minus_member_ids as $i){
				$sql .=",".$i."";
			}
			$sql .= " ) AND ";
		}		
		
			
			$sql .= " m.status = 'active' ";
			
			
			if($relative && $this->timing_type == 'relative'){
					
					$day = $this->timing_event_value;
					$rel = date('Y-m-d',strtotime('-'.$day.' day'));					
				
					if(sizeof($plus_list_ids) > 0){
						$sql .= " AND m.id = lc.omm_list_member_id AND ";
						$sql .= " lc.omm_list_id = ".$plus_list_ids[0]->list_id." AND ";
						$sql .= " lc.reg_date >= '".$rel." 00:00:00' AND ";
						$sql .= " lc.reg_date <= '".$rel." 23:59:59'  ";
					}

			}else{
				
				if(sizeof($plus_list_ids) > 0){
					$sql .= " AND m.id = lc.omm_list_member_id AND ";
		
					$sql .= " lc.omm_list_id IN(-1 ";
		
					foreach($plus_list_ids as $t){
						$sql .= ",".$t->list_id."";	
					}
					
					$sql .= ")";			
				}				
				
			}			
			
			$omm_client_id = $this->omm_campaign->omm_client->id;
			
			$sql .= " and m.omm_client_id = ".$omm_client_id." group by m.id ";			
			/*
			
				AND
					lc.omm_list_id IN(3)
				and
					tc.tag_id IN(19,20)
				
					group by m.id";		
		
		*/
			
		//echo $sql;	
		$members = $this->db->query($sql);		
		
		if($count) {
			return sizeof($members);
		}else{
			return $members;
		}
		
		//$active_members_num = $this->omm_client->membersNumber($allfields,'active',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);
		
	}
	
	public function getRecipientsNumber($count = TRUE, $idsInclude = array(), $idsExclude = array(), $relative = FALSE, $debug = FALSE){
		return $this->getRecipientsNumberFilter($count, $idsInclude, $idsExclude, $relative, $debug);//
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		
		$client = $this->omm_campaign->omm_client;
		
		
		$idIn = array();

		$emailIn = array('x@x.x');
		$ids = array();
		$emailNotIn = array('x@x.x');


		////////minuszos e-mailek leválogatása
		if($debug) echo '<h1>minus listákból jövő mailok</h1>';
		$res = $this->db->query("
				SELECT value AS list_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'list'
									     AND sign = 'minus' 
									     AND allinlist = 1;  
		");		

		foreach ($res as $list){//////////minuszos teljes listák

			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_list_member_connect c
										WHERE c.omm_list_id = ".$list->list_id."
										  AND m.id = c.omm_list_member_id 
										  AND m.status = 'active'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}

			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				$emailNotIn[] = $email->email;
				if($debug) echo $email->email.' - '.$email->id.'<br/>';
			}

		}

		if($debug) echo '<h1>minus csoportokból jövő mailok</h1>';
		/////////////minus csoportok tagjai
		$plusGroupIds = $this->groupCondArray('minus');

		foreach ($plusGroupIds as $gid){
			$group = ORM::factory('omm_list_group')->find($gid);
			if($group->loaded){
				$_fields = $client->getAllFields($group->omm_list_id);
				$members = $group->getMembers($_fields,'active','reg_date','desc',0,0,false, "all" , "all",  "all", "none",  "none",$_fields);
				foreach ($members as $m){
					if(!in_array($m->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában
						$emailNotIn[] = $m->email;
						if($debug) echo $m->email.' - '.$m->id.'<br/>';
					}
				}
			}
		}
		/////////////

		///// minusos címkék
		
		$res = $this->db->query("
				SELECT c.value as tag_id FROM ".$tp."omm_letter_recip_conditions c   
							WHERE c.type = 'tag'  AND c.sign = 'minus'
							  AND c.omm_letter_id = ".$this->id."		
		");				
		
		
		////////////email kivételek

		$res = $this->db->query("
				SELECT c.value as email FROM ".$tp."omm_letter_recip_conditions c   
							WHERE c.type = 'email'  AND c.sign = 'minus'
							  AND c.omm_letter_id = ".$this->id."		
		");

		if($debug) echo '<h1>email kivételekből jövő mailok</h1>';
		foreach($res as $r){

			if(!arr::binary_search($r->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában
				$emailNotIn[] = $r->email;
				if($debug) echo $r->email.'<br/>';
			}

		}

		///////////////

		
		if($debug) echo '<h1>minus termékek jövő mailok</h1>';
		$res = $this->db->query("
				SELECT value AS product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'product'
									     AND sign = 'minus' 
									     AND allinlist = 1;  
		");		

		foreach ($res as $r){//////////minuszos teljes termékek

			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_product_member_connects c
										WHERE c.omm_product_id = ".$r->product_id."
										  AND m.id = c.omm_list_member_id 
										  AND m.status = 'active'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}

			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				if(!arr::binary_search($email->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában
					$emailNotIn[] = $email->email;
					if($debug) echo $email->email.'<br/>';
				}				
			}

		}////minuszos termékek		
		
		
		if($debug) echo '<h1>minus termékek stattal jövő mailok</h1>';
		$res = $this->db->query("
				SELECT value AS status, allinlist as product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									     AND type = 'product_stat'
									     AND sign = 'minus' 
									     
		");		

		foreach ($res as $r){//////////minuszos statos termékek

			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_product_member_connects c
										WHERE c.omm_product_id = ".$r->product_id."
										  AND m.id = c.omm_list_member_id 
										  AND m.status = 'active' 
										  AND c.status = '".$r->status."'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}

			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				if(!arr::binary_search($email->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában
					$emailNotIn[] = $email->email;
					if($debug) echo $email->email.'<br/>';
				}				
			}

		}////minuszos termékek		
		
		///// minusos címkék
		
		$res = $this->db->query("
				SELECT c.value as tag_id FROM ".$tp."omm_letter_recip_conditions c   
							WHERE c.type = 'tag'  AND c.sign = 'minus'
							  AND c.omm_letter_id = ".$this->id."		
		");		
		
		
		foreach ($res as $tag){//////////minuszos címkék//

			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_tag_objects c
										WHERE c.tag_id = ".$tag->tag_id."
										  AND m.id = c.object_id 
										  AND c.object_type = 'member'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";

				if($i<sizeof($emailNotIn)) $sql .=",";

				$i++;
			}

			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$res = $this->db->query($sql);

			foreach ($res as $r){
					if(!arr::binary_search($r->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában
						$emailNotIn[] = $r->email;
						if($debug) echo $r->email.'<br/>';
					}				
			}

		}/////miniuszos címkék		
		

		////////plus listák tagjai
		$res = $this->db->query("
				SELECT value AS list_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'list'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");
		if($debug) echo '<h1>plus listákból jövő mailok - az elző 3 szűrővel</h1>';

		foreach ($res as $list){////////plus listák, már csakazok kerülnek bele amik még nem kerültek bele (email not in)
			//$emails = $this->db->from('omm_list_members')->where('omm_list_id',$list->list_id)->where('status','active')->notin('email',$emailIn)->get();

			
			
			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_list_member_connect c
										WHERE c.omm_list_id = ".$list->list_id."
										  AND m.id = c.omm_list_member_id 	
										  AND m.status = 'active'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailIn as $_email){
				$sql .= "'".$_email."'";

				if($i<sizeof($emailIn)) $sql .=",";

				$i++;
			}
			$sql .=") ";

			$sql .= " AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}


			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				$emailIn[] = $email->email;
				$ids[$email->email] = $email->id;
				if($debug) echo $email->email.' - '.$email->id.'<br/>';

			}
		}///////plus listák

		
		
		////////plus címkék tagjai
		$res = $this->db->query("
				SELECT value AS tag_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'tag'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");
		if($debug) echo '<h1>plus címkék jövő mailok l</h1>';

		foreach ($res as $tag){////////plus címkék, már csakazok kerülnek bele amik még nem kerültek bele (email not in)
			//$emails = $this->db->from('omm_list_members')->where('omm_list_id',$list->list_id)->where('status','active')->notin('email',$emailIn)->get();

			
			
			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_tag_objects c
										WHERE c.tag_id = ".$tag->tag_id."
										  AND m.id = c.object_id 	
										  AND m.status = 'active'
										  AND c.object_type = 'member' 
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailIn as $_email){
				$sql .= "'".$_email."'";

				if($i<sizeof($emailIn)) $sql .=",";

				$i++;
			}
			$sql .=") ";

			$sql .= " AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}


			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				
				if(!in_array($email->email,$emailIn) && !in_array($email->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában és nincs a minuszosok között
					$emailIn[] = $email->email;
					$ids[$email->email] = $email->id;
					if($debug) echo $email->email.' - '.$email->id.'<br/>';
				}					
				

			}
		}///////plus címkék		
		
		
		////////plus teljes termékek tagjai
		$res = $this->db->query("
				SELECT value AS product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'product'  
									   	 AND sign = 'plus' 
									     AND allinlist = 1;  
		");
		if($debug) echo '<h1>plus teljes termékek jövő mailok l</h1>';

		foreach ($res as $prod){////////plus címkék, már csakazok kerülnek bele amik még nem kerültek bele (email not in)
			//$emails = $this->db->from('omm_list_members')->where('omm_list_id',$list->list_id)->where('status','active')->notin('email',$emailIn)->get();

			
			
			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_product_member_connects c
										WHERE c.omm_product_id = ".$prod->product_id."
										  AND m.id = c.omm_list_member_id 	
										  AND m.status = 'active'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailIn as $_email){
				$sql .= "'".$_email."'";

				if($i<sizeof($emailIn)) $sql .=",";

				$i++;
			}
			$sql .=") ";

			$sql .= " AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}


			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				
				if(!in_array($email->email,$emailIn) && !in_array($email->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában és nincs a minuszosok között
					$emailIn[] = $email->email;
					$ids[$email->email] = $email->id;
					if($debug) echo $email->email.' - '.$email->id.'<br/>';
				}					
				

			}
		}///////plus teljes termékek			
		
		////////plus státuszos termékek tagjai
		$res = $this->db->query("
				SELECT value AS status, allinlist as product_id FROM ".$tp."omm_letter_recip_conditions
									   WHERE omm_letter_id = ".$this->id."
									   	 AND type = 'product_stat'  
									   	 AND sign = 'plus' 
									     
		");
	
		if($debug) echo '<h1>plus státuszos termékek jövő mailok l</h1>';

		foreach ($res as $prod){////////plus címkék, már csakazok kerülnek bele amik még nem kerültek bele (email not in)
			//$emails = $this->db->from('omm_list_members')->where('omm_list_id',$list->list_id)->where('status','active')->notin('email',$emailIn)->get();

			
			
			$sql = "SELECT m.email, m.id FROM ".$tp."omm_list_members m, omm_product_member_connects c
										WHERE c.omm_product_id = ".$prod->product_id."
										  AND m.id = c.omm_list_member_id 	
										  AND m.status = 'active'
										  AND c.status = '".$prod->status."'
										  AND m.email NOT IN (";

			$i = 1;
			foreach ($emailIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailIn)) $sql .=",";
				$i++;
			}
			$sql .=") ";
			$sql .= " AND m.email NOT IN (";

			$i = 1;
			foreach ($emailNotIn as $_email){
				$sql .= "'".$_email."'";
				if($i<sizeof($emailNotIn)) $sql .=",";
				$i++;
			}


			$sql .=") GROUP BY m.email ORDER BY m.reg_date ";

			$emails = $this->db->query($sql);

			foreach ($emails as $email){
				
				if(!in_array($email->email,$emailIn) && !in_array($email->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában és nincs a minuszosok között
					$emailIn[] = $email->email;
					$ids[$email->email] = $email->id;
					if($debug) echo $email->email.' - '.$email->id.'<br/>';
				}					
				

			}
		}///////plus statuszos termékek		
		
		
		///////////////

		if($debug) print_r($emailIn);
		if($debug) echo "<br/>";
		if($debug) print_r($emailNotIn);

		if($debug) echo '<h1>plus csoportokból jövő mailok - az előző 4 szürővel</h1>';
		/////////////plus csoportok tagjai
		$plusGroupIds = $this->groupCondArray('plus');

		foreach ($plusGroupIds as $gid){
			$group = ORM::factory('omm_list_group')->find($gid);
			if($group->loaded){
				$_fields = $client->getAllFields($group->omm_list_id);
				$members = $group->getMembers($_fields,'active','reg_date','desc',0,0,false, "all" , "all",  "all", "none",  "none",$_fields);
				foreach ($members as $m){

					if(!in_array($m->email,$emailIn) && !in_array($m->email,$emailNotIn)){ ///csak az megy be ami még nincs fent a listában és nincs a minuszosok között
						$emailIn[] = $m->email;
						$ids[$m->email] = $m->id;
						if($debug) echo $m->email.' - '.$m->id.'<br/>';
					}
				}
			}
		}
		/////////////

		if($debug) echo '<br/><h1>Eredményhalmaz</h1>';

		if($debug) print_r($emailIn);

		if($debug) echo '<br/><h1>Eredményhalmaz id-k</h1>';

		if($debug) print_r($ids);


		$idIn = array();
		foreach ($emailIn as $email){
			if($email != 'x@x.x'){
				$idIn[] = $ids[$email];
			}
		}
			
			

		if($count){


			if(sizeof($idIn)>0){
				$this->db->from('omm_list_members');
				$this->db->in('id', $idIn);


				if($relative && $this->timing_type == 'relative'){

					$day = $this->timing_event_value;

					$rel = date('Y-m-d',strtotime('-'.$day.' day'));

					$this->db->where('activation_date>=', $rel." 00:00:00");
					$this->db->where('activation_date<=', $rel." 23:59:59");

				}

				$cc = $this->db->count_records();

				return $cc;


			}else{
				return 0;
			}


		}else{
			$query = ORM::factory('omm_list_member');


			if(sizeof($idIn)>0){
				$query->in('id', $idIn);


				if(sizeof($idsInclude)){
					$query->in('id', $idsInclude);
				}

				if(sizeof($idsExclude)){
					$query->notin('id', $idsExclude);
				}

				if($relative && $this->timing_type == 'relative'){

					$day = $this->timing_event_value;

					$rel = date('Y-m-d',strtotime('-'.$day.' day'));



					$query->where('activation_date>=', $rel." 00:00:00");
					$query->where('activation_date<=', $rel." 23:59:59");


				}


				return $query->find_all();

			}else{
				return array();
			}


		}








	}

	public function extractText($html){
		$dom = new simple_html_dom();
		$dom->load($html, true);

		return trim($dom->plaintext);
	}

	public function trySetHtml($html){
		$dom = new simple_html_dom();
		$dom->load($html, true);

		$ret = $dom->find('body', 0);
			
		if($ret == null){
			return 0;
		}else{
			$html = str_replace("=iso-8859-2", "=utf-8",$html);
			$html = str_replace("=iso-8859-1", "=utf-8",$html);
			$html = str_replace("=ISO-8859-1", "=utf-8",$html);
			$html = str_replace("=ISO-8859-2", "=utf-8",$html);

			//$html = mb_convert_case($html, MB_CASE_TITLE, "utf-8");

			$mit=array("(õ)","(û)","(Õ)","(Û)","(Í)");
			$mire=array("ő","ű","Ő","Ű","Í");

			$html = preg_replace($mit,$mire,$html);
			//öüóõúéáûí - ÖÜÓÚÉÁÛÍ

			$this->html_content = $html;

			//$this->realhtml_content = $this->getRealHtml();

			return 1;
		}

	}
	/**
	 * @deprecated
	 * Enter description here...
	 * @return unknown_type
	 */
	public function getRealHtml(){
		$clientid = $this->omm_campaign->omm_client->id;
		$dom = new simple_html_dom();

		$dom->load($this->html_content, true);

		foreach($dom->find('img') as $img){

			$src = $img->src;
			url::base();

			$_src = explode("/",$src);

			if(sizeof($_src) == 1){

				if(is_file(DOCROOT.'files/'.$clientid.'/'.$src)){

					$img->src = url::base()."files/".$clientid."/".$src;

				}

			}


		}

		return $dom->save();
	}

	public function getUploadedFiles(){
		$clientid = $this->omm_campaign->omm_client->id;
		//$accid = meta::getAccId(); 
		$accid = "";
		
		fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$clientid);
		 
		$files = fileutils::dir_list(DOCROOT.'files/'.$accid.'/'.$clientid);

		if($this->type == "html" && $this->html_content != ""){
			$dom = new simple_html_dom();
			$dom->load($this->html_content, true);

			foreach($dom->find('img') as $img){
					
				$src = $img->src;
				url::base();
					
				$_src = explode("/",$src);

				if(sizeof($_src) == 1){
					if(!is_file(DOCROOT.'files/'.$clientid.'/'.$src)){
						$files[] = array(   'name' => $src,
                                            'size' => "",
                                            'perm' => "",
                                            'type' => "missing",
                                            'time' => ""
                                            );
					}else{
							
						foreach ($files as $key => $f){

							if($f['name'] == $src){
									
								$files[$key]['type'] = 'valid';
									
							}


						}
							
					}
				}else{
					$files[] = array(   'name' => $src,
                                            'size' => "",
                                            'perm' => "",
                                            'type' => "failed",
                                            'time' => ""
                                            );
				}
					
					
			}

		}
		return $files;
	}


	public function sendjob(){
		$job = ORM::factory('omm_day_job')->where('letter_id',$this->id)->where('type','sendnow')->find();

		if($job->loaded){
			return $job;
		}else{

			$dayjob = ORM::factory('omm_day_job');
			$dayjob = $dayjob->create('sendnow', 0, $this->id);

			return $dayjob;
		}

	}

	/**
	 * csak sendnow és absolute leveleknél van értelme
	 *
	 */
	public function isSent($returnDate = FALSE){

		if($this->status == 'draft') return false;

		if($this->timing_type == 'relative') return false;

		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT j.id, j.day FROM ".$tp."omm_day_job_details d, ".$tp."omm_day_jobs j
						 WHERE j.status = 'sent' 
						   AND j.id=d.omm_day_job_id 
						   AND d.type = '".$this->timing_type."_letter' 
						   AND d.value = '".$this->id."' 
						   ORDER BY j.day DESC
						   ";

		$res = $this->db->query($sql);

		if(sizeof($res)>0){

			if($returnDate){

				return $res[0]->day;
			}else{
				return true;
			}

		}else{

			if($returnDate){
				return NULL;
			}else{
				return false;
			}


		}


	}


	public function unsubscribeClicks(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT c.id FROM ".$tp."omm_jobs j, ".$tp."omm_letter_link_redirections  c
				 WHERE j.id = c.omm_job_id AND j.omm_letter_id = ".$this->id."
				   AND j.type != 'test' AND j.status = 'sent'
				   AND c.clicked is not NULL 
				   AND c.url_name = 'unsubscribe'
				   GROUP BY c.omm_job_id";

		$res = $this->db->query($sql);

		return sizeof($res);
	}

	/**
	 * @deprecated
	 */
	public function letterClicks($unique = true){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT c.id FROM ".$tp."omm_jobs j, ".$tp."omm_letter_link_redirections  c
				 WHERE j.id = c.omm_job_id AND j.omm_letter_id = ".$this->id."
				   AND j.type != 'test' AND j.status = 'sent'
				   AND c.clicked is not NULL 
				   AND c.url_name != 'unsubscribe'";

		if($unique){
			$sql .= "GROUP BY c.omm_job_id";
		}

		$res = $this->db->query($sql);

		return sizeof($res);
	}

	public function letterOpens($unique = true){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		if($unique){
			$sql = "SELECT count(DISTINCT omm_list_member_id) as ossz FROM ".$tp."omm_letter_open_log
						WHERE omm_letter_id = ".$this->id."
						AND opened IS NOT NULL ";				
		}else{
			$sql = "SELECT count(*) as ossz FROM ".$tp."omm_letter_open_log
						WHERE omm_letter_id = ".$this->id."
						AND opened IS NOT NULL ";			
		}


		$res = $this->db->query($sql);

		return $res[0]->ossz;
	}

	public function getDeletedRecipients(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT j.omm_list_member_id as mid, j.email_recipient_email as email
				FROM ".$tp."omm_jobs j  
				 WHERE j.omm_letter_id = ".$this->id."
				   AND j.type != 'test' AND j.status = 'sent'
				   AND (SELECT count(m.id) FROM omm_list_members m WHERE j.omm_list_member_id = m.id) = 0;
		";		

		$res = $this->db->query($sql);

		return $res;
	}
	
	
	
	public function sentMembersToday($date){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT count(j.omm_list_member_id) as ossz
				  FROM ".$tp."omm_jobs j, ".$tp."omm_day_jobs dayj   
				 WHERE j.omm_letter_id = ".$this->id." AND j.omm_day_job_id = dayj.id AND dayj.type='relative' AND dayj.day = '".$date."'
				   AND j.type != 'test' AND j.status = 'sent';
		";
		$res = $this->db->query($sql);

		return $res[0]->ossz;
	}

	public function sentMembers($count = TRUE){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = "SELECT j.omm_list_member_id as mid
				  FROM ".$tp."omm_jobs j  
				 WHERE j.omm_letter_id = ".$this->id."
				   AND j.type != 'test' AND j.status = 'sent';
		";

		$res = $this->db->query($sql);

		if(sizeof($res)>0){

			if($count){
				return sizeof($res);
			}else{

				$ids = array();
				foreach ($res as $r){
					$ids[] = $r->mid;
				}

				$query = ORM::factory('omm_list_member');
				$query->in('id', $ids);
				$members = $query->find_all();

				return $members; ///////itt majd a membereket kell visszaküldeni
			}

		}else{

			if($count){
				return 0;
			}else{
				return array();
			}

		}
			


	}

	/**
	 * @deprecated
	 * Enter description here...
	 * @return unknown_type
	 *
	 * @deprecated
	 */
	public function getLinksForStat(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$return = array();
			
			
		return $return;
	}


	public function getMembersForStatImport($orderby,$order,$offset,$rowperpage){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		$sql .= " l.name as list_name,";		
		$sql .= " j.sent_time as sent_time, ";
		//$sql .= " bounce.dict_id as dict_id, ";
		$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.field_reference='nev') as nev, ";
		$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.field_reference='vezeteknev') as vezeteknev, ";
		$sql .= " (SELECT IFNULL(dd.value,dd.value_text) FROM ".$tp."omm_list_member_datas dd where dd.omm_list_member_id = m.id AND dd.field_reference='keresztnev') as keresztnev, ";
		$sql .= "(SELECT min(opened) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) as open_time, ";
		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) as open, ";
		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NOT NULL) as clicked, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'temp') as temp_error, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'perm') as perm_error, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NULL) as unsubscribed, ";
		$sql .= " m.reg_date as reg_date ";
		$sql .= " FROM   ".$tp."omm_list_members m, ".$tp."omm_jobs j , ".$tp."omm_lists l "    ;
		//$sql .= " ,".$tp."omm_letter_bounces bounce ";
		$sql .= " WHERE  j.omm_list_member_id = m.id AND j.omm_letter_id = ".$this->id." AND m.omm_list_id = l.id ";
		$sql .= "   AND  j.type != 'test' AND j.status = 'sent' ";		
		
		$sql .= " GROUP BY m.id ";
		$sql .= " ORDER BY $orderby $order ";
		$sql .= " LIMIT $offset,$rowperpage";

		//echo $sql;

		return $this->db->query($sql);		
	}	
	
	public function getMembersForStatImport2($orderby,$order,$offset,$rowperpage){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";
		$sql .= " j.sent_time as sent_time, ";
		//$sql .= " bounce.dict_id as dict_id, ";
		$sql .= "(SELECT min(opened) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) as open_time, ";
		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) as open, ";
		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NOT NULL) as clicked, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'temp') as temp_error, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'perm') as perm_error, ";
		$sql .= "(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NULL) as unsubscribed, ";
		$sql .= " m.reg_date as reg_date ";
		$sql .= " FROM   ".$tp."omm_list_members m, ".$tp."omm_jobs j ";
		//$sql .= " ,".$tp."omm_letter_bounces bounce ";
		$sql .= " WHERE  j.omm_list_member_id = m.id AND j.omm_letter_id = ".$this->id." ";
		$sql .= "   AND  j.type != 'test' AND j.status = 'sent' ";		
		
		$sql .= " GROUP BY m.id ";
		$sql .= " ORDER BY $orderby $order ";
		$sql .= " LIMIT $offset,$rowperpage";

		//echo $sql;

		return $this->db->query($sql);		
	}		
	/*
	 * @deprecated
	 */
	public function getMembersBySending($memberStatus,$orderby = "email",$order = "asc" ,$offset = 0,$rowperpage = 0, $all = FALSE){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];



		$sql = "select ";
		$sql .= " m.id as id,";
		$sql .= " m.email as email,";

		if($memberStatus == "temperror" || $memberStatus == "permerror"){
			$sql .= " bounce.dict_id as dict_id, ";
		}

		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) as open, ";

		$sql .= "(SELECT count(id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NOT NULL) as clicked, ";

		$sql .= " m.reg_date as reg_date ";
		$sql .= " FROM   ".$tp."omm_list_members m, ".$tp."omm_jobs j ";

		if($memberStatus == "temperror" || $memberStatus == "permerror"){
			$sql .= " ,".$tp."omm_letter_bounces bounce ";
		}

		$sql .= " WHERE  j.omm_list_member_id = m.id AND j.omm_letter_id = ".$this->id." ";
		$sql .= "   AND  j.type != 'test' AND j.status = 'sent' ";

		if($memberStatus == "opened"){
			$sql .= " AND (SELECT count(id) FROM ".$tp."omm_letter_open_log WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id ) > 0 ";
		}else if($memberStatus == "clicked"){
			$sql .= " AND (SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NOT NULL) > 0 ";
		}else if($memberStatus == "unsubscribed"){
			$sql .= " AND (SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_link_clicks WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND omm_letter_link_id IS NULL) > 0 ";
		}else if($memberStatus == "temperror"){
			$sql .= " AND (SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'temp') > 0 ";
			$sql .= " AND bounce.omm_letter_id = ".$this->id." AND bounce.omm_list_member_id =m.id ";
		}else if($memberStatus == "permerror"){
			$sql .= " AND (SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_bounces WHERE omm_list_member_id = m.id AND omm_letter_id = j.omm_letter_id AND type = 'perm') > 0 ";
			$sql .= " AND bounce.omm_letter_id = ".$this->id." AND bounce.omm_list_member_id =m.id ";
		}

			$sql .= " GROUP BY m.id ";		
			$sql .= " ORDER BY $orderby $order ";
					
		if(!$all){
			$sql .= " LIMIT $offset,$rowperpage";			
		}


		//echo $sql;

		return $this->db->query($sql);
	}


	public function duplicate($new_campaign = NULL){
		/////1 levél adatai

		$new = ORM::factory('omm_letter');
		$new->name = "".$this->name."_másolat";
		
		if($new_campaign != NULL){
			$new->omm_campaign_id = $new_campaign;	
		}else{
			$new->omm_campaign_id = $this->omm_campaign_id;	
		}
		
		$new->type = $this->type;
		$new->subject = $this->subject;
		$new->sender_name = $this->sender_name;
		$new->sender_email = $this->sender_email;
		$new->sender_replyto = $this->sender_replyto;
		$new->html_content = $this->html_content;
		$new->text_content = $this->text_content;
		$new->note = $this->note;
		$new->status = 'draft';
		$new->timing_type = $this->timing_type;
		$new->timing_event_type = $this->timing_event_type;
		$new->timing_event_value = $this->timing_event_value;
		$new->timing_date = $this->timing_date;
		$new->created_date = date("Y-m-d H:i:s");
		$new->saveObject();

		/////címzett feltételek
		$conds = $this->db->from('omm_letter_recip_conditions')->where('omm_letter_id',$this->id)->get();

		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		foreach($conds as $c){

			$sql = "INSERT INTO ".$tp."omm_letter_recip_conditions SET
							omm_letter_id = ".$new->id.",
							type = '".$c->type."',
							value = '".$c->value."',
							sign = '".$c->sign."',
							allinlist = '".$c->allinlist."' ";

			$this->db->query($sql);
		}

	
		return $new;
	}

	public function getLinks(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$sql = " SELECT l.id,
						l.url, 
						l.url_name,
						l.link_number,
						(SELECT count(id) FROM ".$tp."omm_letter_link_clicks WHERE omm_letter_link_id = l.id AND omm_list_member_id != 0) as allclicks,
						(SELECT count(DISTINCT omm_list_member_id) FROM ".$tp."omm_letter_link_clicks WHERE omm_letter_link_id = l.id AND omm_list_member_id != 0 ) as uniqueclicks
						FROM ".$tp."omm_letter_links l 
						WHERE l.omm_letter_id = ".$this->id." 
						AND l.url_name != 'unsubscribe' 
						ORDER BY l.link_number";

		return $this->db->query($sql);
	}

	public function getSystemMessage(){

		if($this->timing_type == "absolute"){
			$date = $this->timing_date;

			if($this->timing_hour != NULL){
				$hour = $this->timing_hour;
			}else{
				$hour = "";
			}

			$recNum = $this->sentMembers();
			$detail = "abszolút";
		}elseif($this->timing_type == "relative"){

			$date = date("Y-m-d");
			$recNum = $this->sentMembersToday($date);
			$hour = "";
			$detail = "relatív";
				
		}


		$preview = 'A "<strong>'.$this->name.'</strong>" nevü '.$detail.' levél sikeresen kiküldve';

		if($this->timing_type == "absolute"){
				
			$body = "<strong>Küldés ideje:</strong> ".dateutils::formatDate($date);
				
			if($hour != "") $body .=", $hour óra";

			$body .= '<br/><strong>Címzettek száma:</strong> '.$recNum."";
				
		}else{
			$preview .= " $recNum címzettnek.";
			$body = "";
		}


		$ret = array("preview" => $preview, "body" => $body);

		return $ret;
	}

	public function getLetterLinkStat(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$return = array();


		$allclicksres = $this->db->query("SELECT count(id) as ossz
											FROM ".$tp."omm_letter_link_clicks 
											WHERE omm_letter_id = ".$this->id." 
											AND omm_letter_link_id IS NOT NULL  AND omm_list_member_id != 0");

		$return['allClicks'] = $allclicksres[0]->ossz;

		$allclicksres_deleted = $this->db->query("SELECT count(id) as ossz
											FROM ".$tp."omm_letter_link_clicks 
											WHERE omm_letter_id = ".$this->id." 
											AND omm_letter_link_id IS NOT NULL AND omm_list_member_id = 0");
		$return['allClicks_deleted'] = $allclicksres[0]->ossz;

		$uniqueres = $this->db->query("SELECT count(DISTINCT omm_list_member_id) as ossz
											FROM ".$tp."omm_letter_link_clicks 
											WHERE omm_letter_id = ".$this->id." 
											AND omm_letter_link_id IS NOT NULL AND omm_list_member_id != 0");

		$return['uniqueClicks'] = $uniqueres[0]->ossz;

		$unsubres = $this->db->query("SELECT count(DISTINCT omm_list_member_id) as ossz
											FROM ".$tp."omm_letter_link_clicks 
											WHERE omm_letter_id = ".$this->id." 
											AND omm_letter_link_id IS NULL AND omm_list_member_id != 0");		

		$return['unsubscribeClicks'] = $unsubres[0]->ossz;

		$return['deletedRecipients'] = $this->getDeletedRecipients();

		$return['sentNumber'] = $this->sentMembers();

		$openres = $this->db->query("SELECT count(DISTINCT omm_list_member_id) as ossz
											FROM ".$tp."omm_letter_open_log 
											WHERE omm_letter_id = ".$this->id." AND omm_list_member_id != 0 ");			

		$return['uniqueOpens'] = $openres[0]->ossz;


		$permErrorres = $this->db->query("SELECT count(DISTINCT b.omm_list_member_id) as ossz
											FROM ".$tp."omm_letter_bounces b, ".$tp."omm_list_members m 
											WHERE b.omm_letter_id = ".$this->id." AND b.type='perm'  AND b.omm_list_member_id != 0 
											AND b.omm_list_member_id = m.id");			

		$return['permErrors'] = $permErrorres[0]->ossz;

		$tempErrorres = $this->db->query("SELECT count(DISTINCT b.omm_list_member_id) as ossz
											FROM ".$tp."omm_letter_bounces b, ".$tp."omm_list_members m 
											WHERE b.omm_letter_id = ".$this->id." AND b.type='temp'  AND b.omm_list_member_id != 0
											AND b.omm_list_member_id = m.id");			

		$return['tempErrors'] = $tempErrorres[0]->ossz;

		if($return['sentNumber'] != 0){
			$return['openratio'] = round((($return['uniqueOpens'] / $return['sentNumber']) * 100 ), 2)."%";
			$return['clickratio'] = round((($return['uniqueClicks'] / $return['sentNumber']) * 100 ), 2)."%";
			$return['unsubratio'] = round((($return['unsubscribeClicks'] / $return['sentNumber']) * 100 ), 2)."%";
			$return['permerrorratio'] = round((($return['permErrors'] / $return['sentNumber']) * 100 ), 2)."%";
			$return['temperrorratio'] = round((($return['tempErrors'] / $return['sentNumber']) * 100 ), 2)."%";
		}else{
			$return['openratio'] = "0%";
			$return['clickratio'] = "0%";
			$return['unsubratio'] = "0%";
			$return['permerrorratio'] = "0%";
			$return['temperrorratio'] = "0%";
		}


		return $return;
	}

	

}
?>