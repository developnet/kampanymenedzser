<?php
/*
* Case of use of Csv.class
* @package csv
* @date 2005-08-01
*/

require("_preload.php");

try {
    $Csv = new Csv($filename = "test.csv", $separator = ";");
    $csvData = $Csv->readAll();

    echo "<table border='1'>";
    foreach($csvData as $row) {
        echo "<tr>";
        foreach($row as $col) {
            echo "<td>$col</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
catch (Exception $e) {
    echo "<hr />";
    echo "Exception code:  <font style='color:blue'>". $e->getCode() ."</font>";
    echo "<br />";
    echo "Exception message: <font style='color:blue'>". nl2br($e->getMessage()) ."</font>";
    echo "<br />";
    echo "Thrown by: '". $e->getFile() ."'";
    echo "<br />";
    echo "on line: '". $e->getLine() ."'.";
    echo "<br />";
    echo "<br />";
    echo "Stack trace:";
    echo "<br />";
    echo nl2br($e->getTraceAsString());
    echo "<hr />";
}


?>