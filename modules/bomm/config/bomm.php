<?php defined('SYSPATH') or die('No direct script access.');

$config['major_version'] = 2;
$config['minor_version'] = 0;
$config['build'] = '0132';

$config['versionstring'] = VERSIONSTRING;

$config['felhasznkezkonyv'] = "kampanymenedzser_felhasznaloi_kezikonyv_v3.pdf";

?>