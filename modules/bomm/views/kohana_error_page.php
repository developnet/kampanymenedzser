<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): 
		$szoftver = "Híreső";
		$email = "ugyfelszolgalat@garbaroyal.hu";
?>
	<link href="/core/views/omm/css/hireso-error.css" media="screen,projection" rel="stylesheet" type="text/css" />
<?php else: 
		$szoftver = "KampányMenedzser";
		$email = "support@kampanymenedzser.hu";
?>
	<link href="/core/views/omm/css/error.css" media="screen,projection" rel="stylesheet" type="text/css" />
<?php endif; ?>
<title><?=$szoftver ?> - hiba</title>
</head>
<body>


<div id="dialogAdditional"></div>
<div id="dialogWrapper">

<div id="dialog">
<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
	<h1 style="padding:0;margin:0;padding-bottom:5px;padding-top:10px;padding-left:1px"><img src="/core/views/omm/img/hireso_logo.png"></h1>
<?php else: ?>
	<h1 style="padding:0;margin:0;padding-bottom:5px;padding-top:10px;padding-left:1px"><img src="/core/views/omm/img/kampanymenedzser_logo.png"></h1>
<?php endif; ?>
<h1>Hiba történt</h1>

<p>
<span>
Idő: <?php echo date("Y.m.d H:i:s") ?><br/>

Felhasználó: <?php echo USERNAME ?><br/>

<h3>Hibaüzenet:</h3>
<br/>
<?php
if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'):
	$trace = str_replace("kampanymenedzser", "hireso", $trace);
	$file = str_replace("kampanymenedzser", "hireso", $file);
endif;

 ?>

 
<?php if ( ! empty($trace)): ?>
<textarea style="width:100%;height:50px;">
<?php echo $message ?>


<?php if ( ! empty($line) AND ! empty($file) ): ?>
<?php echo Kohana::lang('core.error_file_line', $file, $line) ?>
<?php endif ?>

<?php 
echo Kohana::lang('core.stack_trace'); 



	



$tace = str_replace("%body%", "black", "<body text='%body%'>");
echo $trace;
?>
</textarea>

<?php endif ?>


</span>
</p>
<p style="clear:both;height:20px"></p>
</div>



<p class="footer"><?=$szoftver ?> szoftver futása közben hiba történt. Kérjük a hiba minél elöbbi kijavításának érdekében küldje el a hibaüzenetet a <?=$email?> e-mail címre.</p>

</div>
</body>
</html>

<?php /* 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $error ?></title>
<base href="http://php.net/" />
</head>
<body>
<style type="text/css">
<?php include Kohana::find_file('views', 'kohana_errors', FALSE, 'css') ?>
</style>
<div id="framework_error" style="width:42em;margin:20px auto;">
<h3><?php echo html::specialchars($error) ?></h3>
<p><?php echo html::specialchars($description) ?></p>
<?php if ( ! empty($line) AND ! empty($file)): ?>
<p><?php echo Kohana::lang('core.error_file_line', $file, $line) ?></p>
<?php endif ?>
<p><code class="block"><?php echo $message ?></code></p>

<?php if ( ! empty($trace)): ?>
	<h3><?php echo Kohana::lang('core.stack_trace') ?></h3>
	<?php echo $trace ?>
<?php endif ?>

<p class="stats"><?php echo Kohana::lang('core.stats_footer') ?></p>
</div>
</body>
</html>
*/?>