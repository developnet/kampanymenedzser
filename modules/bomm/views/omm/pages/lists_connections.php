
<script>
	$(function(){ 
			
			$('#subscribeAddButton').click(function(){
				
				var contains1 = false;
				
				id = $('#subscribeContainer').val();
				text = $("#subscribeContainer option[value="+id+"]").text();
				

				$('.onthesubscribelist').each(function(){
					if($(this).attr("rel") == id){
						contains1 = true;
					}
					
				});
				
				if(!contains1){
					$('#subscribeList').append('<li rel="'+id+'" class="onthesubscribelist">'+text+' (<a class="deleteList" href="Javascript:;">Töröl</a>)<input type="hidden" value="'+id+'" name="subscribeLists[]" /></li>');	
					deletelist();
				}
				
								
			});
		
			$('#unsubscribeAddButton').click(function(){
				
				var contains = false;
				
				id = $('#unsubscribeContainer').val();
				text = $("#unsubscribeContainer option[value="+id+"]").text();
				

				$('.ontheunsubscribelist').each(function(){
					if($(this).attr("rel") == id){
						contains = true;
					}
					
				});
				
				if(!contains){
					$('#unsubscribeList').append('<li rel="'+id+'" class="ontheunsubscribelist">'+text+' (<a class="deleteList" href="Javascript:;">Töröl</a>)<input type="hidden" value="'+id+'" name="unsubscribeLists[]" /></li>');	
					deletelist();
				}
				
								
			});		
		
			var deletelist = function (){
				$(".deleteList").each(function(){
					
					$(this).click(function(){
						
						$(this).parent().remove();
						
					});
					
				});				
			}
		
			deletelist();
			
			
		var subscribeconf = function(radio){
			val = $('input[@name=subscribe]:checked').val();
			
			if(radio){
				val = radio.val();
			}else{
				val = "<?=$_SESSION['selected_list']->subscribe_connection_type ?>";
			}				
			
			if(val == "only" || val == "all_lists"){
				$("#subscribeDiv").hide();
			}else if(val == "all"){
				$("#subscribeDiv").show();
			}	
		}			

		var unsubscribeconf = function(radio){
			val = $('input[@name=unsubscribe]:checked').val();
			
			if(radio){
				val = radio.val();
			}else{
				val = "<?=$_SESSION['selected_list']->unsubscribe_connection_type ?>";
			}			
			
			if(val == "only" || val == "all_lists"){
				$("#unsubscribeDiv").hide();
			}else if(val == "all"){
				$("#unsubscribeDiv").show();
			}	
		}

		
		subscribeconf(null);
		unsubscribeconf(null);
	
		$('.ss1').click(function(){
			subscribeconf($(this));
		});

		$('.ss2').click(function(){
			unsubscribeconf($(this));
		});		

	});
</script>
<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			Listák összekapcsolása
		</p>
		
		<h1>Listák összekapcsolása</h1>

		<p>Itt beállíthatja, hogy a listához milyen másik listák kapcsolódnak. Feliratkozásnál melyek azok a listák, ahova a felhasználót szintén fel kell iratni illetve leiratkozásnál melyek azok a listák, ahonnan le kell iratkoztatni..</p>

		<form name="unsubscribeForm" action="<?=url::base() ?>pages/listconnections" method="post">
		    
		    <div class="formBG">
		    <div class="formWrapper">
 			
			    <h3 class="topPad">Amennyiben feliratkozik valaki erre a "<?=$listName ?>" listára, történjen a következő:</h3>
    			
			    <p></p>

			    <div class="radioContainer">
				    
				    <div class="radioContainerPad">
				    	<input type="radio" name="subscribe" value="only" class="ss1" <?php echo ($_SESSION['selected_list']->subscribe_connection_type=="only") ? 'checked="checked"' : ''; ?>><label for="only" class="mid"> Csak erre a listára (<?=$listName ?>) iratkoztassa fel (alapbeállítás)</label>
				    </div>

				    <div class="radioContainerPad">
				    	<input type="radio" name="subscribe" value="all_lists" class="ss1" <?php echo ($_SESSION['selected_list']->subscribe_connection_type=="all_lists") ? 'checked="checked"' : ''; ?>><label for="only" class="mid"> Minden másik listára iratkoztassa fel</label>
				    </div>
				    
				    <div class="radioContainerPad">
				    
				    <input type="radio" name="subscribe" value="all" class="ss1" <?php echo ($_SESSION['selected_list']->subscribe_connection_type=="all") ? 'checked="checked"' : ''; ?>><label for="all" class="mid"> A következő listákra is iratkoztassa fel:</label>
                    
                    <div style="display:<?php echo ($_SESSION['selected_list']->subscribe_connection_type=="all") ? 'true' : 'none'; ?>" id="subscribeDiv">
                        
                        <div style="float:left;padding:10px;">
	                        <select name="osszes_fel" size="1" id="subscribeContainer">
	                        	<?php foreach($lists as $l): ?>
									<?php if($_SESSION['selected_list']->id != $l->id): ?>
		                        		<option value="<?=$l->id ?>"><?=$l->name ?></option>
									<?php endif;?>
		                        <?php endforeach; ?>
	                        </select>
                        </div>

                    <div class="mybutton" style="padding:10px 0px 0px 0px;">
                        <button  type="button" class="button" style="padding:3px 11px 3px 8px" id="subscribeAddButton">
                            <img src="<?=$base.$img ?>icons/add.gif" width="16" height="16" alt=""/> 
                            Hozzáadás
                        </button>
                        <div style="clear:both"></div>
                    </div>
                       
                        <div style="float:left;padding:10px;">
	                        <ol id="subscribeList">
	                        	<?php 
	                        	$ids = "";
	                        	foreach($subscribeConnections as $c): 
	                        	?>
	                        		<li rel="<?=$c->relList()->id ?>" class="onthesubscribelist"><?=$c->relList()->name ?> (<a class="deleteList" href="Javascript:;">Töröl</a>)
	                        			<input type="hidden" value="<?=$c->relList()->id ?>" name="subscribeLists[]" />
									</li>
								<?php 
	                        		$ids .= $c->relList()->id.",";
	                        	endforeach; 
	                        	?>
	                        	
	                        	
	                        </ol>
	                        
                        </div>
                                            
                    </div>
                    
                    <div class="clear"></div>
                    
                    </div>
			    </div>
			    <div class="clearButton"></div>
		    </div>		
		    </div>

			<p><br/></p>		
		    <div class="formBG">
		    <div class="formWrapper">
 			
			    <h3 class="topPad">Amennyiben leiratkozik valaki a "<?=$listName ?>" listáról, történjen a következő:</h3>
    			
			    <p></p>

			    <div class="radioContainer">
				    
				    <div class="radioContainerPad">
				    	<input type="radio" name="unsubscribe" value="only" class="ss2" <?php echo ($_SESSION['selected_list']->unsubscribe_connection_type=="only") ? 'checked="checked"' : ''; ?>><label for="all" class="mid"> Csak erről a listáról (<?=$listName ?>) iratkoztassa le (alapbeállítás)</label>
				    </div>

				    <div class="radioContainerPad">
				    	<input type="radio" name="unsubscribe" value="all_lists" class="ss2" <?php echo ($_SESSION['selected_list']->unsubscribe_connection_type=="all_lists") ? 'checked="checked"' : ''; ?>><label for="all" class="mid"> Minden másik listáról iratkoztassa le</label>
				    </div>
				    
				    <div class="radioContainerPad">
				    <input type="radio" name="unsubscribe" value="all" class="ss2" <?php echo ($_SESSION['selected_list']->unsubscribe_connection_type=="all") ? 'checked="checked"' : ''; ?>><label for="only" class="mid"> A következő listákból is vegye ki:</label>
                    
                    <div style="display:<?php echo ($_SESSION['selected_list']->unsubscribe_connection_type=="all") ? 'true' : 'none'; ?>" id="unsubscribeDiv">
                       
                        <div style="float:left;padding:10px;">
	                        <select name="osszes_le" size="1" id="unsubscribeContainer">
	                        	<?php foreach($lists as $l): ?>
									<?php if($_SESSION['selected_list']->id != $l->id): ?>
		                        		<option value="<?=$l->id ?>"><?=$l->name ?></option>
									<?php endif;?>
		                        <?php endforeach; ?>
	                        </select>
                        </div>
                        
                    <div class="mybutton" style="padding:10px 0px 0px 0px;">
                        <button type="button" class="button" style="padding:3px 11px 3px 8px" id="unsubscribeAddButton">
                            <img src="<?=$base.$img ?>icons/add.gif" width="16" height="16" alt=""/> 
                            Hozzáadás
                        </button>
                        <div style="clear:both"></div>
                    </div>
                        
                       
                        <div style="float:left;padding:10px;">
	                        <ol id="unsubscribeList">
	                        	<?php 
	                        		$ids = "";
	                        		foreach($unsubscribeConnections as $c): 
	                        		?>
	                        		<li rel="<?=$c->relList()->id ?>" class="ontheunsubscribelist"><?=$c->relList()->name ?> (<a class="deleteList" href="Javascript:;">Töröl</a>)
									<input type="hidden" value="<?=$c->relList()->id ?>" name="unsubscribeLists[]" /></li>
	                        	<?php 
										$ids .= $c->relList()->id.",";
	                        		endforeach; 
	                        	?>
	                        </ol>
	                        
                        </div>                    
                    </div>
                    
                    <div class="clear"></div>
                    </div>
			    </div>
			 
             <br />
                
                
			    <div class="clearButton"></div>
		    </div>		
		    </div>
		    <?php //?>
		    <p><br/></p>
             <div class="mybutton">
                <button type="submit" class="button" id="saveChangesBtn">
                    <img src="<?=$base.$img ?>buttons/icon-tickcase.gif" width="16" height="16" alt="" />
					Lista összekapcsolásának mentése
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
            </div>
		</form>
		
	</div>
	
	<div id="rightcol">
	
		<div id="options">

	
		</div>
	
	</div>





	
	<div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
