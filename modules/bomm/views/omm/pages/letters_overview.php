<script>
	$(function(){ 
  		$(".letterRow").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				//alert("#client_"+id+"_delete");
				
				$("#letter_"+id+"_delete").show();
				
			});

			$(this).mouseout( function() {
				
				$("#letter_"+id+"_delete").hide();
				
			});
 
 
 		$(".deleteLetter").each(function(){
				$(this).bind('click', function(){
					
					deleteLetter();
					$('#dialog').dialog('open');
					
				});
			});
		
		});

		
	var deleteLetter = function(){ 
			$("#dialog").dialog({
			bgiframe: true,
			resizable: false,
			height:140,
			modal: true,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			},
			buttons: {
				'Levél törlése!': function() {
					$(this).dialog('close');
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			}
		});
	
	}		
		 
	});

	
</script>

<div id="dialog" title="Levél törlése" style="display:none">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span></p>
</div>
<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	<div id="leftcol">
		
	<p class="bread">
		<a href="<?=url::base() ?>pages/campaignoverview">Kampányok</a>
			<span class="breadArrow">&nbsp;</span><?=$_SESSION['selected_campaign']->name?></p>	
				
		
		<h1>Szerkesztés alatt lévő levelek</h1>

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="50%">Szerkesztés alatt lévő levelek</th>
            <!--<th class="cellCenter" nowrap width="300">Kampánynév</th>-->
			<th class="cellCenter" nowrap width="120">Tartalom</th>
			<th class="cellCenter" nowrap width="120" class="cellCenter">Időzítés</th>
			<th class="cellCenter" nowrap width="120" class="cellCenter">Küld. Fel.</th>
            <th class="cellCenter" nowrap width="120" class="cellCenter">Teszt</th>
			<th nowrap width="30" class="headerRight">&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
		
		<?php foreach($drafts as $d):?>
		
			<tr id="0" class="letterRow" rel="<?=$d->id?>">
			    <td class="rowLeft">
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$d->id?>"><?=$d->name ?></a><br />
				    <span>Létrehozva: <?=$d->created()?></span>
			    </td>
	            <!--<td class="cellCenter">Valami kampány</td>-->
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->contentCheck()?>.png" alt=""></td>
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->timingCheck()?>.png" alt=""></td>
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->recipCheck()?>.png" alt=""></td>
	            <td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->testCheck()?>.png" alt=""></td>
				<td class="rowRight"><span id="letter_<?=$d->id?>_delete" style="display:none"><a href="javascript:;" class="deleteLetter" title="Levél törlés" ><img src="<?=$base.$img?>icons/trash.gif" width="10" height="11" alt="Delete"></a></span></td>
			</tr>
		
		
		<?php endforeach; ?>
		
		</table>
		<div class="topPad"></div>
		


        
	    <h1>Aktív Levelek</h1>
	 	<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="sent">
		

		<tr class="noHighlight">
			<th class="headerLeft" width="10">&nbsp;</th>
            <th>Levél neve</th>
			<th>Típusa</th>
            <th>Időzítés</th>
            <th>Küldések</th>
			<th class="cellCenter" width="50">Elküldött</th>
			<th class="cellCenter" width="50">Megnyitás</th>
			<th class="cellCenter" width="50">Átkattintás</th>
            <th class="headerRight cellCenter" width="50">Leiratkozás</th>
		</tr>
		
		<?php foreach($letters as $l):?>
		<tr>
			<td nowrap class="rowLeft" height="40">
            	<img height="9" width="9" border="0" src="<?=$base.$img?>icons/opens-green.gif"/>
            </td>
            <td><a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id?>"><?=$l->name?></a><br /><span>Létrehozva: <?=$l->created()?></span></td>
		    <td><?=$l->type()?></td>
            <td><?=$l->timing()?></td>
            <td><br /><?=$l->sendings()?><br /><br /></td>
			<td class="cellCenter"><?=$l->sentNum()?></td>
			<td class="cellCenter"><?=$l->opened()?></td>
		    <td class="cellCenter"><?=$l->clicked()?></td>
            <td class="rowRight cellCenter"><?=$l->unsubscribed()?></td>
		</tr>		
		<?php endforeach; ?>		
		

		
		</table>
		
	
	</div>
	
	<div id="rightcol">

	
		<div id="options">

			<div class="sidebarbutton"><a href="<?=url::base() ?>pages/letterdetail"><img src="<?=$base.$img?>buttons/create-letter.png" width="207" height="41" alt="Create a new campaign"></a></div>
			
			

			<div class="bghighlight"><h3 class="sidebar">You might also want to...</h3></div>

			<dl class="icon-menu">
			    
				<dt><a href="createsend/dstesting.aspx"><img src="<?=$base.$img?>icons/wand-small.png" width="16" height="16" alt="Run a design and spam test" /></a></dt>
				<dd><a href="createsend/dstesting.aspx" id="dstest">Run a design and spam test</a></dd>
				<dd class="last">Test your design in major email clients.</dd>

				
				<dt><a href="subscribers/"><img src="<?=$base.$img?>icons/subscriber-go.png" width="16" height="16" alt="Manage your subscribers" /></a></dt>
				<dd><a href="subscribers/" id="subscribers">Manage your subscribers</a></dd>
				<dd class="lastBig">Add subscribers, change settings, etc.</dd>
			</dl>
			
			

		</div>
	
	</div>
	
	<div class="clear"></div>
	
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->