<?php


if(isset($_SESSION['selected_campaign'])){
	$selected = $_SESSION['selected_campaign']->id;
	$selected_stat = $_SESSION['selected_campaign']->status;
	$name = $_SESSION['selected_campaign']->name;
}else{
	$selected = 0;
	$selected_stat = 'none';
	$name = "";
}

?>
<script>
	$(function(){ 

	
 		$(".subFolderNotEmpty").each(function(){
				$(this).bind('click', function(){
					
					$(".letter-container").hide();
					
					var rel = $(this).attr('rel');								
						
					$('#letters-'+rel).show();
					$('#letters-'+rel+'-sent').show();
					
				});
			});	
	
 		$(".subFolder").each(function(){
				$(this).bind('click', function(){
					
					$(".letter-container").hide();
					
					var rel = $(this).attr('rel');								
						
					$('#letters-'+rel).show();
					$('#letters-'+rel+'-sent').show();
					
				});
			});		
	
	
		$('.archive_open_click').click(function(){
			$('.archive_camp').toggle();
			$('#archive_close').toggleClass("open");
		});
	
		<?php if($selected_stat == 'archived'): ?>

		$('.archive_camp').toggle();
		$('#archive_close').toggleClass("open");

		<?php endif;?>////	
		 
	});



	
</script>







		<?php 
			if(isset($_GET['sort'])){
				$sort = $_GET['sort'];
				$cookie_params = array(
				               'name'   => 'statistic_view_sort',
				               'value'  => $sort,
				               'expire' => '0',
				               'domain' => USERNAME.'.kampanymenedzser.hu',
				               'path'   => '/'
				               );
				cookie::set($cookie_params);					
			}else{
				$sort = cookie::get("statistic_view_sort", "name", TRUE);
			}
			
			if(isset($_GET['order'])){
				$order = $_GET['order'];
				$cookie_params = array(
				               'name'   => 'statistic_view_order',
				               'value'  => $order,
				               'expire' => '0',
				               'domain' => USERNAME.'.kampanymenedzser.hu',
				               'path'   => '/'
				               );
				cookie::set($cookie_params);				
			}else{
				$order = cookie::get("statistic_view_order", "asc", TRUE);
			}		
		?>

<!-- CONTENT -->
    <div id="dashboard">
		<div id="kampanyList">

	        <div class="mybutton" style="float:right;margin-bottom:8px;">    
	            <a href="<?=url::base() ?>pages/campaignoverview/newcampaign" class="button slim">
	                <img src="<?=$base.$img?>icons/add.gif" alt=""/> 
	                Új Kampány létrehozása
	            </a>
	        </div>        
			<div class="clear"></div>

			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tr class="noHighlight">
				<th class="headerLeft" width="100%" colspan="2"><span>Kampányok listája</span></th>
				<th nowrap>&nbsp;</th>
                <th class="headerRight" style="padding-right:0px;" nowrap>&nbsp;</th>
			</tr>
			
			<?php foreach ($campaigns as $c): 
				$all = $c->sentLetterNum();
			
			if($c->id == $selected){
				
				$relative = $c->relative();
				
				$absLetters = $c->absoluteLetters($sort,$order);
				$absolute = 0;
				
				foreach ($absLetters as $l){
					if($l->isSent()) $absolute++;
				}
				
				$sendnowLetters = $c->sendnowLetters();
				$sendnow = 0;
			
				foreach ($sendnowLetters as $l){
					if($l->isSent()) $sendnow++;
				}			
			
			?>

			<tr class="dashRowActive">
				
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/statisticoverview/deselect/<?=$c->id ?>"><img src="<?=$base.$img?>icons/minusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/statisticoverview/deselect/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
                
				<td nowrap width="13" style="padding-right:0px;">
                </td>
				<td nowrap width="10">
                </td>
			</tr>
			
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($relative > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="relative">Relatív levelek (<?=$relative?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder" rel="relative">Relatív levelek</a>
                	<?php endif; ?>				
				
                </td>
			</tr>                                    
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($absolute > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="absolute">Abszolút levelek (<?=$absolute?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="absolute">Abszolút levelek</a>
                	<?php endif; ?>						
				
                </td>
			</tr>
	
			
			<?php }else{ ///////////////csukott?>
			
			<tr class="dashRow"> 
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/statisticoverview/selectcampaign/<?=$c->id ?>"><img src="<?=$base.$img?>icons/plusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/statisticoverview/selectcampaign/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
				<td nowrap width="13" style="padding-right:0px;">
                </td>
				<td nowrap width="10">
                </td>
			</tr>			
			
			<?php } ?>
			
			
			
			
			<?php endforeach; ?>
			

			</table> 
			<div class="topPad"></div>
			
			<div class="topPad"></div>
	
				
	
			<table cellpadding="0" cellspacing="0" width="100%" >
			
				<tr class="noHighlight">
					<th class="headerDarkGreyLeft" width="" colspan="2"><span class="archive_open_click">Archivált kampányokok listája (<?=sizeof($archived_campaigns)?>)</span> </th>
					<th nowrap class="headerDarkGreyCenter"  class="archive_open_click">&nbsp;</th>
		            <th class="headerDarkGreyRight" style="" nowrap><div  class="archive_open_click" id="archive_close"></div></th> 
				</tr>
				<!-- ARCHIVÁLT -->
	
			<?php foreach ($archived_campaigns as $c): 
				$all = $c->letterNum();
			
			if($c->id == $selected){
				
				$drafts = $c->draftLetterNum();
				
				$relative = $c->relative();
				$absolute = $c->absolute();
				//$sendnow = $c->sendnow();				
			

			
			?>

			<tr class="dashRowActive archive_camp">
				
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/statisticoverview/deselect/<?=$c->id ?>"><img src="<?=$base.$img?>icons/minusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/statisticoverview/deselect/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
                
				<td nowrap width="13" style="padding-right:0px;"></td>
				<td nowrap width="10">
                
                </td>
			</tr>
			
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($drafts > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="drafts">Szerkesztés alatt lévő levelek (<?=$drafts?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="drafts">Szerkesztés alatt lévő levelek</a>
                	<?php endif; ?>
                	
                </td>
			</tr>            
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($relative > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="relative">Relatív levelek (<?=$relative?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder" rel="relative">Relatív levelek</a>
                	<?php endif; ?>				
				
                </td>
			</tr>                                    
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($absolute > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="absolute">Abszolút levelek (<?=$absolute?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="absolute">Abszolút levelek</a>
                	<?php endif; ?>						
				
                </td>
			</tr>

		
			
			<?php }else{ ///////////////csukott?>
			
			<tr class="dashRow archive_camp"> 
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/statisticoverview/selectcampaign/<?=$c->id ?>"><img src="<?=$base.$img?>icons/plusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/statisticoverview/selectcampaign/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
				<td nowrap width="13" style="padding-right:0px;">          </td>
				<td nowrap width="10">

                </td>
			</tr>			
			
			<?php } ?>
			
			
			
			
			<?php endforeach; ?>
			
			<tr class="dashRow">
				
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="">&nbsp;</td>
				<td nowrap width="13" style="padding-right:0px;">&nbsp;</td>
				<td nowrap width="10">&nbsp;</td>
			</tr>
			</table> 				
				
				<!-- ARCHIVÁLT -->			
			<div class="clear"></div>
			
		</div> <!-- kampanyList end-->
		
        <div id="kampanyActivity">
			<div id="activityBG"> <!--Elválasztó BG-->
				<div id="activityContent">

		<?php if(!isset($_SESSION['selected_campaign'])): ?>
		
		<!--<h1>Válasszon kampányt</h1>-->

                <div style="text-align:center;height:400px">
                <?php if(sizeof($campaigns) == 0):?>
                	<h3 style="margin-top:60px;color:#666666">Nincsenek aktív kampányai, <a href="/pages/campaignoverview/newcampaign">itt</a> létrehozhat egyet.</h3>
                <?php else:?>
                	<h3 style="margin-top:60px;color:#666666">Kattintson az egyik kampányra a levelek listázásához.</h3>
                <?php endif;?>
                </div> 

		<?php else: 
			$campaign = $_SESSION['selected_campaign'];
		?>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// ?>
			
		<div style="height:30px;margin-bottom:6px">
		
			<div id="toolbar_first" style="text-align:right;float:left;width:300px">			
			
			</div>
			
			<div id="toolbar" style="text-align:right;float:right;width:200px;padding-top:8px;height:30px">
				
						Rendezés
				<?php if($sort=="name"): //?>
						<a href="<?=url::base() ?>pages/statisticoverview?sort=time&order=<?=$order ?>" > idő </a> / 
						<strong>név</strong>  szerint
				<?php else:?>
						<strong>idő</strong> / 
						<a href="<?=url::base() ?>pages/statisticoverview?sort=name&order=<?=$order ?>" > név </a>
						 szerint			
				<?php endif;?>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
				<?php if($order=="asc"): ///?>
						
						<a href="<?=url::base() ?>pages/statisticoverview?sort=<?=$sort ?>&order=desc" ><img src="<?=$base.$img?>icons/sort-asc.png" alt=""/></a>
				<?php else:?>
						<a href="<?=url::base() ?>pages/statisticoverview?sort=<?=$sort ?>&order=asc" ><img src="<?=$base.$img?>icons/sort-desc.png" alt=""/></a> 
									
				<?php endif;?>
				
				
			</div>		
		
		</div>
		
		
		<div id="letters-relative" class="letter-container">
		
		<!--<h1>Relatív levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="45%">Relatív levelek</th>
			<th class="cellCenter" width="15%">Relatív idő</th>
			<th class="cellCenter" width="10%">Címzet.</th>
            <th class="cellCenter" width="10%">Megny.</th>
            <th class="headerRight cellCenter" width="10%">Átkatt.</th>
		</tr>
		
		<?php foreach($campaign->relativeLetters($sort,$order) as $l):
				$stat = $l->getLetterLinkStat();
			?>

			<tr class="letterRow" >
			    <td class="rowLeft">
				    <a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a> <span>(<?=strtoupper($l->type) ?>)</span>
			    </td>
				<td class="cellLeft"><span><?=$l->timing_event_value ?>. nap</span></td>
				<td class="cellRight"><span><?=$stat['sentNumber'] ?></span></td>
				
				<?php if($l->type == "html"): ?>
				
                <td class="cellRight"><span><?=$stat['uniqueOpens'] ?> (<?=$stat['openratio'] ?>)</span></td>
                <td class="cellRight"><span><?=$stat['uniqueClicks'] ?> (<?=$stat['clickratio'] ?>)</span></td>
                
                <?php else: ?>

                <td class="cellCenter" colspan="2"><span>nem mérhető</span></td>
                
                <?php endif; ?>
                
			</tr>
		
		<?php endforeach; ?>
			
		<?php if($campaign->relative() == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs relatív levél.</span></td>
			</tr>
		<?php endif; ?>
				
         </table>
		
		</div>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// ?>

		
		<div id="letters-absolute-sent" class="letter-container">
		
		<!--<h1>Abszolút levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="45%">Abszolút levelek</th>
			<th class="cellCenter" width="15%">Kiküldve</th>
			<th class="cellCenter" width="10%">Címzet.</th>
            <th class="cellCenter" width="10%">Megny.</th>
            <th class="headerRight cellCenter" width="10%">Átkatt.</th>
		</tr>
			
		<?php 
			$i=0;
			
			foreach($campaign->absoluteLetters($sort,$order) as $l):?>
			
			<?php if($l->isSent()): 
				$stat = $l->getLetterLinkStat();
			?>	
			<tr class="letterRow" >
			    <td class="rowLeft">
				    <a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a> <span>(<?=strtoupper($l->type) ?>)</span>
			    </td>
				
				<td class="cellLeft"><span><?=$l->getTimingDate() ?></span></td>
				<td class="cellRight"><span><?=$stat['sentNumber'] ?></span></td>
				
				<?php if($l->type == "html"): ?>
				
				
                <td class="cellRight"><span><?=$stat['uniqueOpens'] ?> (<?=$stat['openratio'] ?>)</span></td>
                <td class="cellRight"><span><?=$stat['uniqueClicks'] ?> (<?=$stat['clickratio'] ?>)</span></td>
                
                <?php else: ?>

                <td class="cellCenter" colspan="2"><span>nem mérhető</span></td>
                
                <?php endif; ?>
			</tr>
			<?php 
				$i++;	
				endif; 
			?>
		<?php endforeach; ?>
			
		<?php if($i == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs elküldött abszolút levél.</span></td>
			</tr>
		<?php endif; ?>			
		
         </table>
		
		</div>
		
<?php //////////////////////////////////////////////////////////////////////////////// ?>

		
		<div class="topPad"></div>


		<?php endif; ?>
                    
                    

                    
                    
					<div class="clearActivity"></div>
				</div>
			</div>
					
		</div> <!-- clientActivity end-->
		<div class="clear"></div>

		
	</div> <!-- Dashboard VÉGE ********************************************************************************************************************-->

<!-- CONTENT VÉGE ******************************************************************************************************************************-->

