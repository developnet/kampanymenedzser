<script>
	$(function(){ 

		$("#messageLimit").change(function(){
			var limit = $(this).val();

			window.location = '<?=url::base() ?>pages/clientoverview?limit='+limit;
			
			});
		
		$(".message_show_body").each(function(){
			$(this).bind('click', function(){
				var mid = $(this).attr('rel');
				$("#"+mid).slideToggle('slow');
			});
		});
		
		var SELECTED_CLIENT = 0
	
  		$(".dashRow").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				//alert("#client_"+id+"_delete");
				
				$("#client_"+id+"_delete").show();
				
			} );

			$(this).mouseout( function() {
				
				$("#client_"+id+"_delete").hide();
				
			} );
 
 

 		$(".deleteClient").each(function(){
				$(this).bind('click', function(){
					SELECTED_CLIENT = $(this).attr('rel');
					$(".selectionName").html($(this).attr('relName'));
					deleteList();
								
				});
			});
		
		});

		
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteList = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/clientoverview/deleteClient/'+SELECTED_CLIENT;
					
	});	
		

		 
	});


	
	
</script>



<div id="dialog" title="Honlap törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" honlapot?</strong><br />
	A honlap és az alá tartozó listák, kampányok összes adata törlődni fog!
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Honlap törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" honlapot?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	

    <div id="dashboard">
        
		<?=$alert ?>        
        
		<div id="clientList">

       
        <h1>
        
        <?php if(sizeof($clients) >= KOHANA::config('core.max_client_per_user')): ?>
        
        
        <?php else:?>
        <div class="mybutton" style="float:right;margin-bottom:8px;">    
            <a href="<?=url::base() ?>pages/clientadd" class="guibutton add" id="">Új honlap hozzáadása</a>
        </div>        
        <?php endif;?>

        
                
        Honlapok
        </h1>
        <div class="clear"></div>
        
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tr class="noHighlight">
				<th class="headerLeft" width="100%"><span>Honlapok listája</span></th>
				<th class="headerRight" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</tr>
			
			<?php foreach ($clients as $client): ?>
			
				<tr class="dashRow" id="client_<?=$client->id ?>" rel="<?=$client->id ?>">
					<td width="100%" class="rowLeft">
						<strong><a href="<?=url::base() ?>pages/clientoverview/selectclient/<?=$client->id ?>" class="clientName" <?php echo (empty($client->note)) ? "" : 'title="'.nl2br($client->note).'"'; ?>><?=$client->name ?></a></strong>
					</td>
					<td nowrap class="rowRight">
						<span class="deleteClient" id="client_<?=$client->id ?>_delete" rel="<?=$client->id ?>" relName="<?=$client->name ?>" style="display:none">
						
							<a href="#" title="Honlap törlése" >
								<img src="<?=$base.$img ?>/icons/trash.png" width="10" height="11" alt="Törlés">
						 	</a>
						</span>
					</td>
				</tr>
			
			<?php endforeach; ?>


		<?php if(sizeof($clients) == 0): ?>
			<tr >
			
			    <td class="rowLeft" colspan="2" align="center">Még nincs honlap felvéve. <a href="<?=url::base() ?>pages/clientadd">Honlap hozzáadása</a></td>
			    
			</tr>		
		<?php endif; ?>

			
			</table> 
			<div class="topPad"></div>
		</div> <!-- clientList end-->
		<?php 
			if(isset($_GET['limit'])){
				$limit = $_GET['limit'];
				$cookie_params = array(
				               'name'   => 'message_view_limit',
				               'value'  => $limit,
				               'expire' => '0',
				               'domain' => USERNAME.'.kampanymenedzser.hu',
				               'path'   => '/'
				               );
				cookie::set($cookie_params);				
			}else{
				$limit = cookie::get("message_view_limit", 10, TRUE);
			}
			
			$messages = ORM::factory("omm_message")->orlike(array('recipient' => 'all'))->orlike(array('recipient' => ''.meta::getKMuser().''))->find_all($limit,0);
			
		?>
		
        <div id="clientActivity">
			<div id="activityBG">
			 	<div id="activityContent">
             	
					<h1 style="line-height:30px">Üzenetek <span style="margin-left:10px;font-size:10px;font-weight:normal">Az üzenet részleteinek olvasásához kattintson az üzenet sorára.</span></h1>
					<table width="100%" cellspacing="0" cellpadding="0">
						<tbody>
						<tr class="noHighlight">
							<th class="headerDarkGreyLeft" style="padding:6px 6px 10px 10px;">
								<span>
								A legfrisebb
								<select name="messageLimit" id="messageLimit" style="padding:0;margin:0;">
									<option value="10" <?php echo ($limit == 10) ? 'selected="selected"':'';?>>10</option>
									<option value="50" <?php echo ($limit == 50) ? 'selected="selected"':'';?>>50</option>
									<option value="100" <?php echo ($limit == 100) ? 'selected="selected"':'';?>>100</option>
								</select>
								db üzenet megjelenítése
								</span>
							</th>
							<th class="headerDarkGreyRight"></th>
						</tr>
						</tbody>
					</table>
				
					<table class="activity" width="100%" cellspacing="0" cellpadding="0">
						<tbody>
						<?php foreach($messages as $m):?>
						<tr class="message_show_body" rel="message_<?=$m->id?>" style="cursor:pointer">
							<td class="activityType" nowrap="" valign="top">
								
								<?php if($m->type == "news"):?>
									<div class="activityNews" style="margin-top:5px">HÍREK</div>
								<?php elseif($m->type == "system"):?>
									<div class="activitySent" style="margin-top:5px">RENDSZER</div>
								<?php elseif($m->type == "alert"):?>
									<div class="activityAlert" style="margin-top:5px">FONTOS</div>	
								<?php endif;?>
								
							</td>
							<td class="activityAction" width="100%" valign="top">

								<?=$m->preview?>
								<?php if($m->body != ""):?>
								<div style="display:none;" id="message_<?=$m->id?>" class="messageBodyContainer">
									<?=$m->body?>
								</div>
								<?php endif;?>
							</td>
							<td class="activityTime" nowrap="" valign="top"><div><?=dateutils::formatDate($m->date)?></div></td>
						</tr>
						<?php endforeach;?>
						
					<?php if(sizeof($messages) == 0): ?>
						<tr >
						    <td class="rowLeft" colspan="2" align="center">Nincsenek üzenetek.</td>
						</tr>		
					<?php endif; ?>						
						
						</tbody>
					</table>
				
				<div class="clearActivity"/></div>
                
				</div>
            </div>
		</div> <!-- clientActivity end-->
	
		<div class="clear"></div>

		
	</div> <!-- Dashboard VÉGE -->

	
	
	


	
	
	