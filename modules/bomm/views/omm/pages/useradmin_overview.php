<?php 
$gomb = "Előfizetői profil mentése";
$title = 'Felhasználók kezelése';
?>

<script>
	$(function(){ 
	
		var SELECTED_TEMPL = 0;
	
  		$(".listRow").each(function(){ 
  			
			var id = $(this).attr("rel");

			//alert(id);

			$(this).mouseover( function() {

				$("#templ_"+id+"_delete").show();
				
			} );

			$(this).mouseout( function() {
				
				$("#templ_"+id+"_delete").hide();
				
			} );
 
 

 		$(".deleteTempl").each(function(){
				$(this).bind('click', function(){
					SELECTED_TEMPL = $(this).attr('rel');
					$('.selectionName').html($(this).attr('relName'));
					deleteTempl();
								
				});
			});
		
		});

		
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
			}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteTempl = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/useradmin/deleteuser/'+SELECTED_TEMPL;
					
	});	
		
		 
	});

	
</script>

<style>

.clearfix label{
	width:120px;
}

</style>

<div id="dialog" title="Sablon törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" felhasználót?</strong><br />
	  </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Honlap törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" felhasználót?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?>
		
			        <div class="mybutton" style="float:right;padding-bottom:8px;">    
			            <a href="<?=url::base() ?>pages/useradmin/add" class="button">
			                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
			               Új felhasználó hozzáadása
			            </a>
			            <div style="clear:both"></div>
			        </div>		
		
		</h1>
		
		
			<?=$errors ?>
			<?=$alert ?>
			
	 
    
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft cellLeft" width="30%">Név</th>
			<th nowrap="nowrap" width="40%" class="cellCenter">E-mail<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
			<th nowrap="nowrap" width="20%" class="cellCenter">Felhasználónév<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
			<th nowrap="nowrap" width="20%" class="cellCenter">Jogosultság<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>			
			<th nowrap="nowrap" width="10%" class="headerRight"><br /><img src="<?=$base.$img ?>_space.gif" width="30" height="1" alt=""></th>            
		</tr>


		<?foreach ($users as $user): ?>
		
			<tr id="<?=$user->id ?>" class="listRow" rel="<?=$user->id ?>">
			
			    <td class="rowLeft"><a href="<?=url::base() ?>pages/useradmin/selectuser/<?=$user->id ?>"><?=$user->lastname." ".$user->firstname ?></a></td>
	            <td class="cellLeft"><span><?=$user->email?></span></td>
	            <td class="cellLeft"><span><?=$user->username?></span></td>
	
				<?php 
					$accadmin = false;
					foreach($user->roles as $r){
						if($r->name == Admin_Controller::$accadmin_role){
							$accadmin = true;
							break;
						}
					}
					
					if($accadmin):
				?>
	
				<td class="cellLeft"><span>Admnisztrátor</span></td>
				<td class="dashRow"></td>
				
				<?php else:?>
				<td class="cellLeft"><span>Felhasználó</span></td>
	
    		    <td class="dashRow" rel="<?=$user->id ?>">
			    	<span class="deleteTempl" rel="<?=$user->id ?>" relName="<?=$user->lastname." ".$user->firstname ?>"id="templ_<?=$user->id ?>_delete" style="display:none">
			    		<a href="#" title="Felhasználó törlése" >
			    			<img src="<?=$base.$img ?>icons/trash.gif" width="10" height="11" alt="Delete"></a>
			    	</span>
			    </td>				
				<?php endif;?>

			    
			</tr>
		
		<?endforeach; ?>
		
		<?php if(sizeof($users) == 0): ?>
			<tr >
			
			    <td class="rowLeft" colspan="3" align="center">Még nincs felhasználó. <a href="<?=url::base() ?>pages/useradmin/add">Ide kattintva megteheti hozzáadhat.</a></td>
			    
			</tr>		
		<?php endif; ?>
		
		<tr class="noHighlight">
			<td class="footerLeft simple" >&nbsp;</td>
			<td class="footerMiddle" align="center"><strong>&nbsp;</strong></td>
			<td class="footerMiddle" align="center"><strong>&nbsp;</strong></td>
			<td class="footerMiddle" align="center"><strong>&nbsp;</strong></td>
			<td class="footerRight simple">&nbsp;</td>
		</tr>
		</table>
     
        
	

	
    <div id="rightcol">

				<div id="options">


					<!--<div class="bghighlight"><h3 class="sidebar"> Hasznos tanácsok </h3></div>-->


						<div style="clear:both"></div>
	

						</div>
	


				</div>


	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
