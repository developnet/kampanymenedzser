<style>
	.formField{
		cursor:move;
	}
	
	.formFieldCheck{
		width:60px;float:left;text-align:center;
	}
</style>
<script>
	
	var id = 0;
	
	$(function(){
		
		var initType = function(name){


				$("#formFieldList").sortable(	{
						activeclass : 'sortableactive',
						hoverclass : 'sortablehover',
						axis : 'vertically',
						opacity: 	0.5,
						fit :	false,
						stop: serializeOptions
				});
				
				serializeOptions();
				
		}
		
		var serializeOptions = function(){
			
			deleteLinks();
			
		}
		
	
		$("#addButton").click( function(){
		
			referenceId = $("#fieldToAdd").val();
			
			refId = referenceId.split('-');
			
			reference = refId[0];
			id = refId[1];
			
		
			text = $("#fieldToAdd option[value="+referenceId+"]").text();
			
			if(reference != ""){
				
				delLink = '<a href="javascript:;" class="deleteOption" ><img height="10" width="10" class="supporting" alt="Törlés" src="<?=$base.$img ?>icons/smallOff.png"/></a>';
								
				$("#formFieldList").append('<li class="formField"><div class="formFieldCheck"><input type="checkbox" name="'+reference+'_required" value="1" /><input type="hidden" name="'+reference+'_id" value="'+id+'"/><input type="hidden" name="formfields[]" value="'+reference+'"/></div><label class="mid">'+text+'</label>'+delLink+'</li>');
				
				
				serializeOptions();
				
			}else{
				alert("Kérem adja meg a nevet.")
			}
			
			
		});
		
		var deleteLinks = function(){
			$('.deleteOption').each(function(){
				$(this).click(function(){
					$(this).parent().remove();
				});					
			});
		}
		
		
		
		
		initType();
		
		
		$('#generatedCode').click(function(){
			
			$(this).select();
		
		});
		
	});

</script>
<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/formoverview">Űrlapok karbantartása</a>
			<span class="breadArrow">&nbsp;</span><?=$formName ?>
		</p>
		
		<h1>Űrlap szerkesztése</h1>
		<h3><?=$formName ?></h3>
		
		<?=$errors ?>
		
		
		<p class="extraBottomPad"></p>
		<div id="subscribeFormBuilder">
		
		<form name="subscribeForm" id="subscribeForm" action="<?=url::base() ?>pages/formdetail" method="post">
		
		<div class="formBG">
			
			<div class="formWrapper">

			<h3>Lista mezőinek kiválasztása</h3>
			<p>Adja meg, hogy melyik mezőket szeretné szerepeltetni az űrlapon.</p>

			
            <div class="formContainer">		
				<label style="width:240px;padding:0">Legyen ez az adatmódosító úrlap?</label>
				<input id="listName" name="adatmodForm" type="checkbox" size="50" value="1" class="<?=$classes['name'] ?> input_text" align="middle" <?echo ($datamodForm) ? 'checked="checked"' :"";?>/>
			</div>
			
            <div class="formContainer">		
				 <div class="clearfix">
					<label style="width:120px;">Űrlap megnevezése</label>
					<input id="listName" name="name" type="text" size="50" value="<?=$form['name'] ?>" class="<?=$classes['name'] ?> input_text" />
				</div>
				
                <div class="clearfix">
					<label   style="width:120px;" class="middle">Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>					
				
			</div>

            <div class="formContainer">		
				<label style="width:120px;">Küldés gomb felirata</label>
				<input id="listName" name="submit_button" type="text" size="50" value="<?=$form['submit_button'] ?>" class="<?=$classes['submit_button'] ?> input_text" />
			</div>            
            
            <div class="formContainer">		
				<label style="width:120px;">Küldés gomb színe</label>
				 
				<select id="form_button_styles" name="form_button_styles" class="<?=$classes['form_button_styles'] ?> input_text">
				<?php foreach($form_button_styles as $key =>$templ):?>
					<?php if($form['form_style'] == $key):?>
						<option value="<?=$key ?>" selected="selected"><?=$templ ?></option>
					<?php else:?>
						<option value="<?=$key ?>"><?=$templ ?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			</div>             

            <div class="formContainer">		
				<label style="width:120px;">Az űrlap elrendezése</label>
				
				<select id="form_template" name="form_template" class="<?=$classes['form_template'] ?> input_text">
				<?php foreach($form_templates as $key =>$templ):?>
					<?php if($form['form_template'] == $key):?>
						<option value="<?=$key ?>" selected="selected"><?=$templ ?></option>
					<?php else:?>
						<option value="<?=$key ?>"><?=$templ ?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			</div>    
           
            <div class="formContainer">		
				<label style="width:120px;">Az űrlap stílusa</label>
				
				<select id="form_template" name="form_style" class="<?=$classes['form_style'] ?> input_text">
				<?php foreach($form_styles as $key =>$templ):?>
					<?php if($form['form_style'] == $key):?>
						<option value="<?=$key ?>" selected="selected"><?=$templ ?></option>
					<?php else:?>
						<option value="<?=$key ?>"><?=$templ ?></option>
					<?php endif;?>
				<?php endforeach;?>
				</select>
			</div> 
            
            <div class="radioContainer">
            	<div class="radioContainerPad">
				<h3 style="margin-top:0px;">Űrlapon szereplő mezők</h3>
            	<div style="width:60px;float:left;text-align:center;">
            		Kötelező
            	</div>

                <div class="clear"></div>
                </div>
				
				<ol class="radioContainerPad" id="formFieldList">
                	
					<?php if(sizeof($formFields) == 0): ?>
                	<li class="formField">
	                	<div class="formFieldCheck">
							<input type="checkbox" name="email_required" value="1" checked="checked" disabled="disabled" />
							<input type="hidden" name="email_id" value="0"/>
							<input type="hidden" name="formfields[]" value="email"/>
	                    </div>
	                    <label class="mid"> Email cím</label>
					</li>
					<?php endif; ?>
					
					<?php if(isset($_POST['formfields']))://////////////////////////////ha van post?>
					
					<?php foreach($_POST['formfields'] as $f): ?>
					
	                	<li class="formField">
		                	<div class="formFieldCheck">
								<?php if(isset($_POST[$f."_required"]) && $_POST[$f."_required"] == "1"): ?>
									<input type="checkbox" name="<?=$f?>_required" value="1" checked="checked" />
								<?php else: ?>
									<input type="checkbox" name="<?=$f?>_required" value="1" />
								<?php endif; ?>
								
								<input type="hidden" name="<?=$f?>_id" value="<?=$_POST[$f."_id"] ?>"/>
								<input type="hidden" name="formfields[]" value="<?=$f ?>"/>
		                    </div>
		                    <label class="mid">
		                    <?php  
								foreach($fields as $ff){
									if($ff->reference == $f){
										echo $ff->name;
										break;
									}elseif($f == "email"){
										echo "E-mail";
										break;
									}
								}
		                    
		                    ?>
		                    </label>
		                    
		                    <?php if($f != 'email'): ?>
		                    	<a href="javascript:;" class="deleteOption" ><img height="10" width="10" class="supporting" alt="Törlés" src="<?=$base.$img ?>icons/smallOff.png"/></a>
		                    <?php endif; ?>
		                
                        </li>					
						
					
					<?php endforeach; ?>
					
					
					
					
					<?php else://////////////////////////ha nincs POST?>
					
					<?php foreach($formFields as $f): ?>
					
	                	<li class="formField">
		                	<div class="formFieldCheck">
								<?php if((bool)$f->required): ?>
									<input type="checkbox" name="<?=$f->field_reference ?>_required" value="1" checked="checked" />
								<?php else: ?>
									<input type="checkbox" name="<?=$f->field_reference ?>_required" value="1" />
								<?php endif; ?>
								
								<input type="hidden" name="<?=$f->field_reference ?>_id" value="<?=$f->omm_list_field_id ?>"/>
								<input type="hidden" name="formfields[]" value="<?=$f->field_reference ?>"/>
		                    </div>
		                    <label class="mid"><?=$f->getName() ?></label>
		                    
		                    <?php if($f->field_reference != 'email'): ?>
		                    	<a href="javascript:;" class="deleteOption" ><img height="10" width="10" class="supporting" alt="Törlés" src="<?=$base.$img ?>icons/smallOff.png"/></a>
		                    <?php endif; ?>
		                   
						</li>
						 
					<?php endforeach; ?>
					
					
					<?php endif;//////////////////?>
                </ol>
                
				

				<div class="customFieldsPromo">
						   

				
									<select name="fieldToAdd" id="fieldToAdd">
										<option value="email_0">E-mail cím</option>

										<optgroup label="Globális mezők">
											<?php foreach($globalfields as $f): ?>
												<option value="<?=$f->reference ?>-<?=$f->id ?>"><?=$f->name ?></option>
											<?php endforeach; ?>
										</optgroup>
										<optgroup label="Lista mezők">
											<?php foreach($listfields as $f): ?>
												<option value="<?=$f->reference ?>-<?=$f->id ?>"><?=$f->name ?></option>
											<?php endforeach; ?>
										</optgroup>										
										
									</select>				
				
				
				
					<a href="Javascript:;" id="addButton">Mező hozzáadása a listához</a>
					
				</div>			
			
            </div>

            <div class="mybutton">
                <button type="submit" class="button" id="generateBtn">
                    <img src="<?=$base.$img ?>icons/html.gif" width="16" height="16" alt="" />
					Mentés és a forráskód generálása
                </button>
            </div>
		
     		<div class="clearButton"></div>

			</div>	
		</div>

		</form>
		
		<script>
		
		$(function(){
			
			$('#basicTab').click(function(){
				$('#previewTab').removeClass('current');
				$('#codeTab').addClass('current');
				$('#code').show();
				$('#preview').hide();
				
			});

			$('#previewTab').click(function(){
				$('#codeTab').removeClass('current');
				$('#previewTab').addClass('current');
				
				$('#code').hide();
				$('#preview').show();				
			});
		
		});		
		
		</script>
		
			
		<div style="" id="subscribeFormCode">
            
            <div id="contentNavs">
	            <ul>
	                <li class="current" id="codeTab"><a id="basicTab" href="javascript:;"><span>Űrlap forrása</span></a></li>
	                <li id="previewTab" style="display:true"><a id="cssTab" href="javascript:;"><span>Űrlap előnézet</span></a></li>
	                <li id="table" style="display:none"><a id="tableTab" href="#"><span>Table based</span></a></li>
	            </ul>
            </div>
            
            <div style="clear:both;"></div>
            
            <div class="formBGCSS" style="display:true" id="code">
	            <div class="formWrapper">
	            
	            <div class="subscribeCode">
		            <div id="basicCode">
		            	<textarea id="generatedCode" class="codeSample" style="width: 98%; height: 300px;" name="none" rows="4" cols="20" readonly="readonly" ><?=$formHtml ?></textarea>
		            </div>
		            
	            </div>
	            
	            </div>
            </div>

            <div class="formBGCSS" style="display:none" id="preview">
	            <div class="formWrapper">
	            
	            <div class="subscribeCode">
		            <div id="basicCode">
		            	<iframe style="width: 98%;height:300px;background-color:white;display:block;" src="<?=url::base() ?>api/common/showForm/<?=$formid ?>"></iframe>
		            </div>
		            
	            </div>
	            
	            </div>
            </div>
            
            <div class="topPad"></div>
            
		
		
		</div>
			
		
		
		</div>
		
	</div>
	
	<div id="rightcol">
	
		<div id="options">
		
	
		</div>
	
	</div>





	
	<div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
