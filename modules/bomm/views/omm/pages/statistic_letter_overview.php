        <?php
			$stat = $letter->getLetterLinkStat();
			
			//$stat['openratio']
			$megnyitotta = floatval(str_replace("%","",$stat['openratio']));
			$visszapattant = floatval(str_replace("%","",$stat['temperrorratio']))+floatval(str_replace("%","",$stat['permerrorratio']));
			$nem_nyitotta_meg = 100 - ($megnyitotta + floatval(str_replace("%","",$stat['temperrorratio']))+floatval(str_replace("%","",$stat['permerrorratio'])));
			
			
			
			//
			
			//
			//$stat['temperrorratio']+$stat['permerrorratio']
			//			//
			?>

<script type="text/javascript">
	swfobject.embedSWF(
			"<?=$assets?>ampie/ampie.swf", "statistic_chart_1",
			"420", "205", "9.0.0", "expressInstall.swf",
			{
			"chart_settings" : encodeURIComponent("<settings><text_size>16</text_size><text_color>333333</text_color><pie><colors>50b432,ed561b,058dc7</colors><x>100</x><y>100</y><radius>80</radius><height>20</height><angle>30</angle><start_angle>86</start_angle><hover_brightness>50</hover_brightness></pie><decimals_separator>.</decimals_separator><background><alpha>100</alpha><border_alpha>20</border_alpha></background><balloon><show>{value}% {title}</show><text_size>12</text_size><corner_radius>6</corner_radius><border_width>3</border_width><border_color>FFFFFF</border_color><border_alpha>78</border_alpha></balloon><legend><x>200</x><y>60</y><spacing>15</spacing><key><size>12</size></key><values><enabled>1</enabled><text>{percents} %</text></values></legend><animation><start_time>1</start_time><start_effect>strong</start_effect><start_radius>200%</start_radius><start_alpha>9</start_alpha><pull_out_time>1</pull_out_time><pull_out_effect>strong</pull_out_effect></animation><export_as_image><color>FDFDE3</color><alpha>100</alpha></export_as_image></settings>"), 
			"chart_data" : encodeURIComponent("<pie><slice title='Megnyitotta'><?=$megnyitotta?></slice><slice title='Visszapattant'><?=$visszapattant?></slice><slice title='Nem nyitotta meg'><?=$nem_nyitotta_meg?></slice></pie>"), 
			"path" : "<?=$assets?>ampie/",
			"preloader_color" : "#999999",
			"wmode" : "transparent",
			"wmode" : "opaque"
			},
			{"wmode" : "transparent"} );
	
</script>

<div class="twocol">
<!-- CONTENT -->
	<div id="content">

		<div id="leftcol">
		
        <p class="bread"><a href="<?=url::base() ?>pages/statisticoverview">Statisztika</a><span class="breadArrow">&nbsp;</span><?=$letter->name ?></p>
		
        <h1 id="campaignTitle">
        <div class="campaignActions">
            <div class="mybutton">    
                <a href="<?=url::base()."api/common/showLetter/".$letter->id ?>" target="_blank" class="button">
                    <img src="<?=$base.$img?>icons/search.png" alt=""/> 
                    Levél megtekintése
                </a>
            </div> 
        </div>
        
        	<?=$letter->name ?>
        	<?php echo (empty($letter->note)) ? "" : '<a href="Javascript:;" title="'.nl2br($letter->note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?>
        </h1>
		

		<p class="titleSummary">
        <span>típus:</span> <strong><?=$letter->type ?></strong>
		</p>
		
			<?php
			
				if($letter->timing_type == "relative"){
					$tim = "Relatív";
					$sent_label = "<span>Levél küldése feliratkozás után</span> <strong>".$letter->timing_event_value." nappal</strong>";
				}else if($letter->timing_type == "absolute"){
					$tim = "Abszolút";
					if(dateutils::formatDate($letter->isSent(TRUE)) == ""){
						$dd = "Kiküldés folyamatban, állapot: ".$letter->getSendingRatio()."%";
					}else{
						$dd = $letter->getSentTime();
						
						//$dd = dateutils::formatDate($letter->isSent(TRUE))." ".$letter->timing_hour." óra";
					}
					$sent_label = "<span>kiküldés vége:</span> <strong>".$dd." (időzítés: ".$letter->getTimingDate().") </strong>";
				}else if($letter->timing_type == "sendnow"){
					$tim = "Azonnali";
					$sent_label = "<span>elküldve:</span> <strong>".dateutils::formatDate($letter->isSent(TRUE))."</strong>";
				}
				
			?>		
		
		<p class="titleSummary">
		<span>tárgy:</span> <strong><?=$letter->subject ?></strong><br/>
        <span>időzítés:</span> <strong><?=$tim ?></strong>
		</p>
		
		
		<p class="titleSummary">
        	<?=$sent_label ?>
		</p>		
        
        
      
        

		

				
		
		<div class="dataHighlight">
			<table cellpadding="0" cellspacing="0" width="100%" class="goalSummary">
			<tr>
				<td nowrap><div><?=$stat['sentNumber'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Címzettnek elküldve</h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Megnéz</a>)</td>

				<td nowrap><div><?=$stat['uniqueOpens'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Egyedi megnyitás <span>(<?=$stat['openratio'] ?>)</span></h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnéz</a>)</td>
			</tr>
			<tr class="lastGoal">
				<td nowrap><div><?=$stat['uniqueClicks'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Egyedi átkattintás <span>(<?=$stat['clickratio'] ?>)</span></h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Megnéz</a>)</td>
				<td nowrap><div class="unsubcribe"><?=$stat['unsubscribeClicks'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Leiratkozás <span>(<?=$stat['unsubratio'] ?>)</span></h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Megnéz</a>)</td>
			</tr>
			</table>
		
		</div>

		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="" width="50%">
				  <div id="statistic_chart_1"></div>
				</td>
				<td>
				
					<h2>Visszapattanó e-mailek</h2>
					<div class="dataHighlight">
						<table cellpadding="0" cellspacing="0" width="100%" class="goalSummary">
						<tr>
							<td nowrap><div class="temperror"><?=$stat['tempErrors'] ?></div></td>
							<td width="100%" class="goalDescription"><h2>Átmeneti hiba <span>(<?=$stat['temperrorratio'] ?>)</span></h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Megnéz</a>)</td>
						</tr>
						<tr class="lastGoal">
							<td nowrap stye="width:43px"><div class="unsubcribe"><?=$stat['permErrors'] ?></div></td>
							<td width="100%" class="goalDescription"><h2>Állandó hiba <span>(<?=$stat['permerrorratio'] ?>)</span></h2>(<a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Megnéz</a>)</td>
						</tr>
						</table>
					
					</div>				
				
				</td>
			</tr>
		</table>


		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">
		
			<div class="bghighlight"><h3 class="sidebar">Levél statisztikája</h3></div>
		
			<dl class="icon-menu">		
				<dt><img src="<?=$base.$img?>icons/pie.png" width="16" height="16" alt="Snapshot" /></dt>
				<!-- /pages/statisticoverview -->
				<dd class="noLink">Főnézet</dd>
				<dd class="last">Levél statisztikájának előnézete</dd>
				
                <dt><a href="<?=url::base() ?>pages/statisticdetails/recipients" id="recipientActivityIcon"><img src="<?=$base.$img?>icons/subscribers.png" width="18" height="18" alt="Recipient Breakdown" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/statisticdetails/recipients" id="recipientActivity">Címzettek tevékenységei</a></dd>
				<dd class="last">Ki nyitotta meg, ki kattintott rá a linkekre stb...</dd>
				
				<dt><a href="<?=url::base() ?>pages/statisticdetails/link" id="linkClickActivityIcon"><img src="<?=$base.$img?>icons/link-click.png" width="16" height="16" alt="Link Click Activity" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/statisticdetails/link" id="linkClickActivity">Linkek statisztikái</a></dd>
				<dd class="last">Melyik linkre hányszor kattintottak rá.</dd>
				<?php /* 
				<dt><img src="<?=$base.$img?>icons/opens-time.png" width="16" height="16" alt="Opens &amp; Clicks Over Time" /></dt>
				<dd class="noLink">A megnyitások és átkattintások időbeni statisztikái</dd>
				<dd class="last">A címzettek mikor nyitották meg a levelet, mikor kattintottak, stb.. <br/>(Hamarosan!)</dd>
					
				<dt><a href="Javascript:;" id="emailClientsIcon" style="color:grey"><img src="<?=$base.$img?>icons/clients.png" width="16" height="16" alt="Email Client Usage" /></a></dt>
				<dd class="noLink">Levelezőkliens statisztikák</dd>
				<dd class="last">Ki milyen Levelezőklienst használ <br/>(Hamarosan!)</dd>
				*/ ?>
			</dl>
	
		</div>
 <!--options end-->
    </div> <!--rightcol end-->
	

	<div class="clear"></div>
    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
