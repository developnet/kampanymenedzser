<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): 
		$szoftver = "Híreső";
		$email = "ugyfelszolgalat@garbaroyal.hu";
?>
	<link href="<?=$base.$css ?>hireso-redirect.css" media="screen,projection" rel="stylesheet" type="text/css" />
<?php else: 
		$szoftver = "KampányMenedzser";
		$email = "support@kampanymenedzser.hu";
?>
	<link href="<?=$base.$css ?>redirect.css" media="screen,projection" rel="stylesheet" type="text/css" />
<?php endif; ?>



</head>
<body>
<div id="dialogAdditional"></div>
<div id="dialogWrapper">

<div id="dialog">
<h1><?=$message ?></h1>
<p>
<span>
<?=$description ?>
</span>
</p>

</div>



<p class="footer">A levelek kiküldését a <?=$szoftver ?> szoftver végzi.</p>

</div>
</body>
</html>
