	<div id="content">
		
		<?php if(!isset($_SESSION['selected_product'])): ?>
		
		<div id="listMake">
	        <div id="Steps">
	        	<div class="Container Current">
	            	<div class="StepsCubes Current">1</div>
	                Termék adatai
	            </div>
	        </div>
        	<h1 class="step1">1. lépés: A termék adatainak megadása</h1>
		</div>

		<h1 class="extraBottomPad">Új termék felivtele</h1>
		<?php 
			$buttonName = "Tovább";
			$buttonAlign = "right";
		 ?>
		<?php else: ?>

        
			<p class="bread">
				<a href="<?=url::base() ?>pages/listoverview">Termékek</a>
				<span class="breadArrow">&nbsp;</span>
					<a href="<?=url::base() ?>pages/listdetail/index"><?=$_SESSION['selected_product']->name?></a>
				<span class="breadArrow">&nbsp;</span>
					<a href="">Termék adatainak módosítása</a>
			</p>		
		
			<h1 class="extraBottomPad">"<?=$_SESSION['selected_list']->name?>" termék adatainak módosítása</h1>
		<?php 
			$buttonName = "Mentés"; 
			$buttonAlign = "left";
		?>
		<?php endif; ?>
			
			
			
			
		
	        
			<?=$errors ?>


		<div class="formBG">
		<div class="formWrapper">

			<form action="<?=url::base() ?>pages/productadd/first" name="createList" method="post">	

			<h3>Termék neve</h3>
			<p>Adjon egy beszédes nevet a listának, ami alapján késöbb a rendszerben könnyen be tudja azonosítani.</p>

			<div class="formContainer">		
				<div class="clearfix">
					<label class="middle">Termék neve</label>
				
					<input class="<?=$classes['name'] ?> input_text" id="name" name="name" type="text" size="35" value="<?=$form['name'] ?>" />
				
				</div>
  
                <div class="clearfix">
					<label  class="middle">Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>

			</div>
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			<td width="100%" align="<?=$buttonAlign ?>">
            <!--<input type='image' src='<?=$base.$img ?>buttons/next.gif' width='85' height='40' border='0' tabindex='10'>-->
            
            		<div class="mybutton" style="float:<?=$buttonAlign ?>">
                        <button type="submit" class="button rightbutton">
                            <?=$buttonName ?>
                            <img src="<?=$base.$img ?>icons/arrow_right.png" alt="" class="right_mybutton" />
                        </button>
                        <div style="clear:both"></div>
                    </div>
            
            </td>
			</tr>
			</table>
			
		<div class="bottomPad"></div>
			

		</div>
		</div>	

	<div class="clear"></div>




	
	</div>