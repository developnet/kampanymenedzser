<script type="text/javascript" src="<?=$assets?>tinymce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=$assets?>tinymce/plugins/tinybrowser/tb_tinymce.js.php?PHPSESSID=<?=session_id()?>"></script>

<?php

$docbase = str_replace("https:","http:",substr(url::base(),0,sizeof(url::base())-2) );

cookie::set("cid", meta::getAccId()."/".$_SESSION['selected_client']->id);
cookie::set("docroot", DOCROOT);
cookie::set("linkroot", $docbase);


?>

<?php

if(isset($selectedletter)){
	$selected = $selectedletter->id;
	$name = $selectedletter->name;
	
}else{
	$selected = 0;
	$name = "";
}

?>

<script>


	var AUTOSAVE = false;

	function refreshLetters(){

			$.post("<?=url::base() ?>api/common/getCommonLetterTable", {}, 
					  function(data){ 
						json = eval( data );
						$("#letter_table").html(json.HTML);

						iniStuff();
					  	
			}); 
		
	}
	
	function getLetterData(){
		var name;
		var subject;
		var note;
		var sender_name;
		var sender_email;
			var html;


				
			try{
				html = tinyMCE.get("html_content_editor").getContent();
			}catch(e){
				html = $("#html_content_editor").val();
			}

			var text = $("#text_content_field").val();
			var name = $("#name_field").val();
			var subject = $("#subject_field").val();
			var note = $("#note_field").val();
			var sender_name = $("#sender_name_field").val();
			var sender_email = $("#sender_email_field").val();

			
			POSTARRAY =  {  "letter": "<?php echo (isset($selectedletter)) ? $selectedletter->id : 0;  ?>", 
						"name": name,
						"subject": subject,
						"note":note,
						"sender_name":sender_name,
						"sender_email":sender_email,	
						"html": html,
							"text": text
					};	

			return POSTARRAY;
	}
	
	function autosave(){
	
		if(AUTOSAVE){
	
			var time = $("#savetimer").html()-1;
	
			if(time == 0){
			
				$("#saveing").show();
				$("#letter_saved").html("-");

			
	
	
				$.post("<?=url::base() ?>api/common/saveCommonLetter", getLetterData(), 
						  function(data){ 
						  
						  	json = eval( data );
	
						  	if(json.STATUS == "ERROR"){
								alert('Hiba a mentés közben!');
						  	}else if(json.STATUS == "SUCCES"){
								$("#letter_saved").html(json.MESSAGE);
								$("#saveing").hide();
	
								$("#savetimer").html("30");
								
								refreshLetters();
								
						  	}else if(json.STATUS == "SUCCES_NEW"){
						  		var url = '<?=url::base()?>pages/commonletters/select/'+json.NEWLETTER;
						  		window.location = url;
						  		
						  	}
						  	
				}); 
	
			}else{
				$("#savetimer").html(time);
			}	
			
		}else{
			//$("#savetimer").html("30");
		}	
	
		setTimeout('autosave()',1000);
	}
		
	setTimeout('autosave()',1000);


	$(function(){ 
		var SELECTED_LETTER = 0;
		
	 		$(".deleteLetter").each(function(){
				$(this).bind('click', function(){
					SELECTED_LETTER = $(this).attr('rel');
					$(".selectionName").html($(this).attr('relName'));
					deleteLetter();
								
				});
			});
		

			var dialog = function(){ 
				var dialog = $("#dialog").dialog({
				bgiframe: false,
				resizable: false,
				width:320,
				modal: true,
				closable:false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				}
			}); 
			}	
	
		var dialogOk = function(){ 
				var dialog = $("#dialogOk").dialog({
				bgiframe: false,
				resizable: true,
				width:320,
				modal: true,
				closable:false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				}
			}); 
			} 


	 	var deleteLetter = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
		}					
		
		$('#deleteNoButton').click(function(){
				$('#dialog').dialog('close');
				$('#dialog').hide();		
		});
	
	
		$('#deleteYesButton').click(function(){
				$('#dialog').dialog('close');
				$('#dialog').hide();
				dialogOk();
				$('#dialogOk').dialog('open');
				$('#dialogOk').show();					
		});	 						
	 
	 
	 	$('#deleteMegsemButton').click(function(){
				$('#dialogOk').dialog('close');
				$('#dialogOk').hide();		
		});
	
	 	$('#deleteGoButton').click(function(){
				$('#dialogOk').dialog('close');
				$('#dialogOk').hide();
				
				window.location = '<?=url::base() ?>pages/commonletters/deleteLetter/'+SELECTED_LETTER;
						
		});	

		
 		
 		$("#auto_save_letter").click(function(){

 			if(AUTOSAVE){

 				$("#autosave_check").attr('checked', false);
 				AUTOSAVE = false;
 				$("#savetimer").html("30");
 				
 			}else{

 				$("#autosave_check").attr('checked', true);
 				AUTOSAVE = true;
 				$("#savetimer").html("30");
 				
 			}
 			

 		});

 		

 		$("#save_letter").click(function(){
 			$("#saveing").show();
 			$("#letter_saved").html("-");


 			$.post("<?=url::base() ?>api/common/saveCommonLetter", getLetterData(), 
 					  function(data){ 
 					  
 					  	json = eval( data );

 					  	if(json.STATUS == "ERROR"){
 							alert('Hiba a mentés közben!');
 					  	}else if(json.STATUS == "SUCCES"){
 							$("#letter_saved").html(json.MESSAGE);
 							$("#saveing").hide();
 							refreshLetters();
 					  	}else if(json.STATUS == "SUCCES_NEW"){
					  		var url = '<?=url::base() ?>pages/commonletters/select/'+json.NEWLETTER;
					  		window.location = url;
					  		
					  	}
 					  	
 			}); 		

 		});
 		

 		
	});

	var toggleEditor = function(id) {
		if (!tinyMCE.get(id))
			tinyMCE.execCommand('mceAddControl', false, id);
		else
			tinyMCE.execCommand('mceRemoveControl', false, id);
	}	

	function setEditorSize(){
		EDWIDTH = $('#editor_wrapper').width();
		//$('#html_content_editor_parent').css('width', EDWIDTH);
		//$('#html_content_editor_tbl').css('width', EDWIDTH);
	}



	// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		init_instance_callback : "setEditorSize",
		file_browser_callback : "tinyBrowser",
		language: "hu",
		mode : "exact",
		auto_resize : false,
		theme_advanced_resize_horizontal : true,
		//width : EDWIDTH,
		height : "600",
		elements : "html_content_editor",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "silver",
		plugins : "safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullpage,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		fullpage_encodings : "UTF-8",
		fullpage_default_doctype : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">',
		fullpage_default_title : '',
		// Theme options
		theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor,styleprops",
		theme_advanced_buttons2 : "undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,image,cleanup,help,code,|,insertdate,inserttime,preview",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,fullscreen,visualchars,nonbreaking,template,blockquote",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "center",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		theme_advanced_font_sizes : "8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px,26px,27px,28px,29px,30px",
		font_size_style_values : "8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px,26px,27px,28px,29px,30px",
		relative_urls : false,
		remove_script_host : true,
		document_base_url : "<?=$docbase ?>",
		convert_urls : false,
		relative_urls : false,
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "js/template_list.js",
		external_link_list_url : "js/link_list.js",
		external_image_list_url : "js/image_list.js",
		media_external_list_url : "js/media_list.js",
		entity_encoding : "raw",
		valid_elements : "*[*]",
		accessibility_warnings : false 
	});

</script>





<div id="dialog" title="Levél törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a "<span class="selectionName" style="color:red"></span>" nevü levelet?</strong><br />
	
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Levél törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a "<span class="selectionName" style="color:red"></span>" nevü levelet?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>


<!-- CONTENT -->
    <div id="dashboard">
		<div id="kampanyList">

		<div class="clear"></div>

      <div class="mybutton" style="float:right;margin-bottom:8px;">    
            <a href="<?=url::base() ?>pages/commonletters/newLetter" class="button slim">
                <img src="<?=$base.$img?>icons/add.gif" alt=""/> 
                Új levél létrehozása
            </a>
        </div>
        
				<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="letter_table">
				<tr class="noHighlight">
					<th class="headerLeft" width="100%" colspan="2"><span>Levelek listája</span></th>
					<th nowrap>&nbsp;</th>
	                <th class="headerRight" style="padding-right:0px;" nowrap>&nbsp;</th>
				</tr>
				
				<?php foreach ($letters as $l): ?>
				
				<tr class="<?php echo(isset($selectedletter) && $selectedletter->id == $l->id)  ? "dashRowActive":"dashRow"; ?>"> <!-- dashRowActive -->
					
					<td width="9" style="padding-right:0px;">
	                	
	                </td>
					
					<td width="100%">
	                	<strong><a href="<?=url::base() ?>pages/commonletters/select/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a></strong>
	                </td>
	                
					<td nowrap width="13" style="padding-right:0px;">
	
	                </td>
		
					<td nowrap width="10">
	                	<a href="javascript:;" class="deleteLetter" rel="<?=$l->id ?>" relName="<?=$l->name ?>">
	        		       <img src="<?=$base.$img?>icons/trash.png" width="10" height="11" border="0" alt="Törlés">
	    	            </a>
	                </td>
	                
				</tr>
				
				
				<?php endforeach; ?>
				</table> 
  			
			
			<div class="topPad"></div>
	
				
	
	
		
			<div class="clear"></div>
		
		</div> <!-- kampanyList end-->
		
			
        <div id="kampanyActivity">
			<div id="activityBG"> <!--Elválasztó BG-->
				<div id="activityContent">

		<?php 
			
			
			if(!isset($selectedletter)){
				$mod_date = "-";
				$cim = "Új levél létrehozása";
				$tools = false;
			}else{
				$mod_date = $selectedletter->mod_date;
				$letter = $selectedletter;
				$cim = '"'.$letter->name.'"'." levél szerkesztése";
				$tools = true;
			}
			
			
		?>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// ?>
		
		<div style="height:30px;margin-bottom:6px">
		
			<div id="toolbar_first" style="text-align:right;float:left;">
				 
				 <div class="mybutton" style="float:right;margin-top:2px">  
		            
		            <?php if($tools):?>
		            <a href="<?=url::base() ?>pages/commonletters/duplicate/<?=$letter->id?>" class="button slim"  id="duplicate_letter" style="margin-right:10px">
		                Levél <strong>duplikálása</strong>
		            </a>
				 
		            <a href="javascript:;" class="deleteLetter button slim" rel="<?=$letter->id ?>" relName="<?=$letter->name ?>" id="delete_letters" style="margin-right:10px">
		                Levél <strong>törlése</strong>
		            </a>
		            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        
		            <?php endif;?>
			        
			        <a href="Javascript:;" class="button slim" id="auto_save_letter" style="margin-right:10px">
			           	<input type="checkbox" disabled="disabled" id="autosave_check"/>Automentés (<span id="savetimer">30</span>)
			        </a>		            
		            
			         <a href="Javascript:;" class="button slim" id="save_letter">
			              	<img src="<?=$base.$img ?>icons/save.png" alt=""/> 
			             	Levél mentése
			         </a>		            
		            
                      <span style="margin-right:10px;padding-top:5px;float:right;width:210px">Utoljára mentve: <span id="letter_saved" style="font-weight:bold"><?=$mod_date?></span></span>
                      &nbsp;&nbsp;     
                      <img src="<?=$base.$img ?>ajax-loader2.gif" alt="" style="float:right;margin-top:5px;margin-right:5px;display:none;" id="saveing"/>		            
		            
					<div style="clear:both"></div>
				 </div>	
				<div style="clear:both"></div>

			</div>
		
			<div id="toolbar" style="text-align:right;float:right;width:200px;padding-top:8px">
				

				
			</div>
			
			
			<div style="clear:both"></div>
		</div>
		

<?php ////////////////////////////////////////////////////////////////////////////// ?>
		
			<h2><?=$cim?></h2>				
               
			<div class="formBGCSS" style="display:true;padding:10px" id="preview">
				<div class="formBG" style="padding:10px">


						<div  id="verifycation_typeHtml" style="display:true">
							
							<div class="formContainer">

								<div class="clearfix">
									<label>Megnevezés</label>
									<input type="text" id="name_field" name="name" size="80" value="<?=$form['name'] ?>" class="<?=$classes['name'] ?> input_text" />
								</div>
								
				                <div class="clearfix">
									<label>Megjegyzés</label>
									<textarea class="<?=$classes['note'] ?> input_text" name="note" id="note_field" cols="45" rows="4"><?=$form['note'] ?></textarea>
								</div>	
															
								<div class="clearfix">
									<label>Küldő név</label>
									<input type="text" name="sender_name" id="sender_name_field" size="40" value="<?=$form['sender_name'] ?>" class="<?=$classes['sender_name'] ?> input_text" />
									<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" rel="sender_name_field"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
								</div>
								
								<div class="clearfix">
									<label>Küldő Email</label>
									<input type="text" name="sender_email" size="35" id="sender_email_field" value="<?=$form['sender_email'] ?>" class="<?=$classes['sender_email'] ?> input_text" />&nbsp;&nbsp;
									<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" rel="sender_email_field"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
									<span class="mid light"></span>
								</div>
								
								<div class="clearfix">
									<label>Tárgy</label>
									<input type="text" name="subject" size="80" value="<?=$form['subject'] ?>" id="subject_field" class="<?=$classes['subject'] ?> input_text" />
									<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" rel="subject_field"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
								</div>
								
            							
							
								<div class="clearfix">
									<label>Html Tartalom</label>
									<textarea id="html_content_editor" name="html_content_editor" wrap="off" style="width: 97%; height: 500px;"><?= $form['html_content'] ?></textarea>
									<div class="mybutton" style="float:left;margin-top:8px;">    
						            	<a href="Javascript:;" class="button" onclick="javascript:toggleEditor('html_content_editor');">
						                	<img src="<?=$base.$img ?>icons/pencil.png" alt=""/> 
						                	Szerkesztő ki/be kapcsolása
						            	</a>
						            	
					
						               <div class="mybutton" style="float:left;margin-left:10px;">
							            	<a href="Javascript:;" class="button beszurasClick"  rel="html_content_editor">
							                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
							                	Behelyettesítő beszúrása
							            	</a>
						               </div> 
						               
						               <?php /*
						            	<a style="margin-left:10px;" href="<?=url::base() ?>pages/listsubscribeprocess/extracttext/html_verifycation" class="button" >
						                	<img src="<?=$base.$img ?>icons/pencil.png" alt=""/> 
						                	Szöveg másolása textbe
						            	</a>				               
						                 */?>
						                 
						            <div style="clear:both"></div>
					        	</div>							
								</div>
								
								
								
								<div class="clearfix" style="margin-top:50px"><?php /**/ ?>
									<label>Text Tartalom</label>
									<textarea name="text_content_field" id="text_content_field" cols="50" rows="10" style="width:97%" class="textOnlyOutline <?=$classes['text_content'] ?>"><?=$form['text_content'] ?></textarea>
									<div class="mybutton" style="float:left;margin-left:10px;">
							           	<a href="Javascript:;" class="button beszurasClick"  rel="text_content_field">
							               	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
							               	Behelyettesítő beszúrása
							           	</a>
							           	<div style="clear:both"></div>
						            </div>   						
						            <div style="clear:both"></div>
		
								</div>
								
							
								
								<br />
							
							</div>
			
						</div>
				
				</div>
			</div>               
               
               
               
               
		<div class="topPad"></div>


                    
        

                    
                    
					<div class="clearActivity"></div>
				</div>
			</div>
					
		</div> <!-- clientActivity end-->
		<div class="clear"></div>

		
	</div> <!-- Dashboard VÉGE ********************************************************************************************************************-->

<!-- CONTENT VÉGE ******************************************************************************************************************************-->

				<?php 
				/// beszúrás dialog
				
				$sepStart = Kohana::config('core.fieldSeparatorStart');
				$sepEnd = Kohana::config('core.fieldSeparatorEnd');
				$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
				$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
				$activateLink = $sepStart.Kohana::config('core.activation_link_name').$sepEnd;
				$email = $sepStart."email".$sepEnd;
				$regdatum = $sepStart."regdatum".$sepEnd;				
				?>

	                <script>
	                $(function(){

	            		var beszuras_dialog = function(){ 
	            			var dialog = $("#beszuras_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".beszurasClick").click(function(){
	            			var rel = $(this).attr('rel');	
	            			$('#fieldId2').val(rel);		
	            			beszuras_dialog();
	            			$('#beszuras_dialog').dialog('open');
	            			$('#beszuras_dialog').show();
	            			
	            		});

	                	
	    				var SELECTED_LIST = "";
						$('#altalanosInsert2').click(function(){
							var id = $('#fieldId2').val();
							var content = $('#'+id).val();
							var tag = $('#altalansTag2').val();

							
							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								if(tag == '<?=$unsubLink ?>'){
									tag = '<a href="<?=$unsubLink ?>">Leiratkozás</a>';
								}else if(tag == '<?=$datamodLink ?>'){
									tag = '<a href="<?=$datamodLink ?>">Adatmódosítás</a>';
								}else if(tag == '<?=$activateLink ?>'){
									tag = '<a href="<?=$activateLink ?>">Aktiválás</a>';
								}
								
								 tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag);
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}
							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		  
						});
	
	                	$('#listaInsert2').click(function(){
							var id = $('#fieldId2').val();
							var content = $('#'+id).val();
							var tag = $('#'+$('#listak2').val()).val();

							if(tag == "null" || tag == null){
								tag = "";
							}

							if(id.indexOf("_editor") >= 0){
								tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag)
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}

							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		                	
	                	});
	                	
	                	$('#listak2').change(function (){
	                		$('.fieldContainer2').hide();
	                		$('#'+$('#listak2').val()).show();
	                	});
	                	
	                });
	                </script>

<div id="beszuras_dialog" title="Behelyettesítő beszúrása" style="display:none">
        		<div id="behelyettesito">
	        		<input type="hidden" id="fieldId2" value=""/>
	        		<table>
	        			<tr>
	        				
	        				<td valign="top">
		        				<div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>		
	
				                <select size="5" style="width:210px" id="altalansTag2">
				                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
				                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
				                	<option value="<?=$activateLink ?>"><?=$activateLink ?> - aktiváló url</option>
				                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
				                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
				                </select>
				                
	        				</td>
	        				
	        				<td valign="top" align="right">

								<div class="bghighlight" style="text-align:center"><h3 class="sidebar">Listák mezői:</h3></div>
								Lista:
				                <select size="1" style="width:170px" id="listak2">
				                	<?php foreach($lists as $list): 
				                	?>
				                		<option value="2list_<?=$list->id ?>"><?=$list->name ?></option>
				                	<?php endforeach; ?>
				                </select>
								<p><br/></p>
									Mező:
				                	<?php 
				                		$first = true;
				                		foreach($lists as $list):  
				                		
				                			$fields = $list->fields();
				                	?>
										
										<select class="fieldContainer2" size="1" style="width:170px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="2list_<?=$list->id ?>">			
											<?php foreach($fields as $field): ?>
											<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
											<?php endforeach; ?>
										</select>
				                	<?php endforeach; ?>
				
				                
	        				</td>
	        				
	        			</tr>

						<tr>
							<td align="center">
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="altalanosInsert2">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							<td align="center">
							
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="listaInsert2">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div> 								
							
							</td>
						</tr>
	        		</table>
				</div>
</div>
<?php /// beszúrás dialog ?>
