<script>

$(function(){
	

	$('.saveandexit').click(function(){
		$('#exitinput').val('exit');
		document.forms['step1_1'].submit();
		return false;
	});

	$('.saveandgo').click(function(){
			$('#exitinput').val('tonextpage');
			document.forms['step1_1'].submit();
			return false;
	});	

});
</script>

<form name="step1_1" action="<?=url::base() ?>pages/letterdetail/first" method="post">	
<input type="hidden" name="exitinput" id="exitinput" value="0"/>
<!-- CONTENT -->
    <div id="content">

		<div id="listMake">
        <div id="Steps">
        	<div class="Container Current">
            	<div class="StepsCubes Current">1</div>
                Levél adatai
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">2</div>
                Levél tartalma
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">3</div>
                Tesztküldés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">4</div>
                Időzítés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">5</div>
                Címzési feltételek
            </div>
        </div>
        <h1 class="step1">1. lépés: A levél adatainak megadása</h1>
        </div>
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		    <td width="42%">
            
            <?php if(!isset($_SESSION['selected_letter'])): ?>
	            <div class="mybutton">    
	            <a href="<?=url::base() ?>pages/letteroverview" class="button">
	                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/> 
	                Mégsem
	            </a>
	        	</div>
        	<?php endif; ?>
        	
            </td>
			<td width="" align="center">       
	        <div class="mybutton">    
	        <a href="javascript:;" class="button saveandexit">
	            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
	            Ment és kilép
	        </a>
	        </div> 
            </td>		    
		    <td width="42%" align="right">
	        <div class="mybutton" style="float:right">    
	        <a href="javascript:;" class="button rightbutton saveandgo">
				Tovább a tartalom szerkesztéséhez&nbsp;
	            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
	        </a>
	        </div> 
            </td>
		</tr>
		</table>
		
        <br />
        
		<?=$errors ?>
		
        <div class="clear"></div>

		<div class="formBG">
		<div class="formWrapper">

				

			<h3>1. Levél megnevezése</h3>
			<p>Adjon beszédes nevet a levélnek, hogy késöbb a rendszerben könnyen azonosítani tudja.</p>

			<div class="formContainer">		
				<div class="clearfix">
				<label>Megnevezés</label><input type="text" name="name" id="name" size="60" tabindex="2" value="<?=$form['name'] ?>" class="<?=$classes['name'] ?> input_text" />
				</div>
                <div class="clearfix">
					<label>Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>					
			</div>
			
			<h3 class="topPad">2. Levél tárgya</h3>
			<p>A levél tárgya az, amit kiküldéskor a feliratkozók elöször fognak látni. Itt is használhatja a behelyettesítő linkeket.</p>

			<div class="formContainer">		
				<div class="clearfix">

				<label>Tárgy</label>
				<input type="text" name="subject" size="60" tabindex="3" value="<?=$form['subject'] ?>" class="<?=$classes['subject'] ?> input_text" id="htmlSubjectField"/>
				<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" title="Behelyettesítő beszúrása" rel="htmlSubjectField"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
				</div>
			</div>
			
			<h3 class="topPad">3. Küldő név</h3>
			<p>A küldő neve a kiküldőtt levél <strong>Feladó</strong> mezőjében fog szerepelni.</p>

			<div class="formContainer">		
				<div class="clearfix">

				<label>Küldő név</label><input type="text" name="sender_name" size="40" tabindex="4" value="<?=$form['sender_name'] ?>" class="<?=$classes['sender_name'] ?> input_text" maxlength="90" />
				</div>
			</div>
			
			<h3 class="topPad">4. Küldő e-mail cím</h3>
			<p>A feladő e-mail cím.</p>

			<div class="formContainer">		
				<div class="clearfix">

				<label>Küldő e-mail</label>
				<input type="text" name="sender_email" size="40" tabindex="5" class="<?=$classes['sender_email'] ?> input_text" value="<?=$form['sender_email'] ?>" />
                
				</div>
			</div>
			
			<h3 class="topPad">5. Válasz e-mail cím</h3>
			<p>Ha a feliratkozottak válaszolnak a hírlevélre, akkor erre az e-mail címre fognak válaszlni.</p>

			<div class="formContainer">		
				<div class="clearfix">

				<label>Válasz e-mail</label><input type="text" name="sender_replyto" size="40" tabindex="7" value="<?=$form['sender_replyto'] ?>" class="<?=$classes['sender_replyto'] ?> input_text" />
				</div>
			</div>

		</div>
		</div>
			
		<br>
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		    <td width="42%">
            
            <?php if(!isset($_SESSION['selected_letter'])): ?>
	            <div class="mybutton">    
	            <a href="<?=url::base() ?>pages/letteroverview" class="button">
	                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/> 
	                Mégsem
	            </a>
	        	</div>
        	<?php endif; ?>
        	
            </td>
			<td width="" align="center">       
	        <div class="mybutton">    
	        <a href="javascript:;" class="button saveandexit">
	            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
	            Ment és kilép
	        </a>
	        </div>  
            </td>		    
		    <td width="42%" align="right">
	        <div class="mybutton" style="float:right">    
	        <a href="javascript:;" class="button rightbutton saveandgo">
				Tovább a tartalom szerkesztéséhez&nbsp;
	            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
	        </a>
	        </div> 
            </td>
		</tr>
		</table>
		
		

	<div class="clear"></div>


	</div>     
<!-- CONTENT VÉGE -->
</form>




				<?php 
				/// beszúrás dialog
				
				$sepStart = Kohana::config('core.fieldSeparatorStart');
				$sepEnd = Kohana::config('core.fieldSeparatorEnd');
				$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
				$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
				$activateLink = $sepStart.Kohana::config('core.activation_link_name').$sepEnd;
				$email = $sepStart."email".$sepEnd;
				$regdatum = $sepStart."regdatum".$sepEnd;				
				?>

	                <script>
	                $(function(){

	            		var beszuras_dialog = function(){ 
	            			var dialog = $("#beszuras_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".beszurasClick").click(function(){
	            			var rel = $(this).attr('rel');	
	            			$('#fieldId').val(rel);		
	            			beszuras_dialog();
	            			$('#beszuras_dialog').dialog('open');
	            			$('#beszuras_dialog').show();
	            			
	            		});

	                	
	    				var SELECTED_LIST = "";
						$('#altalanosInsert').click(function(){

							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#altalansTag').val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								if(tag == '<?=$unsubLink ?>'){
									tag = '<a href="<?=$unsubLink ?>">Leiratkozás</a>';
								}else if(tag == '<?=$datamodLink ?>'){
									tag = '<a href="<?=$datamodLink ?>">Adatmódosítás</a>';
								}else if(tag == '<?=$activateLink ?>'){
									tag = '<a href="<?=$activateLink ?>">Aktiválás</a>';
								}
								
								 tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag);
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}
							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		  
						});
	
	                	$('#listaInsert').click(function(){
							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#'+$('#listak').val()).val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag)
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}

							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		                	
	                	});
	                	
	                	$('#listak').change(function (){
	                		$('.fieldContainer').hide();
	                		$('#'+$('#listak').val()).show();
	                	});
	                	
	                });
	                </script>

<div id="beszuras_dialog" title="Behelyettesítő beszúrása" style="display:none">
        		<div id="behelyettesito">
	        		<input type="hidden" id="fieldId" value=""/>
	        		<table>
	        			<tr>
	        				
	        				<td valign="top">
		        				<div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>		
	
				                <select size="5" style="width:210px" id="altalansTag">
				                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
				                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
				                	<option value="<?=$activateLink ?>"><?=$activateLink ?> - aktiváló url</option>
				                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
				                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
				                </select>
				                
	        				</td>
	        				
	        				<td valign="top" align="right">

								<div class="bghighlight" style="text-align:center"><h3 class="sidebar">Listák mezői:</h3></div>
								Lista:
				                <select size="1" style="width:170px" id="listak">
				                	<?php foreach($lists as $list): 
				                	?>
				                		<option value="list_<?=$list->id ?>"><?=$list->name ?></option>
				                	<?php endforeach; ?>
				                </select>
								<p><br/></p>
									Mező:
				                	<?php 
				                		$first = true;
				                		foreach($lists as $list):  
				                		
				                			$fields = $list->fields();
				                	?>
										
										<select class="fieldContainer" size="1" style="width:170px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="list_<?=$list->id ?>">			
											<?php foreach($fields as $field): ?>
											<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
											<?php endforeach; ?>
										</select>
				                	<?php endforeach; ?>
				
				                
	        				</td>
	        				
	        			</tr>

						<tr>
							<td align="center">
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button"  id="altalanosInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							<td align="center">
							
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="listaInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div> 								
							
							</td>
						</tr>
	        		</table>
				</div>
</div>
<?php /// beszúrás dialog ?>




