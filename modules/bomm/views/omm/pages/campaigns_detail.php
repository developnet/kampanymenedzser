<?php

if(isset($_SESSION['selected_campaign'])){
	$h1 = "Kampány szerkesztése";
	$button = "Kampány mentése";
}else{
	$h1 = "Új kampány létrehozása";
	$button = "Kampány létrehozása";
}

?>


<!-- CONTENT -->
	<div id="content">
	
    <p class="bread">
    	<a href="<?=url::base() ?>pages/campaignoverview">Kampányok</a>
    	<span class="breadArrow">&nbsp;</span>
    	<?=$name ?></p>
    
    
    <h1><?=$h1 ?></h1>
    
    <?=$errors ?>
    
	<p class="extraBottomPad"></p>
    
	<div class="formBG">
		<div class="formWrapper">

			<form name="step1_1" action="<?=url::base() ?>pages/campaigndetail" method="post">		

			<h3>1. Kampány megnevezése</h3>
			<p>Adjon a kampánynak beszédes nevet, hogy késöbb könnyen be tudja azonosítani a rendszerben.</p>

			<div class="formContainer">		
				<div class="clearfix">
					<label>Megnevezés</label><input type="text" name="name" id="name" size="60" tabindex="2" value="<?=$form['name'] ?>" class="<?=$classes['name'] ?> input_text" >
					<input type="hidden" name="status" value="active"/>
				</div>
		        <div class="clearfix">
					<label  >Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>		
			</div>
			
			<h3 class="topPad">2. Alapértelmezett kampány küldő név</h3>
			<p></p>

			<div class="formContainer">		
				<div class="clearfix">
				<label>Küldő név</label><input type="text" name="sender_name" size="40" tabindex="4" value="<?=$form['sender_name'] ?>" class="<?=$classes['sender_name'] ?> input_text" maxlength="90" />
				</div>
			</div>
			
			<h3 class="topPad">3. Alapértelmezett kampány küldő e-mail cím</h3>
			<p></p>

			<div class="formContainer">		
				<div class="clearfix">
				<label>Küldő email</label><input type="text" name="sender_email" size="40" tabindex="4" value="<?=$form['sender_email'] ?>" class="<?=$classes['sender_email'] ?> input_text" maxlength="90" />
				</div>
			</div>
			
			<h3 class="topPad">4. Alapértelmezett kampány válasz e-mail cím</h3>
			<p>Ez az e-mail fog megjelenni a levelekben mint válasz e-mail cím.</p>

			<div class="formContainer">		
				<div class="clearfix">
				<label>Válasz e-mail</label><input type="text" name="sender_replyto" size="40" tabindex="7" value="<?=$form['sender_replyto'] ?>" class="<?=$classes['sender_replyto'] ?> input_text" />
				</div>
			</div>

		<div class="topPad"></div>

            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img?>icons/add.gif" alt=""/> 
                    <?=$button ?>
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/campaignoverview" id="cancelSaveChanges">mégsem</a></span>
                <div style="clear:both"></div>
            </div> 
            
		<div class="clearButton"></div>

		</div>
		</div>
			



		</form>

	
	<div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
