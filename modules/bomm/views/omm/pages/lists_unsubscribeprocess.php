			
				<?php 
				/// beszúrás dialog
				
				$sepStart = Kohana::config('core.fieldSeparatorStart');
				$sepEnd = Kohana::config('core.fieldSeparatorEnd');
				$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
				$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
				$activateLink = $sepStart.Kohana::config('core.activation_link_name').$sepEnd;
				$email = $sepStart."email".$sepEnd;
				$regdatum = $sepStart."regdatum".$sepEnd;				
				?>

	                <script>
	                $(function(){

	            		var beszuras_dialog = function(){ 
	            			var dialog = $("#beszuras_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".beszurasClick").click(function(){
	            			var rel = $(this).attr('rel');	
	            			$('#fieldId').val(rel);		
	            			beszuras_dialog();
	            			$('#beszuras_dialog').dialog('open');
	            			$('#beszuras_dialog').show();
	            			
	            		});

	                	
	    				var SELECTED_LIST = "";
						$('#altalanosInsert').click(function(){

							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#altalansTag').val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								if(tag == '<?=$unsubLink ?>'){
									tag = '<a href="<?=$unsubLink ?>">Leiratkozás</a>';
								}else if(tag == '<?=$datamodLink ?>'){
									tag = '<a href="<?=$datamodLink ?>">Adatmódosítás</a>';
								}else if(tag == '<?=$activateLink ?>'){
									tag = '<a href="<?=$activateLink ?>">Aktiválás</a>';
								}
								
								 tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag);
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}
							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		  
						});
	
	                	$('#listaInsert').click(function(){
							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#'+$('#listak').val()).val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag)
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}

							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		                	
	                	});
	                	
	                	$('#listak').change(function (){
	                		$('.fieldContainer').hide();
	                		$('#'+$('#listak').val()).show();
	                	});
	                	
	                });
	                </script>

<div id="beszuras_dialog" title="Behelyettesítő beszúrása" style="display:none">
        		<div id="behelyettesito">
	        		<input type="hidden" id="fieldId" value=""/>
	        		<table>
	        			<tr>
	        				
	        				<td valign="top">
		        				<div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>		
	
				                <select size="5" style="width:210px" id="altalansTag">
				                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
				                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
				                	<option value="<?=$activateLink ?>"><?=$activateLink ?> - aktiváló url</option>
				                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
				                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
				                </select>
				                
	        				</td>
	        				
	        				<td valign="top" align="right">

								<div class="bghighlight" style="text-align:center"><h3 class="sidebar">Listák mezői:</h3></div>
								Lista:
				                <select size="1" style="width:170px" id="listak">
				                	<?php foreach($lists as $list): 
				                			if($listName != $list->name) continue;
				                	?>
				                		<option value="list_<?=$list->id ?>"><?=$list->name ?></option>
				                	<?php endforeach; ?>
				                </select>
								<p><br/></p>
									Mező:
				                	<?php 
				                		$first = true;
				                		foreach($lists as $list):  
				                		
				                			if($listName != $list->name) continue;
				                		
				                			$fields = $list->fields();
				                	?>
										
										<select class="fieldContainer" size="1" style="width:170px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="list_<?=$list->id ?>">			
											<?php foreach($fields as $field): ?>
											<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
											<?php endforeach; ?>
										</select>
				                	<?php endforeach; ?>
				
				                
	        				</td>
	        				
	        			</tr>

						<tr>
							<td align="center">
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="altalanosInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							<td align="center">
							
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="listaInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div> 								
							
							</td>
						</tr>
	        		</table>
				</div>
</div>
<?php /// beszúrás dialog ?>


<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			Leiratkozási folyamat beállítása
		</p>
		
		<h1>Leiratkozási folyamat beállítása</h1>

		<?=$errors ?>

		<p>Minden kiküldött hírlevélben szerepelnie kell egy leiratkozás linknek, amivel a hírlevél olvasója egy kattintással leiratkozhat. Itt megadhatja, hogy a leiratkozás folyamata során az olvasó milyen oldalakat lásson.</p>
		
		<form name="unsubscribeForm" action="<?=url::base() ?>pages/listunsubscribeprocess" method=post>
		    <div class="formBG">
		    <div class="formWrapper">
 			
			    <h3 class="topPad">Leiratkozás utáni oldal címe</h3>
			    <div class="formContainer">
				    <div class="clearfix">
				    <input type="text" name="unsubscribe_landing_page" id="unsubscribe_landing_page" size="50" tabindex="1" value="<?=$form['unsubscribe_landing_page'] ?>" class="<?=$classes['unsubscribe_landing_page'] ?> input_text" style="width:80%">
				    <a href="javascript:;" class="beszurasClick" style="cursor:pointer;" title="Behelyettesítő beszúrása" rel="unsubscribe_landing_page"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
				    </div>
			    </div>

				<h3>Ismételt leiratkozás link</h3>
				<div class="formContainer">
					<div class="clearfix">
						<input type="text" name="second_unsubscribe_page" id="second_unsubscribe_page" size="50" value="<?=$form['second_unsubscribe_page'] ?>" class="<?=$classes['second_unsubscribe_page'] ?> input_text" style="width:80%" />
						
					</div>
				</div>

				<h3>Sikertelen leiratkozás link </h3>
				
				<div class="formContainer">
					<div class="clearfix">
						<input type="text" name="error_unsubscribe_page" id="error_unsubscribe_page" size="50" value="<?=$form['error_unsubscribe_page'] ?>" class="<?=$classes['error_unsubscribe_page'] ?> input_text" style="width:80%" />
						
					</div>
				</div>


			<div class="topPad"></div>
            
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>buttons/icon-tick.gif" width="16" height="16" alt="" />
					Leiratkozási folyamat mentése
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
            </div>
            
            <div class="clearButton"></div>


		    </div>		
		    </div>

		</form>
		
	</div>
	
	<div id="rightcol">
	
		<div id="options">
	
	
		</div>
	
	</div>





	
	<div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
