<?php 

$gomb = "Előfizetői profil mentése";
$title = 'Aktuális hónap egyenlege';

?>
<style>

.clearfix label{
	width:120px;
}

</style>

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>
		
			<?=$errors ?>
	
    
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
            <tr class="noHighlight">
                <th class="headerLeft cellLeft" width="25%"><?=$month?>&nbsp;</th>
                <th class="cellLeft" nowrap width="25%">&nbsp;</th>
                <th class="cellLeft" nowrap width="60%" class="cellCenter">&nbsp;</th>
                <th class="headerRight cellRight" nowrap width="25%">Összeg</th>
            </tr>    
		<!-- 
			<tr id="0" class="letterRow">
			    <td class="rowLeft"><?=$account->omm_package->name?> alapdíj</td>
				<td class="cellLeft"></td>
				<td class="cellLeft"></td>
				<td class="rowRight cellRight"><?=$account->omm_package->base_price?> Ft</td>
			</tr>
 		-->
			<tr id="0" class="letterRow">
			    <td class="rowLeft">Egyedi e-mail címek</td>
				<td class="cellLeft"><?=$emailcount?> db</td>
				<td class="cellLeft"><?=$package->name?> (<?=$package->emailfrom?> - <?=$package->emailto?> db egyedi e-mailcím)</td>
				<td class="rowRight cellRight"><?=$package->base_price?> Ft</td>
			</tr>
 
 			<tr id="0" class="letterRow">
			    <td class="rowLeft">Kiküldött levelek</td>
				<td class="cellLeft" colspan="3"><?=$mailcount?> db</td>
			</tr>
                        	
            <tr class="noHighlight">
                <td class="footerLeft simple" colspan="3" style="color:#000"><strong>Aktuális egyenleg (nettó):</strong></td>
                <td class="footerRight simple cellRight" style="color:#000"><strong><?=$package->base_price ?> Ft</strong></td>
            </tr>
		</table> 	

	
	<div id="rightcol">

				<div id="options">

					<div class="bghighlight"><h3 class="sidebar">Előfizetői profil oldalai</h3></div>
					<dl class="icon-menu">		
		                <dt><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Előfizetői adatok" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldLink">Előfizetői adatok</a></dd>
						<div class="clear"></div>
						
		                <dt><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Egyenleg" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldLink">Egyenleg</a></dd>
						<div class="clear"></div>
		                <?php /* 
						<dt><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Számlák" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsLink">Számlatörténet</a></dd>
						<div class="clear"></div>
*/ ?>
		            </dl>
					</div>

					<div class="newFeatures">
                    &nbsp;
					</div>

				</div>

	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
