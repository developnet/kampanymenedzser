<?php 
$gomb = "Előfizetői profil mentése";
$title = 'Levélsablonok';
?>

<script>
	$(function(){ 
	
		var SELECTED_TEMPL = 0;
	
  		$(".listRow").each(function(){ 
  			
			var id = $(this).attr("rel");

			//alert(id);

			$(this).mouseover( function() {

				$("#templ_"+id+"_delete").show();
				
			} );

			$(this).mouseout( function() {
				
				$("#templ_"+id+"_delete").hide();
				
			} );
 
 

 		$(".deleteTempl").each(function(){
				$(this).bind('click', function(){
					SELECTED_TEMPL = $(this).attr('rel');
					$('.selectionName').html($(this).attr('relName'));
					deleteTempl();
								
				});
			});
		
		});

		
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
			}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteTempl = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/lettertemplates/deletetemplate/'+SELECTED_TEMPL;
					
	});	
		
		 
	});

	
</script>

<style>

.clearfix label{
	width:120px;
}

</style>

<div id="dialog" title="Sablon törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" sablont?</strong><br />
	  </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Honlap törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" sablont?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?>
		
			        <div class="mybutton" style="float:right;padding-bottom:8px;">    
			            <a href="<?=url::base() ?>pages/lettertemplates/add" class="button">
			                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
			               Új sablon feltöltése
			            </a>
			            <div style="clear:both"></div>
			        </div>		
		
		</h1>
		
		
			<?=$errors ?>
			<?=$alert ?>
			
	 
    
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft cellLeft" width="60%">Sablon megnevezése</th>
			<th nowrap="nowrap" width="30%" class="cellCenter">Utolsó módosítás dátuma<br /></th>
			<th nowrap="nowrap"  class="headerRight" style="text-align:right"><br /><img src="<?=$base.$img ?>_space.gif" width="30" height="1" alt=""></th>            
		</tr>


		<?foreach ($templates as $template): ?>
		
			<tr id="<?=$template->id ?>" class="listRow" rel="<?=$template->id ?>">
			
			    <td class="rowLeft"><a href="<?=url::base() ?>pages/lettertemplates/selecttemplate/<?=$template->id ?>"><?=$template->name ?></a></td>
	            <td class="cellCenter"><span><?=$template->mod_date?></span></td>
	
    		    <td class="dashRow" rel="<?=$template->id ?>"  style="text-align:right">
			    	<span class="deleteTempl" rel="<?=$template->id ?>" relName="<?=$template->name ?>"id="templ_<?=$template->id ?>_delete" style="display:none">
			    		<a href="#" >
			    			<img src="<?=$base.$img ?>icons/trash.gif" width="10" height="11" alt="Törlés"></a>
			    	</span>
			    </td>
			    
			</tr>
		
		<?endforeach; ?>
		
		<?php if(sizeof($templates) == 0): ?>
			<tr >
			
			    <td class="rowLeft" colspan="3" align="center">Még nincs feltöltött sablon. <a href="<?=url::base() ?>pages/lettertemplates/add">Ide kattintva megteheti.</a></td>
			    
			</tr>		
		<?php endif; ?>
		
		<tr class="noHighlight">
			<td class="footerLeft simple" >&nbsp;</td>
			<td class="footerMiddle" align="center"><strong>&nbsp;</strong></td>
			<td class="footerRight simple">&nbsp;</td>
		</tr>
		</table>
     
        
	

	
    <div id="rightcol">

				<div id="options">


					<!--<div class="bghighlight"><h3 class="sidebar"> Hasznos tanácsok </h3></div>-->


						<div style="clear:both"></div>
	
						<div class="bghighlight"><h3 class="sidebar">Honlapbeállítások</h3></div>
						<dl class="icon-menu">		

			                <dt><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldLink">Honlap adatai</a></dd>			                
							<div class="clear"></div>
							
			                <dt><a href="<?=url::base() ?>pages/fields/index" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/fields/index" id="addCustomFieldLink">Globális mezők</a></dd>			                
							<div class="clear"></div>							

			                <dt><a href="<?=url::base() ?>pages/productoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/productoverview" id="addCustomFieldLink">Termékek</a></dd>			                
							<div class="clear"></div>
							
							<dt><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldLink">Levélsablonok</a></dd>
							<div class="clear"></div>							
										                
			                <dt><a href="<?=url::base() ?>pages/commonletters" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/commonletters" id="addCustomFieldLink">Általános levelek</a></dd>
							<div class="clear"></div>

							
							<?php /*>
			                <dt><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Űrlap sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldLink">Űrlap sablonok</a></dd>
							<div class="clear"></div>
			                */?>

			            </dl>
						</div>
	


				</div>


	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
