<script type="text/javascript" src="<?=$assets?>tinymce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=$assets?>tinymce/plugins/tinybrowser/tb_tinymce.js.php?PHPSESSID=<?=session_id()?>"></script>

<?php
$docbase = str_replace("https:","http:",substr(url::base(),0,sizeof(url::base())-2) );

cookie::set("cid", meta::getAccId()."/".$_SESSION['selected_client']->id);
cookie::set("docroot", DOCROOT);
cookie::set("linkroot", $docbase);


?>

<script>

$(function(){

	$('#html_source_upload_1').click(function(){
		$('#file_upload_1').toggle();
	});
	
	$('#html_source_template_1').click(function(){
		$('#template_choice_1').toggle();		
	});


	$('#html_source_upload_2').click(function(){
		$('#file_upload_2').toggle();
	});
	
	$('#html_source_template_2').click(function(){
		$('#template_choice_2').toggle();		
	});
		
	
	$(".confirmation_html_toogle").click(function(){
	verifycation($(this));
	});

	
	var verifycation = function(radio) {
		val = $('input[@name=verifycation_type]:checked').val();
		
		if(radio){
			val = radio.val();
		}else{
			val = "<?=$form['verifycation_type'] ?>";
		}
		
		if(val == "html"){
			$("#verifycation_typeText").hide();
			$("#verifycation_typeHtml").show();
		}else if(val == "text"){
			$("#verifycation_typeText").show();
			$("#verifycation_typeHtml").hide();		
		}else{
			$("#verifycation_typeText").hide();
			$("#verifycation_typeHtml").hide();		
		}	
	}

	var subscribeconf = function(radio){
		
		val = $('input[@name=subscribe_conf_type]:checked').val();
		
		if(radio){
			val = radio.val();
		}else{
			val = "<?=$form['subscribe_conf_type'] ?>";
		}
		
		
		if(val == "html"){
			$("#subscribe_confText").hide();
			$("#subscribe_confHtml").show();
		}else if(val == "text"){
			$("#subscribe_confText").show();
			$("#subscribe_confHtml").hide();		
		}else{
			$("#subscribe_confText").hide();
			$("#subscribe_confHtml").hide();		
		}	
	}


	$(".subscribe_conf_html_toogle").click(function(){
		subscribeconf($(this));
	});
	
	verifycation();
	subscribeconf();

	var EDWIDTH = $('#editor_wrapper').width();
	
	$(window).bind('resize', function() {
		setEditorSize();
	});
	
});



</script>

</script>
<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			Feliratkozási folyamat beállítása
		</p>

		
		
		<script>
		
		$(function(){
			
			$('#basicTab').click(function(){
				$('#previewTab').removeClass('current');
				$('#codeTab').addClass('current');
				$('#code').show();
				$('#preview').hide();
				
			});

			$('#previewTab').click(function(){
				$('#codeTab').removeClass('current');
				$('#previewTab').addClass('current');
				
				$('#code').hide();
				$('#preview').show();				
			});
		
		});		
		
		</script>		
		
		<h1>Feliratkozási folyamat beállítása</h1>
		
		<?=$errors ?>	
		<?=$alert ?>	
		
		<form name="subscribeprocess" action="<?=url::base() ?>pages/listsubscribeprocess" method="post"  enctype="multipart/form-data">
		
		<div style="" id="subscribeFormCode">
            
            <div id="contentNavs">
	            <ul>
	                <li class="current" id="codeTab"><a id="basicTab" href="javascript:;"><span>Köszönő oldalak</span></a></li>
	                <li id="previewTab" style="display:true"><a id="cssTab" href="javascript:;"><span>Levelek</span></a></li>
	            </ul>
            </div>
            
            <div style="clear:both;"></div>
            
            <div class="formBGCSS" style="display:true;padding:10px" id="code">
				
				
					<div class="formBG">
						<div class="formWrapper">
							<h3>Feliratkozást köszönő, megerősítésre figyelmeztető oldal címe</h3>
							<p>Ez az oldal fog megjelenni sikeres regisztráció után. Kettős Opt-in es lista esetén aktivációs e-mail kiküldése után, aktiváció elött megjelenő oldal.</p>
							
							<div class="formContainer">
								<div class="clearfix">
									
									<input type="text" name="initial_conf_page" id="initial_conf_page" size="50" value="<?=$form['initial_conf_page'] ?>" class="<?=$classes['initial_conf_page'] ?> input_text" style="width:80%" />
									<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" title="Behelyettesítő beszúrása" rel="initial_conf_page"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>
								</div>
							</div>
							
				
							<h3>Ismételt feliratkozás link</h3>
							<p>Ez az oldal akkor jelenik meg, ha egy feliratkozó másodszor próbál meg a listára feliratkozni.</p>
							<div class="formContainer">
								<div class="clearfix">
									
									<input type="text" name="second_subscribe_page" id="second_subscribe_page" size="50" value="<?=$form['second_subscribe_page'] ?>" class="<?=$classes['second_subscribe_page'] ?> input_text" style="width:80%" />
														
								</div>
							</div>
				
				
				
							<h3>Sikertelen feliratkozás link </h3>
							<p>Ez az oldal akkor jelenik meg, ha a feliratkozás sikertelen volt.</p>
							<div class="formContainer">
								<div class="clearfix">
									
									<input type="text" name="error_subscribe_page" id="error_subscribe_page" size="50" value="<?=$form['error_subscribe_page'] ?>" class="<?=$classes['error_subscribe_page'] ?> input_text" style="width:80%" />
														
								</div>
							</div>
							
							<div id="kettosOptin1" <?php echo ($_SESSION['selected_list']->type=='single') ? 'style="display:none;"' : ''; ?>>
								<h3 class="topPad">Sikeres kettős Opt-in feliratkozás megköszönő oldal címe</h3>
								<p>Akitvációs linkre kattintás, aktivációt megerősítő oldal.</p>
								<div class="formContainer">
									<div class="clearfix">
										
										<input type="text" name="second_conf_page" size="40" style="width:80%" value="<?=$form['second_conf_page'] ?>" class="<?=$classes['second_conf_page'] ?> input_text"/>
									<a href="javascript:;" class="beszurasClick" style="cursor:pointer;" title="Behelyettesítő beszúrása" rel="second_conf_page"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>						
									</div>
								</div>						
							</div>
							
							<div class="topPad"></div>
						</div>
					</div>
			</div>
			
			<div class="formBGCSS" style="display:none;display:true;padding:10px" id="preview">
				<div class="formBG" style="padding:10px">
				
					<div id="kettosOptin" <?php echo ($_SESSION['selected_list']->type=='single') ? 'style="display:none;"' : ''; ?>>
					
						<h3 class="topPad">Regisztrációt megerősítő e-mail</h3>
			
						<p>Miután valaki elküldte a feliratkozó űrlapot (aktivációs linket tartalmazó levél) a következő történjen:</p>
			
						<p>
						    <input type="radio" name="verifycation_type" value="html" class="confirmation_html_toogle" <?php echo ($form['verifycation_type'] == "text" || $form['verifycation_type'] == "html") ? 'checked="checked"' : ""; ?>/>
						    <strong>
						        <label for="verifycation_type" >&nbsp;&nbsp;Küldjel ki ezt a levelet: </label>
						    </strong>
						    <select name="verifycation_email_id">
						    	<?php foreach($_SESSION['selected_client']->getCommonLetters() as $letter): ?>
						    		<?php if($_SESSION['selected_list']->verifycation_email_id == $letter->id): ?>
						    		<option value="<?=$letter->id?>" selected="selected"><?=$letter->name?></option>
						    		<?php else: ?>
						    		<option value="<?=$letter->id?>"><?=$letter->name?></option>
						    		<?php endif; ?>
						    	<?php endforeach; ?>
						    </select>
						</p>
						
					
	
	
	
					</div>					
					
					<h3 class="topPad">Feliratkozás után azonnal küldendő levél</h3>
					 <!-- end phDoubleIn -->
					
					<!-- 2./4. Subscription Confirmation Email -->
					<p>Miután valaki aktiválta (Szimpla Opt-in esetén regisztrálta) magát, a következő történjen:</p>

					<p>
					    <input type="radio" name="subscribe_conf_type" value="nothing" id="nothing" class="subscribe_conf_html_toogle" <?php echo ($form['subscribe_conf_type'] == "nothing") ? 'checked="checked"' : ""; ?>/>
					    <strong>
					        <label for="nothing" onClick="">&nbsp;&nbsp;Ne történjen semmi</label>
					    </strong>
					</p>
					
						<p>
						    <input type="radio" name="subscribe_conf_type" value="html" class="subscribe_conf_html_toogle" <?php echo ($form['subscribe_conf_type'] == "text" || $form['subscribe_conf_type'] == "html") ? 'checked="checked"' : ""; ?>/>
						    <strong>
						        <label for="verifycation_type" >&nbsp;&nbsp;Küldjel ki ezt a levelet: </label>
						    </strong>
						    <select name="subscribe_conf_email_id">
						    	<?php foreach($_SESSION['selected_client']->getCommonLetters() as $letter): ?>
						    		<?php if($_SESSION['selected_list']->subscribe_conf_email_id == $letter->id): ?>
						    		<option value="<?=$letter->id?>" selected="selected"><?=$letter->name?></option>
						    		<?php else: ?>
						    		<option value="<?=$letter->id?>"><?=$letter->name?></option>
						    		<?php endif; ?>
						    	<?php endforeach; ?>
						    </select>
						</p>					
					
				
				</div>
            </div>
            
            
            <div class="topPad"></div>
            
		
		
		</div>		
		
		
			            <div class="mybutton">
			                <button type="submit" class="button">
			                    <img src="<?=$base.$img ?>buttons/icon-tick.gif" width="16" height="16" alt="" />
								Feliratkozási folyamat mentése
			                </button>
			                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
			            </div>
			
						<div class="clearButton"></div>
					
					
					
		
				</form>	
		
		
		<!--<p class="extraBottomPad">Állítsa először be a <a href="#">feliratkozó formot</a>.</p>-->
		
		
	</div> <!--leftcol end-->
	
	<div id="rightcol">
	
		<div id="options">
		
			<div class="bghighlight"><h3 class="sidebar">Feliratkozó form generálás</h3></div>
		
			<p>
			<img src="<?=$base.$img?>icons/subscribeForm.png" width="32" height="32" alt="Subscribe Form" class="promoIcon">
			Ha beállította a lista feliratkozási folyamatát tovább mehet a feliratkozó formok generálásához. 
			</p>

			
			<div class="sidebarCTA"><a href="<?=url::base() ?>pages/formoverview">Feliratkozó formok</a> &raquo;</div>
	
	
		<!-- --------------------------------- -->
		</div>
	
	</div> <!--rightcol end-->

	
	<div class="clear"></div>
	
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
				
				<?php 
				/// beszúrás dialog
				
				$sepStart = Kohana::config('core.fieldSeparatorStart');
				$sepEnd = Kohana::config('core.fieldSeparatorEnd');
				$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
				$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
				$activateLink = $sepStart.Kohana::config('core.activation_link_name').$sepEnd;
				$email = $sepStart."email".$sepEnd;
				$regdatum = $sepStart."regdatum".$sepEnd;				
				?>

	                <script>
	                $(function(){

	            		var beszuras_dialog = function(){ 
	            			var dialog = $("#beszuras_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".beszurasClick").click(function(){
	            			var rel = $(this).attr('rel');	
	            			$('#fieldId').val(rel);		
	            			beszuras_dialog();
	            			$('#beszuras_dialog').dialog('open');
	            			$('#beszuras_dialog').show();
	            			
	            		});

	                	
	    				var SELECTED_LIST = "";
						$('#altalanosInsert').click(function(){

							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#altalansTag').val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								if(tag == '<?=$unsubLink ?>'){
									tag = '<a href="<?=$unsubLink ?>">Leiratkozás</a>';
								}else if(tag == '<?=$datamodLink ?>'){
									tag = '<a href="<?=$datamodLink ?>">Adatmódosítás</a>';
								}else if(tag == '<?=$activateLink ?>'){
									tag = '<a href="<?=$activateLink ?>">Aktiválás</a>';
								}
								
								 tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag);
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}
							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		  
						});
	
	                	$('#listaInsert').click(function(){
							var id = $('#fieldId').val();
							var content = $('#'+id).val();
							var tag = $('#'+$('#listak').val()).val();

							if(tag == "null" || tag == null){
								tag = "";
							}
							
							if(id.indexOf("_editor") >= 0){
								tinyMCE.execInstanceCommand(id,"mceInsertContent",false,tag)
							}else if(id.indexOf("_text") >= 0){
								$('#'+id).val(content+""+tag);
								$('#'+id).scrollTop(100000);
							}else{
								$('#'+id).val(content+""+tag);
							}

							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		                	
	                	});
	                	
	                	$('#listak').change(function (){
	                		$('.fieldContainer').hide();
	                		$('#'+$('#listak').val()).show();
	                	});
	                	
	                });
	                </script>

<div id="beszuras_dialog" title="Behelyettesítő beszúrása" style="display:none">
        		<div id="behelyettesito">
	        		<input type="hidden" id="fieldId" value=""/>
	        		<table>
	        			<tr>
	        				
	        				<td valign="top">
		        				<div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>		
	
				                <select size="5" style="width:210px" id="altalansTag">
				                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
				                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
				                	<option value="<?=$activateLink ?>"><?=$activateLink ?> - aktiváló url</option>
				                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
				                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
				                </select>
				                
	        				</td>
	        				
	        				<td valign="top" align="right">

								<div class="bghighlight" style="text-align:center"><h3 class="sidebar">Listák mezői:</h3></div>
								Lista:
				                <select size="1" style="width:170px" id="listak">
				                	<?php foreach($lists as $list): 
				                			if($listName != $list->name) continue;
				                	?>
				                		<option value="list_<?=$list->id ?>"><?=$list->name ?></option>
				                	<?php endforeach; ?>
				                </select>
								<p><br/></p>
									Mező:
				                	<?php 
				                		$first = true;
				                		foreach($lists as $list):  
				                		
				                			if($listName != $list->name) continue;
				                		
				                			$fields = $list->fields();
				                	?>
										
										<select class="fieldContainer" size="1" style="width:170px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="list_<?=$list->id ?>">			
											<?php foreach($fields as $field): ?>
											<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
											<?php endforeach; ?>
										</select>
				                	<?php endforeach; ?>
				
				                
	        				</td>
	        				
	        			</tr>

						<tr>
							<td align="center">
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="altalanosInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							<td align="center">
							
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="listaInsert">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div> 								
							
							</td>
						</tr>
	        		</table>
				</div>
</div>
<?php /// beszúrás dialog ?>
