<?php 

$gomb = "Előfizetői profil mentése";
$title = 'Előző hónapok egyenlege';

?>
<style>

.clearfix label{
	width:120px;
}

</style>

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>
		
			<?=$errors ?>
	    
	

	
	<div id="rightcol">

				<div id="options">

					<div class="bghighlight"><h3 class="sidebar">Előfizetői profil oldalai</h3></div>
					<dl class="icon-menu">		
		
		                <dt><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldLink">Előfizetői adatok</a></dd>
						<div class="clear"></div>
						
		                <dt><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldLink">Aktuális hónap egyenlege</a></dd>
						<div class="clear"></div>
		                
		                <dt><a href="<?=url::base() ?>pages/accountoverview/prevmonth" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/prevmonth" id="addCustomFieldLink">Előző hónapok egyenlege</a></dd>
						<div class="clear"></div>
		
						<dt><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Segments" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsLink">Eddig kiküldött számlák</a></dd>
						<div class="clear"></div>
		            </dl>
					</div>

					<div class="newFeatures">
                    &nbsp;
					</div>

				</div>

	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
