<?php 

$gomb = "Felhasználói profil mentése";
$title = 'Felhasználói profil szerkesztése';

?>
<style>

.clearfix label{
	width:120px;
}

</style>

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">

	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>

			<?=$errors ?>
	    
	    <div class="formBG">
		<div class="formWrapper">
			<form action="<?=url::base() ?>pages/useroverview" name="editAccount" method="post">
			<input type="hidden" name="referer" value="<?=$cancelLink ?>"/>
			<h3>Felhasználó adatai</h3>
			<div class="formContainer">
				<div class="clearfix">
				<label>Felhasználónév</label><input class="input_text" name="username" id="name" type="text" size="45" value="<?= $_SESSION['auth_user']->username ?>" disabled="disabled"/>
				</div>
				<div class="clearfix">
				<label>E-mail</label><input class="<?=$classes['email'] ?> input_text" name="email" type="text" size="45" value="<?=$form['email'] ?>" /> <span class="left"></span>
				</div>
				<div class="clearfix">
				<label>Vezetéknév</label><input class="<?=$classes['lastname'] ?> input_text" name="lastname" type="text" size="45" value="<?=$form['lastname'] ?>" />
				</div>
                <div class="clearfix">
				<label>Keresztnév</label><input class="<?=$classes['firstname'] ?> input_text" name="firstname" type="text" size="45" value="<?=$form['firstname'] ?>" />
				</div>
			</div>
            
            <h3>Jelszóváltoztatáshoz töltse ki az alábbi mezőket is:</h3>
            
            <div class="formContainer">
					<div class="clearfix">
						<label>Új jelszó</label><input class="<?=$classes['newpassword'] ?> input_text" name="newpassword" type="password" size="45" value="" />
					</div>
					<div class="clearfix">
						<label>Új jelszó mégegyszer</label><input class="<?=$classes['newpassword2'] ?> input_text" name="newpassword2" type="password" size="45" value="" />
					</div>
                    <div class="clearfix">
					<strong>Sikeres jelszóváltoztatás után a rendszer automatikusan kilépteti és Önnek újra be kell jelentkezie az új jelszóval!</strong>					
					</div>
            </div>
			<div class="clear"></div>
			
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/accept.png" alt=""/> 
                    <?=$gomb ?>
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=$cancelLink ?>" id="cancelSaveChanges">mégsem</a></span>
                <div style="clear:both"></div>
            </div> 
            
		<div class="clearButton"></div>
			
		</form>
		
		</div>
		</div>
	
	</div>	

	
	<div id="rightcol">

				<div id="options">

					

					<div class="newFeatures">
                    &nbsp;
					</div>

				</div>

	</div>


			<div class="clear"></div>

			</div>
	</div>
</div>
