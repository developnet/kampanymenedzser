<script>
	var OFFSET = 0;
	var LIMIT = 500;
	var POSTARRAY = null;
		
	var ALL_COUNT = 0;
	var ACTIVE_COUNT = 0;
	var UNSUBSCRIBED_COUNT = 0;
	var DELETED_COUNT = 0;
	var UPDATED_COUNT = 0;
	var NEW_COUNT = 0;
	var ERROR_COUNT = 0;
						
		
$(function(){

	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:640,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		});
	};

	$(".method_toogle").click(function(){
		method($(this));
	});

	
	var method = function(radio) {
		if(radio){
			val = radio.val();
		}else{
			val = "<?=$form['method'] ?>";
		}
		
		if(val == "import"){
			$("#method_import").show();
			$("#method_export").hide();
		}else if(val == "export"){
			$("#method_import").hide();
			$("#method_export").show();		
		}else{
		}	
	};

	method();
	
	var postImportReq = function() {
		
			var headernum = $("#headernum").val();
			var import_file = $("#import_file").val();
			var first_line_header = $("#first_line_header").val();
			var list_id = $("#list_id_select").val();
			
			
			POSTARRAY =  {  "import_file": import_file, 
							"headernum": headernum,
							"first_line_header" : first_line_header };
				
			for(var i=0;i<headernum;i++){
				POSTARRAY["header_"+i] = $("#header_"+i).val();
			}				
		
		
		$.post("<?=url::base() ?>api/common/import/"+OFFSET+"/"+LIMIT+"/"+list_id, POSTARRAY, 
		  function(data){ 
		  
		  	json = eval(data);
		  	updateCounts(json);
		  	if(json.STATUS == "WORKING"){
		  		OFFSET = OFFSET + LIMIT;
		  		postImportReq();	
		  	}else if(json.STATUS == "READY"){
		  		$("#error_log").html(json.error_log);
				$(".import_loading").hide();
				$(".import_ready").show();
		  	}else{
		  	
		  	}
		  	
		    //alert("Data Loaded: " + data); 
		  } 
		);		
	};
	

	var updateCounts = function(json){
		
		ALL_COUNT = ALL_COUNT + json.now_imported;
		ACTIVE_COUNT = ACTIVE_COUNT + json.succes_active;
		UNSUBSCRIBED_COUNT = UNSUBSCRIBED_COUNT + json.succes_unsubscribed;
		DELETED_COUNT = DELETED_COUNT + json.succes_deleted;
		UPDATED_COUNT = UPDATED_COUNT + json.updated;
		NEW_COUNT = NEW_COUNT + json.new_count;
		ERROR_COUNT = ERROR_COUNT + json.errors;
	
	
		$("#all_count").html(ALL_COUNT);
		$("#active_count").html(ACTIVE_COUNT);
		$("#unsubscribed_count").html(UNSUBSCRIBED_COUNT);
		$("#deleted_count").html(DELETED_COUNT);
		$("#updated_count").html(UPDATED_COUNT);
		$("#new_count").html(NEW_COUNT);
		$("#error_count").html(ERROR_COUNT);
		
	};
	
	
	$("#continue_import_button").click(function(){
			var headernum = $("#headernum").val();
			var email = false;
			
			for(var i=0;i<headernum;i++){
				if($("#header_"+i).val() == "email"){
					email = true;
				}
			}				
		
			if(!email){
				alert("Állítsa be, hogy melyik az e-mail cím oszlopa!");
				return false;
			}else{
				dialog();				
				//$('#dialog').html($('#tartalom').html());
				$('#dialog').dialog('open');
				$('#dialog').show();
				$('.ui-dialog-titlebar-close').hide();	
				postImportReq();			
			}
	});
});

</script>



<div id="dialog" title="Importálás" style="display:none">

	<table width="90%" align="center">
		<caption>Importálás állapota</caption>

		<tr>
			<th><span id="createLabel">Eddig összesen</span></th>
			<td><span class="allJobs" id="all_count">0</span></td>
		</tr>		

		<tr>
			<th><span id="createLabel">Ebből aktív:</span></th>
			<td><span class="allJobs" id="active_count">0</span></td>
		</tr>	
		
		<tr>
			<th><span id="createLabel">Ebből leiratkozott:</span></th>
			<td><span class="allJobs" id="unsubscribed_count">0</span></td>
		</tr>	

		<tr>
			<th><span id="createLabel">Ebből törölt:</span></th>
			<td><span class="allJobs" id="deleted_count">0</span></td>
		</tr>	
		
		<tr>
			<th style="text-align:center"><span  class="import_loading" style="color:red">Az importálás folyamatban, kérem várjon!</span><span class="import_ready" style="display:none;color:green;">Az importálás befejeződött.</span></th>
			<td valign="middle"><span class="import_loading"><img src="<?=$base.$img ?>ajax-loader.gif"  alt="" align="middle"/></span><div class="import_ready" style="display:none"><a href="<?=url::base() ?>pages/subscribers" style="color:#227FAF;font-weight:bold">Vissza a feliratkozókhoz</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span id="error_log"></span></div></td>
		</tr>
		
		<tr>
			<th><span id="createLabel">Összesen új:</span></th>
			<td><span class="allJobs" id="new_count">0</span></td>
		</tr>	
		<tr>
			<th><span id="createLabel">Összesen módosított:</span></th>
			<td><span class="allJobs" id="updated_count">0</span></td>
		</tr>							
		<tr>
			<th><span id="createLabel">Összesen hibás:</span></th>
			<td><span class="allJobs" id="error_count">0</span></td>
		</tr>							

	</table>

</div>


	<div id="content">
        
		
			
			<p class="bread">
				<a href="<?=url::base() ?>/pages/subscribers">Feliratkozók</a>
				<span class="breadArrow">&nbsp;</span>
			    Feliratkozók importálása
			</p>			
			<h1 class="extraBottomPad">Feliratkozók importálása</h1>
		<?php $buttonName = "Mentés"; ?>
			
	        
			<?=$errors ?>
			
			<?php 
				if(isset($_SESSION['csverror'])){
					echo  $_SESSION['csverror'];
					unset($_SESSION['csverror']);
				}

				if(isset($_SESSION['alert'])){
					echo  $_SESSION['alert'];
					unset($_SESSION['alert']);
				}
				
			?>
			

		<div class="formBG">
		<div class="formWrapper">

		<div class="middleTopPad"></div>		

				<p>
				    <input type="radio" name="method_type" value="import" class="method_toogle" <?php echo ($form['method'] == "import") ? 'checked="checked"' : ""; ?>/>
				    <strong>
				        <label for="method_type" >&nbsp;&nbsp;Feliratkozók importálása</label>
				    </strong>
				</p>
				
				<div id="method_import" style="display:none">
					<p>Speciális oszlopok: regdatum = feliratkozás ideje, statusz - állapot(1=aktív, 2=leiratkozott, 3=törölt, 4=hibás, 5=aktiválás elött)</p>
					
					<?php if(!isset($fieldchoose)):?>
					
					<form action="<?=url::base() ?>pages/listexport/preimport" name="createList" method="post" enctype="multipart/form-data" >
					<div class="formContainer">
					
						<div class="clearfix">
							<!-- <label>Adatelválasztó:</label> -->
							<input type="hidden" name="separator_import" size="40" value="<?=$form['separator_import'] ?>" class="<?=$classes['separator_import'] ?> input_text" disabled="disabled"/>
						</div>					
						<div class="clearfix">
							<!-- <label>Adatjelölő:</label> -->
							<input type="hidden" name="quote_import" size="40" value="<?=$form['quote_import'] ?>" class="<?=$classes['quote_import'] ?> input_text" disabled="disabled"/>
						</div>	
						
						<div class="clearfix">
							<label>CSV fájl:</label>
							<input type="file" name="file_import" class="input_file" />
						</div>							
											
					</div>
	
			        <div class="mybutton">
		                <button type="submit" class="button">
		                    <img src="<?=$base.$img ?>buttons/icon-tick.gif" width="16" height="16" alt="" />
							Feliratkozók importálása CSV-ből
		                </button>
		                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
		            </div>
					
					<div class="clear"></div>
					</form>
				</div>	
				
					<?php else:?>
					
					<p>Válassza ki, hogy a CSV oszlopai melyik lista mezőnek feleljenek meg az import során.</p>
					
					<input type="hidden" name="import_file" value="<?=$importfile?>" id="import_file"/>
					<input type="hidden" name="headernum" value="<?=$headernum?>" id="headernum"/>
					Lista kiválasztása:
					<select name="list_id" id="list_id_select">
						<option value="0">CSV szerint</option>
						<?php foreach($lists as $l): ?>
						<option value="<?=$l->id ?>"><?=$l->name ?></option>
						<?php endforeach; ?>
					</select>
					
					<div class="formContainer">
						<div style="overflow:scroll;overflow-x:scroll;width:auto !important;">
						<table>
							<tr>
								
								<th>&nbsp;<th>
								
								<?php foreach($header as $key => $h):?>
								<th>
									
									<select name="<?=$key ?>" id="<?=$key ?>">
										<option value="skip">Ne importálja</option>
										<option value="email">E-mail cím</option>
										<option value="regdatum">Feliratkozás dátuma</option>
										<option value="statusz">Státusz</option>
										<optgroup label="Globális mezők">
											<?php foreach($globalfields as $f): ?>
												<option value="<?=$f->reference ?>"><?=$f->name ?></option>
											<?php endforeach; ?>
										</optgroup>
										<optgroup label="Extra mezők">
											<option value="tag">Címke</option>
											<option value="product_ordered">Termék (megrendelte)</option>
											<option value="product_purchased">Termék (megvásárolta)</option>
											<option value="product_failed">Termék (hibás)</option>											
											<option value="list">Lista</option>											
										</optgroup>
										<optgroup label="Lista mezők">
											<?php foreach($listfields as $f): ?>
												<option value="<?=$f->reference ?>"><?=$f->name ?></option>
											<?php endforeach; ?>
										</optgroup>										
										
									</select>
									<?php //echo form::dropdown(array('name'=>$key,'id'=>$key),$fielddrop,$h);?>
									
								</th>
								<?php endforeach;?>
							
							</tr>

							<?php $i=1;foreach($rows as $row):?>
							<tr>
								<th><?=$i?>.<th>
							<?php foreach($header as $key => $h):?>
								<td>
									<input type="text" disabled="disabled" value="<?=$row[$key]?>" />	
								</td>
							<?php endforeach;$i++;?>
							</tr>
							<?php endforeach;?>
						</table>
						</div>
					
					
						<table>
							<tr>
								<td valign="center"><input type="checkbox" id="first_line_header" name="first_line_header" size="40" value="1" class=" input_text" checked="checked"/></td>
								<td valign="center">Az első sor a fejléc (importálás a második sortól)</td>
							</tr>
						</table>
					</div>
	
			        <div class="mybutton">
		                <button type="submit" class="button" id="continue_import_button">
		                    <img src="<?=$base.$img ?>buttons/icon-tick.gif" width="16" height="16" alt="" />
							Tovább &rarr;
		                </button>
		                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
		            </div>
					
					<div class="clear"></div>
										
					
					
					<?php endif;?>
								


		
		<br />

		</div>
		</div>	

	<div class="clear"></div>




	
	</div>