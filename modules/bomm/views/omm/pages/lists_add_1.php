	<div id="content">
		
		<?php if(!isset($_SESSION['selected_list'])): ?>
		
		<div id="listMake">
	        <div id="Steps">
	        	<div class="Container Current">
	            	<div class="StepsCubes Current">1</div>
	                Lista adatai
	            </div>
	            <div class="Separater">></div>
	            <div class="Container">
	            	<div class="StepsCubes">2</div>
	                Lista mezői
	            </div>
	        </div>
        	<h1 class="step1">1. lépés: A lista adatainak megadása</h1>
		</div>

		<h1 class="extraBottomPad">Új lista felivtele</h1>
		<?php 
			$buttonName = "Tovább";
			$buttonAlign = "right";
		 ?>
		<?php else: ?>

        
			<p class="bread">
				<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
				<span class="breadArrow">&nbsp;</span>
					<a href="<?=url::base() ?>pages/listdetail/index"><?=$_SESSION['selected_list']->name?></a>
				<span class="breadArrow">&nbsp;</span>
					<a href="">Lista adatainak módosítása</a>
			</p>		
		
			<h1 class="extraBottomPad">"<?=$_SESSION['selected_list']->name?>" lista adatainak módosítása</h1>
		<?php 
			$buttonName = "Mentés"; 
			$buttonAlign = "left";
		?>
		<?php endif; ?>
			
			
			
			
		
	        
			<?=$errors ?>


		<div class="formBG">
		<div class="formWrapper">

			<form action="<?=url::base() ?>pages/listadd/first" name="createList" method="post">	

			<h3>Lista neve</h3>
			<p>Adjon egy beszédes nevet a listának, ami alapján késöbb a rendszerben könnyen be tudja azonosítani.</p>

			<div class="formContainer">		
				<div class="clearfix">
					<label class="middle">Lista neve</label>
				
					<input class="<?=$classes['name'] ?> input_text" id="name" name="name" type="text" size="35" value="<?=$form['name'] ?>" />
				
				</div>
  
                <div class="clearfix">
					<label  class="middle">Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>

			</div>
			
			<h3 class="topPad">Lista típusa</h3>
			<p><strong>Szimpla Opt.in:</strong> A feliratkozók az űrlap kitöltése és elküldése után azonnal a lista aktív tagjai lesznek.<br/> 
			<strong>Kettős Opt-in:</strong> A feliratkozó csak azután kerül be az az e-mail listába aktív státusszal, miután a feliratkozás megerősítését kérő levélből átkattintott.</p>

			<div class="formContainer">		
				<div class="clearfix">
				<label class="middle">Lista típusa</label>
				
				<select name="type" class="<?=$classes['type'] ?>">
                	
                	<?php if($form['type'] == "single"): ?>
                		<option value="single" selected="selected">Szimpla Otp-in (azonnali feliratkozás)</option>
                		<option value="double">Kettős Opt-in (megerősítés szükséges)</option>
					<?php else: ?>
                		<option value="single" >Szimpla Otp-in (azonnali feliratkozás)</option>
						<option value="double" selected="selected">Kettős Opt-in (megerősítés szükséges)</option>
                	<?php endif; ?>
                	
                </select>
				
				</div>
			
			</div>
			
			<input type="hidden" name="unique_email" value="1" />					
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			<td width="100%" align="<?=$buttonAlign ?>">
            <!--<input type='image' src='<?=$base.$img ?>buttons/next.gif' width='85' height='40' border='0' tabindex='10'>-->
            
            		<div class="mybutton" style="float:<?=$buttonAlign ?>">
                        <button type="submit" class="button rightbutton">
                            <?=$buttonName ?>
                            <img src="<?=$base.$img ?>icons/arrow_right.png" alt="" class="right_mybutton" />
                        </button>
                        <div style="clear:both"></div>
                    </div>
            
            </td>
			</tr>
			</table>
			
		<div class="bottomPad"></div>
			

		</div>
		</div>	

	<div class="clear"></div>




	
	</div>