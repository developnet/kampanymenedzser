<script>

var SELECTED_HOUR = <?=$form['timing_hour'] ?>;
var FIRST = true;
var MODE = <?=$form['_time'] ?>;

function timer(){
	$.get("<?=url::base() ?>api/common/getTime", function(data){

		_data = data.split("|");	
		time = _data[3];
		day = _data[2];
		month = _data[1];
		year = _data[0];
		
		$("#server_time").html(time);
		setTimeout('timer()',1000);

		_time = time.split(":");
		hour = _time[0];

		selected = $('#timeselect').val();

		if(FIRST){
			selected = SELECTED_HOUR;
			FIRST = false;
		}
		
		$('#timeselect').html("");
		
		
		seldate = $('#base_time').val();

		_date = seldate.split("-");

		selyear = _date[0];
		selmonth = _date[1];
		selday = _date[2];

		//console.log(selyear+" - "+year);
		//console.log(selmonth+" - "+month);
		//console.log(selday+" - "+day);
		var first = false
		for (i = 0; i < 24; i++) {
			if(i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 ) continue;

			if(selyear == year && selmonth == month && selday == day){
					
				//console.log(i+" - "+hour*1);

				if(i > hour*1){
					
					if(hour < 2 && !first){
						$('#timeselect').append('<option value="0">2 és 6 óra között</option>');
						first = true;
					}				
					


					$('#timeselect').append('<option value="'+i+'">'+i+' órakor</option>');
				}
				
			}else{
			
				if(!first){
					$('#timeselect').append('<option value="0">2 és 6 óra között</option>');
					first = true;
				}				
			
				$('#timeselect').append('<option value="'+i+'">'+i+' órakor</option>');
					
			}
			
			//if(i > 
			
		}

		$('#timeselect').val(selected);
		  
	});	
}

$(function(){
		
	 $('.datepicker').datepicker({ 
		 dateFormat: 'yy-mm-dd', 
		 minDate: new Date(<?=date( "Y,m,d", strtotime( date("Y-m-d")." -1 month" ) ) ?>)
			});


////////////////////////////////////////////

	$('.saveandexit').click(function(){
		$('#exitinput').val('true');
		document.forms['formtosubmit'].submit();
		return false;
	});

	
	$('.toprevpage').click(function(){
		
		//var answer = confirm("Menti az időzítés adatait?")
		
		//if(answer){
			$('#exitinput').val('toprevpage');
			document.forms['formtosubmit'].submit();
			return false;
		//}else{
			//window.location = '<?=url::base() ?>pages/letterdetail/test';
			//return false;
		//}
	});	

	$('.tonextpage').click(function(){
		
		//var answer = confirm("Menti az időzítés adatait?")
		
		//if(answer){
			$('#exitinput').val('tonextpage');
			document.forms['formtosubmit'].submit();
			return false;
		//}else{
			//window.location = '<?=url::base() ?>pages/letterdetail/recipients';
			//return false;
		//}
	

	});
	<?php /* 
	//$('#clockicon').click(function(){
		$('#selecttime').toggle();
		$('#noselecttime').toggle();

		if($("#timeselect_mode").val() == 'true'){
			$("#timeselect_mode").val('false');
		}else{
			$("#timeselect_mode").val('true');
		}
		
	});
	*/ ?>
	//console.log("aaa:"+$("#timeselect_mode").val());
	

	
	timer();
	
});

</script>
<!-- CONTENT -->

		<div id="singleDoubleContent">
		
		<div id="listMake">
		<div id="Steps">
		<div class="Container">
			<div class="StepsCubes">1</div>
			Levél adatai</div>
		<div class="Separater">></div>
		<div class="Container">
			<div class="StepsCubes">2</div>
			Levél tartalma</div>
		<div class="Separater">></div>
		<div class="Container">
			<div class="StepsCubes">3</div>
			Tesztküldés</div>
		<div class="Separater">></div>
		<div class="Container  Current">
			<div class="StepsCubes  Current">4</div>
			Időzítés</div>
		<div class="Separater">></div>
		<div class="Container">
		<div class="StepsCubes">5</div>
			Címzési feltételek</div>
		</div>
		
		
		<h1 class="step1">4. lépés: Időzítés beállítása</h1>
        <p style="padding-left:20px">
        	<strong>Levél neve:</strong> <?=$letter->name ?><br/>
        	<strong>Levél tárgya:</strong> <?=$letter->subject ?><br/>
        </p>

</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0"
	class="LettersDetailsButtonsBackground">
	<tr>
		<td width="42%">
		<div class="mybutton"><a href="javascript:;" class="button toprevpage">
		<img src="<?=$base.$img ?>icons/arrow_left.png" alt="" /> Vissza a
		Tesztküldéséhez </a></div>
		</td>
		<td>
		<div class="mybutton"><a href="javascript:;"
			class="button saveandexit"> <img src="<?=$base.$img ?>icons/save.png"
			alt="" /> Ment és kilép </a></div>
		</td>
		<td width="42%">
	        <div class="mybutton" style="float:right">    
		        <a href="javascript:;" class="button rightbutton tonextpage">
					Tovább a címzési feltétekhez&nbsp;
		            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
		        </a>
	        </div> 
		</td>
	</tr>
</table>

</div>
<!--singleDoubleContent end-->

<div id="content">

		<?=$alert ?>           
		<?=$errors ?>

<div class="formBG">
<div class="formWrapperRadios">

<form name="formtosubmit" method="post"
	action="<?=url::base() ?>pages/letterdetail/timing" id="_ctl0"><input
	type="hidden" name="exit" value="false" id="exitinput" />



<div class="bigRadio"><input type="radio" name="timing_type"
	value="relative" id="now"
	<?php echo ($letter->timing_type == "relative") ? 'checked="checked"' : ""; ?> />
<label for="now" class="h3checkbox">&nbsp;Relatív időzítés</label>
<p></p>

<div class="bigRadioDescription">
<div class="formContainer"><?php //e helyett majd lehetnek radio gombok, ha több féle időzítés típus lesz  ?>
<input type="hidden" name="timing_event_type" value="fromsubscribe" />

<table width="100%" cellspacing="0" cellpadding="5" border="0">
	<tr>
		<td nowrap class="mid">&nbsp;Levél küldése feliratkozás után</td>
		<td nowrap><input type="text" name="fromsubscribe_timing_event_value"
			size="2" value="<?=$form['fromsubscribe_timing_event_value'] ?>"
			class="input_text" /></td>
		<td width="100%" class="mid">nappal</td>
	</tr>
</table>
</div>
</div>
</div>

<div class="bigRadio"><input type="radio" name="timing_type"
	id="absolute" value="absolute"
	<?php echo ($letter->timing_type == "absolute") ? 'checked="checked"' : ""; // ?>>
<label for="schedule" class="h3checkbox">&nbsp;Abszolút időzítés</label>
<p></p>

<div class="bigRadioDescription">
<div class="formContainer">

<table cellSpacing=0 cellPadding=5 width="100%" border="0" class="dates">
	<tr>
		<td class=mid noWrap width="95">&nbsp;Dátum (év/hó/nap)</td>
		<td width="140">
		<table cellSpacing=0 cellPadding=0 border=0 class="noCellPad">
			<tr>
				<td>
					<input type="hidden" name="timeselect_mode" id="timeselect_mode" value="<?=$form['_time'] ?>"/>
					<input type="text" name="base_time" id="base_time"
					value="<?=$form['base_time'] ?>" class="datepicker input_text" /></td>
			</tr>
		</table>
		</td>
		<td width="340">
		<div id="noselecttime" style="display: none"><span>A levél 2 óra és 6
		óra között kerül kiküldésre</span></div>
		<div id="selecttime" style="display: block">Ekkor kerüljön kiküldésre:
		<select name="timing_hour" style="" id="timeselect"></select> &nbsp;</div>
		</td>
		<td width="150">
		<div id="servertime" style="display: block">Szerver idő: <span
			id="server_time"><?=date('H:i') ?></span></div>
		</td>
		<td style="text-align: right">
		<div id="clockicon" style="display: block"><a href="javascript:;" class=""><img
			src="<?=$base.$img ?>buttons/clock.png" width="18" height="18" alt="" /></a>
		</div>
		</td>
	</tr>
	<tr>

</table>
</div>
</div>
</div>
</form>

</div>
</div>
<br>


<table width="100%" cellspacing="0" cellpadding="0" border="0"
	class="LettersDetailsButtonsBackground">
	<tr>
		<td width="42%">
		<div class="mybutton"><a href="javascript:;" class="button toprevpage">
		<img src="<?=$base.$img ?>icons/arrow_left.png" alt="" /> Vissza a
		Tesztküldéséhez </a></div>
		</td>
		<td>
		<div class="mybutton"><a href="javascript:;"
			class="button saveandexit"> <img src="<?=$base.$img ?>icons/save.png"
			alt="" /> Ment és kilép </a></div>
		</td>
		<td width="42%">
			<div class="mybutton" style="float:right">    
		        <a href="javascript:;" class="button rightbutton tonextpage">
					Tovább a címzési feltétekhez&nbsp;
		            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
		        </a>
	        </div> 
		</td>
	</tr>
</table>




<div class="clear"></div>
</div>
<!-- CONTENT VÉGE -->
