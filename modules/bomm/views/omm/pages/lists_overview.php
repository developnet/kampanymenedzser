<script>
	$(function(){
		var SELECTED_LIST = 0;

		var SELECTED_LISTS_FOR_ARCHIVE = new Array();

		var SELECTED_LISTS_FOR_ACTIVATE = new Array();

		var SHOW = false;
		
		var REL = 'nothing';
		
  		$(".listRow").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				//alert("#client_"+id+"_delete");
				
				$("#list_"+id+"_delete").show();

				$("#list_"+id+"_select").show();	
				
				
			} );

			$(this).mouseout( function() {
				
				$("#list_"+id+"_delete").hide();


				if((jQuery.inArray(id, SELECTED_LISTS_FOR_ARCHIVE) == -1) || (jQuery.inArray(id, SELECTED_LISTS_FOR_ARCHIVE) == 'undefined')){

					if(!SHOW){
						$("#list_"+id+"_select").hide();
					}
							
					
				}
				
				
				
			} );
 
 
		});


 		$(".delete_lists").click(function(){
	
 			REL = $(this).attr("rel");

 			if(REL == 'archive'){
 				if(SELECTED_LISTS_FOR_ARCHIVE.length < 1 ){
 					alert("Nincs lista kiválasztva!");
 				}else{
 					deleteList();
 				}
 	 		}else if(REL == 'active'){
 	 			if(SELECTED_LISTS_FOR_ACTIVATE.length < 1 ){
 					alert("Nincs lista kiválasztva!");
 				}else{
 					deleteList();
 				}
 	 	 	}


								
		});
  		
	var dialog = function(id){ 
			var dialog = $("#"+id).dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteList = function(){ 
			dialog('dialog');				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){

		operator = "delete";

		if(REL == 'archive'){
			POSTARRAY =  {  "operator": operator, 
					"selected_lists": SELECTED_LISTS_FOR_ARCHIVE.length
					};

			for(var i=0;i<SELECTED_LISTS_FOR_ARCHIVE.length;i++){
				POSTARRAY["list_"+i] = SELECTED_LISTS_FOR_ARCHIVE[i]; 
			}
		}else if(REL == 'active'){

			POSTARRAY =  {  "operator": operator, 
					"selected_lists": SELECTED_LISTS_FOR_ACTIVATE.length
					};

			for(var i=0;i<SELECTED_LISTS_FOR_ACTIVATE.length;i++){
				POSTARRAY["list_"+i] = SELECTED_LISTS_FOR_ACTIVATE[i]; 
			}
			
		}
	 	
		$.post("<?=url::base() ?>api/common/deleteLists", POSTARRAY, 
		  function(data){ 
		  
		  	json = eval(data);

		  	if(json.STATUS == "ERROR"){
				alert('Hiba a törlés közben!');
		  	}else if(json.STATUS == "SUCCES"){
				window.location.reload();
		  	}else{
		  	
		  	}
		  	
		  }); 
		  			

			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			//window.location = '<?=url::base() ?>pages/listoverview/deleteList/'+SELECTED_LIST;
					
	});		

		$(".select_display").click(function(){

			if(SHOW) {
				SHOW = false;
			}else {
				SHOW = true;
			}
				
			 $(".list_select_for_archive").each(function(){
				 var letter_id = $(this).attr('rel');

					if((jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ARCHIVE) == -1) || (jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ARCHIVE) == 'undefined')){
						$("#list_"+letter_id+"_select").toggle();	
					}				 
					
			});

		});
		
	 $(".list_select_for_archive").click(function(){
			var letter_id = $(this).attr('rel');

			if((jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ARCHIVE) == -1) || (jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ARCHIVE) == 'undefined')){
				SELECTED_LISTS_FOR_ARCHIVE.push(letter_id);
				$(this).parent().parent().addClass('selected_letter_row');
			}else{
				SELECTED_LISTS_FOR_ARCHIVE.splice(SELECTED_LISTS_FOR_ARCHIVE.indexOf(letter_id), 1);
				$(this).parent().parent().removeClass('selected_letter_row');
			}

	});	

	 $(".list_select_for_activate").click(function(){
			var letter_id = $(this).attr('rel');

			if((jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ACTIVATE) == -1) || (jQuery.inArray(letter_id, SELECTED_LISTS_FOR_ACTIVATE) == 'undefined')){
				SELECTED_LISTS_FOR_ACTIVATE.push(letter_id);
				$(this).parent().parent().addClass('selected_letter_row');
			}else{
				SELECTED_LISTS_FOR_ACTIVATE.splice(SELECTED_LISTS_FOR_ACTIVATE.indexOf(letter_id), 1);
				$(this).parent().parent().removeClass('selected_letter_row');
			}

	});		 

		$("#archive_lists").click(function(){
			if(SELECTED_LISTS_FOR_ARCHIVE.length < 1 ){
				alert("Nincs lista kiválasztva!");
			}else{
				dialog('dialogArchive');				
				$('#dialogArchive').dialog('open');
				$('#dialogArchive').show();
			}
			
		});

	 	$('#archiveCancelButtonCampaign').click(function(){
			$('#dialogArchive').dialog('close');
			$('#dialogArchive').hide();		
		});

 		$('#archiveYesButtonCampaign').click(function(){

			operator = "archive";
		 	
			POSTARRAY =  {  "operator": operator, 
							"selected_lists": SELECTED_LISTS_FOR_ARCHIVE.length
							};
		
			for(var i=0;i<SELECTED_LISTS_FOR_ARCHIVE.length;i++){
				POSTARRAY["list_"+i] = SELECTED_LISTS_FOR_ARCHIVE[i]; 
			}	
			
			
			$.post("<?=url::base() ?>api/common/archiveLists", POSTARRAY, 
			  function(data){ 
			  
			  	json = eval(data);

			  	if(json.STATUS == "ERROR"){
					alert('Hiba az archiválás közben!');
			  	}else if(json.STATUS == "SUCCES"){
					window.location.reload();
			  	}else{
			  	
			  	}
			  	
			  }); 			

 			$('#dialogArchive').dialog('close');
			$('#dialogArchive').hide();
		});	


		$("#activate_lists").click(function(){
			if(SELECTED_LISTS_FOR_ACTIVATE.length < 1 ){
				alert("Nincs lista kiválasztva!");
			}else{
				dialog('dialogActivate');				
				$('#dialogActivate').dialog('open');
				$('#dialogActivate').show();
			}
			
		});

	 	$('#activateCancelButtonCampaign').click(function(){
			$('#dialogActivate').dialog('close');
			$('#dialogActivate').hide();		
		});

 		$('#activateYesButtonCampaign').click(function(){

			operator = "active";
		 	
			POSTARRAY =  {  "operator": operator, 
							"selected_lists": SELECTED_LISTS_FOR_ACTIVATE.length
							};
		
			for(var i=0;i<SELECTED_LISTS_FOR_ACTIVATE.length;i++){
				POSTARRAY["list_"+i] = SELECTED_LISTS_FOR_ACTIVATE[i]; 
			}	
			
			
			$.post("<?=url::base() ?>api/common/archiveLists", POSTARRAY, 
			  function(data){ 
			  
			  	json = eval(data);

			  	if(json.STATUS == "ERROR"){
					alert('Hiba az aktiválás közben!');
			  	}else if(json.STATUS == "SUCCES"){
					window.location.reload();
			  	}else{
			  	
			  	}
			  	
			  }); 			

 			$('#dialogActivate').dialog('close');
			$('#dialogActivate').hide();
		});	


		$('.archive_open_click').click(function(){
			$('.archive_list').toggle();
			$('#archive_close').toggleClass("open");
		});
 		
	});

	
</script>
<div id="dialogActivate" title="Listák archiválása" style="display:none">
	
    <div class="activate_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt="" style="float:left"/>
    	<strong>Valóban vissza szeretné állítani a kiválasztott listákat?</strong>
    	<br /><br />
    </div>
	
    <div class="activate_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="activateYesButtonCampaign" type="submit" class="button"  style="margin-left:40px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Igen
            </button>
        </div>
        <div class="mybutton" id="sendingMessage" >
            <button id="activateCancelButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Mégsem
            </button>
        </div>	
    </div>

</div>	

<div id="dialogArchive" title="Listák archiválása" style="display:none">
	
    <div class="archive_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt="" style="float:left"/>
    	<strong>Valóban archiválni szeretné a kiválasztott listákat?</strong>
    	<br /><br />
    </div>
	
    <div class="archive_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="archiveYesButtonCampaign" type="submit" class="button"  style="margin-left:40px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Igen
            </button>
        </div>
        <div class="mybutton" id="sendingMessage" >
            <button id="archiveCancelButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Mégsem
            </button>
        </div>	
    </div>

</div>	

<div id="dialog" title="Lista törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Valóban végelgesen törölni szeretné táblázatban kijelölt listákat?</strong><br />
	A listák összes adata törlődni fognak!
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Lista törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli táblázatban kijelölt listákat?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	


	<div class="twocol">	
	<div id="content">
	
	
	
	<div id="leftcol">
	
		<?=$alert ?>
			
		<div style="height:43px;">
		
			<div id="toolbar_first" style="height:43px;text-align:right;float:left;">
				<h1>Hírlevél listák</h1>
			</div>
		
			<div id="toolbar" style="height:18px;text-align:right;float:right;width:400px;padding-top:25px">
					<a href="Javascript:;" class="delete_lists" rel="archive">Listák törlése</a> |  
					<a href="Javascript:;" id="archive_lists" >Listák archiválása</a> 
			</div>
			
			<div style="clear:both"></div>
		</div>			
		
		<!--<p class="bottomPad">To manage your lists, click on the list name below. You can create as many subscriber lists as you like.</p>-->
		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tr class="noHighlight">
				
				<th class="headerLeft" width="">
					<input type="checkbox" class="select_display" />
				</th>
				
				<th class="cellLeft" width="100%">Lista neve</th>
				<th nowrap="nowrap" width="100" class="cellLeft">Létrehozva<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
	            <th nowrap="nowrap" width="100" class="cellCenter">Csoportok<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
				<th nowrap="nowrap" width="100" class="headerRight">Feliratkozók<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
				
			</tr>
		
		<?foreach ($lists as $list): ?>
		
			<tr id="<?=$list->id ?>" class="listRow" rel="<?=$list->id ?>">
				
				<td  class="rowLeft">
					<input type="checkbox" class="list_select_for_archive" rel="<?=$list->id ?>" style="display:none;" id="list_<?=$list->id ?>_select"/>
				</td>
								
			    <td class="cellLeft">
			    	<a href="<?=url::base() ?>pages/listoverview/selectlist/<?=$list->id ?>" <?php echo (empty($list->note)) ? "" : 'title="'.nl2br($list->note).'"'; ?>><?=$list->name ?></a></td>
	            
	            <td class="cellLeft"><span><?=$list->created() ?></span></td>
			    <td class="cellCenter"><?=$list->groupNumber() ?> </td>
			    <td class="cellCenter" align="center"><?=$list->membersNumber() ?> </td>
	
			    
			</tr>
		
		<?endforeach; ?>
		
		<?php if(sizeof($lists) == 0): ?>
			<tr >
			
			    <td class="rowLeft" colspan="6" align="center">Még nem hozott létre listát. <a href="<?=url::base() ?>pages/listadd/first">Ide kattintva megteheti.</a></td>
			    
			</tr>		
		<?php endif; ?>
		
		<tr class="noHighlight">
			<td class="footerLeft simple" colspan="4">Összes feliratkozó</td>
			<td class="footerRight simple" align="center" style="padding: 4px 6px;"><strong><?=$membersSum ?></strong></td>
		</tr>
		</table>
		
		
		
		<div style="height:43px;">
		
			<div id="toolbar_first" style="height:43px;text-align:right;float:left;width:215px">
				<h1 style="float:left" class="archive_open_click">Archivált listák (<?=sizeof($archibed_lists)?>)</h1> 
				<div class="archive_open_click" id="archive_close" style="float:right;margin-top:13px;margin-left:10px"></div>
			</div>
		
			<div id="toolbar" style="height:18px;text-align:right;float:right;width:400px;padding-top:25px">
					<a href="Javascript:;" class="delete_lists archive_list" rel="active">Listák törlése</a><span class="archive_list"> | </span>
					<a href="Javascript:;" id="activate_lists" class="archive_list" >Listák visszaállítása</a> 
			</div>
			
			<div style="clear:both"></div>
		</div>		

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
		<tr class="noHighlight archive_list">
			<th class="headerLeft" width="100%">Lista neve</th>
			<th nowrap="nowrap" width="100" class="cellLeft">Létrehozva<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
            <th nowrap="nowrap" width="100" class="cellCenter">Csoportok<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
			<th nowrap="nowrap" width="100" class="headerRight">Feliratkozók<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
		</tr>
		
		<?foreach ($archibed_lists as $list): ?>
		
			<tr id="<?=$list->id ?>" class="listRow archive_list" rel="<?=$list->id ?>" >
				
			    <td class="rowLeft">
			    	<input type="checkbox" class="list_select_for_activate" rel="<?=$list->id ?>" />
			    	<a href="<?=url::base() ?>pages/listoverview/selectlist/<?=$list->id ?>" <?php echo (empty($list->note)) ? "" : 'title="'.nl2br($list->note).'"'; ?>><?=$list->name ?></a></td>
	            <td class="cellLeft"><span><?=$list->created() ?></span></td>
			    <td class="cellCenter"><?=$list->groupNumber() ?> </td>
			    <td class="cellCenter"><?=$list->membersNumber() ?> </td>
			    
			</tr>
		
		<?endforeach; ?>
		
		<?php if(sizeof($archibed_lists) == 0): ?>
			<tr class="archive_list">
			
			    <td class="rowLeft" colspan="5" align="center">Nincsen még archivált lista..</a></td>
			    
			</tr>		
		<?php endif; ?>
		
		<tr class="noHighlight archive_list">
			<td class="footerLeft simple" colspan="3">Összes feliratkozó</td>
			<td class="footerRight simple" align="center" style="padding: 4px 6px;"><strong><?=$archived_membersSum ?></strong></td>
		</tr>
		</table>		
		
	</div>
	
	<div id="rightcol">
		<div id="options">
			
        <div class="mybutton" style="float:left;padding-bottom:8px;">    
            <a href="<?=url::base() ?>pages/listadd/first" class="button">
                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                Új hírlevél lista létrehozása
            </a>
        </div>    
			
		</div>	
	</div>
	
	<div class="clear"></div>
	
	</div>
	</div>