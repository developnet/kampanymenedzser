		<style>
			.pagination strong{
				display:block;
				background:#FFFFFF none repeat scroll 0 0;
				display:block;
				float:left;
				padding:1px 4px;
				text-decoration:none;				
			}
			
			#listActivityChart { 
			}
			#resize{
				margin-bottom:10px;
			}
		</style>


<script type="text/javascript">


function ofc_resize(left, width, top, height){
	var tmp = new Array(
	'left:'+left,
	'width:'+ width,
	'top:'+top,
	'height:'+height );
	
	$("#resize_info").html( tmp.join('<br>') );
}


 $(function(){

 	$('#addtags_click').click(function(){
 		addtags();
 	});
 	
	var addtags_dialog = function(){ 
			var dialog = $("#addtagdialog").dialog({
			bgiframe: false,
			resizable: false,
			width:600,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	
		
 	var addtags = function(){ 
			addtags_dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#addtagdialog').dialog('open');
			$('#addtagdialog').show();	
	}					
	
	$('#addtagdialog_cancel').click(function(){
			$('#addtagdialog').dialog('close');
			$('#addtagdialog').hide();		
	});
	
	

});


</script>
<style>
.formContainer label {
	width: 100px;
	font-size: 11px;
	padding: 5px;
	float:none !important;
	font-size: 13px;
	
}

ul.as-list {
	margin:0 !important;
}

</style>		
	
<div id="addtagdialog" title="Címke hozzáadása" style="display:none">
    <div class="delete_dialog_question" style="width:570px;margin-bottom:0;">
   	<strong>Adja hozzá vagy törölje a következő címkéket</strong>
			<form id="tag_mod_form" name="subscribers" action="<?=url::base() ?>/pages/statisticdetails/recipients" method="post" style="width:570px">	
			<div class="formContainer">

				<div class="clearfix">
					<label>Címkék:</label><input class="tagsfield input_text" type="text" name="tags" value="">
				</div>
			
				<div class="clearfix">
					<input class="tinput_text" type="radio" name="selection_tag" value="all" id="all_recipient" checked="checked"><label for="all_recipient">Az összes címzetthez</label><br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="opened" id="opened" ><label for="opened">Annak, aki megnyitotta</label><br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="clicked" id="clicked" ><label for="clicked">Annak, aki átkattintott</label><br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="temperror" id="temp_error" ><label for="temp_error">Annak, aki átmeneti hibát kapott</label><br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="permerror" id="perm_error" ><label for="perm_error">Annak, aki állandó hibát kapott</label><br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="unsubscribed" id="subscribed" ><label for="subscribed">Annak, aki leiratkozott</label><br/>										
				</div>			
			
				<div class="clearfix">
					<input class="tinput_text" type="radio" name="mod_tag" value="add" checked="checked" id="add_tag"><label for="add_tag">Hozzáadás</label><br/>
					<input class="tinput_text" type="radio" name="mod_tag" value="del" id="del_tag"><label for="del_tag">Törlés</label><br/>					
				</div>				
			
			</div>
			</form>	
	
    </div>
	
    <div class="">
		<a href="Javascript:;" class="guibutton tags guisubmit" id="tag_add" rel="tag_mod_form">Címkék módosítása</a> <a href="Javascript:;" class="guibutton cancel" id="addtagdialog_cancel">Mégsem</a>
    </div>	
	
</div>	
		
<div class="twocol">
<!-- CONTENT -->
	<div id="content">

		<div id="leftcol">
        
	        <p class="bread"><a href="<?=url::base() ?>pages/statisticoverview">Statisztika</a><span class="breadArrow">&nbsp;</span><?=$letter->name ?> - Címzettek tevékenységei</p>
			
			<?=$alert ?>
			
	        <h1 id="campaignTitle">
	        <div class="campaignActions">
	            <div class="mybutton">    
	                <a href="<?=url::base()."api/common/showLetter/".$letter->id ?>" target="_blank" class="button">
	                    <img src="<?=$base.$img?>icons/search.png" alt=""/> 
	                    Levél megtekintése
	                </a>
	            </div> 
	        </div>
	        
	        	<?=$letter->name ?>
	        	<?php echo (empty($letter->note)) ? "" : '<a href="Javascript:;" title="'.nl2br($letter->note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?>
	        </h1>
			
			
			<p class="titleSummary">
			<span>tárgy:</span> <strong><?=$letter->subject ?></strong><br/>
	        <span>típus:</span> <strong><?=$letter->type ?></strong>
			</p>
			
				<?php
					if($letter->timing_type == "relative"){
						$tim = "Relatív";
						$sent_label = "<span>Levél küldése feliratkozás után</span> <strong>".$letter->timing_event_value." nappal</strong>";
					}else if($letter->timing_type == "absolute"){
						$tim = "Abszolút";
						$sent_label = "<span>elküldve:</span> <strong>".dateutils::formatDate($letter->isSent(TRUE))."</strong>";
					}else if($letter->timing_type == "sendnow"){
						$tim = "Azonnali";
						$sent_label = "<span>elküldve:</span> <strong>".dateutils::formatDate($letter->isSent(TRUE))."</strong>";
					}
				?>		
			
			<p class="titleSummary">
	        <span>időzítés:</span> <strong><?=$tim ?></strong>
			</p>
			
			
			<p class="titleSummary">
	        	<?=$sent_label ?>
			</p>
        
		<form name="displaySubscribers" method="post" action="#">
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabs">
		<tr>			
			<?php 
				
				$stat = $letter->getLetterLinkStat();
			?>
			<?php if($memberStatus == "all"): ?>
			
	        	<td class="tabOnLeft" nowrap><span>Összes címzett</span><?=$stat['sentNumber'] ?></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnyitott</a></span><?=$stat['uniqueOpens'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Átkattintott</a></span><?=$stat['uniqueClicks'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Átmeneti hiba</a></span><?=$stat['tempErrors'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Állandó hiba</a></span><?=$stat['permErrors'] ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Leiratkozott</a></span><?=$stat['unsubscribeClicks'] ?></div></td>
            
            <?php elseif($memberStatus == "opened"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Összes címzett</a></span><?=$stat['sentNumber'] ?></td>
				<td class="tabOnLeft" nowrap><span>Megnyitott</span><?=$stat['uniqueOpens'] ?> </td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Átkattintott</a></span><?=$stat['uniqueClicks'] ?> </td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Átmeneti hiba</a></span><?=$stat['tempErrors'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Állandó hiba</a></span><?=$stat['permErrors'] ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Leiratkozott</a></span><?=$stat['unsubscribeClicks'] ?></div></td>            
            
            <?php elseif($memberStatus == "clicked"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Összes címzett</a></span><?=$stat['sentNumber'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnyitott</a></span><?=$stat['uniqueOpens'] ?></td>
				<td class="tabOnLeft" nowrap><span>Átkattintott</span><?=$stat['uniqueClicks'] ?></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Átmeneti hiba</a></span><?=$stat['tempErrors'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Állandó hiba</a></span><?=$stat['permErrors'] ?></td>
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Leiratkozott</a></span><?=$stat['unsubscribeClicks'] ?></div></td>            
            
            <?php elseif($memberStatus == "unsubscribed"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Összes címzett</a></span><?=$stat['sentNumber'] ?></td>
				<td class="tabOffLeftOfOn" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnyitott</a></span><?=$stat['uniqueOpens'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Átkattintott</a></span><?=$stat['uniqueClicks'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Átmeneti hiba</a></span><?=$stat['tempErrors'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Állandó hiba</a></span><?=$stat['permErrors'] ?></td>
				<td class="tabOnLeft" nowrap><div><span>Leiratkozott</span><?=$stat['unsubscribeClicks'] ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>            

            <?php elseif($memberStatus == "temperror"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Összes címzett</a></span><?=$stat['sentNumber'] ?></td>
				<td class="tabOffLeftOfOn" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnyitott</a></span><?=$stat['uniqueOpens'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Átkattintott</a></span><?=$stat['uniqueClicks'] ?></td>
				<td class="tabOnLeft" nowrap><span>Átmeneti hiba</span><?=$stat['tempErrors'] ?></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>				
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/permerror">Állandó hiba</a></span><?=$stat['permErrors'] ?></td>
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Leiratkozott</a></span><?=$stat['unsubscribeClicks'] ?></div></td>

            <?php elseif($memberStatus == "permerror"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/all">Összes címzett</a></span><?=$stat['sentNumber'] ?></td>
				<td class="tabOffLeftOfOn" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/opened">Megnyitott</a></span><?=$stat['uniqueOpens'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/clicked">Átkattintott</a></span><?=$stat['uniqueClicks'] ?></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/temperror">Átmeneti hiba</a></span><?=$stat['tempErrors'] ?></td>				
				<td class="tabOnLeft" nowrap><span>Állandó hiba</span><?=$stat['permErrors'] ?></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/statisticdetails/recipients/unsubscribed">Leiratkozott</a></span><?=$stat['unsubscribeClicks'] ?></div></td>

            <?php endif; ?>
            
            <td width="100%" align="right" class="tableHeaderCap">
				<a href="Javascript:;" class="guibutton tags" id="addtags_click">Címkék kezelése</a> 
				
			</td>
		</tr>
		</table>
		

		
		
		<?php /*TÁBLÁZAT**********************************************************************/ ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabsHeader">
		<tr class="noHighlight">

			<?php /*EMAIL**********************************************************************/ ?>
			<?php if($orderby=="email" && $order=="asc"): ?>
		
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/email/desc" title="Rendezés desc szerint">
						e-mail cím
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="email" && $order=="desc"): ?>		
			
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/email/asc" title="Rendezés asc szerint">
						e-mail cím
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/email/asc" title="Rendezés asc szerint">
						e-mail cím
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*EMAIL**********************************************************************/ ?>

			<?php /*MEGNYITÁS**********************************************************************/ ?>
			<?php if($orderby=="open" && $order=="asc" && $memberStatus != "temperror" && $memberStatus != "permerror"): ?>
		
				<th class="">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/open/desc" title="Rendezés desc szerint">
						Megnyitás
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="open" && $order=="desc"  && $memberStatus != "temperror" && $memberStatus != "permerror"): ?>		
			
				<th class="">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/open/asc" title="Rendezés asc szerint">
						Megnyitás
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php elseif($memberStatus != "temperror" && $memberStatus != "permerror"): ?>		

				<th class="">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/open/asc" title="Rendezés asc szerint">
						Megnyitás
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*MEGNYITÁS**********************************************************************/ ?>
			
			<?php /*ÁTKATTINTÁS**********************************************************************/ ?>
			<?php if($orderby=="clicked" && $order=="asc"  && $memberStatus != "temperror" && $memberStatus != "permerror"): ?>
		
				<th nowrap class="tabHeaderRight">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/clicked/desc" title="Rendezés desc szerint">
						Átkattintás
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="clicked" && $order=="desc"  && $memberStatus != "temperror" && $memberStatus != "permerror"): ?>		
			
				<th nowrap class="tabHeaderRight">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/clicked/asc" title="Rendezés asc szerint">
						Átkattintás
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php elseif($memberStatus != "temperror" && $memberStatus != "permerror"): ?>		

				<th nowrap class="tabHeaderRight">
					<a href="<?=url::base() ?>pages/statisticdetails/recipients/<?=$memberStatus ?>/clicked/asc" title="Rendezés asc szerint">
						Átkattintás
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*ÁTKATTINTÁS**********************************************************************/ ?>
        	
        	<?php if($memberStatus == "temperror" || $memberStatus == "permerror"): ?>
        	
        		<th nowrap class="tabHeaderRight" colspan="2">
					
						Hiba oka
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					
				</th>
				
        	<?php endif; ?>
		</tr>
		
		<?php foreach($members as $m): /****************************************TAGOK*/
			?>
				<tr id="<?=$m->id ?>" >
					<td class="tabRowLeft"><a href="<?=url::base() ?>pages/statisticdetails/selectmember/<?=$m->id ?>"><?=$m->email ?></a></td>
					
					<?php if($memberStatus == "temperror" || $memberStatus == "permerror"): ?>
					<td class="tabRowRight" colspan="2"><?=$bounceDict[$m->dict_id] ?></td>					
					<?php else: ?>
					<td><?=$m->open ?></td>
					<td class="tabRowRight"><?=$m->clicked ?></td>					
					<?php endif; ?>
					

					
					
				</tr>
		<?php endforeach;  /****************************************TAGOK*/?>


		<?php if(sizeof($deletedRecipients) > 0): /****************************************TAGOK*/
			?>
				<tr id="" >
					<td class="tabRowLeft" colspan="3">A címzettek közül <?=sizeof($deletedRecipients) ?> db feliratkozó törölve lett.</td>
				</tr>
		<?php endif;  /****************************************TAGOK*/?>


			        
		</table>
		
			<?php /*TÁBLÁZAT**********************************************************************/ ?>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableFooter">
		<tr>
			<td class="footerLeft">&nbsp;</td>
			<td class="footerRight" align="right" valign="top">&nbsp;</td>
		</tr>
		</table>
        <?=$pagination ?>
		</form>	
		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">
			<div class="bghighlight"><h3 class="sidebar">Levél statisztikája</h3></div>
		
			<dl class="icon-menu">		
				<dt><a href="<?=url::base() ?>pages/statisticdetails/letter" ><img src="<?=$base.$img?>icons/pie.png" width="16" height="16" alt="Snapshot" /></a></dt>
				<!-- /pages//statisticdetails/letter -->
				<dd><a href="<?=url::base() ?>pages//statisticdetails/letter" >Főnézet</a></dd>
				<dd class="last">Levél statisztikájának előnézete</dd>
				
                <dt><img src="<?=$base.$img?>icons/subscribers.png" width="18" height="18" alt="Recipient Breakdown" /></dt>
				<dd class="noLink">Címzettek tevékenységei</dd>
				<dd class="last">Ki nyitotta meg, ki kattintott rá a linkekre stb...</dd>
				
				<dt><a href="<?=url::base() ?>pages/statisticdetails/link" id="linkClickActivityIcon"><img src="<?=$base.$img?>icons/link-click.png" width="16" height="16" alt="Link Click Activity" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/statisticdetails/link" id="linkClickActivity">Linkek statisztikái</a></dd>
				<dd class="last">Melyik linkre hányszor kattintottak rá.</dd>
				

			</dl>
            
        </div> <!--options end-->
    </div> <!--rightcol end-->
	

	<div class="clear"></div>
    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->