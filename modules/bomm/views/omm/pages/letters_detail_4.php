<?php 
	   						$fieldValues = array(); 
		   					
	   						
		   					if($lastTestMember != null){
		   						$fieldValues = (array) $lastTestMember->getTestData();
		   						
		   						if($fieldValues == null){
		   							$fieldValues = array();
		   						}
		   						
		   						$fieldValues['email'] = $lastTestMember->email;
		   						$fieldValues['regdate'] = $lastTestMember->reg_date;		   						
		   						
		   						
		   						
		   					}

?>
<!-- CONTENT -->
    
	<div id="singleDoubleContent">

		<div id="listMake">
        <div id="Steps">
        	<div class="Container">
            	<div class="StepsCubes">1</div>
                Levél adatai
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">2</div>
                Levél tartalma
            </div>
            <div class="Separater">></div>
            <div class="Container Current">
            	<div class="StepsCubes Current">3</div>
                Tesztküldés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">4</div>
                Időzítés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">5</div>
                Címzési feltételek
            </div>
        </div>
        <h1 class="step1">3. lépés: Tesztküldés</h1>
        <p style="padding-left:20px">
        	<strong>Levél neve:</strong> <?=$letter->name ?><br/>
        	<strong>Levél tárgya:</strong> <?=$letter->subject ?><br/>
        </p>
        </div>
	
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		<td width="42%">
        <div class="mybutton">    
        <a href="<?=url::base() ?>pages/letterdetail/second" class="button toprevpage">
            <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
            Vissza a tartalom szerkesztéséhez
        </a>
        </div>
        </td>
		<td>
        <div class="mybutton">    
        <a href="<?=url::base() ?>pages/letterdetail/draft" class="button saveandexit">
            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
            Kilép
        </a>
        </div>
        </td>
		<td width="42%">
        <div class="mybutton" style="float:right">    
        <a href="<?=url::base() ?>pages/letterdetail/timing" class="button rightbutton tonextpage">
			Tovább az időzítéshez&nbsp;
            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
        </a>
        </div>        
        </td>
		</tr>
		</table>  
        
	</div> <!--singleDoubleContent end-->
    
    <div id="content">

		<?=$alert ?>           
		<?=$errors ?>
		
		
		<div class="formBG">
		<div class="formWrapper">

		<form id="testForm" name="step4" action="<?=url::base() ?>pages/letterdetail/test" method="post">
			
			<h3 class="topPad">Teszt levél elküldése az alábbi e-mail címre:</h3>
			<p>Amennyiben több címre szeretne egyidejűleg tesztet küldeni sorolja fel az e-mail címeket vesszővel elválasztva.</p>
			<div class="formContainer formContainerDark">
				<div class="clearfix">
				<label class="thin">Email</label>
					<input type="hidden" name="testdetails" id="testdetails" value="" />
					<input type="text" name="email" size="60" class="input_text" value="<?php echo (isset($fieldValues['email'])) ? $fieldValues['email'] : "";  ?>" id="email"/>
					
					<div class="mybutton" style="float:right">  	            	
	            	<a href="Javascript:;" class="button slim reszletesClick" onclick="" style="margin-left:10px">
	                	<img src="<?=$base.$img ?>icons/email.png" alt=""/> 
	                	Teszt e-mail küldése behelyettesítők kitöltésével
	            	</a>
	            	</div>						
					<!--<span class="mid light">Send to up to 5 addresses at once by separating them by a comma</span> <a href="javascript:;" class="reszletesClick" style="cursor:pointer;" title="Behelyettesítők kitöltése" rel="htmlSubjectField"><img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> </a>-->
				</div>
			</div>

			
			<!--
			<div class="radioContainer" style="">
				<div class="clearfix">
				<p class="topPad"><em>&nbsp;Some recent test addresses you've already used...</em></p>
				</div>
			
				<div class="radioContainerPad">
					<?php foreach($recents as $r): ?>
						<input type="checkbox" name="recentmails[]" value="<?=$r->email ?>" >
						<label for="<?=$r->email ?>" class="big" style="font-weight: normal"><?=$r->email ?></label>
					<?php endforeach; ?>
				</div>	
			</div>
            -->	

            <div class="mybutton">
                 <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/email.png" alt=""/>
                     Teszt e-mail elküldése
                 </button>
              <div class="clearButton"></div>
            </div>
		
		</form>
		
		</div>
        </div>
				
		<br>


	<div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
 
 	                <script>
	                $(function(){

	            		var reszletes_dialog = function(){ 
	            			var dialog = $("#reszletes_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".reszletesClick").click(function(){
	            			reszletes_dialog();
	            			$('#reszletes_dialog').dialog('open');
	            			$('#reszletes_dialog').show();
	            			
	            		});


	            		$("#reszletestesztkuldes").click(function(){

	            			var val = "{";

							//{"today_all":0,"today_created":0,"now_selected":0,"now_created":0}
							var sum = <?=sizeof($fields)?>;
							var i = 1;
								
							$('.test_field').each(function(){
								val = val + "\""+$(this).attr('rel')+ "\":" + "\"" + $(this).val() + "\"";

								if(i != sum) val = val + ",";

								i++;
							});

							val = val + "}";
							
							$('#email').val($('#test_email').val());

							$('#testdetails').val(val);

							$('#testForm').submit();	
							
	            			$('#reszletes_dialog').dialog('close');
	            			$('#reszletes_dialog').hide();
	            		});

	            		
	            		
	                });

					</script>
	            		
 
 <div id="reszletes_dialog" title="Mezők megadása" style="display:none">
        		<div class="formBG" style="padding:20px">
						<h3>Behelyettesítők kitöltése</h3>
		        		<table width="90%" align="center" style="border-collapse: collapse">
		   					<tr class="formContainer formContainerDark">
		   						<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail</td>
		   						<td align="right">
									<input type="text" id="test_email" value="<?php echo (isset($fieldValues['email'])) ? $fieldValues['email'] : "";  ?>" class="input_text" size="50"/>
								</td>
		   					</tr>
		   
		   					<?php
		   					foreach($fields as $f):?>

		   					<tr class="formContainer formContainerDark">
		   						<td align="right">{{<?=$f['reference']?>}}</td>
		   						<td align="right">
									<input class="test_field" type="text" rel="<?=$f['reference']?>" value="<?php echo (isset($fieldValues[$f['reference']])) ? $fieldValues[$f['reference']] : "";  ?>" class="input_text" size="50"/>
								</td>
		   					</tr>		   
		   
		   					<?php endforeach;?>
						<tr>
							
							<td align="center" colspan="2">
								<div class="mybutton" style="margin-left:50px">    
					            	<a href="Javascript:;" class="button"  id="reszletestesztkuldes">
					                	<img src="<?=$base.$img ?>icons/email.png" alt=""/> 
					                	Teszt e-mail elküldése a behelyettesítőkkel
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							
							
						</tr>		   
		        	
		        		</table>
				</div>
</div>
 
 
 
 
 
 
 
 
 