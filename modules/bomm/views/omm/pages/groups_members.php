		<style>
			.pagination strong{
				display:block;
				background:#FFFFFF none repeat scroll 0 0;
				display:block;
				float:left;
				padding:1px 4px;
				text-decoration:none;				
			}
		</style>
<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
        
        <p class="bread">
        	<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
        	<span class="breadArrow">&nbsp;</span>
        	<a href="<?=url::base() ?>pages/listdetail"><?=$list->name ?></a>
        	<span class="breadArrow">&nbsp;</span>
        	<a href="<?=url::base() ?>pages/groupoverview">Csoportok</a>
        	<span class="breadArrow">&nbsp;</span>
        	<a href="<?=url::base() ?>pages/groupmembers"><?=$group->name ?></a>
        	<span class="breadArrow">&nbsp;</span>Feliratkozottak</p>
		
		<h1 class="bottomPad">"<?=$group->name ?>" csoportba tartozó feliratkozók</h1>
		

<?php /*TÁBLÁZAT**********************************************************************/ ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabsHeader">
		<tr class="noHighlight">
			<?php /*EMAIL**********************************************************************/ ?>
			<?php if($orderby=="email" && $order=="asc"): ?>
		
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/groupmembers/index/email/desc" title="Rendezés desc szerint">
						e-mail cím
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="email" && $order=="desc"): ?>		
			
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/groupmembers/index/email/asc" title="Rendezés asc szerint">
						e-mail cím
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/groupmembers/index/email/asc" title="Rendezés asc szerint">
						e-mail cím
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*EMAIL**********************************************************************/ ?>
			
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			<?php foreach($gridFields as $gf): ?>		
			
					<?php if($orderby==$gf->reference && $order=="asc"): ?>
				
						<th>
							<a href="<?=url::base() ?>pages/groupmembers/index/<?=$gf->reference ?>/desc" title="Rendezés desc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>
					
					<?php elseif($orderby==$gf->reference && $order=="desc"): ?>		
					
						<th>
							<a href="<?=url::base() ?>pages/groupmembers/index/<?=$gf->reference ?>/asc" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>					
							
					<?php else: ?>		
		
						<th>
							<a href="<?=url::base() ?>pages/groupmembers/index/<?=$gf->reference ?>/asc" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
							</a>
						</th>								
					
					<?php endif; ?>
			
			<?php endforeach; ?>
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			
			<?php /*REG DÁTUMA**********************************************************************/ ?>
			<?php if($orderby=="reg_date" && $order=="asc"): ?>
		
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/groupmembers/index/reg_date/desc" title="Rendezés desc szerint">
						Fel. Dátuma
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="reg_date" && $order=="desc"): ?>		
			
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/groupmembers/index/reg_date/asc" title="Rendezés asc szerint">
						Fel. Dátuma
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/groupmembers/index/reg_date/asc" title="Rendezés asc szerint">
						Fel. Dátuma
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
        	<?php /*REG DÁTUMA**********************************************************************/ ?>
        	
        	
		</tr>
		
		<?php foreach($members as $m): /****************************************TAGOK*/?>
				<tr id="<?=$m->id ?>" >
					<td class="tabRowLeft"><a href="<?=url::base() ?>pages/listdetail/selectmember/<?=$m->id ?>"><?=$m->email ?></a></td>
					<?php 
						foreach ($gridFields as $gf){
							$ref = $gf->reference;
							echo '<td>'.$m->$ref.'</td>';
						}
					?>
					<td class="tabRowRight cellRight"><span><?=$m->reg_date ?></span></td>
				</tr>
		<?php endforeach;  /****************************************TAGOK*/?>
			        
		</table>
		
			<?php /*TÁBLÁZAT**********************************************************************/ ?>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableFooter">
		<tr>
			<td class="footerLeft">&nbsp;</td>
			<td class="footerRight" align="right" valign="top">&nbsp;</td>
		</tr>
		</table>
		<?=$pagination ?>

        
        
        
        
        
	</div>

	<div id="rightcol">

	
		<div id="options">
			
			<div class="fade-fff664" id="fade">
				<div class="segmentCount" id="fade">				
					<?=$group->name ?>
					<span><?=$membersSum ?></span>
					atkív feliratkozó				
				</div>
			</div>
			
			
			
			<div class="bghighlight"><h3 class="sidebar">Csoport eszközök</h3></div>

				
			<dl class="icon-menu">		
				<dt><a href="#" id="viewIcon"><img src="<?=$base.$img?>icons/create-segment.png" width="16" height="16" alt="View subscribers" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/groupadd/second/<?=$group->id ?>" id="viewLink">Csoport feltételei</a></dd>
				<dd class="last">Nézze meg és szerkeszze a csoportba kerülés feltételeit.</dd>


			</dl>
	
		</div>
	
	</div>
	
	<div class="clear"></div>
	
	</div>

	

    
    
    

<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
