
<div class="twocol">
<!-- CONTENT -->
	<div id="content">

		<div id="leftcol">
		
        <p class="bread"><a href="<?=url::base() ?>pages/statisticoverview">Statisztika</a><span class="breadArrow">&nbsp;</span><?=$letter->name ?> - Linkek</p>
		
        <h1 id="campaignTitle">
        <div class="campaignActions">
            <div class="mybutton">    
                <a href="<?=url::base()."api/common/showLetter/".$letter->id ?>" target="_blank" class="button">
                    <img src="<?=$base.$img?>icons/search.png" alt=""/> 
                    Levél megtekintése
                </a>
            </div> 
        </div>
        
        	<?=$letter->name ?>
        	<?php echo (empty($letter->note)) ? "" : '<a href="Javascript:;" title="'.nl2br($letter->note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?>
        </h1>
		
		
		<p class="titleSummary">
        <span>típus:</span> <strong><?=$letter->type ?></strong>
		</p>
		
			<?php
				if($letter->timing_type == "relative"){
					$tim = "Relatív";
					$sent_label = "<span>Levél küldése feliratkozás után</span> <strong>".$letter->timing_event_value." nappal</strong>";
				}else if($letter->timing_type == "absolute"){
					$tim = "Abszolút";
					$sent_label = "<span>elküldve:</span> <strong>".dateutils::formatDate($letter->isSent(TRUE))."</strong>";
				}else if($letter->timing_type == "sendnow"){
					$tim = "Azonnali";
					$sent_label = "<span>elküldve:</span> <strong>".dateutils::formatDate($letter->isSent(TRUE))."</strong>";
				}
			?>		
		
		<p class="titleSummary">
		<span>tárgy:</span> <strong><?=$letter->subject ?></strong><br/>
        <span>időzítés:</span> <strong><?=$tim ?></strong>
		</p>
		
		
		<p class="titleSummary">
        	<?=$sent_label ?>
		</p>		
        
        <?php
			$stat = $letter->getLetterLinkStat();
        ?>



		<div class="dataHighlight">
			
			<table cellpadding="0" cellspacing="0" width="100%" class="goalSummary">
			<tr>
				<td nowrap><div id="peopleClicked"><?=$stat['sentNumber'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Címzettnek elküldve</h2></td>

				<td nowrap><div><?=$stat['uniqueClicks'] ?></div></td>
				<td width="50%" class="goalDescription"><h2>Egyedi átkattintás</h2><strong><?=$stat['clickratio'] ?></strong> átkattinási arány</td>
			</tr>
			<tr class="lastGoal">
				<td nowrap><div class="secondary" id="clicksPerPerson"><?=$stat['allClicks']?></div></td>
				<td width="50%" class="goalDescription"><h2>Összes átkattintás</h2></td>

				<td nowrap></td>
				<td width="50%" class="goalDescription"></td>
			</tr>
			</table>
			
		</div>
		
		
		
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
		<tr class="noHighlight">

			<th class="headerLeft">Link (URL)</th>
			<th class="cellCenter">Egyedi</th>
			<th class="headerRight cellCenter">Összes</th>
		</tr>
		
		<?php 
			foreach($letter->getLinks() as $link): 
			
				$all = $stat['allClicks'];
				
				if($all != 0){
					$linkratio = (($link->allclicks / $all)*100);
				}else{
					$linkratio = 0;
				}
				
				$div = round($linkratio, 0)."%";
				$number = round($linkratio, 2)."%";  // 1.96
				
		?>
		
		<tr class="big">
			<td class="rowLeft clickCountCell"><?=$link->link_number ?>. <?=$link->url_name ?>&nbsp;(<?=$number ?>) <br/> <span><?=$link->url ?></span>
            	<div class="clickCountBG"><div class="clickCountFull" style="width:<?=$div ?>"></div></div>
            </td>
            
			<td class="cellCenter" id="uniqueClicks"><strong><?=$link->uniqueclicks?></strong></td>

			<td class="rowRight cellCenter" id="totalLinks"><span><?=$link->allclicks?></span></td>
		</tr>

		<?php endforeach; ?>

		
		</table>




		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">
		
			<div class="bghighlight"><h3 class="sidebar">Levél statisztikája</h3></div>
		
			<dl class="icon-menu">		
				<dt><a href="<?=url::base() ?>pages/statisticdetails/letter"><img src="<?=$base.$img?>icons/pie.png" width="16" height="16" alt="Snapshot" /></a></dt>
				<!-- /pages/statisticoverview -->
				<dd><a href="<?=url::base() ?>pages/statisticdetails/letter">Főnézet</a></dd>
				<dd class="last">Levél statisztikájának előnézete</dd>
				
                <dt><a href="<?=url::base() ?>pages/statisticdetails/recipients" id="recipientActivityIcon"><img src="<?=$base.$img?>icons/subscribers.png" width="18" height="18" alt="Recipient Breakdown" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/statisticdetails/recipients" id="recipientActivity">Címzettek tevékenységei</a></dd>
				<dd class="last">Ki nyitotta meg, ki kattintott rá a linkekre stb...</dd>
				
				<dt><img src="<?=$base.$img?>icons/link-click.png" width="16" height="16" alt="Link Click Activity" /></dt>
				<dd class="noLink">Linkek statisztikái</dd>
				<dd class="last">Melyik linkre hányszor kattintottak rá.</dd>
				
						</dl>
	
		</div>
 <!--options end-->
    </div> <!--rightcol end-->
	

	<div class="clear"></div>
    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
