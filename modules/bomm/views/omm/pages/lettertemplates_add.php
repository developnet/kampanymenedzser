<?php 
if($mod == "mod"){
	$gomb = "Sablon mentése";
	$title = '"'.$form['name'].'" sablon adatainak módosítása';
	
}else{
	$gomb = "Sablon hozzáadása";
	$title = 'Új sablon hozzáadása';
}
?>


	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">

	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>

			<?=$alert ?>			
			<?=$errors ?>
			<?=$htmlerror ?>
			
	    
	    <div class="formBG">
		<div class="formWrapper">
			<form action="<?=url::base() ?>pages/lettertemplates/add" name="editAccount" method="post"  enctype="multipart/form-data">
			<input type="hidden" name="mod" value="<?=$mod ?>"/>
			<h3>Sablon adatok</h3>
			<div class="formContainer">
				<div class="clearfix">
				<label>Megnevezés</label><input class="<?=$classes['name'] ?> input_text" name="name" id="name" type="text" size="45" value="<?=$form['name'] ?>" />
				</div>
				
				<?php if(isset($_SESSION['fromletter']) && $_SESSION['fromletter']):?>
					<div class="clearfix" style="display:none">
					<label>HTML fájl</label><input class="<?=$classes['html'] ?> input_text" name="html" type="file" size="30" value="" />
					</div>					
				<?php else: ?>
					<div class="clearfix">
					<label>HTML fájl</label><input class="<?=$classes['html'] ?> input_text" name="html" type="file" size="30" value="" />
					</div>		
				<?php endif; ?>

			</div>
			<div class="clear"></div>
			
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/accept.png" alt=""/> 
                    <?=$gomb ?>
                </button>
                
				<?php if(!isset($_SESSION['fromletter'])):?>
					<span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/lettertemplates/overview" id="cancelSaveChanges">mégsem</a></span>					
				<?php endif; ?>
				                
                
                
                <div style="clear:both"></div>
            </div> 
            
		<div class="clearButton"></div>
			
		</form>
		
		</div>
		</div>
	
	</div>	

	
	<div id="rightcol">


				<div id="options">


					<!--<div class="bghighlight"><h3 class="sidebar"> Hasznos tanácsok </h3></div>-->

					<?php if($mod == "mod"):?>
						
	
						<div class="bghighlight"><h3 class="sidebar">Honlap beállítások</h3></div>
						<dl class="icon-menu">		

			                <dt><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldLink">Honlap adatok</a></dd>			                
							<div class="clear"></div>
										                
			                <dt><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldLink">Levél sablonok</a></dd>
							<div class="clear"></div>
							
							<?php /*>
			                <dt><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Űrlap sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldLink">Űrlap sablonok</a></dd>
							<div class="clear"></div>
			                */?>

			            </dl>
						</div>
	

					<?php endif;?>

				</div>

	</div>


			<div class="clear"></div>

			</div>
	</div>
</div>
