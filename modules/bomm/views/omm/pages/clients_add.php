<?php 
if($mod == "mod"){
	$gomb = "Honlap mentése";
	$title = '"'.$form['name'].'" honlap adatainak módosítása';
	
}else{
	$gomb = "Honlap hozzáadása";
	$title = 'Új honlap hozzáadása';
}
?>


	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">

	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>

			<?=$errors ?>
	    
	    <div class="formBG">
		<div class="formWrapper">
			<form action="<?=url::base() ?>pages/clientadd" name="editAccount" method="post" id="client-form">
			<input type="hidden" name="referer" value="<?=$cancelLink ?>"/>
			<input type="hidden" name="mod" value="<?=$mod ?>"/>
			<h3>Honlap adatok</h3>
			<div class="formContainer">
				<div class="clearfix">
				<label>Megnevezés</label><input class="<?=$classes['name'] ?> input_text" name="name" id="name" type="text" size="45" value="<?=$form['name'] ?>" />
				</div>
				<div class="clearfix">
				<label>Domain név</label><input class="<?=$classes['site_domain'] ?> input_text" name="site_domain" type="text" size="45" value="<?=$form['site_domain'] ?>" /> <span class="left">(Példa: sajatcimem.hu)</span>
				</div>
				<div class="clearfix">
				<label>Küldő név</label><input class="<?=$classes['sender_name'] ?> input_text" name="sender_name" type="text" size="45" value="<?=$form['sender_name'] ?>" />
				</div>
                <div class="clearfix">
				<label>Küldő e-mail cím</label><input class="<?=$classes['sender_email'] ?> input_text" name="sender_email" type="text" size="45" value="<?=$form['sender_email'] ?>" />
				</div>
                <div class="clearfix">
				<label>Válasz e-mail cím</label><input class="<?=$classes['sender_replyto'] ?> input_text" name="sender_replyto" type="text" size="45" value="<?=$form['sender_replyto'] ?>" />
				</div>
                <div class="clearfix">
				<label>Megjegyzés</label><textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>
			</div>
			<div class="clear"></div>
			
            <div class="mybutton">
                <a href="Javascript:;" class="guibutton save guisubmit" rel="client-form" id=""><?=$gomb ?></a>
                <a href="<?=$cancelLink ?>" class="guibutton cancel">Mégsem</a>
                
              
                <div style="clear:both"></div>
            </div> 
            
		<div class="clearButton"></div>
			
		</form>
		
		</div>
		</div>
	
	</div>	

	
	<div id="rightcol">


				<div id="options">


					<!--<div class="bghighlight"><h3 class="sidebar"> Hasznos tanácsok </h3></div>-->

					<?php if($mod == "mod"):?>
						
	
						<div class="bghighlight"><h3 class="sidebar">Honlapbeállítások</h3></div>
						<dl class="icon-menu">		

			                <dt><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/clientadd/index/mod" id="addCustomFieldLink">Honlap adatai</a></dd>			                
							<div class="clear"></div>
							
			                <dt><a href="<?=url::base() ?>pages/fields/index" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/fields/index" id="addCustomFieldLink">Globális mezők</a></dd>			                
							<div class="clear"></div>							

			                <dt><a href="<?=url::base() ?>pages/productoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/productoverview" id="addCustomFieldLink">Termékek</a></dd>			                
							<div class="clear"></div>
							
							<dt><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/lettertemplates/overview" id="addCustomFieldLink">Levélsablonok</a></dd>
							<div class="clear"></div>							
										                
			                <dt><a href="<?=url::base() ?>pages/commonletters" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Levél sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/commonletters" id="addCustomFieldLink">Általános levelek</a></dd>
							<div class="clear"></div>
							

							
							<?php /*>
							       * 
							<dt><a href="<?=url::base() ?>pages/listunsubscribeprocess" id="unsubscribeFormIcon"><img src="<?=$base.$img?>icons/unsubscribe.png" width="16" height="16" alt=""></a></dt>
							<dd><a href="<?=url::base() ?>pages/listunsubscribeprocess" id="unsubscribeFormLink">Leiratkozási folyamat beállítása</a></dd>							       * 
			                <dt><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Űrlap sablonok" /></a></dt>
							<dd><a href="<?=url::base() ?>pages/formtemplates/overview" id="addCustomFieldLink">Űrlap sablonok</a></dd>
							<div class="clear"></div>
			                */?>

			            </dl>
						</div>
	

					<?php endif;?>

				</div>

	</div>


			<div class="clear"></div>

			</div>
	</div>
</div>
