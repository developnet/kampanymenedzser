<h1>Aktív levél</h1>

<a href="<?=url::base() ?>pages/letterdetail/duplicateletter">Levél duplikálása</a>

<?php if(
		($letter->timing_type == 'sendnow' || $letter->timing_type == 'absolute') &&
		(!$letter->isSent()) || $letter->timing_type == 'relative'
		): ///////csak akkor lehet szerekeszteni ha relative, vagy ha abs v azonnai és nem lett elküldve?>
	
	<a href="<?=url::base() ?>pages/letterdetail/settodraft">Levél szerkesztetté állítása</a>	
	
<?php endif; ?>	





<?php if($letter->timing_type == 'sendnow'): ///azonnali ?>
<?php 
	
	$recip_number = $letter->getRecipientsNumber();
	$sendjob = $letter->sendjob();
	
	
?>
<script>
$(function(){ 
	
	var RECIP_NUMBER = <?=$recip_number ?>;	
	var PROGRESS_CREATE = 0;
	var PROGRESS_PREPARE = 0;
	var PROGRESS_SEND = 0;
	var CREATE_JOB_ID = <?=$sendjob->id ?>;
	var JOB_STATUS = '<?=$sendjob->status ?>';
	
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: true,
			width:640,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		});
	}
	
	var createAjax = function(){
	
		$('#createJobWindowbutton').attr('disabled','true');
		$('#createJobWindowbutton').addClass('buttondisable');
		$('#sendingMessage').html('<strong style="color:red;font-weight:bold">A küldés folyamatban, kérem várjon...</strong>')
		
		$.getJSON("<?=url::base(TRUE) ?>api/sendnow/create/<?=$letter->id ?>/"+CREATE_JOB_ID, function(json){ 
  			
  			$('.allJobs').html(json.create.today_all);
  			
  			
			//CREATE_JOB_ID = json.job_id;
			all = json.create.today_all;				
			if(json.create.status == "WORKING"){
				created = json.create.today_created + json.create.now_created;
				//console.log(all + ","+ created);
				PROGRESS_CREATE = (created/all)*100;
				//console.log(PROGRESS); 
				$('#progressbarCreate').progressbar('option', 'value', PROGRESS_CREATE);
				$('#createdJobs').html(created);
				createAjax();
			}else if(json.create.status == "READY"){
				PROGRESS_CREATE = 100;
				$('#progressbarCreate').progressbar('option', 'value', PROGRESS_CREATE);
				$('#createdJobs').html(all);
				prepareAjax();
			}else{
				location.reload(); 
			}
	
		});
	}
	
	var prepareAjax = function(){
	
		//$('#createJobWindowbutton').attr('disabled','true');
		//$('#createJobWindowbutton').addClass('buttondisable');
		
		
		$.getJSON("<?=url::base(TRUE) ?>api/sendnow/prepare/<?=$letter->id ?>/"+CREATE_JOB_ID, function(json){ 
  			
  			
			//CREATE_JOB_ID = json.job_id;
			all = json.prepare.today_all;		
			if(json.prepare.status == "WORKING"){
							
						
				created = json.prepare.today_prepared + json.prepare.now_prepared;
				//console.log(all + ","+ created);
				PROGRESS_PREPARE = (created/all)*100;
				//console.log(PROGRESS); 
				$('#progressbarPrepare').progressbar('option', 'value', PROGRESS_PREPARE);
				$('#preparedJobs').html(created);
				prepareAjax();
								
			}else if(json.prepare.status == "READY"){
				PROGRESS_PREPARE = 100;
				$('#progressbarPrepare').progressbar('option', 'value', PROGRESS_PREPARE);
				$('#preparedJobs').html(all);
				sendAjax();
			}else{
				location.reload(); 
			}
	
		});
	}	
	
	var sendAjax = function(){
	
		//$('#createJobWindowbutton').attr('disabled','true');
		//$('#createJobWindowbutton').addClass('buttondisable');
		
		
		$.getJSON("<?=url::base(TRUE) ?>api/sendnow/send/<?=$letter->id ?>/"+CREATE_JOB_ID, function(json){ 
			//CREATE_JOB_ID = json.job_id;
			all = json.send.today_jobs_all;				
			if(json.send.status == "WORKING"){
				
				created = json.send.today_sent + json.send.now_sent;
				//console.log(all + ","+ created);
				PROGRESS_SEND = (created/all)*100;
				//console.log(PROGRESS_SEND); 
				$('#progressbarSend').progressbar('option', 'value', PROGRESS_SEND);
				$('#sentJobs').html(created);
				sendAjax();
								
			}else if(json.send.status == "READY"){
				PROGRESS_SEND = 100;
				$('#progressbarSend').progressbar('option', 'value', PROGRESS_SEND);
				$('#sentJobs').html(all);
				location.reload(); 
			}else{
				location.reload(); 
			}
	
		});
	}
	
	
	$('#createJobWindowbutton').bind('click', function(){	createAjax();	});

	$('#createJobbutton').bind('click', function(){
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();
				
		});
			
	$('#progressbarCreate').progressbar({ value: PROGRESS_CREATE });
	$('#progressbarPrepare').progressbar({ value: PROGRESS_PREPARE });
	$('#progressbarSend').progressbar({ value: PROGRESS_SEND });

});
</script>
	
	
	<?php if($sendjob->status =='_created'):	?>
	   <div class="mybutton">
			<button id="createJobbutton" type="submit" class="button">
		    	<img src="<?=$base.$img ?>icons/accept.png" alt=""/>Küldés
		    </button>
	   </div>
	<?php elseif($sendjob->status != 'sent'): ?>
		<h1>Már el lett kezdve a küldés, de nincs befejezve.</h1>
	   <div class="mybutton">
			<button id="createJobbutton" type="submit" class="button">
		    	<img src="<?=$base.$img ?>icons/accept.png" alt=""/>Folytatás
		    </button>
	   </div>
	<?php else: ?>
		<h1>Elküldve: <?=$sendjob->sent_time ?></h1>
	<?php endif; ?>
	
		
<div style="clear:both"></div>





<div id="dialog" title="Küldés" style="display:none">

	<table width="90%" align="center">
		<caption>Küldés állapota</caption>
		<tr>
			<th></th>
			<th>Kész</th>
			<th></th>
			<th>Összesen</th>
		</tr>
		<tr>
			<td><span id="createLabel">Címzettek leválogatása</span></td>
			<td><span id="createdJobs">0</span></td>
			<td>/</td>
			<td><span class="allJobs">?</span></td>
		</tr>		
		<tr>
			<td colspan="4"><div id="progressbarCreate"></div></td>
		</tr>
		<tr>
			<td><span id="prepareLabel">Levelek előkészítése</span></td>
			<td><span id="preparedJobs">0</span></td>
			<td>/</td>
			<td><span class="allJobs">?</span></td>
		</tr>	
		<tr>
			<td colspan="4"><div id="progressbarPrepare"></div></td>
		</tr>
		<tr>
			<td><span id="sendLabel">Levelek kiküldése</span></td>
			<td><span id="sentJobs">0</span></td>
			<td>/</td>
			<td><span class="allJobs">?</span></td>
		</tr>	
		<tr>
			<td colspan="4"><div id="progressbarSend"></div></td>
		</tr>
	</table>
	
	


	<div class="mybutton" id="sendingMessage">
		<button id="createJobWindowbutton" type="submit" class="button">
	    	<img src="<?=$base.$img ?>icons/accept.png" alt=""/>Indítás
	    </button>
   </div>

</div>


<?php endif; ?>

<div style="clear:both"></div>
