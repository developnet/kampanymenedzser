<script>
	$(function(){ 
		var SELECTED = 0;
		
  		$(".groupRow").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				//alert("#client_"+id+"_delete");
				
				$("#group_"+id+"_delete").show();
				
			} );

			$(this).mouseout( function() {
				
				$("#group_"+id+"_delete").hide();
				
			} );
 
 
 		$(".deleteGroup").each(function(){
				$(this).bind('click', function(){
					SELECTED = $(this).attr('rel');
					$('.selectionName').html($(this).attr('relName'));
					deleteList();
				});
			});
		
		});

		
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteList = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}			
		 
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/groupoverview/deleteGroup/'+SELECTED;
					
	});		
				 
		 
		 
	});

	
</script>
<div id="dialog" title="Csoport törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Valóban végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" csoportot?</strong><br />
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Csoport törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" csoportot?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	

<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
    
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>Csoportok
		</p>
		
		<h1>Csoportok</h1>

		<?php
			if(isset($_SESSION['alert'])) {
				echo $_SESSION['alert'];
				unset($_SESSION['alert']);
			}
		?>
		
		
		<p class="bottomPad">Egy lista csoportjai nem egzakt halmazok, amelyekbe manuálisan lehet a feliratkozókt csoportosítani, hanem feltételek összessége, amelyek a feliratkozókat az adataik alapján dinamikusan szűrik. </p>
		

		<?php if(sizeof($groups) == 0): //////////HA NINCSEN MÉG CSOPORT!!!!!!!!!!!!?>
			<p>
				Még nincs csoport létrehozva a "<?=$_SESSION['selected_list']->name ?>" listához.         <div class="mybutton" style="float:left;padding-bottom:20px;">    
            <a href="<?=url::base() ?>pages/groupadd/first" class="button">
                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                Új csoport létrehozása
            </a>
        <div style="clear:both"></div>
        </div>	
			</p>
			
		<?php else: ////////////egyéként?>
		
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
			<tr class="noHighlight">
				<th width="100%" class="headerLeft"><strong>Csoport név</strong></th>
				<th width="100" nowrap class="cellCenter"><strong>Feliratkozottak</strong></th>
				<th width="40" nowrap class="headerRight" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</tr>
	
			<tr class="skinAltRow">
				<td class="rowLeft">Összes feliratkozó a "<?=$_SESSION['selected_list']->name ?>" listán</td>
			    <td nowrap class="cellCenter"><?=$membersSum ?></td>
				<td nowrap class="rowRight" align="right">&nbsp;</td>
			</tr>
			
			<?php foreach($groups as $group): ?>
			
				<tr rel="<?=$group->id ?>" class="groupRow">
				   	<td class="rowLeft"><a href="<?=url::base() ?>/pages/groupmembers/index/<?=$group->id ?>" <?php echo (empty($group->note)) ? "" : 'title="'.nl2br($group->note).'"'; ?>><?=$group->name ?></a></td>
				    <td nowrap class="cellCenter"><?=$group->getMemberCount($fields) ?></td>
					
					<td nowrap class="rowRight" align="right">
						<span class="deleteGroup" rel="<?=$group->id ?>" relName="<?=$group->name ?>" id="group_<?=$group->id ?>_delete" style="display:none">
							<a href="javascript:;" title="Csoport törlése">
								<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Törlés">
							</a>
						</span>
					</td>
				</tr>		
			
			<?php endforeach; ?>
			</table>

		<?php endif; ?>








	
	</div> <!--leftcol end-->

	<div id="rightcol">
	
		<div id="options">
			
        <div class="mybutton" style="float:left;padding-bottom:20px;">    
            <a href="<?=url::base() ?>pages/groupadd/first" class="button">
                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                Új csoport létrehozása
            </a>
        <div style="clear:both"></div>
        </div>
        <div style="clear:both"></div>
            
			<div class="bghighlight"><h3 class="sidebar">Ismerje meg a feliratkozóit</h3></div>

			<p><a href="#"><img src="<?=$base.$img?>icons/zoom.png" width="30" height="31" alt="Ismerje meg a feliratkozóit" class="promoIcon" style="padding-top:5px;padding-left:3px"></a>Adjon hozzá egyedi mezőket a listájához, hogy minél részletesebben megismerje és követni tudja a feliratkozóit.</p>
			<p>Ezen egyedi mezők alapján késöbb könnyen létrehozhat olyan különböző csoportokat, amelyekhez egyedileg, az igényük szerint kommunikálhat.</p>

			<div class="sidebarCTA"><a href="<?=url::base() ?>pages/listadd/second">Lista mezőinek szerkesztése</a> &raquo;</div>
	
		</div>
	
	</div> <!--rightcol end-->
	
	
    
    
    <div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->