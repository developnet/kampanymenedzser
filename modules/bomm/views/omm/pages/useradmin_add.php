<?php 
if($mod == "mod"){
	$gomb = "Felhasználó mentése";
	$title = '"'.$form['lastname'].' '.$form['firstname'].'" felhasználó adatainak módosítása';
	
}else{
	$gomb = "Felhasználó hozzáadása";
	$title = 'Új Felhasználó hozzáadása';
}
?>


	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">

	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>

			<?=$errors ?>
			
	    
	    <div class="formBG">
		<div class="formWrapper">
			<form action="<?=url::base() ?>pages/useradmin/add" name="editAccount" method="post"  enctype="multipart/form-data">
			<input type="hidden" name="mod" value="<?=$mod ?>"/>
			<h3>Felhasználó adatok</h3>
			<div class="formContainer">

				<div class="clearfix">
				<label>Státusz</label>
				
					<select name="status">
						<option value="1" <?php echo ($form['status'] == 1) ? 'selected="selected"' : "";?>>Aktív</option>
						<option value="0" <?php echo ($form['status'] == 0) ? 'selected="selected"' : "";?>>Inaktív</option>
					</select>
				
				</div>

				<div class="clearfix">
				<label>Felhasználónév</label><input class="<?=$classes['username'] ?> input_text" name="username" type="text" size="45" value="<?=$form['username'] ?>" />
				</div>

				<div class="clearfix">
				<label>Vezetéknév</label><input class="<?=$classes['lastname'] ?> input_text" name="lastname" type="text" size="45" value="<?=$form['lastname'] ?>" />
				</div>
			
				<div class="clearfix">
				<label>Keresztnév</label><input class="<?=$classes['firstname'] ?> input_text" name="firstname"  type="text" size="45" value="<?=$form['firstname'] ?>" />
				</div>

				<div class="clearfix">
				<label>E-mail</label><input class="<?=$classes['email'] ?> input_text" name="email" type="text" size="45" value="<?=$form['email'] ?>" />
				</div>

				<div class="clearfix">
				<label>Megjegyzés</label><input class="<?=$classes['note'] ?> input_text" name="note" type="text" size="45" value="<?=$form['note'] ?>" />
				</div>

 				<h3>Jelszóváltoztatáshoz töltse ki az alábbi mezőket is:</h3>

				<div class="clearfix">
				<label>Jelszó</label><input class="<?=$classes['jelszo'] ?> input_text" name="jelszo" type="password" size="45" value="<?=$form['jelszo'] ?>" autocomplete="off"/>
				</div>

				<div class="clearfix">
				<label>Jelszó mégegyszer</label><input class="<?=$classes['password2'] ?> input_text" name="password2" type="password" size="45" value="<?=$form['password2'] ?>"  autocomplete="off"/>
				</div>

			</div>
			<div class="clear"></div>
			
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/accept.png" alt=""/> 
                    <?=$gomb ?>
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/useradmin/overview" id="cancelSaveChanges">mégsem</a></span>
                <div style="clear:both"></div>
            </div> 
            
		<div class="clearButton"></div>
			
		</form>
		
		</div>
		</div>
	
	</div>	

	
	<div id="rightcol">


				<div id="options">




				</div>

	</div>


			<div class="clear"></div>

			</div>
	</div>
</div>
