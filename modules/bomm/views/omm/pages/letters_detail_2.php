<script type="text/javascript" src="<?=$assets?>tinymce/tiny_mce.js"></script>
 <script type="text/javascript" src="<?=$assets?>tinymce/plugins/tinybrowser/tb_tinymce.js.php?PHPSESSID=<?=session_id()?>"></script>

<?php
$docbase = str_replace("https:","http:",substr(url::base(),0,sizeof(url::base())-2) );

cookie::set("cid", meta::getAccId()."/".$cid);
cookie::set("docroot", DOCROOT);
cookie::set("linkroot", $docbase);



//
?>

<script>
	var AUTOSAVE = false;

	
	 function autosave(){

		if(AUTOSAVE){

			var time = $("#savetimer").html()-1;

			if(time == 0){
			
				$("#saveing").show();
				$("#letter_saved").html("-");
				var html;
					
				try{
					html = tinyMCE.get("html_content_editor").getContent();
				}catch(e){
					html = $("#html_content_editor").val();
				}
	
				var text = $("#text_content").val();
				
				POSTARRAY =  {  "letter": "<?=$letter->id?>", 
								"html": html,
								"text": text
						};
	
	
				$.post("<?=url::base() ?>api/common/saveLetter", POSTARRAY, 
						  function(data){ 
						  
						  	json = eval( data );
	
						  	if(json.STATUS == "ERROR"){
								alert('Hiba a mentés közben!');
						  	}else if(json.STATUS == "SUCCES"){
								$("#letter_saved").html(json.MESSAGE);
								$("#saveing").hide();
	
								$("#savetimer").html("30");
	
								
								
						  	}else{
						  	
						  	}
						  	
				}); 

			}else{
				$("#savetimer").html(time);
			}	
			
		}else{
			//$("#savetimer").html("30");
		}	

		setTimeout('autosave()',1000);
	}
		
	setTimeout('autosave()',1000);
	
$(function(){

	$("#auto_save_letter").click(function(){

		if(AUTOSAVE){

			$("#autosave_check").attr('checked', false);
			AUTOSAVE = false;
			$("#savetimer").html("30");
			
		}else{

			$("#autosave_check").attr('checked', true);
			AUTOSAVE = true;
			$("#savetimer").html("30");
			
		}
		

	});
	
	$("#save_letter").click(function(){
		$("#saveing").show();
		$("#letter_saved").html("-");
		var html;
			
		try{
			html = tinyMCE.get("html_content_editor").getContent();
		}catch(e){
			html = $("#html_content_editor").val();
		}

		var text = $("#text_content").val();
		
		POSTARRAY =  {  "letter": "<?=$letter->id?>", 
						"html": html,
						"text": text
				};


		$.post("<?=url::base() ?>api/common/saveLetter", POSTARRAY, 
				  function(data){ 
				  
				  	json = eval( data );

				  	if(json.STATUS == "ERROR"){
						alert('Hiba a mentés közben!');
				  	}else if(json.STATUS == "SUCCES"){
						$("#letter_saved").html(json.MESSAGE);
						$("#saveing").hide();
				  	}else{
				  	
				  	}
				  	
		}); 		

	});
	
	$(".confirmation_html_toogle").click(function(){
		verifycation();
	});

	
	var type = function(val) {
		if(val == "html"){
			$("#htmlcontent").show();
			$(".typeRadioText").attr('checked','');
			$(".typeRadioHtml").attr('checked','checked');
			$("#lettertype").val('html');
			$("#options").show();
		}else if(val == "text"){
			$("#htmlcontent").hide();	
			$(".typeRadioText").attr('checked','checked');
			$(".typeRadioHtml").attr('checked','');
			$("#lettertype").val('text');
			$("#options").hide();			
				
		}	
	}


	$(".typeRadioText").click(function(){
		type('text');
	});

	$(".typeRadioHtml").click(function(){
		type('html');
	});
	
	type('<?=$letter->type ?>');


	//$("#htmliframe").resizable();
	
	var EDWIDTH = $('#editor_wrapper').width();
	

	$(window).bind('resize', function() {
		setEditorSize();
	});

	
	//tinyMCE.execCommand("mceAddControl", true, "html_content_editor");
	
	var insertHTML = function(editor,html) {
	    tinyMCE.execInstanceCommand(editor,"mceInsertContent",false,html);
	}	
	
	$('.saveandexit').click(function(){
		$('#exitinput').val('true');
		document.forms['tartalom'].submit();
		return false;
	});

	$('.tonextpage').click(function(){
		
		//var answer = confirm("Menti a tartalmat?")
		
		//if(answer){
			$('#exitinput').val('tonextpage');
			document.forms['tartalom'].submit();
			return false;
		//}else{
			//window.location = '<?=url::base() ?>pages/letterdetail/test';
			//return false;
		//}
	

	});	
	
	$('.toprevpage').click(function(){
		
		//var answer = confirm("Menti a tartalmat?")
		
		//if(answer){
			$('#exitinput').val('toprevpage');
			document.forms['tartalom'].submit();
			return false;
		//}else{
		//	window.location = '<?=url::base() ?>pages/letterdetail/first';
		//	return false;
		//}
	

	});	
	
	
	$('#html_source_upload').click(function(){
		$('#file_upload').toggle();
	});
 
	$('#html_source_template').click(function(){
		$('#template_choice').toggle();		
	});
		
	<?php if(isset($htmlerrors) && $htmlerrors != ""):	?>
	
		$('#file_upload').show();
		$('#html_source_upload').attr('checked','checked');
	<?php endif; ?>	
		
	
});
	var toggleEditor = function(id) {
		if (!tinyMCE.get(id))
			tinyMCE.execCommand('mceAddControl', false, id);
		else
			tinyMCE.execCommand('mceRemoveControl', false, id);
	}	

	function setEditorSize(){
		EDWIDTH = $('#editor_wrapper').width();
		$('#html_content_editor_parent').css('width', EDWIDTH);
		$('#html_content_editor_tbl').css('width', EDWIDTH);
	}

	var tinyMCETemplateList = [
		// Name, URL, Description
		//["Regatta sablon", "<?=url::base() ?>omm/views/omm/HTML_levelek/level_01.html", "Regatta sablon"]
	];


	// O2k7 skin (silver)
	tinyMCE.init({
		// General options
		init_instance_callback : "setEditorSize",
		file_browser_callback : "tinyBrowser",
		language: "hu",
		mode : "exact",
		auto_resize : false,
		theme_advanced_resize_horizontal : true,
		//width : EDWIDTH,
		height : "600",
		elements : "html_content_editor",
		theme : "advanced",
		skin : "o2k7",
		skin_variant : "silver",
		plugins : "safari,pagebreak,style,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullpage,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		fullpage_encodings : "UTF-8",
		fullpage_default_doctype : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">',
		fullpage_default_title : '',
		// Theme options
		theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor,styleprops",
		theme_advanced_buttons2 : "undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,image,cleanup,help,code,|,insertdate,inserttime,preview",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,advhr,|,print,|,fullscreen,visualchars,nonbreaking,template,blockquote",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "center",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		theme_advanced_font_sizes : "8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px,26px,27px,28px,29px,30px",
		font_size_style_values : "8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px,26px,27px,28px,29px,30px",
		relative_urls : false,
		remove_script_host : true,
		document_base_url : "<?=$docbase ?>",
		convert_urls : false,
		relative_urls : false,
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "js/template_list.js",
		external_link_list_url : "js/link_list.js",
		external_image_list_url : "js/image_list.js",
		media_external_list_url : "js/media_list.js",
		entity_encoding : "raw",
		valid_elements : "*[*]",
		accessibility_warnings : false 
	});
	
	<?php 
		// content_css : "<?=url::base()."api/common/letterContentCss/".$letter->id " 
	?>
	

	

</script>
<style>

</style>



<!-- CONTENT -->
    
	<div id="singleDoubleContent">

		<div id="listMake">
        <div id="Steps">
        	<div class="Container">
            	<div class="StepsCubes">1</div>
                Levél adatai
            </div>
            <div class="Separater">></div>
            <div class="Container Current">
            	<div class="StepsCubes Current">2</div>
                Levél tartalma
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">3</div>
                Tesztküldés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">4</div>
                Időzítés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">5</div>
                Címzési feltételek
            </div>
        </div>
        
        <h1 class="step1">2. lépés: A levél tartalma</h1>
        <p style="padding-left:20px">
        	<strong>Levél neve:</strong> <?=$letter->name ?><br/>
        	<strong>Levél tárgya:</strong> <?=$letter->subject ?><br/>
        </p>

        
        </div>
	
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		<td width="42%">
        <div class="mybutton">    
        <a href="javascript:;" class="button toprevpage">
            <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
            Vissza a levél adataihoz
        </a>
        </div>
        </td>
		<td>
        <div class="mybutton">    
        <a href="javascript:;" class="button saveandexit">
            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
            Ment és kilép
        </a>
        </div>
        </td>
		<td width="42%">
        <div class="mybutton" style="float:right">    
        <a href="javascript:;" class="button rightbutton tonextpage">
			Tovább a tesztküldéshez&nbsp;
            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
        </a>
        </div>        
        </td>
		</tr>
		</table>       
        
	</div> <!--singleDoubleContent end-->
    
    <div class="twocol">            
    <div id="content">
    
    <div id="leftcol">

        <?=$errors ?>
        <?=$alert ?>
        
        
    	
		<div class="formBG">
		<div class="formWrapperRadios">
		
			
			<div class="bigRadio">
				<input class="typeRadioHtml" type="radio" id="type" name="type" value="html" <?php echo ($letter->type == "html") ? 'checked="checked"' : ""; ?>>
				<label for="htmltext" class="h3checkbox">&nbsp;HTML és sima szöveg</label>

				<p></p>
			</div>
            
            
            <div style="padding-left:23px;" id="htmlcontent">
            
            <h3>1. HTML tartalom</h3>
            
            
            <table border="0" cellpadding="0" cellspacing="0" style="margin:10px 0">
            <tr><td style="color:#2B3941;font-size:14px;font-weight:bold;padding:3px 15px;">
            <input id="html_source_upload" type="checkbox" name="html_source" value="source" style="" /> Kész HTML forrás feltöltése
			</td></tr>
            </table>
            
           	<div id="file_upload" style="display:none;padding:0 15px;">
                <p class="medium">Töltse fel az előre megszerekesztett html fájlt, vagy készítsen újat a szerkesztő segítségével.</p>
                <div class="formContainer">
                <form id="myUploadForm" enctype="multipart/form-data" action="<?=url::base() ?>pages/letterdetail/uploadhtml" method="POST" class="a_form">
                    <div class="clearfix">
                        <label>HTML fájl:</label><input name="htmlFile" type="file" id="fuHTMLFile" tabindex="1" size="40" style="float:left;margin-right:10px;" />
    
                        <div class="mybutton" style="float:left">
                             <button type="submit" class="button slim">
                                <img src="<?=$base.$img ?>icons/arrow_up_green.png" alt=""/>
                                 HTML fájl feltöltése
                             </button>
                          <div style="clear:both"></div>
                        </div>
                        <div style="clear:both"></div>		
                    </div>
                    <div class="htmlError"><?=$htmlerrors ?></div>
                </form>
                </div>
            </div>

            <table border="0" cellpadding="0" cellspacing="0" style="margin:10px 0">
            <tr><td style="color:#2B3941;font-size:14px;font-weight:bold;padding:3px 15px;">
            <input id="html_source_template" type="checkbox" name="html_source" value="source" style="" /> Sablon használata
			</td></tr>
            </table>            
			<div id="template_choice" style="display:none;padding:0 15px;">
                <ul id="html_templates">
                    <?php foreach($templates as $t): ?>

                        <li class="template_list">
                        	 <?php if($t->thumbnail != "thumbnail.png"): ?>
                        	 <a href="<?=url::base() ?>pages/letterdetail/usetemplate/<?=$t->id ?>" onclick="return confirm('Felülírja a sablonnal a jelenlegi tartalmat?');" ><img src="<?=url::base() ?>files/<?=$t->thumbnail?>" alt="<?=$t->name ?>" border="0"/></a><br/>
                        	 <?php endif; ?>
                            <a href="<?=url::base() ?>pages/letterdetail/usetemplate/<?=$t->id ?>" onclick="return confirm('Felülírja a sablonnal a jelenlegi tartalmat?');" ><?=$t->name ?></a>
                        </li>



                    
<!--                        <li class="template_list">
                            <a href="<?=url::base() ?>pages/letterdetail/usetemplate/<?=$t->id ?>" onclick="return confirm('Felülírja a sablonnal a jelenlegi tartalmat?');" title="Sablon beillesztése"><img src="<?=url::base() ?>files/<?=$cid ?>/images/<?=$t->thumbnail ?>" width="125" height="125" border="0" /></a>
                            <a href="<?=url::base() ?>pages/letterdetail/usetemplate/<?=$t->id ?>" onclick="return confirm('Felülírja a sablonnal a jelenlegi tartalmat?');" title="Sablon beillesztése">
                            <?=$t->name ?>   
                            </a>
                        </li>-->
                        
                    <?php endforeach; ?>
                <div class="clear"></div>
                </ul>
			<div class="clear"></div>
            </div>
            <div>
                         <div class="mybutton" style="float:right;width:530px;height:30px">
			            	<a href="Javascript:;" class="button slim" id="auto_save_letter" style="margin-right:10px">
			                	<input type="checkbox" disabled="disabled" id="autosave_check"/>Automentés (<span id="savetimer">30</span>)
			            	</a>
			            	
			            	<a href="Javascript:;" class="button slim" id="save_letter">
			                	<img src="<?=$base.$img ?>icons/save.png" alt=""/> 
			                	Tartalom mentése
			            	</a>
                           
                           
                           
                           <span style="margin-right:10px;padding-top:5px;float:right;width:210px">Utoljára mentve: <span id="letter_saved" style="font-weight:bold"><?=$letter->mod_date?></span></span>
                           
                            <img src="<?=$base.$img ?>ajax-loader2.gif" alt="" style="float:right;margin-top:5px;margin-right:5px;display:none;" id="saveing"/>
                           
                           <div style="clear:both"></div>
                        </div>
            			
            			<h3 style="float:left;width:200px;">Jelenlegi html tartalom:</h3>
            			<div style="clear:both"></div>
            </div>			
			<form action="<?=url::base() ?>pages/letterdetail/second" method="post" name="tartalom">
            <input type="hidden" name="exit" value="false" id="exitinput"/>
            
            
            
            
            <div id="editor_wrapper" style="width:100%;">
				<textarea id="html_content_editor" name="html_content" wrap="off" style="width: 99%; height: 600px;"><?= $form['html_content'] ?></textarea>
			</div>
			<!-- 
			<div class="formContainer" >
				<iframe  style="width:100%;height:400px" src="<?=url::base()."api/common/showLetter/".$letter->id ?>"></iframe>
			</div>
			 -->

			
				<div class="mybutton" style="float:left;margin-top:8px;">    
	            	<a href="Javascript:;" class="button" onclick="javascript:toggleEditor('html_content_editor');">
	                	<img src="<?=$base.$img ?>icons/pencil.png" alt=""/> 
	                	Szerkesztő ki/be kapcsolása
	            	</a>
	            	

	               <div class="mybutton" style="float:left;margin-left:10px;">
	                    <button id="" type="submit" class="button" name="copyText" value="ok">
	                        <img src="<?=$base.$img ?>icons/pencil.png" alt=""/>Szöveg másolása textbe
	                    </button>
	               </div>   
	               
	               <div class="mybutton" style="float:right;margin-left:10px"> 
	                    <button id="" type="submit" class="button" name="copyTemplate" value="ok">
	                        <img src="<?=$base.$img ?>icons/save.png" alt=""/>Levél mentése sablonként
	                    </button>
	               </div>	               
	               
	            	<div style="clear:both"></div>
	        	</div>

	        <!-- 	
               <div class="mybutton" style="float:right;margin-top:8px;">
                    <button id="" type="submit" class="button">
                        <img src="<?=$base.$img ?>icons/save.png" alt=""/>Mentés
                    </button>
               </div>                
 			-->

			<div style="clear:both"></div>
        	
        	</div> <!--padding-left end-->

			<p></p>
			
			<input id="lettertype" type="hidden" name="lettertype" value="<?=$letter->type //?>" />
			<div class="bigRadio">
				<input class="typeRadioText" type="radio" name="type" value="text" id="text" <?php echo ($letter->type == "text") ? 'checked="checked"' : ""; ?>  >
				<label for="text" class="h3checkbox">&nbsp;Sima szöveges levél</label>
				<p>A sima szöveges levél lehetővé teszi, hogy minden kliensben és webes szolgáltatásban megfelelően megjelenik a levél, de a tartalom nem formázható illetve nem gyüjthetőek be a statisztikai adatok a levélről.</p>
			</div>
            
            <div class="formWrapperExtraPad"></div>
			
			<p></p>
			
			<textarea cols="70" rows="15" name="text_content" id="text_content" style="width:99%;height:500px;" class="textOnlyOutline"><?=$form['text_content'] ?></textarea>
			
			<!--<input type="submit" value="Mentés" />-->
			
						<div class="mybutton" style="float:left;margin-top:8px;">
				           	<a href="Javascript:;" class="button beszurasClick"  rel="text_content">
				               	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
				               	Behelyettesítő beszúrása
				           	</a>
				           	<div style="clear:both"></div>
			            </div> 			
			
           <div class="mybutton" style="float:right;margin-top:8px;">
                <button id="" type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/save.png" alt=""/>Mentés
                </button>
           </div>
           
           
           
           
           
           <div class="clear"></div> 
		   </form>




			
						
		</div>
		</div>
    
    

    
    
        
   </div> <!--leftcol end-->
        
        <div id="rightcol">

            <div id="options">
        		<p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></p>
        		<?php
					$sepStart = Kohana::config('core.fieldSeparatorStart');
					$sepEnd = Kohana::config('core.fieldSeparatorEnd');
					$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
					$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
					$email = $sepStart."email".$sepEnd;
					$regdatum = $sepStart."regdatum".$sepEnd;
        		?>
        		
                <div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>
                
                <script>
                $(function(){
					var SELECTED_LIST = "";
					$('#altalanosInsert').click(function(){
						var tag = $('#altalansTag').val();
						
						if(tag == '<?=$unsubLink ?>'){
							tag = '<a href="<?=$unsubLink ?>">Leiratkozás</a>';
						}else if(tag == '<?=$datamodLink ?>'){
							tag = '<a href="<?=$datamodLink ?>">Adatmódosítás</a>';
						}

						if(tag == "null" || tag == null){
							tag = "";
						}
						
						tinyMCE.execInstanceCommand('html_content_editor',"mceInsertContent",false,tag);
					});

                	$('#listaInsert').click(function(){
                		tinyMCE.execInstanceCommand('html_content_editor',"mceInsertContent",false,$('#'+$('#listak').val()).val())
                	});
                	
                	$('#listak').change(function (){
                		$('.fieldContainer').hide();
                		$('#'+$('#listak').val()).show();
                	});
                	
                });
                </script>
                
                <select size="3" style="width:210px" id="altalansTag">
                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
                </select>
                
                <p><br/></p>
                
				<div class="mybutton">    
	            	<a href="Javascript:;" class="button" id="altalanosInsert">
	                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
	                	Beszúr
	            	</a>
	            	<div style="clear:both"></div>
	        	</div>                

				<p><br/></p>
				<div class="bghighlight"><h3 class="sidebar">Globális mezők:</h3></div>
		
						<select class="fieldContainer" size="<?=sizeof($client->getFields())+1 ?>" style="width:210px;" id="globalfields">			
							<?php foreach($client->getFields() as $field): ?>
							<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
							<?php endforeach; ?>
						</select>
                	

                <p><br/></p>
                
				<div class="mybutton">    
	            	<a href="Javascript:;" class="button" id="globalInsert">
	                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
	                	Beszúr
	            	</a>
	            	<div style="clear:both"></div>
	        	</div>  

				<p><br/></p>
				<div class="bghighlight"><h3 class="sidebar">Listák mezői:</h3></div>

                <select size="1" style="width:210px" id="listak">
                	<?php foreach($lists as $list): ?>
                		<option value="list_<?=$list->id ?>"><?=$list->name ?></option>
                	<?php endforeach; ?>
                </select>
				<p><br/></p>

                	<?php 
                		$first = true;
                		foreach($lists as $list):  
                			$fields = $list->fields();
                	?>
						
						<select class="fieldContainer" size="<?=sizeof($fields)+1 ?>" style="width:210px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="list_<?=$list->id ?>">			
							<?php foreach($fields as $field): ?>
							<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
							<?php endforeach; ?>
						</select>
                	<?php endforeach; ?>

                <p><br/></p>
                
				<div class="mybutton">    
	            	<a href="Javascript:;" class="button" id="listaInsert">
	                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
	                	Beszúr
	            	</a>
	            	<div style="clear:both"></div>
	        	</div>                   
                
            </div>

		</div> <!--rightcol vége-->

		<div class="clear"></div>
		<div class="clear"></div>
        
		<br>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		<td width="42%">
        <div class="mybutton">    
        <a href="javascript:;" class="button toprevpage">
            <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
            Vissza a levél adataihoz
        </a>
        </div>
        </td>
		<td>
        <div class="mybutton">    
        <a href="javascript:;" class="button saveandexit">
            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
            Ment és kilép
        </a>
        </div>
        </td>
		<td width="42%">
        <div class="mybutton" style="float:right">    
        <a href="javascript:;" class="button rightbutton tonextpage">
			Tovább a tesztküldéshez&nbsp;
            <img src="<?=$base.$img ?>icons/arrow_right.png" alt=""/> 
        </a>
        </div>        
        </td>
		</tr>
		</table>
		
		
        
        	
	


	<div class="clear"></div>


	</div>     
<!-- CONTENT VÉGE -->
</div>  <!-- twcol VÉGE -->


				<?php 
				/// beszúrás dialog
				
				$sepStart = Kohana::config('core.fieldSeparatorStart');
				$sepEnd = Kohana::config('core.fieldSeparatorEnd');
				$unsubLink = $sepStart.Kohana::config('core.unsubscribe_link_name').$sepEnd;
				$datamodLink = $sepStart.Kohana::config('core.datamod_link_name').$sepEnd;
				$activateLink = $sepStart.Kohana::config('core.activation_link_name').$sepEnd;
				$email = $sepStart."email".$sepEnd;
				$regdatum = $sepStart."regdatum".$sepEnd;				
				?>

	                <script>
	                $(function(){

	            		var beszuras_dialog = function(){ 
	            			var dialog = $("#beszuras_dialog").dialog({
	            			bgiframe: false,
	            			resizable: false,
	            			width:475,
	            			modal: false,
	            			closable:true,
	            			position: ['center','center'],
	            			overlay: {
	            				backgroundColor: '#000',
	            				opacity: 0.2
	            			}
	            		}); 
	            		}
	            	 
	            		$(".beszurasClick").click(function(){
	            			var rel = $(this).attr('rel');	
	            			$('#fieldId2').val(rel);		
	            			beszuras_dialog();
	            			$('#beszuras_dialog').dialog('open');
	            			$('#beszuras_dialog').show();
	            			
	            		});

	                	
	    				var SELECTED_LIST = "";
						$('#altalanosInsert2').click(function(){
							var id = $('#fieldId2').val();
							var content = $('#'+id).val();
							var tag = $('#altalansTag2').val();

							
							if(tag == "null" || tag == null){
								tag = "";
							}
							
							$('#'+id).val(content+""+tag);
							$('#'+id).scrollTop(100000);
							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		  
						});
						
						$('#globalInsert').click(function(){
						
	    					tinyMCE.execInstanceCommand('html_content_editor',"mceInsertContent",false,$('#globalfields').val())
				
						
						});
						
	                	$('#listaInsert2').click(function(){
							var id = $('#fieldId2').val();
							var content = $('#'+id).val();
							var tag = $('#'+$('#listak2').val()).val();

							if(tag == "null" || tag == null){
								tag = "";
							}

							$('#'+id).val(content+""+tag);
							$('#'+id).scrollTop(100000);

							
							$('#beszuras_dialog').dialog('close');
							$('#beszuras_dialog').hide();		                	
	                	});
	                	
	                	$('#listak2').change(function (){
	                		$('.fieldContainer2').hide();
	                		$('#'+$('#listak2').val()).show();
	                	});
	                	
	                });
	                </script>

<div id="beszuras_dialog" title="Behelyettesítő beszúrása" style="display:none">
        		<div id="behelyettesito">
	        		<input type="hidden" id="fieldId2" value=""/>
	        		<table>
	        			<tr>
	        				
	        				<td valign="top">
		        				<div class="bghighlight"><h3 class="sidebar">Általános behelyettesítők:</h3></div>		
	
				                <select size="5" style="width:210px" id="altalansTag2">
				                	<option value="<?=$unsubLink ?>"><?=$unsubLink ?> - leiratkozás url</option>
				                	<option value="<?=$datamodLink ?>"><?=$datamodLink ?> - adatmódosító url</option>
				                	<option value="<?=$activateLink ?>"><?=$activateLink ?> - aktiváló url</option>
				                	<option value="<?=$regdatum ?>"><?=$regdatum ?> - regisztráció dátuma</option>
				                	<option value="<?=$email ?>"><?=$email ?> - e-mail</option>
				                </select>
				                
	        				</td>
	        				
	        				<td valign="top" align="right">

								<div class="bghighlight" style="text-align:center"><h3 class="sidebar">Listák mezői:</h3></div>
								Lista:
				                <select size="1" style="width:170px" id="listak2">
				                	<?php foreach($lists as $list): 
				                	?>
				                		<option value="2list_<?=$list->id ?>"><?=$list->name ?></option>
				                	<?php endforeach; ?>
				                </select>
								<p><br/></p>
									Mező:
				                	<?php 
				                		$first = true;
				                		foreach($lists as $list):  
				                		
				                			$fields = $list->fields();
				                	?>
										
										<select class="fieldContainer2" size="1" style="width:170px;display:<?php echo ($first) ? "true" : "none";$first = false; ?>" id="2list_<?=$list->id ?>">			
											<?php foreach($fields as $field): ?>
											<option value="<?=$sepStart.$field->reference.$sepEnd ?>"><?=$sepStart.$field->reference.$sepEnd ?> - <?=$field->name ?></option>
											<?php endforeach; ?>
										</select>
				                	<?php endforeach; ?>
				
				                
	        				</td>
	        				
	        			</tr>

						<tr>
							<td align="center">
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="altalanosInsert2">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div>							
							</td>
							<td align="center">
							
								<div class="mybutton" style="width:90px">    
					            	<a href="Javascript:;" class="button" id="listaInsert2">
					                	<img src="<?=$base.$img ?>icons/wand-small.png" alt=""/> 
					                	Beszúr
					            	</a>
					            	<div style="clear:both"></div>
					        	</div> 								
							
							</td>
						</tr>
	        		</table>
				</div>
</div>
<?php /// beszúrás dialog ?> 