<style>
	.optionValue{
		cursor:move;
	}

</style>
<script>
	
	var id = 0;
	
	$(function(){
		
		var initType = function(name){

			type = $("#dataTypeCombo").val();
			name = $("#fieldName").val();			
			
			if( (type == "singleselectradio" || type == "singleselectdropdown" || type == "multiselect") && name!=""){
				
				$("#fieldNameText").html(name);

				$("#optionList").sortable(	{
						activeclass : 'sortableactive',
						hoverclass : 'sortablehover',
						axis : 'vertically',
						opacity: 	0.5,
						fit :	false,
						stop: serializeOptions
				});
				
				serializeOptions();
				
				$("#multiple").show();

			}else if( (type == "singleselectradio" || type == "singleselectdropdown" || type == "multiselect") && name=="") {
				
				alert("Kérem adja meg a mező nevét!");
				$("#dataTypeCombo").val("lastname");

				$("#multiple").hide();				
				
			}else{
				$("#multiple").hide();
			}
		}
		
		$("#dataTypeCombo").change( function() { 
			initType();
		} );

		var serializeOptions = function(){
			
			$("#optionValuesInputs").empty();		
			$(".optionli").each( function(){ 
				$("#optionValuesInputs").append("<input type=\"hidden\" value=\""+$(this).attr("rel")+"\" name=\"options[]\" /> ");	
				$("#optionValuesInputs").append("<input type=\"hidden\" value=\""+$(this).attr("code")+"\" name=\"codes[]\" /> ");	
			});		
			
			editLinks();
			deleteLinks();
			
		}
		
	
		$("#addButton").click( function(){
			name = $("#optionNameForAdd").val();
			
			if(name != ""){
				
				id++;
				
				$("#optionList").append("<li class=\"optionli\" rel=\""+name+"\" code=\"nincscode\" ><a href=\"javascript:;\" class=\"deleteOption\" ><img height=\"10\" width=\"10\" class=\"supporting\" alt=\"Delete this option\" src=\"<?=$base.$img ?>icons/smallOff.png\"/></a>&nbsp;&nbsp;<span class=\"optionValue\" rel=\"+id+\" >"+name+"</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"\">(<a class=\"editLink\" href=\"javascript:;\">szerkeszt</a>)</span></li>");
				
				
				$("#optionNameForAdd").val("");
				
				serializeOptions();
				
			}else{
				alert("Kérem adja meg a nevet.")
			}
			
			
		});
		
		var deleteLinks = function(){
			$('.deleteOption').each(function(){
				$(this).click(function(){
					$(this).parent().remove();
					serializeOptions();
				});					
			});
		}
		
		var isThereAnEdit = false;
		
		var editLinks = function(){
			
			//var ezkerulvissza = "<img height=\"10\" width=\"10\" class=\"supporting\" alt=\"Delete this option\" src=\"<?=$base.$img ?>icons/smallOff.png\"/></a>&nbsp;&nbsp;<span class=\"optionValue\" rel=\"+id+\" >"+name+"</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"\">(<a class=\"editLink\" href=\"javascript:;\">szerkeszt</a>)</span>";
				
			$(".editLink").each( function() {
				
				$(this).click(function(){
					
					if(!isThereAnEdit){
						li = $(this).parent().parent();
						
						$(li).empty();
						
						$(li).append("<input id=\"underEdit\" type=\"text\" value=\""+$(li).attr("rel")+"\" />&nbsp;<span class=\"\">(<a class=\"OKLink\" href=\"javascript:;\">OK</a>)</span>");
						
						okLinks();
						isThereAnEdit = true;
					}else{
						alert("Egyszerre csak egy opciót tud szerkeszteni!");
					}

					
				});
				
			});
			
		}		
		
		
		var okLinks = function(){
			
			$(".OKLink").each( function() {
				
				$(this).click(function(){
					
					name = $("#underEdit").val();
					
					li = $(this).parent().parent();
					
					if(name != ""){
						$(li).attr("rel",name);	
					}
					
					$(li).empty();
					
					$(li).append("<a href=\"javascript:;\" class=\"deleteOption\" ><img height=\"10\" width=\"10\" class=\"supporting\" alt=\"Delete this option\" src=\"<?=$base.$img ?>icons/smallOff.png\"/></a>&nbsp;&nbsp;<span class=\"optionValue\" rel=\"+id+\" >"+$(li).attr("rel")+"</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"\">(<a class=\"editLink\" href=\"javascript:;\">szerkeszt</a>)</span>");
					
					serializeOptions();					
					isThereAnEdit = false;
				});
				
			});			
			
			
		}
		
		initType();
		
	var SELECTED_FIELD = 0;	
 	$('.deleteField').click(function(){
 		SELECTED_FIELD = $(this).attr('rel');
 		
 		$('#fieldrow_'+SELECTED_FIELD).css('background-color','#ffb2b2');
 		
 		$('.selectionName').html($(this).attr('relName'));
 		
 		deleteField();
 	});
 	
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			close: function(event, ui) {$('#fieldrow_'+SELECTED_FIELD).css('background-color',''); },
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			close: function(event, ui) {$('#fieldrow_'+SELECTED_FIELD).css('background-color',''); },			
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 

 	var deleteField = function(){ 
			
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			$('#fieldrow_'+SELECTED_FIELD).css('background-color','');		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();	
	});	 		
		
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/fields/deleteField/'+SELECTED_FIELD;
					
	});			
		
	$('#dataTypeCombo').val('<?=$form['type'] ?>');
	initType();
		
		
	});

</script>

<div id="dialog" title="Mező törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Valóban törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" mezőt?</strong><br />
	A feliratkozók mezőhöz tartozó adatai is törölve lesznek!
    </div>

<div class="delete_dialog_short_buttons">
	
	<div class="mybutton" id="sendingMessage">
		<button id="deleteYesButton" type="submit" class="button">
	    	<img src="<?=$base.$img ?>icons/delete.png" alt=""/>Igen
	    </button>
   </div>	

	<div class="mybutton" id="sendingMessage" >
		<button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
	    	<img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
	    </button>
   </div>	

</div>

<div id="dialogOk" title="Mező törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" mezőt?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>
	
</div>	

	<div id="content">

	
		<div id="listMake">
	        <h1 class="step1">"<?=$_SESSION['selected_client']->name ?>" honlap mezőinek megadása</h1>
        </div>
		
		<!-- 
		<p class="bread">
			<a href="default.aspx">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
				<a href="#">Lista neve</a>
			<span class="breadArrow">&nbsp;</span>
				<a href="">Lista mezőinek módosítása</a>
		
		</p>		
		-->
		
		
		
		
		
        <div class="clear"></div>
        
		<!--<h1>Manage custom fields</h1>-->
		<?=$alert ?>					
		<?=$errors ?>
		
		<p class="extraBottomPad">
		Új mező hozzáadásához adja meg a mező elnevezését és a mező adattípusát. Használhat előre definiált adattípusokat, mint pl. a keresztnév, családnév, de akár kitalálhat új egyedi mezőket is az általános típusokkal (dátum, egysoros szöveg, többsoros szöveg).<br/>
		Ha szeretne több listát összekapcsolni a késöbbiek során, akkor ajánlott, hogy az előre definiált mezőtípusokat használja, hisz csak az alapján lehet a listák között a feliratkozókat másolni.
		</p>
		
		<div class="formBG">
		<div class="formWrapper">
			
		<form name="addCustomField" action="<?=$postUrl?>" method="post" onSubmit="">
					
			<h3><?=$headermsg ?></h3>
		
			<table width="100%" cellspacing="0" cellpadding="4" border="0">
			<tr>
				<td bgcolor="#e4e4e4" class="smallPad" nowrap="nowrap">
					<img src="<?=$base.$img ?>_space.gif" width="5" height="1" border="0">
				</td>
			    
			    <td bgcolor="#e4e4e4" class="mid" nowrap="nowrap">
			    
				    <img src="<?=$base.$img ?>_space.gif" width="1" height="4" border="0"><br/>
				    	
	                <strong>Mező neve</strong><br>
	                
	                	<input type="text" name="name" id="fieldName" size="30" tabindex="1" value="<?=$form['name'] ?>"  class="<?=$classes['name'] ?> input_text">
	                	<br/>
	                	<img src="<?=$base.$img ?>_space.gif" width="1" height="5" border="0"><br>
                </td>

			    <td bgcolor="#e4e4e4" class="mid" nowrap="nowrap">

	                <img src="<?=$base.$img ?>_space.gif" width="1" height="5" border="0"><br />
	
	                <strong>Adattípus</strong><br>
						<select id="dataTypeCombo" class="clearfix" tabindex="3" name="type">
							<optgroup label="Előre definiált mezők">
								<option value="fullname">Teljes név</option>
								<option value="lastname">Vezetéknév</option>
								<option value="firstname">Keresztnév</option>
								<option value="fulladdress">Teljes Cím</option>
								<option value="country">Ország</option>
								<option value="state">Megye</option>
								<option value="city">Város</option>
								<option value="street">Utca házszám</option>
								<option value="phone">Telefonszám</option>
								<option value="mobile">Mobilszám</option>
								<option value="fax">Fax</option>
								<option value="www">Weboldal</option>
							</optgroup>
							<optgroup label="További adattípusok">
								<option value="date">Dátum</option>
								<option value="text">Egysoros szöveg</option>
								<option value="multitext">Többsoros szöveg</option>
								<option value="multiselect">Választó checkbox</option>
								<option value="singleselectdropdown">Választó legördülő</option>
								<option value="singleselectradio">Választó rádió gomb</option>
							</optgroup>
							
						</select>	                
	                
	                
	                <?php /*
	                <select id="dataTypeCombo" name="type" tabindex="3" class="clearfix">
	                	
	                	<?php foreach ($fieldTypes as $key => $ft): ?>
		                	
		                	<?php if($form['type'] == $ft->code): ?>		                	
		                		<option value="<?=$ft->code ?>" selected="selected"><?=$ft->value ?></option>
		                	<?php else: ?>
		                		<option value="<?=$ft->code ?>" ><?=$ft->value ?></option>
		                	<?php endif; ?>
		                	
	                	<?php endforeach; ?>
	                	
	                </select> */
	                 ?>
	                
	                <br/>
	                
	                <img src="<?=$base.$img ?>_space.gif" width="1" height="5" border="0"><br>
                
                </td>
                
                <td bgcolor="#e4e4e4" class="mid" width="100%" style="padding:15px 0 0 15px;">
                	<img src="<?=$base.$img ?>_space.gif" width="1" height="5" border="0"><br />
                	
                	<?php if($form['grid'] == 1): ?>
                		<input id="0_box" type="checkbox" name="grid" value="1" class="clearfix" checked="checked"/>
                	<?php else: ?>
                		<input id="0_box" type="checkbox" name="grid" value="1" class="clearfix"/>
                	<?php endif; ?>
                	 <strong>Táblázat</strong> <span class="regenerate" style="font-weight:normal">(Jelölje be, ha azt szeretné, hogy a lista tagjait megjelenítő táblázatban szerepeljen.)</span>
                </td>
			</tr>
			</table>
            
			
			<div id="multiple" style="display:none">
			
			<table width="100%" cellspacing="0" cellpadding="3" border="0" bgcolor="#d3d3d3">
				<tbody>
				<tr>
					<td nowrap="nowrap" class="smallPad">
						<img height="1" width="5" border="0" src="<?=$base.$img ?>_space.gif"/>
					</td>
					
					<td  width="100%" colspan="2">
						<img height="5" width="1" border="0" src="<?=$base.$img ?>_space.gif"/><br/>
                    	<strong><span id="fieldNameText"/></strong> opciói:
                    </td>
                    
				</tr>
				<tr>
					<td nowrap="nowrap" class="smallPad"><img height="1" width="5" border="0" src="img/_space.gif"/></td>
					<td class="mid" colspan="2">
                    
					<div id="fieldOptions">
                    	
						<div id="optionValuesInputs">
							
						</div>
						
                    <ol id="optionList">
                    	
						<?php foreach($fieldvalues as $fv):?>
						
							<li class="optionli" rel="<?=$fv->value?>" code="<?=$fv->code?>" >
								<a href="javascript:;" class="deleteOption" >
									<img height="10" width="10" class="supporting" alt="Delete this option" src="<?=$base.$img ?>icons/smallOff.png"/>
								</a>&nbsp;&nbsp;
								<span class="optionValue" rel="<?=$fv->id?>" ><?=$fv->value?></span>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="">(<a class="editLink" href="javascript:;">szerkeszt</a>)</span>
							
							</li>
						
						<?php endforeach;?>
                    <!--
	                    <li>
	                    
		                    <input type="hidden" value="aaa 1. változója" name="fieldOptionsArray"/>
		                    <input type="hidden" value="0" name="fieldOptionsIDsArray"/>
		                    
		                    <a onclick="" href="">
		                    	<img height="10" width="10" class="supporting" alt="Delete this option" src="<?=$base.$img ?>icons/smallOff.png"/>
		                    </a>
		                    
		                    <a href="#">
		                    	<img height="10" width="10" class="supporting" alt="Move this option up a place" src="<?=$base.$img ?>icons/up.png"/>
		                    </a>
		                    
		                    <a href="#">
		                    	<img height="10" width="10" class="supporting" alt="Move this option down a place" src="<?=$base.$img ?>icons/down.png"/>
		                    </a>
		                    aaa 1. változója
		                    
		                    <span class="editLink">(<a href="#">szerkeszt</a>)</span>
	                    
	                    </li>
                    -->
                    
                    </ol>
                    
                    </div>
					
					</td>
				</tr>
				<tr>
					
					<td nowrap="nowrap" class="smallPad"><img height="1" width="5" border="0" src="<?=$base.$img ?>_space.gif"/></td>
				    
				    <td nowrap="nowrap" class="midRed"><img height="5" width="1" border="0" src="<?=$base.$img ?>_space.gif"/><br/>
                    
                    	<input type="text" id="optionNameForAdd" tabindex="4" size="50" name="optionName" class="input_text"/>
                    	
						<br/>
                    	
                    	<img height="5" width="1" border="0" src="<?=$base.$img ?>_space.gif"/><br/>
                    </td>
                    
				    <td width="100%" class="mid">
                    
<!--	                    <span  style="">
	                    	<a href="javascript:" id="addButton">
	                    		<img height="22" width="90" border="0" src="<?=$base.$img ?>buttons/add-option.gif"/>
	                    	</a>
	                    </span>-->
                        
                    <div class="mybutton" style="">    
                        <a href="javascript:" id="addButton" class="button slim">
                            <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                            Opció hozzáadása
                        </a>
                    </div> 
                    
                    </td>
				</tr>

				</tbody>
				</table>
			
			</div>
			
			<div class="topPad"></div>
			
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/add.gif" width="16" height="16" alt=""/> 
                    <?=$buttonmsg ?>
                </button>
                <div style="clear:both"></div>
        	</div>
			
			</form>
			
			<div class="clearButton"></div>			

		</div>		
		</div>
		
		<h3 class="extraTopPad">"<?=$_SESSION['selected_client']->name ?>" honlap globális mezői</h3>
		<?php 
			$sepStart = Kohana::config('core.fieldSeparatorStart');
			$sepEnd = Kohana::config('core.fieldSeparatorEnd');
		?>	
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="tableHeader">
		<tr class="noHighlight">
        	<th width="15" class="headerLeft">&nbsp;</th>
		    <th width="50%">Mező neve</th>
		    <th>Behelyettesítő tag</th>
		    <th width="120" nowrap>Adattípus<br /><img src="img/_space.gif" width="120" height="1" alt=" "></th>
		    <th width="20" nowrap class="headerRight">&nbsp;</th>
		</tr>

		<tr>
        	<td class="rowLeft"><img alt="Alapadat" src="<?=$base.$img ?>icons/smallTick.png"/></td>
		    <td>E-mail cím</td>
		    <td><?=$sepStart?>email]</td>
		    <td><span>E-mail</span></td>
		    <td class="rowRight">&nbsp;</td>
		</tr>

		<tr>
        	<td class="rowLeft"><img alt="Alapadat" src="<?=$base.$img ?>icons/smallTick.png"/></td>
		    <td>Regisztráció dátuma</td>
		    <td><?=$sepStart?>regdatum<?=$sepEnd?></ndtd>
		    <td><span>Dátum</span></td>
		    <td class="rowRight">&nbsp;</td>
		</tr>

		<?php foreach ($fields as $f): ?>

			<tr class="fieldRow" id="fieldrow_<?=$f->id ?>">
	        	<td class="rowLeft">
	        		
	        		<?php if($f->grid): ?>
	        			<img alt="Alapadat" src="<?=$base.$img ?>icons/smallTick.png"/>
	        		<?php endif; ?>
	        	</td>
			    <td><?=$f->name ?> <span>(<a href="<?=url::base() ?>pages/fields/index/<?=$f->id ?>">szerkeszt</a>)</span></td>
			    <td><?=$sepStart?><?=$f->reference ?><?=$sepEnd?></td>
			    <td><span><?=$f->type() ?></span></td>
			    <td class="rowRight">
			    	<span relName="<?=$f->name ?>" rel="<?=$f->id ?>" style="display:block" class="deleteField">
			    		<a href="JavaScript:;" title="Mező törlése" >
			    			<img src="<?=$base.$img ?>icons/trash.png" width="10" height="11" alt="Delete">
			    		</a>
			    	</span>
			    </td>
			</tr>

		<?php endforeach; ?>

		</table>


		<input type="hidden" name="fCount" value="3">

	
    <br />
    <form action="<?=url::base() ?>pages/listdetail">
        <div class="mybutton">
            <button type="submit" class="button">
                <img src="<?=$base.$img ?>icons/accept.png" width="16" height="16" alt=""/> 
                Szerkesztés befejezése
            </button>
            <div style="clear:both"></div>
        </div>
	</form>
			<!-- <span class="formcancel"><a href="#" id="cancelSaveChanges">mégsem</a></span> -->
			<div class="clearButton"></div>
	
	<div class="clear"></div>
	
	</div>