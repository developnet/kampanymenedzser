<script>
$(function(){

 
		var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	
		
		
	
	$('#duplicateNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#duplicateYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			window.location = '<?=url::base() ?>pages/letterdetail/duplicateletter';
	});			

	$('#duplicatelink').click(function(){
			dialog();				
			$('#dialog').dialog('open');
			$('#dialog').show();
	});	


});
</script>

	
	
		
<div style="clear:both"></div>

<div id="dialog" title="Levél duplikálása" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Valóban duplikálni szeretné a levelet?</strong><br />
	A levél duplikálásával egy új levelet hoz létre az összes beállítás lemásolásával.
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="duplicateYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/email_add.png" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="duplicateNoButton" type="submit" class="button" style="margin-left:20px;">
                Nem
            </button>
        </div>	
    </div>

</div>



<!-- CONTENT -->
    
	<div id="singleDoubleContent">

		<div id="listMake">
        <div id="Steps">
        	<div class="Container">
            	<div class="StepsCubes">1</div>
                Levél adatai
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">2</div>
                Levél tartalma
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">3</div>
                Tesztküldés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">4</div>
                Időzítés
            </div>            
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">5</div>
                Címzési feltételek
            </div>
        </div>
        
        <?php
			$status = "";
			$mod = 0;
			if($letter->timing_type == 'relative' && $letter->status == "draft"){
				$status = "Relatív, szerkesztés alatt álló levél.";
				$mod = 1;
			}elseif($letter->timing_type == 'relative' && $letter->status == "active"){
				$status = "Relatív, kiküldés alatt álló - aktív - levél.";
				$mod = 2;
			}elseif($letter->timing_type == 'absolute' && $letter->status == "draft"){
				$status = "Abszolút, szerkesztés alatt álló levél";
				$mod = 3;
			}elseif($letter->timing_type == 'absolute' && $letter->status == "active" && $letter->isSent()){
				$status = "Abszolút, elküldőtt levél.";
				$mod = 4;
			}elseif($letter->timing_type == 'absolute' && $letter->status == "active" && !$letter->isSent()){
				$status = "Abszolút, még nem elküldött levél.";
				$mod = 5;
			}elseif($letter->timing_type == 'sendnow' && $letter->status == "draft"){
				$status = "Azonnali, szerkesztés alatt álló levél.";
				$mod = 6;
			}elseif($letter->timing_type == 'sendnow' && $letter->status == "active" && $letter->isSent()){
				$status = "Azonnali, aktivált, elküldőtt levél.";
				$mod = 7;
			}elseif($letter->timing_type == 'sendnow' && $letter->status == "active" && !$letter->isSent()){
				$status = "Azonnali, aktivált, még nem elküldőtt levél.";
				$mod = 8;
			}else{
				$status = "Hiányos levél.";
				$mod = 9;				
			}
			
			
			if(!isset($_SESSION['alert']) && $letter->isReady() && $letter->status != "active"){
				meta::createAlert("alert","A levél kész a kiküldésre!","A levél végelgesítéséhez, nyomja meg az \"Levél aktiválása\" gombot!");	
			}
			
        ?>
        
        <h1 class="step1">
        	Levél: <?=$letter->name ?>
        	<?php echo (empty($letter->note)) ? "" : '<a href="Javascript:;" title="'.nl2br($letter->note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?>
        	<br/>
        	<span>Kampány: <span><?=$campaign->name ?></span></span><br/>
        	<span>Státusz: <span><?=$status ?></span></span>
        </h1>
        </div>
	
        
	</div> <!--singleDoubleContent end-->
    
    
    
    <div id="content">
           
         <?=$errors ?> 
         <?php
			if(isset($_SESSION['alert'])){
				echo $_SESSION['alert'];
				unset($_SESSION['alert']);
			}
         ?>  
           
		<div class="formBG">
		<div class="formWrapper">
			
			<!-- ////////////////////////// -->
			<h3>
            <?php if($letter->status == "draft"): ?>
            <div class="mybutton" style="float:right;">    
            <a href="<?=url::base() ?>pages/letterdetail/first" class="button slim">
                <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
                Levél adatainak szerkesztése
            </a>
        	</div>
            <?php else: ?>
				
					            
	        	            
            <?php endif; ?>            
            
            Levél adatai
            </h3>

			<div class="formContainer">

			<table cellpadding="2" cellspacing="0" width="100%" class="grid">

			<tr>
			    <th style="width:200px">Levél megnevezése</th>
			    <td><?=$letter->name ?></td>
			</tr>
			<tr>
			    <th>Tárgy</th>
			    <td><?=$letter->subject ?></td>

			</tr>
			<tr>
			    <th>Küldő név</th>
			    <td><?=$letter->sender_name ?></td>
			</tr>
			<tr>
			    <th>Küldő e-mail</th>

			    <td><?=$letter->sender_email ?></td>
			</tr>
		    <tr>
		        <th class="last">Válasz e-mail</th>
		        <td class="last"><?=$letter->sender_replyto ?></td>
		    </tr>
			</table>

			</div>
			<!-- ////////////////////////// -->

			<!-- ////////////////////////// -->			
			<h3 class="topPad">
            
            <?php if($letter->status == "draft"): ?>
	            <div class="mybutton" style="float:right;">    
	            <a href="<?=url::base() ?>pages/letterdetail/second" class="button slim">
	                <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
	                Levél tartalmának szerkesztése
	            </a>
	        	</div>            
            <?php else: ?>
				
					            
	        	            
            <?php endif; ?>

        	
            Levél tartalma
			<img src="<?=$base.$img?>icons/small<?=$letter->contentCheck()?>.png" alt="">
			</h3>
			
			<div class="formContainer">
			<?php if($letter->contentCheck(true)): ?>
			<table cellpadding="2" cellspacing="0" width="100%" class="grid">
				<tr>
				    <td class="last" style="width:200px" nowrap><strong>Típusa</strong>&nbsp;&nbsp;&nbsp;</td>
					<td class="last" style="width:100px"><?=$letter->type ?></td>
					<td class="last" style="width:125px;text-align:right"><a href="<?=url::base()."api/common/showLetter/".$letter->id ?>" target="_blank">Levél megtekintése</a></td>
	                <td class="last">&nbsp;</td>
				</tr>
			</table>
			<?php else: ?>
			<table cellpadding="2" cellspacing="0" width="100%" class="grid">
				<tr>
				    <td class="last" style="width:100%" align="center" nowrap>A levél tartalma még nincs megadva.</td>
				</tr>
			</table>			
			<?php endif; ?>
			</div>
			<!-- ////////////////////////// -->			
		    
			
			<!-- ////////////////////////// -->
			<h3 class="topPad">
       	
            <?php if($letter->status == "draft"): ?>
            <div class="mybutton" style="float:right;">    
            <a href="<?=url::base() ?>pages/letterdetail/test" class="button slim">
                <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
                Tesztküldés
            </a>
        	</div>
        	<?php else: ?>        	
        	<?php endif; ?>
			Tesztküldés
			<img src="<?=$base.$img?>icons/small<?=$letter->testCheck()?>.png" alt="" />
			</h3>
			
			<div class="formContainer">
				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
					<?php $i=0; foreach($letter->getTests() as $t): ?>
					
					<?php if($i == 0): ?>
					<tr>
						<th>Teszt létrehozása</th>
						<th>Címzett</th>
						<th>Eredmény</th>
					</tr>
					<?php endif; $i++; ?>						
										
					<?php 
						if($i > 3){
							echo '<tr style="display:none" class="moreTest">';
						}else{
							echo '<tr>';
						}
					?>
					
						<td class="last" style="width:200px"><?=$t->created_time ?></td>
						<td class="last"><?=$t->email_recipient_email ?></td>
						<td class="last">
							<?php if($t->status == 'created' || $t->status == 'prepared') echo 'A teszt nem lett elküldve.';
								  elseif($t->status == 'sent') echo 'A teszt sikeresen elküldve.';
								  elseif($t->status == 'error') echo 'A Tesztküldése során hiba volt.';
							?>
						</td>
					</tr>

					<?php 
						if($i == 3){
							echo '<tr><td colspan="3"><a href="Javascript:;" id="showMoreTest">többi teszt küldés mutatása/elrejtése </a></td></tr>';
							
						}
					?>
				
					<?php endforeach; ?>
					<?php if(sizeof($letter->getTests()) == 0): ?>
					<tr>
					    <td class="last" style="width:100%" align="center" nowrap>A levél még nem lett tesztelve.</td>
					</tr>					
					<?php endif; ?>
				</table>
			</div>

			<script>
			$(function(){
				$('#showMoreTest').click(function(){
						$('.moreTest').toggle();
					});
			});
			
			</script>
			<!-- ////////////////////////// -->			
			
			
			




			
			
			<!-- ////////////////////////// -->
			<h3 class="topPad">
            
            <?php if($letter->status == "draft"): ?>
            <div class="mybutton" style="float:right;">    
            <a href="<?=url::base() ?>pages/letterdetail/timing" class="button slim">
                <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
                Időzítés szerkesztése
            </a>
        	</div>
        	<?php else: ?>        	
        	<?php endif; ?>
        	            
			Időzítés
			<img src="<?=$base.$img?>icons/small<?=$letter->timingCheck()?>.png" alt="">
			</h3>
			<?php
				if($letter->timing_type == "relative"){
					$tim = "Relatív";
				}else if($letter->timing_type == "absolute"){
					$tim = "Abszolút";
				}else{
					$tim = "Azonnali";
				}
			?>
			<div class="formContainer">
				<?php if($letter->timingCheck(true)): ?>
				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
				<tr>
					<th style="width:200px">Időzítés típusa</th>
					<th colspan="2"><?=$tim ?></th>
				</tr>
				
				<?php if($letter->timing_type == "relative"): ?>
					<tr>
						<td class="last" style="width:200px">Levél küldése feliratkozás után</td>
						<td class="last" colspan="2"><strong><?=$letter->timing_event_value ?></strong> nappal</td>
					</tr>
				<?php endif; ?>
				
				<?php if($letter->timing_type == "absolute"): ?>
					<tr>
						<td class="last" style="width:200px">Abszolút időzítés</td>
						<td class="last"><?=dateutils::formatDate($letter->timing_date) ?></td>
						<td class="last">
						<?php
							if($letter->timing_hour == NULL){
								echo 'A levél 2 óra és 6 óra között kerül kiküldésre';
							}else{
								echo 'A levél '.$letter->timing_hour.' órakor kerül kiküldésre';
							}
						?>
						</td>
						
						
						
					</tr>
				<?php endif; ?>			

				<?php if($letter->timing_type == "sendnow"): ?>
					<tr>
						<td class="last" style="width:200px">Azonnali küldés</td>
						<td class="last">
							<?php if($letter->isSent()){
								echo "Elküldve: ".dateutils::formatDate($letter->isSent(TRUE));
							}else{
								echo "Még nem lett elküldve.";
							} 
							?>
						</td>
					</tr>
				<?php endif; ?>	
				</table>
				<?php else: ?>
				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
					<tr>
					    <td class="last" style="width:100%" align="center" nowrap>A levél időzítése még nem lett megadva.</td>
					</tr>
				</table>			
				<?php endif; ?>
			
			</div>
			<!-- ////////////////////////// -->			

			
			<!-- ////////////////////////// -->
			<h3 class="topPad">
            
            <?php if($letter->status == "draft"): ?>
            <div class="mybutton" style="float:right;">    
            <a href="<?=url::base() ?>pages/letterdetail/recipients" class="button slim">
                <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
                Címzési feltételek szerkesztése
            </a>
        	</div>
        	<?php else: ?>
				
					            
	        	            
            <?php endif; ?>
        	
        	
        	<?php if($mod == 2 || $mod == 4 || $mod == 7): ////////////ha már i lett küldve?>
        			Címzettek száma	
        	<?php else:?>
        			Címzési feltételek
        	<?php endif;?>
            
			<img src="<?=$base.$img?>icons/small<?=$letter->recipCheck()?>.png" alt="">
			</h3>
			
			<div class="formContainer">
				
				<?php 
				if($letter->recipCheck(true)): 
					$listsIncludes = $letter->listsIncludes();
					$listsExcludes = $letter->listsExcludes();
					$emailConditions = $letter->emailConditions();
		
					$groupsPlus = $letter->groupCondArray('plus');
					$groupsMinus = $letter->groupCondArray('minus');
				
				?>
				
					
    				<?php if($mod == 2 || $mod == 4 || $mod == 7): ////////////ha már ki lett küldve?>
    				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
    				
    				<tr>
						<td class="" width="200" class="sum"><strong>Kiküldve összesen:</strong></td>
						<td class="" width="220" class="sum" style="text-align:left"><strong><?=$letter->sentMembers() ?></strong> feliratkozónak</td>
                        <td class="">&nbsp;</td>
    				</tr> 
    				<?php if($letter->timing_type == "relative"):

    						$today = date("Y.m.d");
    						$yesterday = date("Y.m.d", strtotime("-1 days"));
    						$tomorrow = date("Y.m.d", strtotime("+1 days"));
    						$aftertomorrow = date("Y.m.d", strtotime("+2 days"));
//    				
    				?> 
    				<tr>
						<td class="last" width="200" class="sum"><strong>Tegnap megkapta (<?=$yesterday?>):</strong></td>
						<td class="last" width="220" class="sum" style="text-align:left"><strong><?=$letter->sentMembersToday($yesterday)?></strong> feliratkozó</td>
                        <td class="last">&nbsp;</td>
    				</tr>    				

    				<tr>
						<td class="last" width="200" class="sum"><strong style="color:green">Ma megkapta (<?=$today?>):</strong></td>
						<td class="last" width="220" class="sum" style="text-align:left"><strong><?=$letter->sentMembersToday($today)?></strong> feliratkozó</td>
                        <td class="last">&nbsp;</td>
    				</tr> 
    				
    				<tr>
						<td class="last" width="200" class="sum"><strong>Holnap megkapja (<?=$tomorrow?>):</strong></td>
						<td class="last" width="220" class="sum" style="text-align:left"><strong>0</strong> feliratkozó</td>
                        <td class="last">&nbsp;</td>
    				</tr>     				

    				<tr>
						<td class="last" width="200" class="sum"><strong>Holnap után megkapja (<?=$aftertomorrow?>):</strong></td>
						<td class="last" width="220" class="sum" style="text-align:left"><strong>0</strong> feliratkozó</td>
                        <td class="last">&nbsp;</td>
    				</tr>     				
    				
    				<tr>
						<td class="last" width="200" class="sum">&nbsp;</td>
						<td class="last" width="220" class="sum" style="text-align:left">&nbsp;</td>
                        <td class="last">&nbsp;</td>
    				</tr>    				
    				<?php endif;?>
    				
    				<tr>
						<td class="last" width="200" class="sum"><strong>Címzési feltételek:</strong></td>
						<td class="last" width="220" class="sum" style="text-align:left"></td>
                        <td class="sum last">&nbsp;</td>
    				</tr>
    				</table>
    				<?php endif;?>
    				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
    				<?php /* 
    				
					<!-- pluszok -->
					<?php 
						foreach($listsIncludes as $lc):
						$fields = $lc->fields();
					?>
					<?php if($lc->testCondition($letter->id,'plus') == '1'): ?>		 
						<tr>
							<td width="300"><strong><?=$lc->name ?></strong></td>
							<td width="120" style="text-align:right"><?=$lc->membersNumber() ?></td>
	                        <td>&nbsp;</td>
	    				</tr>					
						<?php else: ?>
						<tr>
							<td width="300"><strong><?=$lc->name ?></strong></td>
							<td width="120" style="text-align:right">&nbsp;</td>
	                        <td>&nbsp;</td>
	    				</tr>						
						<?php endif; ?>
					
						<?php foreach($lc->groups() as $g) : ?>
						
							<?php if(in_array($g->id,$groupsPlus)): ?>
							<tr>
								<td width="300" style="padding-left:15px;"><?=$g->name ?></td>
								<td width="120" style="text-align:right"><?=$g->getMemberCount($fields) ?></td>
		                        <td>&nbsp;</td>
		    				</tr>							
							<?php endif; ?>
							
						<?php endforeach; ?>					
					
					<?php endforeach; ?> 
					<!-- minuszok -->
					<?php 
						foreach($listsExcludes as $lc):
						$fields = $lc->fields();
					?>
						<?php if($lc->testCondition($letter->id,'minus') == '1'): ?>		 
						<tr>
							<td width="300" style="color:#C31C03"><strong><?=$lc->name ?></strong></td>
							<td width="120" style="text-align:right;color:#C31C03;"><?=$lc->membersNumber() ?></td>
	                        <td>&nbsp;</td>
	    				</tr>					
						<?php else: ?>
						<tr>
							<td width="300" style="color:#C31C03"><strong><?=$lc->name ?></strong></td>
							<td width="120" style="text-align:right">&nbsp;</td>
	                        <td>&nbsp;</td>
	    				</tr>						
						<?php endif; ?>
					
						<?php foreach($lc->groups() as $g) : ?>
						
							<?php if(in_array($g->id,$groupsMinus)): ?>
							<tr>
								<td width="300" style="padding-left:15px;color:#C31C03;"><?=$g->name ?></td>
								<td width="120" style="text-align:right;color:#C31C03;"><?=$g->getMemberCount($fields) ?></td>
		                        <td>&nbsp;</td>
		    				</tr>							
							<?php endif; ?>
							
						<?php endforeach; ?>					
					
					<?php endforeach; ?> 
*/ ?>
					<tr>
						<td class="last" width="300" class="sum"><strong>Összes címzett</strong></td>
						<td class="last" width="120" class="sum" style="text-align:right"><strong><?=$letter->getRecipientsNumberFilter() ?></strong></td>
                        <td class="sum last">&nbsp;</td>
    				</tr>    				
    				
    				
				</table>
				<?php else: ?>
				<table cellpadding="2" cellspacing="0" width="100%" class="grid">
					<tr>
					    <td class="last" style="width:100%" align="center" nowrap>A levél címzésének feltételei még nincsenek megadva.</td>
					</tr>
				</table>			
				<?php endif; ?>				
				

			</div>
			<!-- ////////////////////////// -->			
			

		</div>
		</div>
			
		<br>
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
		    <td width="41%">
                <div class="mybutton">    
                <a href="<?=url::base() ?>pages/campaignoverview" class="button">
                    <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
                    Vissza a kampány nézethez
                </a>
                </div>            
            </td>
			<td align="center">
            	
            	
            	<div class="mybutton">    
                <a href="javascript:;" id="duplicatelink" class="button">
                    <img src="<?=$base.$img ?>icons/email_add.png" alt=""/> 
                    Levél duplikálása
                </a>
                </div>
                            
			</td>
		    <td width="41%">
				
				<?php if($letter->status == "draft"): ?>
            	<div class="mybutton" style="float:right;">    
                <a href="<?=url::base() ?>pages/letterdetail/activate" class="button">
                    <img src="<?=$base.$img ?>icons/accept.png" alt=""/> 
                    Levél aktiválása
                </a>
                </div>
				<?php endif; ?>

				<?php if($mod == 8): ?>
				
					
				
					<?php if($sendjob->status =='_created'):	?>
						<div class="mybutton" id="createJobbutton" style="float:right;margin-left:30px;">    
		                <a href="javascript:;" class="button">
		                    <img src="<?=$base.$img ?>icons/email_go.png" alt=""/> 
		                    Levél azonnali kiküldése!
		                </a>
		                </div>
					<?php elseif($sendjob->status != 'sent'): ?>
						<div class="mybutton" id="createJobbutton" style="float:right;margin-left:30px;">    
		                <a href="javascript:;" class="button">
		                    <img src="<?=$base.$img ?>icons/email_go.png" alt=""/> 
		                    Levél azonnali kiküldésének folytatása!
		                </a>
		                </div>
					<?php else: ?>
						<h1>Elküldve: <?=$sendjob->sent_time ?></h1>
					<?php endif; ?>				
				
				

                
                
				<?php endif; ?>
				
				<?php if(($mod == 8 || $mod == 2 || $mod == 5) && $letter->timingCheckForHour(true)):?>
				<div class="mybutton" style="float:right;">    
                <a href="<?=url::base() ?>pages/letterdetail/editletter/<?=$letter->id ?>" class="button">
                    <img src="<?=$base.$img ?>icons/email_edit.png" alt=""/> 
                    Levél szerkesztése
                </a>
                </div>
                
	             <?php elseif(($mod == 8 || $mod == 2 || $mod == 5) && !$letter->timingCheckForHour(true)): ?>
	                <div style="float:left;width:200px;"><h3 style="text-align:center;margin:0;border:1px solid #F1DF95;background-color:#FFF5CA;padding:6px">Kiküldés folyamatban!</span></h3></div>
					<div class="mybutton" style="float:right;">    
	                <a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$letter->id ?>" class="button">
	                    <img src="<?=$base.$img ?>icons/google-ana.png" alt=""/> 
	                    Levél statisztikája
	                </a>
	                </div>	                
                <?php endif; ?>
                
				<?php if($letter->isSent()):?>
				<div class="mybutton" style="float:right;">    
                <a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$letter->id ?>" class="button">
                    <img src="<?=$base.$img ?>icons/google-ana.png" alt=""/> 
                    Levél statisztikája
                </a>
                </div>
                <?php endif; ?>
                                
                 
                
            </td>
		</tr>



		</table>

		
        
        
        <?php

        //$letter->getRecipientsNumber(FALSE, array(), array(), FALSE, TRUE);
        	
        ?>
        


	<div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
 