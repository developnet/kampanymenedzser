<?php 

$gomb = "Előfizetői profil mentése";
$title = 'Számlatörténet';

?>
<style>

.clearfix label{
	width:120px;
}

</style>

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>
		
			<?=$errors ?>
	 
    
    	<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft cellLeft" width="25%">Dátum</th>
            <th class="cellLeft" nowrap width="25%">Esemény</th>
			<th class="cellLeft" nowrap width="25%" class="cellCenter">Számla sorszáma</th>
			<th class="headerRight cellRight" nowrap width="25%">Összeg</th>
		</tr>
		<?php 
		$osszeg = 0;
		if(sizeof($log)>0):?>

			<?php 
			
			foreach($log as $l):?>
			<trclass="letterRow">
				<td class="rowLeft cellLeft" width="25%"><?=dateutils::formatDate($l->date);?></td>
	            <td class="cellLeft" nowrap width="25%"><?=$eventdict[$l->event_type]?></td>
				<td class="cellLeft" nowrap width="25%" class="cellCenter"><?=$l->invoice_number ?></td>
				<td class="rowRight cellRight" nowrap width="25%"><?php echo ($l->event_type == "invoice") ? "".$l->amount : "-".$l->amount; ?> Ft</td>
			</tr>			
			<?php 
			
				if($l->event_type == "invoice"){
					$osszeg = $osszeg + $l->amount;
				}else{
					$osszeg = $osszeg - $l->amount;
				}
			
			endforeach;
			
			?>
		
		<?php else:?>
 			<tr id="0" class="letterRow">
				<td class="center" align="center" colspan="4">Nincs még kiküldött számla.</td>
			</tr>
		
		<?php endif;?>

            <tr class="noHighlight">
                <td class="footerLeft simple" colspan="3" style="color:#000"><strong>Egyenleg (teljes időszakra):</strong></td>
                <td class="footerRight simple cellRight" style="color:#000"><strong><?=$osszeg?> Ft</strong></td>
            </tr>
		</table> 
     
        
	

	
    <div id="rightcol">

				<div id="options">

					<div class="bghighlight"><h3 class="sidebar">Előfizetői profil oldalai</h3></div>
					<dl class="icon-menu">		
		                <dt><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Előfizetői adatok" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldLink">Előfizetői adatok</a></dd>
						<div class="clear"></div>
						
		                <dt><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Egyenleg" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldLink">Egyenleg</a></dd>
						<div class="clear"></div>
		                <?php /* 
						<dt><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Számlák" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsLink">Számlatörténet</a></dd>
						<div class="clear"></div>
*/ ?>
		            </dl>
					</div>

					<div class="newFeatures">
                    &nbsp;
					</div>

				</div>

	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
