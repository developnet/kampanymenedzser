<script>

$(function(){

	$("#editSubscriberButton").click(function(){
		
		$("#editSubscriber").show();
		$("#viewSubscriber").hide();		
	
	});
	
	$("#editSubscriberDetailButton").click(function(){
		
		$("#detail_data_html").hide();
		$("#detail_data_html_edit").show();		
	
	});
	
	$(".cancelDetailSaveChanges").click(function(){
		
		$("#detail_data_html").show();
		$("#detail_data_html_edit").hide();	
		
	
	});	

	$(".cancelSaveChanges").click(function(){
		
		$("#editSubscriber").hide();
		$("#viewSubscriber").show();		
		
	
	});


	 $('.datepicker').datepicker({ dateFormat: 'yy.mm.dd' });

		var predata = {pre: [
		<?php foreach($member->getTags() as $tag): ?>
			{value: "<?=$tag->id?>", name: "<?=$tag->name?>"},
		<?php endforeach; ?>
		]};		
		
		$(".tags").autoSuggest(
			URL_BASE+"api/common/getTags", 
				{	
					minChars: 2,
					asHtmlID: "tags",
					neverSubmit: true,
					startText: "Adja meg a címkéket..",
					selectedItemProp: "name", 
					searchObjProps: "name",
					preFill: predata.pre
				}
			);

	var detail_dialog = function(){ 
		var dialog = $("#detail_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};

	var product_dialog = function(){ 
		var dialog = $("#product_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};

	var listadd_dialog = function(){ 
		var dialog = $("#listadd_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};

	var sendcommon_dialog = function(){ 
		var dialog = $("#sendcommon_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};
	
	var dialog_dellist = function(){ 
		var dialog = $("#dialog_dellist").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	



	$("#listadd_click").click(function(){
		listadd_dialog();					
		$('#listadd_dialog').dialog('open');
		$('#listadd_dialog').show();		
		$( ".datepicker" ).datepicker($.datepicker.regional['hu']);
		
	});

	$("#sentcommon_click").click(function(){
		sendcommon_dialog();					
		$('#sendcommon_dialog').dialog('open');
		$('#sendcommon_dialog').show();		
	});




	$(".productdetaillink").click(function(){
		var product_connect_id = $(this).attr('rel');
		
		product_dialog();					
		$('#product_dialog').dialog('open');
		$('#product_dialog').show();		
		$( ".datepicker" ).datepicker($.datepicker.regional['hu']);
		
		$.get(URL_BASE+"api/common/getProductEditForm/"+product_connect_id, function(data){
			
			$('#product_detail_data_html').html(data);
			$( ".datepicker" ).datepicker($.datepicker.regional['hu']);
		});			
		
		
	});
	
	$(".listdellink").click(function(){
		var list_id = $(this).attr('rel');
		$('#del_list_id').val(list_id);		
		dialog_dellist();					
		$('#dialog_dellist').dialog('open');
		$('#dialog_dellist').show();	
	});
	
	$('.cancelDelList').click(function(){
		$('#dialog_dellist').dialog('close');
		$('#dialog_dellist').hide();		
	});
	
	$(".listdetaillink").click(function(){
		var list_id = $(this).attr('rel');
		var list_name = $(this).attr('rel2');
		var member_id = <?=$member->id?>;			
		
		$('#detail_list_name').html(list_name);
		detail_dialog();

		$('#detail_dialog').dialog('open');
		$('#detail_dialog').show();
		$("#showResult").show();
		$("#showResult").html("Adatok betöltése..." + "<span class=\"import_loading\"><img src=\"<?=$base.$img ?>ajax-loader.gif\"  alt=\"\" align=\"\"/></span>");
		
		$.get(URL_BASE+"api/common/getMemberData/"+member_id+"/"+list_id, function(data){
			
			$.get(URL_BASE+"api/common/getMemberDataEditForm/"+member_id+"/"+list_id, function(data2){
				$('#detail_data_html').html(data);
				$('#detail_data_html_edit_form').html(data2);
				$("#showResult").hide();		
				$( ".datepicker" ).datepicker($.datepicker.regional['hu']);		
			});
		});				
	});
	
	
	$('#common_letter_id').change(function(){
		
		$('#showCommonLink').attr('href', '/api/common/showCommonLetter/'+$(this).val());
		//
	});
	
});

</script>

<div id="dialog_dellist" title="Törlés a listáról" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Valóban törölni szeretné a feliratkozót a listáról?</strong><br />
	
    </div>
	<form name="" method="post" id="dellist_form" action="/pages/memberdetail/delFromList/<?=$member->id ?>">
	    
	    <div class="delete_dialog_short_buttons" style="width:250px;">
	        <div class="mybutton" id="" style="float:left">
	 			<a href="JavaScript:;" class="guibutton save guisubmit" rel="dellist_form" id="">Igen, törlöm</a>
	        </div>	
	        <div class="mybutton" id="" style="float:right">
				<a href="Javascript:;" class="guibutton cancel cancelDelList" id="">Mégse</a>
	        </div>	
	    </div>
    	<input type="hidden" name="list_id" value="" id="del_list_id" />
	</form>
</div>	

    <div id="detail_dialog" title="Feliratkozó adatai" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#4d5e69">
		        <div class="mybutton" style="float:right;">    
		        	<a href="javascript:;" class="guibutton modify" id="editSubscriberDetailButton">Módosít</a>
		
		        </div> 
				<strong id="detail_list_name"></strong>
				<span>Listához tartozó adatok</span>

			</div>
			<div class="subscriberSnapshot" id="detail_data_html">
				
			</div>	
			
			<div class="" id="detail_data_html_edit" style="display:none">
	            
	            <div class="mybutton" style="margin:10px">
	                <a href="JavaScript:;" class="guibutton save submita" rel="subscribers" id="saveDetailChangesBtn">Adatok mentése</a>
	                <?php if($mo != ""): ?>
	                <a href="Javascript:;" class="guibutton cancel cancelDetailSaveChanges" id="">Mégse</a>
	                <?php endif; ?>
	                
	                <div style="clear:both"></div>
	            </div>				
				<div class="" id="detail_data_html_edit_form">
				</div>
				
			</div>	
			
						
					
			<div id="showResult" style="clear:both"></div>
		</div>
	</div>


    <div id="product_dialog" title="Termék kapcsolat adatai" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#4d5e69">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton save guisubmit" id="saveProductButton" rel="product_connect_form">Módosít</a>
		
		        </div> 
		        <strong id="">Módosítás</strong>
				<span>Termék kapcsolat</span>

			</div>
			<div class="subscriberSnapshot" id="product_detail_data_html">
				
			</div>	
			
					
			<div id="showResult" style="clear:both"></div>
		</div>
	</div>

    <div id="listadd_dialog" title="Lista hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#4d5e69">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="listadd_form">Hozzáad</a>
		
		        </div> 
		        <strong id="">Lista hozzáadása</strong>
				<span>Válasszon listát</span>		        
				

			</div>
			<div class="subscriberSnapshot" id="">
				
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form name="" method="post" id="listadd_form">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Lista</label>
									<select name="listadd_list_id">
										<?php foreach($client->lists() as $list): ?>
										<option value="<?=$list->id ?>"><?=$list->name ?></option>
										<?php endforeach; ?>
									</select>				

								</div>
								
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Feliratkozás dátuma</label>
									<input class="datepicker input_text" type="text" name="listadd_regdate" size="45" value="<?=date('Y-m-d')?>" />
								</div>
								
								<input type="hidden" name="multiselect_fields" value="">	
								
								</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
			
					
			<div id="showResult" style="clear:both"></div>
		</div>
	</div>

    <div id="sendcommon_dialog" title="Azonnali levél küldése" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#4d5e69">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton send guisubmit" id="saveProductButton" rel="sendcommon_form">Küldés</a>
		
		        </div> 
		        <strong id="">Azonnali levél küldése</strong>
				<span>Válasszon levélsablont</span>		        
				

			</div>
			<div class="subscriberSnapshot" id="">
				
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form name="" method="post" id="sendcommon_form" action="/pages/memberdetail/sendCommonLetter/<?=$member->id ?>">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Levél</label>
									<select name="common_letter_id" id="common_letter_id">
										<?php 
										$letters = $client->getCommonLetters();
										foreach($letters as $letter): ?>
										<option value="<?=$letter->id ?>"><?=$letter->name ?></option>
										<?php endforeach; ?>
									</select>				
									
								</div>
								<?php 
									if(isset($letters[0])){
										$ld = $letters[0]->id;
									}else{
										$ld = 0;
									}
									
									
									
						 
								
								?>
								<p class="bread">
									<a href="/api/common/showCommonLetter/<?=$ld ?>" target="_blank" id="showCommonLink" style="color:blue">Kiválasztott levél előnézete</a>
								</p>
								<input type="hidden" name="multiselect_fields" value="">	
								
								</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
			
					
			<div id="showResult" style="clear:both"></div>
		</div>
	</div>


<div class="twocol" style="background:none">



<!-- CONTENT -->
	<div id="content" style="background:none;min-height:2000px">


		<div id="leftcol" style="margin-right:400px">

		<?php

		     
		//
		?>

        <p class="bread"><a href="<?=url::base() ?>pages/subscribers">Feliratkozók</a><span class="breadArrow">&nbsp;</span><?=$name ?></p>
		

		<?=$errors?>
		
		<div class="subscriberSnapshotHeadEdit">

        <div class="mybutton" style="float:right;padding-top:5px;">    
        <?php if($mo != ""): ?>
        	<a href="javascript:;" class="guibutton modify" id="editSubscriberButton">Adatok módosítása</a>
        	<a href="<?=url::base() ?>pages/subscribers" class="guibutton back" id="">Vissza</a>
		<?php endif; ?>
        </div> 
        	<?=$name ?> 
        	<span>
        		<?php echo (isset($mo->email)) ? $mo->email : ""; ?>
        		
        		
        	</span>
			<div class="clear"></div>
		</div>		
		
		<?php if(isset($_POST['status']) || $mo == "") $display = "true"; else $display = "none;" ?>	
		
		<div id="editSubscriber" style="display:<?=$display?>"> <!--ez a div akkor jelenik meg ha módosítás van-->
		
		<div class="formBG">
		<div class="formWrapper">
		<form name="subscribers" action="<?=url::base() ?>pages/memberdetail/index/<?=$member->id?>" method="post" id="subscribers">
        
            <div class="mybutton" style="margin-top:10px">
                <a href="JavaScript:;" class="guibutton save submita" rel="subscribers" id="saveChangesBtn">Adatok mentése</a>
                <?php if($mo != ""): ?>
                <a href="Javascript:;" class="guibutton cancel cancelSaveChanges" id="">Mégse</a>
                <?php endif; ?>
                
                <div style="clear:both"></div>
            </div>

			
			<h3>E-mail cím, státusz</h3>
			<p></p>
			<div class="formContainer">

				<div class="clearfix">
					<label>Címkék:</label><input class="tags input_text" type="text" name="tags" value="">
				</div>
			
			</div>
			
	
			
			<div class="formContainer">
				<div class="clearfix">
				<label>E-mail cím</label><input type="text" name="email" id="emailAddress" size="35" value="<?=$form['email'] ?>" class="<?=$classes['email'] ?> input_text"/>
				</div>
				<div class="clearfix">
				<label>Státusz</label>
                <select name="status">

	                	<?php foreach ($statuses as $key => $ft): ?>
		                	
		                	<?php if($form['status'] == $ft->code): ?>		                	
		                		<option value="<?=$ft->code ?>" selected="selected"><?=$ft->value ?></option>
		                	<?php else: ?>
		                		<option value="<?=$ft->code ?>" ><?=$ft->value ?></option>
		                	<?php endif; ?>
		                	
	                	<?php endforeach; ?>

                </select>
				</div>
			</div>
			
			<h3 class="topPad">Feliratkozó adatai</h3>
			<p></p>
				
			
				
			<div class="formContainer">
				<?php 
					$multiselect_fields = "";	
					foreach($fields as $f): ?>
					<?php if($f->type == "singleselectradio"): ?>

						<div class="clearfix">
							<label><?=$f->name ?></label>
							<div class="radioInset">
		                        
		                        <?php foreach($f->getValues() as $v): ?>
		                        
		            				<?php if($v->code == $form[$f->reference]): ?>	            	
		                        		<input type="radio" name="<?=$f->reference ?>" value="<?=$v->code ?>" checked="checked"/>
		                        	<?php else: ?>
		                        		<input type="radio" name="<?=$f->reference ?>" value="<?=$v->code ?>"/>
		                        	<?php endif; ?>
		                        		<label><?=$v->value ?></label>
		                        		<img src="img/_space.gif" width="1" height="5" /><br/>
		                        		
		                        <?php endforeach; ?>
		                        
							</div>	
						</div>
					
					<?php elseif($f->type == "singleselectdropdown"): ?>

						<div class="clearfix">
							<label><?=$f->name ?></label>
							<select name="<?=$f->reference ?>">
		                        
		                        <?php foreach($f->getValues() as $v): ?>

		            				<?php if(isset($form[$f->reference."_code"]) && $v->code == $form[$f->reference."_code"]): ?>	      	            	

		                        		<option value="<?=$v->code ?>" selected="selected"><?=$v->value ?></option>

		                        	<?php else: ?>

		                        		<option value="<?=$v->code ?>"><?=$v->value ?></option>

		                        	<?php endif; ?>


		                        <?php endforeach; ?>
		                        
							</select>	
						</div>
					
					<?php elseif($f->type == "multiselect"): 
							
							$multiselect_fields .= $f->reference.",";
					
					?>

						<div class="clearfix">
							<label><?=$f->name ?></label>
							<div class="radioInset">
		                        
		                        <?php foreach($f->getValues() as $v): ?>
		                        
		            				<?php if(  strpos  ( $form[$f->reference] , $v->code ) !== false ): ?>	            	
		                        		<input type="checkbox" name="<?=$f->reference ?>[]" value="<?=$v->code ?>" checked="checked"/>
		                        	<?php else: ?>
		                        		<input type="checkbox" name="<?=$f->reference ?>[]" value="<?=$v->code ?>"/>
		                        	<?php endif; ?>
		                        		<label><?=$v->value ?></label>
		                        		<img src="img/_space.gif" width="1" height="5" /><br/>
		                        		
		                        <?php endforeach; ?>
		                        
							</div>	
						</div>
					
					<?php elseif($f->type == "multitext"): ?>
					
						<div class="clearfix">
							<label><?=$f->name ?></label>
							<div class="radioInset">
								<textarea name="<?=$f->reference ?>" cols="100" rows="10"><?=$form[$f->reference] ?></textarea>		                        
							</div>	
						</div>
					
					<?php elseif($f->type == "date"): ?>					
					
						<div class="clearfix">
							<label><?=$f->name ?></label><input class="datepicker input_text" type="text" name="<?=$f->reference ?>" size="45" value="<?=$form[$f->reference] ?>">
						</div>	
					
					<?php else: ?>
				
						<div class="clearfix">
							<label><?=$f->name ?></label><input type="text" name="<?=$f->reference ?>" size="45" value="<?=$form[$f->reference] ?>" class="input_text">
						</div>				
				
					<?php endif; ?>
					

				
				<?php endforeach; ?>
				
				<input type="hidden" name="multiselect_fields" value="<?=$multiselect_fields?>"/>	

			</div>
			
            <div class="mybutton">
                <a href="JavaScript:;" class="guibutton save submita" rel="subscribers" id="saveChangesBtn">Adatok mentése</a>
                <?php if($mo != ""): ?>
                <a href="Javascript:;" class="guibutton cancel cancelSaveChanges" id="">Mégse</a>
                <?php endif; ?>
                <div style="clear:both"></div>
            </div>
			<div class="clearButton"></div>
			</form>
		</div>
		</div> 
		
		<div class="topPad bottomPad"></div>	
		
		</div>	<!--editSubscriber end-->	
		
		<?php if($mo != ""): ///////////CSAK AKKOR HA EDITÁLJUK A EMBERKÉT!!?>      
		
        <div id="viewSubscriber" style="display:true;">

		<div class="subscriberSnapshot">
			<table cellpadding="0" cellspacing="0" width="100%" class="snapshotStats">
				<tr>
					<th nowrap class="camelCase">Címkék:</th>
					<td width="100%">
						<ul id="as-selections-tags" class="as-selections" style="border:0;background:0;">
							<?php foreach($member->getTags() as $tag): ?>
								<li  class="as-selection-item" style="border-color:#<?=$tag->color1?>;margin-left:0;background-color:#<?=$tag->color1?>;color:#<?=$tag->color2?>"><?=$tag->name?></li>
							<?php endforeach; ?>
							
						</ul>
					</td>
				
				
				</tr>
			<?php foreach($fields as $f): $ref = $f->reference;?>
				
				
				<?php if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"): ?>
				<tr>
					<th nowrap class="camelCase"><?=$f->name ?></th>
					<td width="100%">
		                        <?php foreach($f->getValues() as $v): ?>

		            				<?php 
			            				$rrr = $ref."_code";
		            					if($v->code == $mo->$rrr): ?>	            	

		                        		<?=$v->value ?>

		                        	<?php endif; ?>

		                        <?php endforeach; ?>					
					</td>
				</tr>			
				
				
				<?php elseif($f->type == "multiselect"): ?>
				
				
				<tr>
					<th nowrap class="camelCase"><?=$f->name ?></th>
					<td width="100%">
		                        <?php foreach($f->getValues() as $v): ?>
		            				<?php if( strpos  ( $mo->$ref , $v->code ) !== false ): ?>	            	
		                        		<?=$v->value ?><br/>
		                        	<?php endif; ?>
		                        <?php endforeach; ?>					
					</td>
				</tr>	
				
				<?php elseif($f->type == "multitext"): ?>
				
				
				<tr>
					<th nowrap class="camelCase"><?=$f->name ?></th>
					<td width="100%"><?=nl2br($mo->$ref) ?></td>
				</tr>
				
				<?php else: ?>
				<tr>
					<th nowrap class="camelCase"><?=$f->name ?></th>
					<td width="100%"><?php echo (isset($mo->$ref)) ? $mo->$ref : ""; ?></td>
				</tr>
				
				
				<?php endif; ?>
				
				
				
				
			<?php endforeach; ?>
			
			<tr>
				<th nowrap>Státusz</th>
				<td width="100%">
					
				
	                	<?php 
	                		$statcolor['active'] = "#346720";
	                		$statcolor['unsubscribed'] = "#C31C03";
	                		$statcolor['deleted'] = "#C31C03";
	                		$statcolor['prereg'] = "#C31C03";
	                		$statcolor['error'] = "#C31C03";
	                	
	                		foreach ($statuses as $key => $ft): ?>
		                	
		                	<?php if($mo->status == $ft->code): ?>		                	
		                		
		                		<span style="color:<?=$statcolor[$ft->code] ?>"><?=$ft->value ?></span>
		                		
		                	<?php endif; ?>
		                	
	                	<?php endforeach; ?>				
					
				</td> <!--color:# ha leiratkoztt, vagy törölt-->

			</tr>
			<tr>
				<th nowrap class="noLine">Feliratkozás ideje</th>
				<td width="100%" class="noLine"><?=dateutils::formatDateWithTime($mo->reg_date) ?>

				</td>
			</tr>
			<?php if($mo->status == "unsubscribed"):?>
			<tr>
				<th nowrap class="noLine">Leiratkozás ideje</th>
				<td width="100%" class="noLine"><?=dateutils::formatDateWithTime($member->unsubscribe_date) ?>
				</td>
			</tr>
			<?php endif;?>
			</table>			
		</div>
		</div> <!--viewSubscriber end-->
		
		
		<?php if($mo != ""): ///////////CSAK AKKOR HA EDITÁLJUK A EMBERKÉT!!?> 
		<div>
			<h1 class="subscriberSnapshotTitle" style="width:50%;float:left">
				<?=_('Feliratkozó a következő listákon szerepel')?> 
			</h1>
		
            <div class="mybutton" style="text-align:right;width:40%;float:right">
                <a href="JavaScript:;" class="guibutton add" rel="subscribers" id="listadd_click">Hozzáadás másik listához</a>
                <div style="clear:both"></div>
            </div>
            <div style="clear:both"></div>
		</div>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
		<tr class="noHighlight">
            <th nowrap="nowrap" width="100" class="headerLeft">Feliratkozás ideje<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
			<th class="cellLeft" width="40%">Lista neve</th>
			<th class="headerRight cellLeft" width="40%">Műveletek</th>			
		</tr>
		
		<?php foreach($member->getLists() as $list):?>
		
			<tr id="" class="listRow" rel="">
					
			    <td class="cellLeft">
	               	<?=dateutils::formatDateWithTime($list->reg_date) ?>
			    </td>
					
					
			    <td class="rowLeft">
			    	<a href="<?=url::base() ?>pages/listoverview/selectlist/<?=$list->list_id?>"><?=$list->list_name?></a>
					<?php echo (empty($list->list_note)) ? "" : '<a href="Javascript:;" title="'.nl2br($list->list_note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?>			    	
			    </td>
			    
			    <td class="rowLeft">
			    	<a href="Javascript:;" rel="<?=$list->list_id?>" rel2="<?=$list->list_name?>" class="listdetaillink">Részletek</a> | <a href="Javascript:;" rel="<?=$list->list_id?>" class="listdellink">Törlés a listáról</a>
								    	
			    </td>			    
			    
			</tr>
		
		<?php endforeach;?>
				
		<tr class="noHighlight">
			<td class="footerLeft simple" colspan="2">&nbsp;</td>
			<td class="footerRight simple" align="center" style="padding: 4px 6px;">&nbsp;</td>
		</tr>
		</table>		
		
		<?php /* 
		<h1 class="subscriberSnapshotTitle">
			<?=_('Feliratkozó kiküldési statisztikája')?>
		</h1>
		
		<div class="dataHighlight">
			<table cellpadding="0" cellspacing="0" width="100%" class="goalSummary">
			<tr>
				<td nowrap><div style="background-color:#D3D4C0;color:black"><?=$stat['allsent']  ?></div></td>
				<td width="50%" class="goalDescription"><h2>Levél elküldve</h2><strong><?=dateutils::formatDateWithTime($mo->reg_date) ?>-óta </strong></td>

				<td nowrap><div class="secondary" style="background-color:#FAFA87"><?=round($stat['openedratio'],2) ?></div></td>
				<td width="50%" class="goalDescription"><h2>% Megnyitott levél</h2>a(z) <?=$stat['allhtml'] ?> html levélből <?=$stat['opened']?> levelet nyitott meg</td>
			</tr>
			<tr class="lastGoal">
				<td nowrap><div class="secondary" style="background-color:#9DDB84"><?=round($stat['clickedratio'],2) ?></div></td>
				<td width="50%" class="goalDescription"><h2>% Átkattintások száma</h2>A(z) <?=$stat['alllinks'] ?> linket tartalmazó levélből. <br/>Egy levélben csak egyszer számolja</td>
				<td nowrap><!--<div class="secondary">0</div>--></td>
				<td width="50%" class="goalDescription"><!--<h2>Forwards</h2>To their friends and colleagues--></td>
			</tr>
			</table>
			
		</div>
		*/ ?>
		<div>
			<h1 class="subscriberSnapshotTitle" style="width:50%;float:left">
				<?=_('Küldött levelek')?><br>(<span style="background-color:#FAFA87"><?=_('Megnyitott')?></span> | <span style="background-color:#9DDB84"><?=_('Átkattintott')?></span> | <span style="background-color:#FFADAD"><?=_('Visszapattant')?></span>)   
			</h1>
		
            <div class="mybutton" style="text-align:right;width:40%;float:right">
                <a href="JavaScript:;" class="guibutton mail" rel="" id="sentcommon_click">Új azonnali levél küldése</a>
                <div style="clear:both"></div>
            </div>
            <div style="clear:both"></div>
		</div>		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
		<tr class="noHighlight">
            <th nowrap="nowrap" width="120" class="headerLeft">Kiküldése ideje<br /><img src="<?=$base.$img ?>_space.gif" width="120" height="1" alt=""></th>
			<th class="cellLeft" width="">Kampány</th>
			<th class="cellLeft" width="">Levél</th>
			<th class="headerRight" width="120">Levél típusa<br/><img src="<?=$base.$img ?>_space.gif" width="120" height="1" alt=""></th>
		</tr>
		
		<?php 
			$letter_types = meta::getCodeDictArray(9);
			foreach($allletters as $letter):
				
			if($letter->job_type == 'dayjob'){
			
				if($letter->open_count > 0){
					$bg = '#FAFA87';
				}
				if($letter->click_count > 0){
					$bg = '#9DDB84';
				}				

				if($letter->bounce_count > 0){ //a
					$bg = '#FFADAD';
				}						
			}else{
				if($letter->common_open_count > 0){
					$bg = '#FAFA87';
				}
				if($letter->common_click_count > 0){
					$bg = '#9DDB84';
				}				

				if($letter->common_bounce_count > 0){ //a
					$bg = '#FFADAD';
				}	
				
			
			}
				
			?>
		
			<tr id="" class="listRow" rel="" <?php echo (isset($bg)) ? 'style="background-color:'.$bg.'"' : ''; ?>>
					
			    <td class="cellLeft" style="overflow:hidden;white-space:nowrap;;text-overflow: ellipsis;">
	               	<?=dateutils::formatDateWithTime($letter->job_sent_time) ?>
			    </td>

			    <td class="cellLeft" style="overflow:hidden;white-space:nowrap;text-overflow: ellipsis;">
			    	<?php
			    		if($letter->job_type == 'dayjob'){
			    			echo $letter->campaign_name;		
			    		}else{
			    			echo '<i>Nem tartozik kampányhoz</i>';
			    		}
			    	
			    	?>
	               	
			    </td>					
			     
			    <td class="cellLeft" style="overflow:hidden;white-space:nowrap;;text-overflow: ellipsis;"> 
			    	<?php if($letter->job_type == 'dayjob'):?>
			    			<?=$letter->letter_name?> (<a href="/api/common/showLetter/<?=$letter->letter_id ?>" target="_blank">megnéz</a>)		
			    		<?php else: ?>
			    			<?=$letter->cletter_name?> (<a href="/api/common/showCommonLetter/<?=$letter->cletter_id ?>" target="_blank">megnéz</a>)
			    		<?php endif; ?>
			    	
			           	
			    	
	               	
			    </td>					
			    <td class="cellLeft" style="overflow:hidden;white-space:nowrap;;text-overflow: ellipsis;">
			    	<?php
			    		if($letter->job_type == 'dayjob'){
				    		if($letter->letter_timing_type == 'absolute'){
								
								$d = dateutils::formatDate($letter->letter_timing_date);
								if($letter->letter_timing_hour != NULL){
									$d .= " ".$letter->letter_timing_hour." óra";
								}
															
							}else{
								$d = $letter->letter_timing_event_value.". nap";
							}
							if(isset($letter_types[$letter->letter_timing_type])){
								echo $letter_types[$letter->letter_timing_type].' ('.$d.')';	
							}else{
								echo 'API-val kiküldött azonnali';/////
							}
							
										    			
			    		}else{
			    			echo 'Azonnali';
			    		}
					?>
			    </td>						
							    
			    
			</tr>		
		
		<?php 
			unset($bg);
			endforeach; 
		?>
		
		<tr class="noHighlight">
			<td class="footerLeft simple" colspan="3">&nbsp;</td>
			<td class="footerRight simple" align="center" style="padding: 4px 6px;">&nbsp;</td>
		</tr>
		</table>	
       
		<div id="campaignActions">
        
        </div>

       <?php endif; ?>
       
     	<?php endif; ?>
	<div class="clear"></div>
	
	</div>
	<?php if($mo != ""): ///////////CSAK AKKOR HA EDITÁLJUK A EMBERKÉT!!?>  
	<div id="rightcol" style="width:390px;margin-top:39px">
	
		<div class="subscriberSnapshotHeadEdit" style="background-color:#040775">
            	<?=_('Eladási adatok')?>
        	<span>
				<?=_('A következő termékeket vásárolta meg')?>
        	</span>
			<div class="clear"></div>
		</div>	
		<div class="subscriberSnapshot">

		<form name="add_product_form" id="add_product_form" action="" method="post" style="">
			<select name="product_id" style="width:136px;margin:2px">
				<?php foreach($client->products() as $prod): ?>
				<option value="<?=$prod->id?>"><?=$prod->name?></option>
				<?php endforeach;?>
			</select> 
			<select name="product_status" style="width:94px;margin:2px">
				<option value="ordered">Megrendelte</option>
				<option value="purchased">Megvásárolta</option>
				<option value="failed">Hibás</option>
			</select>
			 <a href="Javascript:;" class="guibutton add guisubmit" id="" rel="add_product_form">Hozzáad</a>
		</form>		

		<?php
			/*
			 * order: color:#efe702
			 * purchase: color:#afdda0
			 * failed: color:#cea09c
			 */
		?>
			<table cellpadding="0" cellspacing="0" width="100%" class="snapshotStats">				
				<?php foreach($member->getProducts() as $product): 
					
					if($product->status == 'ordered'){////
						$pstat = '<strong style="color:#efe702">Megrendelte</strong>';
					}elseif($product->status == 'purchased'){
						$pstat = '<strong style="color:#afdda0">Megvásárolta</strong>';
					}elseif($product->status == 'failed'){
						$pstat = '<strong style="color:#cea09c">Hibás</strong>';
					}//
					
				
				?>
				
				<tr>
					<th nowrap class="camelCase">
						<a href="Javascript:;" class="productdetaillink" rel="<?=$product->connect_id ?>" title="<?=$product->product_name ?>">
							<?=string::trimName($product->product_name,20) ?>
						</a> (<a href="/pages/productoverview/selectproduct/<?=$product->product_id?>">#</a>)
						
						
						</th>
					<td width="80"><?=$pstat?></td>
					
					<?php if($product->status == 'ordered'): ?>
					<td width="80"><?=dateutils::formatDate($product->order_date) ?></td>
					<?php elseif($product->status == 'purchased'): ?>
					<td width="80"><?=dateutils::formatDate($product->purchase_date) ?></td>
					<?php elseif($product->status == 'failed'): ?>
					<td width="80"><?=dateutils::formatDate($product->failed_date) ?></td>
					<?php endif; ?>
				</tr>				
				
				<?php endforeach; ?>
			</table>		
		
		
		
		
		
		</div>		
		


		<div class="subscriberSnapshotHeadEdit" style="">
            <?=_('Feliratkozó kiküldési statisztikája')?>
        	<span></span>
			<div class="clear"></div>
		</div>			
		
		<div class="dataHighlight" style="margin-top:0">
			<style>
			table.goalSummary td {
				padding: 11px 0;
				border-bottom: 1px solid #EBEBC7;
				vertical-align: middle;
			}
			</style>
			<table cellpadding="0" cellspacing="0" width="100%" class="goalSummary">
			<tr>
				<td nowrap><div style="background-color:#D3D4C0;color:black"><?php echo $stat['allsent']; ?></div></td>
				<td width="50%" class="goalDescription"><h2><a href="Javascript:;" title="<?=dateutils::formatDateWithTime($mo->reg_date) ?>-óta ">Levél elküldve</a></h2></td>

				<td nowrap><div class="secondary" style="background-color:#FAFA87"><?=round($stat['openedratio'],2) ?></div></td>
				<td width="50%" class="goalDescription"><h2><a href="Javascript:;" title="A(z) <?=$stat['allhtml'] ?> html levélből <?=$stat['opened']?> levelet nyitott meg">% Megnyitás</a></h2></td>
			</tr>
			<tr class="lastGoal">
				<td nowrap><div class="secondary" style="background-color:#9DDB84"><?=round($stat['clickedratio'],2) ?></div></td>
				<td width="50%" class="goalDescription"><h2><a href="Javascript:;" title="A(z) <?=$stat['alllinks'] ?> linket tartalmazó levélből. <br/>Egy levélben csak egyszer számolja">% Átkattintás</a></h2></td>
				<td nowrap><!--<div class="secondary">0</div>--></td>
				<td width="50%" class="goalDescription"><!--<h2>Forwards</h2>To their friends and colleagues--></td>
			</tr>
			</table>
			
		</div>


		 <?php if($_SESSION['km_user'] == 'teszt1'): //?>
		 
			<div class="subscriberSnapshotHeadEdit" style="background-color:#FAA719">
	            	Jegyzetek 
	        	<span></span>
				<div class="clear"></div>
			</div>	
			<div class="subscriberSnapshot" style="background-color: #FDFDE3">
			asdasda
			</div>		 
		 
		 <?php endif; ?>
		<?php /* 
		<div class="subscriberSnapshotHeadEdit" style="background-color:#FAA719">
            	Jegyzetek 
        	<span></span>
			<div class="clear"></div>
		</div>	
		<div class="subscriberSnapshot" style="background-color: #FDFDE3">
		asdasda
		</div>			
		*/ ?>
	<div style="clear:both"></div>	
	</div>
	<?php endif; ?>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
