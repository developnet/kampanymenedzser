
<script>
	$(function(){ 
  		$(".groupCondition").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				$("#deleteCondition_"+id).show();
				
			} );

			$(this).mouseout( function() {
				$("#deleteCondition_"+id).hide();
			} );
 
 
		
		});

		$("#cancelSaveChanges").click(function(){
			$('#renameSegment').toggle();
		});

		$("#renameLink").click(function(){
			$('#renameSegment').toggle();
		});
		
		$(".conditionType").change( function() { 
			
			var id = $(this).attr("rel");
			var value = $(this).val();
			 
			 if(value == "ispresent" || value == "notpresent" ){
			 	
			 	$('#conditionValue_'+id).hide();
			 	$('#second_conditionValue_'+id).hide();
			 	
			 	$('#conditionValue_'+id).val(value);
			 	
			 	
			 }else if(value == "checkedatleast" ||  value == "checkedless" || value == "checkedequal"){
			 	
			 	$('#conditionValue_'+id).hide();
			 	$('#second_conditionValue_'+id).show();
			 	$('#second_conditionValue_'+id).val("");			 
			 
			 }else{
			 	
			 	$('#second_conditionValue_'+id).hide();
			 	$('#conditionValue_'+id).show();
			 	$('#conditionValue_'+id).val("");
			 				 
			 }
			 
			
			 	
			
			});
		
 		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
	});
	

	
</script>

<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		<form name="rulesForm" action="<?=url::base() ?>pages/groupadd/second/<?=$_SESSION['selected_group']->id?>" method="post">
		
		 
		<p class="bread"><a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a><span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/groupoverview" id="segmentsBreadcrumb">Csoportok</a>
			<span class="breadArrow">&nbsp;</span><?=$_SESSION['selected_group']->name ?>
		</p>
		
		<h1 class="bottomPad">Csoport: <?=$_SESSION['selected_group']->name ?></h1>

		<?=$errors ?>

		<div id="renameSegment" style="display:none">
			<div class="formBG">
			<div class="formWrapper">	
				<h3>Csoport átnevezése</h3>
				<div class="formContainer">		
					<div class="clearfix">
					<label class="middle">Csoport neve</label>
						<input type="text" id="name" name="name" value="<?=$form['name'] ?>" style="width:360px" size="50" class="<?=$classes['name'] ?> input_text">
					</div>
				</div>

                <div class="mybutton">
                    <button type="submit" class="button" id="saveChangesBtn">
                        <img src="<?=$base.$img?>buttons/icon-tickcase.gif" width="16" height="16" alt=""/> 
                        Új név mentése
                    </button>
                    <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="JavaScript:;" id="cancelSaveChanges">mégsem</a></span>
                    <div style="clear:both"></div>
                </div>
				<div class="clearButton"></div>
			</div>
			</div>
			<div class="extraTopPad"></div>
		</div> <!--renameSegment end-->

		<?php $lc = 1;
			foreach($condlevels as $lev): 
				
				$level = $lev->level;
	    		$emailConds = $group->getFixedConditions("email",$level);
	    		$regdConds = $group->getFixedConditions("reg_date",$level);
	    		$fieldcondCount = $group->getFieldConditionNumberOnLevel($level);
		?>
				



		<?php if(sizeof($emailConds)>0): ?>
			
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
			
						<tr class="noHighlight">
							<th width="100%" class="headerLeft" colspan="3">E-mail cím</th>
							<th nowrap="nowrap" class="headerRight" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
						</tr>			
			
			<?php $i=1; foreach($emailConds as $c): ?>
			
							<!-- FELTÉTEL -->	
							<tr rel="<?=$c->id ?>" class="groupCondition"><!-- ide kell onmousedelete -->
							    
							    <td nowrap="nowrap" valign="top">
						            <select name="conditionType_<?=$c->id ?>" class="conditionType" rel="<?=$c->id ?>">
						            
						            	<?php foreach($groupConditionTypes as $t): ?>
						            		
						            		<?php if( !(($t->code == "biggerthan" || $t->code == "smallerthan" || $t->code == "checkedatleast" || $t->code == "checkedless"  || $t->code == "checkedequal"  ) ) ): ?>
						            		
							            		<?php if($form['conditionType_'.$c->id] == $t->code): ?>
							            			<option value="<?=$t->code ?>" selected="selected"><?=$t->value ?></option>
							            		<?php else: ?>
							            			<option value="<?=$t->code ?>" ><?=$t->value ?></option>
							            		<?php endif; ?>
						            		
						            		<?php endif; ?>
						            		
						            	<?php endforeach; ?>
						            	
						            </select>
					            </td>
					            
					            
					            <td nowrap="nowrap" class="rowleft" valign="top">
							    	<div>
							    		<?php
											if($c->type =="ispresent" || $c->type == "notpresent") $display = "none";
											else $display = "true";	
							    		?>			
									
									<input style="display:<?=$display ?>" id="conditionValue_<?=$c->id ?>" type="text" name="conditionValue_<?=$c->id ?>" value="<?=$form['conditionValue_'.$c->id] ?>" class="<?=$classes['conditionValue_'.$c->id] ?> input_text">
								
								<td width="100%">
									<span class="mid">
										<?php if($i != sizeof($emailConds)): ?>
											<span class='conditionDivider'>VAGY</span>
										<?php endif; ?>
									</span>
								</td>
								
								
								
								
								<td nowrap class="rowRight">
									
									<input type="hidden" name="conditionid_<?=$c->id ?>" value="<?=$c->id ?>">
									
									<span id="deleteCondition_<?=$c->id ?>" style="display:none">
										<a href="<?=url::base() ?>pages/groupadd/delcondition?condId=<?=$c->id ?>" title="Feltétel törlése">
											<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Törlés">
										</a>
									</span>
								</td>
								
							</tr>
				
			
			<?php $i++;endforeach; ?>
			
					<tr class="noHighlight segmentConditionAdd">
						<td width="100%" colspan="4" class="normal">
							<span>
								<a href="<?=url::base() ?>pages/groupadd/addcondition?conditionfield=email&level=<?=$level ?>">
									<img src="<?=$base.$img?>icons/plus.gif" width="10" height="10" alt="" class="bullet">
									Újjab "vagy" feltétel
								</a>
								 a "E-mail" nevü mezőhöz
							</span>
						</td>
					</tr>
				</table>			
			

			
		<?php endif; ?>
		
		<?php if(sizeof($emailConds) >0 && sizeof($regdConds) > 0): ?>
				
        	<div class='segmentDivider'><span>Továbbá feljen meg a következőknek</span></div>				
				
		<?php endif; ?>
		
		<?php if(sizeof($regdConds)>0): ?>
			
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
			
						<tr class="noHighlight">
							<th width="100%" class="headerLeft" colspan="3">Regisztráció dátuma</th>
							<th nowrap="nowrap" class="headerRight" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
						</tr>			
			
			<?php $i=1; foreach($regdConds as $c): ?>
			
							<!-- FELTÉTEL -->	
							<tr rel="<?=$c->id ?>" class="groupCondition"><!-- ide kell onmousedelete -->
							    
							    <td nowrap="nowrap" valign="top">
						            <select name="conditionType_<?=$c->id ?>" class="conditionType" rel="<?=$c->id ?>">
						            
						            	<?php foreach($groupConditionTypes as $t): ?>
						            		
						            		<?php if( !(($t->code == "checkedatleast" || $t->code == "checkedless"  || $t->code == "checkedequal"  ) ) ): ?>						            		
						            		
							            		<?php if($form['conditionType_'.$c->id] == $t->code): ?>
							            			<option value="<?=$t->code ?>" selected="selected"><?=$t->value ?></option>
							            		<?php else: ?>
							            			<option value="<?=$t->code ?>" ><?=$t->value ?></option>
							            		<?php endif; ?>
						            		
							            	<?php endif; ?>
						            		
						            	<?php endforeach; ?>
						            	
						            </select>
					            </td>
					            
					            
					            <td nowrap="nowrap" class="rowleft" valign="top">
							    	<div>
							    		<?php
											if($c->type =="ispresent" || $c->type == "notpresent") $display = "none";
											else $display = "true";	
							    		?>			
									
									<input style="display:<?=$display ?>" id="conditionValue_<?=$c->id ?>" type="text" name="conditionValue_<?=$c->id ?>" value="<?=$form['conditionValue_'.$c->id] ?>" class="datepicker <?=$classes['conditionValue_'.$c->id] ?> input_text">
								
								<td width="100%">
									<span class="mid">
										<?php if($i != sizeof($regdConds)): ?>
											<span class='conditionDivider'>VAGY</span>
										<?php endif; ?>
									</span>
								</td>
								
								
								
								
								<td nowrap class="rowRight">
									
									<input type="hidden" name="conditionid_<?=$c->id ?>" value="<?=$c->id ?>">
									
									<span id="deleteCondition_<?=$c->id ?>" style="display:none">
										<a href="<?=url::base() ?>pages/groupadd/delcondition/<?=$_SESSION['selected_group']->id?>/?condId=<?=$c->id ?>" title="Feltétel törlése">
											<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Törlés">
										</a>
									</span>
								</td>
								
							</tr>
				
			
			<?php $i++;endforeach; ?>
			
					<tr class="noHighlight segmentConditionAdd">
						<td width="100%" colspan="4" class="normal">
							<span>
								<a href="<?=url::base() ?>pages/groupadd/addcondition/<?=$_SESSION['selected_group']->id?>/?conditionfield=reg_date&level=<?=$level ?>">
									<img src="<?=$base.$img?>icons/plus.gif" width="10" height="10" alt="" class="bullet">
									Újjab "vagy" feltétel
								</a>
								 a "Regisztráció dátuma" nevü mezőhöz
							</span>
						</td>
					</tr>
				</table>			
			

			
		<?php endif; ?>		
		
		
		
		
		<!-- EGY MEZŐ -->
		<?php $j=0; foreach ($fields as $f): ?>
			<?php 
				$j++;
				$conditions = $f->getConditions($_SESSION['selected_group']->id,$level);
				
				
				
				if(sizeof($conditions)>0):
				$i=1;
			?>
			<?php if(sizeof($regdConds) > 0): ?>
				
        		<div class='segmentDivider'><span>Továbbá feljen meg a következőknek</span></div>				
				
			<?php endif; ?>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
			
						<tr class="noHighlight">
							<th width="100%" class="headerLeft" colspan="3"><?=$f->name ?></th>
							<th nowrap="nowrap" class="headerRight" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
						</tr>
						
						<?php foreach($conditions as $c): ?>
							<!-- FELTÉTEL -->	
							<tr rel="<?=$c->id ?>" class="groupCondition"><!-- ide kell onmousedelete -->
							    
							    <td nowrap="nowrap" valign="top">
						            <select name="conditionType_<?=$c->id ?>" class="conditionType" rel="<?=$c->id ?>">
						            
						            	<?php foreach($groupConditionTypes as $t): ?>
						            		
						            		<?php if( !(($t->code == "biggerthan" || $t->code == "smallerthan") && ($f->type != "date")) && !(($t->code == "checkedatleast" || $t->code == "checkedless"  || $t->code == "checkedequal") && ($f->type != "multiselect"))): ?>
						            		
							            		<?php if($form['conditionType_'.$c->id] == $t->code): ?>
							            			<option value="<?=$t->code ?>" selected="selected"><?=$t->value ?></option>
							            		<?php else: ?>
							            			<option value="<?=$t->code ?>" ><?=$t->value ?></option>
							            		<?php endif; ?>
						            		
						            		<?php endif; ?>
						            		
						            	<?php endforeach; ?>
						            	
						            </select>
					            </td>
							    
							    <td nowrap="nowrap" class="rowleft" valign="top">
							    	<div>
							    		<?php
							    			$second_display = "none";
							    		
											if($c->type =="ispresent" || $c->type == "notpresent") $display = "none";
											else $display = "true";	
											
											if($f->type == "multiselect" && (($form['conditionType_'.$c->id] == "checkedatleast" || $form['conditionType_'.$c->id] == "checkedless"  || $form['conditionType_'.$c->id] == "checkedequal"))){
												$display = "none";
												$second_display = "true";
											}
											
							    		?>
							    		
							    		
							    		<?php if(($f->type == "singleselectradio" || $f->type == "singleselectdropdown" || $f->type == "multiselect")): ?>
											
											<select name="conditionValue_<?=$c->id ?>" id="conditionValue_<?=$c->id ?>" style="display:<?=$display ?>">
						                        
						                        <?php foreach($f->getValues() as $v): ?>
						                        
						            				<?php if($v->code == $form['conditionValue_'.$c->id]): ?>	            	
						                        		<option value="<?=$v->code ?>" selected="selected"><?=$v->value ?></option>
						                        	<?php else: ?>
						                        		<option value="<?=$v->code ?>"><?=$v->value ?></option>
						                        	<?php endif; ?>
						                        		
						                        <?php endforeach; ?>
						                        
											</select>
																		    			
							    		
					                    <?php elseif($f->type == "date"): ?>

											<input style="display:<?=$display ?>" id="conditionValue_<?=$c->id ?>" type="text" name="conditionValue_<?=$c->id ?>" value="<?=$form['conditionValue_'.$c->id] ?>" class="datepicker <?=$classes['conditionValue_'.$c->id] ?> input_text">							    		

										<?php else: ?>							    		
											
											<input style="display:<?=$display ?>" id="conditionValue_<?=$c->id ?>" type="text" name="conditionValue_<?=$c->id ?>" value="<?=$form['conditionValue_'.$c->id] ?>" class="<?=$classes['conditionValue_'.$c->id] ?> input_text">							    		
							    		
							    		<?php endif; ?>
							    		
							    		<?php if($f->type == "multiselect"): ?>
											
											<input style="display:<?=$second_display ?>" id="second_conditionValue_<?=$c->id ?>" type="text" name="second_conditionValue_<?=$c->id ?>" value="<?=$form['second_conditionValue_'.$c->id] ?>" class="<?=$classes['second_conditionValue_'.$c->id] ?> input_text">							    		
							    		
							    		<?php endif; ?>							    		
							    		
							    		
							    	</div>
							    </td>
								
								<td width="100%">
									<span class="mid">
										<?php if($i != sizeof($conditions)): ?>
											<span class='conditionDivider'>VAGY</span>
										<?php endif; ?>
									</span>
								</td>
								
								
								
								
								<td nowrap class="rowRight">
									
									<input type="hidden" name="conditionid_<?=$c->id ?>" value="<?=$c->id ?>">
									
									<span id="deleteCondition_<?=$c->id ?>" style="display:none">
										<a href="<?=url::base() ?>pages/groupadd/delcondition/<?=$_SESSION['selected_group']->id?>/?condId=<?=$c->id ?>" title="Feltétel törlése">
											<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Törlés">
										</a>
									</span>
								</td>
								
							</tr>
							<!-- ////////FELTÉTEL -->
						<?php $i++;endforeach; ?>
						
					
							
					<tr class="noHighlight segmentConditionAdd">
						<td width="100%" colspan="4" class="normal">
							<span>
								<a href="<?=url::base() ?>pages/groupadd/addcondition/<?=$_SESSION['selected_group']->id?>/?conditionfield=<?=$f->id ?>&level=<?=$level ?>">
									<img src="<?=$base.$img?>icons/plus.gif" width="10" height="10" alt="" class="bullet">
									Újjab "vagy" feltétel
								</a>
								 a "<?=$f->name ?>" nevü mezőhöz
							</span>
						</td>
					</tr>
				</table>

		        <?php if($j < $fieldcondCount): ?>
		        	<div class='segmentDivider'><span>Továbbá feljen meg a következőknek</span></div>
		        <?php endif; ?>
				
				<!-- //////EGY MEZŐ -->		
		        <?php endif; ?>

        <?php endforeach; ?>
		
		
		
		<?php if(sizeof($condlevels) > $lc): ?>
				
        	<div class='segmentDivider'><span>Továbbá feljen meg a következőknek</span></div>				
				
		<?php endif; $lc++?>
		
		
		<?php endforeach;/////LEVELS?>

		<div class="TopPad"></div>
		
        <div class="mybutton">
                <button type="submit" class="button" name="btnSaveSegment" style="margin-bottom:20px;">
                    <img src="<?=$base.$img ?>icons/arrow_refresh.png" width="16" height="16" alt=""/> 
                    Mentés és újraszámolás
                </button>
                <div style="clear:both"></div>
        </div>
		</form>		


		<form name="rulesForm" action="<?=url::base() ?>pages/groupadd/addcondition/<?=$_SESSION['selected_group']->id ?>" method="get">
		<div class="formBG">
		<div class="formWrapper">
        <h3>Új feltétel hozzáadása</h3>
            <div class="formContainer">		
                <div class="clearfix">
                    <input type="hidden" name="level" value="<?=sizeof($condlevels)+1 ?>" />
                    <select name="conditionfield" style="float:left;">
                    
							<optgroup label="Globális mezők">
	                            <option value="email">E-mail cím</option>
	                            <option value="reg_date">Regisztráció dátuma</option>
	                        <?php foreach($clientfields as $f): ?>
	    
	                            <option value="<?=$f->id ?>"><?=$f->name ?></option>
	    
	                        <?php endforeach; ?>							 
							 </optgroup>                    
							<optgroup label="Lista mezők">
	                        <?php foreach($listfields as $f): ?>
	    
	                            <option value="<?=$f->id ?>"><?=$f->name ?></option>
	    
	                        <?php endforeach; ?>							 
							 </optgroup>                                   


                    </select>    
                    <div class="mybutton" style="float:left;margin-left:15px;">
                    <button type="submit" class="button slim" name="btnCreateRule">
                        <img src="<?=$base.$img ?>icons/add.gif" width="16" height="16" alt=""/> 
                        Feltétel hozzáadása
                    </button>
                    <div style="clear:both"></div>
                    </div>
                <div style="clear:both"></div>                    
                </div>
            </div>
        </div>
        </div>
		</form>
		
		<!--<form name="rulesForm" action="<?=url::base() ?>pages/groupadd/addcondition" method="get">
			<table cellspacing="0" cellpadding="0" border="0" class="darkFooter" width="100%">
			<tr>
			    <th class="footerLeft" width="90%">
				
				<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>Új feltétel:</td>
					<td>
						
		                <select name="conditionfield">
		                		<option value="email">E-mail cím</option>
		                		<option value="reg_date">Regisztráció dátuma</option>
	        				<?php foreach($fields as $f): ?>
		
								<option value="<?=$f->id ?>"><?=$f->name ?></option>
		
							<?php endforeach; ?>
		                </select>
	                </td>
	
					<td>
                    
                <div class="mybutton">
                <button type="submit" class="button slim" name="btnCreateRule">
                    <img src="<?=$base.$img ?>icons/add.gif" width="16" height="16" alt=""/> 
                    Feltétel hozzáadása
                </button>
                <div style="clear:both"></div>
        		</div>
                </td>
				</tr>
				</table>	
				
				</th>
			    <th class="footerRight" width="10%">&nbsp;</th>
			</tr>
			</table>
		</form>-->
		
		
		

	</div>

	<div id="rightcol">

	
		<div id="options">
			
			<div class="fade-fff664" id="fade">
				<div class="segmentCount" id="fade">				
					<?=$_SESSION['selected_group']->name ?>
					<span><?=$membercount ?></span>
					atkív feliratkozó				
				</div>
			</div>
			
			
			
			<div class="bghighlight"><h3 class="sidebar">Csoport ezsközök</h3></div>

				
			<dl class="icon-menu">		
				<dt><a href="#" id="viewIcon"><img src="<?=$base.$img?>icons/view-subscriber.png" width="16" height="16" alt="View subscribers" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/groupmembers/index/<?=$_SESSION['selected_group']->id ?>" id="viewLink">Feliratkozók</a></dd>
				<dd class="last">Nézze meg a feliratkozókat akik megfelelnek a csoportnak</dd>

			</dl>
	
		</div>

	</div>
	
	<div class="clear"></div>
	
	</div>
</div>