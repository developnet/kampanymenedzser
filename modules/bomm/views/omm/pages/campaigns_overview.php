<?php

if(isset($_SESSION['selected_campaign'])){
	$selected = $_SESSION['selected_campaign']->id;
	$selected_stat = $_SESSION['selected_campaign']->status;
	$name = $_SESSION['selected_campaign']->name;
}else{
	$selected = 0;
	$selected_stat = 'none';
	$name = "";
}

?>

<script>
	$(function(){ 
		var SELECTED = <?=$selected?>;
		var SELECTED_CAMPAIGN = 0;
		var SELECTED_LETTER = 0;
		var SELECTED_LETTERS = new Array();
		
		 $(".deleteCampaign").each(function(){
				$(this).bind('click', function(){
					SELECTED_CAMPAIGN = $(this).attr('rel');
					$('.selectionName').html($(this).attr('relName'));
					deleteCampaign();
								
				});
			});
	
	 	var deleteCampaign = function(){ 
			dialog('dialogCampaign1');				
			//$('#dialog').html($('#tartalom').html());
			$('#dialogCampaign1').dialog('open');
			$('#dialogCampaign1').show();	
	}	


		 $("#delete_letters").each(function(){
				$(this).bind('click', function(){
					//SELECTED_LETTER = $(this).attr('rel');
					//$('.selectionNameLetter').html($(this).attr('relName'));
					
					if(SELECTED_LETTERS.length < 1 ){
	 					alert("Nincs levél kiválasztva!");
	 				}else{
	 					deleteLetter();
	 				}					
					
					
								
				});
			});
	
	 	var deleteLetter = function(){ 
			dialog('dialogLetter1');				
			//$('#dialog').html($('#tartalom').html());
			$('#dialogLetter1').dialog('open');
			$('#dialogLetter1').show();	
		}		
	
		var dialog = function(id){ 
			var dialog = $("#"+id).dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	
	
	$('#deleteNoButtonCampaign').click(function(){
			$('#dialogCampaign1').dialog('close');
			$('#dialogCampaign1').hide();		
	});


	$('#deleteYesButtonCampaign').click(function(){
			$('#dialogCampaign1').dialog('close');
			$('#dialogCampaign1').hide();
			dialog('dialogCampaign2');
			$('#dialogCampaign2').dialog('open');
			$('#dialogCampaign2').show();					
	});	 						
 
 
 	$('#deleteMegsemButtonCampaign').click(function(){
			$('#dialogCampaign2').dialog('close');
			$('#dialogCampaign2').hide();		
	});

 	$('#deleteGoButtonCampaign').click(function(){
			$('#dialogCampaign2').dialog('close');
			$('#dialogCampaign2').hide();
			
			window.location = '<?=url::base() ?>pages/campaignoverview/deleteCampaign/'+SELECTED_CAMPAIGN;
					
	});	
	
	
	$('#deleteNoButtonLetter').click(function(){
			$('#dialogLetter1').dialog('close');
			$('#dialogLetter1').hide();		
	});


	$('#deleteYesButtonLetter').click(function(){
			$('#dialogLetter1').dialog('close');
			$('#dialogLetter1').hide();
			dialog('dialogLetter2');
			$('#dialogLetter2').dialog('open');
			$('#dialogLetter2').show();					
	});	 						
 
 
 	$('#deleteMegsemButtonLetter').click(function(){
			$('#dialogLetter2').dialog('close');
			$('#dialogLetter2').hide();		
	});

 	$('#deleteGoButtonLetter').click(function(){

			POSTARRAY =  {  "operator": "delete", 
					"selected_letters": SELECTED_LETTERS.length
					};
	
			for(var i=0;i<SELECTED_LETTERS.length;i++){
				POSTARRAY["letter_"+i] = SELECTED_LETTERS[i]; 
			}

			$.post("<?=url::base() ?>api/common/deleteLetters", POSTARRAY, 
					  function(data){ 
					  
					  	json = eval(data);

					  	if(json.STATUS == "ERROR"){
							alert('Hiba a törlés közben!');
					  	}else if(json.STATUS == "SUCCES"){
							window.location.reload();
					  	}else{
					  	
					  	}
					  	
					  }); 
					  			

		

			$('#dialogLetter2').dialog('close');
			$('#dialogLetter2').hide();


			
			//window.location = '<?=url::base() ?>pages/campaignoverview/deleteLetter/'+SELECTED_LETTER;
					
	});	
	
		
		
	/////////////////////////////////////////////////////	
 		$(".subFolderNotEmpty").each(function(){
				$(this).bind('click', function(){
					
					$(".letter-container").hide();
					
					var rel = $(this).attr('rel');								
						
					$('#letters-'+rel).show();
					$('#letters-'+rel+'-sent').show();
					
				});
			});	
	
 		$(".subFolder").each(function(){
				$(this).bind('click', function(){
					
					$(".letter-container").hide();
					
					var rel = $(this).attr('rel');								
						
					$('#letters-'+rel).show();
					$('#letters-'+rel+'-sent').show();
					
				});
			});		
	
	
 		////////////////////
 		
		 $(".letter_select").click(function(){
					var letter_id = $(this).attr('rel');

					if((jQuery.inArray(letter_id, SELECTED_LETTERS) == -1) || (jQuery.inArray(letter_id, SELECTED_LETTERS) == 'undefined')){
						SELECTED_LETTERS.push(letter_id);
						$(this).parent().parent().addClass('selected_letter_row');
					}else{
						SELECTED_LETTERS.splice(SELECTED_LETTERS.indexOf(letter_id), 1);
						$(this).parent().parent().removeClass('selected_letter_row');
					}

					try{
						console.log(SELECTED_LETTERS);
					}catch(e){}
								
			});	

		 $('#lettercopyCancelButtonCampaign').click(function(){
				$('#dialogLetterCopy').dialog('close');
				$('#dialogLetterCopy').hide();		
		});
	 		
		$("#copy_letter").click(function(){
			
			if (SELECTED_LETTERS.length < 1 ){
				alert("Nincs levél kijelölve!");		
			}else{
				dialog('dialogLetterCopy');				
				//$('#dialog').html($('#tartalom').html());
				$('#dialogLetterCopy').dialog('open');
				$('#dialogLetterCopy').show();
			}
	

		});
			
		 $('#letterCopyYesButtonCampaign').click(function(){

				operator = "copy";

				if($("#copyletters_move_check").is(':checked')){
					operator = "move";
				}
			 	
				POSTARRAY =  {  "operator": operator, 
								"selected_letters": SELECTED_LETTERS.length,
								"selected_campaign" : $("#selectCampaigns").val() };
			
				for(var i=0;i<SELECTED_LETTERS.length;i++){
					POSTARRAY["letter_"+i] = SELECTED_LETTERS[i]; 
				}	

				
				
				$.post("<?=url::base() ?>api/common/copyletters", POSTARRAY, 
				  function(data){ 
				  
				  	json = eval(data);

				  	if(json.STATUS == "ERROR"){
						alert('Hiba a másolás közben!');
				  	}else if(json.STATUS == "SUCCES"){
						window.location.reload();
				  	}else{
				  	
				  	}
				  	
				  });
	  

				$('#dialogLetterCopy').dialog('close');
				$('#dialogLetterCopy').hide();		
		});

		$('.archive_open_click').click(function(){
			$('.archive_camp').toggle();
			$('#archive_close').toggleClass("open");
		});


		$("#archive_campaign").click(function(){

			dialog('dialogArchive');				
			//$('#dialog').html($('#tartalom').html());
			$('#dialogArchive').dialog('open');
			$('#dialogArchive').show();
						
			
		});

	 	$('#archiveCancelButtonCampaign').click(function(){
			$('#dialogArchive').dialog('close');
			$('#dialogArchive').hide();		
		});

 		$('#archiveYesButtonCampaign').click(function(){
			$('#dialogArchive').dialog('close');
			$('#dialogArchive').hide();
			
			window.location = '<?=url::base() ?>pages/campaignoverview/archiveCampaign/<?=$selected?>';
					
		});	
	

		$("#activate_campaign").click(function(){

			dialog('dialogActivate');				
			//$('#dialog').html($('#tartalom').html());
			$('#dialogActivate').dialog('open');
			$('#dialogActivate').show();
						
			
		});

	 	$('#activateCancelButtonCampaign').click(function(){
			$('#dialogActivate').dialog('close');
			$('#dialogActivate').hide();		
		});

 		$('#activateYesButtonCampaign').click(function(){
			$('#dialogActivate').dialog('close');
			$('#dialogActivate').hide();
			
			window.location = '<?=url::base() ?>pages/campaignoverview/activateCampaign/<?=$selected?>';
					
		});			

		<?php if($selected_stat == 'archived'): ?>

		$('.archive_camp').toggle();
		$('#archive_close').toggleClass("open");

		<?php endif;?>

 		
	});



</script>


<div id="dialogArchive" title="Kampány archiválása" style="display:none">
	
    <div class="archive_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt="" style="float:left"/>
    	<strong>Valóban archiválni szeretné a kiválasztott kampányt?</strong>
    	<br /><br />
    </div>
	
    <div class="archive_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="archiveYesButtonCampaign" type="submit" class="button"  style="margin-left:40px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Igen
            </button>
        </div>
        <div class="mybutton" id="sendingMessage" >
            <button id="archiveCancelButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Mégsem
            </button>
        </div>	
    </div>

</div>	

<div id="dialogActivate" title="Kampány visszaállítása" style="display:none">
	
    <div class="activate_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt="" style="float:left"/>
    	<strong>Valóban vissza szeretné állítani a kiválasztott kampányt?</strong>
    	<br /><br />
    </div>
	
    <div class="activate_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="activateYesButtonCampaign" type="submit" class="button"  style="margin-left:40px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Igen
            </button>
        </div>
        <div class="mybutton" id="sendingMessage" >
            <button id="activateCancelButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Mégsem
            </button>
        </div>	
    </div>

</div>

<div id="dialogLetterCopy" title="Levelek másolása/mozgatása" style="display:none">
	
    <div class="lettercopy_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt="" style="float:left"/>
    	Válassza ki, hogy melyik kampányba szeretné másolni a kijelölt leveleket.<br />
    	<br />
    	Kampányok: <select id="selectCampaigns">
    		
    		<optgroup label="Aktív kampányok">	
			<?php foreach ($campaigns as $c): ?>
    			<option value="<?=$c->id?>"><?=$c->name?></option>
    		<?php endforeach;?>
    		</optgroup>
    		
    		<optgroup label="Archív kampányok">
    		<?php foreach ($archived_campaigns as $c): ?>
    			<option value="<?=$c->id?>"><?=$c->name?></option>
    		<?php endforeach;?>    
    		</optgroup>
    				
    	</select>
    	<br /><br />
    	Levelek mozgatása: <input type="checkbox" id="copyletters_move_check" /> 
    	<br /><br />
    </div>
	
    <div class="lettercopy_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="letterCopyYesButtonCampaign" type="submit" class="button"  style="margin-left:40px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Másolás
            </button>
        </div>
        <div class="mybutton" id="sendingMessage" >
            <button id="lettercopyCancelButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Mégsem
            </button>
        </div>	
    </div>

</div>	



<div id="dialogCampaign1" title="Kampány törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" kampányt?</strong><br />
	A kampán törlésével elveszik a benne lévő összes levél és azok statisztikái!
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButtonCampaign" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogCampaign2" title="Kampány törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" kampányt?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButtonCampaign" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButtonCampaign" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>

<div id="dialogLetter1" title="Levél törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a kijelölt leveleket?</strong><br />
	A levélt törlésével elveszik levél egész tartalma és statisztikája!
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButtonLetter" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButtonLetter" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogLetter2" title="Levél törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a kijelölt leveleket?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButtonLetter" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButtonLetter" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>


<!-- CONTENT -->
    <div id="dashboard">
		<div id="kampanyList">

		<div class="clear"></div>

      <div class="mybutton" style="float:right;margin-bottom:8px;">    
            <a href="<?=url::base() ?>pages/campaignoverview/newcampaign" class="button slim">
                <img src="<?=$base.$img?>icons/add.gif" alt=""/> 
                Új Kampány létrehozása
            </a>
        </div>

			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tr class="noHighlight">
				<th class="headerLeft" width="100%" colspan="2"><span>Kampányok listája</span></th>
				<th nowrap>&nbsp;</th>
                <th class="headerRight" style="padding-right:0px;" nowrap>&nbsp;</th>
			</tr>
			
			<?php foreach ($campaigns as $c): 
				$all = $c->letterNum();
			
			if($c->id == $selected){
				
				$drafts = $c->draftLetterNum();
				
				$relative = $c->relative();
				$absolute = $c->absolute();
				//$sendnow = $c->sendnow();				
			

			
			?>

			<tr class="dashRowActive">
				
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/deselect/<?=$c->id ?>"><img src="<?=$base.$img?>icons/minusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/campaignoverview/deselect/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
                
				<td nowrap width="13" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/selectcampaignforedit/<?=$c->id ?>">
	                	<img src="<?=$base.$img?>icons/folder_edit.png" width="13" height="11" border="0" alt="Szerkesztés">
                	</a>
                </td>
	
				<td nowrap width="10">
                	<a href="javascript:;" class="deleteCampaign" rel="<?=$c->id ?>" relName="<?=$c->name ?>">
        		       <img src="<?=$base.$img?>icons/trash.png" width="10" height="11" border="0" alt="Törlés">
    	            </a>
                </td>
                
			</tr>
			
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($drafts > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="drafts">Szerkesztés alatt lévő levelek (<?=$drafts?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="drafts">Szerkesztés alatt lévő levelek</a>
                	<?php endif; ?>
                	
                </td>
			</tr>            
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($relative > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="relative">Relatív levelek (<?=$relative?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder" rel="relative">Relatív levelek</a>
                	<?php endif; ?>				
				
                </td>
			</tr>                                    
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($absolute > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="absolute">Abszolút levelek (<?=$absolute?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="absolute">Abszolút levelek</a>
                	<?php endif; ?>						
				
                </td>
			</tr>

            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
                	<a href="<?=url::base() ?>pages/letterdetail"><strong>Új levél létrehozása</strong></a>
                </td>
			</tr> 			
			
			<?php }else{ ///////////////csukott?>
			
			<tr class="dashRow"> 
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/selectcampaign/<?=$c->id ?>"><img src="<?=$base.$img?>icons/plusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/campaignoverview/selectcampaign/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
				<td nowrap width="13" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/selectcampaignforedit/<?=$c->id ?>">
                		<img src="<?=$base.$img?>icons/folder_edit.png" width="13" height="11" border="0" alt="Szerkesztés">
                	</a>
                </td>
				<td nowrap width="10">
	                <a href="javascript:;" class="deleteCampaign" rel="<?=$c->id ?>" relName="<?=$c->name ?>" >
	                	<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" border="0" alt="Delete">
	                </a>
                </td>
			</tr>			
			
			<?php } ?>
			
			
			
			
			<?php endforeach; ?>
			

			</table> 
			
  			
			
			<div class="topPad"></div>
	
				
	
			<table cellpadding="0" cellspacing="0" width="100%" >
			
				<tr class="noHighlight">
					<th class="headerDarkGreyLeft" width="" colspan="2"><span class="archive_open_click">Archivált kampányokok listája (<?=sizeof($archived_campaigns)?>)</span> </th>
					<th nowrap class="headerDarkGreyCenter"  class="archive_open_click">&nbsp;</th>
		            <th class="headerDarkGreyRight" style="" nowrap><div  class="archive_open_click" id="archive_close"></div></th> 
				</tr>
				<!-- ARCHIVÁLT -->
	
			<?php foreach ($archived_campaigns as $c): 
				$all = $c->letterNum();
			
			if($c->id == $selected){
				
				$drafts = $c->draftLetterNum();
				
				$relative = $c->relative();
				$absolute = $c->absolute();
				//$sendnow = $c->sendnow();				
			

			
			?>

			<tr class="dashRowActive archive_camp">
				
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/deselect/<?=$c->id ?>"><img src="<?=$base.$img?>icons/minusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/campaignoverview/deselect/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
                
				<td nowrap width="13" style="padding-right:0px;"></td>
				<td nowrap width="10">
                	<a href="javascript:;" class="deleteCampaign" rel="<?=$c->id ?>" relName="<?=$c->name ?>">
                <img src="<?=$base.$img?>icons/trash.png" width="10" height="11" border="0" alt="Törlés">
                </a>
                </td>
			</tr>
			
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($drafts > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="drafts">Szerkesztés alatt lévő levelek (<?=$drafts?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="drafts">Szerkesztés alatt lévő levelek</a>
                	<?php endif; ?>
                	
                </td>
			</tr>            
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($relative > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="relative">Relatív levelek (<?=$relative?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder" rel="relative">Relatív levelek</a>
                	<?php endif; ?>				
				
                </td>
			</tr>                                    
            <tr class="dashRow archive_camp">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
				
					<?php if($absolute > 0): ?>			
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="absolute">Abszolút levelek (<?=$absolute?>)</a>
                	<?php else : ?>
                		<a href="Javascript:;" class="subFolder"  rel="absolute">Abszolút levelek</a>
                	<?php endif; ?>						
				
                </td>
			</tr>

		
			
			<?php }else{ ///////////////csukott?>
			
			<tr class="dashRow archive_camp"> 
				<td width="9" style="padding-right:0px;">
                	<a href="<?=url::base() ?>pages/campaignoverview/selectcampaign/<?=$c->id ?>"><img src="<?=$base.$img?>icons/plusz.gif" border="0" width="9" height="9" alt=""/></a>
                </td>
				<td width="100%">
                	<strong><a href="<?=url::base() ?>pages/campaignoverview/selectcampaign/<?=$c->id ?>" <?php echo (empty($c->note)) ? "" : 'title="'.nl2br($c->note).'"'; ?>><?=$c->name ?> (<?=$all ?>)</a></strong>
                </td>
				<td nowrap width="13" style="padding-right:0px;">          </td>
				<td nowrap width="10">
	                <a href="javascript:;" class="deleteCampaign" rel="<?=$c->id ?>" relName="<?=$c->name ?>" >
	                	<img src="<?=$base.$img?>icons/trash.png" width="10" height="11" border="0" alt="Delete">
	                </a>
                </td>
			</tr>			
			
			<?php } ?>
			
			
			
			
			<?php endforeach; ?>
			
			<tr class="dashRow">
				
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="">&nbsp;</td>
				<td nowrap width="13" style="padding-right:0px;">&nbsp;</td>
				<td nowrap width="10">&nbsp;</td>
			</tr>
			</table> 				
				
				<!-- ARCHIVÁLT -->
	
		
			<div class="clear"></div>
		
		</div> <!-- kampanyList end-->
		
			
        <div id="kampanyActivity">
			<div id="activityBG"> <!--Elválasztó BG-->
				<div id="activityContent">

		<?php if(!isset($_SESSION['selected_campaign'])): ?>
		
		<!--<h1>Válasszon kampányt</h1>-->
        
                <div style="text-align:center;height:400px">
                <?php if(sizeof($campaigns) == 0):?>
                	<h3 style="margin-top:60px;color:#666666">Nincsenek aktív kampányai, <a href="/pages/campaignoverview/newcampaign">itt</a> létrehozhat egyet.</h3>
                <?php else:?>
                	<h3 style="margin-top:60px;color:#666666">Kattintson az egyik kampányra a levelek listázásához.</h3>
                <?php endif;?>
                </div>        

		<?php else: 
			$campaign = $_SESSION['selected_campaign'];
		?>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// ?>
		<?php 
			if(isset($_GET['sort'])){
				$sort = $_GET['sort'];
				$cookie_params = array(
				               'name'   => 'campaign_view_sort',
				               'value'  => $sort,
				               'expire' => '0',
				               'domain' => KOHANA::config("core.site_domain"),
				               'path'   => '/'
				               );
				cookie::set($cookie_params);				
			}else{
				$sort = cookie::get("campaign_view_sort", "name", TRUE);
			}
			
			if(isset($_GET['order'])){
				$order = $_GET['order'];
				$cookie_params = array(
				               'name'   => 'campaign_view_order',
				               'value'  => $order,
				               'expire' => '0',
				               'domain' => KOHANA::config("core.site_domain"),
				               'path'   => '/'
				               );
				cookie::set($cookie_params);
			}else{
				$order = cookie::get("campaign_view_order", "asc", TRUE);
			}		//
		?>		
		
		<div style="height:30px;margin-bottom:6px">
		
			<div id="toolbar_first" style="text-align:right;float:left;">
				 
				 <div class="mybutton" style="float:right;margin-top:2px">  
		            
		            <a href="Javascript:;" class="button slim"  id="copy_letter" style="margin-right:10px">
		                Levelek <strong>másolása</strong>
		            </a>
		            
				 
		            <a href="Javascript:;" class="button slim"  id="delete_letters" style="margin-right:10px">
		                Levelek <strong>törlése</strong>
		            </a>
		            

				<?php if($selected_stat != 'archived'): ?>
					 <a href="Javascript:;" id="archive_campaign"  class="button slim" style="margin-right:10px"><strong>"<?=$name ?>"</strong> Kampány archiválása</a>
				<?php else:?>
					 <a href="Javascript:;" id="activate_campaign"  class="button slim" style="margin-right:10px"><strong>"<?=$name ?>"</strong> Kampány visszaállítása</a>
				<?php endif;?>
					
					<div style="clear:both"></div>
				 </div>	
				<div style="clear:both"></div>

			</div>
		
			<div id="toolbar" style="text-align:right;float:right;width:200px;padding-top:8px">
				
						Rendezés
				<?php if($sort=="name"): //?>
						<a href="<?=url::base() ?>pages/campaignoverview?sort=time&order=<?=$order ?>" > idő </a> / 
						<strong>név</strong>  szerint
				<?php else:?>
						<strong>idő</strong> / 
						<a href="<?=url::base() ?>pages/campaignoverview?sort=name&order=<?=$order ?>" > név </a>
						 szerint			
				<?php endif;?>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
				<?php if($order=="asc"): ///?>
						
						<a href="<?=url::base() ?>pages/campaignoverview?sort=<?=$sort ?>&order=desc" ><img src="<?=$base.$img?>icons/sort-asc.png" alt=""/></a>
				<?php else:?>
						<a href="<?=url::base() ?>pages/campaignoverview?sort=<?=$sort ?>&order=asc" ><img src="<?=$base.$img?>icons/sort-desc.png" alt=""/></a> 
									
				<?php endif;?>
				
			</div>
			
			
			<div style="clear:both"></div>
		</div>
		
		<div id="letters-drafts" class="letter-container">
			
		<!--<h1>Szerkesztés alatt lévő levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Szerkesztés alatt lévő levelek</th>
            <th class="cellCenter" nowrap width="10%">Tartalom</th>
			<th class="cellCenter" nowrap width="10%" class="cellCenter">Teszt</th>
			<th class="cellCenter" nowrap width="10%" class="cellCenter">Időzítés</th>
			<th class="headerRight" nowrap width="10%" class="cellCenter">Címzettek</th>
			
		</tr>
		
		<?php foreach($campaign->drafts($sort,$order) as $d):?>

			<tr id="0" class="letterRow">
			    <td class="rowLeft">
			    	<input type="checkbox" class="letter_select" rel="<?=$d->id ?>" />
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$d->id ?>" <?php echo (empty($d->note)) ? "" : 'title="'.nl2br($d->note).'"'; ?>><?=$d->name ?></a><br />
			    </td>
			    
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->contentCheck()?>.png" alt=""></td>
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->testCheck()?>.png" alt=""></td>
				<td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->timingCheck()?>.png" alt=""></td>
	            <td class="cellCenter"><img src="<?=$base.$img?>icons/small<?=$d->recipCheck()?>.png" alt=""></td>


			</tr>		
		
		
		<? endforeach; ?>
		
		<?php if($campaign->draftLetterNum() == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs szerkesztés alatt álló levél.</span></td>
			</tr>
		<?php endif; ?>		
		
		</table>
		
		</div>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
		
		<div id="letters-relative" class="letter-container">
		
		<!--<h1>Relatív levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Relatív levelek</th>
			<th class="cellCenter" width="30%">Relatív idő</th>
            <th class="headerRight" width="20%">Statisztika</th>
		</tr>
		
		<?php foreach($campaign->relativeLetters($sort,$order) as $l):?>

			<tr class="letterRow" >
			    <td class="rowLeft">
			    	<input type="checkbox" class="letter_select" rel="<?=$l->id ?>" />
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a>
			    </td>
				<td class="cellCenter"><span><?=$l->timing_event_value ?>. nap</span></td>
                <td class="cellCenter"><a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$l->id ?>">eredmények</a></td>
			</tr>
		
		<?php endforeach; ?>
			
		<?php if($campaign->relative() == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs relatív levél.</span></td>
			</tr>
		<?php endif; ?>
				
         </table>
		
		</div>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// ?>

		<div id="letters-absolute" class="letter-container">
		
		<!--<h1>Időzített abszolút levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" id="drafts">
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Időzített abszolút levelek</th>
			<th class="cellCenter" width="30%">Küldési idő</th>
            <th class="headerRight" width="20%"></th>
		</tr>
		
		<?php 
			$absLetters = $campaign->absoluteLetters($sort,$order);
			$i=0;
			foreach($absLetters as $l):?>
			
			<?php if(!$l->isSent()): ?>	
			<tr class="letterRow" >
			    <td class="rowLeft">
			    	<input type="checkbox" class="letter_select" rel="<?=$l->id ?>" />
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a>
			    </td>
				<td class="cellCenter"><span><?=$l->getTimingDate() ?></span></td>
                <td class="cellCenter"></td>
			</tr>
			<?php 
				$i++;	
				endif; 
			?>
		<?php endforeach; ?>
			
		<?php if($i == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs nem elküldött abszolút levél.</span></td>
			</tr>
		<?php endif; ?>		
         </table>
         
         </div>

<?php //////////////////////////////////////////////////////////////////////////////// ?>
		
		<div id="letters-absolute-sent" class="letter-container">
		
		<!--<h1>Elküldött abszolút levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" >
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Elküldött abszolút levelek</th>
			<th class="cellCenter" width="30%">Küldési idő</th>
            <th class="headerRight" width="20%">Statisztika</th>
		</tr>
			
		<?php 
			$i=0;
			foreach($absLetters as $l):?>
			
			<?php if($l->isSent()): ?>	
			<tr class="letterRow" >
			    <td class="rowLeft">
			    	<input type="checkbox" class="letter_select" rel="<?=$l->id ?>" />
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id ?>" <?php echo (empty($l->note)) ? "" : 'title="'.nl2br($l->note).'"'; ?>><?=$l->name ?></a>
			    </td>
				<td class="cellCenter"><span><?=$l->getTimingDate() ?></span></td>
                <td class="cellCenter"><a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$l->id ?>">eredmények</a></td>
			</tr>
			<?php 
				$i++;	
				endif; 
			?>
		<?php endforeach; ?>
			
		<?php if($i == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs elküldött abszolút levél.</span></td>
			</tr>
		<?php endif; ?>			
		
         </table>
		
		</div>
		
<?php //////////////////////////////////////////////////////////////////////////////// ?>
<?php //////////////////////////////////////////////////////////////////////////////// 
/*
		
		<div id="letters-sendnow" class="letter-container">
		
		<!--<h1>Azonnali levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" >
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Azonnali levelek</th>
			<th class="cellCenter" width="20%">Címzettek</th>
            <th class="cellCenter" width="20%"></th>
            <th nowrap width="30" class="headerRight">&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
		
		<?php 
			$sendnowLetters = $campaign->sendnowLetters();
			$i=0;
			foreach($sendnowLetters as $l):?>
			
			<?php if(!$l->isSent()): ?>	
			<tr class="letterRow" >
			    <td class="rowLeft">
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id ?>"><?=$l->name ?></a>
			    </td>
				<td class="cellCenter"><span> <?=$l->getRecipientsNumber() ?> </span></td>
                <td class="cellCenter"></td>
                <td class="rowRight">
                	<span  style="display:true">
                		<a href="javascript:;" class="deleteLetter" title="Levél törlés" rel="<?=$l->id ?>">
                		<img src="<?=$base.$img?>icons/trash.gif" width="10" height="11" alt="Törlés"></a>
                	</span>
                </td>
			</tr>
			<?php 
				$i++;	
				endif; 
			?>
		<?php endforeach; ?>
			
		<?php if($i == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs nem elküldött azonnali levél.</span></td>
			</tr>
		<?php endif; ?>		
			
         </table>
		
		</div>

<?php //////////////////////////////////////////////////////////////////////////////// ?>

		<div id="letters-sendnow-sent" class="letter-container">

		<!--<h1>Elküldött azonnali levelek</h1>-->

		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" >
		<tr class="noHighlight">
			<th class="headerLeft" width="60%">Elküldött azonnali levelek</th>
			<th class="cellCenter" width="20%">Elküldve</th>
            <th class="cellCenter" width="20%">Statisztika</th>
            <th nowrap width="30" class="headerRight">&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
			
			<?php
			$i=0;
			foreach($sendnowLetters as $l):?>
			
			<?php if($l->isSent()): ?>	
			<tr class="letterRow" >
			    <td class="rowLeft">
				    <a href="<?=url::base() ?>pages/letteroverview/selectletter/<?=$l->id ?>"><?=$l->name ?></a>
			    </td>
				<td class="cellCenter"><span> <?=dateutils::formatDate($l->isSent(TRUE)) ?>  </span></td>
                <td class="cellCenter"><a href="<?=url::base() ?>pages/letteroverview/selectletterforstat/<?=$l->id ?>">eredmények</a></td>
                <td class="rowRight">
                	<span  style="display:true">
                		<a href="javascript:;" class="deleteLetter" title="Levél törlés" rel="<?=$l->id ?>">
                		<img src="<?=$base.$img?>icons/trash.gif" width="10" height="11" alt="Törlés"></a>
                	</span>
                </td>
			</tr>
			<?php 
				$i++;	
				endif; 
			?>
		<?php endforeach; ?>
			
		<?php if($i == 0): ?>		
			<tr class="letterRow" >
				<td colspan="5" align="center"><span>Nincs elküldött azonnali levél.</span></td>
			</tr>
		<?php endif; ?>					
			
         </table>

		</div>
		       */
               ?>
               
               
		<div class="topPad"></div>


		<?php endif; ?>
                    
        

                    
                    
					<div class="clearActivity"></div>
				</div>
			</div>
					
		</div> <!-- clientActivity end-->
		<div class="clear"></div>

		
	</div> <!-- Dashboard VÉGE ********************************************************************************************************************-->

<!-- CONTENT VÉGE ******************************************************************************************************************************-->

