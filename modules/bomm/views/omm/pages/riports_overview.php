		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"bAutoWidth": false,
					"bPaginate": false,
					"bLengthChange": false,
					"bSort": false,
					"bInfo": false,
					"bAutoWidth": false,					
					"oLanguage": {
		                "sUrl": "<?=$this->template->assets."DataTables-1.9.4/media/js/datatable.hun.js" ?>"
		            }
				} );
			} );
		</script>



		
	<div id="content">
	
	
	
	
		<?=$alert ?>
			
		<div style="height:43px;">
		
			<div id="toolbar_first" style="height:43px;text-align:right;float:left;">
				<h1>Riportok</h1>
			</div>
		
			
			<div style="clear:both"></div>
		</div>			
		
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tr class="noHighlight">
				
				<th class="headerLeft" width="">
					
				</th>
				
				<th class="cellLeft" width="100%">Riport neve</th>
				<th nowrap="nowrap" width="100" class="headerRight">Létrehozva<br /><img src="<?=$base.$img ?>_space.gif" width="100" height="1" alt=""></th>
				
			</tr>
		
		<?foreach ($riports as $r): //
		?>
		
			<tr id="<?=$r->id ?>" class="listRow" rel="<?=$r->id ?>">
				
				<td  class="rowLeft">
					
				</td>
								
			    <td class="cellLeft">
			    	<a href="<?=url::base() ?>pages/riportsoverview/index/<?=$r->id ?>" <?php echo (empty($r->note)) ? "" : 'title="'.nl2br($r->note).'"'; ?>><?=$r->name ?></a></td>
	            
	            <td class="cellLeft"><span><?=$r->created() ?></span></td>
			    
			</tr>
		
		<?endforeach; ?>
		
		<?php if(sizeof($riports) == 0): ?>
			<tr >
			
			    <td class="rowLeft" colspan="3" align="center">Önnek még nincsen beállított riportja.</a></td>
			    
			</tr>		
		<?php endif; ?>
		
		<tr class="noHighlight">
			<td class="footerLeft simple" colspan="2">&nbsp;</td>
			<td class="footerRight simple" align="center" style="padding: 4px 6px;"><strong>&nbsp;</strong></td>
		</tr>
		</table>
		
	
	

	
	<div class="clear"></div>
	
	<div>
	
	<?php if(isset($riport)): ?>
				
			<div id="toolbar_first" style="height:43px;width:700px;text-align:right;float:left;">
				<h1 style="float:left">Kiválasztott riport: <?=$riport->name ?></h1>
				<div style="float:right;width:200px;padding-top:5px">
					<a href="/pages/riportsoverview/index/<?=$riport->id?>/<?=$back_from ?>/<?=$back_to ?>">Lapozás viszafelé</a> | 
					<a href="/pages/riportsoverview/index/<?= $riport->id?>/<?=$forward_from ?>/<?=$forward_to ?>">Lapozás Előre</a>
				</div>			
			</div>				
		
		<div style="clear:both"></div>
		
	
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
				<thead>
					<tr>
					<?php $first = true;
						foreach($riportd['header'] as $h): ?>
						<?php if($first): $first = false;?>
						<th style="width:350px"><?=$h?></th>
						<?php else: ?>
						<th><?=$h?></th>
						<?php endif; ?>						
						
						
					<?php endforeach; ?>
					</tr>
				</thead>
				<tbody>
					<?php 
						$c = 0;//
						foreach($riportd['data'] as $row): ?>
					<tr class="<?=($c++%2==1)?'odd':''?>">
					
					<?php $first = true;
						foreach($riportd['header'] as $key => $h): ?>
						<?php if($first): $first = false;?>
						<td class="" style="text-align:right"><strong><?=$row[$key]?></strong></td>
						<?php else: ?>
						<td class=""><?php 
						
							if(isset($row[$key])) echo $row[$key];
							else echo ' - ';
						
						?></td>
						<?php endif; ?>
					
					
					<?php endforeach; // ?>
					
						
						
					</tr>
					<?php endforeach; ?>		
					
			
				</tbody>
				<tfoot>
			</table>		
		</div>		
		
		
			
		<?php endif; ?>	
	
	</div>
	</div>
