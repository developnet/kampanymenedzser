		<style>
			.pagination strong{
				display:block;
				background:#FFFFFF none repeat scroll 0 0;
				display:block;
				float:left;
				padding:1px 4px;
				text-decoration:none;				
			}
			
			#listActivityChart { 
			}
			#resize{
				margin-bottom:10px;
			}
		</style>
<script type="text/javascript">
	var SELECTED_LIST = 0;
	<?php if(isset($_SESSION['selected_members'])):?>
		var SELECTED_MEMBERS = new Array(
		<?php $first = true; 
			foreach($_SESSION['selected_members'] as $m):?>
			<?php 
				if($first){
					echo "".$m;
					$first = false;
				}else{
					echo ",".$m;
				}
			?>
		<?php endforeach;?>
			);
	<?php else: ?>
		var SELECTED_MEMBERS = new Array();
	<?php endif; ?>
</script>

<div id="loading_takaro">
	<table align="center" width="100%" height="100%">
		<tr>
			<td valign="" style="text-align:center"><img src="<?=$base.$img ?>ajax-loader_km.gif" /><br/>Adatok betöltése<td>	
		</tr>
	</table>
</div>	
	
	
<div id="addtagdialog" title="Címke hozzáadása" style="display:none">
    <div class="delete_dialog_question" style="width:570px;margin-bottom:0;">
   	<strong>Adja hozzá vagy törölje a következő címkéket</strong>
			<form id="tag_mod_form" name="subscribers" action="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" method="post" style="width:570px">	
			<div class="formContainer">

				<div class="clearfix">
					<label>Címkék:</label><input class="tagsfield input_text" type="text" name="tags" value="">
				</div>
			
				<div class="clearfix">
					<input class="tinput_text" type="radio" name="selection_tag" value="selected" checked="checked">A kiválasztott feliratkozókhoz (<span class="selectedmembers">0</span>)<br/>
					<input class="tinput_text" type="radio" name="selection_tag" value="all">Mindenkinek, aki a táblázatban szerepel<br/>					
				</div>			
			
				<div class="clearfix">
					<input class="tinput_text" type="radio" name="mod_tag" value="add" checked="checked">Hozzáadás<br/>
					<input class="tinput_text" type="radio" name="mod_tag" value="del">Törlés<br/>					
				</div>				
			
			</div>
			</form>	
	
    </div>
	
    <div class="">
		<a href="Javascript:;" class="guibutton tags guisubmit" id="tag_add" rel="tag_mod_form">Címkék módosítása</a> <a href="Javascript:;" class="guibutton cancel" id="addtagdialog_cancel">Mégsem</a>
    </div>	
	
</div>		

<div id="dialogOk" title="Összes feliratkozó törlése" style="display:none">
	<div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Ön most törölni fogja az összes feliratkozót!</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons"> 
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
        </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>
    </div>	
</div>	
		
<div class="twocol">
<!-- CONTENT //-->
	<div id="content" style="min-height:1400px">

		<div id="leftcol">
	
        <div class="clear"></div>
        <?php 
			//
        ?>
	    <?=$alert ?>
        <style>
        	#search{
				font-size: 18px;
				color: #333;
				width: 336px;
				height: 28px;        	
        	}
        </style>
		<div id="kereso" style="position:relative"  style="margin:auto;">        
        <form id="main_seach_form" class="kereso_doboz" action="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?>" method="get" style="float:left;width:400px">
        <input type="hidden" name="searchfield" value="all">
            <table>
            <tr>
            	<td>
            		<input type="text" class="kereso searchField decode" value="<?=$main_searchkeyword ?>" id="search" name="keyword" style="width:400px !important"/>
            		<a href="#" class="trigger"></a>
            	</td>
            	
            	<td>
            		<a href="#" class="guibutton big search-big guisubmit_decode load" id="" rel="main_seach_form">Keresés</a>
            	</td>
            </tr>
            </table>
           </form>    
            
            <form id="detail_seach_form" class="" action="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?>" method="get" style="float:left;width:450px">
            <input type="hidden" name="searchfield" value="detail">
            <div class="toggle_container">
                <h3>Részletes keresés</h3>
                <?php foreach($gridFields as $field): 
                	
                	if(isset($_GET[$field->reference])){
                		$val = urldecode($_GET[$field->reference]); 
                	}else{
               			$val = "";	
                	}
                
                   	if(isset($_GET[$field->reference])){
                		$val = urldecode($_GET[$field->reference]); 
                	}else{
               			$val = "";	
                	}                	
                	
                ?>
                <p>
                    <label><?=$field->name ?></label>
                    <input type="text" class="text decode" name="<?=$field->reference ?>" value="<?=$val?>"/>
                </p>                
                <?php endforeach; ?>
                <p>
                    <label>Feliratkozás dátuma:</label>
                </p>
                <ul>
                    <li><input id="datepicker_tol" class="datum text" type="text" name="rdate_from" value="<?=$rdate_from?>"></li>
                    <li style="width: 20px;padding:5px;text-align: center;">-</li>
                    <li><input id="datepicker_ig" class="datum text" type="text" name="rdate_to" value="<?=$rdate_to?>"></li>
                </ul>
                <p style="margin:10px">
                    <a href="#" class="guibutton search guisubmit_decode" rel="detail_seach_form">Keresés</a>
                </p>
            </div>
            
               
        </form>   
        
        <div id="filter_doboz" style="">
        <h3 style="text-align: center;">Szűrők</h3>
        <div class="doboz">
            <div class="belso">
                <div class="title">Listák: </div>
                <div class="content">
		        	<?php if($listfilter == "all"){
		        		echo " <i>mindegyik</i>";
		        	}else{
		        		$ls = explode("_", $listfilter);
		        		$lists = ORM::factory('omm_list')->in('id',$ls)->find_all();
		        		foreach($lists as $l):
		        			
		        			$listurl = "";
		        			foreach($filteredlists as $ll){
		        				if($ll != $l->id){
		        					$listurl .= $ll."_";
		        				}
		        			}
		        			$listurl = trim($listurl, "_");
		        			if($listurl == "") $listurl = "all";
		        			
		        		?>
		        			<a href="#" title="<?=$l->name?>"><?=string::trimName($l->name,30) ?></a><a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listurl ?>/all/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" class="red">x</a>
		        		<?php endforeach; 
		        	}
		        	?>                 
                </div>
            </div>
            <div class="clear"></div>
            <div class="belso">
                <div class="title">Csoportok: </div>
                <div class="content">
		        	<?php if($groupfilter == "all"){
		        		echo " <i>mindegyik</i>";//
		        	}else{
		        		$ls = explode("_", $groupfilter);
		        		$groups = ORM::factory('omm_list_group')->in('id',$ls)->find_all();
		        		foreach($groups as $l):?>
		        			<a href="#" title="<?=$l->name?>"><?=string::trimName($l->name,30) ?></a><a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/all/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" class="red">x</a>
		        		<?php endforeach; 
		        	} 
		        	
        	?> 
                </div>
            </div>
            <div class="clear"></div>
            <div class="belso">
                <div class="title">Termékek: </div>
                <div class="content">
		        	<?php if($productfilter == "all"){
		        		echo " <i>mindegyik</i>";
		        	}else{
		        		$ls = explode("_", $productfilter);
		        		$prods = ORM::factory('omm_product')->in('id',$ls)->find_all();
		        		foreach($prods as $p):
		        		
		        			$purl = "";
		        			foreach($filteredproducts as $pp){
		        				if($pp != $p->id){
		        					$purl .= $pp."_";
		        				}
		        			}
		        			$purl = trim($purl, "_");
		        			if($purl == "") $purl = "all";		        		
		        		
		        		?>
		        			<a href="#" title="<?=$p->name?>"><?=string::trimName($p->name,30) ?></a><a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$purl?><?=$uriparam?>" class="red">x</a>
		        		<?php endforeach; 
		        	} 
		        	
        	?> 
                </div>
            </div>
            <div class="clear"></div>            
            <div class="belso">
                <div class="title">Címkék: </div>
                <div class="content">
		        	<?php if($tagfilter == "all"){
		        		echo " <i>mindegyik</i>";
		        	}else{
		        		$ls = explode("_", $tagfilter);
		        		$tags = ORM::factory('omm_tag')->in('id',$ls)->find_all();
		        		foreach($tags as $l):
		        		
		        			$turl = "";
		        			foreach($filteredtags as $tt){
		        				if($tt != $l->id){
		        					$turl .= $tt."_";
		        				}
		        			}
		        			$turl = trim($turl, "_");
		        			if($turl == "") $turl = "all";				        		
		        		
		        		?>
		        			<a href="#" title="<?=$l->name?>"><?=string::trimName($l->name,30) ?></a><a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$turl ?>/<?=$productfilter?><?=$uriparam?>" class="red">x</a>
		        		<?php endforeach; 
		        	} 
		        	
		        	?>  
                </div>
            </div>
            <div class="clear"></div>
        </div>        	
        	
        </div>
             
        </div>
        
		<a name="table"></a>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabs">
		<caption>Kijelölt feliratkozók száma: <span class="selectedmembers">0</span> (<a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>?show=selected">listázás</a> | <a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>?show=delselected">kijelölés törlése</a>) </caption>
		<tr>			
			
			
			<?php if($memberStatus == "active"): ?>
			
	        	<td class="tabOnLeft" nowrap><span>Aktív</span><div style="color:#32ae00"><?=$activeMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/unsubscribed/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/error/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/prereg/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/deleted/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Törölt</a></span><?=$deletedMemberCount ?></td>
            	
            <?php elseif($memberStatus == "unsubscribed"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/active/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Leiratkozott</span><div style="color:#b92f2f"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/error/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/prereg/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/deleted/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            

            <?php elseif($memberStatus == "error"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/active/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/unsubscribed/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Hibás</span><div style="color:#b92f2f"><?=$errorMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/prereg/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/deleted/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            
				
            <?php elseif($memberStatus == "prereg"): ?>
            
	            <td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/active/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><div><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/unsubscribed/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/error/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Aktiválás elött</span><?=$preregMemberCount ?></td>				
				<td class="tabOnRight" nowrap>&nbsp;</td>	
				<td class="tabOffFarRight" nowrap><div><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/deleted/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            
            
            
            <?php elseif($memberStatus == "deleted"): ?>

	            <td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/active/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/unsubscribed/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/error/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a class="load" href="<?=url::base() ?>pages/subscribers/index/prereg/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>
				<td class="tabOnLeft" nowrap><div><span>Törölt</span><?=$deletedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>            
            
            <?php endif; ?>
            
            
            
            <td width="100%" align="right" class="tableHeaderCap">
				<a href="Javascript:;" class="guibutton" id="addtags_click">Címkék</a> 
				<a href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$orderby?>/<?=$order?>/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?>/csv<?=$uriparam?>" class="guibutton" id="export_click">Export</a> 
				
			</td>
		</tr>
		</table>
		 

		
		
		<?php /*TÁBLÁZAT**********************************************************************/ ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabsHeader">
		<tr class="noHighlight">
		
				<th class="tabHeaderLeft" width="">
					<input type="checkbox" class="select_all_member" />
				</th>		
		
			<?php /*EMAIL**********************************************************************/ ?>
			<?php if($orderby=="email" && $order=="asc"): ?>
		
				<th class="">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/email/desc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés desc szerint">
						E-mail cím
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="email" && $order=="desc"): ?>		
			
				<th class="">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/email/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
						E-mail cím<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th class="">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/email/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
						E-mail cím
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*EMAIL**********************************************************************/ ?>
			
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			<?php foreach($gridFields as $gf): ?>		
			
					<?php if($orderby==$gf->reference && $order=="asc"): ?>
				
						<th>
							<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$gf->reference ?>/desc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés desc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>
					
					<?php elseif($orderby==$gf->reference && $order=="desc"): ?>		
					
						<th>
							<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$gf->reference ?>/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>					
							
					<?php else: ?>		
		
						<th>
							<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/<?=$gf->reference ?>/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
								<?=$gf->name ?>
							</a>
						</th>								
					
					<?php endif; ?>
			
			<?php endforeach; ?>
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			<th>Címkék</th>
			<?php /*REG DÁTUMA************************************************************************/ ?>

						<?php 
						if($memberStatus == "unsubscribed"){
							$datumsz = "Leir. Dátuma";
						}else{
							$datumsz = "Fel. Dátuma";
						}
						
						?>
							
			<?php if($orderby=="reg_date" && $order=="asc"): ?>
		
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/reg_date/desc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés desc szerint">
						<?=$datumsz?>						
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="reg_date" && $order=="desc"): ?>		
			
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/reg_date/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
						<?=$datumsz?>
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a class="load" href="<?=url::base() ?>pages/subscribers/index/<?=$memberStatus ?>/reg_date/asc/<?=$listfilter?>/<?=$groupfilter?>/<?=$tagfilter?>/<?=$productfilter?><?=$uriparam?>" title="Rendezés asc szerint">
						<?=$datumsz?>
					</a>
				</th>								
			
			<?php endif; ?>
        	<?php /*<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					REG DÁTUMA**********************************************************************/ ?>
        	
        	
		</tr>
		
		
		<?php if(isset($_POST['searchfield'])):?>
				<tr id="" style="background-color:#fafafa;" >
					<td class="tabRowLeft" colspan="<?php echo sizeof($gridFields)+2;?>" align="center" >A keresés feltételeinek megfelelő <strong><?=sizeof($members)?></strong> listatag:</td>
				</tr>		
		<?php endif;?>
		
		<?php 
		
			
		foreach($members as $m): /****************************************TAGOK*/?>
				<tr id="<?=$m->id ?>" >
					<td  class="tabRowLeft"><input type="checkbox" class="select_member" rel="<?=$m->id ?>" /></td>
					<td><a href="<?=url::base() ?>pages/memberdetail/index/<?=$m->id ?>"><?=$m->email ?></a></td>
					<?php 
						foreach ($gridFields as $gf){
							$ref = $gf->reference;
							echo '<td>'.$m->$ref.'</td>';
						}
					?>
					
					<td class="tabRowLeft">
						<ul class="table-tags" style="border:0;background:0;">
						<?php foreach(Omm_tag_Model::getTagsForMember($m->id)  as $tag ): 
							
							if(strlen($tag->name) > 10) {
								$tagn = string::trimName($tag->name,8, '.');
							}else{
								$tagn = $tag->name;
							}
						
								if($tagfilter != 'all'){
									$taurl = $tagfilter."_".$tag->id;
								}else{
									$taurl = $tag->id;//
								}								
						
								$linkg = url::base()."pages/subscribers/index/".$memberStatus."/reg_date/desc/".$listfilter."/".$groupfilter."/".$taurl."/".$productfilter.$uriparam;							
							
							
						?>
							<li class="as-selection-item" style=""><a href="<?=$linkg ?>" title="<?=$tag->name ?>"><?=$tagn ?></li>
						<?php endforeach; ?>								
						</ul>
					</td>					
					
					<td class="tabRowRight cellRight"><span>
					<?php
					if($memberStatus == "unsubscribed"){
						echo $m->unsubscribe_date;
					}else{
						echo $m->reg_date;
					}	
					 ?>	
						
					
					</span></td>
				</tr>
		<?php endforeach;  /****************************************TAGOK*/?>
			        
		</table>
		
			<?php /*TÁBLÁZAT**********************************************************************/ ?>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableFooter">
		<tr>
			<td class="footerLeft">&nbsp;</td>
			<td class="footerRight" align="right" valign="top">&nbsp;</td>
		</tr>
		</table>
        <?=$pagination ?>
        
		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">
            <div class="mybutton" style="float:left;padding-bottom:30px;">    
                <a href="<?=url::base() ?>pages/memberdetail" class="guibutton add" id="" style="margin-right:0">Hozzáadás</a>
                <a href="<?=url::base() ?>/pages/listexport/index/import" class="guibutton add" id="" style="margin-right:0">Importálás</a>
			<div style="clear:both"></div>	
            </div>	

			<div style="clear:both"></div>
			
			<?php /**/ ?>
<script type="text/javascript">
 $(function(){
 	$('.list_open').click(function(){
 		
 		rel = $(this).attr("rel");
 		$("."+rel).toggle();
 	
 	});
 
 });
 </script>			
			
			
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" style="margin-bottom:0">
			<tbody><tr class="noHighlight even">
				<th class="headerLeft" width="100%" colspan="2"><span>Hírlevél-listák</span></th>
				<th nowrap="">&nbsp;</th>
                <th class="headerRight" style="padding-right:0px;" nowrap="">&nbsp;</th>
			</tr>
			</table>
			<div style="width:218px;height:300px;display:block;overflow:scroll;overflow-x:hidden;">
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" style="margin-bottom:0">
			<?php foreach($client->lists() as $list): 
				
				if($listfilter != 'all'){
					$listurl = $listfilter."_".$list->id;
				}else{
					$listurl = $list->id;
				}
			
				$link = url::base()."pages/subscribers/index/".$memberStatus."/reg_date/desc/".$listurl."/all/".$tagfilter."/".$productfilter.$uriparam;
				$groups = $list->groups();
				if(sizeof($groups) > 0){
					$g = true;
				}else{
					$g = false;
				}
				
				if(in_array($list->id,$filteredlists)){
					$open = true;
				}else{
					$open = false;
				}
				
			?>

			<tr class="dashRow">
				<td width="9" style="padding-right:0px;">
				<?php if($g): ?>
                	<a href="Javascript:;" class="list_open" rel="list_<?=$list->id ?>">
                	<?php if($open): ?>
                		<img src="<?=url::base() ?>/core/views/omm/img/icons/plusz.gif" border="0" width="9" height="9" alt="" class="list_<?=$list->id ?>" style="display:none">
                		<img src="<?=url::base() ?>/core/views/omm/img/icons/minusz.gif" border="0" width="9" height="9" alt="" class="list_<?=$list->id ?>">
                	<?php else: ?>
                		<img src="<?=url::base() ?>/core/views/omm/img/icons/plusz.gif" border="0" width="9" height="9" alt="" class="list_<?=$list->id ?>">
                		<img src="<?=url::base() ?>/core/views/omm/img/icons/minusz.gif" border="0" width="9" height="9" alt="" class="list_<?=$list->id ?>" style="display:none">                	
                	<?php endif; ?>
                	</a>
                <?php endif; ?>
                </td>
				<td width="100%">
                	<a href="<?=$link ?>" title="<?=$list->name ?>" class="load"><?=string::trimName($list->name,30) ?></a>
                </td>
                <td colspan="2"></td>
			</tr>		
			
			<?php foreach($groups as $group): 
			
					$gurl = $group->id;
			
			
					$linkg = url::base()."pages/subscribers/index/".$memberStatus."/reg_date/desc/".$list->id."/".$gurl."/".$tagfilter."/".$productfilter.$uriparam;
			////?>
            <tr class="list_<?=$list->id ?>" <?php echo ($open) ? '' : 'style="display:none"'; ?>>
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
               		<a href="<?=$linkg ?>" class="subFolderNotEmpty load" rel="drafts" title="<?=$group->name ?>"><?=string::trimName($group->name,30) ?></a>
                </td>
			</tr> 					
			<?php endforeach; ?>
			
			<?php endforeach; ?>
			</tbody>
			</table>	
			</div>
			
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader" style="margin-bottom:0">
			<tbody><tr class="noHighlight even">
				<th class="headerLeft" width="100%" colspan="2"><span>Termékek</span></th>
				<th nowrap="">&nbsp;</th>
                <th class="headerRight" style="padding-right:0px;" nowrap="">&nbsp;</th>
			</tr>
			</table>
			<div style="width:218px;height:300px;display:block;overflow:scroll;overflow-x:hidden;margin-bottom:30px">
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<?php foreach($client->products() as $prod): 
			
				if($productfilter != 'all'){
					$produrl = $productfilter."_".$prod->id;
				}else{
					$produrl = $prod->id;
				}			
			
				$link = url::base()."pages/subscribers/index/".$memberStatus."/reg_date/desc/".$listfilter."/".$groupfilter."/".$tagfilter."/".$produrl.$uriparam;
				
			?>

			<tr class="dashRow">
				<td width="9" style="padding-right:0px;">
                </td>
				<td width="100%">
                	<a href="<?=$link ?>" title="<?=$prod->name ?>" class="load"><?=string::trimName($prod->name, 30)?></a>
                </td>
                <td colspan="2"></td>
			</tr>		
			
			<?php endforeach; ?>
			</tbody>
			</table>				
			</div>
			
			
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
			<tbody><tr class="noHighlight even">
				<th class="headerLeft" width="100%" colspan="2"><span>Címkék</span></th>
				<th nowrap="">&nbsp;</th>
                <th class="headerRight" style="padding-right:0px;" nowrap="">&nbsp;</th>
			</tr>
			</tbody>
			</table>
			<div style="width:218px;height:300px;display:block;overflow:scroll;overflow-x:hidden;">
			<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">
	
				
						
						<?php foreach($client->getTags() as $tag): 

								if($tagfilter != 'all'){
									$taurl = $tagfilter."_".$tag->id;
								}else{
									$taurl = $tag->id;
								}								
						
								$linkg = url::base()."pages/subscribers/index/".$memberStatus."/reg_date/desc/".$listfilter."/".$groupfilter."/".$taurl."/".$productfilter.$uriparam;
								$tagn = string::trimName($tag->name,30);//
						?>
						<tr class="dashRow">
							<td width="100%">
								<a href="<?=$linkg ?>" title="<?=$tag->name ?>"><?=$tagn ?>
							</td>
						</tr>
						<?php endforeach; ?>								
								
						
                </td>
			</tr>				
			</tbody>
			</table>			
			</div>		
			<?php /* 
			       
			       
			<tr class="dashRowActive">
				<td width="9" style="padding-right:0px;">
                	<a href="http://new.kmsrv2.hu/pages/campaignoverview/deselect/2"><img src="http://new.kmsrv2.hu/core/views/omm/img/icons/minusz.gif" border="0" width="9" height="9" alt=""></a>
                </td>
				<td width="100%" >
                	<strong><a href="http://new.kmsrv2.hu/pages/campaignoverview/deselect/2" title="">Lista neve</a></strong>
                </td>
			</tr>			        
            <tr class="dashRow even">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
               		<a href="Javascript:;" class="subFolderNotEmpty" rel="drafts">Csoport neve</a>
                </td>
			</tr>            
            <tr class="dashRow">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
               		<a href="Javascript:;" class="subFolderNotEmpty" rel="relative">Csoport 2 neve</a>
                </td>
			</tr>                                    
            <tr class="dashRow even">
				<td width="9" style="padding-right:0px;">&nbsp;</td>
				<td width="100%" colspan="3">
                		<a href="Javascript:;" class="subFolderNotEmpty" rel="absolute">Csoport 3 neve</a>
                </td>
			</tr>
			<tr class="dashRow">
				<td width="9" style="padding-right:0px;">
                	<a href="http://new.kmsrv2.hu/pages/campaignoverview/deselect/2"><img src="http://new.kmsrv2.hu/core/views/omm/img/icons/plusz.gif" border="0" width="9" height="9" alt=""></a>
                </td>
				<td width="100%">
                	<strong><a href="http://new.kmsrv2.hu/pages/campaignoverview/deselect/2" title="">Lista 2 neve</a></strong>
                </td>
			</tr>			
			*/ ?>
			
			
			
			<?php /**/ ?>
			
			
        </div> <!--options end-->
    <div class="clear"></div> 
    </div> <!--rightcol end-->

    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->