<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			Értesítések beállításai
		</p>
		
		<h1>Értesítések beállításai</h1>

		<?=$errors ?>

		<p></p>
		
		<form name="unsubscribeForm" action="<?=url::base() ?>pages/listnotifications" method=post>
		    <div class="formBG">
		    <div class="formWrapper">
 			
			    <h3 class="topPad">Értesítés küldése feliratkozáskor a következő e-mail címre:</h3>
			    <div class="formContainer">
				    <div class="clearfix">
				    <input type="checkbox" name="subscribe_email_check" value="1" <?php echo ($form['subscribe_email_check'] == "1") ? 'checked="checked"' : ""; ?>/><input type="text" name="subscribe_email" size="255" tabindex="1" value="<?=$form['subscribe_email'] ?>" class="<?=$classes['subscribe_email'] ?> input_text" style="width:400px">
				    </div>
			    </div>
			
				<h3>Értesítés küldése leiratkozáskor a következő e-mail címre:</h3>
				<div class="formContainer">
					<div class="clearfix">
						<input type="checkbox" name="unsubscribe_email_check" value="1"  <?php echo ($form['unsubscribe_email_check'] == "1") ? 'checked="checked"' : ""; ?> /><input type="text" name="unsubscribe_email" id="unsubscribe_email" size="255" value="<?=$form['unsubscribe_email'] ?>" class="<?=$classes['unsubscribe_email'] ?> input_text" style="width:400px" />
					</div>
				</div>

<!-- 
				<h3>Napi összesítő küldése a következő e-mail címre:</h3>
				
				<div class="formContainer">
					<div class="clearfix">
						<input type="checkbox" name="daily_email_check" value="1" <?php echo ($form['daily_email_check'] == "1") ? 'checked="checked"' : ""; ?>/><input type="text" name="daily_email" id="daily_email" size="255" value="<?=$form['daily_email'] ?>" class="<?=$classes['daily_email'] ?> input_text" style="width:400px" />
					</div>
				</div>
 -->

			<div class="topPad"></div>
            
            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>buttons/icon-tick.gif" width="16" height="16" alt="" />
					Beállítások mentése
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/listdetail" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
            </div>
            
            <div class="clearButton"></div>


		    </div>		
		    </div>

		</form>
		
	</div>
	
	<div id="rightcol">
	
		<div id="options">

	
		</div>
	
	</div>





	
	<div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->
