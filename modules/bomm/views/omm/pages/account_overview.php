<?php 

$gomb = "Előfizetői profil mentése";
$title = 'Előfizetői profil';

?>

<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'):
			$email = 'ugyfelszolgalat@garbaroyal.hu';
		else:
			$email = 'support@kampanymenedzser.hu';
	 	endif; 
?>


<style>

.clearfix label{
	width:120px;
}

</style>

	<div class="twocol">
	<div id="adminWrap">		
	<div id="content">


	<div id="leftcol">
		<h1 class="extraBottomPad"><?=$title?></h1>
		
			<div id="bigAlert">	
	            <h1>Előfizetői profil módosítása</h1>
	            <p>Az itt látható adatok módosításához írjon nekünk e-mailt az <?=$email ?> e-mail címre.</p>
	        </div>
	        
			<?=$errors ?>
	    
	    <div class="formBG">
		<div class="formWrapper">
			<form action="<?=url::base() ?>pages/useroverview" name="editAccount" method="post">
			
			<div class="formContainer">
				<div class="clearfix">
					<label>Aldomain</label><input class="input_text" name="aldomain" id="name" type="text" size="45" value="<?=meta::getKMuser() ?>" disabled="disabled" />
				</div>
            </div>    
                
			<h3>Számlázási adatok</h3>
			
            <div class="formContainer">
            	<div class="clearfix">
                	<label>Cégnév</label><input class="input_text" name="email" type="text" size="45" value="<?=$account->company ?>" disabled="disabled" />
				</div>
				<div class="clearfix">
					<label>Adószám</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->billing_taxnumber ?>"  disabled="disabled"/>
				</div>
<!--				<div class="clearfix">
					<label>Város</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->billing_city ?>"  disabled="disabled"/>
				</div>-->
				<div class="clearfix">
					<label>Irsz., város</label><input class="input_text" name="lastname" type="text" size="5" value="<?=$account->billing_zip ?>" disabled="disabled" />
                    <input class="input_text" name="lastname" type="text" size="32" value="<?=$account->billing_city ?>"  disabled="disabled"/>
				</div>
				<div class="clearfix">
					<label>Utca, házszám</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->billing_address ?>"  disabled="disabled"/>
				</div>
             </div>
             
			 <h3>Postacím</h3>
             
             <div class="formContainer">
				<div class="clearfix">
                	<label>Címzett</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->posting_city ?>"  disabled="disabled"/>
				</div>
				<div class="clearfix">
					<label>Irsz., város</label><input class="input_text" name="lastname" type="text" size="5" value="<?=$account->posting_zip ?>"  disabled="disabled"/>
                    <input class="input_text" name="lastname" type="text" size="32" value="<?=$account->posting_city ?>"  disabled="disabled"/>
				</div>
				<div class="clearfix">
					<label>Utca, házszám</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->posting_address ?>"  disabled="disabled"/>
				</div>
             </div>
             
            <h3>Kapcsolattartó adatok</h3>
             
			<div class="formContainer">	
				<div class="clearfix">
                	<label>Vezetéknév</label><input class="input_text" name="email" type="text" size="45" value="<?=$account->contact_familyname ?>"  disabled="disabled"/> <span class="left"></span>
				</div>
				<div class="clearfix">
                	<label>Keresztnév</label><input class="input_text" name="email" type="text" size="45" value="<?=$account->contact_firstname ?>"  disabled="disabled"/> <span class="left"></span>
				</div>
				<div class="clearfix">
					<label>E-mail cím</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->contact_email ?>"  disabled="disabled"/>
				</div>
				<div class="clearfix">
					<label>Telefonszám</label><input class="input_text" name="lastname" type="text" size="45" value="<?=$account->contact_phone ?>"  disabled="disabled"/>
				</div>
			</div>
			<div class="clear"></div>
			
            <div class="mybutton">

                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>" id="cancelSaveChanges">Vissza</a></span>
                <div style="clear:both"></div>
            </div> 
             
		<div class="clearButton"></div>
			
		</form>
		
		</div>
		</div>
	

	
	<div id="rightcol">

				<div id="options">

					<div class="bghighlight"><h3 class="sidebar">Előfizetői profil oldalai</h3></div>
					<dl class="icon-menu">		
		                <dt><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Előfizetői adatok" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview" id="addCustomFieldLink">Előfizetői adatok</a></dd>
						<div class="clear"></div>
						
				<?php /*
		                <dt><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Egyenleg" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/actmonth" id="addCustomFieldLink">Egyenleg</a></dd>
						<div class="clear"></div>
		                 
						<dt><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Számlák" /></a></dt>
						<dd><a href="<?=url::base() ?>pages/accountoverview/billing" id="segmentsLink">Számlatörténet</a></dd>
						<div class="clear"></div>
						*/ ?>
		            </dl>
					</div>

					<div class="newFeatures">
                    &nbsp;
					</div>

				</div>

	</div>


			<div class="clear"></div>


	</div>
	</div>
	</div>
