		<style>
			.pagination strong{
				display:block;
				background:#FFFFFF none repeat scroll 0 0;
				display:block;
				float:left;
				padding:1px 4px;
				text-decoration:none;				
			}
			
			#listActivityChart { 
			}
			#resize{
				margin-bottom:10px;
			}
		</style>
<script type="text/javascript">
</script>


<script type="text/javascript">
 $(function(){
	
	<?php if($showing!="all"): ?>
	
	swfobject.embedSWF(
		"<?=$assets?>amcolumn/amcolumn.swf", "listActivityChart",
		"100%", "400", "9.0.0", "expressInstall.swf",
		{
		"settings_file" : "<?=KOHANA::config('core.report_xml')?>list_report_01.xml", 
		"data_file" : "<?=url::base() ?>charts/listactivity/index/<?=$list_id?>/<?=$showing?>/<?=$param?>", 
		"path" : "<?=$assets?>amcolumn/",
		"preloader_color" : "#999999",
		"wmode" : "transparent",
		"wmode" : "opaque"
		},
		{"wmode" : "transparent"} );
 
 
 	<?php endif; ?>
 
 	
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	
 	
 

 
 });

function ofc_resize(left, width, top, height){
	var tmp = new Array(
	'left:'+left,
	'width:'+ width,
	'top:'+top,
	'height:'+height );
	
	$("#resize_info").html( tmp.join('<br>') );
}

</script>
		

<div class="twocol">
<!-- CONTENT -->
	<div id="content">

		<div id="leftcol">
		<p class="bread"><a href="<?=url::base() ?>pages/productoverview">Termékek</a><span class="breadArrow">&nbsp;</span><?=$product->name ?></p>
		<h1>
            
            <div class="List_TimeSelector">

		<form action="<?=url::base().url::current(true)?>#table" method="post">
				<?php 
					if(isset($_POST['searchkeyword'])) $keyword = $_POST['searchkeyword'];
					else $keyword = ""; 

					if(isset($_POST['searchfield'])) $searchfield = $_POST['searchfield'];
					else $searchfield = "email";					
					
				?>
				<table border="0" cellpadding="0" cellspacing="0" class="searching">
				<tr>
					<td colspan="3" class="searchTitle">Keresés:</td>
				</tr>
                <tr>	
                    <td>
						<select name="searchfield">
								<option value="email" <?php echo ($searchfield == "email") ? 'selected="selected"' : ""; ?>>E-mail cím</option>
								<option value="regdatum" <?php echo ($searchfield == "regdatum") ? 'selected="selected"' : ""; ?>>Regisztráció dátuma</option>			
							<?php foreach($gridFields as $f):?>
								<?php if($searchfield == $f->reference):?>
									<option value="<?=$f->reference?>" selected="selected"><?=$f->name?></option>
								<?php else:?>
									<option value="<?=$f->reference?>"><?=$f->name?></option>
								<?php endif;?>
							<?php endforeach;?>
						</select>
					</td>
                    <td>	
						<input type="text" name="searchkeyword" id="search" value="<?=$keyword?>" class="searchField input_text">
                    </td>
					<td>
                    <div class="mybutton">
                        <button type="submit" class="button" style="padding:3px 6px 3px 8px;margin:0px;">
                            <img src="<?=$base.$img?>icons/search.png" width="16" height="16" alt=""/>
                        </button>
                        <div style="clear:both"></div>
                    </div>
                    </td>
				</tr>
				<tr>
					<td colspan="3" align="right">
                			<?php if(isset($_POST['searchkeyword'])):?>
                			<a href="<?=url::base().url::current(true)?>" class="button">
                    		Keresés törlése
			                </a>
			                <?php else:?>
			                	&nbsp;
			                <?php endif;?>
					</td>
				</tr>

				</table>
        		</form>            
            
            
                <div class="clear"></div>
            
    		
    		</div>
		<?=$product->name ?><?php echo (empty($note)) ? "" : '<a href="Javascript:;" title="'.nl2br($product->note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?><br/>
		
        <span>Terméket megvásárolt feliratkozók száma: <span class="green"><?=$allActiveMember ?></span></span>
        </h1>
        <div class="clear"></div>
        
	    <?=$alert ?>
        
        
        <?php if($showing !="all"): ?>
        <!--LISTA GRAFIKON-->
        <div id="resize">
        	<div class="chart" id="listActivityChart"></div>
        </div>
        <!--LISTA GRAFIKON-->
        <?php endif; ?>
        		
		<a name="table"></a>
		<?php if($showing =="all"): ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabs">
		<tr>			
			
			
			<?php if($memberStatus == "purchased"): ?>
			
	        	<td class="tabOnLeft" nowrap><span>Megvásárolta</span><div style="color:#32ae00"><?=$purchasedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/ordered?showing=<?=$showing ?>&param=<?=$param ?>">Megrendelte</a></span><div style="color:#efe702"><?=$orderedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/failed?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$failedMemberCount ?></div></td>
            	
            <?php elseif($memberStatus == "ordered"): //?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/purchased?showing=<?=$showing ?>&param=<?=$param ?>">Megvásárolta</a></span><div style="color:#afdda0"><?=$purchasedMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Megrendelte</span><div style="color:#efe702"><?=$orderedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/failed?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$failedMemberCount ?></div></td>

            <?php elseif($memberStatus == "failed"): ?>

	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/purchased?showing=<?=$showing ?>&param=<?=$param ?>">Megvásárolta</a></span><div style="color:#afdda0"><?=$purchasedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id?>/ordered?showing=<?=$showing ?>&param=<?=$param ?>">Megrendelte</a></span><div style="color:#efe702"><?=$orderedMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Hibás</span><div style="color:#b92f2f"><?=$failedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
            
            <?php endif; ?>
            
            
            
            <td width="100%" align="right" class="tableHeaderCap">
		
				
			</td>
		</tr>
		</table>
		

		
		
		<?php /*TÁBLÁZAT**********************************************************************/ ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabsHeader">
		<tr class="noHighlight">
			<?php /*EMAIL**********************************************************************/ ?>
			<?php if($orderby=="email" && $order=="asc"): ?>
		
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/email/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
						E-mail cím
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="email" && $order=="desc"): ?>		
			
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/email/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						E-mail cím
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/email/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						E-mail cím
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*EMAIL**********************************************************************/ ?>
			
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			<?php foreach($gridFields as $gf): ?>		
			
					<?php if($orderby==$gf->reference && $order=="asc"): ?>
				
						<th>
							<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/<?=$gf->reference ?>/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>
					
					<?php elseif($orderby==$gf->reference && $order=="desc"): ?>		
					
						<th>
							<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/<?=$gf->reference ?>/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>					
							
					<?php else: ?>		
		
						<th>
							<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/<?=$gf->reference ?>/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
							</a>
						</th>								
					
					<?php endif; ?>
			
			<?php endforeach; ?>
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>

			<?php if($memberStatus == "purchased"): ?>
			
				<?php /*ORDER DÁTUMA************************************************************************/ ?>
								
				<?php if($orderby=="purchase_date" && $order=="asc"): ?>
			
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/purchase_date/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
							Vásár. dátuma						
							<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>
				
				<?php elseif($orderby=="purchase_date" && $order=="desc"): ?>		
				
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/purchase_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Vásár. dátuma
							<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>					
						
				<?php else: ?>		
	
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/purchase_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Vásár. dátuma
							<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
						</a>
					</th>								
				
				<?php endif; ?>
	        	<?php /*REG DÁTUMA**********************************************************************/ ?>			
            	
            <?php elseif($memberStatus == "ordered"): //?>

				<?php /*ORDER DÁTUMA************************************************************************/ ?>
								
				<?php if($orderby=="order_date" && $order=="asc"): ?>
			
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/order_date/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
							Megrend. dátuma						
							<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>
				
				<?php elseif($orderby=="order_date" && $order=="desc"): ?>		
				
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/order_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Megrend. dátuma
							<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>					
						
				<?php else: ?>		
	
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/order_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Megrend. dátuma
							<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
						</a>
					</th>								
				
				<?php endif; ?>
	        	<?php /*REG DÁTUMA**********************************************************************/ ?>

            <?php elseif($memberStatus == "failed"): ?>

				<?php /*ORDER DÁTUMA************************************************************************/ ?>
								
				<?php if($orderby=="failed_date" && $order=="asc"): ?>
			
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/failed_date/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
							Hiba dátuma						
							<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>
				
				<?php elseif($orderby=="failed_date" && $order=="desc"): ?>		
				
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/failed_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Hiba dátuma
							<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
						</a>
					</th>					
						
				<?php else: ?>		
	
					<th nowrap="nowrap" class="tabHeaderRight cellRight">
						<a href="<?=url::base() ?>pages/productdetail/index/<?=$product->id ?>/<?=$memberStatus ?>/failed_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
							Hiba dátuma
							<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
						</a>
					</th>								
				
				<?php endif; ?>
	        	<?php /*REG DÁTUMA**********************************************************************/ ?>
            
            <?php endif; ?>			
			

        	
        	
		</tr>
		
		
		<?php if(isset($_POST['searchfield'])):?>
				<tr id="" style="background-color:#fafafa;" >
					<td class="tabRowLeft" colspan="<?php echo sizeof($gridFields)+2;?>" align="center" >A keresés feltételeinek megfelelő <strong><?=sizeof($members)?></strong> listatag:</td>
				</tr>		
		<?php endif;?>
		
		<?php foreach($members as $m): ///****************************************TAGOK*/?>
				<tr id="<?=$m->id ?>" >
					<td class="tabRowLeft"><a href="<?=url::base() ?>pages/productdetail/selectmember/<?=$m->id ?>"><?=$m->email ?></a></td>
					<?php 
						foreach ($gridFields as $gf){
							$ref = $gf->reference;
							echo '<td>'.$m->$ref.'</td>';
						}
					?>
					<td class="tabRowRight cellRight"><span>
					<?php 
						if($memberStatus == "purchased"){
							echo $m->purchase_date;
						}elseif($memberStatus == "ordered"){
							echo $m->order_date;
						}elseif($memberStatus == "failed"){
							echo $m->failed_date;
						}
					
						// $product->getProductDate($m->id,$memberStatus)
						?>
					
					
					</span></td>
				</tr>
		<?php endforeach;  /****************************************TAGOK*/?>
			        
		</table>
		
			<?php /*TÁBLÁZAT**********************************************************************/ ?>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableFooter">
		<tr>
			<td class="footerLeft">&nbsp;</td>
			<td class="footerRight" align="right" valign="top">&nbsp;</td>
		</tr>
		</table>
        <?=$pagination ?>
        
        <?php endif; ///táblázat ?>
        
		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">

			<div style="clear:both"></div>
			
			<div class="bghighlight"><h3 class="sidebar">Eszközök</h3></div>
			<dl class="icon-menu">		
                <dt><a href="<?=url::base() ?>pages/listadd/first" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/productadd/first/<?=$product->id ?>" id="addCustomFieldLink">Termék adatainak módosítása</a></dd>
				<div class="clear"></div>
            </dl>
		
			
        </div> <!--options end-->
    <div class="clear"></div> 
    </div> <!--rightcol end-->

    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->