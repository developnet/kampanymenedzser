		<style>
			.pagination strong{
				display:block;
				background:#FFFFFF none repeat scroll 0 0;
				display:block;
				float:left;
				padding:1px 4px;
				text-decoration:none;				
			}
			
			#listActivityChart { 
			}
			#resize{
				margin-bottom:10px;
			}
		</style>
<script type="text/javascript">
</script>


<script type="text/javascript">
 $(function(){
	
	<?php if($showing!="all"): ?>
	
	swfobject.embedSWF(
		"<?=$assets?>amcolumn/amcolumn.swf", "listActivityChart",
		"100%", "400", "9.0.0", "expressInstall.swf",
		{
		"settings_file" : "<?=KOHANA::config('core.report_xml')?>list_report_01.xml", 
		"data_file" : "<?=url::base() ?>charts/listactivity/index/<?=$list_id?>/<?=$showing?>/<?=$param?>", 
		"path" : "<?=$assets?>amcolumn/",
		"preloader_color" : "#999999",
		"wmode" : "transparent",
		"wmode" : "opaque"
		},
		{"wmode" : "transparent"} );
 
 
 	<?php endif; ?>
 
 	/*
 	$('#resize').resizable({
 						'maxWidth' : 800
 						});
 	*/					
 	//
 	
 	$('#deleteMembers').click(function(){
 		deleteMembers();
 	});
 	
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: false,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	
 	
 	var deleteMembers = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '/pages/listdetail/deleteAllMember';
					
	});

 
 });

function ofc_resize(left, width, top, height){
	var tmp = new Array(
	'left:'+left,
	'width:'+ width,
	'top:'+top,
	'height:'+height );
	
	$("#resize_info").html( tmp.join('<br>') );
}

</script>
		
<div id="dialog" title="Összes feliratkozó törlése" style="display:none">
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
	<strong>Valóban végelgesen törölni szeretné az összes feliratkozót?</strong>
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
       </div>
    </div>	
	
</div>		

<div id="dialogOk" title="Összes feliratkozó törlése" style="display:none">
	<div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Ön most törölni fogja az összes feliratkozót!</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons"> 
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
        </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>
    </div>	
</div>	
		
<div class="twocol">
<!-- CONTENT -->
	<div id="content">

		<div id="leftcol">
		<p class="bread"><a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a><span class="breadArrow">&nbsp;</span><?=$listName ?></p>
		<h1>
            
            <div class="List_TimeSelector">
            	<div class="period">
                    <div class="periods">
                    	<?php if($showing!="year"): ?>
                    	<a href="?showing=year">Éves</a>
                    	<?php else: ?>
                    	Éves
                    	<?php endif; ?>                       
                    </div>
                    <div class="periods">
                    	<?php if($showing!="month"): ?>
                    	<a href="?showing=month">Havi</a>
                    	<?php else: ?>
                    	Havi
                    	<?php endif; ?>
                    </div>
                    <div class="periods">
                    	<?php if($showing!="week"): ?>
                    	<a href="?showing=week">Heti</a>
                    	<?php else: ?>
                    	Heti
                    	<?php endif; ?>                      
                    </div>
                    <div class="periods">
                    	<?php if($showing!="day"): ?>
                    	<a href="?showing=day">Napi</a>
                    	<?php else: ?>
                    	Napi
                    	<?php endif; ?>                        
                    </div>
                </div>
            
                <div class="clear"></div>
            
                <div class="select">
                	<?php
                		$left = "";
                		$right = "";
                		
						if($showing == "month"){

							$left = date( "Y-m-d", strtotime( $param."-01 -1 month" ));
                			$right = date( "Y-m-d", strtotime( $param."-01 +1 month" ));	
							$left = substr($left,0,7);
							$right = substr($right,0,7);                			

						}elseif($showing == "year"){

							$left = date( "Y-m-d", strtotime( $param."-01-01 -1 year" ));
							$right = date( "Y-m-d", strtotime( $param."-01-01 +1 year" ));
							$left = substr($left,0,4);
							$right = substr($right,0,4);                									
						
						}elseif($showing == "week"){
							
							$left = date( "Y-m-d", strtotime( $param." -1 week" ));
							$right = date( "Y-m-d", strtotime( $param." +1 week" ));
														
						}elseif($showing == "day"){
							
							$left = date( "Y-m-d", strtotime( $param." -1 day" ));
							$right = date( "Y-m-d", strtotime( $param." +1 day" ));
														
						}

                	
                	?>
                	<?php if($showing != "all"): ?>
                	<a href="<?=string::add_to_query_string("param",$left) ?>">&laquo; </a> <?=" ".$param." " ?> <a href="<?=string_Core::add_to_query_string("param",$right) ?>"> &raquo; </a>
                	<?php else: ?>
                	<a href="/pages/listdetail/index?showing=month">Feliratkozók számának alakulása</a>
                	<?php endif; ?>
                </div>
    		
    		</div>
		<?=$listName ?><?php echo (empty($note)) ? "" : '<a href="Javascript:;" title="'.nl2br($note).'"><img src="'.$base.$img.'icons/information.png" /></a>'; ?><br/>
		
        <span>Aktív feliratkozók száma: <span class="green"><?=$allActiveMember ?></span></span>
        </h1>
		<p class="titleSummary">
		<span style="font-size:12px;">
        <!--<a href="?showing=all"></a>-->
		<?php if($showing!="all"): ?>
        <a href="?showing=all">A lista összes tagjának megjelenítése</a>
        <?php else: ?>
        A lista összes tagjának megjelenítése
        <?php endif; ?>
        <!--<a href="<?=url::base() ?>pages/listadd/first">Lista nevének / típusának megváltoztatása</a>-->
        </span>
        </p>
        <div class="clear"></div>
        
	    <?=$alert ?>
        
        <?php if($checkUnsubProcess == false && $checkSubProcess == false):?>
        
          <div id="bigAlert">	
            <h1>Hiányos feliratkozási folyamatok!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listsubscribeprocess">feliratkozási</a> folyamatokat!</p>
          </div>        
        
        <?php elseif($checkUnsubProcess == false && $checkSubProcess == true): ?>
		
        <?php /* 	
          <div id="bigAlert">	
            <h1>Hiányos leiratkozási folyamatok!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listunsubscribeprocess">leiratkozási</a> folyamatokat!</p>
          </div>
        */ ?>
        
        <?php elseif($checkUnsubProcess == true && $checkSubProcess == false): ?>
        
          <div id="bigAlert">	
            <h1>Hiányos feliratkozási folyamatok!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listsubscribeprocess">feliratkozási</a> folyamatokat!</p>
          </div>        
        
        <?php endif; ?>
        
        <?php if($showing !="all"): ?>
        <!--LISTA GRAFIKON-->
        <div id="resize">
        	<div class="chart" id="listActivityChart"></div>
        </div>
        <!--LISTA GRAFIKON-->
        <?php endif; ?>
        		
		<a name="table"></a>
		<?php if($showing =="all"): ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabs">
		<caption>
			<?php
			if($showing == "all"){
				echo "Lista összes tagja";
			}elseif($showing == "year"){
				echo "Lista tagjainaik aktivitása $param-ben";	
			}elseif($showing == "month"){
				echo "Lista tagjainaik aktivitása a $param hónapban";
			}elseif($showing == "week"){
				echo "Lista tagjainaik aktivitása a $param-ai héten";
			}elseif($showing == "day"){
				echo "Lista tagjainaik aktivitása a $param-á(é)n";
			}
			
			?>
		</caption>
		<tr>			
			
			
			<?php if($memberStatus == "active"): ?>
			
				<?php if($showing != "all"): ?>
	        	<td class="tabOnLeft" nowrap><span>Feliratkozott</span><div style="color:#32ae00"><?=$activeMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/unsubscribed?showing=<?=$showing ?>&param=<?=$param ?>#table">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>			
				<?php else: ?>
	        	<td class="tabOnLeft" nowrap><span>Aktív</span><div style="color:#32ae00"><?=$activeMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/unsubscribed?showing=<?=$showing ?>&param=<?=$param ?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/error?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/prereg?showing=<?=$showing ?>&param=<?=$param ?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/deleted?showing=<?=$showing ?>&param=<?=$param ?>">Törölt</a></span><?=$deletedMemberCount ?></td>
				<?php endif; ?>
            	
            <?php elseif($memberStatus == "unsubscribed"): ?>

				<?php if($showing != "all"): ?>
	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>#table">Feliratkozott</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Leiratkozott</span><div style="color:#b92f2f"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>				
				<?php else: ?>
	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Leiratkozott</span><div style="color:#b92f2f"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/error?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/prereg?showing=<?=$showing ?>&param=<?=$param ?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/deleted?showing=<?=$showing ?>&param=<?=$param ?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            
				<?php endif; ?>

            <?php elseif($memberStatus == "error"): ?>

				<?php if($showing != "all"): ?>
	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>#table">Feliratkozott</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Leiratkozott</span><div style="color:#b92f2f"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>				
				<?php else: ?>
	            <td class="tabOffFarLeft" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/unsubscribed?showing=<?=$showing ?>&param=<?=$param ?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Hibás</span><div style="color:#b92f2f"><?=$errorMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>	            
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/prereg?showing=<?=$showing ?>&param=<?=$param ?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>				
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/deleted?showing=<?=$showing ?>&param=<?=$param ?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            
				<?php endif; ?>
				
            <?php elseif($memberStatus == "prereg"): ?>
            
	            <td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/unsubscribed?showing=<?=$showing ?>&param=<?=$param ?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/error?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOnLeft" nowrap><span>Aktiválás elött</span><?=$preregMemberCount ?></td>				
				<td class="tabOnRight" nowrap>&nbsp;</td>	
				<td class="tabOffFarRight" nowrap><div><span><a href="<?=url::base() ?>pages/listdetail/index/deleted?showing=<?=$showing ?>&param=<?=$param ?>">Törölt</a></span><?=$deletedMemberCount ?></td>	            
            
            
            <?php elseif($memberStatus == "deleted"): ?>

	            <td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/active?showing=<?=$showing ?>&param=<?=$param ?>">Aktív</a></span><div style="color:#afdda0"><?=$activeMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/unsubscribed?showing=<?=$showing ?>&param=<?=$param ?>">Leiratkozott</a></span><div style="color:#cea09c"><?=$unsubscribedMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/error?showing=<?=$showing ?>&param=<?=$param ?>">Hibás</a></span><div style="color:#cea09c"><?=$errorMemberCount ?></div></td>
				<td class="tabOffMiddle" nowrap><span><a href="<?=url::base() ?>pages/listdetail/index/prereg?showing=<?=$showing ?>&param=<?=$param ?>">Aktiválás elött</a></span><?=$preregMemberCount ?></td>
				<td class="tabOnLeft" nowrap><div><span>Törölt</span><?=$deletedMemberCount ?></div></td>
				<td class="tabOnRight" nowrap>&nbsp;</td>            
            
            <?php endif; ?>
            
            
            
            <td width="100%" align="right" class="tableHeaderCap">
            
				<a href="<?=url::base() ?>pages/listexport/export" class="guibutton" id="export_click">Export</a> 
				
            
            <?php /* 
				<form action="<?=url::base().url::current(true)?>#table" method="post">
				<?php 
					if(isset($_POST['searchkeyword'])) $keyword = $_POST['searchkeyword'];
					else $keyword = ""; 

					if(isset($_POST['searchfield'])) $searchfield = $_POST['searchfield'];
					else $searchfield = "email";					
					
				?>
				<table border="0" cellpadding="0" cellspacing="0" class="searching">
				<tr>
					<td colspan="3" class="searchTitle">Keresés:</td>
				</tr>
                <tr>	
                    <td>
						<select name="searchfield">
								<option value="email" <?php echo ($searchfield == "email") ? 'selected="selected"' : ""; ?>>E-mail cím</option>
								<option value="regdatum" <?php echo ($searchfield == "regdatum") ? 'selected="selected"' : ""; ?>>Regisztráció dátuma</option>			
							<?php foreach($gridFields as $f):?>
								<?php if($searchfield == $f->reference):?>
									<option value="<?=$f->reference?>" selected="selected"><?=$f->name?></option>
								<?php else:?>
									<option value="<?=$f->reference?>"><?=$f->name?></option>
								<?php endif;?>
							<?php endforeach;?>
						</select>
					</td>
                    <td>	
						<input type="text" name="searchkeyword" id="search" value="<?=$keyword?>" class="searchField input_text">
                    </td>
					<td>
                    <div class="mybutton">
                        <button type="submit" class="button" style="padding:3px 6px 3px 8px;margin:0px;">
                            <img src="<?=$base.$img?>icons/search.png" width="16" height="16" alt=""/>
                        </button>
                        <div style="clear:both"></div>
                    </div>
                    </td>
				</tr>
				<tr>
					<td colspan="3" align="right">
                			<?php if(isset($_POST['searchkeyword'])):?>
                			<a href="<?=url::base().url::current(true)?>" class="button">
                    		Keresés törlése
			                </a>
			                <?php else:?>
			                	&nbsp;
			                <?php endif;?>
					</td>
				</tr>

				</table>
        		</form>
				*/ ?>
			</td>
		</tr>
		</table>
		

		
		
		<?php /*TÁBLÁZAT**********************************************************************/ ?>
		<table cellpadding="0" cellspacing="0" width="100%" class="tableTabsHeader">
		<tr class="noHighlight">
			<?php /*EMAIL**********************************************************************/ ?>
			<?php if($orderby=="email" && $order=="asc"): ?>
		
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/email/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
						E-mail cím
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="email" && $order=="desc"): ?>		
			
				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/email/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						E-mail cím
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th class="tabHeaderLeft">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/email/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						E-mail cím
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
			<?php /*EMAIL**********************************************************************/ ?>
			
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			<?php foreach($gridFields as $gf): ?>		
			
					<?php if($orderby==$gf->reference && $order=="asc"): ?>
				
						<th>
							<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/<?=$gf->reference ?>/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>
					
					<?php elseif($orderby==$gf->reference && $order=="desc"): ?>		
					
						<th>
							<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/<?=$gf->reference ?>/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
							</a>
						</th>					
							
					<?php else: ?>		
		
						<th>
							<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/<?=$gf->reference ?>/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
								<?=$gf->name ?>
								<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
							</a>
						</th>								
					
					<?php endif; ?>
			
			<?php endforeach; ?>
			<?php /*EGYEDI MEZŐK**********************************************************************/ ?>
			
				<th nowrap="nowrap" class="">
					Címkék						
				</th>			
			
			<?php /*REG DÁTUMA************************************************************************/ ?>

						<?php 
						if($memberStatus == "unsubscribed"){
							$datumsz = "Leir. Dátuma";
						}else{
							$datumsz = "Fel. Dátuma";
						}
						
						?>
							
			<?php if($orderby=="reg_date" && $order=="asc"): ?>
		
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/reg_date/desc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés desc szerint">
						<?=$datumsz?>						
						<img src="<?=$base.$img?>icons/sort-asc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>
			
			<?php elseif($orderby=="reg_date" && $order=="desc"): ?>		
			
				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/reg_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						<?=$datumsz?>
						<img src="<?=$base.$img?>icons/sort-desc.png" width="9" height="8" class="sortIcon" />
					</a>
				</th>					
					
			<?php else: ?>		

				<th nowrap="nowrap" class="tabHeaderRight cellRight">
					<a href="<?=url::base() ?>pages/listdetail/index/<?=$memberStatus ?>/reg_date/asc?showing=<?=$showing ?>&param=<?=$param ?>#table" title="Rendezés asc szerint">
						<?=$datumsz?>
						<img src="<?=$base.$img?>_space.gif" width="9" height="8" class="sortIcon" />
					</a>
				</th>								
			
			<?php endif; ?>
        	<?php /*REG DÁTUMA**********************************************************************/ ?>
        	
        	
		</tr>
		
		
		<?php if(isset($_POST['searchfield'])):?>
				<tr id="" style="background-color:#fafafa;" >
					<td class="tabRowLeft" colspan="<?php echo sizeof($gridFields)+2;?>" align="center" >A keresés feltételeinek megfelelő <strong><?=sizeof($members)?></strong> listatag:</td>
				</tr>		
		<?php endif;?>
		
		<?php foreach($members as $m): /****************************************TAGOK*/?>
				<tr id="<?=$m->id ?>" >
					<td class="tabRowLeft"><a href="<?=url::base() ?>pages/listdetail/selectmember/<?=$m->id ?>"><?=$m->email ?></a></td>
					<?php 
						foreach ($gridFields as $gf){
							$ref = $gf->reference;
							echo '<td>'.$m->$ref.'</td>';
						}
					?>
					<td class="tabRowLeft">
						<ul class="table-tags" style="border:0;background:0;">
						<?php foreach(Omm_tag_Model::getTagsForMember($m->id)  as $tag ): 
							
							if(strlen($tag->name) > 10) {
								$tagn = string::trimName($tag->name,8, '.');
							}else{
								$tagn = $tag->name;
							}
						
						?>
							<li class="as-selection-item" style=""><a href="#" title="<?=$tag->name ?>"><?=$tagn ?></li>
						<?php endforeach; ?>								
						</ul>
					</td>
					<td class="tabRowRight cellRight"><span><?=$m->reg_date ?></span></td>
				</tr>
		<?php endforeach;  /****************************************TAGOK*/?>
			        
		</table>
		
			<?php /*TÁBLÁZAT**********************************************************************/ ?>
		
		<table cellpadding="0" cellspacing="0" width="100%" class="tableFooter">
		<tr>
			<td class="footerLeft">&nbsp;</td>
			<td class="footerRight" align="right" valign="top">&nbsp;</td>
		</tr>
		</table>
        <?=$pagination ?>
        
        <?php endif; ///táblázat ?>
        
		</div> <!--leftcol end-->

	
	<div id="rightcol">
		<div id="options">
            <div class="mybutton" style="float:left;padding-bottom:30px;">    
                <a href="<?=url::base() ?>pages/memberdetail" class="button">
                    <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                    Új feliratkozó hozzáadása
                </a>
			<div style="clear:both"></div>	
            </div>	

			<div style="clear:both"></div>
			
			<div class="bghighlight"><h3 class="sidebar">Lista menedzselése</h3></div>
			<dl class="icon-menu">		
				
                <dt><a href="<?=url::base() ?>pages/listadd/first" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/listadd/first" id="addCustomFieldLink">Lista adatainak módosítása</a></dd>
				<div class="clear"></div>
                
                <dt><a href="<?=url::base() ?>pages/listadd/second" id="addCustomFieldIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Custom fields" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/listadd/second" id="addCustomFieldLink">Lista mezőinek módosítása</a></dd>
				<div class="clear"></div>
				
				<dt><a href="<?=url::base() ?>pages/groupoverview" id="segmentsIcon"><img src="<?=$base.$img?>icons/segments.png" width="16" height="16" alt="Segments" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/groupoverview" id="segmentsLink">Csoportok</a></dd>
				<div class="clear"></div>
				
				
        		<?php /* 
				<dt><a href="#" id="removeIcon"><img src="<?=$base.$img?>icons/delete.png" width="16" height="16" alt="Remove subscribers" /></a></dt>
				<dd><a id="deleteMembers" href="Javascript:;">Összes feliratkozó törlése</a></dd>
				<div class="clear"></div>
				*/ ?>
            </dl>
			<div class="bghighlight"><h3 class="sidebar">Feliratkozás - Leíratkozás</h3></div>
			<dl class="icon-menu">		
				<dt><a href="#" id="createFormIcon"><img src="<?=$base.$img?>icons/add-form.png" width="16" height="16" alt="Create a subscribe form" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/formoverview" id="createFormLink">Űrlapok karbantartása</a></dd>
				<div class="clear"></div>
				<dt><a href="#" id="subscribeProcessIcon"><img src="<?=$base.$img?>icons/subscriber-go.png" width="16" height="16" alt="Customize subscribe process" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/listsubscribeprocess" id="subscribeProcessLink">Feliratkozási folyamat beállítása</a></dd>
				<div class="clear"></div>

				<dt><a href="#" id="unsubscribeFormIcon"><img src="<?=$base.$img?>icons/list_links.gif" width="16" height="16" alt="Control landing pages, emails, etc." /></a></dt>
				<dd><a href="<?=url::base() ?>pages/listconnections"id="unsubscribeFormLink">Listák összekapcsolása</a></dd>
				<div class="clear"></div>
			</dl>
			
			<div class="bghighlight"><h3 class="sidebar">Egyéb</h3></div>

			<dl class="icon-menu">		
				<dt><a href="#" id="createFormIcon"><img src="<?=$base.$img?>icons/vcard.png" width="16" height="16" alt="Értesítések beállításai" /></a></dt>
				<dd><a href="<?=url::base() ?>pages/listnotifications" id="createFormLink">Értesítések beállításai</a></dd>
				<div class="clear"></div>
			</dl>
			
        </div> <!--options end-->
    <div class="clear"></div> 
    </div> <!--rightcol end-->

    <div class="clear"></div>
    </div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->