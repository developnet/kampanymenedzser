<?php 
if($letter->timing_type == "" || empty($letter->timing_type)){
	$disabled = true;
}else{
	$disabled = false;//
}
?>
<script>

var MEMBERS = new Array();
var MEMBERS_SAVE = new Array();
var SELECTED = new Array();

function pageselectCallback(page_index, jq){

    // Get number of elements per pagionation page from form
    var items_per_page = 20;
    var max_elem = Math.min((page_index+1) * items_per_page, MEMBERS.length);
    var newcontent = '';
    
    // Iterate through a selection of the content and build an HTML string
    for(var i=page_index*items_per_page;i<max_elem;i++){

    	if( $.inArray(MEMBERS[i]['email'], SELECTED) == -1){
    		newcontent += '<li><input type="checkbox" rel="' + MEMBERS[i]['email'] + '" class="recipCheck"/><a href="/pages/memberdetail/index/'+MEMBERS[i]['id']+'" target="_blank"><span class="name"> ' + MEMBERS[i]['name'] + '</span> <span class="email">&lt;' + MEMBERS[i]['email'] + '&gt;</span></a></li>';	 
        }else{
        	newcontent += '<li class="selected"><input type="checkbox" rel="' + MEMBERS[i]['email'] + '" class="recipCheck" checked="checked"/><span class="name">' + MEMBERS[i]['name'] + '</span> <span class="email">&lt;' + MEMBERS[i]['email'] + '&gt;</span></li>';
        }

        
    }

    
    // Replace old content with new content 
    $('#showResult').html(newcontent);

    $('#selectedEmails').html(SELECTED.length);
    
	  $('.recipCheck').click(function(){

		  	$("#kijelolesAlert").html("");
			var email = $(this).attr("rel");
			
			if ( $(this).parent().hasClass("selected") ){
				$(this).parent().removeClass("selected");

				SELECTED = $.grep(SELECTED, function(value) {
			        return value != email;
			      });
				
			}else{

				if(SELECTED.length == 20){ 
					$("#kijelolesAlert").html("Egyszerre csak 20 db e-mail jelölhető ki!");
					$(this).attr("checked","");
					$(this).parent().removeClass("selected");
				}else{
					
					$(this).parent().addClass("selected");
					
					SELECTED.push(email);
				}
				

				
			}

		$('#selectedEmails').html(SELECTED.length);
		      
	    });

    // Prevent click eventpropagation
    return false;
    
}

$(function(){


	$('#searchbox').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);

		 if(code == 13) { //Enter keycode

			var keyword = $("#searchbox").val();

			MEMBERS = new Array();
			
			if(keyword == ""){
				
				MEMBERS = MEMBERS_SAVE;
				
			}else{
				$.each(MEMBERS_SAVE, function() {

					if(this['email'].toUpperCase().indexOf(keyword.toUpperCase()) != -1 || this['name'].toUpperCase().indexOf(keyword.toUpperCase()) != -1){

						MEMBERS.push(this);
					}
					
				  });
			}
			

		 
			 
            var num_entries = MEMBERS.length;
            // Create pagination element
            $("#Pagination").pagination(num_entries, {
                num_edge_entries: 2,
                num_display_entries: 3,  
                callback: pageselectCallback,
                items_per_page:20,
        		prev_text:"Előző",
        		next_text:"Következő" 
            });

            			
			   
		 }
		});
	
	$(".emailokKizarasa").click(function(){

		var rel = <?=$letter->id?>;	

		$.get(URL_BASE+"api/common/addEmailConditions/"+rel+"/"+SELECTED, function(data){
			if(data == "OK"){
				location.reload(); 
			}

		});
			
	});
	
	$(".osszesKijeloles").click(function(){

		$.each(MEMBERS, function() {
			SELECTED.push(this['email']);
		  });
		
		$(".recipCheck").each(function(){
			$(this).parent().addClass("selected");
			$(this).attr("checked","checked");
		});
		
		$('#selectedEmails').html(SELECTED.length);

	});

	$(".egyiksemKijeloles").click(function(){
		$("#kijelolesAlert").html("");
		SELECTED = new Array();
		
		$(".recipCheck").each(function(){
			$(this).parent().removeClass("selected");
			$(this).attr("checked","");
		});
		
		$('#selectedEmails').html(SELECTED.length);

	});
	
	var reciplist_dialog = function(){ 
		var dialog = $("#reciplist_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};

	 
	$(".reciplistClick").click(function(){
		var rel = <?=$letter->id?>;			
		SELECTED = new Array();
		$("#searchbox").val("");
		reciplist_dialog();

		$('#reciplist_dialog').dialog('open');
		$('#reciplist_dialog').show();
		
		$("#showResult").html("Címzettek lisájának betöltése..." + "<span class=\"import_loading\"><img src=\"<?=$base.$img ?>ajax-loader.gif\"  alt=\"\" align=\"\"/></span>");
		
		$.get(URL_BASE+"api/common/reciplist/"+rel+"/0", function(data){

			MEMBERS = eval(data);

			$.each(MEMBERS, function() {
				MEMBERS_SAVE.push(this);
			});
			
			
			
            var num_entries = MEMBERS.length;
            // Create pagination element
            $("#Pagination").pagination(num_entries, {
                num_edge_entries: 2,
                num_display_entries: 3,  
                callback: pageselectCallback,
                items_per_page:20,
        		prev_text:"Előző",
        		next_text:"Következő" 
            });
			
		});				
		
		
	});

	
	$("#addEmailLink").click(function(){
		addEmail();
	});
	
	
	
	
	$('.includeListAll').click(function(){
		
		if($(this).attr('checked')){
		
			id = $(this).attr('rel');

			$('.includegroupfor_'+id).each(function (){
				$(this).removeAttr('checked');		
			});

		}else{
		
			$('.includegroupfor_'+id).each(function (){
				$(this).attr('checked','checked');
			});		
		
		}
	
	});
	
	
	$('.excludeProductAll').click(function(){
		
		if($(this).attr('checked')){
		
			id = $(this).attr('rel');

			$('.excludeproductfor_'+id).each(function (){
				$(this).removeAttr('checked');		
			});

		}else{
		
			$('.excludeproductfor_'+id).each(function (){
				$(this).attr('checked','checked');
			});		
		
		}
	
	});	
	
	
	$('.excludeproductfcheck').click(function(){
		id = $(this).attr('rel');
		if($(this).attr('checked')){
			
			$('#excallinproduct_'+id).removeAttr('checked');
			 
		}else{
		
			van = false;
			
			$('.excludeproductfor_'+id).each(function (){
				
				if($(this).attr('checked')){
					van = true;
				}
						
			});			
				
		
			if(van == false){
				$('#excallinproduct_'+id).attr('checked','checked');
			}
		
		}
	
	});	
	
	$('.includeProductAll').click(function(){
		
		if($(this).attr('checked')){
		
			id = $(this).attr('rel');

			$('.includeproductfor_'+id).each(function (){
				$(this).removeAttr('checked');		
			});

		}else{
		
			$('.includeproductfor_'+id).each(function (){
				$(this).attr('checked','checked');
			});		
		
		}
	
	});	
	
	
	$('.includeproductfcheck').click(function(){
		id = $(this).attr('rel');
		if($(this).attr('checked')){
			
			$('#incallinproduct_'+id).removeAttr('checked');
			 
		}else{
		
			van = false;
			
			$('.includeproductfor_'+id).each(function (){
				
				if($(this).attr('checked')){
					van = true;
				}
						
			});			
				
		
			if(van == false){
				$('#incallinproduct_'+id).attr('checked','checked');
			}
		
		}
	
	});	
	
	$('.includegroupfcheck').click(function(){
		id = $(this).attr('rel');
		if($(this).attr('checked')){
			
			$('#incallinlist_'+id).removeAttr('checked');
			 
		}else{
		
			van = false;
			
			$('.includegroupfor_'+id).each(function (){
				
				if($(this).attr('checked')){
					van = true;
				}
						
			});			
				
		
			if(van == false){
				$('#incallinlist_'+id).attr('checked','checked');
			}
		
		}
	
	});
	
	$('.excludeListAll').click(function(){
		if($(this).attr('checked')){
		
			id = $(this).attr('rel');

			$('.excludegroupfor_'+id).each(function (){
				$(this).removeAttr('checked');		
			});
				

		}else{
		
			$('.excludegroupfor_'+id).each(function (){
				$(this).attr('checked','checked');
			});			
		
		}
	
	});
	
	
	$('.excludegroupfcheck').click(function(){
		id = $(this).attr('rel');
		if($(this).attr('checked')){
			
			$('#exallinlist_'+id).removeAttr('checked');
			 
		}else{
		
			van = false;
			
			$('.excludegroupfor_'+id).each(function (){
				
				if($(this).attr('checked')){
					van = true;
				}
						
			});			
				
		
			if(van == false){
				$('#exallinlist_'+id).attr('checked','checked');
			}
		
		}
	
	});	

	var addEmail = function(){
		email = $("#emailadd").val();
		
		if(email == ""){
			alert("Adjon meg egy e-mail címet!");
			$("#emailadd").focus();
			return false;
		}
		
		if(checkmail(email)){
			
			$('#addemailForm').submit();
			
		
		}else{
			alert("Adjon meg egy helyes e-mail címet!");
		}
		
	}


	var checkmail = function checkemail(email){
		var filter=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if (filter.test(email)){
			return true;
		}	
		else{
			return false;
		}

	}

////////////////////////////////////////////

	$('.saveandexit').click(function(){

		<?php if($disabled):?>

		window.location = '<?=url::base() ?>pages/letterdetail/draft';
		
		<?php else:?>

		$('#exitinput').val('true');
		document.forms['formtosubmit'].submit();
		return false;		
		
		<?php endif;?>		

	});

	$('.toprevpage').click(function(){

		<?php if($disabled):?>

		window.location = '<?=url::base() ?>pages/letterdetail/timing';
			
		<?php else:?>

		//var answer = confirm("Menti a cimzés feltételeit?")
		
		//if(answer){
			$('#exitinput').val('toprevpage');
			document.forms['formtosubmit'].submit();
			return false;
		//}else{
			//window.location = '<?=url::base() ?>pages/letterdetail/timing';
			//return false;
		//}			
		
		<?php endif;?>

	});	
	
	
	var listadd_dialog = function(){ 
		var dialog = $("#listadd_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	
	
	$("#listadd_click").click(function(){
		listadd_dialog();					
		$('#listadd_dialog').dialog('open');
		$('#listadd_dialog').show();		
		
	});
	
	var listadd_dialog_minus = function(){ 
		var dialog = $("#listadd_dialog_minus").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	
	
	$("#listadd_click_minus").click(function(){
		listadd_dialog_minus();					
		$('#listadd_dialog_minus').dialog('open');
		$('#listadd_dialog_minus').show();		
		
	});	




	var tagadd_dialog = function(){ 
		var dialog = $("#tagadd_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	
	
	$("#tagadd_click").click(function(){
		tagadd_dialog();					
		$('#tagadd_dialog').dialog('open');
		$('#tagadd_dialog').show();		
		
	});
	
	var tagadd_dialog_minus = function(){ 
		var dialog = $("#tagadd_dialog_minus").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	

	$("#productadd_click").click(function(){
		productadd_dialog();					
		$('#productadd_dialog').dialog('open');
		$('#productadd_dialog').show();		
		
	});	


	var productadd_dialog = function(){ 
		var dialog = $("#productadd_dialog").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	
	$("#productadd_click_minus").click(function(){
		productadd_dialog_minus();					
		$('#productadd_dialog_minus').dialog('open');
		$('#productadd_dialog_minus').show();		
		
	});	


	var productadd_dialog_minus = function(){ 
		var dialog = $("#productadd_dialog_minus").dialog({
		bgiframe: false,
		resizable: false,
		width:500, 
		modal: false,
		closable:true,
		position: ['center','center'],
		overlay: {
			backgroundColor: '#000',
			opacity: 0.2
		}
	}); 
	};	
	
	
	$("#tagadd_click_minus").click(function(){
		tagadd_dialog_minus();					
		$('#tagadd_dialog_minus').dialog('open');
		$('#tagadd_dialog_minus').show();		
		
	});	


});


</script>

<style>
#showResult{
	list-style: none;
	margin:0 !important;
	padding:0 !important;
	height:420px;
}

#showResult li.selected{
	background-color:#faf7c7;	
}

#showResult li{
	list-style: none;
	height:20px;
	border-bottom: 1px solid #d2d2d2;
	background-color:#ffffff;
}

#showResult li span{
	line-height:20px;
}

#showResult li .recipCheck{
	margin-right: 10px;
	margin-top: 2px;
}

#showResult li .name{
	font-weight:bold;
}

#showResult li .email{

}
  
.osszesKijeloles{
	color:#227FAF !important;
}
.egyiksemKijeloles{
	color:#227FAF !important;
}

#selectedEmails{
	font-weight:bold;
}

#kijelolesAlert{
	font-weight:bold;
	color:red;
}
        	.plus_list{
        		margin:5px;
        		border-bottom: 1px solid #d2d2d2;
        	}
        	.plus_list_groups{
        		list-style: none;
        		margin-left:20px;
        	}
			.plus_list_groups li{
        		list-style: none;
        	
        	} 
        	
			div.segmentDividerNo {
				padding: 12px 0 6px 0px;
				text-align: left;
			}        	
        	     
        	div.segmentDivider {
				padding: 12px 0 6px 0px;
				text-align: left;
			}
</style>


  <div id="productadd_dialog" title="Termék hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#75ABCD">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_productform">Hozzáad</a>
		        </div> 
		        <strong id="">Termék hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi termékhez kapcsolódó szereplőknek ne kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="" id="add_productform">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Termék</label>
									<select name="productsForInclude" style="width:300px">
										<?php foreach($client->products() as $p): //?>
										<option value="<?=$p->id ?>"><?=$p->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>

  <div id="productadd_dialog_minus" title="Termék hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#FF8585">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_productform_minus">Hozzáad</a>
		        </div> 
		        <strong id="">Termék hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi termékhez kapcsolódó szereplőknek ne kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="" id="add_productform_minus">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Termék</label>
									<select name="productsForExclude" style="width:300px">
										<?php foreach($client->products() as $p): //?>
										<option value="<?=$p->id ?>"><?=$p->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>



  <div id="tagadd_dialog" title="Címke hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#75ABCD">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_tagform">Hozzáad</a>
		        </div> 
		        <strong id="">Címke hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi címével rendelkező szereplőknek kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="" id="add_tagform">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Címke</label>
									<select name="tagsForInclude">
										<?php foreach($client->getTags() as $tag): //?>
										<option value="<?=$tag->id ?>"><?=$tag->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>

  <div id="tagadd_dialog_minus" title="Címke hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#FF8585">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_tagform_minus">Hozzáad</a>
		        </div> 
		        <strong id="">Címke hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi címkével rendelkező szereplőknek ne kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="" id="add_tagform_minus">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Címke</label>
									<select name="tagsForExclude">
										<?php foreach($client->getTags() as $tag): //?>
										<option value="<?=$tag->id ?>"><?=$tag->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>





  <div id="listadd_dialog" title="Lista hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#75ABCD">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_listform">Hozzáad</a>
		        </div> 
		        <strong id="">Lista hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi listán szereplőknek kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="" id="add_listform">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Lista</label>
									<select name="listsForInclude">
										<?php foreach($lists as $list): //?>
										<option value="<?=$list->id ?>"><?=$list->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>

  <div id="listadd_dialog_minus" title="Lista hozzáadása" style="display:none">
		<div id="">
			<div class="subscriberSnapshotHeadEdit" style="text-align:left;background-color:#FF8585">
		        <div class="mybutton" style="float:right;padding-top:5px;">    
		        	<a href="javascript:;" class="guibutton add guisubmit" id="saveProductButton" rel="add_listform_minus">Hozzáad</a>
		        </div> 
		        <strong id="">Lista hozzáadása a feltételekhez</strong>
				<span style="color:black">Az alábbi listán szereplőknek ne kerüljön kiküldésre a levél</span>		        

			</div>
			<div class="subscriberSnapshot" id="">
				<div class="" id="">
				<div class="formBG">
					<div class="formWrapper">
						<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" id="add_listform_minus">
							<div class="formContainer">
								<div class="clearfix" style="text-align:left;vertical-align:top">
									<label>Lista</label>
									<select name="listsForExclude" style="width:380px">
										<?php foreach($lists as $list): //?>
										<option value="<?=$list->id ?>"><?=$list->name ?></option>
										<?php endforeach; ?>
									</select>				
								</div>
							</div>
								<div class="clearButton"></div>
						</form>
					</div>
				</div>
				</div>				
			</div>	
		</div>
	</div>

<!-- CONTENT -->
    
    <div id="reciplist_dialog" title="Címzettek listája" style="display:none">
		<div id="reciplist_html">
		     <div id="Pagination" class="pagination"></div>
			<br style="clear:both;" />
			<a  style="display:block;width:200px;float:left" href="Javascript:;" class="egyiksemKijeloles">kijelölés megszüntetése</a>
			
			<div style="float:right;text-align:right;width:220px;margin-bottom:2px">
				Keresés: <input type="text" id="searchbox" />
			</div>
			 
			<ul id="showResult" style="clear:both"></ul>
			
			<div class="mybutton" style="margin-top:5px">    
        		<a href="javascript:;" class="button emailokKizarasa">
            	<img src="<?=$base.$img ?>icons/email-delete.gif" alt=""/> 
            		Kijelöltek kizárása a címzettekből
        		</a>
        	</div>
        	<div style="float:right;text-align:right">
        		Eddig kijelölve: <span id="selectedEmails">0</span> db e-mail cím.
        		<br/>
        		<span id="kijelolesAlert"></span>
        	</div>
			<div id="reciplistResult" style="display:none"></div>
		</div>
	</div>
    
    
	<div id="singleDoubleContent">

		<div id="listMake">
        <div id="Steps">
        	<div class="Container">
            	<div class="StepsCubes">1</div>
                Levél adatai
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">2</div>
                Levél tartalma
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">3</div>
                Tesztküldés
            </div>
            <div class="Separater">></div>
            <div class="Container">
            	<div class="StepsCubes">4</div>
                Időzítés
            </div>
            <div class="Separater">></div>
            <div class="Container  Current">
            	<div class="StepsCubes  Current">5</div>
                Címzési feltételek
            </div>
        </div>
        <h1 class="step1">5. lépés: Címzési feltételek megadása</h1>
        <p style="padding-left:20px">
        	<strong>Levél neve:</strong> <?=$letter->name ?><br/>
        	<strong>Levél tárgya:</strong> <?=$letter->subject ?><br/>
        </p>
                
        </div>
	
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		<td width="42%">
        <div class="mybutton">    
        <a href="javascript:;" class="button toprevpage">
            <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
            Vissza az időzítéshez
        </a>
        </div>
        </td>
		<td>
        <div class="mybutton">    
        <a href="javascript:;" class="button saveandexit">
            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
            Ment és kilép
        </a>
        </div>
        </td>
		<td width="42%">
			&nbsp;       
        </td>
		</tr>
		</table>  	
        
	</div> <!--singleDoubleContent end-->
    
    
    <div class="twocol">            
    <div id="content">
    
    <div id="leftcol">
		
		<?php if($disabled):?>
		
		<div id="bigAlert">	
            <h1>A címzési feltételek megadásához előbb időzítse be a levelet!</h1>
            <p>A levél időzítési beálításait <a href="/pages/letterdetail/timing">itt</a> tudja megadni.</p>
          </div> 
		
		
		<?php else:?>
		
		
		<form action="<?=url::base() ?>pages/letterdetail/recipients" method="post" name="formtosubmit" id="formtosubmit"> 
		<input type="hidden" name="exit" value="false" id="exitinput"/>
        
            <div class="mybutton" style="margin:auto;width:200px;text-align.center;margin-bottom:10px">
            	<a href="JavaScript:;" class="guibutton save submita" rel="formtosubmit" id="saveChangesBtn">Mentés és újraszámolás</a>
              	<div style="clear:both"></div>
            </div>
                    
        <div style="width:45%;float:left;padding:10px;background-color:#deedf7;border-radius: 5px;">
        
	        <div class='segmentDivider'><span>Az alábbiakban szereplőknek kerüljön kiküldésre:</span></div>
	        
	
	        <div class='segmentDivider'><span>Listák:</span>&nbsp;&nbsp;
				<?php if(sizeof($listsIncludes) > 0 && $letter->timing_type == "relative"):?>
				<a href="JavaScript:;" class="guibutton add" rel="" id="" title="Relatív levélnek csak egy lista lehet a címzettje" style="color:#d2d2d2">Hozzáad</a>
				<?php else:?>
				<a href="JavaScript:;" class="guibutton add" rel="" id="listadd_click">Hozzáad</a>
				<?php endif;?>	        
	        	
	        	
	        </div>
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($listsIncludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='includeLists[]' value="<?=$lc->id ?>" />
	        		<h4><input class="includeListAll" type="checkbox" id="incallinlist_<?=$lc->id ?>" name="incallinlist_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>" <?php echo ($lc->testCondition($letter->id,'plus') == '1') ? 'checked="checked"' : '' ; ?>/> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteListConditionInclude/<?=$lc->id ?>" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        		<ul class="plus_list_groups">
						<?php foreach($lc->groups() as $g) : ?>
						<li class="segmentList">
							<input type="checkbox" name="includeGroups[]" value="<?=$g->id ?>" class="includegroupfor_<?=$lc->id ?> includegroupfcheck" rel="<?=$lc->id ?>"  <?php echo (in_array($g->id,$groupsPlus)) ? 'checked="checked"' :'' ; ?> /> <label class="segment" ><?=$g->name ?> </label>
						</li>	
						<?php endforeach; ?>        		
	        		</ul>
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>
	        
	        	<div style="cler:both"></div>
	        	
			</div>
	
	        <div class='segmentDivider'><span>Címkék:</span>&nbsp;&nbsp;<a href="JavaScript:;" class="guibutton add" rel="" id="tagadd_click">Hozzáad</a></div>
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($tagsIncludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='includeTags[]' value="<?=$lc->id ?>" />
	        		<h4><input class="includeListAll" type="checkbox" id="incallinlist_<?=$lc->id ?>" name="incallinlist_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>" checked="checked"/> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteTagCondition/<?=$lc->id ?>/plus" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>	        
	        
	        	<div style="cler:both"></div>
	        	
			</div>
	
	        <div class='segmentDivider'><span>Termékek:</span>&nbsp;&nbsp;<a href="JavaScript:;" class="guibutton add" rel="" id="productadd_click">Hozzáad</a></div>
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($productsIncludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='includeProducts[]' value="<?=$lc->id ?>" />
	        		<h4><input class="includeProductAll" type="checkbox" id="incallinproduct_<?=$lc->id ?>" name="incallinproduct_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('plus',$lc->id,'all') == '1') ? 'checked="checked"' : '' ; ?> /> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteProductCondition/<?=$lc->id ?>/plus" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        		<ul class="plus_list_groups">
						<li class="segmentList">
							<input type="checkbox" name="includeProductStat[]" value="purchased_<?=$lc->id ?>" class="includeproductfor_<?=$lc->id ?> includeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('plus',$lc->id,'purchased') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Megvásárolta</label>
						</li>	
						<li class="segmentList">
							<input type="checkbox" name="includeProductStat[]" value="ordered_<?=$lc->id ?>" class="includeproductfor_<?=$lc->id ?> includeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('plus',$lc->id,'ordered') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Megrendelte</label>
						</li>	
						<li class="segmentList">
							<input type="checkbox" name="includeProductStat[]" value="failed_<?=$lc->id ?>" class="includeproductfor_<?=$lc->id ?> includeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('plus',$lc->id,'failed') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Hibás</label>
						</li>	
	        		</ul>	        		
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>		        
	        
	        	<div style="cler:both"></div>
	        	
			</div>
		</div>
		
		
		<div style="width:45%;float:left;margin-left:5px;padding:10px;background-color: #feefef;border-radius: 5px;">
			<div class='segmentDividerNo'><span>Az alábbiakban szereplőknek ne kerüljön kiküldésre:</span></div>
			<div class='segmentDividerNo'><span>Listák:</span>&nbsp;&nbsp;<a href="JavaScript:;" class="guibutton add" rel="" id="listadd_click_minus">Hozzáad</a></div>
			
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($listsExcludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='excludeLists[]' value="<?=$lc->id ?>" />
	        		<h4><input class="excludeListAll" type="checkbox" id="exallinlist_<?=$lc->id ?>" name="exallinlist_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>" <?php echo ($lc->testCondition($letter->id,'minus') == '1') ? 'checked="checked"' : '' ; ?>/> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteListConditionExclude/<?=$lc->id ?>" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        		<ul class="plus_list_groups">
						<?php foreach($lc->groups() as $g) : ?>
						<li class="segmentList">
							<input type="checkbox" name="excludeGroups[]" value="<?=$g->id ?>" class="excludegroupfor_<?=$lc->id ?> excludegroupfcheck" rel="<?=$lc->id ?>"  <?php echo (in_array($g->id,$groupsMinus)) ? 'checked="checked"' :'' ; ?> /> <label class="segment" ><?=$g->name ?> </label>
						</li>	
						<?php endforeach; ?>        		
	        		</ul>
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>
	        
	        	<div style="cler:both"></div>
	        	
			</div>		
			
			<div class='segmentDividerNo'><span>Címkék:</span>&nbsp;&nbsp;<a href="JavaScript:;" class="guibutton add" rel="" id="tagadd_click_minus">Hozzáad</a></div>
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($tagsExcludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='excludeTags[]' value="<?=$lc->id ?>" />
	        		<h4><input class="includeListAll" type="checkbox" id="incallinlist_<?=$lc->id ?>" name="incallinlist_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>" checked="checked"/> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteTagCondition/<?=$lc->id ?>/minus" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>	        
	        
	        	<div style="cler:both"></div>
	        	
			</div>			
			
			
			
			
			<div class='segmentDividerNo'><span>Termékek:</span>&nbsp;&nbsp;<a href="JavaScript:;" class="guibutton add" rel="" id="productadd_click_minus">Hozzáad</a></div>
	        <div style="clear:both;">
	        
	        <?php 
	        $i = 1;
	        foreach($productsExcludes as $lc): 
	        ?>
	        	
	        	<div style="" class="plus_list">
	        		<input type='hidden' name='excludeProducts[]' value="<?=$lc->id ?>" />
	        		<h4><input class="excludeProductAll" type="checkbox" id="excallinproduct_<?=$lc->id ?>" name="excallinproduct_<?=$lc->id ?>" value="1" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('minus',$lc->id,'all') == '1') ? 'checked="checked"' : '' ; ?> /> <?=$lc->name ?> <a href="<?=url::base() ?>pages/letterdetail/deleteProductCondition/<?=$lc->id ?>/minus" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a></h4>
	        		<ul class="plus_list_groups">
						<li class="segmentList">
							<input type="checkbox" name="excludeProductStat[]" value="purchased_<?=$lc->id ?>" class="excludeproductfor_<?=$lc->id ?> excludeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('minus',$lc->id,'purchased') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Megvásárolta</label>
						</li>	
						<li class="segmentList">
							<input type="checkbox" name="excludeProductStat[]" value="ordered_<?=$lc->id ?>" class="excludeproductfor_<?=$lc->id ?> excludeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('minus',$lc->id,'ordered') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Megrendelte</label>
						</li>	
						<li class="segmentList">
							<input type="checkbox" name="excludeProductStat[]" value="failed_<?=$lc->id ?>" class="excludeproductfor_<?=$lc->id ?> excludeproductfcheck" rel="<?=$lc->id ?>"  <?php echo ($letter->testProductCondition('minus',$lc->id,'failed') == '1') ? 'checked="checked"' : '' ; ?>/> <label class="segment" >Hibás</label>
						</li>	
	        		</ul>	        		
	        	</div>
	        	
				        
	        <?php $i++; endforeach; ?>		        
	        
	        	<div style="cler:both"></div>
	        	
			</div>			
			
			
			
		</div>
		
		<div style="clear:both"></div>
		
	


	<!--egyedi e-mail cím kizárás-->
	
    <br />
    
    
    
            <div class="mybutton" style="margin:auto;width:200px;text-align.center">
            	<a href="JavaScript:;" class="guibutton save submita" rel="formtosubmit" id="saveChangesBtn">Mentés és újraszámolás</a>
              	<div style="clear:both"></div>
            </div>
    
    </form> 

	<div style="height:10px;"></div>
    
    

		<form id="addemailForm" action="<?=url::base() ?>pages/letterdetail/addemailconditions" method="post"> 
		<table width="95%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
		<tr class="noHighlight">
			<th width="100%" class="headerLeftRed" colspan="2">Kézi E-mail cím kizárás</th>
			<th nowrap class="headerRightRed" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
		
		<tr class="noHighlight segmentConditionAdd">
			<td width="100%" colspan="4" class="normal">
				
				<div>
				<input type="text" name="emailadd" id="emailadd" class="input_text" style="width:220px;float:left;margin-right:10px;" />
				
                
                <div class="mybutton" style="float:left">    
	            	<a href="Javascript:;" class="button slim" id="addEmailLink" onclick="">
	                	<img src="<?=$base.$img ?>icons/add2.png" alt=""/> 
	                	Újabb e-mail cím hozzáadása
	            	</a>
	            	
	            	<a href="Javascript:;" class="button slim reciplistClick" onclick="" style="margin-left:10px">
	                	<img src="<?=$base.$img ?>icons/add2.png" alt=""/> <img src="<?=$base.$img ?>icons/subscribers.png" alt=""/> 
	                	E-mail cím hozzáadása a címzettek közül
	            	</a>
	            	<div style="clear:both"></div>
	        	</div>
                
                    <!-- aas<a href="JavaScript:;" id="addEmailLink">
                        <img src="<?=$base.$img?>icons/plus.gif" width="10" height="10" alt="" class="bullet">Újabb e-mail cím felvitele
                    </a>
-->				</div>
			</td>
		</tr>		
		
		<?php $i=1; foreach($emailConditions as $e): ?>

		<tr id="r1" class="segmentCondition">
		    <td nowrap class="rowleft">
		    	<span id="1">
		    		<input type="text" name="emails[]" value="<?=$e->value ?>" class="input_text" style="width:220px;"></span>
		    </td>
			
			<td width="100%">
				
				<?php if($i != sizeof($emailConditions)): ?>
				<span class="mid"><span class='conditionDivider'> ÉS </span></span>
				<?php endif; ?>
			</td>
			
			<td nowrap class="rowRight">
				<span id="r1_delete" style="display:block">
					<a href="<?=url::base() ?>pages/letterdetail/deleteemailconditions/<?=$e->id ?>" title="Törlés" ><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Delete"></a>
				</span>
			</td>
		</tr>		
		
		<?php $i++; endforeach; ?>

		
		
		

		</table>
    
    	</form>
        <?php endif;?>
        
    </div> <!--leftcol end-->
        
        <div id="rightcol">

            <div id="options">
			
			<div class="fade-fff664" id="fade">
				<div class="segmentCount" id="fade">				
					Címzettek száma:
					<span><?=$recipNumber ?></span>
					<em>(Egyedi címzettek száma.)</em>
				</div>
			</div>
			
                <div class="mybutton">    
	            	<a href="Javascript:;" class="button reciplistClick" onclick="" style="margin-left:30px">
	                	<img src="<?=$base.$img ?>icons/subscribers.png" alt=""/> 
	                	Címzettek tallózása
	            	</a>
	            	<div style="clear:both"></div>
	        	</div>	
            

            
            </div>

		</div> <!--rightcol vége-->

		<div class="clear"></div>

		<br>
		<br>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="LettersDetailsButtonsBackground">
		<tr>
		<td width="42%">
        <div class="mybutton">    
        <a href="javascript:;" class="button toprevpage">
            <img src="<?=$base.$img ?>icons/arrow_left.png" alt=""/> 
            Vissza az időzítéshez
        </a>
        </div>
        </td>
		<td>
        <div class="mybutton">    
        <a href="javascript:;" class="button saveandexit">
            <img src="<?=$base.$img ?>icons/save.png" alt=""/> 
            Ment és kilép
        </a>
        </div>
        </td>
		<td width="42%">
			&nbsp;
        </td>
		</tr>
		</table>      	
	


	<div class="clear"></div>

	
	</div>
	     
<!-- CONTENT VÉGE -->
</div>  <!-- twcol VÉGE -->

