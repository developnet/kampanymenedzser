<script>
	$(function(){ 
	
		var SELECTED_FORM = 0;
		
  		$(".formRow").each(function(){ 
  			
			var id = $(this).attr("rel");
			
			$(this).mouseover( function() {
				//alert("#client_"+id+"_delete");
				
				$("#form_"+id+"_delete").show();
				
			} );

			$(this).mouseout( function() {
				$("#form_"+id+"_delete").hide();
			} );
 
 
 		$(".deleteForm").each(function(){
				$(this).bind('click', function(){
					SELECTED_FORM = $(this).attr('rel');
					$('.selectionName').html($(this).attr('relName'));
					deleteForm();
				});
			});
		});

		
	var dialog = function(){ 
			var dialog = $("#dialog").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	

	var dialogOk = function(){ 
			var dialog = $("#dialogOk").dialog({
			bgiframe: false,
			resizable: true,
			width:320,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		} 	

 	
 	var deleteForm = function(){ 
			dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#dialog').dialog('open');
			$('#dialog').show();	
	}					
	
	$('#deleteNoButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();		
	});


	$('#deleteYesButton').click(function(){
			$('#dialog').dialog('close');
			$('#dialog').hide();
			dialogOk();
			$('#dialogOk').dialog('open');
			$('#dialogOk').show();					
	});	 						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			window.location = '<?=url::base() ?>pages/formoverview/deleteForm/'+SELECTED_FORM;
					
	});	



	
		 
	});

	
</script>


<div id="dialog" title="Űrlap törlése" style="display:none">
	
    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Végelgesen törölni szeretné a(z) "<span class="selectionName" style="color:red"></span>" űrlapot?</strong><br />
	Az űrlap összes adata el fog veszni! A kihelyezett űrlapok nem fognak működni!
    </div>
	
    <div class="delete_dialog_short_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteYesButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen
            </button>
        </div>	
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteNoButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Nem
            </button>
        </div>	
    </div>

</div>		

<div id="dialogOk" title="Űrlap törlése" style="display:none">

    <div class="delete_dialog_question">
    <img src="<?=$base.$img ?>icons/warning.png" width="32" height="32" id="warning" alt=""/>
    <strong>Biztos hogy törli a(z) "<span class="selectionName" style="color:red"></span>" űrlapot?</strong>
    </div>
	
    <div class="delete_dialog_wide_buttons">
        <div class="mybutton" id="sendingMessage">
            <button id="deleteGoButton" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/delete.gif" alt=""/>Igen, törlöm!
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage" >
            <button id="deleteMegsemButton" type="submit" class="button" style="margin-left:20px;">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Mégsem
            </button>
       </div>	
   </div>
   
</div>	

<div class="twocol">
<!-- CONTENT -->
	<div id="content">
	
    
	<div id="leftcol">
		
		<p class="bread">
			<a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>Űrlapok
		</p>
		
		<h1>Űrlapok</h1>

        <?php if($checkUnsubProcess == false && $checkSubProcess == false):?>
        
          <div id="bigAlert">	
            <h1>A lista adatai hiányosak!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listsubscribeprocess">fel-</a> és <a href="<?=url::base() ?>pages/listunsubscribeprocess">leiratkozási</a> folyamatokat!</p>
          </div>
        
        <?php elseif($checkUnsubProcess == false && $checkSubProcess == true): ?>

          <div id="bigAlert">	
            <h1>A lista adatai hiányosak!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listunsubscribeprocess">leiratkozási</a> folyamatokat!</p>
          </div>
        
        <?php elseif($checkUnsubProcess == true && $checkSubProcess == false): ?>
        
          <div id="bigAlert">	
            <h1>A lista adatai hiányosak!</h1>
            <p>Állítsa be a listához tartozó <a href="<?=url::base() ?>pages/listsubscribeprocess">feliratkozási</a> folyamatokat!</p>
          </div>        
        
        <?php endif; ?>
		
		<p class="bottomPad"></p>
		

		<?php if(sizeof($forms) == 0): //////////HA NINCSEN MÉG CSOPORT!!!!!!!!!!!!?>
			
			<p>
				Még nincs létrehozva egy űrlap sem "<?=$_SESSION['selected_list']->name ?>" listához 
                    <div class="mybutton" style="float:left;padding-bottom:8px;">    
                        <a href="<?=url::base() ?>pages/formdetail" class="button">
                            <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                            Új űrlap létrehozása
                        </a>
                        <div style="clear:both"></div>
                    </div>
			</p>
			
		<?php else: ////////////egyéként?>
		
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeader">
			<tr class="noHighlight">
				<th width="100%" class="headerLeft"><strong>Űrlap neve</strong></th>
				<th width="200" nowrap class="cellCenter"></th>
				<th width="40" nowrap class="headerRight" align="right">&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</tr>
	
			<tr class="skinAltRow">
				<td class="rowLeft">Összes feliratkozó a "<?=$_SESSION['selected_list']->name ?>" listán</td>
			    <td nowrap class="cellCenter"><?=$membersSum ?></td>
				<td nowrap class="rowRight" align="right">&nbsp;</td>
			</tr>
			
			<?php foreach($forms as $form): ?>
			
				<tr rel="<?=$form->id ?>" class="formRow">
				   	<td class="rowLeft"><a href="<?=url::base() ?>pages/formoverview/selectform/<?=$form->id ?>" <?php echo (empty($form->note)) ? "" : 'title="'.nl2br($form->note).'"'; ?>><?=$form->name ?></a><?echo ($listDatamodForm == $form->id) ? " (Adatmódosító úrlap)" : ""?></td>
				    <td nowrap class="cellCenter"></td>
					
					<td nowrap class="rowRight" align="right">
						<span id="form_<?=$form->id ?>_delete" style="display:none">
							<a href="javascript:;" title="Form törlése" class="deleteForm" rel="<?=$form->id ?>" relName="<?=$form->name ?>"><img src="<?=$base.$img?>icons/trash.png" width="10" height="11" alt="Törlés"></a>
						</span>
					</td>
				</tr>		
			
			<?php endforeach; ?>
			</table>

		<?php endif; ?>








	
	</div> <!--leftcol end-->

	<div id="rightcol">
	
		<div id="options">

        <div class="mybutton" style="float:left;padding-bottom:8px;">    
            <a href="<?=url::base() ?>pages/formdetail" class="button">
                <img src="<?=$base.$img ?>icons/add.gif" alt=""/> 
                Új űrlap hozzáadása
            </a>
            <div style="clear:both"></div>
        </div>
	

	
		</div>
	
	</div> <!--rightcol end-->
	
	
    
    
    <div class="clear"></div>
	</div>
<!-- CONTENT VÉGE -->
</div> <!--twocol end-->