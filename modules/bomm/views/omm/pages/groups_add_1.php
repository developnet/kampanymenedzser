
<!-- CONTENT -->

<div id="content">
		
		<form name="rulesForm" action="<?=url::base() ?>pages/groupadd/first" method="post">
		
		
		<p class="bread"><a href="<?=url::base() ?>pages/listoverview">Hírlevél-listák</a><span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/listdetail"><?=$_SESSION['selected_list']->name ?></a>
			<span class="breadArrow">&nbsp;</span>
			<a href="<?=url::base() ?>pages/groupoverview" id="segmentsBreadcrumb">Csoportok</a>
			<span class="breadArrow">&nbsp;</span>Új csoport létrehozása
		</p>

		
		<h1 class="extraBottomPad">Új csoport létrehozása</h1>

		<?=$errors ?>
		
		<div class="formBG">
		<div class="formWrapper">	

			<h3>Csoport neve</h3>
			<p>Adjon beszédes nevet a csoportnak, mely alapján késöbb könnyeb be tudja azonosítani a rendszerben.</p>

			<div class="formContainer">		
				<div class="clearfix">
					<label class="middle">Csoport neve</label>
					<input type="text" name="name" id="name" value="<?=$form['name'] ?>" size="50" style="width:360px" class="<?=$classes['name'] ?> input_text">
				</div>
				
                <div class="clearfix">
					<label  class="middle">Megjegyzés</label>
					<textarea class="<?=$classes['note'] ?> input_text" name="note" cols="45" rows="4"><?=$form['note'] ?></textarea>
				</div>				
				
			</div>

            <div class="mybutton">
                <button type="submit" class="button">
                    <img src="<?=$base.$img ?>icons/create-segment.png" width="16" height="16" alt="" />
					Tovább a csoport feltételeinek megadásához
                </button>
                <span class="formcancel">&nbsp;&nbsp;&nbsp;<a href="<?=url::base() ?>pages/groupoverview" id="cancelSaveChanges"><?=KOHANA::lang("bomm.cancel") ?></a></span>
            </div>
			

			<div class="clearButton"></div>
			
		</div>
		</div>
		</form>
	
	</div>



<!-- CONTENT VÉGE -->
