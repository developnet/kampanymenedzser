 $(function(){

		var eszrevetelek_dialog = function(){ 
			var dialog = $("#eszrevetelek_dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:500,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.2
			}
		}); 
		}		

		$("#eszrevetelek").click(function(){
			eszrevetelek_dialog();	

			$("#note_subject").val("");
			$("#note_type").val("");
			$("#note_message").val("");
						
			$('#eszrevetelek_dialog').dialog('open');
			$('#eszrevetelek_dialog').show();	
		});

		$("#cancelEszrevetel").click(function(){
			$('#eszrevetelek_dialog').dialog('close');
			$('#eszrevetelek_dialog').hide();	
		});		

		$("#sendEszrevetel").click(function(){

				var note_screen = $("#note_sceen").val();
				var note_subject = $("#note_subject").val();
				var note_type = $("#note_type").val();
				var note_message = $("#note_message").val();
				
				if(note_subject == ""){
					alert('Adja meg az észrevétel tárgyát!');
					return false;
				}
				
				if(note_message == ""){
					alert('Adja meg az észrevétel szövegét!');
					return false;
				}				

				$('#eszrevetelek_dialog').dialog('close');
				$('#eszrevetelek_dialog').hide();				
				
				$.post(URL_BASE+"api/common/postnote", 
						  { screen: note_screen, 
					  		subject: note_subject,
					  		type: note_type,
					  		message: note_message
					  		 }, 
						  function(data){ 
					  			$("#note_recieved").effect('bounce',{},500,callback);
					  			
						  } 
						);
										
		
		});			

		
	
	//callback function to bring a hidden box back
	function callback(){
		setTimeout(function(){
			$("#note_recieved").effect('drop',{},500, function(){} );
			
		}, 1000);
	};

		
	 
 });