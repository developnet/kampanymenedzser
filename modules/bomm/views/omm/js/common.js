function iniStuff(){
	$( ".datepicker" ).datepicker($.datepicker.regional['hu']);
	$("table.tableHeader tr:even").addClass("even");
	$("table.tableTabsHeader tr:even").addClass("even");
 	$('a[title]').qtip({ 	   
	 	   position: {
	 	      corner: {
	 	         target: 'bottomMiddle',
	 	         tooltip: 'topMiddle'
	 	      }
	 	   },
	 	   style: { 
	 	      name: 'dark', // Inherit from preset style
	 	      tip: 'topMiddle',
		 	  border: {
		 	      width: 2,
		 	      radius: 2
		 	       
		 	    },
		 	   tip: true	 	      
	 	   }
	 	   
	 	});		
	 	
	 $('a.submita').click(function(){
			$('[name="'+$(this).attr('rel')+'"]').submit();
	});
	 	
	
} 

$(function(){
	 	
	 	$("a.guisubmit_decode").click(function(event){
	 		rel = $(this).attr("rel");
	 		
	 		$('input.decode').each(function(){
	 			
	 			val = $(this).val();
	 			
	 			$(this).val(encodeURI(val));
	 		
	 		});
	 		
	 		$('#'+rel).submit();
	 	});	 	
	 	
	 	$("a.guisubmit").click(function(event){
	 		rel = $(this).attr("rel");
	 		$('#'+rel).submit();
	 	});
	 	
	 	
			$("a.load").click(function(event) {
			   event.preventDefault();
			   
			   $("#loading_takaro").show();
			   
			   window.location.href = this.href;
			});	 	
	 	
	 	
		var sugo_dialog = function(){ 
			var dialog = $("#sugo_dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:400,
			modal: false,
			closable:true,
			position: ['right','top'],
			overlay: {
				backgroundColor: '#000',
				opacity: 0.2
			}
		}); 
		}
	 
		$(".sugoClick").click(function(){
			var rel = $(this).attr('rel');			
			
			sugo_dialog();

			$('#sugo_dialog').dialog('open');
			$('#sugo_dialog').show();
			
			$("#sugo_html").html("Súgó betöltése...");
			
			$.get(URL_BASE+"api/common/sugo/"+rel, function(data){
				$("#sugo_html").html(data);
			});				
			
		});
	 
		var eszrevetelek_dialog = function(){ 
			var dialog = $("#eszrevetelek_dialog").dialog({
			bgiframe: false,
			resizable: false,
			width:500,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.2
			}
		}); 
		}

		$("#eszrevetelek").click(function(){
			eszrevetelek_dialog();	

			$("#note_subject").val("");
			$("#note_type").val("");
			$("#note_message").val("");
						
			$('#eszrevetelek_dialog').dialog('open');
			$('#eszrevetelek_dialog').show();	
		});

		$("#cancelEszrevetel").click(function(){
			$('#eszrevetelek_dialog').dialog('close');
			$('#eszrevetelek_dialog').hide();	
		});		

		$("#sendEszrevetel").click(function(){

				var note_screen = $("#note_sceen").val();
				var note_subject = $("#note_subject").val();
				var note_type = $("#note_type").val();
				var note_message = $("#note_message").val();
				
				if(note_subject == ""){
					alert('Adja meg az észrevétel tárgyát!');
					return false;
				}
				
				if(note_message == ""){
					alert('Adja meg az észrevétel szövegét!');
					return false;
				}				

				$('#eszrevetelek_dialog').dialog('close');
				$('#eszrevetelek_dialog').hide();				
				
				$.post(URL_BASE+"api/common/postnote", 
						  { screen: note_screen, 
					  		subject: note_subject,
					  		type: note_type,
					  		message: note_message
					  		 }, 
						  function(data){ 
					  			$("#note_recieved").effect('bounce',{},500,callback);
					  			
						  } 
						);
										
		
		});			

		
	
	//callback function to bring a hidden box back
	function callback(){
		setTimeout(function(){
			$("#note_recieved").effect('drop',{},500, function(){} );
			
		}, 1000);
	};

	
	
	iniStuff();
	
	server_main_timer();
	
 });
 
 
 function server_main_timer(){
		$.get(URL_BASE+"api/common/getTime", function(data){

			_data = data.split("|");	
			time = _data[3];
			day = _data[2];
			month = _data[1];
			year = _data[0];
			
			$("#server_time_main").html(year+"."+month+"."+day+" "+time);
			setTimeout('server_main_timer()',200000);
			
		});	
	}
 
 