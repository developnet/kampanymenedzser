 $(function(){
		
		
		
		
		var predata = {pre: []};		
		
		$(".tagsfield").autoSuggest(
			URL_BASE+"api/common/getTags", 
				{	
					minChars: 2,
					asHtmlID: "tags",
					neverSubmit: true,
					startText: "Adja meg a címkéket..",
					selectedItemProp: "name", 
					searchObjProps: "name",
					preFill: predata.pre
				}
			);	
	
	
		$( "#datepicker_tol" ).datepicker($.datepicker.regional['hu']);
		$( "#datepicker_ig" ).datepicker($.datepicker.regional['hu']);		
		
		$("a.trigger").click(function(){
               $(".toggle_container").toggle();
               return false; //Prevent the browser jump to the link anchorasasaa
        });
	
		$(".select_member").each(function(){
			var member_id = parseInt($(this).attr('rel'));
			
			
			if((jQuery.inArray(member_id, SELECTED_MEMBERS) == -1) || (jQuery.inArray(member_id, SELECTED_MEMBERS) == 'undefined')){
				//nincs benne
			}else{
				
				$(this).parent().parent().addClass('selected_letter_row');
				$(this).attr('checked', true);
			}			
		});	
			
		$('.selectedmembers').each(function(){
		 		$(this).html(SELECTED_MEMBERS.length);
		});
			
		$(".select_member").click(function(){
				
			var member_id = parseInt($(this).attr('rel'));	
				
			if((jQuery.inArray(member_id, SELECTED_MEMBERS) == -1) || (jQuery.inArray(member_id, SELECTED_MEMBERS) == 'undefined')){
				SELECTED_MEMBERS.push(member_id);
				$(this).parent().parent().addClass('selected_letter_row');
			}else{
				SELECTED_MEMBERS.splice(SELECTED_MEMBERS.indexOf(member_id), 1);
				$(this).parent().parent().removeClass('selected_letter_row');
			}

			saveselected();

		});	
	
	$(".select_all_member").click(function(){
		
		if($(this).is(':checked')){
		
			$(".select_member").each(function(){
				var member_id = parseInt($(this).attr('rel'));	
				if((jQuery.inArray(member_id, SELECTED_MEMBERS) == -1) || (jQuery.inArray(member_id, SELECTED_MEMBERS) == 'undefined')){
					SELECTED_MEMBERS.push(member_id);
					$(this).parent().parent().addClass('selected_letter_row');
					$(this).attr('checked', true);
				}
			});			
		
		}else{
		
			$(".select_member").each(function(){
				var member_id = parseInt($(this).attr('rel'));	
				if((jQuery.inArray(member_id, SELECTED_MEMBERS) == -1) || (jQuery.inArray(member_id, SELECTED_MEMBERS) == 'undefined')){
					//
				}else{
					SELECTED_MEMBERS.splice(SELECTED_MEMBERS.indexOf(member_id), 1);
					$(this).parent().parent().removeClass('selected_letter_row');
					$(this).attr('checked', false);
				}			
				
			});			
		
		
		}
		

		
		saveselected();
	});	
	
 	
 	$('#addtags_click').click(function(){
 		addtags();
 	});
 	
	var addtags_dialog = function(){ 
			var dialog = $("#addtagdialog").dialog({
			bgiframe: false,
			resizable: false,
			width:600,
			modal: true,
			closable:false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}); 
		}	
 	var addtags = function(){ 
			addtags_dialog();				
			//$('#dialog').html($('#tartalom').html());
			$('#addtagdialog').dialog('open');
			$('#addtagdialog').show();	
	}					
	
	$('#addtagdialog_cancel').click(function(){
			$('#addtagdialog').dialog('close');
			$('#addtagdialog').hide();		
	});


						
 
 
 	$('#deleteMegsemButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();		
	});

 	$('#deleteGoButton').click(function(){
			$('#dialogOk').dialog('close');
			$('#dialogOk').hide();
			
			
					
	});

 
 });

function ofc_resize(left, width, top, height){
	var tmp = new Array(
	'left:'+left,
	'width:'+ width,
	'top:'+top,
	'height:'+height );
	
	$("#resize_info").html( tmp.join('<br>') );
}


	function saveselected(){
			console.log(SELECTED_MEMBERS.length);
			POSTARRAY =  {  "operator": "save", 
							"selected_members": SELECTED_MEMBERS.length
							};
		
			for(var i=0;i<SELECTED_MEMBERS.length;i++){
				POSTARRAY["member_"+i] = SELECTED_MEMBERS[i]; 
			}	
			
			
			$.post("/api/common/saveSelectedMembers", POSTARRAY, 
			  function(data){ 
			  
			  	$('.selectedmembers').each(function(){
			  		$(this).html(SELECTED_MEMBERS.length);
			  	});
			  
			  });//asa
			  	
	}
	
	
	