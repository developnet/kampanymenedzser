<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<chart>
  <!-- <message bg_color="#BBBB00" text_color="#FFFFFF"><![CDATA[You can broadcast any message to chart from data XML file]]></message> -->
	<series>
		<?php foreach($labels as $key => $label): ?>
			<value xid="<?=$key ?>"><?=$label ?></value>
		<?php endforeach; ?>
	</series>
	<graphs>
		<graph gid="1">
		<?php foreach($data as $key => $d): ?>
			<value xid="<?=$key ?>"><?=$d ?></value>
		<?php endforeach; ?>
		</graph>

		<graph gid="2">
		<?php foreach($data2 as $key => $d): ?>
			<value xid="<?=$key ?>"><?=$d ?></value>
		<?php endforeach; ?>
		</graph>

 
	</graphs>
</chart>
