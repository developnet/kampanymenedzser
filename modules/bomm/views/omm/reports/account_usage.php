<table id="tree">
  
  <tr id="node-1">
    <td>Parent</td>
  </tr>
  
  <tr id="node-2" class="child-of-node-1">
    <td>Child</td>
  </tr>
  
  <tr id="node-3" class="child-of-node-2">
    <td>Child</td>
  </tr>
</table>
