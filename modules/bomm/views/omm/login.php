<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"/>
<script language="JavaScript1.2" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<title><?=$title?> - Bejelentkezés</title>
<link href="<?=Kohana::config('core.viewpath').$css ?>login.css" media="screen,projection" rel="stylesheet" type="text/css" />
<link href="<?=Kohana::config('core.viewpath').$css ?>buttons.css" media="screen,projection" rel="stylesheet" type="text/css" />

<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
	<link href="<?=Kohana::config('core.viewpath').$css ?>hireso.css" media="screen,projection" rel="stylesheet" type="text/css" />
<?php endif; ?>

<script language="JavaScript1.2">

$(function(){
	 	
	 	$("a.guisubmit_decode").click(function(event){
	 		rel = $(this).attr("rel");
	 		
	 		$('input.decode').each(function(){
	 			
	 			val = $(this).val();
	 			
	 			$(this).val(encodeURI(val));
	 		
	 		});
	 		
	 		$('#'+rel).submit();
	 	});	 	
	 	
	 	$("a.guisubmit").click(function(event){
	 		rel = $(this).attr("rel");
	 		$('#'+rel).submit();
	 	});
	 	
	 });

</script>


</head>
<body>

<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
	<h1 style="padding:0;margin:0;text-align:center"><img src="<?=Kohana::config('core.viewpath').$img ?>hireso_garbaroyal_logo.png"></h1>
<?php endif; ?>

<div id="formWrapper">

<div id="formCasing" style="padding-top:5px">

<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
	<h1 style="padding:0;margin:0;padding-bottom:5px;padding-top:10px;padding-left:1px"><img src="<?=Kohana::config('core.viewpath').$img ?>hireso_logo.png"></h1>
<?php else: ?>
	<h1 style="padding:0;margin:0;padding-bottom:5px;padding-top:10px;padding-left:1px"><img src="<?=Kohana::config('core.viewpath').$img ?>kampanymenedzser_logo.png"></h1>
<?php endif; ?>


	<div id="loginForm" style="display:block">
    <?php if($message != ""): ?>
    <p class="error">
    <?=$message?>
    </p>
    <?php endif; ?>
		<form action="<?=url::site()?>login" method="post" name="loginForm" id="loginform">
			<input	type="hidden" name="cookieexists" value="false" />
			<dl>
				<dt>
					<label for="username"><?=Kohana::lang('bomm.user')?></label>
				</dt>
				<dd>
					<input type="text" name="username" id="username" value="" class="input" />
				</dd>
				<dt>
					<label for="password"><?=Kohana::lang('bomm.password')?></label>
				</dt>
				<dd>
					<input type="password" name="password" id="password" value="" class="input" />
					<!-- <span><a href="#">Elfelejtett jelszó</a></span> --> 
				</dd>
				
				<?php
				// 
					if(isset($_GET['caprica']) && $_GET['caprica'] == "six"){
						
						$dbcore = new Database();
						$res = $dbcore->from("omm_accounts")->where('status','1')->get();
						
						
						?>
						
						<select name="km_user">
						<?php foreach($res as $ku):?>
						<option value="<?=$ku->km_user?>"><?=$ku->km_user?></option>	
						<?php endforeach;?>	
						</select>
						
						<?php 
					}
					
					if(isset($_GET['austra']) && $_GET['austra'] == "loseit"){
						
						?>
						<input type="text" name="km_user" id="km_user" value="" class="input" />
	
						
						<?php 
					}					
					
				?>
                <dt>&nbsp;</dt>
				<dd>
					<div class="mybutton">
					<a href="Javascript:;" class="guibutton guisubmit_decode load" id="" rel="loginform">Bejelentkezés</a>
                        <div style="clear:both"></div>
                    </div>        
				</dd>
			</dl>
		</form>
		
	</div>

	

</div>



</div>
<div style="text-align:center;margin-right:40px"><code style="font-size:11px">Verzió: <span><?=meta::getVersionString()?></span>,  Szerver idő: <span id="server_time_main"><?=date('Y.m.d H:i') ?></span></code></div>



</body>
</html>
