body{
	font-size:12px;
}

h1 {
border-bottom:1px solid #BECCCC;
color:#D8000C;
font-family:Georgia,"Times New Roman",Times,serif;
font-size:22px;
font-weight:bold;
line-height:1;
margin:0 0 20px;
padding:0 0 8px;
}

h2 {
font-family:Georgia,"Times New Roman",Times,serif;
font-size:20px;
font-weight:bold;
line-height:1.3;
margin:0 0 16px;
color:#D8000C;
}

h3 {
font-family:Georgia,"Times New Roman",Times,serif;
font-size:18px;
font-weight:bold;
line-height:1.3;
margin:0 0 16px;
color:#D8000C;
}

h4 {
font-family:Georgia,"Times New Roman",Times,serif;
font-size:16px;
font-weight:bold;
line-height:1.3;
margin:0 0 16px;
color:#D8000C;
}

h5 {
font-family:Georgia,"Times New Roman",Times,serif;
font-size:14px;
font-weight:normal;
line-height:1.2;
margin:0 0 16px;
color:#D8000C;
}

h6 {
font-family:Georgia,"Times New Roman",Times,serif;
font-size:14px;
font-weight:normal;
line-height:1.2;
margin:0 0 16px;
color:#D8000C;
}

p {
margin:0 0 16px;
text-align:left;
font-family:Georgia,"Times New Roman",Times,serif;
font-size:12px;
}


a {
color:#D8000C;
font-size:12px;
}

table, td, th, tr, ol, ul{
font-family:Georgia,"Times New Roman",Times,serif;
font-size:12px;
}
