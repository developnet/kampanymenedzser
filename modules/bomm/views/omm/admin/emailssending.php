<script type="text/javascript">
 $(function(){
	
	swfobject.embedSWF(
		"<?=$assets?>amcolumn/amcolumn.swf", "activityChart",
		"100%", "400", "9.0.0", "expressInstall.swf",
		{
		"settings_file" : "<?=KOHANA::config('core.report_xml')?>user_activity.xml", 
		"data_file" : "<?=url::base() ?>charts/useractivity/index/11/<?=$showing?>/<?=$param?>/<?=$km_user?>", 
		"path" : "<?=$assets?>amcolumn/",
		"preloader_color" : "#999999",
		"wmode" : "transparent",
		"wmode" : "opaque"
		},
		{"wmode" : "transparent"} );
 
 });
</script>

<h1>Kiküldések / Feliratkozók statisztika</h1>

		<p>
            
                    	<?php if($showing!="year"): ?>
                    	<a href="?showing=year&km_user=<?=$km_user?>">Éves</a>
                    	<?php else: ?>
                    	Éves
                    	<?php endif; ?>
                    	&nbsp;|&nbsp;                       
                    	<?php if($showing!="month"): ?>
                    	<a href="?showing=month&km_user=<?=$km_user?>">Havi</a>
                    	<?php else: ?>
                    	Havi
                    	<?php endif; ?>
						&nbsp;|&nbsp;
                    	<?php if($showing!="week"): ?>
                    	<a href="?showing=week&km_user=<?=$km_user?>">Heti</a>
                    	<?php else: ?>
                    	Heti
                    	<?php endif; ?>                      
						&nbsp;|&nbsp;
                    	<?php if($showing!="day"): ?>
                    	<a href="?showing=day&km_user=<?=$km_user?>">Napi</a>
                    	<?php else: ?>
                    	Napi
                    	<?php endif; ?>                        

            
                <div class="clear"></div>
            
                <div class="select">
                	<?php
                		$left = "";
                		$right = "";
                		
						if($showing == "month"){

							$left = date( "Y-m-d", strtotime( $param."-01 -1 month" ));
                			$right = date( "Y-m-d", strtotime( $param."-01 +1 month" ));	
							$left = substr($left,0,7);
							$right = substr($right,0,7);                			

						}elseif($showing == "year"){

							$left = date( "Y-m-d", strtotime( $param."-01-01 -1 year" ));
							$right = date( "Y-m-d", strtotime( $param."-01-01 +1 year" ));
							$left = substr($left,0,4);
							$right = substr($right,0,4);                									
						
						}elseif($showing == "week"){
							
							$left = date( "Y-m-d", strtotime( $param." -1 week" ));
							$right = date( "Y-m-d", strtotime( $param." +1 week" ));
														
						}elseif($showing == "day"){
							
							$left = date( "Y-m-d", strtotime( $param." -1 day" ));
							$right = date( "Y-m-d", strtotime( $param." +1 day" ));
														
						}

                	
                	?>
                	<?php if($showing != "all"): ?>
                	<a href="<?=string::add_to_query_string("param",$left) ?>">&laquo; </a> <?=" ".$param." " ?> <a href="<?=string_Core::add_to_query_string("param",$right) ?>"> &raquo; </a>
                	<?php endif; ?>
                </div>
    		
		Kiküldések<br/>
        <span>Eddig összesen: <span class="green"><?=number_format($ossz, 0, ".", " ");?></span></span><br/>
        <?php 
        	$parameters = input::instance()->get();
			$uri =  url::base().url::current().'?'.http_build_query($parameters);
        
        ?>
        <form action="<?=$uri?>" method="get">
        	<select name="km_user">
        		<?php foreach($km_users as $u):?>
        		
        			<?php if($u->km_user == $km_user):?>
        				<option value="<?=$u->km_user?>" selected="selected"><?=$u->km_user?></option>
        			<?php else:?>
        				<option value="<?=$u->km_user?>"><?=$u->km_user?></option>
        			<?php endif;?>
        			
        		<?php endforeach;?>
        	</select><input type="submit" value="választ" />
        </form>
        
        </p>		
	        
		<?=$errors ?>
	    
        <!-- GRAFIKON-->
        <div id="resize">
        	<div class="chart" id="activityChart"></div>
        </div>
        <!-- GRAFIKON-->


