<style>
#adatok{}
#notes{display:none;}

#felhasznalok{display:none;}

#demolista{display:none;}

#elofizetolista{display:none;}

#adatok_click{
	cursor:pointer;
}

#notes_click{
	cursor:pointer;
	font-size:13px;
}

#felhasznalok_click{
	cursor:pointer;
}

#demolista_click{
	cursor:pointer;
}

#elofizetolista_click{
	cursor:pointer;
}

</style>

<?php
//
?>

<script type="text/javascript">

 $(function(){
 
 	$("#adatok_click").click(function(){
 		$("#adatok").toggle();
 	});

 	 $("#notes_click").click(function(){
 		$("#notes").toggle();
 	});

 	$("#felhasznalok_click").click(function(){
 		$("#felhasznalok").toggle();
 	});
 	
 	 $("#demolista_click").click(function(){
 		$("#demolista").toggle();
 	});
 
  	 $("#elofizetolista_click").click(function(){
 		$("#elofizetolista").toggle();
 	});
 
 });

</script>

<p>
	<a href="/admin/accounts" id="osszes">vissza</a>
</p>

<h1 id="adatok_click">1. '<?=$account->km_user?>' felhasználói fiók</h1>
<div id="adatok">


	<div class="form-container"> 
 
	<form action="/admin/accounts/detail/<?=$account->id?>" method="post"> 
		<div><label for="uname" class="">Azonosító</label> <input id="km_user" type="text" name="km_user" value="<?=$account->km_user?>" class="error" disabled="disabled" /></div>
		<div></div>	
		<div><label for="lname">Csomag</label>
			<select name="omm_package_id" class="" >
		<?php foreach($packages as $p): ?>
			<option value="<?=$p->id ?>" <?php echo ($account->omm_package_id == $p->id) ? 'selected="selected"' : ''; ?>><?=$p->base_price.' - '.$p->name ?></option>
		<?php endforeach; ?>	
			</select>				
		</div>
										
		<fieldset> 
			<legend>Számlázási adatok</legend> 
				<div><label for="fname">Cégnév </label> <input id="company" type="text" name="company" value="<?=$account->company?>" size="50" /></div> 
				<div><label for="lname">Bankszámlaszám</label> <input id="bank_account" type="text" name="bank_account" value="<?=$account->bank_account?>" size="50" /></div>
				
				<div><label for="lname">Adószám</label> <input id="billing_taxnumber" type="text" name="billing_taxnumber" value="<?=$account->billing_taxnumber?>" size="50" /></div>
				<div><label for="lname">Irányítószám</label> <input id="billing_zip" type="text" name="billing_zip" value="<?=$account->billing_zip?>" size="50" /></div>
				<div><label for="lname">Város</label> <input id="billing_city" type="text" name="billing_city" value="<?=$account->billing_city?>" size="50" /></div>
				<div><label for="lname">Cím</label> <input id="billing_address" type="text" name="billing_address" value="<?=$account->billing_address?>" size="50" /></div>
		</fieldset>
			
		<fieldset> 
			<legend>Postázási adatok</legend> 
				<div><label for="fname">Név </label> <input id="posting_name" type="text" name="posting_name" value="<?=$account->posting_name?>" size="50" /></div> 
				<div><label for="lname">Irányítószám</label> <input id="posting_zip" type="text" name="posting_zip" value="<?=$account->posting_zip?>" size="50" /></div>
				<div><label for="lname">Város</label> <input id="posting_city" type="text" name="posting_city" value="<?=$account->posting_city?>" size="50" /></div>
				<div><label for="lname">Cím</label> <input id="posting_address" type="text" name="posting_address" value="<?=$account->posting_address?>" size="50" /></div>
		</fieldset>

		<fieldset> 
			<legend>Kapcsolattartó adatok</legend> 
				<div><label for="fname">Vezetéknév </label> <input id="contact_familyname" type="text" name="contact_familyname" value="<?=$account->contact_familyname?>" size="50" /></div> 
				<div><label for="lname">Keresztnév</label> <input id="contact_firstname" type="text" name="contact_firstname" value="<?=$account->contact_firstname?>" size="50" /></div>
				<div><label for="lname">E-mail</label> <input id="contact_email" type="text" name="contact_email" value="<?=$account->contact_email?>" size="50" /></div>
				<div><label for="lname">Telefon</label> <input id="contact_phone" type="text" name="contact_phone" value="<?=$account->contact_phone?>" size="50" /></div>
		</fieldset>

		<fieldset> 
			<legend>API kulcs</legend> 
				<div><code style="font-size:16px"><?=$account->apikey?></code></div> 
		</fieldset>
		<div class="buttonrow">
			<input type="submit" value="Módosítás" class="button">
			
		</div>	
	</form>
	
	</div> 
	
	<h1 id="notes_click">Megjegyzések</h1>
	<div id="notes">
		
		<table width="100%" cellspacing="0" id="">
	
		<tr id="tableHeader">
			<th class="left" style="width:100px">Felhasználó</th>
			<th class="left">Megjegyzés</th>
		</tr>
		
		<?php foreach($notes as $n):
			$style = array(
					"telefon" => "#ffb033",
					"email" => "#33cdff",
					"személyes" => "#ff44f8",
					"megjegyzés" => "#bebebe"
			);
		?>
		
		<tr class="">
			<td valign="top"><strong><?=$n->user?><br/><small><?=$n->timestamp?></small><br/><small style="color:<?=$style[$n->type]?>"><?=$n->type?></small></strong></td>
			<td><?=text::auto_p($n->note)?></td>
		</tr>	
		<?php endforeach;?>
		
		</table>	
		<div class="form-container"> 
		<form action="/admin/accounts/detail/<?=$account->id?>" method="post"> 
		<inptu type="hidden" name="omm_account_id" value="<?=$account->id?>"/>
		<fieldset> 
			<legend>Új megjegyzés</legend>
				<div><label for="fname">Felhasználó </label> 
					<select name="note_user" class="" >
		   			  		<option value="Berta Márton">Berta Márton</option>
		   			  		<option value="Bölcsföldi Árpád">Bölcsföldi Árpád</option>

					</select>
				</div> 
				<div><label for="lname">Típus</label> 
					<select name="note_type" class="" >
		   			  		<option value="telefon">telefon</option>
		   			  		<option value="email">email</option>
		   			  		<option value="személyes">személyes</option>
		   			  		<option value="megjegyzés">megjegyzés</option>
					</select>				
				</div>
				<div><label for="lname">Szöveg</label> 
					<textarea name="note_note" style="width:400px;height:50px"></textarea>
				</div>

		</fieldset>

		<div class="buttonrow">
			<input type="submit" value="Beírás" class="button">
		</div>				
		</form>
		</div>
	</div>

</div>

	<p><br/></p>
	
	<a name="users"></a>
	
	<h1 id="felhasznalok_click">2. Fiókhoz tartozó hozzáférések</h1>
	
	<div id="felhasznalok">	
	<div class="form-container"> 
	<form action="/admin/accounts/detail/<?=$account->id?>" method="post"> 

		<fieldset> 
			<legend>Új hozzáférés</legend>
				<div><label for="fname">Vezetéknév </label> <input id="user_lastname" type="text" name="user_lastname" value="" size="50" /></div> 
				<div><label for="lname">Keresztnév</label> <input id="user_firstname" type="text" name="user_firstname" value="" size="50" /></div>
				<div><label for="lname">E-mail</label> <input id="user_email" type="text" name="user_email" value="" size="50" /></div>
				<div><label for="lname">Felhasználónév</label> <input id="user_username" type="text" name="user_username" value="" size="50" /></div>
				<div><label for="lname">Jelszó</label> <input id="user_password" type="text" name="user_password" value="" size="50" /></div>
				<div><label for="lname">Adminisztrátor</label> <input id="user_admin" type="checkbox" name="user_admin" value="1" size="50" /></div>
		</fieldset>

		<div class="buttonrow">
			<input type="submit" value="Hozzáadás" class="button">
		</div>	
	</form>	
	</div>
	
	<table width="100%" cellspacing="0" id="cronjobsTable">
	
	<tr id="tableHeader">
		<th>e-mail</th>
		<th class="right">felhasználónév</th>
		<th class="right">név</th>
		<th class="right">belépések</th>
		<th class="right">utolsó belépés</th>
	</tr>
	
	<?php 
	foreach($users as $u):
		$status = $u->has(new Role_Model("accountadmin"));
	?>
	
	<tr class="">
		<td><strong><?=$u->email?><?php echo ($status) ? ' <small style="color:#FF0084">admin</small>' : "";?></strong></td>
		<td class="right"><?=$u->username?></td>
		<td class="right"><?=$u->lastname?> <?=$u->firstname?></td>
		<td class="right"><small><?=$u->logins?></small></td>
		<td class="right"><small><?=date("Y-m-d H:i:s",$u->last_login)?></small></td>
	</tr>
	
	<?php endforeach;//?>
	
	</table>	
</div>	
<p><br/></p>
<h1 id="demolista_click">3. Demosok listájára feliratás</h1>

<div id="demolista">
 
	<div class="form-container"> 
	<form action="http://kampanymenedzser.kmsrv1.hu/api/subscribe?a=HMhefQAY" method="post" class="subscribe_form">
		<input type="hidden" name="formcode" value="af95d67efd3968c9662c53c947f3897b"/>
		<input type="hidden" name="listcode" value="4c9a20ddb220920a11e6a4cf7dcd7999"/>
		
		<fieldset> 
			<legend>Adatok</legend>
				<div><label for="fname">Vezetéknév </label> <input id="vezeteknev" type="text" name="vezeteknev" value="<?=$account->contact_familyname?>" size="50" /></div> 
				<div><label for="lname">Keresztnév</label> <input id="keresztnev" type="text" name="keresztnev" value="<?=$account->contact_firstname?>" size="50" /></div>
				<div><label for="lname">E-mail</label> <input id="email" type="text" name="email" value="<?=$account->contact_email?>" size="50" /></div>
				
				<div><label for="lname">Cégnév</label> <input id="cegnev" type="text" name="cegnev" value="<?=$account->company?>" size="50" /></div>
				<div><label for="lname">Cég címe</label> <input id="ceg_cime" type="text" name="ceg_cime" value="<?=$account->billing_zip." ".$account->billing_city.", ".$account->billing_address?>" size="50" /></div>
				<div><label for="lname">Telefonszám</label> <input id="telefonszam" type="text" name="telefonszam" value="<?=$account->contact_phone?>" size="50" /></div>
				
				<div><label for="lname">Honlap</label> <input id="honlap" type="text" name="honlap" value="" size="50" /></div>
				<div><label for="lname">KM azonosito</label> <input id="km_azonosito" type="text" name="km_azonosito" value="<?=$account->km_user?>" size="50" /></div>
				<div><label for="lname">Demo lejárta</label> <input id="demo_lejarta" type="text" name="demo_lejarta" value="<?=date("Y.m.d", strtotime("+30 day")) ?>" size="50" />
					 Ma +30 nap.
				</div>
				
				
				<div><label for="lname">Login</label> <input id="login_url" type="text" name="login_url" value="http://www.kampanymenedzser.hu/start" size="50" /></div>
				<div><label for="lname">Felhasználónév</label> <input id="felhasznalonev" type="text" name="felhasznalonev" value="" size="50" /></div>
				<div><label for="lname">Jelszó</label> <input id="jelszo" type="text" name="jelszo" value="" size="50" /></div>
				
				<div><label for="lname">Honnan jött</label>
					<select name="honnan_jott" class="" >
						<optgroup label="Honnan jött">
				        			  		<option value="ifn7yABm" >honlapról</option>
				        			  		<option value="P3peQEcN" >saját</option>
				        			  		<option value="VbPAFBCG" >Jagodics</option>
				        			  		<option value="Ju1wyidP" >Agró Napló</option>
				        					
						</optgroup>
					</select>				
				</div>
				
		</fieldset>

		<div class="buttonrow">
			<input type="submit" value="Feliratás" class="button">
		</div>	
	</form>	
	</div>
</div>

<p><br/></p>
<h1 id="elofizetolista_click">4. Előfizetők listájára feliratás</h1>

<div id="elofizetolista">
 
	<div class="form-container"> 
	<form action="http://kampanymenedzser.kmsrv1.hu/api/subscribe?a=HMhefQAY" method="post" class="subscribe_form">
		<input type="hidden" name="formcode" value="b86487b338e1595ffaa56e15aba9fee0"/>
		<input type="hidden" name="listcode" value="4b77aeb49d05a4db6e5159ae8657a4a3"/>
		
		<fieldset> 
			<legend>Adatok</legend>
				<div><label for="fname">Vezetéknév </label> <input id="vezeteknev" type="text" name="vezeteknev" value="<?=$account->contact_familyname?>" size="50" /></div> 
				<div><label for="lname">Keresztnév</label> <input id="keresztnev" type="text" name="keresztnev" value="<?=$account->contact_firstname?>" size="50" /></div>
				<div><label for="lname">E-mail</label> <input id="email" type="text" name="email" value="<?=$account->contact_email?>" size="50" /></div>
				
				<div><label for="lname">Cégnév</label> <input id="cegnev" type="text" name="cegnev" value="<?=$account->company?>" size="50" /></div>
				
				<div><label for="lname">KM azonosito</label> <input id="elofizetes_azonosito" type="text" name="elofizetes_azonosito" value="<?=$account->km_user?>" size="50" /></div>
				<div><label for="lname">Előfizetés kezdete</label> <input id="elofizetes_kezdete" type="text" name="elofizetes_kezdete" value="<?=date("Y.m.d") ?>" size="50" />
					 Ma.
				</div>
				
				
				<div><label for="lname">Honnan jött</label>
					<input id="honnan" type="text" name="honnan" value="" />	
				</div>
				
		</fieldset>

		<div class="buttonrow">
			<input type="submit" value="Feliratás" class="button">
		</div>	
	</form>	
	</div>
</div>


<?php 
/*
	id  	int(11)  	 	  	Nem  	 	auto_increment  	  Különböző értékek böngészése   	  Változtat   	  Eldob   	  Elsődleges   	  Egyedi   	  Index   	 Fulltext
	km_user 	varchar(30) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	omm_package_id 	int(11) 			Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(4) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(30) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(4) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(255) 	utf8_general_ci 		Nem 			Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	code 	varchar(8) 	utf8_general_ci 		Igen 	NULL 		Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	 	varchar(32) 	utf8_general_ci 		Igen 	NULL 		Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	mod_date 	timestamp 		ON UPDATE CURRENT_TIMESTAMP 	Nem 	CURRENT_TIMESTAMP 		Különböző értékek böngészése 	Változtat 	Eldob 	Elsődleges 	Egyedi 	Index 	Fulltext
	mod_user_id

*/
?>
