<script>
$(document).ready(function(){

	$("#osszes").click(function(){
		$("#cronjobsTable tr").show();
		$("#cronjobsTable tr").removeClass("odd");
		$("table tr:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#daily").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .daily").show();
		$("table .daily:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#hourly").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .hourly").show();
		$("table .hourly:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#bounce").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .bounce").show();
		$("table .bounce:odd").addClass("odd");
		$("#tableHeader").show();
	});

	setTimeout("location.reload(true);",60000);
	setTimeout("countDown();",1000);
});

function countDown(){
	$("#countdown").html(($("#countdown").html() - 1));
	setTimeout("countDown();",1000);
}
</script>
<h1>Cron jobok ellenőrzése</h1>

<p>
	<a href="Javascript:;" id="osszes">összes</a>
	&nbsp;|&nbsp;
	<a href="Javascript:;" id="daily">daily</a>
	&nbsp;|&nbsp;
	<a href="Javascript:;" id="hourly">hourly</a>
	&nbsp;|&nbsp;
	<a href="Javascript:;" id="bounce">bounce</a>
	&nbsp;&nbsp;&nbsp;&nbsp;Utolsó frissítés: <small id="refresh"><?=date("Y.m.d H:i:s");?> ... <span id="countdown">60</span></small>
</p>

<table width="100%" cellspacing="0" id="cronjobsTable">

<tr id="tableHeader">
	<th>cronjob</th>
	<th class="right">pid</th>
	<th class="right">start</th>
	<th class="right">end</th>
</tr>

<?php foreach($cronjobs as $job):

if( strpos($job->type, "KM HOURLY") === 0) {
	$rel = "hourly";
}elseif( strpos($job->type, "KM BOUNCEDETECT") === 0){
	$rel = "bounce";
}elseif( strpos($job->type, "KM DAILY") === 0){
	$rel = "daily";
}else{
	$rel = "";
}

if($job->end == NULL){
	$rel .= " yellow";
}

?>

<tr class="<?=$rel?>">
	<td><strong><?=$job->type?></strong></td>
	<td class="right"><small><?=$job->pid?></small></td>
	<td class="right"><?=$job->start?></td>
	<td class="right"><?=$job->end?></td>
</tr>

<?php endforeach;?>
</table>