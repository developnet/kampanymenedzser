<h1>Új felhasználói fiók</h1>

	<div class="form-container"> 
 
		<form action="/admin/accounts/add" method="post"> 
			<input type="hidden" name="code" value="<?=string::random_string() ?>"/>
			<input type="hidden" name="omm_package_id" value="8"/>
			<div><label for="uname" class="">Azonosító</label> <input id="km_user" type="text" name="km_user" value="" class="error" /></div>
				
			<fieldset> 
				<legend>Számlázási adatok</legend> 
					<div><label for="fname">Cégnév </label> <input id="company" type="text" name="company" value="" size="50" /></div> 
					<div><label for="lname">Bankszámlaszám</label> <input id="bank_account" type="text" name="bank_account" value="" size="50" /></div>
					
					<div><label for="lname">Adószám</label> <input id="billing_taxnumber" type="text" name="billing_taxnumber" value="" size="50" /></div>
					<div><label for="lname">Irányítószám</label> <input id="billing_zip" type="text" name="billing_zip" value="" size="50" /></div>
					<div><label for="lname">Város</label> <input id="billing_city" type="text" name="billing_city" value="" size="50" /></div>
					<div><label for="lname">Cím</label> <input id="billing_address" type="text" name="billing_address" value="" size="50" /></div>
			</fieldset>
				
			<fieldset> 
				<legend>Postázási adatok</legend> 
					<div><label for="fname">Név </label> <input id="posting_name" type="text" name="posting_name" value="" size="50" /></div> 
					<div><label for="lname">Irányítószám</label> <input id="posting_zip" type="text" name="posting_zip" value="" size="50" /></div>
					<div><label for="lname">Város</label> <input id="posting_city" type="text" name="posting_city" value="" size="50" /></div>
					<div><label for="lname">Cím</label> <input id="posting_address" type="text" name="posting_address" value="" size="50" /></div>
			</fieldset>
	
			<fieldset>  
				<legend>Kapcsolattartó adatok</legend> 
					<div><label for="fname">Vezetéknév </label> <input id="contact_familyname" type="text" name="contact_familyname" value="" size="50" /></div> 
					<div><label for="lname">Keresztnév</label> <input id="contact_firstname" type="text" name="contact_firstname" value="" size="50" /></div>
					<div><label for="lname">E-mail</label> <input id="contact_email" type="text" name="contact_email" value="" size="50" /></div>
					<div><label for="lname">Telefon</label> <input id="contact_phone" type="text" name="contact_phone" value="" size="50" /></div>
			</fieldset>
	
			<div class="buttonrow">
				<input type="submit" value="Hozzáadás" class="button">
				<input type="button" value="Mégsem" class="button">
			</div>	
		</form>
	
	</div> 
	
	<?php ?>