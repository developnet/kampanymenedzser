<script>
$(document).ready(function(){

	$("#osszes").click(function(){
		$("#cronjobsTable tr").show();
		$("#cronjobsTable tr").removeClass("odd");
		$("table tr:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#daily").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .daily").show();
		$("table .daily:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#hourly").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .hourly").show();
		$("table .hourly:odd").addClass("odd");
		$("#tableHeader").show();
	});

	$("#bounce").click(function(){
		$("#cronjobsTable tr").hide();
		$("#cronjobsTable tr").removeClass("odd");
		$("#cronjobsTable .bounce").show();
		$("table .bounce:odd").addClass("odd");
		$("#tableHeader").show();
	});

	//setTimeout("location.reload(true);",60000);
	//setTimeout("countDown();",1000);
});

function countDown(){
	//$("#countdown").html(($("#countdown").html() - 1));
	//setTimeout("countDown();",1000);
}
</script>
<h1>Felhasználói fiókok</h1>

<p>
	<a href="/admin/accounts/">előfizetők</a> (<?=$numbers['elofizetok']?>) 
	&nbsp;|&nbsp;
	<a href="/admin/accounts/index/8">demosok</a> (<?=$numbers['demosok']?>) 
	&nbsp;|&nbsp;
	<a href="/admin/accounts/index/12">teszt</a> (<?=$numbers['tesztesek']?>) 
</p>

<table width="100%" cellspacing="0" id="cronjobsTable">

<tr id="tableHeader">
	<th>azonosító</th>
	<th class="right">cég</th>
	<th class="right">akt. email</th>
	<th class="right">csomag</th>
</tr>

<?php $ossz = 0;
foreach($accounts as $acc):
	//$ossz = $ossz + $packages[$acc->km_user]['package']->base_price;
	
?>

<tr class="">
	<td><strong><a href="/admin/accounts/detail/<?=$acc->id?>"><?=$acc->km_user?></a></strong></td>
	<td class="right"><small><?=$acc->company?></small></td>
	<td class="right"><?=$packages[$acc->km_user]['emailcount']?></td>
	<td class="right"></td>
	<?php /*
<?=$packages[$acc->km_user]['package']->name." (".number_format($packages[$acc->km_user]['package']->base_price, 0, ' ', ' ')." Ft/hó)"?>
* */ ?>
</tr>

<?php endforeach;?>

<tr class="aaaaaaaa" style="border-top:2px solid black">
	<td class="right" colspan="3"><strong>Összesen</strong></td>
	<td class="right"><?=number_format($ossz, 0, ' ', ' ');?></td>
</tr>

</table>