<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title>KampányMenedzser feliratkozás</title>
		<link rel="stylesheet" href="<?=$assets?>css/main.css" />
		<link rel="stylesheet" href="<?=$assets?>css/pcss3fs.css" />
		<script src="<?=$assets?>js/jquery-1.9.1.min.js"></script>
		<script src="<?=$assets?>js/jquery.validate.min.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="<?=$assets?>css/pcss3fs-ie8.css" />
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?=$assets?>js/ie8.js"></script>
		<![endif]-->
	</head>
	<body>

	<?php if(isset($form_title)):?>
		<h1><?=$form_title?></h1>
	<?php endif;?>

		
		
		<form action="<?=$formAction?>" id="form" class="pcss3f pcss3f-vm pcss3f-layout-resp" style="">
			<input type="hidden" name="formcode" value="<?=$formCode ?>"/>
			<input type="hidden" name="listcode" value="<?=$listCode ?>"/>


			<?php if(isset($form_text)):?>
				<header><?=$form_text?></header>
			<?php endif;?>			
			

		<?php foreach($fields as $f): ?>


		<?php if($f->field_reference == "email"): ?>

		<!-- E-mail cím -->

			<section class="state-normal">
				<label for="email">E-mail cím<span>*</span></label>
				<input type="email" name="email" id="email" value="<?php if(isset($values) ) echo $values['email'];?>" placeholder="E-mail cím">
				<i class="icon-envelope"></i>
			</section>

		
		<!-- //E-mail cím ?> -->
		<?php else: ?>


			<?php if($f->omm_list_field->type == "singleselectradio"): ?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<section class="state-normal">
				<label for=""><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<span>*</span>' : ''; ?></label>
	    		<?php foreach($f->omm_list_field->getValues() as $key => $v): ?>
				<input type="radio" name="<?=$f->omm_list_field->reference ?>" id="<?=$f->omm_list_field->reference.'_'.$key ?>" value="<?=$v->code ?>" <?php if(isset($values) && $values[$f->omm_list_field->reference] == $v->code ) echo 'checked="checked"';?>>
				<label for="<?=$f->omm_list_field->reference.'_'.$key ?>"><?=$v->value ?></label>				
				<?php endforeach; ?>					
			</section>

			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php elseif($f->omm_list_field->type == "singleselectdropdown"): ?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<section class="state-normal">
				<label for="<?=$f->omm_list_field->reference ?>"><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<span>*</span>' : ''; ?> </label>
				<select name="<?=$f->omm_list_field->reference ?>" id="gender">
					<option value="" disabled selected></option>
				<?php foreach($f->omm_list_field->getValues() as $v): ?>					
					<option value="<?=$v->code ?>" <?php if(isset($values) && $values[$f->omm_list_field->reference] == $v->code ) echo 'selected="selected"';?>><?=$v->value ?></option>				
				<?php endforeach; ?>	
				</select>
				<span></span>
			</section>

			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php elseif($f->omm_list_field->type == "multiselect"): ?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<section class="state-normal">
				<label for=""><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<span>*</span>' : ''; ?></label>
				<?php foreach($f->omm_list_field->getValues() as $key => $v): ?>

				<input type="checkbox" name="<?=$f->omm_list_field->reference ?>[]" id="<?=$f->omm_list_field->reference.'_'.$key ?>" value="<?=$v->code ?>" <?php if(isset($values) && strpos( $values[$f->omm_list_field->reference] , $v->code ) !== false) echo 'checked="checked"'; ?>>
				<label for="<?=$f->omm_list_field->reference.'_'.$key ?>"><?=$v->value ?></label>

				<?php endforeach; ?>					
			</section>

			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php elseif($f->omm_list_field->type == "multitext"): ?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<section class="state-normal">
				<label for="<?=$f->omm_list_field->reference ?>"><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<span>*</span>' : ''; ?></label>
				<textarea cols="1" rows="1" name="<?=$f->omm_list_field->reference ?>" placeholder="<?=$f->omm_list_field->name ?>" class="resizable" style="margin-top: 0px; margin-bottom: 0px; height: 90px;"><?php if(isset($values)  ) echo $values[$f->omm_list_field->reference];?></textarea>
				<i class="icon-question-sign"></i>
			</section>


			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php elseif($f->omm_list_field->type == "date"): ?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php else: ///////sima típusok?>
			<!-- <?=$f->omm_list_field->name ?> -->

			<!-- //<?=$f->omm_list_field->name ?> -->
			<?php endif; ?>


		<?php endif; ?>

		<?php endforeach; ?>

			<section class="state-normal">
				<label for="email">Login <span>*</span></label>
				<input type="text" name="login" id="login">
				<i class="icon-user"></i>
			</section>
			
			
			<footer>
				<button type="submit" class="color-blue"><?=$submitButtonLabel ?></button>
			</footer>
		</form>
		
		<!-- validation init -->
		<script type="text/javascript">
			$(function()
			{
				$('<i class="icon-ok"></i><i class="icon-remove"></i>').appendTo($('#form section'));
						
				$("#form").validate(
				{
					// Rules for form validation
					rules:
					{
						login:
						{
							required: true
						},
						email:
						{
							required: true,
							email: true
						},
						gender:
						{
							required: true
						},
						password:
						{
							required: true,
							minlength: 3,
							maxlength: 20
						},
						passwordConfirm:
						{
							required: true,
							minlength: 3,
							maxlength: 20,
							equalTo: '#password'
						},
						terms:
						{
							required: true
						}
					},
					
					// Messages for form validation
					messages:
					{
						login:
						{
							required: 'Please enter your login'
						},
						email:
						{
							required: 'Please enter your email address',
							email: 'Please enter a VALID email address'
						},
						gender:
						{
							required: 'Please select your gender'
						},
						password:
						{
							required: 'Please enter your password'
						},
						passwordConfirm:
						{
							required: 'Please enter your password one more time',
							equalTo: 'Please enter the same password as above'
						},
						terms:
						{
							required: 'You must agree with terms and conditions'
						}
					},					
					
					// Do not change code below
					errorPlacement: function(error, element)
					{
						error.appendTo(element.parent());
					}
				});
			});			
		</script>
		<!--/ validation init -->
	</body>
</html>