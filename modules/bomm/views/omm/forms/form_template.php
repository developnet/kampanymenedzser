<script>
	
	var KOTELEZO_MEZO = "* A mező megadása kötelező";
	var KOTELEZO_LISTA = "* Válasszon a listából";
	var ERVENYTELEN_EMAIL = "* Érvénytelen e-mail cím";

</script>
<link rel="stylesheet" href="<?=Kohana::config("core.formvalidatorpath") ?>css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
<script src="<?=Kohana::config("core.formvalidatorpath") ?>js/jquery.js" type="text/javascript"></script>
<script src="<?=Kohana::config("core.formvalidatorpath") ?>js/jquery.validationEngine-hu.js" type="text/javascript"></script>

<?php echo $layout;?>

<?php echo $style;?>

<?php if($formAction != ""): ?>
<form action="<?=$formAction?>" method="post" class="subscribe_form">
<?php else: ?>
<form class="subscribe_form">
<?php endif; ?>
<div id="subscribe_container">
	
	<?php if(isset($form_title)):?>
		<h2><?=$form_title?></h2>
	<?php endif;?>
	
	<?php if(isset($form_text)):?>
		<div class="form-desc"><?=$form_text?></div>
	<?php endif;?>
	
	<div class="formfields">

		<input type="hidden" name="formcode" value="<?=$formCode ?>"/>
		<input type="hidden" name="listcode" value="<?=$listCode ?>"/>
		
		<p class="legend"><strong>Figyelem:</strong> A csillaggal jelölt mező kitöltése kötelező (<em>*</em>)</p>
		<?php foreach($fields as $f): ?>
		<?php if($f->field_reference == "email"): ?>

		<!-- E-mail cím -->
		<div class="formfield-item" id="">
			<label for="email">E-mail cím<em>*</em></label>
			<div><input name="email" value="<?php if(isset($values) ) echo $values['email'];?>" type="text" class="validate[required,custom[email]]"/></div>
			<div style="clear: both;"></div>
		</div>
		
		<!-- //E-mail cím ?> -->
		<?php else: ?>
		<?php if($f->omm_list_field->type == "singleselectradio"): ?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
				<label><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<em>*</em>' : ''; ?></label>
					<ul class="radio">
					    <?php foreach($f->omm_list_field->getValues() as $v): ?>
						<li>
							<input type="radio" name="<?=$f->omm_list_field->reference ?>" value="<?=$v->code ?> <?php if(isset($values) && $values[$f->omm_list_field->reference] == $v->code ) echo 'checked="checked"';?> "/>
							<label for="<?=$f->omm_list_field->reference ?>"><?=$v->value ?></label>
					    <?php endforeach; ?>					
					</ul>
			<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php elseif($f->omm_list_field->type == "singleselectdropdown"): ?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
			<label for="<?=$f->omm_list_field->reference ?>"><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<em>*</em>' : ''; ?> </label>
			<select name="<?=$f->omm_list_field->reference ?>" class="<?php echo ($f->required) ? 'validate[required]' : ''; ?>" >
				<optgroup label="<?=$f->omm_list_field->name ?>">
		        <?php foreach($f->omm_list_field->getValues() as $v): ?>
			  		<option value="<?=$v->code ?>" <?php if(isset($values) && $values[$f->omm_list_field->reference] == $v->code ) echo 'selected="selected"';?>><?=$v->value ?></option>
		        <?php endforeach; ?>					
				</optgroup>
			</select>
			<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php elseif($f->omm_list_field->type == "multiselect"): ?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
			<label><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<em>*</em>' : ''; ?></label>
				<ul class="radio">
				    <?php foreach($f->omm_list_field->getValues() as $v): ?>
					<li>
						<input type="checkbox" name="<?=$f->omm_list_field->reference ?>[]" value="<?=$v->code ?>"  class="<?php echo ($f->required) ? 'validate[required]' : ''; ?>"  <?php if(isset($values) && strpos( $values[$f->omm_list_field->reference] , $v->code ) !== false) echo 'checked="checked"'; ?> /><label class="checkbox-label"><?=$v->value ?></label>
					</li>
				    <?php endforeach; ?>					
				</ul>    
		<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php elseif($f->omm_list_field->type == "multitext"): ?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
			<label for="<?=$f->omm_list_field->reference ?>"><?=$f->omm_list_field->name ?></label>
			<textarea name="<?=$f->omm_list_field->reference ?>" rows="5"  class="<?php echo ($f->required) ? 'validate[required]' : ''; ?>"><?php if(isset($values)  ) echo $values[$f->omm_list_field->reference];?></textarea>
		<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php elseif($f->omm_list_field->type == "date"): ?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
			<label><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<em>*</em>' : ''; ?> </label>
			<input name="<?=$f->omm_list_field->reference ?>" value="<?php if(isset($values)  ) echo $values[$f->omm_list_field->reference];?>" type="text"  class="<?php echo ($f->required) ? 'validate[required]' : ''; ?>"/>
		<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php else: ///////sima típusok?>
		<!-- <?=$f->omm_list_field->name ?> -->
		<div class="formfield-item" id="">
			<label><?=$f->omm_list_field->name ?> <?php echo ($f->required) ? '<em>*</em>' : ''; ?> </label> 
			<input name="<?=$f->omm_list_field->reference ?>" value="<?php if(isset($values)  ) echo $values[$f->omm_list_field->reference];?>" type="text" class="<?php echo ($f->required) ? 'validate[required]' : ''; ?>" />
		<div style="clear: both;"></div>
		</div>
		<!-- //<?=$f->omm_list_field->name ?> -->
		<?php endif; ?>
		<?php endif; ?>
		<?php endforeach; ?>
		<div class="form-submit">
			<input value="<?=$submitButtonLabel ?>" class="button" type="submit" />
		</div>
		
	
	</div>
</div>
</form>

