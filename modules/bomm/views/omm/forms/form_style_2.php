<style type="text/css" media="all">
form.subscribe_form {background-color: #CCCCCC;border: 1px solid #999999;color: #000000;}
#subscribe_container {background-color: #ffffff;}
#subscribe_container h2 {font-family: Georgia;font-size: 170%;font-weight: normal;line-height: 100%;}
#subscribe_container .formfields {color: #000000;}
#subscribe_container .form-desc {border-bottom:1px solid #999999;}
#subscribe_container .formfield-item input {border-width: 1px;border-style: solid;border-color: #CCCCCC;}
#subscribe_container ul.radio li input {border: 0;}
#subscribe_container .formfield-item input[type="checkbox"] {border: 0;}
#subscribe_container .form-submit {text-align: center;}
#subscribe_container .formfields .active{background-color: #999999;}
</style>