<style type="text/css" media="all">
form.subscribe_form {background-color: #CFA967;border: 1px solid #CFA967;color: #295B6F;}
#subscribe_container {background-color: #FFE2AF;}
#subscribe_container h2 {font-family: Georgia;font-size: 170%;font-weight: normal;line-height: 100%;}
#subscribe_container .formfields {color: #000000;}
#subscribe_container .form-desc {border-bottom:1px solid #CFA967;}
#subscribe_container .formfield-item input {border-width: 1px;border-style: solid;border-color: #CFA967;}
#subscribe_container ul.radio li input {border: 0;}
#subscribe_container .formfield-item input[type="checkbox"] {border: 0;}
#subscribe_container .form-submit {text-align: center;}
#subscribe_container .formfields .active{background-color: #EFCE95;}
</style>