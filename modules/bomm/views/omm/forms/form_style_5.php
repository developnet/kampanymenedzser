<style type="text/css" media="all">
form.subscribe_form {background-color: #8EBCCF;border: 1px solid #8EBCCF;color: #295B6F;}
#subscribe_container {background-color: #EFFAFF;}
#subscribe_container h2 {font-family: Georgia;font-size: 170%;font-weight: normal;line-height: 100%;}
#subscribe_container .formfields {color: #295B6F;}
#subscribe_container .form-desc {border-bottom:1px solid #0365A6;}
#subscribe_container .formfield-item input {border-width: 1px;border-style: solid;border-color: #8EBCCF;}
#subscribe_container ul.radio li input {border: 0;}
#subscribe_container .formfield-item input[type="checkbox"] {border: 0;}
#subscribe_container .form-submit {text-align: center;}
#subscribe_container .formfields .active{background-color: #D1E6EF;}
</style>