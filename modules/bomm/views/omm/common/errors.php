<div id="failure">
	
	<h3><strong>Hiba</strong> 
		
		<?php 
			if(isset($errorSubject)) 
				echo $errorSubject;
			else
				echo 'az adatok megadásában:';
		?>
	
	</h3>
	
</div>
	
<div class="failureMessage">
	<ol>
		<?foreach($errors as $key => $error): ?>
			
			<?php if($error!="") echo '<li>'.$error.'</li>'; ?>
			
		<?endforeach; ?>
	</ol>
</div>
