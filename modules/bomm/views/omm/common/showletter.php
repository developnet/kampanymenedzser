<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"/>
<title>Levél tartalmának megtekintése</title>
		    		
		    		<script language="JavaScript1.2" src="<?=KOHANA::config('core.assetspath')?>jquery/js/jquery-1.3.2.min.js"></script>
		    		<script language="JavaScript1.2" src="<?=KOHANA::config('core.assetspath')?>jquery/js/jquery-ui-1.7.1.custom.min.js"></script>
		    		<link rel="stylesheet" type="text/css" href="<?=KOHANA::config('core.assetspath')?>jquery/css/km_theme/jquery-ui-1.7.2.custom.css" />



<script type="text/javascript"> 
	$(function() {
		$("#tabs").tabs();
	});
</script> 
	

<style>
	body {
		font-size: 62.5%;
		font-family: sans-serif;
	}
	
	.textContent{
		border: 1px solid #d2d2d2;
		font-family: monospace;
		padding: 10px;
		font-size: 100%;
	}
	
	
	
</style>
</head>

<body>
	<p>
		<h2><span style="font-weight:normal">Tárgy:</span> <?=$l->subject?></h2>
		<h2><span style="font-weight:normal"></>Feladó:</span> <?=$l->sender_name?> &#60;<?=$l->sender_email?>&#62; </h2>
	</p>
	<div id="tabs"> 
		<ul> 
			<li><a href="#html">HTML tartalom</a></li> 
			<li><a href="#text">TEXT tartalom</a></li>  
		</ul> 
		<div id="html">
			<iframe src="<?=url::base()."api/common/getLetterHtmlContent/".$l->id ?>" style="width:100%;height:6000px"></iframe> 
		</div> 
		<div id="text"> 
			<div class="textContent"><?=$text?></div> 
		</div> 
	</div> 
 
</body>