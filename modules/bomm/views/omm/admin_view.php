<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>KampányMenedzser - Adminisztráció</title>

<script>
 var URL_BASE = "<?=url::base() ?>";
</script>

<script src="http://www.google.com/jsapi"></script> 
<script> 
  	// Load jQuery
	google.load("jquery", "1.4.1");
</script> 

<script type="text/javascript" src="<?=$assets?>js/swfobject.js"></script> 



<style type="text/css">

	body{font-family:Arial, Helvetica, sans-serif; font-size:13px; background:#FFFFFF;}

	ul, li{margin:0; border:0; list-style:none; padding:0;}
	ul{height:21px;}

	h1 { font-size:18px; }
	p { line-height:18px; }

	#wrapper { margin:0 auto; width:960px;}

	#info { margin:auto; width:960px; color:#333333; padding:10px; background:#f4f4f4; border:1px solid #DDD; }

	#nicemenu { margin:0 auto; width:960px; border-bottom:dotted 1px #E5E5E5; }
	#nicemenu a { color:#0066CC; text-decoration:none; }
	#nicemenu a:hover { text-decoration:underline; }	
	#nicemenu li { display:inline; position:relative; }
	#nicemenu li span { position:relative; z-index:10; padding:4px 4px 4px 6px;  border-bottom:none; line-height:18px; }	
	#nicemenu li span a { font-weight:bold; padding:0 6px 0px 2px;  }	
	#nicemenu li span.over { padding:4px 3px 4px 5px;  border-top:solid 1px #E5E5E5; border-left:solid 1px #E5E5E5;  border-right:solid 1px #999999; border-bottom:solid 1px #fff;  }
	*+html #nicemenu li span.over {  border-top:solid 2px #E5E5E5; padding-bottom:3px; } /* IE6 */
	#nicemenu li span.over a { }
	#nicemenu li span.over a:hover { text-decoration:none; }
	#nicemenu li span.active { padding:4px 3px 4px 5px;  border-top:solid 1px #E5E5E5; border-left:solid 1px #E5E5E5;  border-right:solid 1px #999999; border-bottom:solid 1px #fff;  }
	*+html #nicemenu li span.active {  border-top:solid 2px #E5E5E5; padding-bottom:3px; }
	#nicemenu li span.active a { }
	#nicemenu li span.active a:hover { text-decoration:none; }	
	#nicemenu img.arrow { /*margin-left:4px;*/ cursor:pointer; }
	#nicemenu div.sub_menu { display:none; position:absolute; left:0; top:0px; margin-top:18px; border-top:solid 1px #E5E5E5; border-left:solid 1px #E5E5E5; border-right:solid 1px #999999; border-bottom:solid 1px #999999; padding:4px; top:2px; width:160px; background:#FFFFFF; }
	* html #nicemenu div.sub_menu { margin-top:23px; } /* IE6 */
	*+html #nicemenu div.sub_menu { margin-top:23px; } /* IE7 */
	#nicemenu div.sub_menu a:link, 
	#nicemenu div.sub_menu a:visited, 
	#nicemenu div.sub_menu a:hover{ display:block; font-size:11px; padding:4px;}	
	#nicemenu a.item_line { border-top:solid 1px #E5E5E5; padding-top:6px !important; margin-top:3px; }
	
	h1 {
		color:#FF0084;
		font-size:18px;
		font-weight:normal;
		margin-bottom:10px;
	}
	
	table {
		border:0 none;
	}	
		
		
	tr {
		vertical-align:top;
	}	
		
	th {
		border-bottom:1px solid #EEEEEE;
		color:#666666;
		font-size:11px;
		font-weight:normal;
		text-align:left;
		padding-left:5px;
		padding-right:5px;
		text-align:left;		
	}	
		
		
	td {
		color:#000000;
		font-size:12px;
		border-bottom:1px solid #E5E5E5;
		padding:5px 5px 7px;
	}	
	
	td .right{
		text-align:right;
	}

	tr .right{
		text-align:right;
	}
	
	small {
		color:#999999;
		font-size:11px;
	}	
	
	.odd{
		background-color:#F5F5F5;
	}
	
	.yellow{
		background-color: #fff5b2 !important;
	}
	
 
	/* Form styles */
	div.form-container { margin: 10px; padding: 5px; background-color: #FFF; border: #EEE 1px solid; }
	 
	p.legend { margin-bottom: 1em; }
	p.legend em { color: #C00; font-style: normal; }
	 
	div.errors { margin: 0 0 10px 0; padding: 5px 10px; border: #FC6 1px solid; background-color: #FFC; }
	div.errors p { margin: 0; }
	div.errors p em { color: #C00; font-style: normal; font-weight: bold; }
	 
	div.form-container form p { margin: 0; }
	div.form-container form p.note { margin-left: 170px; font-size: 90%; color: #333; }
	div.form-container form fieldset { margin: 10px 0; padding: 10px; border: #DDD 1px solid; }
	div.form-container form legend { font-weight: bold; color: #666; }
	div.form-container form fieldset div { padding: 0.25em 0; }
	div.form-container label, 
	div.form-container span.label { margin-right: 10px; padding-right: 10px; width: 150px; display: block; float: left; text-align: right; position: relative; }
	div.form-container label.error, 
	div.form-container span.error { color: #C00; }
	div.form-container label em, 
	div.form-container span.label em { position: absolute; right: 0; font-size: 120%; font-style: normal; color: #C00; }
	div.form-container input.error { border-color: #C00; background-color: #FEF; }
	div.form-container input:focus,
	div.form-container input.error:focus, 
	div.form-container textarea:focus {	background-color: #FFC; border-color: #FC6; }
	div.form-container div.controlset label, 
	div.form-container div.controlset input { display: inline; float: none; }
	div.form-container div.controlset div { margin-left: 170px; }
	div.form-container div.buttonrow { margin-left: 180px; }
	 
	
</style>
<script type="text/javascript">

$(document).ready(function(){

	$("#nicemenu img.arrow").click(function(){ 
								
		$("span.head_menu").removeClass('active');
		
		submenu = $(this).parent().parent().find("div.sub_menu");
		
		if(submenu.css('display')=="block"){
			$(this).parent().removeClass("active"); 	
			submenu.hide(); 		
			$(this).attr('src','<?=$base.$img?>arrow_hover.png');									
		}else{
			$(this).parent().addClass("active"); 	
			submenu.show(); 		
			$(this).attr('src','<?=$base.$img?>arrow_select.png');	
		}
		
		$("div.sub_menu:visible").not(submenu).hide();
		$("#nicemenu img.arrow").not(this).attr('src','<?=$base.$img?>arrow.png');
						
	})
	.mouseover(function(){ $(this).attr('src','<?=$base.$img?>arrow_hover.png'); })
	.mouseout(function(){ 
		if($(this).parent().parent().find("div.sub_menu").css('display')!="block"){
			$(this).attr('src','<?=$base.$img?>arrow.png');
		}else{
			$(this).attr('src','<?=$base.$img?>arrow_select.png');
		}
	});

	$("#nicemenu span.head_menu").mouseover(function(){ $(this).addClass('over')})
								 .mouseout(function(){ $(this).removeClass('over') });
	
	$("#nicemenu div.sub_menu").mouseover(function(){ $(this).fadeIn(); })
							   .blur(function(){ 
							   		$(this).hide();
									$("span.head_menu").removeClass('active');
								});		
								
	$(document).click(function(event){ 		
			var target = $(event.target);
			if (target.parents("#nicemenu").length == 0) {				
				$("#nicemenu span.head_menu").removeClass('active');
				$("#nicemenu div.sub_menu").hide();
				$("#nicemenu img.arrow").attr('src','<?=$base.$img?>arrow.png');
			}
	});			   



	
    $("table tr:odd").addClass("odd");
	
	
	server_main_timer();
	   
								   
});

function server_main_timer(){
	$.get(URL_BASE+"api/common/getTime", function(data){

		_data = data.split("|");	
		time = _data[3];
		day = _data[2];
		month = _data[1];
		year = _data[0];
		
		$("#server_time_main").html(year+"."+month+"."+day+" "+time);
		setTimeout('server_main_timer()',5000);
		
	});	
}

</script>
</head>

<body>

<div id="wrapper">
<?php 
$page = $this->uri->segment(2,"");

?>
<div id="nicemenu">
    <ul>
        <li><span class="head_menu"><a href="/">Kezdőoldal</a></span>
      
        <li><span class="head_menu <?php echo ($page == "accounts") ? "over" : "" ?>"><a href="/admin/accounts">Felhasználók</a><img src="<?=$base.$img?>arrow.png" width="18" height="15" align="top" class="arrow" /></span>
            <div class="sub_menu">
                 <a href="/admin/accounts/add">Új felhasználó</a>
                 <a href="/admin/accounts/delete">Felhasználó törlés</a>
            </div> 
        </li>
      
        <li><span class="head_menu <?php echo ($page == "userstat") ? "over" : "" ?>"><a href="#">Statisztika</a><img src="<?=$base.$img?>arrow.png" width="18" height="15" align="top" class="arrow" /></span>
             <div class="sub_menu">
                <a href="/admin/userstat">Kiküldések / Feliratkozók</a>
            </div>
        </li>
      
        <li><span class="head_menu"><a href="index.html">Számlák</a><img src="<?=$base.$img?>arrow.png" width="18" height="15" align="top" class="arrow" /></span>
            <div class="sub_menu">
                <a href="index.html">Tranzakciók</a>
                <a href="index.html">Postai feladás generálás</a>
            </div>
        </li>

        <li><span class="head_menu <?php echo ($page == "cronstat") ? "over" : "" ?>"><a href="/admin/cronstat">Cron ellenőrzés</a><img src="<?=$base.$img?>arrow.png" width="18" height="15" align="top" class="arrow" /></span>
        	<div class="sub_menu">
        		<a href="/admin/mailqueue">Postfix mail queue</a>
            </div>
        </li>

    </ul>
</div>

<?php if(isset($pageContent)) echo "$pageContent";?>

<p><br/></p>
<div id="info"> 
<code style="font-size:11px">Verzió: <span><?=meta::getVersionString()?></span>,  Szerver idő: <span id="server_time_main"><?=date('Y.m.d H:i') ?></span></code>
 </div>

</div>

</body>


</html>
