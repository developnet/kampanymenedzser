<h1><?=$title?></h1>

<p>Ezen a képernyőn a „Feliratkozási folyamat beállítása képernyő”-höz hasonlóan a leiratkozás során történő átirányításokat adhatja meg.</p>

<p>A beállítások mentéséhez nyomja meg a „Leiratkozási folyamat mentése” gombot.</p>