<h1><?=$title?></h1>

<p>A statisztikai képernyőkön a levelek megnyitási, átkattintási és kiküldési adatait követheti nyomon. Megnézheti, hogy hány címzett nyitotta meg a levelet, mikor és hány linkre kattintott át,  a levélben szereplő linkeket milyen arányban  használták stb..</p>

<p>Ez a képernyő nagyon hasonlít a „Kampányok böngészése képernyő”-re, azzal a különbséggel, hogy itt csupán a levelek kiválasztását végezheti el. Válassza ki a levelet, aminek a részletes statisztikáit látni szeretné.</p>