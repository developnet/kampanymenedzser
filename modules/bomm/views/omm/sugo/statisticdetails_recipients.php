<h1><?=$title?></h1>

<p>Az előnézettel szemben itt a konkrét feliratkozókat böngészheti, akik megnyitották a levelet, átkattintottak vagy leiratkoztak. A kategóriák váltásához kattintson a táblázat feletti tabokra.</p>

<p>Az „Átmeneti hiba” és „Állandó hiba” nézeteknél megnézheti, hogy mi volt a hiba oka. Mindkét esetben a kiküldés után az adott címzettől visszapattanó e-mail jött és a rendszer ezeket dolgozza fel és szortírozza szét átmeneti illetve állandó hibákra.</p> 

<p class="info">Egy feliratkozó akkor fog hibás állapotba kerülni ha egy év alatt kettő állandó hibát vagy két hónap alatt öt darab átmeneti hibát kap. A hibás állapotú feliratkozókat a lista előnézeténél tudja megnézni, illetve újra aktív állapotba rakni.</p>