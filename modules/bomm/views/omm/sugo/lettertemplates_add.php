<h1><?=$title?></h1>

<p>Itt tudja kiválasztani a saját gépéről a feltöltendő html fájlt, illetve a későbbi könnyebb megkülönböztethetőség céljából itt megadhat egy képfájlt is, ami a sablon előnézetét mutatja.</p> 

<p class="info">A html fájlnak meg kell felelnie egy szabványos HTML 4.0 fájlnak és tartalmaznia kell <code>&lt;body&gt;</code> tag-et. Amennyiben szeretne a saját honlapjához illő levélsablonokat létrehozni, de nincs rá lehetősége, vegye fel velünk a kapcsolatot a <a href="mailto:support@kampanymenedzser.hu">support@kampanymenedzser.hu</a> e-mail címen.</p>
