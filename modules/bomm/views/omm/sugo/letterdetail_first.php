<h1><?=$title?></h1>

<p>Ezen a képernyőn adhatók meg a levél alapadatait. A levél tárgyában használhatók a listák behelyettesítő tag-ei (lásd a „Lista hozzáadása/szerkesztése – Lista mezői képernyő” leírását).</p>

<p>Ha jól megadta az adatokat, két lehetősége van. Vagy menti és kilép a „Levél átnézeti képernyő”-re vagy továbblép a „Levél tartalmának szerkesztése képernyőre”. Mind a két esetben mentheti vagy elvetheti az adatokat.</p>