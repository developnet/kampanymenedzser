<h1><?=$title?></h1>

<p>Ez a képernyő áttekintést ad arról, hogy a levélben szereplő linkekre hányan és milyen arányban kattintottak. Ezek fontos információk lehetnek ahhoz, hogy mérni tudja, a levélben melyik pozícióban elhelyezkedő linkek a jobbak.</p>