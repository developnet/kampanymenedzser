<h1><?=$title?></h1>

<p>A csoport feltételeinek megadása mindig a lista egy mezőjének kiválasztásával indul. Válassza ki a mezőt, amit szeretne belerakni a feltételekbe, majd nyomja meg a „Feltétel hozzáadása” gombot. Ezután a mező hozzáadódik a feltételekhez. Miután a mező szerepel a feltételek között, beállíthatja, hogy mi legyen a feltétel típusa ,és típustól függően a feltétel értékét is megadhatja.</p>

<h2>A feltétel típusai az alábbiak lehetnek:</h2>

<ul>
	<li><i>„meg van adva”</i><br/>a mező ki van töltve valamilyen értékkel<br/><br/></li>
	<li><i>„megegyezik”</i><br/>a mező értéke megegyezik a megadott értékkel<br/><br/></li>
	<li><i>„nem egyezik meg”</i><br/>a mező értéke nem egyezik meg a megadott értékkel<br/><br/></li>
	<li><i>„nincs megadva”</i><br/>a mezőben nincs érték<br/><br/></li>
	<li><i>„kisebb, mint”</i><br/>a mező értéke kisebb a megadott értéknél <br/>(csak dátum típusnál)<br/><br/></li>
	<li><i>„nagyobb, mint”</i><br/>a mező értéke nagyobb a megadott értéknél <br/>(csak dátum típusnál)<br/><br/></li>
	<li><i>„ennyi van bejelölve”</i><br/>pontosan ennyi érték van bejelölve a checkboxok közül <br/>(csak „Választó checkbox” típusnál)<br/><br/></li>
	<li><i>„kevesebb, mint ennyi van bejelölve”</i><br/>ennél az értéknél kevesebb van bejelölve a checkboxok közül <br/>(csak „Választó checkbox” típusnál)<br/><br/></li>
	<li><i>„több, mint ennyi van bejelölve”</i><br/>ennél az értéknél több van bejelölve a checkboxok közül <br/>(csak „Választó checkbox” típusnál)<br/><br/></li>
</ul>

<p>Amennyiben egy feliratkozóra igaz valamelyik feltétel, bele fog kerülni a csoportba. A csoporthoz bármennyi mezőre adhat meg feltételt, így sok és változatos csoportot tud létrehozni.</p>

<p class="info">A csoportok fontos elemét képezik a KampányMenedzser-nek, mert egy levél címzettjeit a csoportokon keresztül tudja a legpontosabban megadni. Gondolja át, hogy a feliratkozói közül a jövőben milyen belső csoportoknak szeretne leveleket küldeni, és aszerint tervezze meg a lista mezőit, majd a rájuk épülő csoportokat.</p>

<p><br/></p>