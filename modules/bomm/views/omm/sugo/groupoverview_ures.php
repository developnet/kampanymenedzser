<h1><?=$title?></h1>

<p>A csoportok segítségével különböző szűrőket állíthat be a lista mezői alapján a feliratkozókra, amikkel később bonyolultabb levélcímzéseket is végrehajthat. A csoportok nem fix halmazok, hanem a beállított feltételek alapján dinamikusan mutatják az adott pillanatban a csoportba tartozó feliratkozókat. Például ha a listában van egy választható mező, amin meg lehet adni, hogy az adott feliratkozó férfi vagy nő, akkor könnyedén létrehozhatunk egy csoportot a nőknek és egy csoportot a férfiaknak.</p>

<p>Ezen a képernyőn láthatja, milyen csoportok tartoznak a listához, és megnézheti, melyik csoportba jelenleg hány feliratkozó tartozik. Új csoportot az „Új csoport létrehozása” gomb megnyomásával hozhat létre.</p>