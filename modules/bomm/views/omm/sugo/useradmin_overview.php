<h1><?=$title?></h1>

<p>Ezen a felületen kezelheti az előfizetéséhez tartozó felhasználókat. A felhasználók jogosultsága két féle lehet.</p>
<p>Egy előfizetéshez egy "Adminisztrátor" jogosultságú felhasználót kap. Ez a felhasználó nem törölhető és ez a felhasználó tud csak másik felhasználókat létrehozni, illetve csak ő lát bele az előfizetés egyenlegébe.</p>
<p>"Felhasználó" jogosultságú hozzáférésből bármennyit létrehozhat, így egy előfizetéshez akár több ember is hozzáférhet saját felhasználónév jelszó párossal.</p>