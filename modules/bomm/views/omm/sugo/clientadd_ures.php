<h1><?=$title?></h1>

<p>A sikeres hozzáadáshoz töltse ki a beviteli mezőket és nyomja meg a „Honlap hozzáadása” gombot.  A „Domain név” mezőt kitöltésekor figyeljen rá, hogy a címet http:// és www előtagok nélkül adja meg. Az itt megadott domain lesz összekapcsolva a honlappal.</p> 

<p>Az itt megadott „Küldő e-mail cím” és „Küldő név” mezőknek az a szerepe, hogy a későbbiek során a honlap alatt létrehozandó leveleknél ne kelljen minden alkalommal kitölteni a feladó adatait. Az itt megadott adatok minden kampány létrehozáskor alapértelmezetten fel lesznek ajánlva, egyedi esetekben kell és lehet külön küldő nevet és e-mail címet megadni.</p> 

<p>A „Válasz e-mail cím” mező kitöltése sem itt, sem a levelek létrehozásakor nem kötelező. Ha mégis megadja, akkor a kiküldött levelekben külön be lesz állítva. Ez az az e-mail cím, ami a feliratkozóknak alapértelmezetten fel lesz ajánlva, amikor a rendszerből kiküldött levelekre válaszolni szeretnének. Amennyiben nem tölti ki, ez megegyezik a „Küldő e-mail cím” mező értékével.</p>