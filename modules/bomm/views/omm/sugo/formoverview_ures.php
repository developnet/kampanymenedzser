<h1><?=$title?></h1>

<p>KampányMenedzserben az űrlapok két funkciót tölthetnek be. Egyrészt feliratkozó-űrlapokat lehet  generálni a listákhoz, amiknek a forráskódját be tudja illeszteni a saját weboldalába, másrészt hogy a feliratkozók a saját adataikat egy adott űrlapon keresztül tudják módosítani. Az űrlapok közül mindig csak egy lehet kijelölve mint adatmódosító űrlap. </p>

<p>Ezen a képernyőn láthatja át a már elkészült űrlapokat, illetve itt adhat hozzájuk újat. Meglévő úrlap szerkesztéséhez kattintson a táblázatban a szerkeszteni kívánt úrlap nevére. Új űrlap felviteléhez pedig nyomja meg az „Új űrlap hozzáadása” gombot.</p>

<p class="info">A táblázatban azt is látja, hogy az egyes űrlapon keresztül eddig hányan iratkoztak fel. Ezzel a funkcióval hatékonyságot tud mérni, hogy a különböző helyekre elhelyezett űrlapok miként működnek.</p>