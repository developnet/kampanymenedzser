<h1><?=$title?></h1>

<p>Ez a képernyő biztosítja a KampányMenedzsernek az átjárást más rendszerekbe, illetve más rendszerekből. Ezen az interfészen keresztül tud feliratkozókat importálni és exportálni. Az interfész eszköze a CSV fájl, ami egy szöveges dokumentum, amiben vesszővel elválasztva vannak megadva az adatok. Az adatokat  idézőjel (”) jelöli. Ezt a formátumot a legtöbb táblázatkezelő szoftver (pl. Microsoft Excel) kezeli.</p>

<h2>Importálás</h2>

<p>A CSV fájlnak mindig egy fejléccel kell kezdődnie, ami megmondja, hogy milyen mezőbe kerülnek majd importálásra az adatok. A fejlécben a mezők referenciáinak kell szerepelnie, amiket a „Lista hozzáadása/szerkesztése – Lista mezői” képernyő táblázatában talál. A fejléc után a CSV fájlban minden sor egy feliratkozót jelent. A fejlécben mindig szerepelni kell az "email" és "regdatum"  referenciáknak. Ezek hiányában az importálás nem fog lefutni.</p>

<p>Jelölje ki a saját gépén a CSV fájlt és nyomja meg „Feliratkozók importálása CSV-ből” gombot. Importálása után láthatja az importálása eredményét.</p>

<p class="info">Importálás során ha a listán már létező e-mailt talál akkor a CSV-ben lévő adatokkal frissíti a listán lévő feliratkozó adatait. Ezzel a funkcióval szükség esetén tömegesen tudja a feliratkozók adatait módosítani.</p>

<h3>Speciális típusú mezők importálása</h3>
<ul>
	<li>A <strong>dátum</strong> formátuma mindig É-H-N Ó:P:M (pl.: 2009-10-19 21:02:35) legyen.</li>
	<li><strong>Választós</strong> mezőknél az opció megnevezést kell használni.</li>
	<li><strong>Többválasztós</strong> mezőknél az opciókat az oszlopon belül „;”-vel elválasztva lehet megadni. <br/>
		<code>Pl. ..."Csoport 1;Csoport 2";....</code>
	</li>
</ul>


<h2>Exportálás</h2>

<p>Exportáláshoz jelölje ki a „Lista exportálása” jelölőt és nyomja meg a „Feliratkozók exportálása CSV-be” gombot, majd mentse el a saját gépére a CSV fájlt.</p>

<p class="info">Az exportálás során létrejött CSV fájl teljes mértékben megfelel az importálás követelményeinek, ezért az egyik listáról a másikra való tömeges másolást plusz munka nélkül meg tudja oldani.</p>

<p><br/></p> 