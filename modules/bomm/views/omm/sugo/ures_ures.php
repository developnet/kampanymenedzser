<h1><?=$title?></h1>

<p>Az alkalmazás legnagyobb egysége a honlap. Előfizetéstől függően bizonyos számú honlap kezelhető a rendszerben. Egy honlap egy domain-címet jelent. A honlapok alá fognak tartozni a listák, kampányok illetve a levelek.</p>

<p>Ezen a képernyőn tudja kiválasztani, hogy melyik honlappal szeretne dolgozni. Ha még nincs létrehozott  honlap, akkor az „Új honlap hozzáadása” gomb megnyomásával a „Honlap hozzáadása” képernyő segítségével teheti azt meg.</p> 

<p><strong>Honlap törléséhez</strong> vigye az egeret a honlapokat tartalmazó táblázat megfelelő sorára és kattintson a megjelenő kis kuka ikonra. Kétszeri megerősítés során törölhető a honlap.</p>

<p class="info">Egy honlap törlése után már nem tudja elérni a honlap alá tartozó adatokat (listák, kampányok, statisztikák, stb..). Amennyiben tévesen törölte a honlapot és szeretné újra előhozni az adatokat, azt e-mailben kérheti a <a href="mailto:support@kampanymenedzser.hu">support@kampanymenedzser.hu</a> címen. Az e-mailben írja meg az előfizetői azonosítóját, a honlap nevét és hogy mikor lett törölve.</p>