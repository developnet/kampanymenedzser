<h1><?=$title?></h1>

<p>Ez a lista fő képernyője. Innen minden beállítást és adatot elér, ami egy meglévő listához tartozik. Grafikonokon láthatja a lista mozgásait (fel- és leiratkozások időbeni eloszlása) illetve a jobb oldalon található menü segítségével navigálhat a listához tartozó képernyők között.</p>

<p>A képernyő bal oldalának jobb felső részén található eszközzel állíthatja be, hogy a grafikon és az alatta lévő táblázat milyen időintervallum adatait mutassa. Éves, havi, heti és napi intervallumokból választhat, és a nyilak segítségével navigálhat az intervallum-ablakok között. Az intervallum változtatásával a grafikon és az alatta lévő táblázat a regisztráció dátuma alapján mutatja a feliratkozókat.</p>

<p>A „Lista összes tagjának megjelenítése” linkre kattintva időszűrés nélkül böngészheti a feliratkozókat. Ebben a nézetben külön fülön láthatja az aktív, leiratkozott, hibás, aktiválás előtt illetve törölt státusszal rendelkező feliratkozókat.</p> 

<p class="info">Az „Összes feliratkozó törlése” linkre kattintva kétszeri megerősítés után az összes feliratkozót törölheti a listáról. Az így törölt feliratkozó adatok nem állíthatóak vissza és mindig végleges törlést jelentenek, ezért jól gondolja át ennek a funkciónak a használatát.</p> 