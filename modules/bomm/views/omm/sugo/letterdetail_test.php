<h1><?=$title?></h1>

<p>Amíg a levél legalább egyszer nem lett teszt jelleggel kiküldve, nem aktiválható. Ezzel a rendszer használata során elkerülhető a hibás levelek aktiválása, kiküldése. Egy hibás levél a feliratkozók szemében erősen negatív érzéseket kelthet, akár leiratkozáshoz is vezethet.</p> 

<p>Teszt küldéséhez adjon meg egy e-mail címet és nyomja meg a „Teszt e-mail elküldése” gombot.</p>