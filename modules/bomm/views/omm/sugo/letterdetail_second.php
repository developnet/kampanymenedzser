<h1><?=$title?></h1>

<p>Egy levél tartalmának megadására több lehetősége van. Elsőként döntse el, hogy csak sima szöveges vagy html és szöveges levelet szeretne létrehozni. A html-leveleknek számos előnye van. Többek között, hogy tetszetős formázásokat alkalmazhat, képeket szúrhat be és ami ennél is fontosabb, hogy a statisztikamérések csak html-levelek esetében működnek. Amennyiben úgy dönt, hogy sima szöveges levelet küld ki, le kell mondania a megnyitási és átkattintási statisztikákról.</p>

<p>Html-levél tartalmának megadása során használhatja a már ismertetett levél sablonjait, közvetlenül feltölthet egy html fájlt, vagy a szerkesztő segítségével saját kezűleg hozhatja létre a tartalmat. A tartalomban használhatóak a listák behelyettesítő tag-ei (lásd a „Lista hozzáadása/szerkesztése – Lista mezői képernyő” leírását). Ehhez segítséget nyújthatnak a jobb oldalon található listák, melyekkel mind az általános (e-mail cím, regisztrációs dátum, leiratkozó link), mind a listához kötött behelyettesítő tageket tallózhatja. Beszúráshoz jelölje ki a beszúrni kívánt tag-et és nyomja meg a „Beszúr” gombot.</p>

<p class="info">A behelyettesítő tagek megadása során lehetősége van, hogy megadjon egy értéket, ami akkor fog behelyettesítődni, ha a feliratkozónak nincsen kitöltve az adott mezője. Ezt a következő képpen adhatja meg: <code>{{keresztnev,ures=érdeklődő}}</code>. </p>

<p>Miután létrehozta a levél tartalmát,  vagy visszalép a levél alapadataihoz, menti és kilép a „Levél átnézeti képernyő”-re, vagy továbblép a „Levél címzési feltételeinek megadása képernyőre”.</p> 