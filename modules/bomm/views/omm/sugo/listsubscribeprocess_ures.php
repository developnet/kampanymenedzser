<h1><?=$title?></h1>

<p>Ezen a képernyőn tudja beállítani, hogy feliratkozáskor a felhasználó milyen utat járjon be. A beállítási lehetőségeket  a lista típusa (Szimpla Opt-in, Kettős Opt-in) determinálja.</p>

<p>A képernyő első felében állíthatja be azokat az érkező oldalakat (landing-page) melyekre a rendszer elirányítja a feliratkozót bizonyos események hatására. Ezen beállítások segítségével a KampányMenedzsert transzparenssé teheti a látogatók szemében.</p>

<p>A képernyő következő blokkja csak Kettős Opt-in lista esetén állítható. Itt adhatjuk meg, hogy mi szerepeljen a megerősítést kérő e-mailben, illetve beállíthatjuk, hogy aktiválás után a látogató milyen url-re legyen elirányítva. Választhat, hogy text- vagy html-típusú levelet szeretne küldeni megerősítéskor. Fontos, hogy az aktivációs linket mindig szerepeltesse a levélben. Elég, ha bemásolja a <code>{{aktivacios_link}}</code> szöveget a levél tartalmába, és a rendszer automatikusan kicseréli a megfelelő linkre a levél kiküldésekor.</p>

<p>Az ezt követő blokkban a sikeres feliratkozás után történő eseményt állíthatja be. Három lehetősége van: (1) ne történjen semmi, (2) text-típusú levelet vagy (3) html-típusú levelet küldjön a rendszer. A levelek szerkesztése az aktiválás során küldendő levél szerkesztésével megegyezik.</p> 

<p>A folyamat beállításainak mentéséhez nyomja meg a „Feliratkozási folyamat mentése” gombot. Sikeres mentés esetén vissza fog térni a „Lista előnézete” képernyőre.</p>