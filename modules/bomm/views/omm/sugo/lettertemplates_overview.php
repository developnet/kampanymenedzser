<h1><?=$title?></h1>

<p>A levélsablonok azok az előre betárazható html minták, amiket később a levelek szerkesztésekor bármikor elővehet. Ezen a képernyőn kezelheti a már felvitt sablonjait illetve újat vihet fel. Egy sablon a gyakorlatban egy html fájlt jelent, amely tartalmazza a szükséges külalakot és formázásokat, melyekre szükség lehet egy igényes levél létrehozása során.</p> 

