<h1><?=$title?></h1>

<p>Egy lista felvitelének első két lépése, hogy megadjuk a lista alapadatait, majd megadjuk a lista mezőit.</p>

<p>Ezen a képernyőn tudja beállítani, hogy az adott listát milyen mezők jellemezzék. Egy mező a listán szereplő feliratkozók egyféle adatát jelenti. Minden lista minimális mezői az „E-mail cím” és a „Regisztráció dátuma” mezők. Ez a két mező kötött és nem lehet sem eltávolítani, sem szerkeszteni. Az e-mail cím egy listán mindig egyedi. Az egyediség megsértésének kezelését feliratkozáskor a „Lista feliratkozási folyamatának beállítása” képernyőn tudja állítani. A regisztráció dátuma jelzi azt, hogy az adott feliratkozó mikor vált a lista aktív tagjává.</p>

<p class="info">Kettős Opt-in listánál a megerősítés dátuma, szimpla Opt-in listánál a feliratkozás dátuma, manuális felvitelnél a felvitel dátuma,  importnál pedig vagy az importálás dátuma, vagy az importállományban egyedileg megadott időpont jelenti a regisztráció dátumát.</p>

<p class="info">A mezőket felsoroló táblázat második oszlopában láthatja a „Behelyettesítő tag” oszlopot. Ez az a referenciatag amit a későbbiek során a levél tárgyában és tartalmában használhat. Ezek a tag-ek a levél kiküldése során automatikusan behelyettesítődnek a címzett feliratkozó odavonatkozó adataival.</p>

<p>A két beépített mezőn kívül tetszőleges számú mezőt vihet fel a listához. Minél többféle információt tud begyűjteni a feliratkozóitól, annál jobban megismeri őket, és annál részletesebben tudja őket később csoportokra bontani.</p>

<p><strong>Új mező hozzáadásához</strong> írja be a „Mező nevét”, válassza ki az „Adattípust” és nyomja meg a „Mező hozzáadása” gombot. Ha szeretné, hogy a „Lista előnézete” képernyőn a táblázatban megjelenjenek a feliratkozók mezőhöz tartozó adatai, akkor pipálja be a „Táblázat” kiválasztót.</p> 

<p><strong>Mező törléséhez</strong> kattintson a mezőket felsoroló táblázatban a törlendő mező sorának végén elhelyezkedő kis kuka ikonra. Kétszeri megerősítés után a mező törlődni fog. Amennyiben a listán már vannak feliratkozók, akkor a mezőhöz tartozó adataik el fognak veszni, illetve a mezőhöz tartozó csoportfeltételek is törlődni fognak.</p>

<p>Egy meglévő <strong>mező módosításához</strong> kattintson a mezőket felsoroló táblázatban a módosítani kívánt mező nevére. Kattintás után a mező adatai betöltődnek a felviteli form-ba és az adatok módosíthatóak.</p>

<h2>Adattípusok</h2>

<p>Mező felvitelekor többféle adattípust választhat ki. Az alapadatokhoz célszerű az „Előre definiált mezők” közül választani, mert ezeket könnyen be tudja azonosítani a rendszer. A beazonosításra szükség lehet például kapcsolt listák esetén, ahol a listák között adatokat kell másolni.</p> 

<p>Az „Előre definiált mezők” mindegyike egy szabadon szerkeszthető szöveges mezőt jelent.</p>

<p>A „További adattípusok” között találhatóak a speciális típusok.</p>
  
<p>A „Választó ...” típusoknál lehetőség van több opció felvitelére, amikből majd a feliratkozó adatainak megadásakor választani lehet (feliratkozó farm, adatmódosítás, feliratkozó módosítása stb..). Az opciókat egyenként tudja felvinni az „Új opció” gomb melletti szövegbeviteli mező és a gomb segítségével. Szerkesztésükhöz kattintson a nevük melletti „szerkeszt” linkre, illetve törléshez a kis stoptábla ikonra. A sorrendjük változtatásához pedig egyszerűen fogja meg az „opciót” és drag-and-drop technikával határozza meg a sorrendet.</p>
