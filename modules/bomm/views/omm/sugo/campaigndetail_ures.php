<h1><?=$title?></h1>

<p>Kampány létrehozásához töltse ki a mezőket és nyomja meg a „Kampány létrehozása” gombot. Az itt megadott adatok a levelek létrehozásakor alapértelmezettként fel lesznek ajánlva, de azok ott módosíthatóak. Ez egy kis könnyebbséget nyújt, mivel általában az egy kampány alá tartozó leveleket küldő név és e-mail adatai megegyeznek.</p>