<h1><?=$title?></h1>

<p>Egy honlap adatait bármikor módosíthatja, ha a „Honlapbeállítások” tabfülre kattint a fejlécben. Az adatok módosítása a felvitelükhöz hasonló módon történik. Erről a képernyőről éri el a „Levélsablonok képernyőt” is.</p>