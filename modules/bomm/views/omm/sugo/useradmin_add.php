<h1><?=$title?></h1>

<p>Töltse ki az űrlapot a felhasználó hozzáadásához vagy módosításához.</p>
<p class="info">A jelszó módosításához töltse ki a jelszó mezőket is. Amennyiben nem tölti ki ezeket a jelszó minden esetben a régi marad.</p>