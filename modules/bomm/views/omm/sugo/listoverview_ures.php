<h1><?=$title?></h1>

<p>A hírlevél-listák képezik a KampányMenedzser adatbázisát. Itt vannak eltárolva a feliratkozók, itt állíthatja be, hogy a honlapunkon a látogatók milyen folyamatokon menjenek keresztül, miután feliratkoztak a listákra. Listákat hozhat létre, beállíthatja a feliratkozási és leiratkozási folyamatokat, űrlapokat generálhat, vagy akár más rendszerekből importálhat adatokat.</p>

<p>Ezen képernyőn egy áttekintést kap a meglévő listáiról, illetve új listát hozhat létre. Új lista létrehozásához nyomja meg az „Új hírlevél lista létrehozása” gombot. Meglévő lista kiválasztásához pedig kattintson a lista nevére. Kiválasztás után a „Lista előnézete” képernyőre kerül.</p>
