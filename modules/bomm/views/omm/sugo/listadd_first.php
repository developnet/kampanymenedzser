<h1><?=$title?></h1>

<p>Egy lista felvitelének első két lépése, hogy megadjuk a lista alapadatait, majd megadjuk a lista mezőit.</p>

<p>A lista neve bármi lehet, illetve egy lista nevét a későbbiek során bármikor módosíthatja. Fontos szem előtt tartani, hogy a lista neve lesz az, ami mindenhol azonosítani fogja a listát a képernyőn. Ez a név fog szerepelni a táblázatokban és címzési feltételekben, ezért adjon beszédes nevet a listának, hogy a későbbiek során könnyen be tudja azonosítani azt.</p>

<p>A lista típusa kétféle lehet. A „Szimpla Opt-in” listára feliratkozás során nem szükséges megerősítés, a látogató a feliratkozó űrlap segítségével azonnal aktív tagja lesz a listának. A „Kettős Opt-in” listánál azonban feliratkozás után egy – a „Lista feliratkozási folyamatának beállítása” képernyőn beállított – e-mailt küld ki a rendszer, melyben megerősítést kér a felhasználótól a feliratkozáshoz. A felhasználó csak a megerősítés után lesz aktív tagja a listának. A kettős opt-in előnye, hogy véd az ad hoc beírt e-mail címektől, hiszen  a megerősítő e-mail fogadásához működő, valós cím kell.</p>