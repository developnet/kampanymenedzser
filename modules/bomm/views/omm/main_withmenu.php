<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"/>
<title><?=$title ?> - <?=meta::getTitle($this->uri->segment(2,"ures")."/".$this->uri->segment(3,"")) ?></title>

<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

<script language="JavaScript" src="<?=$viewpath.$js ?>stats.js"></script>
<script>
 var URL_BASE = "<?=url::base() ?>";
</script>

		<link href="<?=$base.$css ?>pretty.css" media="screen,projection" rel="stylesheet" type="text/css" />
		<link href="<?=$base.$css ?>themes/rozsda.css" media="screen,projection" rel="stylesheet" type="text/css" />
		
		<link href="<?=$viewpath.$css ?>sugo.css" media="screen,projection" rel="stylesheet" type="text/css" />
 
		<?foreach ($javascripts as $js): ?>
    		<script language="JavaScript1.2" src="<?=$js['link']?>"></script>
		<?endforeach; ?>

		<?foreach ($stylesheets as $css): ?>
    		<link rel="stylesheet" type="text/css" href="<?=$css['link']?>" />
		<?endforeach; ?>


</head>
<body class="<?=$bodyClass?>">



<div id="sugo_dialog" title="Súgó" style="display:none">
	<div id="sugo_html"></div>
</div>

<div id="eszrevetelek_dialog" title="Észrevétel beküldése" style="display:none">
    <div class="" style="">
    
		<form action="Javascript:;" method="post" id="subscribe_form">
		<fieldset style="text-align:left">
		<p class="note" style="margin:0px;text-align:center;">Töltse ki az űrlapot az észrevétel elküldéséhez</p>
		<div>
			<label style="width:70px">Képernyő  </label> 
			<input id="note_sceen" name="kepernyo" value="<?=meta::getTitle($this->uri->segment(2,"ures")."/".$this->uri->segment(3,"")) ?>" type="text" class="" disabled="disabled" style="width:320px" />
		</div>

		<div>
			<label for="tipus" style="width:70px">Típus </label>
			<select  id="note_type" name="tipus" class="" style="width:320px" >
				<optgroup label="Észrevétel típusa">
		        	  		<option value="note">Megjegyzés</option>
		        	  		<option value="suggestion">Javaslat</option>
		        	  		<option value="question">Kérdés</option>
		        	  		<option value="bug">Hiba</option>
				</optgroup>
			</select>
		</div>
		<div>
			<label style="width:70px">Tárgy </label> 
			<input id="note_subject" name="targy" value="" type="text" class="" style="width:320px" />
		</div>
		<div>
			<label for="Uzenet" style="width:70px">Üzenet</label>
			<textarea id="note_message" name="Uzenet" cols="35" rows="5"  class="" style="width:320px"></textarea>
		</div>
		</fieldset>
		</form>	
	
	
    </div>
	
    <div class="delete_dialog_short_buttons" style="width:260px;">

        <div class="mybutton" id="sendingMessage">
            <button id="sendEszrevetel" type="submit" class="button">
                <img src="<?=$base.$img ?>icons/accept.png" alt=""/>Elküldöm
            </button>
       </div>	
    
        <div class="mybutton" id="sendingMessage">
            <button id="cancelEszrevetel" type="submit" class="button" style="margin-left:20px;float:left;">
                <img src="<?=$base.$img ?>icons/delete.png" alt=""/>Mégsem
            </button>
       </div>
       
    </div>	
	
</div>
	

<!-- FELSŐ MENÜ -->

<div id="adminNavsWrapper" title="Adminisztrációs funkciók">
	<div class="adminNavs">
	
		<?php if(sizeof($adminNavs)>0): ?>
			<ul>
				
			<?foreach ($adminNavs as $a): ?>
	    		<li id="<?=$a['id'] ?>"><a href="<?=$a['link'] ?>"><span><?=$a['name'] ?></span></a></li>
			<?endforeach; ?>	
				
			</ul>	
		
		<?php endif; //?>
	</div>
	
	<div class="adminNavs adminRight">
	<ul>
		<li><a href="Javascript:;" id="note_recieved" style="display:none;width:150px"><span style="color:red">Észrevétel elküldve!</span></a></li>
		<li><a href="Javascript:;" id="eszrevetelek"><span>Észrevételek</span></a></li>
		<?php /*
		       * <li><a href="<?=url::base()?>core/<?=KOHANA::config("bomm.felhasznkezkonyv")?>" target="_blank"><span>Felhasználói kézikönyv</span></a></li>
		     
				*/ ?>
		       
				<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
					<li><a href="<?=url::base()?>core/hireso_felhasznaloi_kezikony.pdf" target="_blank"><span>Felhasználói kézikönyv</span></a></li>
				<?php else: ?>
					
				<?php endif; ?>  		       
		       
		<li><a href="javascript:;" class="sugoClick" rel="<?=$this->uri->segment(2,"ures")."_".$this->uri->segment(3,"ures")?>"><span>Súgó</span></a></li>
		<li><?=$metainfo['logoutlink'] ?></li>
	</ul>
	</div>
</div> <!--adminNavsWrapper end-->


<div class="clear"></div>
<!-- FELSŐ VÉGE -->

<div id="case">

<!-- KÉK HEADER -->
<div class="shadowHeader">
<div class="navheaderleft">
<div class="shadowMidLeft">
<div class="navheader">

	<div id="header"><!--  -->
            <h1 style="padding:0;margin:0;padding-bottom:5px;padding-top:10px;padding-left:1px">
            	<a href="<?=url::site()?>" style="display:block;float:left;width:330px;">


				<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
					<img src="<?=$base.$img ?>hireso_garbaroyal_logo.png" width="300" />
				<?php else: ?>
					<img src="<?=$base.$img ?>kampanymenedzser_logo.png" width="300">
				<?php endif; ?>            		
            		
            		
            		
            	</a>
            	
				<?php	if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'): ?>
                <span id="clientName" style="display:block;float:left;width:300px;padding-top:20px;color: #b51819;">
                    Kiválasztott honlap: <?=$clientname ?>
                </span>
				<?php else: //?>
                <span id="clientName" style="display:block;float:left;width:300px;padding-top:35px">
                    Kiválasztott honlap: <?=$clientname ?>
                </span>
				<?php endif; ?>               	
            	

                <div style="clear:both"></div>
            </h1>
		
		<div id="primaryNavs">
			<div id="secondaryNavs">		
				<ul>
					
					<?php foreach ($secondaryNavs as $id => $n): ?>
						<li id="<?=$n['current'] ?>"><a id="<?=$id ?>" href="<?=$n['link'] ?>"><span><?=$n['name'] ?></span></a></li>
					<?php endforeach; ?>
					
				</ul>		
			</div>
		
			<ul>
				<?php foreach ($primaryNavs as $id => $n): ?>
					<li id="<?=$n['current'] ?>"><a id="<?=$id ?>" href="<?=$n['link'] ?>"><span><?=$n['name'] ?></span></a></li>
				<?php endforeach; ?>
			</ul>
		
		</div>
        <div class="clearMyHead"></div>
	</div>
</div></div></div></div>
<!-- KÉK HEADER VÉGE -->

<div class="shadowWrap">
<div class="shadowMidLeft">
<div class="shadowMidContent">

<!-- CONTENT -->
	<?=$pageContent ?>

<!-- CONTENT VÉGE -->

</div>
</div>
</div>
<div class="shadowBottomLeft"><div class="shadowMidLeft"></div></div>

</div> <!--case end-->
<div style="text-align:right;margin-right:40px">
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<code id="tooltiptest" style="font-size:11px">Verzió: <span><?=meta::getVersionString()?></span>,  Szerver idő: <span id="server_time_main"><?=date('Y.m.d H:i') ?></span></code>
	</div>


<!-- DEMO CONSOLE -->
<?php 
$development = false;
$urlParts = explode('.', $_SERVER['HTTP_HOST']);

if($urlParts[0] == 'dev'){
	$development = true;
}

$development = false;

if( $development ):?>
<style>
#demo_console{
	border: none;
	height: 146px;
	left: 0px;
	min-width: 900px;
	position: fixed;
	bottom: 0px;
	width: 100%;
	z-index: 50;
	background-image: url('<?=$base.$img ?>demo_console_back.png');
	color: white;
	font-family: Helvetica;
}

</style>

	<div id="demo_console">
		<h1>
			Üdvözöljük a KampányMenedzser szoftverben!
		</h1>
	</div>



<?php endif;?>
<!-- DEMO CONSOLE -->


<script language="javascript" type="text/javascript">
<!--
stats(612329);
//--></script> 



</body>
</html>
