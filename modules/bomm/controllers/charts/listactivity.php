<?php
class Listactivity_Controller extends Chartcontroller_Controller  {
	
	public function index(){
		
		$listId = $this->uri->segment(4,"");
		$today = date("Y-m-d");
		
		$showing = $this->uri->segment(5, "month");
		
		

		
		if($listId == ""){
			//url::redirect("/pages/listoverview");
		}else{
			
			$list = ORM::factory("omm_list")->find($listId);
			
			if($list->loaded){
				
				setlocale(LC_NUMERIC, 'en_US.UTF8');
					
				$data = array();
				$data2 = array();
				$labels = array();

				$db = new Database("own");	

				$morning = " 00:00:00";
				$evening = " 23:59:59";
				$tablename = Kohana::config("database.default");
				$tp = $tablename['table_prefix'];				
				
				
				if($showing == "month"){	
					
					$month = $this->uri->segment(6, date("Y-m"));
					$year = date("Y");

					$from = $month."-01"; 
					$to = date( "Y-m-d", strtotime( $from." +1 month" ));				
//					$to = date( "Y-m-d", strtotime( $to." -1 day" ));

					//echo $from." - ".$to;
					
				$act = $from;
				$max_sub = 0;
				$max_unsub = 0;
				$i = 1;
					
					while($act != $to){
						
						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE reg_date <= '".$act.$morning."' AND omm_list_id=".$list->id." ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE unsubscribe_date IS NOT NULL AND unsubscribe_date <= '".$act.$morning."' AND omm_list_id=".$list->id." ";
						$unsubscribed = $db->query($sql);
						$data2[$i] = $unsubscribed[0]->ossz;						
						
						$labels[$i] = $act;
						$i++;
						
						
						
						$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
					}
					
				}elseif($showing == "year"){
					$year = $this->uri->segment(6, date("Y"));
					
					for ($i = 1; $i <= 12; $i++){
						
						if($i<10) $ho = "0".$i;
						else $ho = $i;
						
						$from = $year."-".$ho."-01 00:00:00";
						$to = date( "Y-m-d", strtotime( $from." +1 month" ));		
						$to = date( "Y-m-d", strtotime( $to." -1 day" ))." 23:59:59";
						
						$labels[$i] = $year.".".$ho;
						
						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE reg_date <= '".$from."' AND omm_list_id=".$list->id." ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE unsubscribe_date IS NOT NULL AND unsubscribe_date <= '".$from."' AND omm_list_id=".$list->id." ";
						$unsubscribed = $db->query($sql);
						$data2[$i] = $unsubscribed[0]->ossz;						
						
						
					}
				}elseif($showing == "week"){
					
					$param = $this->uri->segment(6, '');
					
					if($param == ''){
						$param = dateutils::nrweekday(date("Y-m-d"));
						$param = date( "Y-m-d", strtotime( $param." -1 week" )); 						
					}
					
					$from = $param." 00:00:00";
					$to = $param." 23:59:59";
					
					for ($i = 1; $i <= 7; $i++){
						
						$labels[$i] = substr($from,0,10);
						
						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE reg_date <= '".$from."'  AND omm_list_id=".$list->id." ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE unsubscribe_date IS NOT NULL AND unsubscribe_date <= '".$from."' AND omm_list_id=".$list->id." ";
						$unsubscribed = $db->query($sql);
						$data2[$i] = $unsubscribed[0]->ossz;						
						
						$from = date( "Y-m-d", strtotime( $from." +1 day" ))." 00:00:00";
						$to = date( "Y-m-d", strtotime( $to." +1 day" ))." 23:59:59";
					}
				}elseif($showing == "day"){
					
					$param = $this->uri->segment(6, date( "Y-m-d"));
					
					$from = $param." 00:00:00";
					
					
					for ($i = 1; $i <= 24; $i++){

						$to = date( "Y-m-d H:i:s", strtotime( $from." +1 hour" ))."";

						$labels[$i] = substr($from,11,5)." - ".substr($to,11,5);
						
						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE reg_date <= '".$from."' AND omm_list_id=".$list->id." ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT count(id) as ossz FROM ".$tp."omm_list_member_connect WHERE unsubscribe_date IS NOT NULL AND unsubscribe_date <= '".$from."' AND omm_list_id=".$list->id." ";
						$unsubscribed = $db->query($sql);
						$data2[$i] = $unsubscribed[0]->ossz;							
						
						
						$from = $to;
						
					}
					
					
				}					
							
//				$from = $this->uri->segment(5, date( "Y-m-d", strtotime( $today." -1 month" )));

//				$act = $from;
//				$max_sub = 0;
//				$max_unsub = 0;
//				$i = 1;
//
//				while($act != $to){
//					
//					$stat = $db->from('omm_stat_lists')->where('day',$act)->where('omm_list_id',$list->id)->get();
//					
//					//print_r($stat[0]);
//
//					if(sizeof($stat) == 1){
//
//						$data[$i] = (int)$stat[0]->subscribed_today;
//						$data2[$i] = (int)$stat[0]->unsubscribed_today;							
//
//						if((int)$stat[0]->subscribed_today > $max_sub){
//							$max_sub = (int)$stat[0]->subscribed_today;
//						}
//
//						if((int)$stat[0]->unsubscribed_today > $max_unsub){
//							$max_unsub = (int)$stat[0]->unsubscribed_today;
//						}
//						
//					}else{
//						$data[$i] = 0;
//						$data2[$i] = 0;						
//					}
//					$labels[$i] = substr($act,5);
//					
//					$i++;
//					
//					$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
//				}
				
			
				
				$this->content_view ="list_report_01_data";
				$this->content['data'] = $data;
				$this->content['data2'] = $data2;
				$this->content['labels'] = $labels;
				
        		
			}else{
				//url::redirect("/pages/listoverview");	
			}
			
		}		
		
	}
	
}
?>