<?php
class Useractivity_Controller extends Chartcontroller_Controller  {
	
	public function index(){
		
		$listId = $this->uri->segment(4,"");
		$today = date("Y-m-d");
		
		$showing = $this->uri->segment(5, "month");
		
		$km_user = $this->uri->segment(7, USERNAME);
		$csv = $this->uri->segment(8, 0);

				
				setlocale(LC_NUMERIC, 'en_US.UTF8');
					
				$data = array();
				$data2 = array();
				$labels = array();

				$db = new Database();	

				$morning = " 00:00:00";
				$evening = " 23:59:59";
				
				$tablename = Kohana::config("database.default");
				$tp = $tablename['table_prefix'];				
				
				
				if($showing == "month"){	
					
					$month = $this->uri->segment(6, date("Y-m"));

					$from = $month."-01"; 
					$to = date( "Y-m-d", strtotime( $from." +1 month" ));				
//					$to = date( "Y-m-d", strtotime( $to." -1 day" ));

					//echo $from." - ".$to;
					
				$act = $from;
				$max_sub = 0;
				$max_unsub = 0;
				$i = 1;
					
					while($act != $to){

						list($year,$month,$day) = explode("-",$act);
						
						$sql = "SELECT sum(count) as ossz FROM ".$tp."omm_sendcounts WHERE year = '".$year."' AND month = '".$month."' AND day = '".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						
						$sql = "SELECT max(count) as ossz FROM ".$tp."omm_emailcounts WHERE day = '".$year."-".$month."-".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data2[$i] = $subscribed[0]->ossz;						
						
						
						$labels[$i] = $act;
						$i++;
						
						$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
					}
					
				}elseif($showing == "year"){
					$year = $this->uri->segment(6, date("Y"));
					
					for ($i = 1; $i <= 12; $i++){
						
						if($i<10) $ho = "0".$i;
						else $ho = $i;
						
						$labels[$i] = $year.".".$ho;
						
						$sql = "SELECT sum(count) as ossz FROM ".$tp."omm_sendcounts WHERE year = '".$year."' AND month = '".$i."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;
						
						$sql = "SELECT max(count) as ossz FROM ".$tp."omm_emailcounts WHERE day LIKE '".$year."-".$ho."%' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data2[$i] = $subscribed[0]->ossz;								
						

					}
				}elseif($showing == "week"){
					
					$param = $this->uri->segment(6, '');
					
					if($param == ''){
						$param = dateutils::nrweekday(date("Y-m-d"));
						$param = date( "Y-m-d", strtotime( $param." -1 week" )); 						
					}
					
					$act = $param;
					for ($i = 1; $i <= 7; $i++){

						list($year,$month,$day) = explode("-",$act);
						
						$labels[$i] = $act." ".$i;
						
						$sql = "SELECT sum(count) as ossz FROM ".$tp."omm_sendcounts WHERE year = '".$year."' AND month = '".$month."' AND month = '".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT max(count) as ossz FROM ".$tp."omm_emailcounts WHERE day = '".$year."-".$month."-".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data2[$i] = $subscribed[0]->ossz;							
						
						$act = date( "Y-m-d", strtotime( $act." +1 day" ))."";
						
					}
				}elseif($showing == "day"){
					
					$param = $this->uri->segment(6, date( "Y-m-d"));
					
					list($year,$month,$day) = explode("-",$param);
					for ($i = 0; $i < 24; $i++){
						
						$labels[$i] = $param." ".$i;
						
						$sql = "SELECT sum(count) as ossz FROM ".$tp."omm_sendcounts WHERE year = '".$year."' AND month = '".(int)$month."'  AND day = '".(int)$day."' AND hour = '".$i."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data[$i] = $subscribed[0]->ossz;

						$sql = "SELECT max(count) as ossz FROM ".$tp."omm_emailcounts WHERE day = '".$year."-".$month."-".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$data2[$i] = $subscribed[0]->ossz;													
						
						
					}
					
					
				}					
							
//				$from = $this->uri->segment(5, date( "Y-m-d", strtotime( $today." -1 month" )));

//				$act = $from;
//				$max_sub = 0;
//				$max_unsub = 0;
//				$i = 1;
//
//				while($act != $to){
//					
//					$stat = $db->from('omm_stat_lists')->where('day',$act)->where('omm_list_id',$list->id)->get();
//					
//					//print_r($stat[0]);
//
//					if(sizeof($stat) == 1){
//
//						$data[$i] = (int)$stat[0]->subscribed_today;
//						$data2[$i] = (int)$stat[0]->unsubscribed_today;							
//
//						if((int)$stat[0]->subscribed_today > $max_sub){
//							$max_sub = (int)$stat[0]->subscribed_today;
//						}
//
//						if((int)$stat[0]->unsubscribed_today > $max_unsub){
//							$max_unsub = (int)$stat[0]->unsubscribed_today;
//						}
//						
//					}else{
//						$data[$i] = 0;
//						$data2[$i] = 0;						
//					}
//					$labels[$i] = substr($act,5);
//					
//					$i++;
//					
//					$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
//				}
				
			
				if($csv == 1){
					$this->content_view ="list_report_01_data_csv";
				}else{
					$this->content_view ="list_report_01_data";	
				}
				$this->content['km_user'] = $km_user;
				$this->content['data'] = $data;
				$this->content['data2'] = $data2;
				$this->content['labels'] = $labels;
				
        		
		
	}
	
}
?>