<?php
class Ommpage_Controller extends Admin_Controller{
	
	var $pageview = "clients_overview";
	var $pagetemplate;
	var $maintemplate;
	
	
	public function __construct(){
		$this->main_template = $this->maintemplate;
		parent::__construct();
	}

	public function init(){
		$this->pagetemplate = new View(Kohana::config('admin.theme')."/pages/".$this->pageview);	
		
		$this->pagetemplate->css = $this->template->css;
		$this->pagetemplate->img = $this->template->img;
		$this->pagetemplate->js = $this->template->js;
		$this->pagetemplate->swf = $this->template->swf;
		$this->pagetemplate->base = $this->template->base;
		
		$this->pagetemplate->assets = $this->template->assets;
		
		
	}
	
	public function render(){
		$this->template->pageContent = $this->pagetemplate->render(FALSE,FALSE);
	}
	
}

?>