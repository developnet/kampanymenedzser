<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    admin
 * @author     mberta
 * @copyright  (c) 2008
 */
abstract class Chartcontroller_Controller extends Controller {

   	// Template view name
	public static $admin_role = "admin";
	
	public $main_template = "empty";
	public $template = 'main_view';
	
	public $pagetemplate;
	
	public $auth;
	
	public $content_view = "";
	public $content = array();
	
	// Default to do auto-rendering
	public $auto_render = TRUE;


	public function __construct()
	{
		parent::__construct();
		$this->session = Session::instance();
			
		$this->auth = Auth::instance();
		$this->_checkLogin();		
		
		
        
		$this->template = new View(Kohana::config('admin.theme')."/".$this->main_template);
		
        
		if ($this->auto_render == TRUE)
		{
			// Render the template immediately after the controller method
			Event::add('system.post_controller', array($this, '_render'));
		}
	}

	
	/**
	 * Render the loaded template.
	 */
	public function _render()
	{
		if ($this->auto_render == TRUE)
		{
        	$this->pagetemplate = new View_Core(Kohana::config('admin.theme')."/reports/".$this->content_view, $this->content);	
			
        	
        	
			$this->template->content = $this->pagetemplate->render(FALSE);
			
	        // Render the template when the class is destroyed
			$this->template->render(TRUE);
		}
	}

	private function _checkLogin(){
		
		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
			
			url::redirect('login');
			
		}
	}
	
	

} // End Template_Controller