<?php defined('SYSPATH') or die('No direct script access.');

	function errorHandler($code, $string, $file, $line){
		$ret = array();
		$ret['REQUEST'] = 'ERROR';
		$ret['code'] = $code;
		$ret['string'] = $string;
		$ret['file'] = $file;
		$ret['line'] = $line;

		echo json_encode($ret);
		KOHANA::shutdown();
		die();

	}
	
/**
 */
class Publicapi_Controller extends Apicontroller_Controller {

	private $ret = array();
	private $db;
	private $dbcore;
	private $km_user;
	private $km_user_code;

	public function __construct(){
		parent::__construct();
		set_error_handler('errorHandler', E_ALL); 
	}
	


	
	
	
	private function start(){
		

		
		if(!isset($_POST['apiuser']) || !isset($_POST['apikey']) || strlen($_POST['apikey']) != 32){
			$this->ret = array('REQUEST' => 'OK',
	        			 	   'STATUS'  => 'ERROR00',
	        			 	   'MSG'  => 'Érvénytelen API kulcs.'	
	        			 	   );
	        			 	   $this->close();
		}else{
			$this->dbcore = new Database("");
			$apikey = $_POST['apikey'];
			$acc = $this->dbcore->query("SELECT km_user,code FROM omm_accounts WHERE apikey = '".$apikey."' AND km_user = '".$_POST['apiuser']."' ");

			if(sizeof($acc) == 0){

				$this->ret = array('REQUEST' => 'OK',
	        				       'STATUS'  => 'ERROR01',
	        				       'MSG'  => 'Érvénytelen API kulcs.'	
	        				       );

	        				       $this->close();
			}

			$user = $acc[0]->km_user;
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;
			Kohana::config_set('database.own',$dbown);

			$this->km_user = $user;
			$this->km_user_code = $acc[0]->code;
			$this->db = new Database("own");
		}
	}

	private function close(){
		echo json_encode($this->ret);
		KOHANA::shutdown();
		die();
	}


	public function showForm($km_user_code,$code){
		
		//$cache = new Memcached('public_forms');

		//if($cache->get($km_user_code.'_'.$code)){
		//	echo $cache->get($km_user_code.'_'.$code);
		//	die();
		//}

		$dbcore = new Database("");
		$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$km_user_code."' ");
		
		if(sizeof($acc) == 0){
			echo "Invalid user code.";
			die();
		}

		$user = $acc[0]->km_user;
		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;
		Kohana::config_set('database.own',$dbown);



		$f = ORM::factory('omm_list_form')->where('code',$code)->find();

		$assets = Kohana::config('core.assetspath').'formassets/';

		if($f->form_template == 'form_layout_4'){
			$header = '';
			$footer = '';
		}else{

			$header = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
					   <html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<title>Untitled Document</title>
						</head>
					<body>';

			$footer = '</body></html>';

		}



		if($f->loaded){//
			$html = $header.$f->getHtml(true, url::site().'api/subscribe?a='.$km_user_code, null, $km_user_code).$footer;
			
			//$cache->set($km_user_code.'_'.$code,$html);

			echo $html;
		}
	}	
	

	


	public function index(){
		$this->start();
			
		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'NOTHING';
		$this->ret['MSG'] = '';

		$this->close();
	}

	private function decode($string){
		
	}
	
	


}

?>