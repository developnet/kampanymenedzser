<?php
class Kmadmin_Controller extends Clicontroller_Controller {

	var $versionstring = "km_dev_version";
	
	public function testURL(){
		echo url::base();
	}
	public function testSMTPS(){//
		
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 1 teszt','SMTP 1 teszt',array('bertamarton@gmail.com'),'188.227.227.171');
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 2 teszt','SMTP 2 teszt',array('bertamarton@gmail.com'),'188.227.227.172');
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 3 teszt','SMTP 3 teszt',array('bertamarton@gmail.com'),'188.227.227.173');
		
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 4 teszt','SMTP 4 teszt',array('bertamarton@gmail.com'),'188.227.227.174');	
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 5 teszt','SMTP 5 teszt',array('bertamarton@gmail.com'),'188.227.227.175');
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 6 teszt','SMTP 6 teszt',array('bertamarton@gmail.com'),'188.227.227.176');

		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 7 teszt','SMTP 7 teszt',array('bertamarton@gmail.com'),'188.227.227.177');	
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 8 teszt','SMTP 8 teszt',array('bertamarton@gmail.com'),'188.227.227.178');
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 9 teszt','SMTP 9 teszt',array('bertamarton@gmail.com'),'188.227.227.179');		
		jobs::sendEmail('info@kampanymenedzser.hu','SMTP 10 teszt','SMTP 10 teszt',array('bertamarton@gmail.com'),'188.227.227.180');		
	}
	
	public function nagykerRelativ(){
		$today = date('Y-m-d');
		$km_user = 'nagykermarketing';
		
		$db = new Database();
		$db->query("use km_".$km_user);		
		
		
		
		//Nagyker relativot kapja 96 Nagykereskedelmi relatív sorozat 41
		$sql = "DELETE FROM omm_tag_objects where tag_id = 96";
		$db->query($sql);		
		
		$from = date( "Y-m-d", strtotime( $today." -81 days" ));	
		
		$sql = "SELECT m.id as id FROM omm_list_members m, omm_list_member_connect l WHERE l.omm_list_member_id = m.id AND l.omm_list_id = 41 AND l.reg_date > '".$from."' ";
		
		$membs = $db->query($sql);
		
		foreach($membs as $m){
			
			$sql = "INSERT INTO omm_tag_objects SET tag_id = 96, object_type = 'member', object_id = ".$m->id." ";
			$db->query($sql);
			
		}
		
		//Kisker relativot kapja 97 Kiskereskedelmi relatív sorozat 70
		$sql = "DELETE FROM omm_tag_objects where tag_id = 97";
		$db->query($sql);		
		
		$from = date( "Y-m-d", strtotime( $today." -81 days" ));	
		
		$sql = "SELECT m.id as id FROM omm_list_members m, omm_list_member_connect l WHERE l.omm_list_member_id = m.id AND l.omm_list_id = 70 AND l.reg_date > '".$from."' ";
		
		$membs = $db->query($sql);
		
		foreach($membs as $m){
			
			$sql = "INSERT INTO omm_tag_objects SET tag_id = 97, object_type = 'member', object_id = ".$m->id." ";
			$db->query($sql);
			
		}		
		
		
		
		
		$egyeb = 1;
		$kisker = 2;
		$nagyker = 3;
		$gyarto = 4;
		
		$nincs_cegtipus = 5;
		
		$sql = "select 
					(SELECT count(id) FROM omm_tag_objects where tag_id IN(1,2,3,4,5,78) and m.id = object_id and object_type = 'member') as count,
					m.id 
				from omm_list_members m 
				where
					m.status != 'test'
				AND
					(SELECT count(id) FROM omm_tag_objects where tag_id IN(1,2,3,4,5,78) and m.id = object_id and object_type = 'member') = 0";
		
		$membs = $db->query($sql);	

		foreach($membs as $m){
			
			$sql = "INSERT INTO omm_tag_objects SET tag_id = 5, object_type = 'member', object_id = ".$m->id." ";
			$db->query($sql);
			
		}

		$sql = "select 
					(SELECT count(id) FROM omm_tag_objects where tag_id IN(1,2,3,4,5,78) and m.id = object_id and object_type = 'member') as count,
					m.id 
				from omm_list_members m 
				where
					m.status != 'test'
				AND
					(SELECT count(id) FROM omm_tag_objects where tag_id IN(1,2,3,4,78) and m.id = object_id and object_type = 'member') > 0
				AND 
					(SELECT count(id) FROM omm_tag_objects where tag_id IN(5) and m.id = object_id and object_type = 'member') > 0
						";
			$membs = $db->query($sql);	

		foreach($membs as $m){
			
			$sql = "DELETE FROM omm_tag_objects WHERE tag_id = 5 AND object_type = 'member' AND object_id = ".$m->id." ";
			$db->query($sql);
			
		}		
		
		
	}
	
	public function sendHiresoStat(){
		//$to = '2013-07-01';

		$today = date('Y-m-d');
		$showing = "month";
		
		$__month = date("Y-m");
		$_month = date( "Y-m", strtotime( $__month." -1 month" ));	
		
		//$from = $_month."-01"; 
		$from = date( "Y-m-d", strtotime( $today." -2 month" ));	
		//$to = date( "Y-m-d", strtotime( $from." +1 month" ));			
		$to = $today;
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];			
		
		$db = new Database();
		$res = $db->from("omm_hireso")->orderby('km_user')->get();		
		$br = "\n";
		$csv = '"Fiók";"Dátum";"Napi kiküldések";"Aktív feliratkozó szám"'.$br;
		foreach ($res as $u){
			
			$km_user = $u->km_user;
			
				$act = $from;

					while($act != $to){

						list($year,$month,$day) = explode("-",$act);
						
						$sql = "SELECT sum(count) as ossz FROM ".$tp."omm_sendcounts WHERE year = '".$year."' AND month = '".$month."' AND day = '".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$send = $subscribed[0]->ossz;

						
						$sql = "SELECT max(count) as ossz FROM ".$tp."omm_emailcounts WHERE day = '".$year."-".$month."-".$day."' AND km_user = '".$km_user."' ";
						$subscribed = $db->query($sql);
						$count = $subscribed[0]->ossz;						
						$csv .= '"'.$km_user.'";"'.$act.'";"'.$send.'";"'.$count.'"'.$br;
						
						$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
						
					}			
					
					
		}	

		//echo $csv;
		
		$time = time();
		
		file_put_contents("/var/www/kampanymenedzser/www/hireso/upload/hiresolog_".$today."_".$time.".csv", $csv);

		$log_link = 'https://hireso.garbaroyal.hu/upload/hiresolog_'.$today.'_'.$time.'.csv';
		
		
		$msg = "<h2>Híreső felhasználó statisztika (".$today.")<h2>".$br.$br;
		$msg .= "<h3>Felhasználók:</h3>".$br;
		foreach ($res as $u){
			$msg .= "   <strong>".$u->km_user."</strong> (".$u->start.")<br>".$br;	
		}
		$msg .= $br.$br;
		$msg .= "<h3>Havi statisztika:</h3>".$log_link;
		//
		jobs::sendEmail('support@kampanymenedzser.hu','Híreső felhasználó statisztika ('.$today.')',$msg,array('bertamarton@gmail.com','perge.peter@garbaroyal.hu'),'188.227.227.172');
		//echo $log;  
		//php /var/www/kampanymenedzser/km_dev_version/cronapp/index.php api/kmadmin/sendHiresoStat
		
	}
	
	public function userBounceFetch(){
		$day = date('Y-m-d');
		$hour = date('H:i');		
		$message = "<h1>[KM BOUNCEDETECT USERFETCH] $day $hour<br/></h1>";
		$db = new Database();
		$res = $db->from("omm_accounts")->where('status','1')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			//$message .= ''.$u->km_user.':'.$this->br;

			$out = array();

			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userBounceFetch/".$u->km_user,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}		
		

		$from = 'noreply@kmsrv2.hu';
		$subject = "[KM BOUNCEDETECT USERFETCH] $day $hour";
		//$to = array('support@aktivhonlapok.hu');
		$to = array('bertamarton@gmail.com');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");

		jobs::sendEmail($from,$subject,$message,$to);

		jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"end","");			
		
	} 
	
	public function bounceDetect(){
		$day = date('Y-m-d');
		$hour = date('H:i');
 
		$message = "<h1>[KM BOUNCEDETECT CRON] $day $hour<br/></h1>";

		/////////////// bejövő mailok feldolgozása, file szinten
		$dir = "/var/www/kampanymenedzser/bounce/new";
		//$dir2 = "/var/www/kampanymenedzser/bounce/new2";

		$copy = "/var/www/kampanymenedzser/bounce/archive/siker";
		$copy_error = "/var/www/kampanymenedzser/bounce/archive/error";
		$copy_ismeretlen = "/var/www/kampanymenedzser/bounce/archive/unknown";//
		$copy_autoreply = "/var/www/kampanymenedzser/bounce/archive/autoreply";

		$db = new Database();

		$process_id = jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"start");

		
		$cur = fileutils::dir_list($dir, true);

		$ossz = 0;
		$siker = 0;
		$error = 0;
		$unknown = 0;
		$autoreply = 0;
		
		foreach ($cur as $c){/////fájlokon végigmegyünk

			$r = $this->advancedBounceProcess($dir."/".$c['name'],$db);
			$ossz++;
			
			if(isset($r['job_id']) && $r['job_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){////ha vannak adatok

				if($r['km_user'] == "autoreply"){////ha autoreply akkor elvetjük és bemásoljuk az autoreplykhez
					$autoreply++;
					copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
				}else{////visszapattano insert
					
					
					if(!isset($r['type']) ) $r['type'] = 'temp';
					
					if($r['type'] == 'resend') $r['type'] = 'temp';
					
					$siker++;
					$status = $db->insert('omm_bounces', array('omm_job_id' => $r['job_id'], 'type' => $r['type'], 'dict_id' => $r['dict_id'], 'km_user' => $r['km_user']));

					if($r['dict_id'] == 0){ ////szótár nem ismerü külön másoljuk, hogy lehessen okosítani
						$unknown++;
						copy($dir."/".$c['name'], $copy_ismeretlen."/".$c['name']);
					}else{
						copy($dir."/".$c['name'], $copy."/".$c['name']);
					}

				}

			}else{ ////nem tudtuk parsolni

				if(isset($r['km_user']) && $r['km_user'] == "autoreply"){
					$autoreply++;
					copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
				}else{
					$error++;
					$message .= "error:".$c['name'].$this->br."<br/>";
					copy($dir."/".$c['name'], $copy_error."/".$c['name']);
				}
			}
				
				
			unlink($dir."/".$c['name']); ////töröljük az inboxból

		}
		echo "Össz: ".$ossz." | ";
		echo "Sike: ".$siker." | ";		
		echo "Unkn: ".$unknown." | ";		
		echo "Erro: ".$error." | ";
		echo "Auto: ".$autoreply." | ";
		//////////////
		/////////////userek megnézik van-e nekik új bounce - db


		$res = $db->from("omm_accounts")->where('status','1')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			//$message .= ''.$u->km_user.':'.$this->br;

			$out = array();

			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userBounceFetch/".$u->km_user,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}




		$from = 'noreply@kmsrv2.hu';
		$subject = "[KM BOUNCEDETECT CRON] $day $hour";
		//$to = array('support@aktivhonlapok.hu');
		$to = array('bertamarton@gmail.com');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");

		jobs::sendEmail($from,$subject,$message,$to);

		jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"end",$process_id);				
	}
	
	
	public function advancedBounceProcess($file,$db){
		
		require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."rfc822_addresses.php";
		require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."mime_parser.php";
		
		
		$mime=new mime_parser_class;
	
		
		/*
		 * Set to 0 for parsing a single message file
		 * Set to 1 for parsing multiple messages in a single file in the mbox format
		 */
		$mime->mbox = 0;
		$mime->decode_bodies = 1;
		$mime->ignore_syntax_errors = 1;
		$mime->track_lines = 1;		
		
		
		//$new = fileutils::dir_list($copy_ismeretlen, true);
		//$new = fileutils::dir_list($copy_stat, true);
		
		
		$return = array();
		$return['dict_id'] = 0;
		$ossz = 0;
		$i = 0;
		$j = 0;
		$nn = 0;
		$x = 0;
		$y = 0;
		$dicts = array();
		$dictsid = array();
			
			$x++;
			
			//if($x == 1000) break;
			
			$decoded = array();//
			$mime->Decode(array('File'=>$file), $decoded); //,'SkipBody'=>0
			
			//print_r($decoded[0]['Headers']['to:']."\n\r");//
			
			if(isset($decoded[0]['Headers']['to:'])){
				$i++;
				if(is_array($decoded[0]['Headers']['to:'])){
					$to = $decoded[0]['Headers']['to:'][0];
				}else{
					$to = $decoded[0]['Headers']['to:'];
				}
				
				$string = trim($to);
				$string = trim($string, "<");
				$string = trim($string, ">");
				
				if(strstr($string,"@kmsrv2.hu") != false && strstr($string,"_") != false){

					$dd = explode("_", $string);
			
					$return['km_user'] = $this->getKmUserFromCode(trim($dd[0]));
					$return['job_id'] = trim($dd[1]);					

					
				}else{///azonosítatlan
					unset($mime);
					return $return;
				}
				
				
				if(isset($decoded[0]['Parts'][0]['Body'])){
					$type = array();
					$type = $this->getTypeFromHeader($decoded[0]['Parts'][0]['Body'],$db);
					
					if($type['dict_id'] == 0){ ///nincs még meg
						
						$type = array();
						if(isset($decoded[0]['Parts'][1]['Body'])){
							
							$type = $this->getTypeFromHeader($decoded[0]['Parts'][1]['Body'],$db);
							
							if($type['dict_id'] == 0){/////másodikból sem ismert, megy vissza ismeretlennel
								$j++;
								
								foreach($type as $key => $t){
									$return[$key] = $t;
								}
								
								//print_r($type);
								
								//break;								
							}else{ ///minden megvan, nem ismeretlen, a második partból lett meg

								foreach($type as $key => $t){
									$return[$key] = $t;
								}
							}
							
						}

					}else{ ///minden megvan, nem ismeretlen, már első partból megvan
						
						foreach($type as $key => $t){
							$return[$key] = $t;
						}
						
						
					}	
					
					
				}else{/////ismeretlen visszapattanó
					$nn++;
					$return['dict_id'] = 0;
				}
				
				
			}else{////azonosítatlan visszapattanó
				$y++;
			}
		
		
		unset($mime);
		return $return;
	}
	
	
	private function getKmUserFromCode($code){

		$dbcore = new Database("");
		$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$code."' ");

		if(sizeof($acc) > 0){
			$km_user = $acc[0]->km_user;
		}else{
			$km_user = "undefined";
		}
		unset($dbcore);
		return $km_user;
	}		
		
	private function getTypeFromHeader($header,$db){
		$return = array();
		$return['type'] = "temp";
		$return['dict_id'] = 0;

		$res = $db->from("omm_bounces_dict")->where('status','1')->orderby('ord')->get();

		foreach($res as $r){

			if(strstr($r->keyphrase,"+")){

				$keys = explode("[+]",$r->keyphrase);
				$van = true;
				foreach($keys as $key){
					if(strstr($header,$key) === false){
						$van = false;
						break;
					}
				}


				if($van){
					$return['type'] = $r->type;
					$return['dict_id'] = $r->id;
					$return['dict_reason'] = $r->reason;
					$return['dict_keyphrase'] = $r->keyphrase;
					return $return;
				}


			}else{
				if(strstr($header,$r->keyphrase) != false){
					$return['type'] = $r->type;
					$return['dict_id'] = $r->id;
					$return['dict_reason'] = $r->reason;
					$return['dict_keyphrase'] = $r->keyphrase;					
					return $return;
				}
			}
		}
		return $return;
	}	
	
	public function sendTest($job_id){
			
		$swift = jobs::getSwift(true);
		$job = ORM::factory('omm_job')->find($job_id);
		$jobs = array();
		$jobs[] = $job;
		$letter = ORM::factory('omm_letter')->find($job->omm_letter_id);
		$db = new Database("own");
		jobs::sendJobs($jobs,$letter->type,$swift, $db);		
		$swift->disconnect();
		
	}
	
	
	
	public function global_fields($km_user){
		Benchmark::start('global_fields');	
		
		$db = new Database();
		$db->query("use km_".$km_user);

		$sql = "SELECT * FROM omm_list_fields";
		$fields = $db->query($sql);		
		
		$sql = "SELECT * FROM omm_list_members";
		$members = $db->query($sql);
		foreach($members as $m) {
			//$sql = 'DELETE FROM omm_list_member_datas where omm_list_field_id = 63 and omm_list_member_id = '.$m->id.' limit 1,100';
			
			
			foreach($fields as $f){
				$sql = 'SELECT id FROM omm_list_member_datas where omm_list_field_id = '.$f->id.' and omm_list_member_id = '.$m->id.' limit 1,100';
				$fordel = $db->query($sql);
				
				foreach($fordel as $d){
					$db->query('DELETE FROM omm_list_member_datas WHERE id='.$d->id.'');
				}
				
			}
			
			
			
		}
		
		
		
	}
	
	public function migrateMembersTo20($km_user){
		Benchmark::start('migrateMembersTo20');	
		
		$db = new Database();
		$db->query("use km_".$km_user);
		
		
		$sql = "SELECT DISTINCT email FROM omm_list_members WHERE omm_list_id != 0";
		$emails = $db->query($sql);
		
		$sql = "SELECT MAX( id ) as max FROM `omm_list_members` ";
		$res = $db->query($sql);
	
		if($res[0]->max > 0 && $res[0]->max < 100)
			$newauto = 100;
		elseif($res[0]->max > 100 && $res[0]->max < 1000)
			$newauto = 1000;
		elseif($res[0]->max > 1000 && $res[0]->max < 10000)	
			$newauto = 10000;		
		elseif($res[0]->max > 10000 && $res[0]->max < 100000)	
			$newauto = 100000;
		elseif($res[0]->max > 100000 && $res[0]->max < 1000000)	
			$newauto = 1000000;
		elseif($res[0]->max > 1000000 && $res[0]->max < 10000000)	
			$newauto = 10000000;
		elseif($res[0]->max > 10000000 && $res[0]->max < 100000000)	
			$newauto = 10000000;			
			
		$autoincrsql = "ALTER TABLE omm_list_members_new AUTO_INCREMENT = ".$newauto." ";
		$res = $db->query($autoincrsql);
		
		
		$first = true;
		foreach($emails as $email) {

			$newinsertsql = "INSERT INTO omm_list_members_new 
									SET email='".$email->email."', 
										status='active', 
										code='".string::random_string('unique')."',
										mod_user_id = 1
										";

			$res = $db->query($newinsertsql);
			$newid = $res->insert_id();				
				
			
			$sql2 = "SELECT * FROM omm_list_members WHERE email LIKE '".$email->email."'";
			$oldmembers = $db->query($sql2);
			foreach($oldmembers as $old) {

				$connectsql = "
							INSERT INTO omm_list_member_connect SET
								id = ".$old->id.",
								omm_list_id = ".$old->omm_list_id.",
								omm_list_member_id = ".$newid.",
								reg_date = '".$old->reg_date."',
								activation_date = '".$old->activation_date."',
								unsubscribe_date = '".$old->unsubscribe_date."',
								status = '".$old->status."',
								code = '".$old->code."',
								omm_list_form_id = '".$old->omm_list_form_id."',
								mod_date = '".$old->mod_date."',
								mod_user_id = '".$old->mod_user_id."'
				";
				
				try {
					$res = $db->query($connectsql);	
				}catch(Exception $e){
					//var_dump($e);
					echo $email->email."\n";
					
				}
				
				
				$updatedata = "UPDATE omm_list_member_datas SET omm_list_id = ".$old->omm_list_id." WHERE omm_list_member_id = ".$old->id."  ";
				$res = $db->query($updatedata);
				
				$updatedata2 = "UPDATE omm_list_member_datas SET omm_list_member_id = ".$newid." WHERE omm_list_member_id = ".$old->id."  ";
				$res = $db->query($updatedata2);				
				
				
				
				
			}
			
			
			
		}
		
		Benchmark::stop('migrateMembersTo20');
		var_dump(Benchmark::get('migrateMembersTo20'));
		
		/*
		
		Loop

			
			DELETE FROM omm_list_members WHERE id = id LIMIT 1
			*/
		
	}
	
	public function migrateMembersTo20_unsubscribe_date($km_user){

		$db = new Database();
		$db->query("use km_".$km_user);			
		
		$sql = "SELECT * FROM omm_list_members";
		$members = $db->query($sql);
		foreach($members as $m) {
			
			$sql2 = "SELECT m.id as id, (select min(reg_date) from omm_list_member_connect where omm_list_member_id = m.id AND omm_list_id = 53) as reg_date FROM omm_list_members m WHERE m.id= ".$m->id." ";
			$reg_date = $db->query($sql2);
			
			if(isset($reg_date[0]) && $reg_date[0]->reg_date != NULL){
				$db->query("UPDATE omm_list_members SET status='unsubscribed', unsubscribe_date ='".$reg_date[0]->reg_date."' WHERE id = ".$m->id." LIMIT 1 ");
				var_dump($db);
			}
			
		}		
	}		
	
	public function migrateMembersTo20_subscribe_date($km_user){

		$db = new Database();
		$db->query("use km_".$km_user);			
		
		$sql = "SELECT * FROM omm_list_members";
		$members = $db->query($sql);
		foreach($members as $m) {
			
			$sql2 = "SELECT m.id as id, (select min(reg_date) from omm_list_member_connect where omm_list_member_id = m.id) as reg_date FROM omm_list_members m WHERE m.id= ".$m->id." ";
			$reg_date = $db->query($sql2);
			
			if(isset($reg_date[0])){
				$db->query("UPDATE omm_list_members SET reg_date ='".$reg_date[0]->reg_date."' WHERE id = ".$m->id." LIMIT 1 ");
			}
			
		}		
	}	
	
	public function migrateMembersTo20_fill_client_id($km_user){

		$db = new Database();
		$db->query("use km_".$km_user);			
		
		$sql = "SELECT * FROM omm_lists";
		$lists = $db->query($sql);
		foreach($lists as $list) {
			
			$client_id = $list->omm_client_id;
			
			$sql2 = "SELECT * FROM omm_list_member_connect WHERE omm_list_id = ".$list->id." ";
			$connects = $db->query($sql2);
			foreach($connects as $con) {
				$memupdate = $db->query("UPDATE omm_list_members SET omm_client_id = ".$client_id." WHERE id = ".$con->omm_list_member_id." ");
				
			}
		}		
	}
	
	public function migrateMembersTo20_fill_member_id($km_user){
		
			$db = new Database();
			$db->query("use km_".$km_user);				
		
			$sql = "SELECT * FROM omm_list_member_connect";
			$connects = $db->query($sql);
			
			$tables = array(
					'omm_jobs',
					'omm_letter_bounces',
					'omm_letter_link_clicks',
					'omm_letter_open_log'
					
					
			);
			
			foreach($connects as $con) {
				//
				foreach($tables as $t){
					$memupdate = $db->query("UPDATE ".$t." SET omm_list_member_id = ".$con->omm_list_member_id." WHERE omm_list_member_id = ".$con->id." ");	
				}
				
				
				
				
			}		
		
	}
	
	
	public function clearJobHtml(){
			
		Benchmark::start('cron');

		$db = Kohana::config('database.own');

		$dbcore = new Database();

		$res = $dbcore->from("users")->where('status','1')->groupby('km_user')->get();

		$users = array();
		foreach ($res as $u){
			if($u->km_user != "admin"){
				$users[] = $u->km_user;
			}
		}

		$db = new Database();
		//create

		foreach ($users as $u){
			
			
				$sql1 = "SHOW TABLE STATUS FROM km_".$u." WHERE name='omm_jobs'";
				$sql2 = "UPDATE ".'km_'.$u.".omm_jobs SET email_content_html = NULL, email_content_text = NULL WHERE status='sent';";
				$sql3 = "OPTIMIZE TABLE ".'km_'.$u.".omm_jobs ";
								
				$stat = $db->query($sql1);
				echo $u." omm_jobs data before: ".$stat[0]->Data_length."\n";

				$db->query($sql2);
				$db->query($sql3);
				
				$stat = $db->query($sql1);
				echo $u." omm_jobs data after : ".$stat[0]->Data_length."\n\n";				
				
			
			

		}
		
	}	
	

	public function checkMemberTables(){

		$d = date("Y-m-d");

		$db = new Database();
		$res = $db->from("users")->where('status','1')->groupby('km_user')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;
				
			$db->query("use km_".$u->km_user);

			//$count = $db->from("omm_list_member_datas")->where("omm_list_member_id",$member->id)->where("omm_list_field_id",$field->id)->count_records();

			$errors = $db->query("SELECT
									f.id as field_id, 
									m.id as member_id
									FROM omm_list_fields f, omm_list_members m
									WHERE
									f.omm_list_id = 1 AND m.omm_list_id = 1 AND
									(SELECT count(id) as ossz FROM omm_list_member_datas WHERE omm_list_field_id=f.id AND omm_list_member_id = m.id) > 1");

			foreach($errors as $e) {
				echo  $u->km_user."  "."member_id: ".$e->member_id." , field_id: ".$e->field_id."\n";
			}
		}
	}

	public function createUser(){


		$km_user = $this->uri->segment(4,"");
		$username = $this->uri->segment(5,"");
		$password = $this->uri->segment(6,"");

		if($username == "" || $password == "" || $km_user == ""){
			die("Empty username or password!".$this->br);
		}

		$db = new Database();

		$user = ORM::factory('user');

		if ( ! $user->username_exists($username)){
			$user->km_user = $km_user;
			$user->username = $username;
			$user->password = $password;
			$user->code = string::random_string('unique');
			$user->status = 1;
			$user->datum = date("Y-m-d");

			if ($user->save() AND $user->add(ORM::factory('role', 'login')) AND  $user->add(ORM::factory('role', 'accountadmin')) ){
				echo "User '$username' created for '$km_user'!".$this->br;
			}
		}else{
			die("User '$username' already exist!".$this->br);
		}

	}

	public function backupfilesIncrement(){
		$day = date('Y-m-d');
		$hour = date('H:i');	
		$d = date("Ymd_His");
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM FILES BACKUP INC $day $hour",getmypid(),"start");
		
		
		exec ("find /var/kampanymenedzser/www/application -type f -mtime -2 -mtime +0 | zip -@ /var/archive/filebackups/km_files_i_".$d.".zip");
		exec("gzip /var/archive/filebackups/km_files_i_".$d.".zip");
		
		$this->ftp_copy("/var/archive/filebackups/km_files_i_".$d.".zip.gz","km_file_backups/km_files_i_".$d.".zip.gz");
		
		jobs::logCron($dbcore,"KM FILES BACKUP INC $day $hour",getmypid(),"end", $process_id);
	
	}
	
	public function backupfiles(){
		$day = date('Y-m-d');
		$hour = date('H:i');	
		$d = date("Ymd_His");
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM FILES BACKUP $day $hour",getmypid(),"start");
		
		exec("zip -9 -r /var/archive/filebackups/km_files_".$d.".zip /var/kampanymenedzser/www/application");
		exec("gzip /var/archive/filebackups/km_files_".$d.".zip");
		
		$this->ftp_copy("/var/archive/filebackups/km_files_".$d.".zip.gz","km_file_backups/km_files_".$d.".zip.gz");
		
		jobs::logCron($dbcore,"KM FILES BACKUP $day $hour",getmypid(),"end", $process_id);
	}
	
	public function backupdbs(){
		
		//exec("php /var/www/kampanymenedzser/km_dev_version/cronapp/index.php api/kmadmin/clearJobHtml");
		
		$day = date('Y-m-d');
		$hour = date('H:i');			
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM DB BACKUP $day $hour",getmypid(),"start");
		
		$db = new Database();

		$res = $db->from("omm_accounts")->where('status','1')->get();

		
		$d = date("Ymd_His");
		foreach ($res as $u){
			if($u->km_user != "admin")
			exec("mysqldump -uroot -pba74tReWr5wU --databases km_".$u->km_user." > /home/developnet/tmp/km_".$u->km_user."_".$d.".sql ");
			exec("zip /home/developnet/tmp/km_".$u->km_user."_".$d.".zip /home/developnet/tmp/km_".$u->km_user."_".$d.".sql");
			exec("rm /home/developnet/tmp/km_".$u->km_user."_".$d.".sql");
		}

		exec("mysqldump -uroot -pba74tReWr5wU --databases km_core > /home/developnet/tmp/km_core_".$d.".sql ");
		exec("zip /home/developnet/tmp/km_core_".$d.".zip /home/developnet/tmp/km_core_".$d.".sql");
		exec("rm /home/developnet/tmp/km_core_".$d.".sql");
		
		exec("zip /var/www/kampanymenedzser/backup/km_dbbackup_".$d." /home/developnet/tmp/*.zip");
		exec("rm /home/developnet/tmp/*.zip");

		//$this->ftp_copy("/var/archive/dbbackups/archive/km_dbbackup_".$d.".zip","km_db_backups/km_dbbackup_".$d.".zip");
		
		jobs::logCron($dbcore,"KM DB BACKUP $day $hour",getmypid(),"end", $process_id);
		
	}

	private function generateCode(){
		$d = date("Y-m-d");

		$db = new Database();
		$db->query("use km_core");
		
		$res = $db->from("omm_accounts")->get();
		
		foreach ($res as $u){
			if($u->km_user == "admin") continue;
			$db->query("UPDATE omm_accounts SET code = '".string::random_string()."' WHERE id = ".$u->id." LIMIT 1");
		}



	}

	public function countEmailsHistory($d){
		
		$day = date('Y-m-d');
		$hour = date('H:i');			
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM COUNT EMAILS $day $hour",getmypid(),"start");
		
		//$d = date("Y-m-d");

		$db = new Database();
		$res = $db->from("users")->where('status','1')->groupby('km_user')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			$db->query("use km_".$u->km_user);

			$sql = "SELECT count(distinct m.email) as ossz
						FROM omm_list_members m WHERE m.status = 'active' AND m.reg_date <= '".$d." 23:59:59' ";		
			$emails = $db->query($sql);

			$count = $emails[0]->ossz;

			$db->query("use km_core");

			$db->query("INSERT INTO omm_emailcounts SET km_user ='".$u->km_user."', day = '".$d."', count = ".$count." ");
		}
		
		jobs::logCron($dbcore,"KM COUNT EMAILS $day $hour",getmypid(),"end", $process_id);
		
	}	
	
	public function countEmails(){
		
		$day = date('Y-m-d');
		$hour = date('H:i');			
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM COUNT EMAILS $day $hour",getmypid(),"start");
		
		$d = date("Y-m-d");

		$db = new Database();
		$res = $db->from("users")->where('status','1')->groupby('km_user')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			$db->query("use km_".$u->km_user);

			$sql = "SELECT count(distinct m.email) as ossz
						FROM omm_list_members m WHERE m.status = 'active' ";		
			$emails = $db->query($sql);

			$count = $emails[0]->ossz;

			$db->query("use km_core");

			$db->query("INSERT INTO omm_emailcounts SET km_user ='".$u->km_user."', day = '".$d."', count = ".$count." ");
		}
		
		jobs::logCron($dbcore,"KM COUNT EMAILS $day $hour",getmypid(),"end", $process_id);
		
	}

	public function countSendsIntervall($from,$to){

		exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/kmadmin/countSends/".$from);
		$from = date( "Y-m-d", strtotime( $from." +1 day" ))."";

		while($from != $to){
				
			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/kmadmin/countSends/".$from);
			$from = date( "Y-m-d", strtotime( $from." +1 day" ))."";
				
		}

		exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/kmadmin/countSends/".$to);

	}

	public function countSends($date = ""){
				
		$day = date('Y-m-d');
		$hour = date('H:i');			
		$dbcore = new Database();
		$process_id = jobs::logCron($dbcore,"KM COUNT SENDS $day $hour",getmypid(),"start");
		
		if($date == ""){
			$_date = date( "Y-m-d", strtotime( date("Y-m-d")." -1 day" ))."";
		}else{
			$_date = $date;
		}

		list($year,$month,$day) = explode("-",$_date);
		//echo $year."-".$month."-".$day;

		$db = new Database();
		$res = $db->from("users")->where('status','1')->groupby('km_user')->get();

		$morning = " 00:00:00";
		$evening = " 23:59:59";

		foreach ($res as $u){
			if($u->km_user == "admin") continue;
				

			for($i = 0; $i < 24; $i++){

				if($i < 10){
					$hh = "0".$i;
				}else{
					$hh = $i;
				}

				$from = $year."-".$month."-".$day." ".$hh.":00:00";
				$to = $year."-".$month."-".$day." ".$hh.":59:59";


				$db->query("use km_".$u->km_user);

				$sql = "SELECT count(id) as ossz FROM omm_jobs WHERE sent_time BETWEEN '".$from."' AND '".$to."'  AND status='sent' AND type='dayjob' ";

				$res = $db->query($sql);
				$count = $res[0]->ossz;

				$db->query("use km_core");

				$db->query("INSERT INTO omm_sendcounts SET km_user ='".$u->km_user."', year = '".$year."', month = '".$month."', day = '".$day."', hour = '".$i."', count = ".$count." ");


			}
		}
		
		jobs::logCron($dbcore,"KM COUNT SENDS $day $hour",getmypid(),"end", $process_id);
		
	}

	private function ftp_copy($source_file, $destination_file)
	{
		$ftp_server = 's0.mediacenter.hu';
		$ftp_user = 'bolcsfoldia';
		$ftp_password = 'bf99x';

		$conn_id = ftp_connect($ftp_server);
		$login_result = ftp_login($conn_id, $ftp_user, $ftp_password);

		if((!$conn_id) || (!$login_result))
		{
			echo "FTP connection has failed!";
			echo "Attempted to connect to $ftp_server for user $ftp_user";
		}

		$upload = ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY);
		ftp_close($conn_id);

		if(!$upload)
		{
			echo "FTP copy has failed!";
			return false;
		}
		else
		{
			return true;
		}
	}


}