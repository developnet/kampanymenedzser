<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Public_Controller extends Controller{
	
	var $json_in;
	var $json_out = array();
	
	public function __construct(){
		parent::__construct();
		
		Event::add('system.post_controller', array($this, '_render'));
		
		if(!isset($_GET['data'])){
			$this->json_out['status'] = 'error';
			$this->json_out['message'] = '#01 Missing data.';
			Event::run('system.post_controller');
			KOHANA::shutdown();
		}else{
			$datastring = base64_decode($_GET['data']);
			$json = json_decode($datastring);
			if($json instanceof stdClass){
				$this->json_in = $json;
				
			}else{
				
				$this->json_out['status'] = 'error';
				$this->json_out['message'] = '#02 Bad data.';
				$this->json_out['data'] = $json;
				Event::run('system.post_controller');
				KOHANA::shutdown();				
			}
			
			
		}		
		
	}	
	
	public function index(){
		$this->json_out['status'] = 'ok';
		
		foreach ($this->json_in as $key => $s){
			echo $key." . ".$s."|";
		}
		
	}
	
	public function __call($method,$arguments){
		$this->json_out['status'] = 'error';
		$this->json_out['message'] = '#02 No such method!';
		Event::run('system.post_controller');
		KOHANA::shutdown();		
	}	
	
	public function _render(){
		echo json_encode($this->json_out);
	}
}

?>