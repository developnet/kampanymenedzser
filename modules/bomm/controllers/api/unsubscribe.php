<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Unsubscribe_Controller extends Apicontroller_Controller {

	private $UNSUBSCRIBED = 'unsubscribed';


	public function __construct(){
		parent::__construct();
	}


	public function index(){
		Event::run('member.pre_unsubscription');

		if(isset($_GET['code']) && $_GET['code'] != ""){

			$dbcore = new Database("");
				
			if(!isset($_GET['a'])){
				Event::run('error.unsubscribe_missingorinvalid_code');
			}
				
			$acode = $_GET['a'];
				
			$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
				
			if(sizeof($acc) == 0){
				Event::run('error.unsubscribe_missingorinvalid_code');
			}
				
			$user = $acc[0]->km_user;
				
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;

			Kohana::config_set('database.own',$dbown);


			$member = ORM::factory('omm_list_member')->where('code',$_GET['code'])->find();

			if($member->loaded){
				
				/*
				////LEIRATKOZOTTAK LISTA				
				$db = new Database('own');
				$list = $member->omm_list;
				$cid = $list->omm_client_id;
				$res = $db->query("SELECT omm_lists.id as id FROM omm_lists WHERE omm_client_id = ".$cid." AND UPPER(name) LIKE '%LEIRATKOZOTTAK [AUTO]%' ");
				foreach($res as $r){
					$_list_id = $r->id;
				}
				if(isset($_list_id)){
					$_list =  ORM::factory("omm_list")->find($_list_id);
					
					if($_list->loaded){
						$_list->_subscribe($member->email);
					}			
				}			
				////LEIRATKOZOTTAK LISTA	
				*/
				
				
				if($member->status == $this->UNSUBSCRIBED){
					//$list = $member->omm_list;
					$data = array('member' => $member,'km_user' => $user);					
					Event::run('error.unsubscribe_second_unsubscribe',$data);
				}else{
					$member->status = $this->UNSUBSCRIBED;
					$date =  date("Y-m-d H:i:s");
					$member->unsubscribe_date = $date;
					try {
						$member->save();
						$data = array('member' => $member,'km_user' => $user);
						Event::run('member.post_unsubscription',$data);
					}catch (Exception $e){
						KOHANA::log("error","leiratkozás db hiba, member_id=".$member->id." ::: ".$e);
						//$list = $member->omm_list;
						Event::run('error.unsubscribe_db_error');
					}
				}
			}else{
				Event::run('error.unsubscribe_missingorinvalid_code');
			}
		}else{
			Event::run('error.unsubscribe_missingorinvalid_code');
		}
	}



	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}



}
?>