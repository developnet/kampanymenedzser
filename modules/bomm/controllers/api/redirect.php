<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

/**
 */
class Redirect_Controller extends Apicontroller_Controller {


	public function __construct(){
		parent::__construct();
	}


	public static function errorHandler($errno, $errstr, $errfile, $errline){
		echo "Hiba";
	}

	public function index(){

		//		set_error_handler(array('Redirect_Controller', 'errorHandler'));
		//		set_exception_handler(array('Redirectcom_Controller', 'errorHandler'));

		if(isset($_GET['url']) && isset($_GET['m'])){

			$tablename = Kohana::config("database.default");
			$tp = $tablename['table_prefix'];

			try{

				$dbcore = new Database("");
					
				if(!isset($_GET['a'])){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód!");
				}
					
				$acode = $_GET['a'];
					
				$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
					
				if(sizeof($acc) == 0){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód!");
				}
					
				$user = $acc[0]->km_user;
					
				$dbown = Kohana::config('database.own');
				$dbown['connection']['database']="km_".$user;

				Kohana::config_set('database.own',$dbown);


				$db = new Database("own");

				$member = $db->query("SELECT id,code FROM ".$tp."omm_list_members WHERE code='".$_GET['m']."' LIMIT 1");
				$link = $db->query("SELECT id,url,omm_letter_id,url_name FROM ".$tp."omm_letter_links WHERE code='".$_GET['url']."' LIMIT 1");

				if(sizeof($member) != 1) $member_id = 0;
				else $member_id = $member[0]->id;

				//throw new Kohana_User_Exception('Invalid member!', "Invalid member!")

				if(sizeof($link) != 1) throw new Kohana_User_Exception('Invalid url!', "Invalid url!");

				if($member_id == 0 && $link[0]->url_name == 'unsubscribe'){
					throw new Kohana_User_Exception('Nincs ilyen felhasználó!', "Ezt a felhasználót törölték a rendszerből, vagy hibás a kód!");
				}

				if($link[0]->url_name == 'unsubscribe') $link_id = 'NULL';
				else $link_id = $link[0]->id;

				$db->query("INSERT INTO ".$tp."omm_letter_link_clicks
				               SET omm_letter_link_id=".$link_id.", 
				                   omm_list_member_id = ".$member_id.",
								   omm_letter_id = ".$link[0]->omm_letter_id."
				                   ");

				////////////////////////////////////////////////////////////////////////////////
				//////////////////megnyitás regisztrálás ha még nem volt illetve eltelt legalább fél óra

				if($member_id != 0){
					$open_timeout = Kohana::config("core.open_timeout");
					$today = date("Y-m-d H:i:s");
					$date = date( "Y-m-d H:i:s", strtotime( $today." -".$open_timeout." seconds" ));

					$openres = $db->query("SELECT id
											 FROM ".$tp."omm_letter_open_log 
											WHERE omm_list_member_id = ".$member_id." 
											  AND omm_letter_id = ".$link[0]->omm_letter_id."
											  AND opened >= '".$date."' ");				

					if(sizeof($openres) == 0){
						if(isset($_SERVER['HTTP_USER_AGENT'])){
							$user_agent = $_SERVER['HTTP_USER_AGENT'];
						}else{
							$user_agent = 'unknow';
						}

						$db->query("INSERT INTO ".$tp."omm_letter_open_log
						               SET omm_list_member_id = ".$member_id.",
										   omm_letter_id = ".$link[0]->omm_letter_id.",
										   user_agent = '".$user_agent."'
						                   ");					
					}
				}

				//////////////////////megynitás
				////////////////////////////////////////////////

				$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
				header("Referer: ".$ref);

				if($link[0]->url_name == 'unsubscribe'){

					url::redirect($link[0]->url."?a=".$acode."&code=".$member[0]->code);

				}else{
					url::redirect($link[0]->url);
				}



			}catch (Exception $e){
				throw $e;
			}
		}
	}

	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}



}
?>