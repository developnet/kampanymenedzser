<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Modify_Controller extends Apicontroller_Controller {

	private $UNSUBSCRIBED = 'unsubscribed';


	public function __construct(){
		parent::__construct();
	}


	public function index(){
		Event::run('member.pre_unsubscription');

		if(isset($_GET['code']) && $_GET['code'] != ""){


			$dbcore = new Database("");
				
			if(!isset($_GET['a'])){
				throw new Kohana_User_Exception("Hibás űrlap","Nincs megadva kód!");
			}
				
			$acode = $_GET['a'];
				
			$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
				
			if(sizeof($acc) == 0){
				throw new Kohana_User_Exception("Hibás űrlap","Nincs megadva kód!");
			}
				
			$user = $acc[0]->km_user;
				
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;

			Kohana::config_set('database.own',$dbown);


			$member = ORM::factory('omm_list_member')->where('code',$_GET['code'])->find();

			if($member->loaded){
				$view = new View("omm/datamodify");


				$list = $member->omm_list;
				$form = $list->getDataModForm();

				if(is_null($form)){
					url::redirect('/pages/redirect/subscribe/nomodify');
					//throw new Kohana_User_Exception("Hibás űrlap","A listához nincs adatmódosító űrlap kapcsolva!");
				}
				$fields = $list->fields();
				$mo = $member->getData($fields);

				$values = array();

				foreach ($mo as $key => $v){
					$values[$key] = $v;
				}



				$view->values = $mo;
				if(isset($_POST['email'])){

					$post = new Validation($_POST);
					$post->pre_filter('trim', TRUE);

					$post->add_rules('email','required', 'email');

					if ($post->validate())  {
							
						if($this->_unique_email($post->email, $list->id) && $member->email != $post->email){

							$view->message = "Ez az e-mail cím már regisztrálva van a rendszerben!";
							$view->form = $form->getHtml(true, url::site().'api/modify?a='.$acode.'&code='.$_GET['code'].'',$post);

						}else{

							$member->email = $post->email;
							$member->save();

							$member->saveData($post,$fields);

							$view->message = "Az adatmentés sikeres volt!";
							$view->form = "";

						}


					}else{
						$view->message = "Az adatok hibásak, a mentés nem sikerült!";

						$view->form = $form->getHtml(true, url::site().'api/modify?a='.$acode.'&code='.$_GET['code'].'',$post);
					}




				}else{//nincs post

					$view->form = $form->getHtml(true, url::site().'api/modify?a='.$acode.'&code='.$_GET['code'].'',$values);

				}




				//url::site().'api/subscribe';

				$view->render(TRUE);
			}else{
				throw new Kohana_User_Exception("Hibás űrlap","Nincs ilyen feliratkozó!");
			}
		}else{
			throw new Kohana_User_Exception("Hibás űrlap","Nincs megadva kód!");
		}
	}



	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}

	private function _unique_email($email, $listid){
		////van-e ilyen aktív user?
		return  (bool) ORM::factory('omm_list_member')->where('omm_list_id',$listid)->where('email',$email)->where('status','active')->count_all();
	}

}
?>