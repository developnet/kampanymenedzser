<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

/**
 */
class Common_Controller extends Adminapicontroller_Controller {

	public function index(){

	}
	
	public function mcriport_product(){
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$db = new Database("own");		
		
		$from = '2012-04-01 00:00:00';
		$to = '2012-10-05 00:00:00';		
		
		$result = $db->query("SELECT * FROM omm_products");
		
		foreach($result as $p){
			
			$fullossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_product_member_connects c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.omm_product_id = ".$p->id."  
				");			
			
			$ossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_product_member_connects c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.purchase_date  BETWEEN '".$from."' AND '".$to."'
				 AND c.omm_product_id = ".$p->id."  
				");

			$newossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_product_member_connects c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.purchase_date  BETWEEN '".$from."' AND '".$to."'
				 AND m.reg_date >= c.purchase_date 
				 AND c.omm_product_id = ".$p->id."  
				");
			
			echo $p->id.";".$fullossz[0]->ossz.";".$ossz[0]->ossz.";".$newossz[0]->ossz."\n";
		}
		
	}
	
	public function mcriport_lists(){
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$db = new Database("own");		
		
		$from = '2012-04-01 00:00:00';
		$to = '2012-10-05 00:00:00';		
		
		$result = $db->query("SELECT * FROM omm_lists where status ='active' order by name asc ");
		
		foreach($result as $p){
			
			$fullossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_list_member_connect c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.omm_list_id = ".$p->id."  
				");			
			
			$ossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_list_member_connect c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.reg_date  BETWEEN '".$from."' AND '".$to."'
				 AND c.omm_list_id = ".$p->id."  
				");

			$newossz = $db->query("
				SELECT count(m.id) as ossz FROM omm_list_members m, omm_list_member_connect c 
				 WHERE m.status='active' 
				 AND c.omm_list_member_id = m.id 
				 AND c.reg_date  BETWEEN '".$from."' AND '".$to."'
				 AND m.reg_date >= c.reg_date 
				 AND c.omm_list_id = ".$p->id."  
				");
			
			echo $p->id.";'".$p->name."';".$fullossz[0]->ossz.";".$ossz[0]->ossz.";".$newossz[0]->ossz."\n";
		}
		
	}	
		
	
	public function getCommonLetterTable(){
			$template = new View("ajax_view");
			$template->output_json_file = true;	
			$letters = ORM::factory("omm_common_letter")->where('status','active')->orderby('name','asc')->find_all();
		
			$html = "";
			//$html .='<table cellpadding="0" cellspacing="0" width="100%" class="tableHeader">';
			$html .='<tr class="noHighlight">';
			$html .='<th class="headerLeft" width="100%" colspan="2"><span>Levelek listája</span></th>';
			$html .='<th nowrap>&nbsp;</th>';
            $html .='<th class="headerRight" style="padding-right:0px;" nowrap>&nbsp;</th>';
			$html .='</tr>';
			
			foreach ($letters as $l):
				if(isset($_SESSION['selected_common_letter']) && $_SESSION['selected_common_letter']->id == $l->id){
					$class = "dashRowActive";
				}else{
					$class = "dashRow";
				}
				
				if(empty($l->note)){
					$note = "";
				}else{
					$note = 'title="'.nl2br($l->note).'"';
				}				
				
				$html .='<tr class="'.$class.'">';
				$html .='<td width="9" style="padding-right:0px;">';
	            $html .='</td>';
				$html .='<td width="100%">';
	            $html .='<strong><a href="'.url::base().'pages/commonletters/select/'.$l->id.'" '.$note.'>'.$l->name.'</a></strong>';
	            $html .='</td>';
				$html .='<td nowrap width="13" style="padding-right:0px;">';
	            $html .='</td>';
				$html .='<td nowrap width="10">';
	            $html .='<a href="javascript:;" class="deleteCampaign" rel="<?=$l->id ?>" relName="'.$l->name.'">';
	        	$html .='<img src="'.Kohana::config('core.viewpath').Kohana::config('admin.img').'icons/trash.png" width="10" height="11" border="0" alt="Törlés">';
	    	    $html .='</a></td></tr>';
			
			endforeach;
			//$html .='</table>'; 		
		
		$result['STATUS'] = "SUCCES"; 		
		$result['HTML'] = $html;
		//
		$template->json = json_encode($result);
		$template->render(TRUE);			
		die();
	}
	
	public function saveCommonLetter(){
		$template = new View("ajax_view");
		$template->output_json_file = true;		
		
		if(!isset($_POST['html']) || !isset($_POST['letter']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}		
		
		
		$letter = ORM::factory('omm_common_letter')->find($_POST['letter']);
		
		$new = false;
		
		
		if(!$letter->loaded){
			$letter = ORM::factory('omm_common_letter');
			$new = true;
		}
		
		$letter->name = $_POST['name'];
		
		if($letter->name == ""){
			$letter->name = "(Új levél)";
		}
			
		
		$letter->subject = $_POST['subject'];
		$letter->note = $_POST['note'];
		$letter->sender_name = $_POST['sender_name'];
		$letter->sender_email = $_POST['sender_email'];
		
		
		$letter->text_content = $_POST['text'];
		$letter->setHtmlContent($_POST['html']);		
		$letter->type = 'html';
		$letter->saveObject();
		
		if($new){
			$result['STATUS'] = "SUCCES_NEW";	
		}else{
			$result['STATUS'] = "SUCCES";
		}
		
		
		$result['NEWLETTER'] = $letter->id; 		
		$result['MESSAGE'] = date("Y.m.d H:i:s");
		//
		$template->json = json_encode($result);
		$template->render(TRUE);			
		die();
	}	
	
	public function saveLetter(){
		$template = new View("ajax_view");
		$template->output_json_file = true;		
		
		if(!isset($_POST['html']) || !isset($_POST['letter']) || empty($_POST['html']) || empty($_POST['letter']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}		
		
		
		$letter = ORM::factory('omm_letter')->find($_POST['letter']);
		$letter->text_content = $_POST['text'];
		$letter->setHtmlContent($_POST['html']);		

		$letter->saveObject();
		
		$result['STATUS'] = "SUCCES"; 		
		$result['MESSAGE'] = date("Y.m.d H:i:s");
		//
		$template->json = json_encode($result);
		$template->render(TRUE);			
		die();
	}
	
	public function deleteLetters(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_letters']) || empty($_POST['operator']) || empty($_POST['selected_letters']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_letters = $_POST['selected_letters'];
		$operator = $_POST['operator'];
		
		$letters = array();
		
		for($i=0;$i<$selected_letters;$i++){
				$letters[] = $_POST['letter_'.$i]; 
		}			
		
		foreach($letters as $l){
			$letter = ORM::factory('omm_letter')->find($l);
			$letter->deleteLetter();
		}
			
		$result['STATUS'] = "SUCCES"; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
	}	

	public function deleteProducts(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_items']) || empty($_POST['operator']) || empty($_POST['selected_items']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_lists = $_POST['selected_items'];
		$operator = $_POST['operator'];
		
		$lists = array();
		
		for($i=0;$i<$selected_lists;$i++){
				$lists[] = $_POST['list_'.$i]; 
		}			
		
		foreach($lists as $l){
			$list = ORM::factory('omm_product')->find($l);
			
			$list->deleteProduct();
			
			$list->saveObject();	
		}
			
		$result['STATUS'] = "SUCCES"; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
	}	
	
	public function deleteLists(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_lists']) || empty($_POST['operator']) || empty($_POST['selected_lists']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_lists = $_POST['selected_lists'];
		$operator = $_POST['operator'];
		
		$lists = array();
		
		for($i=0;$i<$selected_lists;$i++){
				$lists[] = $_POST['list_'.$i]; 
		}			
		
		foreach($lists as $l){
			$list = ORM::factory('omm_list')->find($l);
			
			$list->deleteList();
			
			$list->saveObject();	
		}
			
		$result['STATUS'] = "SUCCES"; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
	}

	public function archiveProducts(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_items']) || empty($_POST['operator']) || empty($_POST['selected_items']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_lists = $_POST['selected_items'];
		$operator = $_POST['operator'];
		
		$lists = array();
		
		for($i=0;$i<$selected_lists;$i++){
				$lists[] = $_POST['list_'.$i]; 
		}			
		
		foreach($lists as $l){
			$list = ORM::factory('omm_product')->find($l);
			$list->status = $operator;
			$list->saveObject();	
		}
			
		$result['STATUS'] = "SUCCES"; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
	}	
	
	public function saveSelectedMembers(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_members']) || empty($_POST['operator']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_members = $_POST['selected_members'];
		$operator = $_POST['operator'];		
		
		unset($_SESSION['selected_members']);
		$members = array();
		
		for($i=0;$i<$selected_members;$i++){
			if(isset($_POST['member_'.$i]) && !in_array($_POST['member_'.$i], $members))
				$members[] = $_POST['member_'.$i]; 
		}			
		
		$_SESSION['selected_members'] = $members;
			
		$result['STATUS'] = "SUCCES";
		$result['DATA'] = $_SESSION['selected_members']; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
		
		
	}
	
	public function archiveLists(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_lists']) || empty($_POST['operator']) || empty($_POST['selected_lists']) ){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}
		
		$selected_lists = $_POST['selected_lists'];
		$operator = $_POST['operator'];
		
		$lists = array();
		
		for($i=0;$i<$selected_lists;$i++){
				$lists[] = $_POST['list_'.$i]; 
		}			
		
		foreach($lists as $l){
			$list = ORM::factory('omm_list')->find($l);
			$list->status = $operator;
			$list->saveObject();	
		}
			
		$result['STATUS'] = "SUCCES"; 		
		
		$template->json = json_encode($result);
		$template->render(TRUE);			
	}
	
	public function copyletters(){
		$template = new View("ajax_view");
		$template->output_json_file = true;
		
		$result = array();
		
		if(!isset($_POST['operator']) || !isset($_POST['selected_letters']) || !isset($_POST['selected_campaign']) || empty($_POST['operator']) || empty($_POST['selected_letters'])  || empty($_POST['selected_campaign'])){
			$result['STATUS'] = "ERROR"; 
			$template->json = json_encode($result);
			$template->render(TRUE);
			Kohana::shutdown();
			die();			
		}


		
		$selected_letters = $_POST['selected_letters'];
		$selected_campaign = $_POST['selected_campaign'];
		$operator = $_POST['operator'];
		
		$letters = array();
		
		for($i=0;$i<$selected_letters;$i++){
				$letters[] = $_POST['letter_'.$i]; 
		}			
		
		if($operator == "move"){
			
			foreach($letters as $l){
				$letter = ORM::factory('omm_letter')->find($l);
				$letter->omm_campaign_id = $selected_campaign;
				$letter->saveObject();	
			}
			
			$result['STATUS'] = "SUCCES"; 
			
		}else if($operator == "copy"){
			
			foreach($letters as $l){
				$letter = ORM::factory('omm_letter')->find($l);
				$letter->duplicate($selected_campaign);
			}
			
			$result['STATUS'] = "SUCCES";
						
			
		}
		
		$template->json = json_encode($result);
		$template->render(TRUE);
		
	}
	
	public function letterContentCss($header = TRUE){
		$lid = $this->uri->segment(4,"");
		$letter = ORM::factory('omm_letter')->find($lid);

		$typo = $letter->getDetail("typo");

		$typoCssView = new View(Kohana::config('admin.theme')."/typos/".$typo);

		if($header)
		header('Content-type: text/css');
			
			
		echo $typoCssView->render(FALSE,FALSE);

	}

	public function editLetter(){

		$head = '<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>Untitled Document</title>
					<script src="http://teszt1.kampanymenedzser.hu/core/views/omm/js/nicEdit.js" type="text/javascript"></script>
		    		<script language="JavaScript1.2" src="http://teszt1.kampanymenedzser.hu/core/assets/jquery/js/jquery-1.3.2.min.js"></script>
		    		<script language="JavaScript1.2" src="http://teszt1.kampanymenedzser.hu/core/assets/jquery/js/jquery-ui-1.7.1.custom.min.js"></script>
		    		<script language="JavaScript1.2" src="http://teszt1.kampanymenedzser.hu/core/views/omm/js/lettereditor.js"></script>
		';



		$lid = $this->uri->segment(4,"");
		$letter = ORM::factory('omm_letter')->find($lid);

		if($letter->loaded){
			if($letter->type=="html"){

				$dom = new simple_html_dom();
				$dom->load($letter->html_content, true);
				$head .= '<body style="width:600px;border:1px solid black;border-right:2px solid black;border-bottom:2px solid black;margin:auto;padding:0;margin-top: 30px;margin-bottom: 30px; style="font-family: Georgia, '."'".'Times New Roman'."'".', Times, serif;">';

				$head .= '<div style="float:left;top:0;left:0;z-index:1000;position:fixed;margin:auto;border-bottom:2px solid black;width: 100%;" id="editorPanel" ></div>';

				foreach($dom->find('body') as $element)
				$head .= $element->innertext . '';
					
				$head .= '</body>';
				$head .= '</html>';

				echo $head;

			}else{
				echo nl2br($letter->text_content);
			}
		}
	}

	public function showLetter(){
		$lid = $this->uri->segment(4,"");

		$letter = ORM::factory('omm_letter')->find($lid);

		if($letter->loaded){

			if($letter->type=="html"){
				$html = $letter->getRealHtml();
				$text = nl2br($letter->text_content);
			}else{
				$html = "";
				$text = nl2br($letter->text_content);
			}
		}
		
		
		$view = new View(Kohana::config('admin.theme')."/common/showletter");

		$view->html = $html;
		$view->text = $text;
		$view->l = $letter;	
			
		echo $view->render(FALSE,FALSE);		
	}
	
	public function showCommonLetter(){
		$lid = $this->uri->segment(4,"");

		$letter = ORM::factory('omm_common_letter')->find($lid);

		if($letter->loaded){

			if($letter->type=="html"){
				$html = $letter->getRealHtml();
				$text = nl2br($letter->text_content);
			}else{
				$html = "";
				$text = nl2br($letter->text_content);
			}
		}
		
		
		$view = new View(Kohana::config('admin.theme')."/common/showcommonletter");

		$view->html = $html;
		$view->text = $text;
		$view->l = $letter;	
			
		echo $view->render(FALSE,FALSE);		
	}	
	
	
	
	public function getCommonLetterHtmlContent(){
		$lid = $this->uri->segment(4,"");

		$letter = ORM::factory('omm_common_letter')->find($lid);

		if($letter->loaded){

			if($letter->type=="html"){
				echo $letter->getRealHtml();
			}else{
				echo "<h1>Nincs HTML tartalom!</h1>";
			}
		}
	}

	
	public function getLetterHtmlContent(){
		$lid = $this->uri->segment(4,"");

		$letter = ORM::factory('omm_letter')->find($lid);

		if($letter->loaded){

			if($letter->type=="html"){
				echo $letter->getRealHtml();
			}else{
				echo "<h1>Nincs HTML tartalom!</h1>";
			}
		}
	}

	public function addEmailConditions($letterId, $emails){

		$result = explode(",", $emails);
		$letter = ORM::factory('omm_letter')->find($letterId);

		foreach($result as $email){
			$letter->addEmailCondition($email);
		}

		echo "OK";
	}
	
	public function getProductEditForm($product_connect_id){
		
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];		//
		$db = new Database("own");
				
		$sql = "SELECT
					p.name as product_name,
					p.id as product_id,
					c.order_date,
					c.purchase_date,
					c.failed_date,
					c.status
					FROM 
					".$tp."omm_product_member_connects c, ".$tp."omm_products p 
					WHERE 
					c.omm_product_id = p.id
					AND
					c.id = ".$product_connect_id."
					ORDER BY c.order_date DESC,c.purchase_date DESC,c.failed_date DESC
					";
		
		$result = $db->query($sql);
		
		$prod = $result[0];
		
		$html = '<div class="formBG">';
		$html .= '<div class="formWrapper">';
		$html .= '<form name="subscribers" id="product_connect_form" action="" method="post">';		

		$html .= '<input type="hidden" value="'.$product_connect_id.'" name="product_connect_id">';

		$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
 		$html .= '<label><strong>'.$prod->product_name.'</strong></label>';
		
		$html .= '<select name="product_mod_status" style="width:94px;margin:2px">';
		
		$_ordered = "";
		$_purchased = "";
		$_failed = "";
		
		if($prod->status == "ordered") $_ordered = 'selected="selected"';
		if($prod->status == "purchased") $_purchased = 'selected="selected"';
		if($prod->status == "failed") $_failed = 'selected="selected"';
		
		$html .= '	<option value="ordered" '.$_ordered.'>Megrendelte</option>';
		$html .= '	<option value="purchased" '.$_purchased.'>Megvásárolta</option>';
		$html .= '	<option value="failed" '.$_failed.'>Hibás</option>';
		
		$html .= '</select>';		
		$html .= '</div>';
		
		$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
		$html .= '<label>Megrendelés ideje: </label>';
		$html .= '<input type="text" class="datepicker" name="product_order_date" value="'.$prod->order_date.'" /> ';
		$html .= '</div>';

		$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
		$html .= '<label>Megvásárlás ideje: </label>';
		$html .= '<input type="text" class="datepicker" name="product_purchase_date" value="'.$prod->purchase_date.'" /> ';		
		$html .= '</div>';		

		$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
		$html .= '<label>Hiba ideje: </label>';
		$html .= '<input type="text" class="datepicker" name="product_failed_date" value="'.$prod->failed_date.'" /> ';		
		$html .= '</div>';				
		
		$html .= '<div class="clearButton"></div>';
		$html .= '</form>';
		$html .= '</div>';
		$html .= '</div>'; 	

		echo $html;
		
	}
	
	public function getMemberDataEditForm($member_id,$list_id){
		$list = ORM::factory('omm_list')->find($list_id);//
		$member = ORM::factory('omm_list_member')->find($member_id);
		$fields = $list->fields();
		$mo = $member->getData($fields);		
		
		
		$html = '<div class="formBG">';
		$html .= '<div class="formWrapper">';
		$html .= '<form name="subscribers" action="'.url::base().'pages/memberdetail/detail/'.$member->id.'/'.$list_id.'" method="post">';
        
		$html .= '<div class="formContainer">';
		$multiselect_fields = "";	
		
		$form = array();
		$errors = array();
	    foreach ($fields as $f) {
				
        	$r = $f->reference;
        	
        	$form[$r] = '';
        	$errors[$r] = '';
        	
	        if($member->loaded){
				
	        	if(isset($mo->$r))
	        		$form[$r] = $mo->$r;
	        	else
	        		$form[$r] = "";
	        			 
				$rrr = $r."_code";
				
				if(isset($mo->$rrr)){
					$form[$rrr] = $mo->$rrr; 	
				}

	        }    
        	
//	        if($f->required){
//	    		$post->add_rules($r,'required');    	
//	        }
	        
        }			
		
		
		foreach($fields as $f):
			if($f->type == "singleselectradio"):
				$html .= '<div class="clearfix" style="text-align:left">';
				$html .= '<label>'.$f->name.'</label>';
				$html .= '<div class="radioInset">';
		        foreach($f->getValues() as $v): 
          				if($v->code == $form[$f->reference]):           	
		                 	$html .= '<input type="radio" name="'.$f->reference.'" value="'.$v->code.'" checked="checked"/>';
		                 else:
		                 	$html .= '<input type="radio" name="'.$f->reference.'" value="'.$v->code.'"/>';
		                  endif;
		                    $html .= '<label>'.$v->value.'</label>';
		                    $html .= '<img src="img/_space.gif" width="1" height="5" /><br/>';
		                 endforeach;
							$html .= '</div>';	
							$html .= '</div>';
					
						elseif($f->type == "singleselectdropdown"):
							$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
							$html .= '<label>'.$f->name.'</label>';
							$html .= '<select name="'.$f->reference.'">';
		                        
		                        foreach($f->getValues() as $v): 

		            				if(isset($form[$f->reference."_code"]) && $v->code == $form[$f->reference."_code"]):      	            	

		                        		$html .= '<option value="'.$v->code.'" selected="selected">'.$v->value.'</option>';

		                        	 else: 

		                        		$html .= '<option value="'.$v->code.'">'.$v->value.'</option>';

		                        	endif;


		                        endforeach;
		                        
							$html .= '</select>';
						$html .= '</div>';
					
					elseif($f->type == "multiselect"): 
							
						$multiselect_fields .= $f->reference.",";

						$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
						$html .= '<label>'.$f->name.'</label>';
						$html .= '<div class="radioInset">';
		                        
		                        foreach($f->getValues() as $v):
		                        
		            				if(  strpos  ( $form[$f->reference] , $v->code ) !== false ):            	
		                        		$html .= '<input type="checkbox" name="'.$f->reference.'[]" value="'.$v->code.'" checked="checked"/>';
		                        	else:
		                        		$html .= '<input type="checkbox" name="'.$f->reference.'[]" value="'.$v->code.'"/>';
		                        	endif;
		                        		$html .= '<label>'.$v->value.'</label>';
		                        		$html .= '<img src="img/_space.gif" width="1" height="5" /><br/>';
		                        		
		                        endforeach; 
		                        
							$html .= '</div>';	
						$html .= '</div>';
					
					elseif($f->type == "multitext"):
					
						$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
							$html .= '<label>'.$f->name .'</label>';
							$html .= '<div class="radioInset">';
								$html .= '<textarea style="width:300px" name="'.$f->reference .'" cols="100" rows="10">'.$form[$f->reference] .'</textarea>		';                        
							$html .= '</div>';	
						$html .= '</div>';
					
					elseif($f->type == "date"):					
					
						$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
						$html .= '<label>'.$f->name .'</label><input class="datepicker input_text" type="text" name="'.$f->reference .'" size="45" value="'.$form[$f->reference] .'">';
						$html .= '</div>';	
					
					else:
				
						$html .= '<div class="clearfix" style="text-align:left;vertical-align:top">';
						$html .= '<label>'.$f->name .'</label><input type="text" name="'.$f->reference .'" size="45" value="'.$form[$f->reference] .'" class="input_text">';
						$html .= '</div>';				
				
					endif;
					

				
				endforeach;
				
				$html .= '<input type="hidden" name="multiselect_fields" value="'.$multiselect_fields.'"/>	';

				$html .= '</div>';

			$html .= '<div class="clearButton"></div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>'; 		
		
		echo $html;
	}
	
	
	public function getMemberData($member_id,$list_id){
		$list = ORM::factory('omm_list')->find($list_id);//
		$member = ORM::factory('omm_list_member')->find($member_id);
		$fields = $list->fields();
		$mo = $member->getData($fields);
		
		$html = '<table cellpadding="0" cellspacing="0" width="100%" class="snapshotStats">';
				
		foreach($fields as $f): 
			
			$ref = $f->reference;

			if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"):

				$html .= '<tr>';
				$html .= '<th nowrap class="camelCase">'.$f->name.'</th>';
				$html .= '<td width="100%" style="text-align:left">';
		           foreach($f->getValues() as $v):
           				$rrr = $ref."_code";
       					if($v->code == $mo->$rrr):	            	
                        	$html .= $v->value;
                       	endif;
                    endforeach;					
				$html .= '</td>';
				$html .= '</tr>';			
				
			elseif($f->type == "multiselect"):

				$html .= '<tr>';
				$html .= '<th nowrap class="camelCase">'.$f->name.'</th>';
				$html .= '<td width="100%" style="text-align:left">';
		        foreach($f->getValues() as $v):
		            if( strpos  ( $mo->$ref , $v->code ) !== false ):	            	
		                 $html .= $v->value.'<br/>';
		           	endif;
		        endforeach;					
				$html .= '</td>';
				$html .= '</tr>';	
				
			elseif($f->type == "multitext"):
				$html .= '<tr>';
				$html .= '<th nowrap class="camelCase">'.$f->name.'</th>';
				$html .= '<td width="100%" style="text-align:left">'.nl2br($mo->$ref).'</td>';
				$html .= '</tr>';
			else:
				$html .= '<tr>';
				$html .= '<th nowrap class="camelCase">'.$f->name.'</th>';
				$html .= '<td width="100%" style="text-align:left">'; 
				if(isset($mo->$ref)) $html .= $mo->$ref;
				$html .= '</td>';
				$html .= '</tr>';
			endif;
		
		endforeach;		
		
		$html .= '</table>';
		
		echo $html;
	}
	
	public function reciplist($letterId, $page){

		$lists = array();

		$listsFields = array();

		$listRefs = array();

		$listFirst = array();
		$listLast = array();

		$letter = ORM::factory('omm_letter')->find($letterId);
		$recips = $letter->getRecipientsNumberFilter(FALSE);

		$fields = $letter->omm_campaign->omm_client->getFields();
		
		$rec = array();
		foreach($recips as $_r){//
			$r = ORM::factory('omm_list_member')->find($_r->id);
			$m = array();
			$name = "";

			unset($first);
			unset($last);
			$mo = $r->getData($fields);
			
			$nameref = $letter->omm_campaign->omm_client->getNameField();
			
			if($nameref == "bothnames"){
			
				$first = $letter->omm_campaign->omm_client->searchReferenceByType("firstname");
				$last = $letter->omm_campaign->omm_client->searchReferenceByType("lastname");
				
				$name = $mo->$last." ".$mo->$first;
			}else{
				
				if($nameref != ""){
					$name = $mo->$nameref;	
				}else{
					$name = $mo->email;
				}
				
			}			
			
			$m['id'] = $r->id;
			$m['email'] = $r->email;
			$m['name'] = $name;
			$rec[] = $m;
		}


		echo json_encode($rec);
	}

	public function showForm(){
		$lid = $this->uri->segment(4,"");

		$f = ORM::factory('omm_list_form')->find($lid);

		$header = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				   <html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>Untitled Document</title>
					</head>
				<body>';

		$footer = '</body></html>';

		if($f->loaded){
			echo $header.$f->getHtml(true).$footer;
		}
	}

	public function postnote(){

		if(isset($_POST['type'])){
			$note = ORM::factory('omm_note');

			$note->user_id = $_SESSION['auth_user']->id;
			$note->type = $_POST['type'];
			$note->subject = $_POST['subject'];
			$note->screen = $_POST['screen'];
			$note->message = $_POST['message'];

			$note->saveObject();

			if ( ! class_exists('Swift', FALSE)){
				// Load SwiftMailer
				require Kohana::find_file('vendor', 'swift/Swift');

				// Register the Swift ClassLoader as an autoload
				spl_autoload_register(array('Swift_ClassLoader', 'load'));
			}

			$from = 'noreply@kmsrv1.hu';

			$subject = '[KM ÉSZREVÉTEL '.strtoupper($note->type).'] '.$note->subject;
			$message = "<h1>[KM ÉSZREVÉTEL] <br/>".$note->subject."</h1>";
			$message .= "<h3>User: ".$_SESSION['auth_user']->username."</h3>";
			$message .= "<h3>Screen: ".$note->screen."</h3>";
			$message .= '<br/><br/><code>---------------------------------------------------</code><br/>';
			$message .= "<p>".nl2br($note->message)."</p>";
			$message .= '<br/><br/><code>---------------------------------------------------<br/>';
			$message .= 'Automatikus e-mail km észrevétel.<code>';

			//email::send($to, $from, $subject, $message, TRUE);


			$m = new Swift_Message($subject);
			$m->headers->setEncoding("Q");
			//$message->setTo('bertamarton@aktivhonlapok.hu');
			//$message->setTo('bolcsfoldiarpad@aktivhonlapok.hu');

			$part_html = new Swift_Message_Part($message, "text/html");
			$part_html->setCharset("utf-8");
			$m->attach($part_html);

			$swift = jobs::getSwift();
			
			if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'){
				$swift->send($m, 'ugyfelszolgalat@garbaroyal.hu', $from);
			}else{
				$swift->send($m, 'ugyfelszolgalat@garbaroyal.hu', $from);
				$swift->send($m, 'support@kampanymenedzser.hu', $from);
			} 
				
							
			
			
			
			$from = $_SESSION['auth_user']->email;
			$swift->send($m, 'helpdesk@kmsrv2.hu', $from);
			
			
			
			$swift->disconnect();


			//$to      = 'bertamarton@aktivhonlapok.hu';
			//email::send($to, $from, $subject, $message, TRUE);


			echo "OK";
		}else{
			echo "ERROR";
		}

	}
	
	public function getTags(){
		$input = $_GET["q"];
		$data = array();
		$db = new Database("own");
		
		$sql = "SELECT tag.* FROM omm_tags tag WHERE tag.status='active' AND UPPER(tag.name) LIKE '%".$input."%' ";		
		$rows = $db->query($sql);
		
		foreach($rows as $row) {
			$json = array();
			$json['value'] = $row->id;
			$json['name'] = $row->name;
			$data[] = $json;
		}
		header("Content-type: application/json");
		echo json_encode($data);
	}

	public function getTime(){
		echo   date('Y')."|".date('m')."|".date('d')."|".date('H:i');
	}


	public function sugo($rel){
		$view = new View("omm/sugo/$rel");

		$title = $rel;

		if($title == "ures_ures"){
			$title = meta::getTitle("ures/");
		}else{

			$title = str_replace("_","/",$title);
			$title = str_replace("ures","",$title);

			$title = meta::getTitle($title);
		}

		$view->title = $title;

		$view->render(TRUE)."<p><br/></p>";
	}

	public function import($offset,$limit,$list_id = 0){

		if($offset == 0){
			unset($_SESSION['import_rownumber']);
			unset($_SESSION['import_actrownumber']);
			unset($_SESSION['import_errors']);
		}

		$template = new View("ajax_view");
		$template->output_json_file = true;
		$result = array();

		if(isset($_SESSION['client_id'])){
			
			//$list = $_SESSION['selected_list'];
			
			$client_id = $_SESSION['client_id'];
			$client = $_SESSION['selected_client'];
			
		}else{
			$result['STATUS'] = "CLIENT_ERROR";
			$template->json = json_encode($result);
			$template->render(TRUE);
			KOHANA::shutdown();
			die();
		}

		if (isset($_POST['import_file'])){
			$filename = $_POST['import_file'];
			$headernum = $_POST['headernum'];

			if(!isset($_SESSION['import_rownumber'])){
				$csvRowNum = fileutils::getCsvRowNumber($filename);
				$_SESSION['import_rownumber'] = $csvRowNum;
			}else{
				$csvRowNum = $_SESSION['import_rownumber'];
			}

			$alloffset = 0;
			if(isset($_POST['first_line_header']) && $_POST['first_line_header'] == 1){
				$firstline = false;
				$alloffset = 1;
			}else{
				$firstline = true;
			}

			if($offset > 0){
				$firstline = true;
			}

			$csvData = fileutils::getCsvContent($filename,$offset,$limit);

			$i = 0;
			$header = array();
			$fields = array();
			
			foreach($client->getAllFields($list_id) as $f){
				$fields[$f->reference] = $f;
			}


			$regdatum_key = -1;
			$email_key = -1;
			$statusz_key = -1;

			$tag_key = -1;
			$product_ordered_key = -1;
			$product_purchased_key = -1;
			$product_failed_key = -1;
			$list_key = -1;			
			
			$headerreferences = array();
			for ($i=0;$i<$headernum;$i++){

				if($_POST['header_'.$i] == "email"){
					$email_key = $i;
				}elseif($_POST['header_'.$i] == "regdatum"){
					$regdatum_key = $i;
				}elseif($_POST['header_'.$i] == "statusz"){
					$statusz_key = $i;
				}elseif($_POST['header_'.$i] == "tag"){
					$tag_key = $i;
				}elseif($_POST['header_'.$i] == "product_ordered"){
					$product_ordered_key = $i;
				}elseif($_POST['header_'.$i] == "product_purchased"){
					$product_purchased_key = $i;
				}elseif($_POST['header_'.$i] == "product_failed"){
					$product_failed_key = $i;
				}elseif($_POST['header_'.$i] == "list"){
					$list_key = $i;
				}elseif($_POST['header_'.$i] != "skip"){
					$headerreferences[$_POST['header_'.$i]] = $i;
				}

			}
			////////////////////

			if($email_key == -1){
				$result['STATUS'] = "EMAIL_HEADER_ERROR";
				$template->json = json_encode($result);
				$template->render(TRUE);
				die();
			}

			$notuniquemail = array();
			$errors = array();
			$all = 0;
			$imported_active = array();
			$imported_unsubscribed = array();
			$imported_deleted = array();

			$modified_user = array();
			$new_user = array();

			$i = 0;
			foreach($csvData as $row) {//////////adatok begyüjtése
				if(($i == 0 && $firstline) || $i > 0){////header skip

					if(isset($row[$email_key])){
						$email = $row[$email_key];
					}else{
						$email = "nincs email:$i";
					}

					$all++;

					//$email = trim($email);
					if(valid::email($email) == false){
						if($email == "" || $email == " "){
							$email = "[üres e-mail]";
						}
						$errors[] = $email." (sor: ". ($i+$offset) .")";
						$i++;
						continue;
					}

					$member = ORM::factory('omm_list_member')->where('email',$email)->where('omm_client_id',$client_id)->find();

					if(!$member->loaded){ ///új user létrehozzuk
						$member->omm_client_id = $client_id;
						//$member->omm_list_id = $list->id;
						$member->manual = 2;
						$member->unsubscribe_date = null;

						if($regdatum_key == -1){
							$member->reg_date = date("Y-m-d H:i:s");
						}else{
							$member->reg_date = $row[$regdatum_key];
						}

						if($statusz_key == -1){

							$member->status = 'active';
							$member->activation_date = $member->reg_date;

						}else{

							switch ($row[$statusz_key]){

								case 1:
									$member->status = 'active';
									$member->activation_date = $member->reg_date;
									break;
								case 2:
									$member->status = 'unsubscribed';
									$member->activation_date = $member->reg_date;
									$member->unsubscribe_date = $member->reg_date;
									break;
								case 3:
									$member->status = 'deleted';
									break;
								case 4:
									$member->status = 'error';
									break;
								case 5:
									$member->status = 'prereg';
									$member->activation_date = null;
									break;
								default:
									$member->status = 'active';
									$member->activation_date = $member->reg_date;

									
							}
						}

						$member->code = string::random_string('unique');
						$member->email = $email;


						$new_user[] = $email;
							
					}else{////meglévő user updatelünk
						
						if($regdatum_key != -1){
							$member->reg_date = $row[$regdatum_key];
							$member->activation_date = $member->reg_date;
						}


						if($statusz_key != -1){

							switch ($row[$statusz_key]){

								case 1:{
									$member->status = 'active';

									if($member->activation_date == null && $regdatum_key == -1){
										$member->activation_date = date("Y-m-d H:i:s");
									}

									break;
								}
								case 2:{
									$member->status = 'unsubscribed';

									if($member->unsubscribe_date == null){
										$member->unsubscribe_date = date("Y-m-d H:i:s");
									}

									break;
								}
								case 3:
									$member->status = 'deleted';
									break;
								case 4:
									$member->status = 'error';
									break;
								case 5:
									$member->status = 'prereg';
									break;
								default:
									$member->status = 'active';
									
									
							}
						}

						$modified_user[] = $email;
					}

					$member->save();
					
					
					
					//$tag_key = -1;
					//$product_key = -1;
					//$list_key = -1;						
					//////////////////
					
					if($tag_key != -1){
						$t = $row[$tag_key];
						if($t != "" && $t != "-"){
							$tags = explode(',',$t);
							$tags[] = 'Mindenki';
						}else{
							$tags= array('Mindenki');
						}
					}else{
						$tags= array('Mindenki');
					}
					
					$_tags = array();
					foreach($tags as $t){
							$tago = ORM::factory("omm_tag");
							$tag_id = $tago->createIfNotExist($t,'active');
							$_tags[] = array('id' => $tag_id);
					}
					$member->addTags($_tags, false);
					///////////////////////////////
					
					
					if($product_ordered_key != -1){
						$p = $row[$product_ordered_key];
						
						if($p != "" && $p != "-"){//

							$prod = ORM::factory("omm_product");		
							$_p = explode(',',$p);
							
							$prod_id = $prod->createIfNotExist($_p[0]);
							
							if(isset($_p[1])){
								$member->addProduct($prod_id,'ordered',$_p[1]);	
							}else{
								$member->addProduct($prod_id,'ordered');
							}					
						}
					}					

					if($product_purchased_key != -1){
						$p = $row[$product_purchased_key];
						
						if($p != "" && $p != "-"){//
							$prod = ORM::factory("omm_product");		
							
							$_p = explode(',',$p);
							
							$prod_id = $prod->createIfNotExist($_p[0]);
							
							if(isset($_p[1])){
								$member->addProduct($prod_id,'purchased',$_p[1]);	
							}else{
								$member->addProduct($prod_id,'purchased');
							}
							
							
													
						}
					}					
					
					if($product_failed_key != -1){
						$p = $row[$product_failed_key];
						
						if($p != "" && $p != "-"){//
							$prod = ORM::factory("omm_product");		
							
							$_p = explode(',',$p);
							
							$prod_id = $prod->createIfNotExist($_p[0]);
							
							if(isset($_p[1])){
								$member->addProduct($prod_id,'failed',$_p[1]);	
							}else{
								$member->addProduct($prod_id,'failed');
							}
							
							
													
						}
					}					
					
					
					if($list_key != -1){
						
						$_listname = $row[$list_key];
						
						$l = explode(',',$_listname);
						
						$_list = ORM::factory("omm_list")->where('name', $l[0])->find();
						
						if(!$_list->loaded){
							$_list->name = $_listname;
							$_list->code = string::random_string('unique');
				        	$_list->omm_client_id = $client->id;
				        	$_list->created_date = date("Y-m-d");
				        	$_list->type = 'single';
				        	$_list->saveObject();							
						}
						
						if(isset($l[1])){
							$member->addList($_list->id, $l[1]);
						}else{
							$member->addList($_list->id, date("Y-m-d H:i:s"));	
						}
						
					}else{
						if($list_id != 0){
							$member->addList($list_id, date("Y-m-d H:i:s"));
						}
						
					}
					
					//$member->addTags($tags, false);						
					
					$post = new Object();
					foreach ($headerreferences as $key => $h){
						$ff = $fields[$key];
						//echo $key."<br>";
						//echo $ff->type;
						if(isset($row[$h])){
							if($ff->type == "singleselectradio" || $ff->type == "singleselectdropdown" || $ff->type == "multiselect"){
								//$post->$h = $field->getCodeFromValue($row[$key]);
								$array = explode(",",$row[$h]);
								$atad = array();
								foreach ($array as $k => $a){
									$b = trim($a);
									if($b != "") {
										$code = $ff->getCodeFromValue($b);
										if($code != '-'){
											$atad[] = $code;
										}
									}
								}
								$post->$key = $atad;
								unset($array);
							}else{
								$post->$key = string::replaceChars($row[$h]);
							}

						}else{
							$post->$key = "";
						}
						//$post->$key = $this->replaceChars($row[$h]);
						//echo "<br>".$key." = ".$post->$key."<br/>";
					}

					try {
						$member->saveData($post,$client->getAllFields($list_id), false);
						switch ($member->status){
							case 'active':
								$imported_active[] = $email;
								break;
							case 'unsubscribed':
								$imported_unsubscribed[] = $email;
								break;
							case 'deleted':
								$imported_deleted[] = $email;
								break;
							default:
								break;
						}
						unset($member);
						unset($post);
					}catch (Exception $e){
						KOHANA::log("error","prereg member adatok mentés, member_id=".$member->id." ::: ".$e);
						$errors[] = $email;
					}
				}
				$i++;
			}/////////////adatok vége

			$result["now_imported"] = $all;
			$result["succes_active"] = sizeof($imported_active);
			$result["succes_unsubscribed"] = sizeof($imported_unsubscribed);
			$result["succes_deleted"] = sizeof($imported_deleted);
			$result["updated"] = sizeof($modified_user);
			$result["new_count"] = sizeof($new_user);
			$result["errors"] = sizeof($errors);

			if(!isset($_SESSION['import_errors'])){
				$_SESSION['import_errors'] = " ";
			}

			foreach($errors as $e){
				$_SESSION['import_errors'] .= $e.",";
			}

			$hason = $all;

			if(isset($_SESSION['import_actrownumber'])){
				$act = $_SESSION['import_actrownumber'] + $hason;
			}else{
				$act = $hason;
			}

			$result['ACTROWNUMBER'] = $act;
			$result['ALLROWNUMBER'] = $csvRowNum;

			if(($act + $alloffset) >= $csvRowNum){

				$result['STATUS'] = "READY";

				unset($_SESSION['import_rownumber']);
				unset($_SESSION['import_actrownumber']);


				$log = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body style=\"font-family: sans-serif;\">";
				$log .= "Importálás "."\n"."<br>";
				$log .= "Dátum: ".date("Y-m-d H:i:s")."\n"."<br>";
				$log .= "Felhasználó:".$_SESSION['auth_user']->username." (".$_SESSION['auth_user']->email.")"."\n"."<br>";
				$log .= "-----------------------------"."\n"."<br>";
				$log .= "\n\n"."<br>"."<br>";
				$log .= "Hibás e-mail: "."\n"."<br>";
				$log .= "-----------------------------"."\n"."<br>";

				$err = explode(",",$_SESSION['import_errors']);

				foreach($err as $m  ){
					$log .= $m."\n"."<br>";
				}

				$log .= "</body></html>";

				$time = date("YmdHis");

				file_put_contents("./upload/importlog_".$time."_".$_SESSION['auth_user']->username.".html", $log);

				$error_log = '<a style="color:#227FAF;font-weight:bold" target="_blank" href="'.url::base().'upload/importlog_'.$time.'_'.$_SESSION['auth_user']->username.'.html">Hibás sorok</a>';

				$result['error_log'] = $error_log;

				unset($_SESSION['import_errors']);

			}else{
				$result['STATUS'] = "WORKING";

				$_SESSION['import_actrownumber'] = $act;

			}

			$template->json = json_encode($result);
			$template->render(TRUE);

		}
	}
	
	
	public function deleteClient($id){
		$db = new Database("own");
		
		$client = ORM::factory("omm_client")->where('id' , $id)->find();
		var_dump($client->name);
		$campaigns = ORM::factory("omm_campaign")->where("omm_client_id",$client->id)->find_all();

		$sql = "DELETE FROM omm_letter_templates WHERE omm_client_id = ".$client->id." ";
		$db->query($sql);
		
		foreach($campaigns as $c){
			
			$letters = $db->from("omm_letters")->where("omm_campaign_id",$c->id)->get();
			
			foreach($letters as $l){
				var_dump($l->name);
				$sql = "DELETE FROM omm_letter_bounces WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);
				
				$sql = "DELETE FROM omm_letter_links WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);

				$sql = "DELETE FROM omm_letter_link_clicks WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);

				$sql = "DELETE FROM omm_letter_open WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);
				
				$sql = "DELETE FROM omm_letter_open_log WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);
				
				$sql = "DELETE FROM omm_letter_recip_conditions WHERE omm_letter_id = ".$l->id." ";
				$db->query($sql);												
				
				$sql = "DELETE FROM omm_letters WHERE id = ".$l->id." ";
				$db->query($sql);				
					
			}
		
				$sql = "DELETE FROM omm_campaigns WHERE id = ".$c->id." ";
				$db->query($sql);
						
		}
		
		
		$lists = $db->from("omm_lists")->where("omm_client_id",$client->id)->get();
		
		foreach($lists as $l){
			var_dump($l->name);	
			
			$sql = "DELETE FROM omm_list_connections WHERE omm_list_id = ".$l->id." ";
			$db->query($sql);			
			
			$fields = $db->from("omm_list_fields")->where("omm_list_id",$l->id)->get();
			foreach($fields as $f){
				$sql = "DELETE FROM omm_list_field_values WHERE omm_list_field_id = ".$f->id." ";
				$db->query($sql);	

				$sql = "DELETE FROM omm_list_fields WHERE id = ".$f->id." ";
				$db->query($sql);			
			}

			$forms = $db->from("omm_list_forms")->where("omm_list_id",$l->id)->get();
			foreach($forms as $f){
				$sql = "DELETE FROM omm_list_form_fields WHERE omm_list_form_id = ".$f->id." ";
				$db->query($sql);	

				$sql = "DELETE FROM omm_list_forms WHERE id = ".$f->id." ";
				$db->query($sql);			
			}			 
			
			$groups = $db->from("omm_list_groups")->where("omm_list_id",$l->id)->get();
			foreach($groups as $f){
				$sql = "DELETE FROM omm_list_group_conditions WHERE omm_list_group_id = ".$f->id." ";
				$db->query($sql);	

				$sql = "DELETE FROM omm_list_groups WHERE id = ".$f->id." ";
				$db->query($sql);			
			}			
			 
			$members = $db->from("omm_list_members")->where("omm_list_id",$l->id)->get();
			foreach($members as $f){
				$sql = "DELETE FROM omm_list_member_datas WHERE omm_list_member_id = ".$f->id." ";
				$db->query($sql);	

				$sql = "DELETE FROM omm_list_members WHERE id = ".$f->id." ";
				$db->query($sql);			
			}			
			
			$sql = "DELETE FROM omm_lists WHERE id = ".$l->id." ";
			$db->query($sql);	
		}
		
		$sql = "DELETE FROM omm_clients WHERE id = ".$id." ";
		$db->query($sql);					
		die();	
	}
	
	
	public function getMemberStatistic2(){
		ini_set('max_execution_time', 0);
		$members = ORM::factory("omm_list_member")->where('omm_list_id!=' , '0')->where('status','active')->find_all();
		$lists = array();
		foreach($members as $m){
			$line = $m->getMemberStat();
			
			if($m->omm_list->status != 'deleted'){

				//$ret = array('clickedratio' => 0, 'openedratio' => 0, 'allsent' => 0, 'allhtml' => 0, 'alllinks' => 0 , 'opened' => 0, 'clicked' => 0);
				$csv = '"'.$m->email.'";"'.$line['allsent'].'";"'.$line['allhtml'].'";"'.$line['alllinks'].'";"'.$line['opened'].'";"'.$line['clicked'].'"'."\n";
				file_put_contents("/tmp/silco_members.csv", $csv, FILE_APPEND | LOCK_EX);				
				
			}
			

				
		}
		
	}
	
	public function epitkezemProc($limit,$page){
		ini_set('max_execution_time', 0); //300 seconds = 5 minutes
		
		$db = new Database("own");
		
		$sql = "SELECT id FROM omm_list_members LIMIT $limit, $page";
		
		$mems = $db->query($sql);
		
		foreach($mems as $m){

			$relativ = $db->query("SELECT count(id) as ossz FROM omm_list_member_connect WHERE omm_list_member_id = ".$m->id." AND omm_list_id = 4 "); 	
			$kupon = $db->query("SELECT count(id) as ossz FROM omm_list_member_connect WHERE omm_list_member_id = ".$m->id." AND omm_list_id = 6 ");
			$magazin = $db->query("SELECT count(id) as ossz FROM omm_list_member_connect WHERE omm_list_member_id = ".$m->id." AND omm_list_id = 5 ");
			
			if($relativ[0]->ossz <= 0){
				
				if($kupon[0]->ossz <= 0 && $magazin[0]->ossz <= 0){
					
					$db->query("INSERT INTO omm_list_member_connect SET omm_list_member_id = ".$m->id.",omm_list_id = 4, reg_date = '2012-09-17 22:00',status='active',code='".string::random_string('unique')."' ");
					
				}
				
				
			}
			
		}
		
		
		
		
	} 
	
	public function getMemberStatistic(){
		ini_set('max_execution_time', 0); //300 seconds = 5 minutes

		$letters = ORM::factory("omm_letter")->where('status' , 'active')->find_all();
		
		$members_all = array();
		$i = 0;
		foreach($letters as $l){
			if($i < 20) continue;
			if($l->isSent()){
				
				$members = $l->getMembersForStatImport('email','asc',0,10000);	
				
				foreach($members as $m){
					
					$csv = '"'.$m->email.'";"1";"'.$m->open.'";"'.$m->clicked.'";"'.$m->temp_error.'";"'.$m->perm_error.'";"'.$m->unsubscribed.'"'."\n";
					
					file_put_contents("/tmp/silko_".$i.".csv", $csv, FILE_APPEND | LOCK_EX);
/*					
					if(!($members_all[$m->email])){
						$sor = array();
						$sor['letters'] = 1;
						$sor['open'] = $m->open;
						$sor['clicked'] = $m->clicked;		
						$sor['temp_error'] = $m->temp_error;		
						$sor['perm_error'] = $m->perm_error;		
						$sor['unsubscribed'] = $m->unsubscribed;		
						
						$members_all[$m->email] = $sor;
										
					}else{
						
						$oldsor = $members_all[$m->email];
						
						$sor = array();
						$sor['letters'] = $oldsor['open']+1;
						$sor['open'] = $oldsor['open'] + $m->open;
						$sor['clicked'] = $oldsor['clicked'] + $m->clicked;		
						$sor['temp_error'] = $oldsor['temp_error'] + $m->temp_error;		
						$sor['perm_error'] = $oldsor['perm_error'] + $m->perm_error;		
						$sor['unsubscribed'] = $oldsor['unsubscribed'] + $m->unsubscribed;						
						
						$members_all[$m->email] = $sor;
						
					}					
*/
				}
				
				
							
				
				
			$i++;
			}
		}
		

		$csv = '"E-mail cím";"Kiküldött levelek száma";"Megnyitás szám";"Átkattintások száma";"Átmeneti hibák","Állandó hibák";"Leiratkozás"'."\n";

		echo "ok";	
		
	}		
	
	public function getLetterSendingStatistic($letterid){
		$letter = ORM::factory("omm_letter")->where('id' , $letterid)->find();
		$db = new Database("own");
				
		$start = $letter->timing_date." ".$letter->timing_hour.":00:00";
		
		$sql = "SELECT max(sent_time) as max FROM omm_jobs WHERE omm_letter_id = '".$letterid."' AND status = 'sent' ";
		$ends = $db->query($sql);		
		$end = $ends[0]->max;		
		
		$start_date = DateTime::createFromFormat('Y-m-d H:i:s', $start);
		$date = $start_date;
		$end_date = DateTime::createFromFormat('Y-m-d H:i:s', $end);
		echo 'name:;'.$letter->name."\n";
		echo 'start:;'.$start_date->format('Y-m-d H:i:s')."\n"; 
		echo 'end:;'.$end_date->format('Y-m-d H:i:s')."\n";
		
		while($date < $end_date){
			
			$sql = "SELECT count(id) as ossz FROM omm_jobs WHERE omm_letter_id = '".$letterid."' AND status = 'sent' AND sent_time LIKE '".$date->format('Y-m-d H')."%'";	
			$_ossz = $db->query($sql);		
			$ossz = $_ossz[0]->ossz;
	
			echo $date->format('Y-m-d H').";".$ossz."\n";
			
			$date->add(new DateInterval('PT1H'));
		}
		
		
		
	}	
	
	public function getLetterStatistic($letterid){
		$letter = ORM::factory("omm_letter")->where('id' , $letterid)->find();
		
		$members = $letter->getMembersForStatImport('email','asc',0,10000);

		$csv = '"Lista neve";"E-mail cím";"Vezetéknév";"Keresztnév";"Kiküldés ideje";"Megnyitás ideje";"Megnyitás szám";"Átkattintások száma";"Átmeneti hibák";"Állandó hibák";"Leiratkozás"'."\n";
		
		foreach($members as $m){

			$csv .= '"'.$m->list_name.'";"'.$m->email.'";"'.$m->vezeteknev.'";"'.$m->keresztnev.'";"'.$m->sent_time.'";"'.$m->open_time.'";"'.$m->open.'";"'.$m->clicked.'";"'.$m->temp_error.'";"'.$m->perm_error.'";"'.$m->unsubscribed.'"'."\n";
		
		}
		
		echo $csv;	
		
	}	
	
	
	public function getLetterStatisticMore(){
		$letters = ORM::factory("omm_letter")->in('id' , array(43,46,54,49,51,56,44,57,66,68))->find_all();
		$rows = array();
		$row = array();
		
		foreach($letters as $letter){
			echo $letter->name."<br>";
			$members = $letter->getMembersForStatImport2('email','asc',0,10000);	
			$newrow = array();
			echo sizeof($members)."<br>";
			foreach($members as $m){
			
				if(isset($rows[$m->email])){
					
					//$newrow['vezeteknev'] = $m->vezeteknev;
					//$newrow['keresztnev'] = $m->keresztnev;				
					$newrow['open'] = intval($rows[$m->email]['open']) + intval($m->open);
					$newrow['clicked'] = intval($rows[$m->email]['clicked']) + intval($m->clicked);
					$newrow['temp_error'] = intval($rows[$m->email]['temp_error']) + intval($m->temp_error);
					$newrow['perm_error'] = intval($rows[$m->email]['perm_error']) + intval($m->perm_error);
					$newrow['unsubscribed'] = intval($rows[$m->email]['unsubscribed']) + intval($m->unsubscribed);	
					
					
				}else{
					//$newrow['vezeteknev'] = $m->vezeteknev;
					//$newrow['keresztnev'] = $m->keresztnev;
					$newrow['open'] = $m->open;
					$newrow['clicked'] = $m->clicked;
					$newrow['temp_error'] = $m->temp_error;
					$newrow['perm_error'] = $m->perm_error;
					$newrow['unsubscribed'] = $m->unsubscribed;	
					
				}
				
				$rows[$m->email] = $newrow;
			}
			
			echo '<br/>!!!'.sizeof($rows)."<br/>";
		}
		
		

		$csv = '"E-mail cím";"Megnyitás szám";"Átkattintások száma";"Átmeneti hibák","Állandó hibák";"Leiratkozás"'."\n";
		$open = 0;
		foreach($rows as $key => $row){
			$open = $open + $row['open'];
			$csv .= '"'.$key.'";"'.$row['open'].'";"'.$row['clicked'].'";"'.$row['temp_error'].'","'.$row['perm_error'].'";"'.$row['unsubscribed'].'"'."\n";
		
		}
		echo "<br><br>".$open."<br>";
		echo $csv;	
		
	}	
	
	public function getLetterStatisticCampaign($cid){
		
		$campaigns = ORM::factory("omm_campaign")->where("omm_client_id",$cid)->find_all();
		
		$letters = ORM::factory("omm_letter")->where('status', 'active')->where('omm_campaign_id' , $cid)->find_all();
		
		$letnum = sizeof($letters);
				
		$rows = array();
		$row = array();
		
		foreach($letters as $letter){
			//echo $letter->name."<br>";
			$members = $letter->getMembersForStatImport2('email','asc',0,10000);	
			$newrow = array();
			//echo sizeof($members)."<br>";
			foreach($members as $m){
			
				if(isset($rows[$m->email])){
					
					//$newrow['vezeteknev'] = $m->vezeteknev;
					//$newrow['keresztnev'] = $m->keresztnev;				
					$newrow['open'] = intval($rows[$m->email]['open']) + intval($m->open);
					$newrow['clicked'] = intval($rows[$m->email]['clicked']) + intval($m->clicked);
					$newrow['temp_error'] = intval($rows[$m->email]['temp_error']) + intval($m->temp_error);
					$newrow['perm_error'] = intval($rows[$m->email]['perm_error']) + intval($m->perm_error);
					$newrow['unsubscribed'] = intval($rows[$m->email]['unsubscribed']) + intval($m->unsubscribed);	
					
					
				}else{
					//$newrow['vezeteknev'] = $m->vezeteknev;
					//$newrow['keresztnev'] = $m->keresztnev;
					$newrow['open'] = $m->open;
					$newrow['clicked'] = $m->clicked;
					$newrow['temp_error'] = $m->temp_error;
					$newrow['perm_error'] = $m->perm_error;
					$newrow['unsubscribed'] = $m->unsubscribed;	
					
				}
				
				$rows[$m->email] = $newrow;
			}
			
			//echo '<br/>!!!'.sizeof($rows)."<br/>";
		}
		
		

		$csv = '"E-mail cím";"Levelek száma";"Megnyitás szám";"Átkattintások száma";"Átmeneti hibák","Állandó hibák";"Leiratkozás"'."\n";
		$open = 0;
		foreach($rows as $key => $row){
			$open = $open + $row['open'];
			$csv .= '"'.$key.'";"'.$letnum.'";"'.$row['open'].'";"'.$row['clicked'].'";"'.$row['temp_error'].'","'.$row['perm_error'].'";"'.$row['unsubscribed'].'"'."\n";
		
		}
		//echo "<br><br>".$open."<br>";
		echo $csv;	
		
	}		
	
	public function getLetterStatisticCampaignGroups(){
		
		$groups = array(
			'Levél 1' => array(246),
			'Levél 2' => array(249),
			'Levél 3' => array(255,257,254,256),
			'Levél 4' => array(267,266,265,264),
			'Levél 5' => array(270,274,271,275,272,273,277,276),
			'Levél 6' => array(279)
		);
		
		$rows = array();
		$row = array();		

		$hh = array('reg_date','vezeteknev','keresztnev','cegnev','varos','iranyitoszam','cim','mobil','cegtipus','partner');


		foreach($groups as $gkey => $g){
			
			$grows = array();
			$grow = array();
			$letters = ORM::factory("omm_letter")->in('id' , $g)->find_all();
			
			foreach($letters as $letter){
				$members = $letter->getMembersForStatImport2('email','asc',0,10000);	
				$gnewrow = array();
				foreach($members as $m){
					if(isset($grows[$m->email])){
						//$newrow['vezeteknev'] = $m->vezeteknev;
						//$newrow['keresztnev'] = $m->keresztnev;				
						$gnewrow['open'] = intval($grows[$m->email]['open']) + intval($m->open);
						$gnewrow['clicked'] = intval($grows[$m->email]['clicked']) + intval($m->clicked);
						$gnewrow['temp_error'] = intval($grows[$m->email]['temp_error']) + intval($m->temp_error);
						$gnewrow['perm_error'] = intval($grows[$m->email]['perm_error']) + intval($m->perm_error);
						$gnewrow['unsubscribed'] = intval($grows[$m->email]['unsubscribed']) + intval($m->unsubscribed);	
					
					}else{
						//$newrow['vezeteknev'] = $m->vezeteknev;
						//$newrow['keresztnev'] = $m->keresztnev;
						$gnewrow['id'] = $m->id;
						$gnewrow['open'] = $m->open;
						$gnewrow['clicked'] = $m->clicked;
						$gnewrow['temp_error'] = $m->temp_error;
						$gnewrow['perm_error'] = $m->perm_error;
						$gnewrow['unsubscribed'] = $m->unsubscribed;	
						
					}
	
					$grows[$m->email] = $gnewrow;
				}
			
				
			}///levelek
			
			//echo '<br>'.sizeof($grows);		
				
			foreach($grows as $key => $gr){
				
				if(isset($rows[$key])){
					
					$rows[$key][$gkey." open"] = $gr['open'];	
					$rows[$key][$gkey." clicked"] = $gr['clicked'];	
					$rows[$key][$gkey." temp_error"] = $gr['temp_error'];	
					$rows[$key][$gkey." unsubscribed"] = $gr['unsubscribed'];													
					
				}else{
					
					$member = ORM::factory("omm_list_member")->find($gr['id']);
					$list = ORM::factory("omm_list")->find($member->omm_list_id);
		        	$mo = $member->getFullData($list->fields());
					
					
					foreach($hh as $h){
						if(isset($mo->$h)){
							$rows[$key][$h] = $mo->$h;							
						}else{
							$rows[$key][$h] = "-";								
						}
					}
					
															
					$rows[$key][$gkey." open"] = $gr['open'];	
					$rows[$key][$gkey." clicked"] = $gr['clicked'];	
					$rows[$key][$gkey." temp_error"] = $gr['temp_error'];	
					$rows[$key][$gkey." unsubscribed"] = $gr['unsubscribed'];													
						
				}
		
			}				
			
				
		}///group
		
		//var_dump($rows);
		$csv = '"E-mail cím"';

		foreach($hh as $h){
			$csv .= ';"'."$h".'"';
		}		

		
		foreach($groups as $gkey => $g){
			$csv .= ';"'."$gkey open".'"';
			$csv .= ';"'."$gkey clicked".'"';
			$csv .= ';"'."$gkey temp_error".'"';
			$csv .= ';"'."$gkey unsubscribed".'"';
		}
		
		
		$csv .= "\n";
		
		$open = 0;
		foreach($rows as $key => $row){
			
			$csv .= '"'.$key.'"';
			
			foreach($hh as $h){
				$csv .= ';"'."$row[$h]".'"';
			}		
			

			
			
			foreach($groups as $gkey => $g){
				if(isset($row[$gkey." open"])){
					$csv .= ';"'.$row[$gkey." open"].'"';	
				}else{
					$csv .= ';"-"';				
				}

				if(isset($row[$gkey." clicked"])){
					$csv .= ';"'.$row[$gkey." clicked"].'"';	
				}else{
					$csv .= ';"-"';				
				}
				
				if(isset($row[$gkey." temp_error"])){
					$csv .= ';"'.$row[$gkey." temp_error"].'"';	
				}else{
					$csv .= ';"-"';				
				}
				
				if(isset($row[$gkey." unsubscribed"])){
					$csv .= ';"'.$row[$gkey." unsubscribed"].'"';	
				}else{
					$csv .= ';"-"';				
				}								
				
			}			
			
			$csv .= "\n";
		}
		//echo "<br><br>".$open."<br>";
		echo $csv;	
		
	}		
	
	
}
?>