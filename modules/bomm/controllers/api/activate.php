<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Activate_Controller extends Apicontroller_Controller {

	private $ACTIVE = 'active';


	public function __construct(){
		parent::__construct();
	}

	public function index(){
		Event::run('member.pre_activate');

		if(isset($_GET['code']) && $_GET['code'] != ""){

			$dbcore = new Database("");
				
			if(!isset($_GET['a'])){
				Event::run('error.activate_missingorinvalid_code');
			}
				
			$acode = $_GET['a'];
				
			$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
				
			if(sizeof($acc) == 0){
				Event::run('error.activate_missingorinvalid_code');
			}
				
			$user = $acc[0]->km_user;
				
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;

			Kohana::config_set('database.own',$dbown);

				
			$member = ORM::factory('omm_list_member')->where('status','prereg')->where('code',$_GET['code'])->find();
				
			if($member->loaded){

				$member->status = $this->ACTIVE;

				$date =  date("Y-m-d H:i:s");

				$member->activation_date = $date;
				$list = ORM::factory('omm_list')->where('code',$_GET['lcode'])->find();
				try {
					$member->save();
					$param = array('member' => $member, 'kmuser' => $user, 'list' => $list);	
					Event::run('member.post_activate',$param);

				}catch (Exception $e){
					KOHANA::log("error","aktiválás db hiba, member_id=".$member->id." ::: ".$e);
					Event::run('error.activate_db_error',$member);
				}

			}else{

				$member = ORM::factory('omm_list_member')->where('status!=','prereg')->where('code',$_GET['code'])->find();

				if($member->loaded){
					//$list = ORM::factory("omm_list")->find($member->omm_list_id);
					$list = ORM::factory('omm_list')->where('code',$_GET['lcode'])->find();
					Event::run('error.subscribe_second_subscribe',$list);
				}else{
					Event::run('error.activate_missingorinvalid_code');
				}


			}
		}else{
			Event::run('error.activate_missingorinvalid_code');
		}
	}



	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}



}
?>