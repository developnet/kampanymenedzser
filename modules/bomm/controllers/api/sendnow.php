<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Sendnow_Controller extends Ajax_Controller {
	
	public function __construct(){
		$this->req_auth = TRUE;
		$this->req_ajax = TRUE;
		$this->output_json_file = FALSE;
		parent::__construct();
	}
	
	
	public function index(){
		
	}
	
	public function create(){
		$letter_id = $this->uri->segment(4,0);
		$job_id = $this->uri->segment(5,0);
		
		$breaklimit = KOHANA::config('core.job_create_break_limit_sendnow');
		//$breaklimit = 1;
		 
		Benchmark::start('create');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('sendnow', $job_id, $letter_id);
		$job_id = $dayjob->id;
		
		$cr = $dayjob->createJobs($breaklimit, $letter_id);
		
		Benchmark::stop('create');
		$cr['create']['benchmark'] = Benchmark::get('create');
		
		$this->jsonarray['create'] = $cr['create'];
		$this->jsonarray['job_id'] = $job_id;
	}
	
	public function prepare(){
		$letter_id = $this->uri->segment(4,0);
		$job_id = $this->uri->segment(5,0);
		$breaklimit = KOHANA::config('core.job_prepare_break_limit_sendnow');

		Benchmark::start('prepare');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('sendnow', $job_id, $letter_id);
		$job_id = $dayjob->id;
		
		$cr = $dayjob->prepareJobs($breaklimit);		
		
		Benchmark::stop('prepare');
		$cr['prepare']['benchmark'] = Benchmark::get('prepare');

		
		$this->jsonarray['prepare'] = $cr['sendnow'];
		$this->jsonarray['job_id'] = $job_id;
	}
	
	public function send(){
		$letter_id = $this->uri->segment(4,0);
		$job_id = $this->uri->segment(5,0);
		$breaklimit = KOHANA::config('core.job_send_break_limit_sendnow');		
		
		Benchmark::start('send');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('sendnow', $job_id, $letter_id);
		$job_id = $dayjob->id;
		
		$cr = $dayjob->sendJobs($breaklimit);		
		
		Benchmark::stop('send');
		$cr['send']['benchmark'] = Benchmark::get('send');

		
		$this->jsonarray['send'] = $cr['sendnow'];
		$this->jsonarray['job_id'] = $job_id;		
		
	}
	
}
?>