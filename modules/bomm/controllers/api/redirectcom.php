<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

/**
 */
class Redirectcom_Controller extends Apicontroller_Controller {


	public function __construct(){
		parent::__construct();
	}

	public static function errorHandler($errno, $errstr, $errfile, $errline){
		echo "Hiba";
	}

	public function index(){

		//set_error_handler(array('Redirectcom_Controller', 'errorHandler'));
		//set_exception_handler(array('Redirectcom_Controller', 'errorHandler'));

		if(isset($_GET['url'])){

			$tablename = Kohana::config("database.default");
			$tp = $tablename['table_prefix'];

			try{

				$dbcore = new Database("");

				if(!isset($_GET['a'])){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód1!");
				}

				$acode = $_GET['a'];

				$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");

				if(sizeof($acc) == 0){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód2!");
				}

				$user = $acc[0]->km_user;

				$dbown = Kohana::config('database.own');
				$dbown['connection']['database']="km_".$user;

				Kohana::config_set('database.own',$dbown);


				$db = new Database("own");


				$member = $db->query("SELECT id,code FROM ".$tp."omm_list_members WHERE code='".$_GET['m']."' LIMIT 1");
				$link = $db->query("SELECT id,url,url_name,omm_letter_id,omm_job_id FROM ".$tp."omm_common_letter_links WHERE code='".$_GET['url']."' LIMIT 1");

				if(sizeof($member) != 1) throw new Kohana_User_Exception('Invalid member!', "Invalid member!");
				if(sizeof($link) != 1) throw new Kohana_User_Exception('Invalid url!', "Invalid url!");


				$db->query("INSERT INTO ".$tp."omm_common_letter_link_clicks
				               SET omm_letter_link_id=".$link[0]->id.", 
				               	   omm_job_id=".$link[0]->omm_job_id.",
				                   omm_list_member_id = ".$member[0]->id.",
								   omm_letter_id = ".$link[0]->omm_letter_id."
				                   ");

					

				////////////////////////////////////////////////////////////////////////////////
				//////////////////megnyitás regisztrálás ha még nem volt illetve eltelt legalább fél óra

				$open_timeout = Kohana::config("bomm.open_timeout");
				$today = date("Y-m-d H:i:s");
				$date = date( "Y-m-d H:i:s", strtotime( $today." -".$open_timeout." seconds" ));

				$openres = $db->query("SELECT id
										 FROM ".$tp."omm_common_letter_open_log 
										WHERE omm_list_member_id = ".$member[0]->id." 
										  AND omm_letter_id = ".$link[0]->omm_letter_id."
										  AND opened >= '.$date.' ");				

				if(sizeof($openres) == 0){
					$db->query("INSERT INTO ".$tp."omm_common_letter_open_log
					               SET 
					                   omm_list_member_id = ".$member[0]->id.",
					                   omm_job_id = ".$link[0]->omm_job_id.",
									   omm_letter_id = ".$link[0]->omm_letter_id."
					                   ");					
				}
					
				//////////////////////megynitás
				////////////////////////////////////////////////
				//url::redirect($link[0]->url);

				if($link[0]->url_name == 'unsubscribe'){
					url::redirect($link[0]->url."?a=".$acode."&code=".$member[0]->code);
				}else{
					url::redirect($link[0]->url);
				}

			}catch (Exception $e){
				throw $e;
			}



		}

	}

	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}



}
?>