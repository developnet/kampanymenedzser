<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Statistic_Controller extends Ajax_Controller {

	private $midnight = ' 23:59:59';
	private $morning = ' 00:00:00';
	
	public function __construct(){
		$this->req_auth = FALSE;
		$this->req_ajax = FALSE;
		$this->output_json_file = FALSE;
		parent::__construct();
	}	
	
	public function index(){
		
		$today = date( "Y-m-d");
		$this->generateListStat($today,$today);
	}
	
	
	public function generateListStat($_from,$_to){
		$from = $this->uri->segment(4,$_from);
		$to = $this->uri->segment(5,$_to);		
		
		if($from == "" && $to == ""){
			throw new Exception("Intervall not set");
		}
		
		$db = new Database("own");
		
		$act = $from;
		
		$to = date( "Y-m-d", strtotime( $to." +1 day" ));
		
		while($act != $to){
			
			$lists = $db->from('omm_lists')->get();
			
			foreach($lists as $list){
				
				$sql ="
				   INSERT INTO b_omm_stat_lists (day,omm_list_id,prereg_count,active_count,error_count,unsubscribed_count,subscribed_today,unsubscribed_today)
						SELECT 
							'".$act."' as day,	
							l.id as omm_list_id,
							(SELECT count(*) FROM b_omm_list_members WHERE status = 'prereg' AND omm_list_id = l.id) as prereg_count,	
							(SELECT count(*) FROM b_omm_list_members WHERE status = 'active' AND omm_list_id = l.id) as active_count,
							(SELECT count(*) FROM b_omm_list_members WHERE status = 'error' AND omm_list_id = l.id) as error_count,	
							(SELECT count(*) FROM b_omm_list_members WHERE status = 'unsubscribed' AND omm_list_id = l.id) as unsubscribed_count,
							(SELECT count(*) FROM b_omm_list_members WHERE reg_date between '".$act." 00:00:00' AND '".$act." 23:59:59' AND omm_list_id = l.id) as subscribed_today, 
							(SELECT count(*) FROM b_omm_list_members WHERE unsubscribe_date between '".$act." 00:00:00' AND '".$act." 23:59:59' AND unsubscribe_date IS NOT NULL AND omm_list_id = l.id) as unsubscribed_today
							FROM b_omm_lists l WHERE l.id = ".$list->id." ;
							";	
				
				
				$count = $db->from('omm_stat_lists')->where('day',$act)->where('omm_list_id',$list->id)->count_records();

				if($count == 0){
					$db->query($sql);	
				}
				 
								
			}
			
			$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
		}
		
		
		
	}
	
}
?>