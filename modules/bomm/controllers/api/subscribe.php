<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Subscribe_Controller extends Apicontroller_Controller {

	private $list;

	public function __construct(){
		parent::__construct();
	}

	private function parse_url_domain($url){
		$parsed = parse_url($url);
		$hostname = $parsed['host'];
		return $hostname;
	}

	public function index(){
		Event::run('member.pre_subscription',$_POST);

		if(isset($_POST['apikey']) && $_POST['apikey'] != "" && $_POST['apiuser'] && $_POST['apiuser'] != ""){
			$api = true;
		}else{
			$api = false;
		}

		if($api){

			if(strlen($_POST['apikey']) != 32){

				$ret = array('REQUEST' => 'OK',
	        				 'STATUS'  => 'ERROR00',
	        				 'MSG'  => 'Érvénytelen API kulcs.'	
	        				 );

	        	echo json_encode($ret);
	        	KOHANA::shutdown();
	        	die();

			}
				
			$dbcore = new Database("");
			$apikey = $_POST['apikey'];
			$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE apikey = '".$apikey."' AND km_user = '".$_POST['apiuser']."' ");
			
			if(sizeof($acc) == 0){
				
				$ret = array('REQUEST' => 'OK',
	        				 'STATUS'  => 'ERROR01',
	        				 'MSG'  => 'Érvénytelen API kulcs.'	
	        				 );

	        	echo json_encode($ret);
	        	KOHANA::shutdown();
	        	die();
			}			
			
			$user = $acc[0]->km_user;
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;
			Kohana::config_set('database.own',$dbown);			
			
		}else{
				
			$dbcore = new Database("");
			if(!isset($_GET['a'])){
				Event::run('error.subscribe_list_error',"acc");
			}

			$acode = $_GET['a'];
			$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
			if(sizeof($acc) == 0){
				Event::run('error.subscribe_list_error',"acc");
			}

			$user = $acc[0]->km_user;
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;
			Kohana::config_set('database.own',$dbown);
			
		}

		if(!isset($_POST['listcode'])){
			
			if($api){
				$ret = array('REQUEST' => 'OK',
	        				 'STATUS'  => 'ERROR02',
	        				 'MSG'  => 'Érvénytelen lista kód.'	
	        				 );

	        	echo json_encode($ret);
	        	KOHANA::shutdown();
	        	die();				
			}else{
				Event::run('error.subscribe_missing_list_code');	
			}
			
			
			
		}		

		
		
		$list = ORM::factory("omm_list")->where("code",$_POST['listcode'])->find();

		if($list->loaded){

			//if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!=""){
				/////////ha van referrer akkor lecsekkoljuk, hogy a domain megegyezik-e a clientnél beállítottal

				//$sitedomain = $list->omm_client->site_domain;

				//$raw_url = parse_url($_SERVER['HTTP_REFERER']);
				//preg_match ("/\.([^\/]+)/", $raw_url['host'], $domain_only);

				//$refDomain = $raw_url['host'];

				//if($refDomain != $sitedomain){
					//Event::run('error.subscribe_invalid_domain',$refDomain); invaldi domain védelem OFF
				//}

			//}//////////////és mit csináljunk ha nincs referrer?......
			
			$newpost = array();
			if(isset($_POST)){
				foreach($_POST as $key => $p){
					if(isset($_REQUEST[$key])){

						if(is_array($_REQUEST[$key])){
							$p = $_POST[$key];
						}else{
							$p = mb_convert_encoding($_REQUEST[$key], 'UTF-8',  mb_detect_encoding($_REQUEST[$key], 'UTF-8, ISO-8859-1, ISO-8859-2', true));
						}

					}else{
					 $p = $_POST[$key];
					}
					$p = str_replace("û","ű",$p);
					$p = str_replace("õ","ő",$p);
					$newpost[$key] = $p;
				}
			}

			$this->list = $list;
			$post = new Validation($newpost);
			$post->pre_filter('trim', TRUE);

			$post->add_rules('email','required', 'email');

			if ($post->validate())  {
					

				if($this->_unique_email($post->email,$list->omm_client->id) && !isset($post->km_update)){///////aktív usereket kiszürjük, másodszor ne iratkozzon már fel
					
					
					if($api){

						$ret = array('REQUEST' => 'OK',
	        						 'STATUS'  => 'ERROR1',
	        						 'MSG'  => 'Ez a feliratkozó már létezik a listán! Sikertelen feliratkozás.'	
	        						 );

	        						 echo json_encode($ret);
	        						 KOHANA::shutdown();
	        						 die();

					}else{
						Event::run('error.subscribe_second_subscribe',$list);
					}
					
					
				}

				$fields = $list->omm_client->getAllFields($list->id);

				if((bool)$list->unique_email){//ha a lista egyedi mailos

					////////ha van már ilyen mail a listán....és ugye nem aktív...mert az aktívok kiszűrődtek validálásnál
					////////ezáltal a prereg, leiratkozott, törölt stb státuszu emberek újra feliratkoznak, azért ujra vizsgáljuk h ne legyen aktív
					$member = ORM::factory("omm_list_member")->where('email',$post->email)->where('omm_client_id',$list->omm_client->id)->where('status!=','active')->find();

					if(isset($post->km_update)){
						$member = ORM::factory("omm_list_member")->where('email',$post->email)->where('omm_client_id',$list->omm_client->id)->find();	
					}
					
					if(!$member->loaded){
						$member = ORM::factory("omm_list_member");
						$member->code = string::random_string('unique');
						$member->omm_client_id = $list->omm_client->id;
					}
				}else{
					$member = ORM::factory("omm_list_member");
					$member->omm_client_id = $list->omm_client->id;
				}


				try {

					
					//$member->omm_list_id = $list->id;
					
					$member->manual = 0;
					
					if($member->reg_date == null || $member->reg_date == "" || isset($post->km_update_reg_date)){
						$member->reg_date = date("Y-m-d H:i:s");	
					}
					
					
					$member->status = 'prereg';

					$member->email = $post->email;
					$member->activation_date = null;
					$member->unsubscribe_date = null;

					$member->save();

					
					if(isset($post->list_delete) && $post->list_delete != ""){
						$lists = explode(',',$post->list_delete);
						
						foreach($lists as $l){
							
							$listo = ORM::factory("omm_list")->where('name', $l)->find();
							
							if($listo->loaded)							
								$member->delFromList($listo->id);
							
						}
					}					
					
					
					if(isset($post->km_update) && $member->loaded){
						$member->addList($list->id,date('Y-m-d H:i:s'));	
					
					}else{
						$member->addList($list->id,$member->reg_date);
					}				
					
					
					if(isset($post->tag_delete) && $post->tag_delete != ""){
						$tags = explode(',',$post->tag_delete);
						$_tags = array();
						$tago = ORM::factory("omm_tag");
						foreach($tags as $t){
							$tag_id = $tago->searchTagId($t);
							$_tags[] = array('id' => $tag_id);
						}
						
						$member->delTagsById($_tags);
						
					}					
					
					//////
					if(isset($post->tag) && $post->tag != ""){
						$tags = explode(',',$post->tag);
						$tags[] = 'Mindenki';
					}else{
						$tags = array('Mindenki');
					}

					$_tags = array();
					foreach($tags as $t){
						$tago = ORM::factory("omm_tag");
						$tag_id = $tago->createIfNotExist($t,'active');
						$_tags[] = array('id' => $tag_id);
					}
						
					$member->addTags($_tags, false);					
					/////////////////
						
						
	
					
					////product
					if(isset($post->product_purchased) && $post->product_purchased != ""){

						$p = $post->product_purchased;
						
						if($p != "" && $p != "-"){//
							$prod = ORM::factory("omm_product");		
							$prod_id = $prod->createIfNotExist(trim($p),$member->omm_client_id);
							
							$member->addProduct($prod_id,'purchased');						
						}						
					
					}					
					try {
					////product
						if(isset($post->product_ordered) && $post->product_ordered != ""){
							
							$prods = explode(',',$post->product_ordered);
							foreach($prods as $p){
								//$p = $post->product_ordered;
								if($p != "" && $p != "-"){//
									$prod = ORM::factory("omm_product");		
									$prod_id = $prod->createIfNotExist(trim($p),$member->omm_client_id);
									
									$member->addProduct($prod_id,'ordered');
								}								
								
							}
							
						}
										
					}catch (Exception $e){
						
						if($api){

						$ret = array('REQUEST' => 'OK',
	        						 'STATUS'  => 'ERROR22',
	        						 'MSG'  => 'Hiba a termék felvitelnél.'.$e	
	        						 );

	        						 echo json_encode($ret);
	        						 KOHANA::shutdown();
	        						 die();

						}else{
							KOHANA::log("error","Hiba a termék felvitelnél ::: ".$e);
						}
					}
					/////////////////////////különböző kódok
/*
					try {

						if(isset($post->formcode)){////////ha van formcode ...

							$form_code = $post->formcode;
							$form = ORM::factory('omm_list_form')->where('code',$form_code)->find();

							if($form->loaded){
								$member->omm_list_form_id = $form->id;
							}

						}

					}catch (Exception $e){
						KOHANA::log("warning","formcode ::: ".$e);
						/////////hát max nem íródik be
					}
*/
					//					try{
					//			        	if(isset($post->affiliatecode)){////////ha van affiliate kód ...
					//
					//			        		$aff_code = $post->affiliatecode;
					//			        		$code = ORM::factory('omm_aff_code')->where('code',$aff_code)->find();
					//
					//			        		if($code->loaded){
					//			        			$member->omm_aff_code_id = $code->id;
					//
					//			        			$log = ORM::factory('omm_aff_log');
					//			        			$log->create($code->id,'prereg',$list->id,$post->email);
					//
					//			        		}
					//		        		}else{//ha nincs aff kód megnézzük szerepel-e már logban, és azokoz a kódokhoz hozzádobjuk
					//
					//		        			$log = ORM::factory('omm_aff_log');
					//		        			$oldlogs = $log->getLogForEmail($post->email);
					//
					//
					//		        			foreach ($oldlogs as $l){
					//		        				$_log = ORM::factory('omm_aff_log');
					//			        			$_log->create($l->code_id,'prereg',$list->id,$post->email);
					//		        			}
					//
					//
					//
					//		        		}
					//	        		}catch (Exception $e){
					//						KOHANA::log("warning","affcode ::: ".$e);
					//	        			/////////hát max nem íródik be
					//					}
					/////////////////////////különböző kódok


					$member->save();


				}catch (Exception $e){

					if($api){

						$ret = array('REQUEST' => 'OK',
	        						 'STATUS'  => 'ERROR2',
	        						 'MSG'  => 'Hiba a prereg mentés során.'	
	        						 );

	        						 echo json_encode($ret);
	        						 KOHANA::shutdown();
	        						 die();

					}else{
						KOHANA::log("error","prereg member mentés, list_id=".$list->id." ::: ".$e);
					}


				}


				try {
					if(isset($post->km_emptynoupadte)){
						$member->saveData($post,$fields,false);	
					}else{
						$member->saveData($post,$fields,true);
					}
					
					
				}catch (Exception $e){

					if($api){

						$ret = array('REQUEST' => 'OK',
	        						 'STATUS'  => 'ERROR2',
	        						 'MSG'  => 'Hiba a prereg mentés során.'	
	        						 );

	        						 echo json_encode($ret);
	        						 KOHANA::shutdown();
	        						 die();

					}else{
						KOHANA::log("error","prereg member adatok mentés, member_id=".$member->id." ::: ".$e);
					}


				}





				try{///////////kapcsolódó listákra feliratkozás

					$connectedLists = $list->connectedLists('subscribe');
					$email = $member->email;

					foreach($connectedLists as $l){
						$member->addList($l->id,$member->reg_date);
					}


				}catch (Exception $e){///////////kapcsolódó listákra feliratkozás/////////////////////////////////
					KOHANA::log("error","list connections subscribe member_id=".$member->id." ::: ".$e);
				}


				$param = array('member' => $member, 'list' => $list, 'api' => $api, 'kmuser' => $user);
				Event::run('member.post_subscription', $param);
					
			}else{

				if($api){

					$ret = array('REQUEST' => 'OK',
	        						 'STATUS'  => 'ERROR3',
	        						 'MSG'  => 'Hibás e-mail.'	
	        						 );

	        						 echo json_encode($ret);
	        						 KOHANA::shutdown();
	        						 die();

				}else{
					Event::run('error.subscribe_email_error',$list);
				}


			}



		}else{

			if($api){

				$ret = array('REQUEST' => 'OK',
	        				 'STATUS'  => 'ERROR4',
	        				 'MSG'  => 'Hibás lista.'	
	        				 );

	        				 echo json_encode($ret);
	        				 KOHANA::shutdown();
	        				 die();

			}else{
				Event::run('error.subscribe_list_error',$_POST['listcode']);
			}


		}


	}

	public function _unique_email($email,$omm_client_id){
		////van-e ilyen aktív user?
		return  (bool) ORM::factory('omm_list_member')->where('email',$email)->where('omm_client_id',$omm_client_id)->where('status','active')->count_all();
	}



}
?>