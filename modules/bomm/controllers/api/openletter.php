<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Openletter_Controller extends Apicontroller_Controller {



	public function __construct(){
		parent::__construct();


	}


	public function index(){

		if(isset($_GET['code']) && $_GET['code'] != "" && isset($_GET['m']) && $_GET['m'] != ""){
			$tablename = Kohana::config("database.default");
			$tp = $tablename['table_prefix'];
				
			try {


				$dbcore = new Database("");
					
				if(!isset($_GET['a'])){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód!");
				}
					
				$acode = $_GET['a'];
					
				$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$acode."' ");
					
				if(sizeof($acc) == 0){
					throw new Kohana_User_Exception('Nincs ilyen fiók!', "Hibás a kód!");
				}
					
				$user = $acc[0]->km_user;
					
				$dbown = Kohana::config('database.own');
				$dbown['connection']['database']="km_".$user;

				Kohana::config_set('database.own',$dbown);


				$db = new Database("own");

				$open_timeout = Kohana::config("core.open_timeout");
				$today = date("Y-m-d H:i:s");
				$date = date( "Y-m-d H:i:s", strtotime( $today." -".$open_timeout." seconds" ));
				/////////////ha már volt és legalább fél órája (config)


				$member = $db->query("SELECT id FROM ".$tp."omm_list_members WHERE code='".$_GET['m']."' LIMIT 1");
				$open = $db->query("SELECT id,omm_letter_id FROM ".$tp."omm_letter_open WHERE code='".$_GET['code']."' LIMIT 1");

				if(sizeof($member) != 1) throw new Kohana_User_Exception('Invalid member!', "Invalid member!");
				if(sizeof($open) != 1) throw new Kohana_User_Exception('Invalid url!', "Invalid url!");

				$openres = $db->query("SELECT id
										 FROM ".$tp."omm_letter_open_log 
										WHERE omm_list_member_id = ".$member[0]->id." 
										  AND omm_letter_open_id = ".$open[0]->id."
										  AND opened >= '".$date."' ");
				if(sizeof($openres) == 0){
					if(isset($_SERVER['HTTP_USER_AGENT'])){
						$user_agent = $_SERVER['HTTP_USER_AGENT'];
					}else{
						$user_agent = 'unknow';
					}
						
						
					$db->query("INSERT INTO ".$tp."omm_letter_open_log
					               SET omm_letter_open_id=".$open[0]->id.", 
					                   omm_list_member_id = ".$member[0]->id.",
									   omm_letter_id = ".$open[0]->omm_letter_id.",
									   user_agent = '".$user_agent."'
					                   ");					
				}




			}catch(Exception $e){
				KOHANA::log("error","levél megnyitásának logolása, code=".$_GET['code']." ::: ".$e);
			}
				
				
		}

		$this->image = new Image('./stat.gif');

		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		$this->image->render();
	}



	public function __call($method,$arguments){
		throw new Kohana_User_Exception('Invalid url!', "Invalid url!");
	}



}
?>