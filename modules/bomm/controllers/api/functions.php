<?php defined('SYSPATH') or die('No direct script access.');

	function errorHandler($code, $string, $file, $line){
		$ret = array();
		$ret['REQUEST'] = 'ERROR';
		$ret['code'] = $code;
		$ret['string'] = $string;
		$ret['file'] = $file;
		$ret['line'] = $line;

		echo json_encode($ret);
		KOHANA::shutdown();
		die();

	}
	
/**
 */
class Functions_Controller extends Apicontroller_Controller {

	private $ret = array();
	private $db;
	private $dbcore;
	private $km_user;
	private $km_user_code;

	public function __construct(){
		parent::__construct();
		set_error_handler('errorHandler', E_ALL); 
	}
	

	public function getNagykerCeg(){
		$this->start();
		$client_id = 4;
		/*
		 		'Nagykereskedő' => '= 3 ',
				'Gyártó' => '= 4 ',
				'Kiskereskedő' => '= 2 ',
				'Egyéb' => '= 1 ',
				'Nincs cégtípus' => '= 5 '* 
				'EgyébNagyker' => 78

			Kiskereskedelmi relatív sorozat 70
			Nagykereskedelmi relatív sorozat 41
		 * 
		 */
		
		
		
		if(!isset($_POST['email']) || $_POST['email'] == ""){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy email cím.';
			$this->close();
		}		
		$email = $_POST['email'];
		
		$member = ORM::factory('omm_list_member')->where('email',$email)->where('omm_client_id',$client_id)->find();			
		
		if(!$member->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'MISSING';
			$this->ret['MSG'] = 'Nincs ilyen e-mail cím ebben a fiókban.';
	
			$this->close();			
		}		
		
		
		$cegtipus = "";
		
		if($member->hasTag(3)){
			$cegtipus = 'Nagykereskedő';
			
		}elseif($member->hasTag(4)){
			$cegtipus = 'Gyártó';
			
		}elseif($member->hasTag(2)){
			$cegtipus = 'Kiskereskedő';
			
		}elseif($member->hasTag(1)){
			$cegtipus = 'Egyéb';
			
		}elseif($member->hasTag(78)){
			$cegtipus = 'EgyébNagyker';
			
		}elseif($member->hasTag(5)){
			$cegtipus = 'Nincs cégtípus';
			
		}else{
			$cegtipus = 'Nincs cégtípus';
		}
		
		$nagyker_sorozat = 0;
		$kisker_sorozat = 0;
		
		if($member->hasList(70)){
			$kisker_sorozat = 1;
		}

		if($member->hasList(41)){
			$nagyker_sorozat = 1;
		}		
		
		$this->ret['DATA'] = array(
			'CEGTIPUS' => $cegtipus,
			'NAGYKER_SOROZAT' => $nagyker_sorozat,
			'KISKER_SOROZAT' => $kisker_sorozat, 
		
		);
		
		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'FETCHED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok átküldésre kerültek.';

		$this->close();		
		
		
	}
	
	
	
	private function start(){
		

		
		if(!isset($_POST['apiuser']) || !isset($_POST['apikey']) || strlen($_POST['apikey']) != 32){
			$this->ret = array('REQUEST' => 'OK',
	        			 	   'STATUS'  => 'ERROR00',
	        			 	   'MSG'  => 'Érvénytelen API kulcs.'	
	        			 	   );
	        			 	   $this->close();
		}else{
			$this->dbcore = new Database("");
			$apikey = $_POST['apikey'];
			$acc = $this->dbcore->query("SELECT km_user,code FROM omm_accounts WHERE apikey = '".$apikey."' AND km_user = '".$_POST['apiuser']."' ");

			if(sizeof($acc) == 0){

				$this->ret = array('REQUEST' => 'OK',
	        				       'STATUS'  => 'ERROR01',
	        				       'MSG'  => 'Érvénytelen API kulcs.'	
	        				       );

	        				       $this->close();
			}

			$user = $acc[0]->km_user;
			$dbown = Kohana::config('database.own');
			$dbown['connection']['database']="km_".$user;
			Kohana::config_set('database.own',$dbown);

			$this->km_user = $user;
			$this->km_user_code = $acc[0]->code;
			$this->db = new Database("own");
		}
	}

	private function close(){
		echo json_encode($this->ret);
		KOHANA::shutdown();
		die();
	}

		/**
	 *
	 * Enter description here...
	 * @return unknown_type
	 */
	public function getListMemberByCode(){
		$this->start();
			
		if(!isset($_POST['code']) || $_POST['code'] == ""){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos kód.';
			$this->close();
		}
		
		$member = ORM::factory('omm_list_member')->where('code',$_POST['code'])->find();	
		
		$fields = $member->omm_client->getFields();

		$data = array();
		$header = array('mod_date','regdatum','email','statusz');
		$main = array();

		foreach ($fields as $f){
			$header[] = $f->reference;
		}
		$mo = $member->getData($fields, TRUE);

		
			$line = array($mo->mod_date,$mo->regdatum,$mo->email);

			switch ($mo->status){
				case 'active':
					$line[] = 1;
					break;
				case 'unsubscribed':
					$line[] = 2;
					break;
				case 'deleted':
					$line[] = 3;
					break;
				case 'error':
					$line[] = 4;
					break;
				case 'prereg':
					$line[] = 5;
					break;
				default:
					$line[] = 6;
					break;
			}

			foreach ($fields as $f){

				$ref = $f->reference;

				if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){

					$code = $mo->$ref;

					$line[] = $f->getValueFromCode($code);

				}elseif($f->type == "multiselect"){

					$codes = $mo->$ref;

					$codesArray = explode(",",$codes);

					$tocsv = '';
					foreach ($codesArray as $c){
						if($c != ""){
							$tocsv .= $f->getValueFromCode($c).",";
						}
							
					}

					$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
					$line[] = $tocsv;
				}else{
					$line[] = $mo->$ref;
				}



			}
			
			
			
			$main[] = $line;


		$data['header'] = $header;
		$data['members'] = $main;
		$data['mo'] = $mo;
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = base64_encode(json_encode($data));	
		}		
		
		$this->ret['DATA'] = array('MEMBERS' => $data);

		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'FETCHED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok átküldésre kerültek.';

		$this->close();
	}
	
	
	public function moveMember(){
		$this->start();
		
		$listcode = $_POST['listcode_from'];
		$email = $_POST['email'];	
		
		$list_from = ORM::factory("omm_list")->where('code' , $_POST['listcode_from'])->find();
		$member = ORM::factory('omm_list_member')->where('email',$email)->where('omm_list_id',$list_from->id)->find();			
		
		if(!$member->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'MISSING';
			$this->ret['MSG'] = 'Nincs ilyen e-mail cím a listán.';
	
			$this->close();			
			
		}
		
		if(!isset($_POST['listcode_to'])){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista_to kód.';
			$this->close();
		}		
		
		$list_to = ORM::factory("omm_list")->where('code' , $_POST['listcode_to'])->find();				
		$member->omm_list_id = $list_to->id;
		
		$member->delFromList($list_from->id);
		$member->addList($list_to->id,date('Y-m-d H:i:s'));
		
		$member->saveObject();
		
		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'A feliratkozó átmozgatva.';
		$this->ret['MSG'] = '';

		$this->close();		
		
	}	
	

	public function getMember(){
		$this->start();
		$listcode = $_POST['listcode'];
		$email = $_POST['email'];		
		
		$list = ORM::factory("omm_list")->where('code' , $_POST['listcode'])->find();
		$member = ORM::factory('omm_list_member')->where('email',$email)->where('omm_client_id',$list->omm_client_id)->find();			
		
		if(!$member->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'MISSING 1';
			$this->ret['MSG'] = 'Nincs ilyen e-mail cím ebben a fiókban.';
	
			$this->close();			
		}
		
		$lists = $member->getLists($list->id);
		
		if(!isset($_POST['client']) || $_POST['client'] != 'true'){

			if(!isset($lists[0])){
				$this->ret['REQUEST'] = 'OK';
				$this->ret['STATUS'] = 'MISSING 2';
				$this->ret['MSG'] = 'Ez az e-mail cím nem szerepel ezen a listán.';
		
				$this->close();			
			}				
			
		}
	
		if(!isset($_POST['client']) || $_POST['client'] != 'true'){
			$fields = $list->getAllFields();	
		}else{
			$fields = $list->omm_client->getAllAllFields();
		}
		

		$data = array();
		$header = array('mod_date','regdatum','email','statusz');
		$main = array();

		foreach ($fields as $f){
			$header[] = $f->reference;
		}
		$header[] = 'code';
		
		if(isset($_POST['from']) && $_POST['from'] != ""){
			$from = $_POST['from'];
		}else{
			$from = "";
		}			

		if(isset($_POST['to']) && $_POST['to'] != ""){
			$to = $_POST['to'];
		}else{
			$to = "";
		}			
		
		if(!isset($_POST['client']) || $_POST['client'] != 'true'){
			$_members = $list->getMembersForImportByEmail($member->email,$fields);
		}else{
			$_members = $list->omm_client->getMembersForImportByEmail($member->email,$fields);
		}		
		
		
		
		foreach($_members as $m){
			$line = array($m->mod_date,$m->regdatum,$m->email);

			switch ($m->statusz){
				case 'active':
					$line[] = 1;
					break;
				case 'unsubscribed':
					$line[] = 2;
					break;
				case 'deleted':
					$line[] = 3;
					break;
				case 'error':
					$line[] = 4;
					break;
				case 'prereg':
					$line[] = 5;
					break;
				default:
					$line[] = 6;
					break;
			}

			foreach ($fields as $f){

				$ref = $f->reference;

				if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){

					$code = $m->$ref;

					$line[] = $f->getValueFromCode($code);

				}elseif($f->type == "multiselect"){

					$codes = $m->$ref;

					$codesArray = explode(",",$codes);

					$tocsv = '';
					foreach ($codesArray as $c){
						if($c != ""){
							$tocsv .= $f->getValueFromCode($c).",";
						}
							
					}

					$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
					$line[] = $tocsv;
				}else{
					$line[] = $m->$ref;
				}



			}
			$line[] = $m->code;
			$main[] = $line;

		}

		$data['header'] = $header;
		$data['members'] = $main;
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = base64_encode(json_encode($data));	
		}		
		
		$this->ret['DATA'] = array('MEMBERS' => $data);

		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'FETCHED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok átküldésre kerültek.';

		$this->close();
		
		
	}
	
	public function sendLetter(){
		$this->start();//

			
		
		$letter_id = $_POST['letter'];
		$listcode = $_POST['listcode'];
		$email = $_POST['email'];
		
		$list = ORM::factory("omm_list")->where('code' , $_POST['listcode'])->find();
		//$member = ORM::factory('omm_list_member')->where('email',$email)->find();
		$client_id = $list->omm_client->id;	
		$member = ORM::factory('omm_list_member')->where('email',$email)->where('omm_client_id',$client_id)->find();
		
		$job = ORM::factory('omm_job');
		$job->create("dayjob", $letter_id, null, $member->id, null);

				
		
		$dom = new simple_html_dom();
		$job->prepare($dom,$this->km_user,$list->id);
		unset($dom);
		try{
			
			
			$job->execute();		

	
		
		}catch(Exception $e){
			
			//var_dump($e->getTrace());
			
			$this->ret = array('REQUEST' => 'OK',
		    			       'STATUS'  => 'ERROR',
		    				    'MSG'  	  => $e->getMessage()
		           );
	
		    $this->close();			
		    die();
		}
		$this->ret = array('REQUEST' => 'OK',
	    			       'STATUS'  => 'SENT',
	    				   'MSG'  => ''	
	           );

	    $this->close();		
			
	}
	
	public function createLetter(){
		$this->start();

		
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = json_decode(base64_decode($_POST['data']));
			
		}else{
			$data = json_decode($_POST['data']);
			//echo "a";
		}
		
		//$html = file_get_contents($data->linktohtml);
		
		//print_r($_POST);
		//print_r($data);
		
		$l = $data->template_letter;
		
		$letter = ORM::factory('omm_letter')->find($l);
		
		$new = $letter->duplicate();
				
		$new->subject = $data->subject;
		$new->name = $data->subject;
		$new->timing_date = $data->timing_date;
		$new->status = 'active';
		$new->html_content = urldecode($_POST['html']);
		$new->text_content = "";
		
		$new->saveObject();
		
		
		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'LETTER READY';
		$this->ret['MSG'] = '';

		$this->close();
	}

	public function index(){
		$this->start();
			
		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'NOTHING';
		$this->ret['MSG'] = '';

		$this->close();
	}

	private function decode($string){
		
	}
	
	public function importToList(){
		$offset = 0;
		$this->start();
			
		if(!isset($_POST['listcode'])){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		$list = ORM::factory("omm_list")->where('code' , $_POST['listcode'])->find();

		if(!$list->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR2';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		//
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = json_decode(base64_decode($_POST['data']));	
		}else{
			$data = json_decode($_POST['data']);
		}
		

		$header = $data->header;
		$members = $data->members;

		
		unset($data);

		if(!in_array('email',$header)){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR3';
			$this->ret['MSG'] = 'Nincs e-mail a fejlécben.';
			$this->close();
		}

		$i = 0;
		$fields = array();

		foreach($list->fields() as $f){
			$fields[$f->reference] = $f;
		}


		$regdatum_key = -1;
		$email_key = -1;
		$statusz_key = -1;
		$headerreferences = array();
		
		
		foreach($header as $key => $h){

			if($h == "email"){
				$email_key = $key;
			}elseif($h == "regdatum"){
				$regdatum_key = $key;
			}elseif($h == "statusz"){
				$statusz_key = $key;
			}else{
				$headerreferences[$h] = $key;
			}

		}
		////////////////////

		$notuniquemail = array();
		$errors = array();
		$all = 0;
		$imported_active = array();
		$imported_unsubscribed = array();
		$imported_deleted = array();

		$modified_user = array();
		$new_user = array();

		$i = 0;
		foreach($members as $row) {//////////adatok begyüjtése

				if(isset($row[$email_key])){
					$email = $row[$email_key];
				}else{
					$email = "nincs email:$i";
				}

				$all++;

				//$email = trim($email);
				if(valid::email($email) == false){
					if($email == "" || $email == " "){
						$email = "[üres e-mail]";
					}
					$errors[] = $email." (sor: ". ($i+$offset) .")";
					$i++;
					continue;
				}

				$member = ORM::factory('omm_list_member')->where('omm_list_id',$list->id)->where('email',$email)->find();

				if(!$member->loaded){ ///új user létrehozzuk

					$member->omm_list_id = $list->id;
					$member->manual = 2;
					$member->unsubscribe_date = null;

					if($regdatum_key == -1){
						$member->reg_date = date("Y-m-d H:i:s");
					}else{
						$member->reg_date = $row[$regdatum_key];
					}

					if($statusz_key == -1){

						$member->status = 'active';
						$member->activation_date = $member->reg_date;

					}else{

						switch ($row[$statusz_key]){

							case 1:
								$member->status = 'active';
								$member->activation_date = $member->reg_date;
								break;
							case 2:
								$member->status = 'unsubscribed';
								$member->activation_date = $member->reg_date;
								$member->unsubscribe_date = $member->reg_date;
								break;
							case 3:
								$member->status = 'deleted';
								break;
							case 4:
								$member->status = 'error';
								break;
							case 5:
								$member->status = 'prereg';
								$member->activation_date = null;
								break;
						}
					}

					$member->code = string::random_string('unique');
					$member->email = $email;


					$new_user[] = $email;

				}else{////meglévő user updatelünk

					if($regdatum_key != -1){
						$member->reg_date = $row[$regdatum_key];
						$member->activation_date = $member->reg_date;
					}


					if($statusz_key != -1){

						switch ($row[$statusz_key]){

							case 1:{
								$member->status = 'active';

								if($member->activation_date == null && $regdatum_key == -1){
									$member->activation_date = date("Y-m-d H:i:s");
								}

								break;
							}
							case 2:{
								$member->status = 'unsubscribed';

								if($member->unsubscribe_date == null){
									$member->unsubscribe_date = date("Y-m-d H:i:s");
								}

								break;
							}
							case 3:
								$member->status = 'deleted';
								break;
							case 4:
								$member->status = 'error';
								break;
							case 5:
								$member->status = 'prereg';
								break;
						}
					}

					$modified_user[] = $email;
				}

				$member->save();
				$post = new Object();
				foreach ($headerreferences as $key => $h){
					$ff = $fields[$key];
					//echo $key."<br>";
					//echo $ff->type;
					if(isset($row[$h])){
						if($ff->type == "singleselectradio" || $ff->type == "singleselectdropdown" || $ff->type == "multiselect"){
							//$post->$h = $field->getCodeFromValue($row[$key]);
							$array = explode(",",$row[$h]);
							$atad = array();
							foreach ($array as $k => $a){
								$b = trim($a);
								if($b != "") {
									$code = $ff->getCodeFromValue($b);
									if($code != '-'){
										$atad[] = $code;
									}
								}
							}
							$post->$key = $atad;
							unset($array);
						}else{
							$post->$key = string::replaceChars($row[$h]);
						}

					}else{
						$post->$key = "";
					}
					//$post->$key = $this->replaceChars($row[$h]);
					//echo "<br>".$key." = ".$post->$key."<br/>";
				}

				try {
					$member->saveData($post,$list->fields());
					switch ($member->status){
						case 'active':
							$imported_active[] = $email;
							break;
						case 'unsubscribed':
							$imported_unsubscribed[] = $email;
							break;
						case 'deleted':
							$imported_deleted[] = $email;
							break;
						default:
							break;
					}
					unset($member);
					unset($post);
				}catch (Exception $e){
					$errors[] = $email;
				}
			$i++;
		}/////////////adatok vége
			
		$result["now_imported"] = $all;
		$result["succes_active"] = sizeof($imported_active);
		$result["succes_unsubscribed"] = sizeof($imported_unsubscribed);
		$result["succes_deleted"] = sizeof($imported_deleted);
		$result["updated"] = sizeof($modified_user);
		$result["new_count"] = sizeof($new_user);
		$result["errors"] = sizeof($errors);
		
		$result["errors_data"] = $errors;

		$this->ret['DATA'] = array('RESULT' => $result);

		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'IMPORTED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok beimportálva.';

		$this->close();
			
			
	}
	
	public function importToListDebug(){
		$offset = 0;
		$this->start();
			
		if(!isset($_POST['listcode'])){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		$list = ORM::factory("omm_list")->where('code' , $_POST['listcode'])->find();

		if(!$list->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR2';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		//
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = json_decode(base64_decode($_POST['data']));	
		}else{
			$data = json_decode($_POST['data']);
		}
		
		
		$header = $data->header;
		$members = $data->members;

		
		unset($data);

		if(!in_array('email',$header)){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR3';
			$this->ret['MSG'] = 'Nincs e-mail a fejlécben.';
			$this->close();
		}

		$i = 0;
		$fields = array();

		foreach($list->fields() as $f){
			$fields[$f->reference] = $f;
		}


		$regdatum_key = -1;
		$email_key = -1;
		$statusz_key = -1;
		$headerreferences = array();
		

		
		foreach($header as $key => $h){

			if($h == "email"){
				$email_key = $key;
			}elseif($h == "regdatum"){
				$regdatum_key = $key;
			}elseif($h == "statusz"){
				$statusz_key = $key;
			}else{
				$headerreferences[$h] = $key;
			}

		}
		////////////////////

		$notuniquemail = array();
		$errors = array();
		$all = 0;
		$imported_active = array();
		$imported_unsubscribed = array();
		$imported_deleted = array();

		$modified_user = array();
		$new_user = array();

		$i = 0;
		foreach($members as $row) {//////////adatok begyüjtése

				if(isset($row[$email_key])){
					$email = $row[$email_key];
				}else{
					$email = "nincs email:$i";
				}

				$all++;

				//$email = trim($email);
				if(valid::email($email) == false){
					if($email == "" || $email == " "){
						$email = "[üres e-mail]";
					}
					$errors[] = $email." (sor: ". ($i+$offset) .")";
					$i++;
					continue;
				}

				$member = ORM::factory('omm_list_member')->where('omm_list_id',$list->id)->where('email',$email)->find();

				if(!$member->loaded){ ///új user létrehozzuk

					$member->omm_list_id = $list->id;
					$member->manual = 2;
					$member->unsubscribe_date = null;

					if($regdatum_key == -1){
						$member->reg_date = date("Y-m-d H:i:s");
					}else{
						$member->reg_date = $row[$regdatum_key];
					}

					if($statusz_key == -1){

						$member->status = 'active';
						$member->activation_date = $member->reg_date;

					}else{

						switch ($row[$statusz_key]){

							case 1:
								$member->status = 'active';
								$member->activation_date = $member->reg_date;
								break;
							case 2:
								$member->status = 'unsubscribed';
								$member->activation_date = $member->reg_date;
								$member->unsubscribe_date = $member->reg_date;
								break;
							case 3:
								$member->status = 'deleted';
								break;
							case 4:
								$member->status = 'error';
								break;
							case 5:
								$member->status = 'prereg';
								$member->activation_date = null;
								break;
						}
					}

					$member->code = string::random_string('unique');
					$member->email = $email;


					$new_user[] = $email;

				}else{////meglévő user updatelünk



					if($regdatum_key != -1){
						$member->reg_date = $row[$regdatum_key];
						$member->activation_date = $member->reg_date;
					}


					if($statusz_key != -1){

						switch ($row[$statusz_key]){

							case 1:{
								$member->status = 'active';

								if($member->activation_date == null && $regdatum_key == -1){
									$member->activation_date = date("Y-m-d H:i:s");
								}

								break;
							}
							case 2:{
								$member->status = 'unsubscribed';

								if($member->unsubscribe_date == null){
									$member->unsubscribe_date = date("Y-m-d H:i:s");
								}

								break;
							}
							case 3:
								$member->status = 'deleted';
								break;
							case 4:
								$member->status = 'error';
								break;
							case 5:
								$member->status = 'prereg';
								break;
						}
					}else{
						
							
					}

					$modified_user[] = $email;
				}
	
				$member->save();
				

				
				$post = new Object();
				
			
				
				foreach ($headerreferences as $key => $h){
					$ff = $fields[$key];
					//echo $key."<br>";
					//echo $ff->type;
					if(isset($row[$h])){
						if($ff->type == "singleselectradio" || $ff->type == "singleselectdropdown" || $ff->type == "multiselect"){
							//$post->$h = $field->getCodeFromValue($row[$key]);
							$array = explode(",",$row[$h]);
							$atad = array();
							foreach ($array as $k => $a){
								$b = trim($a);
								if($b != "") {
									$code = $ff->getCodeFromValue($b);
									if($code != '-'){
										$atad[] = $code;
									}
								}
							}
							$post->$key = $atad;
							unset($array);
						}else{
							$post->$key = string::replaceChars($row[$h]);
						}

					}else{
						$post->$key = "";
					}
					//$post->$key = $this->replaceChars($row[$h]);
					//echo "<br>".$key." = ".$post->$key."<br/>";
				}



				try {
					$member->saveData($post,$list->fields());
					switch ($member->status){
						case 'active':
							$imported_active[] = $email;
							break;
						case 'unsubscribed':
							$imported_unsubscribed[] = $email;
							break;
						case 'deleted':
							$imported_deleted[] = $email;
							break;
						default:
							break;
					}
					unset($member);
					unset($post);
				}catch (Exception $e){
					$errors[] = $email;
				}
			$i++;
		}/////////////adatok vége
			
		$result["now_imported"] = $all;
		$result["succes_active"] = sizeof($imported_active);
		$result["succes_unsubscribed"] = sizeof($imported_unsubscribed);
		$result["succes_deleted"] = sizeof($imported_deleted);
		$result["updated"] = sizeof($modified_user);
		$result["new_count"] = sizeof($new_user);
		$result["errors"] = sizeof($errors);
		
		$result["errors_data"] = $errors;

		$this->ret['DATA'] = array('RESULT' => $result);

		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'IMPORTED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok beimportálva.';

		$this->close();
			
			
	}	

	/**
	 *
	 * Enter description here...
	 * @return unknown_type
	 */
	public function getListMembers(){
		$this->start();
			
		if(!isset($_POST['listcode'])){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR1';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		$list = ORM::factory("omm_list")->where('code' , $_POST['listcode'])->find();

		if(!$list->loaded){
			$this->ret['REQUEST'] = 'OK';
			$this->ret['STATUS'] = 'ERROR2';
			$this->ret['MSG'] = 'Érvénytelen, vagy hiányos lista kód.';
			$this->close();
		}

		$fields = $list->fields();

		$data = array();
		$header = array('mod_date','regdatum','email','statusz');
		$main = array();

		foreach ($fields as $f){
			$header[] = $f->reference;
		}

		if(isset($_POST['from']) && $_POST['from'] != ""){
			$from = $_POST['from'];
		}else{
			$from = "";
		}			

		if(isset($_POST['to']) && $_POST['to'] != ""){
			$to = $_POST['to'];
		}else{
			$to = "";
		}			
		
		$_members = $list->getMembersForImport($from,$to);
		
		foreach($_members as $m){
			$line = array($m->mod_date,$m->regdatum,$m->email);

			switch ($m->statusz){
				case 'active':
					$line[] = 1;
					break;
				case 'unsubscribed':
					$line[] = 2;
					break;
				case 'deleted':
					$line[] = 3;
					break;
				case 'error':
					$line[] = 4;
					break;
				case 'prereg':
					$line[] = 5;
					break;
				default:
					$line[] = 6;
					break;
			}

			foreach ($fields as $f){

				$ref = $f->reference;

				if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){

					$code = $m->$ref;

					$line[] = $f->getValueFromCode($code);

				}elseif($f->type == "multiselect"){

					$codes = $m->$ref;

					$codesArray = explode(",",$codes);

					$tocsv = '';
					foreach ($codesArray as $c){
						if($c != ""){
							$tocsv .= $f->getValueFromCode($c).",";
						}
							
					}

					$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
					$line[] = $tocsv;
				}else{
					$line[] = $m->$ref;
				}



			}

			$main[] = $line;

		}

		$data['header'] = $header;
		$data['members'] = $main;
		
		if(isset($_POST['base64']) && $_POST['base64'] == "1"){
			$data = base64_encode(json_encode($data));	
		}		
		
		$this->ret['DATA'] = array('MEMBERS' => $data);

		$this->ret['REQUEST'] = 'OK';
		$this->ret['STATUS'] = 'FETCHED';
		$this->ret['MSG'] = 'Minden rendben. Az adatok átküldésre kerültek.';

		$this->close();
	}

}

?>