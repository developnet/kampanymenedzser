<?php defined('SYSPATH') or die('No direct script access.');
/**
 */


if ( ! class_exists('Swift', FALSE)){
	// Load SwiftMailer
	require Kohana::find_file('vendor', 'swift/Swift');

	// Register the Swift ClassLoader as an autoload
	spl_autoload_register(array('Swift_ClassLoader', 'load'));
}

class Cron_Controller extends Clicontroller_Controller {

	var $versionstring = "km_dev_version";

	var $jsonarray = array();

	var $create_limit = 200000;
	var $prepare_limit = 10000;
	var $send_limit = 2;

	public function __construct(){

		$this->create_limit = Kohana::config('core.cron_create_limit');
		$this->prepare_limit = Kohana::config('core.cron_prepare_limit');
		$this->send_limit = Kohana::config('core.cron_send_limit');

		$this->versionstring = Kohana::config('bomm.versionstring');



	}

	public function index(){
		Benchmark::start('cron');
		//$this->jsonarray['create'] = $this->create();
		//$this->jsonarray['prepare'] = $this->prepare();
		//$this->jsonarray['send'] = $this->send();
		Benchmark::stop('cron');
		$this->jsonarray['benchmark'] = Benchmark::get('cron');

	}

	public function searchLetter($day,$hour){
			
		Benchmark::start('cron');



		$db = Kohana::config('database.own');

		$dbcore = new Database();

		$res = $dbcore->from("users")->where('status','1')->groupby('km_user')->get();

		$users = array();
		foreach ($res as $u){
			if($u->km_user != "admin"){
				$users[] = $u->km_user;
			}
		}


		$db = new Database();
		//create

		foreach ($users as $u){

			
			$letters = $db->query("SELECT id FROM ".'km_'.$u.".omm_letters WHERE timing_type='absolute' AND timing_date='".$day."' AND timing_hour='".$hour."' ");
			
			if(sizeof($letters) > 0):
			
				echo $u."\n";
				
				foreach($letters as $l){
					echo $l->id."\n";	
				}
				
			endif;		


		}
		
	}

	public function sendManualDaily($user,$day){


		Benchmark::start('cron');



		$db = Kohana::config('database.own');

		$dbcore = new Database();



		$process_id = jobs::logCron($dbcore,"KM DAILY CRON MANUAL $day $hour $user",getmypid(),"start");



		$u = $user;



		$message = "<h1>[KM DAILY CRON MANUAL] $day $hour<br/></h1>";



		$message .= ''.$u.':'.$this->br;
		$errorusers = array();


		$con = true;
		
		while($con){

			$out = array();
			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailySend/".$u."/".$day."",$out);

			if(sizeof($out)>0){
				$abs = json_decode($out[0]);
				if(isset($abs->absolute) && $abs->absolute->status == 'READY'){

					$con = false;

					////rendszer üzeneteket megcsinálni a kiküldésekről

					$out2 = array();

					exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailySystemMessages/".$u."/".$day."",$out2);

	

	
	
				}else if(!isset($abs->absolute)){
					$con = false;

					$errorusers[] = $u;
	

					$subject = "[KM DAILY MANUAL ERROR] $day $hour";

					$msg = "<h1>'".$u."' SEND ERROR</h1>";

	

					foreach ($out as $o){

						$msg .= "$o<br/>";

					}

	

					$this->sendMail($subject,$msg);

	
				}
			}
		
		}
		
		foreach ($out as $o){
			$message .= "$o<br/>";
		}

		unset($out);
		
		
		

		Benchmark::stop('cron');



		$arr = Benchmark::get('cron');



		foreach ($arr as $key => $o){

			$message .= "$key: $o<br/>";

		}


		$subject = "[KM DAILY CRON] $day $hour";

		$this->sendMail($subject,$message);
		jobs::logCron($dbcore,"KM DAILY CRON MANUAL $day $hour $user",getmypid(),"end", $process_id);
		
	}	
	
	public function sendManualHourly($day,$hour,$user){


		Benchmark::start('cron');



		$db = Kohana::config('database.own');

		$dbcore = new Database();



		$process_id = jobs::logCron($dbcore,"KM HOURLY CRON MANUAL $day $hour $user",getmypid(),"start");



		$u = $user;



		$message = "<h1>[KM HOURLY CRON MANUAL] $day $hour<br/></h1>";



		$message .= ''.$u.':'.$this->br;
		$errorusers = array();


		$con = true;
		
		while($con){

			$out = array();
			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlySend/".$u."/".$day."/".$hour,$out);

			if(sizeof($out)>0){
				$abs = json_decode($out[0]);
				if(isset($abs->absolute) && $abs->absolute->status == 'READY'){

					$con = false;

					////rendszer üzeneteket megcsinálni a kiküldésekről

					$out2 = array();

					exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlySystemMessages/".$u."/".$day."/".$hour,$out2);

	

	
	
				}else if(!isset($abs->absolute)){
					$con = false;

					$errorusers[] = $u;
	

					$subject = "[KM HOURLY MANUAL ERROR] $day $hour";

					$msg = "<h1>'".$u."' SEND ERROR</h1>";

	

					foreach ($out as $o){

						$msg .= "$o<br/>";

					}

	

					$this->sendMail($subject,$msg);

	
				}
			}
		
		}
		
		foreach ($out as $o){
			$message .= "$o<br/>";
		}

		unset($out);
		
		
		

		Benchmark::stop('cron');



		$arr = Benchmark::get('cron');



		foreach ($arr as $key => $o){

			$message .= "$key: $o<br/>";

		}


		$subject = "[KM HOURLY CRON] $day $hour";

		$this->sendMail($subject,$message);
		jobs::logCron($dbcore,"KM HOURLY CRON MANUAL $day $hour $user",getmypid(),"end", $process_id);
		
	}
	
	
	
	public function createtest($user){
		$domain = array(1 => '@aktivhonlapok.dyndns.biz', 2 => '@aktivhonlapok.dyndns.info', 3 => '@aktivhonlapok.dyndns.org', 4 => '@aktivhonlapok.dyndns.tv', 5 => '@aktivhonlapok.dyndns.ws');

		$csoport = array(1 => 'EePg4fd7', 2 => 'q82ikgWE', 3 => 'Hhe7hC7l');

		//$users = array('teszt1','teszt2','teszt3');

		$users = array($user);

		$dbown = Kohana::config('database.own');
		$sd = Kohana::config('core.site_domain');

		foreach($users as $u){

			$dbown['connection']['database']="km_".$u;
			Kohana::config_set('database.own',$dbown);
			//Kohana::config_set('core.site_domain',$u.$sd);

			for($i=1;$i<=1000;$i++){

				$member = ORM::factory('omm_list_member');

				$m = date('m');
				$day = rand(12,16);
				$hour = rand(10,23);
				$min = rand(10,59);
				$sec = rand(10,59);

				$d = rand(1,5);

				$d_hibas = rand(1,10);

				if($d_hibas == 6){
					$domainnev = "@visszafogpattannierrol.hu";
				}else{
					$domainnev = $domain[$d];
				}


				$email = $u."_".$i.$domainnev;

				//2009-10-15 18:46:46

				echo "$email "."2010-$m-$day $hour:$min:$sec"."\n";

				$id = $member->createTestUser($email,1,"2010-$m-$day $hour:$min:$sec");
					
				$csop = rand(1,3);

				$data = ORM::factory("omm_list_member_data")->where("omm_list_field_id",1)->where("omm_list_member_id",$id)->find();

				if(!$data->loaded){
					$data->omm_list_field_id = 1;
					$data->omm_list_member_id = $id;
				}

				$data->value = $csoport[$csop].",";

				$jj = rand (0,2);

				if($jj == 1){
					$csop2 = $csop = rand(1,3);
					$data->value .= $csoport[$csop2].",";
				}


				$data->save();

			}

		}
	}





	public function userHourlyCreate($user,$day,$hour){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsHourly($day,$hour);

		$cre = $dayjob->createJobs($this->create_limit);

		echo json_encode($cre);
		//print_r($cre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userHourlyPrepare($user,$day,$hour){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsHourly($day,$hour);

		$pre = $dayjob->prepareJobs($this->prepare_limit,$user);
			
		echo json_encode($pre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userHourlySystemMessages($user,$day,$hour){
		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsHourly($day,$hour);

		$dayjob->createSystemMessages($user);
	}

	public function userDailySystemMessages($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");


		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsDaily($day);

		$dayjob->createSystemMessages($user);

	}

	public function userDailyRelativeSystemMessages($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");


		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createRelDaily($day);

		$dayjob->createSystemMessages($user);

	}

	public function userHourlySend($user,$day,$hour){
		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");


		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsHourly($day,$hour);

		$send = $dayjob->sendJobs($this->send_limit,$this->getKmUserCode($user));
		echo json_encode($send);
		//print_r($send);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function hourly(){

		Benchmark::start('cron');

		$db = Kohana::config('database.own');
		$dbcore = new Database();

		$day = date('Y-m-d');
		$hour = date('H');

		$process_id = jobs::logCron($dbcore,"KM HOURLY CRON $day $hour",getmypid(),"start");



		$message = "<h1>[KM HOURLY CRON] $day $hour<br/></h1>";


		$res = $dbcore->from("omm_accounts")->where('status','1')->get();
		
		$users = array();
		foreach ($res as $u){
			if($u->km_user != "admin"){
				$users[] = $u->km_user;
			}
		}

		$message .= "<h1>[ABSOLUTE CREATE] $day $hour</h1>";

		//create
		foreach ($users as $u){
			$message .= ''.$u.':'.$this->br;
			$out = array();
			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlyCreate/".$u."/".$day."/".$hour,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}

		$message .= "<h1>[ABSOLUTE PREPARE] $day $hour</h1>";

		//prepare
		foreach ($users as $u){
			$message .= ''.$u.':'.$this->br;

			$continue = true;
			while ($continue) {
				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlyPrepare/".$u."/".$day."/".$hour,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					if(isset($abs->absolute) && $abs->absolute->status == 'READY'){
						$continue = false;
					}else if(!isset($abs->absolute)){
						$continue = false;

						$subject = "[KM HOURLY ERROR] $day $hour";

						$msg = "<h1>'".$u."' PREPARE ERROR</h1>";

						foreach ($out as $o){
							$msg .= "$o<br/>";
						}
						$this->sendMail($subject,$msg);

					}
				}
					
				foreach ($out as $o){
					$message .= "$o<br/>";
				}
				unset($out);
			}
		}

		$message .= "<h1>[ABSOLUTE SEND] $day $hour</h1>";

		$sendusers = $users;
		$errorusers = array();
		while (sizeof($sendusers) > 0) {///addig csináljuk amíg el nem fogynak a userek

			foreach ($sendusers as $key => $u){ /////sending
				$message .= ''.$u.':'.$this->br;


				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlySend/".$u."/".$day."/".$hour,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					if(isset($abs->absolute) && $abs->absolute->status == 'READY'){

						////rendszer üzeneteket megcsinálni a kiküldésekről
						$out2 = array();
						exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userHourlySystemMessages/".$u."/".$day."/".$hour,$out2);


						unset($sendusers[$key]);
					}else if(!isset($abs->absolute)){

						$errorusers[] = $u;
						unset($sendusers[$key]);

						$subject = "[KM HOURLY ERROR] $day $hour";

						$msg = "<h1>'".$u."' SEND ERROR</h1>";

						foreach ($out as $o){
							$msg .= "$o<br/>";
						}

						$this->sendMail($subject,$msg);

					}
				}

				foreach ($out as $o){
					$message .= "$o<br/>";
				}

				unset($out);
					
			}
			//////absolute
		}




		if(sizeof($errorusers)>0){
			$message .= "<h1>ERROR SENDING ABSOLUTE:</h1>";
			foreach ($errorusers as $u){
				$message .= "$u<br/>";
			}
		}





		Benchmark::stop('cron');

		$arr = Benchmark::get('cron');

		foreach ($arr as $key => $o){
			$message .= "$key: $o<br/>";
		}


		$subject = "[KM HOURLY CRON] $day $hour";
			
		$this->sendMail($subject,$message);

		jobs::logCron($dbcore,"KM HOURLY CRON $day $hour",getmypid(),"end", $process_id);

	}

	public function userDailyCreate($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsDaily($day);

		$cre = $dayjob->createJobs($this->create_limit);

		echo json_encode($cre);
		//print_r($cre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userDailyPrepare($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);


		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsDaily($day);

		$pre = $dayjob->prepareJobs($this->prepare_limit,$user);
			
		echo json_encode($pre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userDailySend($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");


		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createAbsDaily($day);

		$send = $dayjob->sendJobs($this->send_limit,$this->getKmUserCode($user));

		echo json_encode($send);
		//print_r($send);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));

	}

	private function getKmUserCode($km_user){

		$dbcore = new Database("");
		$acc = $dbcore->query("SELECT code FROM omm_accounts WHERE km_user = '".$km_user."' ");

		if(sizeof($acc) > 0){
			$km_user_code = $acc[0]->code;
		}else{
			$km_user_code = "xx";
		}
		unset($dbcore);
		return $km_user_code;
	}

	private function getKmUserFromCode($code){

		$dbcore = new Database("");
		$acc = $dbcore->query("SELECT km_user FROM omm_accounts WHERE code = '".$code."' ");

		if(sizeof($acc) > 0){
			$km_user = $acc[0]->km_user;
		}else{
			$km_user = "undefined";
		}
		unset($dbcore);
		return $km_user;
	}

	public function userDailyCreateRelative($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createRelDaily($day);

		$cre = $dayjob->createJobs($this->create_limit);

		echo json_encode($cre);
		//print_r($cre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userDailyPrepareRelative($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createRelDaily($day);

		$pre = $dayjob->prepareJobs($this->prepare_limit,$user);
			
		echo json_encode($pre);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));
	}

	public function userDailySendRelative($user,$day){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);
		//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");


		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->createRelDaily($day);

		$send = $dayjob->sendJobs($this->send_limit,$this->getKmUserCode($user));

		echo json_encode($send);
		//print_r($send);
			
		unset($dayjob);

		sleep(Kohana::config('core.waitafteruseroperation'));

	}

	public function daily(){

		Benchmark::start('cron');

		$dbcore = new Database();


		$day = date('Y-m-d');
		$hour = date('H');

		$process_id = jobs::logCron($dbcore,"KM DAILY CRON $day",getmypid(),"start");

		$db = Kohana::config('database.own');


		$message = "<h1>[KM DAILY CRON] $day<br/></h1>";


		$res = $dbcore->from("omm_accounts")->where('status','1')->get();

		$users = array();
		foreach ($res as $u){
			if($u->km_user != "admin"){
				$users[] = $u->km_user;
			}
		}

		$message .= "<h1>[ABSOLUTE CREATE] $day</h1>";
		//////absolute
		////create
		foreach ($users as $u){
			$message .= ''.$u.':'.$this->br;
			$out = array();

			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailyCreate/".$u."/".$day,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}

		$message .= "<h1>[ABSOLUTE PREPARE] $day</h1>";

		foreach ($users as $u){ ///prepare
			$message .= ''.$u.':'.$this->br;

			$continue = true;
			while ($continue) {
				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailyPrepare/".$u."/".$day,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					if(isset($abs->absolute) && $abs->absolute->status == 'READY'){
						$continue = false;
					}else if(!isset($abs->absolute)){
						$continue = false;
					}
				}

				foreach ($out as $o){
					$message .= "$o<br/>";
				}
				unset($out);
			}
		}

		$message .= "<h1>[ABSOLUTE SEND] $day</h1>";

		$sendusers = $users;
		$errorusers = array();
		while (sizeof($sendusers) > 0) {///addig csináljuk amíg el nem fogynak a userek

			foreach ($sendusers as $key => $u){ /////sending
				$message .= ''.$u.':'.$this->br;


				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailySend/".$u."/".$day,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					if(isset($abs->absolute) && $abs->absolute->status == 'READY'){

						/////system messages
						$out2 = array();
						exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailySystemMessages/".$u."/".$day,$out2);



						unset($sendusers[$key]);
					}else if(!isset($abs->absolute)){
						$errorusers[] = $u;
						unset($sendusers[$key]);
					}
				}

				foreach ($out as $o){
					$message .= "$o<br/>";
				}

				unset($out);
					
			}
			//////absolute
		}

		if(sizeof($errorusers)>0){
			$message .= "<h1>ERROR SENDING ABSOLUTE:</h1>";
			foreach ($errorusers as $u){
				$message .= "$u<br/>";
			}
		}


		$message .= "<h1>[RELATIVE CREATE] $day</h1>";

		///////relative
		foreach ($users as $u){////create

			$message .= ''.$u.':'.$this->br;
			$out = array();
			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailyCreateRelative/".$u."/".$day,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}

		$message .= "<h1>[RELATIVE PREPARE] $day</h1>";

		foreach ($users as $u){ ///prepare
			$message .= ''.$u.':'.$this->br;

			$continue = true;
			while ($continue) {
				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailyPrepareRelative/".$u."/".$day,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					
					if(isset($abs->relative) && isset($abs->relative->status)){
						if($abs->relative->status == 'READY'){
							$continue = false;
						}						
						
					}else{
						$continue = false;
						$subject = "[KM DAILY ERROR] $day $hour";
						$msg = "<h1>'".$u."' PREPARE ERROR</h1>";
						foreach ($out as $o){
							$msg .= "$o<br/>";
						}

						$this->sendMail($subject,$msg);						
					}					
					
				}
				foreach ($out as $o){
					$message .= "$o<br/>";
				}
				unset($out);
			}
		}


		$message .= "<h1>[RELATIVE SEND] $day</h1>";

		foreach ($users as $u){ /////sending
			$message .= ''.$u.':'.$this->br;

			$continue = true;
			while ($continue) {
				$out = array();
				exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailySendRelative/".$u."/".$day,$out);

				if(sizeof($out)>0){
					$abs = json_decode($out[0]);
					
					if(isset($abs->relative) && isset($abs->relative->status)){
						if($abs->relative->status == 'READY'){
							//////system messages
							$out2 = array();
							exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userDailyRelativeSystemMessages/".$u."/".$day,$out2);
	
	
							$continue = false;
						}						
						
					}else{
						$continue = false;
						$subject = "[KM DAILY ERROR] $day $hour";
						$msg = "<h1>'".$u."' SEND ERROR</h1>";
						foreach ($out as $o){
							$msg .= "$o<br/>";
						}

						$this->sendMail($subject,$msg);						
					}						
					

				}
				foreach ($out as $o){
					$message .= "$o<br/>";
				}

				unset($out);
			}
		}
		///////relative


		Benchmark::stop('cron');
		$arr = Benchmark::get('cron');

		foreach ($arr as $key => $o){
			$message .= "$key: $o<br/>";
		}

		$from = 'noreply@kmsrv2.hu';
		$subject = "[KM DAILY CRON] $day";
		$to = array('support@aktivhonlapok.hu');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");

		jobs::sendEmail($from,$subject,$message,$to);

		jobs::logCron($dbcore,"KM DAILY CRON $day",getmypid(),"end",$process_id);

	}

	private function create(){
		$breaklimit = KOHANA::config('core.job_create_break_limit');

		Benchmark::start('absolute');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('absolute');
		$abs = $dayjob->createJobs($breaklimit);
		Benchmark::stop('absolute');
		$abs['absolute']['benchmark'] = Benchmark::get('absolute');

		$breaklimit = $breaklimit - $abs['absolute']['now_created'];

		Benchmark::start('relative');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('relative');
		$rel = $dayjob->createJobs($breaklimit);
		Benchmark::stop('relative');
		$rel['relative']['benchmark'] = Benchmark::get('relative');

		$arr = array();
		$arr['absolute'] = $abs['absolute'];
		$arr['relative'] = $rel['relative'];
		return $arr;
	}

	//@deprecated
	private function prepare(){
		$breaklimit = KOHANA::config('core.job_prepare_break_limit');

		Benchmark::start('absolute');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('absolute');
		$abs = $dayjob->prepareJobs($breaklimit);
		Benchmark::stop('absolute');
		$abs['absolute']['benchmark'] = Benchmark::get('absolute');

		$breaklimit = $breaklimit - $abs['absolute']['now_prepared'];

		Benchmark::start('relative');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('relative');
		$rel = $dayjob->prepareJobs($breaklimit);
		Benchmark::stop('relative');
		$rel['relative']['benchmark'] = Benchmark::get('relative');

		$arr = array();
		$arr['absolute'] = $abs['absolute'];
		$arr['relative'] = $rel['relative'];
		return $arr;
	}

	//@deprecated
	private function send(){
		$breaklimit = KOHANA::config('core.job_send_break_limit');

		Benchmark::start('absolute');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('absolute');
		$abs = $dayjob->sendJobs($breaklimit);
		Benchmark::stop('absolute');
		$abs['absolute']['benchmark'] = Benchmark::get('absolute');

		$breaklimit = $breaklimit - $abs['absolute']['now_sent'];

		Benchmark::start('relative');
		$dayjob = ORM::factory('omm_day_job');
		$dayjob = $dayjob->create('relative');
		$rel = $dayjob->sendJobs($breaklimit);
		Benchmark::stop('relative');
		$rel['relative']['benchmark'] = Benchmark::get('relative');

		$arr = array();
		$arr['absolute'] = $abs['absolute'];
		$arr['relative'] = $rel['relative'];
		return $arr;
	}

	public function userBounceFetch($user){

		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);

		//$sd = Kohana::config('core.site_domain');
		//Kohana::config_set('core.site_domain',$user.$sd);

		$dbcore = new Database();


		$bounces = $dbcore->from('omm_bounces')->where('km_user',$user)->where('status','new')->get();


		if(sizeof($bounces) >0){
			$dbown = new Database("own");
		}
		$i = 0;
		$resend_jobs_html = array();
		$resend_jobs_text = array();

		foreach ($bounces as $b){
			if($b->type == 'resend') continue;
			
			//$jobs = $dbown->from('omm_jobs')->where('message_id',$b->message_id)->get();

			$jobs = $dbown->from('omm_jobs')->where('id',$b->omm_job_id)->get();
			
			if(sizeof($jobs)>0){
				$job = $jobs[0];

				$perm_error_limit = Kohana::config('core.perm_error_limit');
				$perm_error_limit_timeout = Kohana::config('core.perm_error_limit_timeout');
				$temp_error_limit = Kohana::config('core.temp_error_limit');
				$temp_error_limit_timeout = Kohana::config('core.temp_error_limit_timeout');

				$today = date("Y-m-d H:i:s");

				if($b->type == 'perm'){////ha végleges megnézzük, hogy az adott usernek volt-e már végleges hibája
					$date = date( "Y-m-d H:i:s", strtotime( $today." -".$perm_error_limit_timeout." month" ));

					$perms = $dbown->from('omm_letter_bounces')->where('omm_list_member_id',$job->omm_list_member_id)->where('type','perm')->where('bounced >=', $date)->count_records();

					if($perms >= $perm_error_limit){
						$dbown->update('omm_list_members', array('status' => 'error'), array('id' => $job->omm_list_member_id));
					}

					$dbown->insert('omm_letter_bounces', array(
										'omm_letter_id' => $job->omm_letter_id, 
										'omm_list_member_id' => $job->omm_list_member_id,
										'type' => $b->type,
										'dict_id' => $b->dict_id
					));

				}elseif($b->type == 'temp'){////ha átmeneti megnézzük, hogy az adott usernek volt-e már átmeneti hibája a timeout alatt
					$date = date( "Y-m-d H:i:s", strtotime( $today." -".$temp_error_limit_timeout." month" ));

					$temps = $dbown->from('omm_letter_bounces')->where('omm_list_member_id',$job->omm_list_member_id)->where('type','temp')->where('bounced >=', $date)->count_records();

					if($temps >= $temp_error_limit){
						$dbown->update('omm_list_members', array('status' => 'error'), array('id' => $job->omm_list_member_id));
					}

					$dbown->insert('omm_letter_bounces', array(
										'omm_letter_id' => $job->omm_letter_id, 
										'omm_list_member_id' => $job->omm_list_member_id,
										'type' => $b->type,
										'dict_id' => $b->dict_id
					));

				}elseif($b->type == 'resend'){////ha újraküldendő
					
					
					
					//volt-e már erre a job-id-re
					$res = $dbcore->from('omm_bounces')->where('km_user',$user)->where('status','fetched')->where('type','resend')->where('omm_job_id',$job->id)->count_records();

					if($res >= Kohana::config('core.resend_email_after_bounce')){///ez már volt x-szer újraküldve

						$dbown->insert('omm_letter_bounces', array(
											'omm_letter_id' => $job->omm_letter_id, 
											'omm_list_member_id' => $job->omm_list_member_id,
											'type' => 'temp',
											'dict_id' => $b->dict_id
						));

					}else{////még nem volt újraküldve
							
						if($job->email_type == 'html'){
							$resend_jobs_html[] = $job;
						}else{
							$resend_jobs_text[] = $job;
						}


					}


				}
				$dbcore->update('omm_bounces', array('status' => 'fetched', 'omm_job_id' => $job->id), array('id' => $b->id));
			}////jobs


			$i++;
		}///bounces


		if(sizeof($resend_jobs_html) > 0 || sizeof($resend_jobs_text) >0){////újraküldés
			$waitafter = KOHANA::config("core.waitaftersend");
			Kohana::config_set('core.waitaftersend',KOHANA::config("core.waitaftersend_atresend"));

			//Kohana::config_set('core.mail_smtp_username',$user."@kampanymenedzser.hu");
			//Kohana::config_set('core.mail_return_path',$user."_bounceaddress@kampanymenedzser.hu");

			$swift = jobs::getSwift(true);

			$j = 0;

			if(sizeof($resend_jobs_html) > 0){
				$count = jobs::sendJobs($resend_jobs_html,'html',$swift, $dbown, false);
				$j = $j + $count;
			}

			if(sizeof($resend_jobs_text) > 0){
				$count = jobs::sendJobs($resend_jobs_text,'text',$swift, $dbown, false);
				$j = $j + $count;
			}


			unset($switft);
			Kohana::config_set('core.waitaftersend',$waitafter);
			echo "<br/>".$user.": ".$j." resends";
		}


		echo "<br/>".$user.": ".$i." bounces";
	}

	public function bounceDetect(){
		$day = date('Y-m-d');
		$hour = date('H:i');

		$message = "<h1>[KM BOUNCEDETECT CRON] $day $hour<br/></h1>";

		/////////////// bejövő mailok feldolgozása, file szinten
		$dir = "/home/mailgyujto/Maildir/cur";
		$dir2 = "/home/mailgyujto/Maildir/new";

		$copy = "/var/kampanymenedzser/bouncemails";
		$copy_error = "/var/kampanymenedzser/bouncemails/error";
		$copy_ismeretlen = "/var/kampanymenedzser/bouncemails/ismeretlen";
		$copy_autoreply = "/var/kampanymenedzser/bouncemails/autoreply";

		$db = new Database();

		$process_id = jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"start");

		$new = fileutils::dir_list($dir2, true);

		foreach ($new as $n){

			if (!copy($dir2."/".$n['name'], $dir."/".$n['name'])) {
				echo "failed to copy ..".$dir2."/".$n['name'].$this->br;
			}else{
				unlink($dir2."/".$n['name']);
			}

		}


		$cur = fileutils::dir_list($dir, true);


		foreach ($cur as $c){/////fájlokon végigmegyünk

			$r = $this->readErrorHeaderFromMail($dir."/".$c['name'],$db);

			if(isset($r['message_id']) && $r['message_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){////ha vannak adatok

				if($r['km_user'] == "autoreply"){////ha autoreply akkor elvetjük és bemásoljuk az autoreplykhez
					copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
				}else{////visszapattano insert
					$status = $db->insert('omm_bounces', array('message_id' => $r['message_id'], 'type' => $r['type'], 'dict_id' => $r['dict_id'], 'km_user' => $r['km_user']));

					if($r['dict_id'] == 0){ ////szótár nem ismerü külön másoljuk, hogy lehessen okosítani
						copy($dir."/".$c['name'], $copy_ismeretlen."/".$c['name']);
					}else{
						copy($dir."/".$c['name'], $copy."/".$c['name']);
					}

				}

			}else{////ha nincsen adat megpróbáljuk a másikkal parsolni

				$r = $this->readErrorHeaderFromMail2($dir."/".$c['name'],$db);

				if(isset($r['message_id']) && $r['message_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){///ha van adat

					if($r['km_user'] == "autoreply"){////ha autoreply akkor csak elmásoljuk
						copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
					}else{////visszapattanó insert
						$status = $db->insert('omm_bounces', array('message_id' => $r['message_id'], 'type' => $r['type'], 'dict_id' => $r['dict_id'], 'km_user' => $r['km_user']));
							
						if($r['dict_id'] == 0){////nem ismeri a szótár
							copy($dir."/".$c['name'], $copy_ismeretlen."/".$c['name']);
						}else{
							copy($dir."/".$c['name'], $copy."/".$c['name']);
						}
					}
				}else{ ////nem tudtuk parsolni

					if(isset($r['km_user']) && $r['km_user'] == "autoreply"){
						copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
					}else{
						$message .= "error:".$c['name'].$this->br."<br/>";
						copy($dir."/".$c['name'], $copy_error."/".$c['name']);
					}
				}
				//unlink($dir."/".$c['name']);
			}
			unlink($dir."/".$c['name']); ////töröljük az inboxból
		}


		//////////////
		/////////////userek megnézik van-e nekik új bounce - db


		$res = $db->from("omm_accounts")->where('status','1')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			//$message .= ''.$u->km_user.':'.$this->br;

			$out = array();

			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userBounceFetch/".$u->km_user,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}




		$from = 'noreply@kmsrv1.hu';
		$subject = "[KM BOUNCEDETECT CRON] $day $hour";
		$to = array('support@aktivhonlapok.hu');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");

		jobs::sendEmail($from,$subject,$message,$to);

		jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"end",$process_id);
	}
	
	public function advancedBounceProcess($file,$db){
		
		require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."rfc822_addresses.php";
		require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."mime_parser.php";
		
		
		$mime=new mime_parser_class;
	
		
		/*
		 * Set to 0 for parsing a single message file
		 * Set to 1 for parsing multiple messages in a single file in the mbox format
		 */
		$mime->mbox = 0;
		$mime->decode_bodies = 1;
		$mime->ignore_syntax_errors = 1;
		$mime->track_lines = 1;		
		
		
		//$new = fileutils::dir_list($copy_ismeretlen, true);
		//$new = fileutils::dir_list($copy_stat, true);
		
		
		$return = array();
		$ossz = 0;
		$i = 0;
		$j = 0;
		$nn = 0;
		$x = 0;
		$y = 0;
		$dicts = array();
		$dictsid = array();
			
			$x++;
			
			//if($x == 1000) break;
			
			$decoded = array();//
			$mime->Decode(array('File'=>$file), $decoded); //,'SkipBody'=>0
			
			//print_r($decoded);
			
			if(isset($decoded[0]['Headers']['x-original-to:'])){
				$i++;
				$string = trim($decoded[0]['Headers']['x-original-to:']);

				if(strstr($string,"@kmsrv1.hu") != false && strstr($string,"_") != false){

					$dd = explode("_", $string);
			
					$return['km_user'] = $this->getKmUserFromCode(trim($dd[0]));
					$return['job_id'] = trim($dd[1]);					

					
				}else{///azonosítatlan
					unset($mime);
					return $return;
				}
				
				
				if(isset($decoded[0]['Parts'][0]['Body'])){
					$type = array();
					$type = $this->getTypeFromHeader($decoded[0]['Parts'][0]['Body'],$db);
					
					if($type['dict_id'] == 0){ ///nincs még meg
						
						$type = array();
						if(isset($decoded[0]['Parts'][1]['Body'])){
							
							$type = $this->getTypeFromHeader($decoded[0]['Parts'][1]['Body'],$db);
							
							if($type['dict_id'] == 0){/////másodikból sem ismert, megy vissza ismeretlennel
								$j++;
								
								foreach($type as $key => $t){
									$return[$key] = $t;
								}
								
								//print_r($type);
								
								//break;								
							}else{ ///minden megvan, nem ismeretlen, a második partból lett meg

								foreach($type as $key => $t){
									$return[$key] = $t;
								}
								/*
								if(!isset($dicts[$type['dict_reason']." ".$type['type']])){
									$dicts[$type['dict_reason']." ".$type['type']] = 1;
								}else{
									$dicts[$type['dict_reason']." ".$type['type']] = $dicts[$type['dict_reason']." ".$type['type']] +1;
								}

								if(!isset($dictsid[$type['dict_id']])){
									$dictsid[$type['dict_id']] = 1;
								}else{
									$dictsid[$type['dict_id']] = $dictsid[$type['dict_id']] +1;
								}								
								
								/*
								if(copy($copy_stat."/".$n['name'], $copy_stat."/megvolt/".$n['name'])){
									unlink($copy_stat."/".$n['name']);
								}	*/							
								
							}
							
						}

					}else{ ///minden megvan, nem ismeretlen, már első partból megvan
						
						foreach($type as $key => $t){
							$return[$key] = $t;
						}
						
						/*
						if(!isset($dicts[$type['dict_reason']." ".$type['type']])){
							$dicts[$type['dict_reason']." ".$type['type']] = 1;
						}else{
							$dicts[$type['dict_reason']." ".$type['type']] = $dicts[$type['dict_reason']." ".$type['type']] +1;
						}
						
						if(!isset($dictsid[$type['dict_id']])){
							$dictsid[$type['dict_id']] = 1;
						}else{
							$dictsid[$type['dict_id']] = $dictsid[$type['dict_id']] +1;
						}						
						
						if(copy($copy_stat."/".$n['name'], $copy_stat."/megvolt/".$n['name'])){
							unlink($copy_stat."/".$n['name']);
						}*/
						
					}	
					
					
				}else{/////ismeretlen visszapattanó
					$nn++;
					$return['dict_id'] = 0;
				}
				
				
			}else{////azonosítatlan visszapattanó
				$y++;
			}
		
		//print $y."/".$nn."/".$j."/".$i."/".($x)."\n";
		//print_r($dicts);
		//print_r($dictsid);
		//print_r($decoded);
		
		
		
		//$dd = explode("_", $data);
		
		//$this->getKmUserFromCode(trim($dd[0]));
		
		unset($mime);
		return $return;
	}
	
	
	public function advancedBounceProcess_test(){
		
		$dir = "/var/kampanymenedzser/bouncemails/statisztika";
		$cur = fileutils::dir_list($dir, true);

		$db = new Database();
		
		$ismeretlen = 0;
		$ismert = 0;
		$error = 0;
		$ossz = sizeof($cur);
				
		foreach ($cur as $c){/////fájlokon végigmegyünk

			$r = $this->advancedBounceProcess($dir."/".$c['name'],$db);

			if(isset($r['job_id']) && $r['job_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){
				
				if($r['dict_id'] == 0){
					$ismeretlen++;
				}else{
					$ismert++;
				}
				
				
			}else{
				$error++;
			}			
			
		}
		
		echo $ismeretlen."/".$ismert."/".$error."/".$ossz;
		
	}
	
		
	public function newBounceDetect(){
		$day = date('Y-m-d');
		$hour = date('H:i');

		$message = "<h1>[KM BOUNCEDETECT CRON] $day $hour<br/></h1>";

		/////////////// bejövő mailok feldolgozása, file szinten
		$dir = "/var/www/kampanymenedzser/bounce/new";
		$dir2 = "/var/www/kampanymenedzser/bounce/new2";

		$copy = "/var/www/kampanymenedzser/bounce/siker";
		$copy_error = "/var/www/kampanymenedzser/bounce/error";
		$copy_ismeretlen = "/var/www/kampanymenedzser/bounce/unknown";//
		$copy_autoreply = "/var/www/kampanymenedzser/bounce/autoreply";

		$db = new Database();

		$process_id = jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"start");

		$new = fileutils::dir_list($dir2, true);

		foreach ($new as $n){

			if (!copy($dir2."/".$n['name'], $dir."/".$n['name'])) {
				echo "failed to copy ..".$dir2."/".$n['name'].$this->br;
			}else{
				unlink($dir2."/".$n['name']);
			}

		}


		$cur = fileutils::dir_list($dir, true);


		foreach ($cur as $c){/////fájlokon végigmegyünk

			$r = $this->advancedBounceProcess($dir."/".$c['name'],$db);

			if(isset($r['job_id']) && $r['job_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){////ha vannak adatok

				if($r['km_user'] == "autoreply"){////ha autoreply akkor elvetjük és bemásoljuk az autoreplykhez
					copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
				}else{////visszapattano insert
					
					if(!isset($r['type'])) $r['type'] = 'temp';
					
					$status = $db->insert('omm_bounces', array('omm_job_id' => $r['job_id'], 'type' => $r['type'], 'dict_id' => $r['dict_id'], 'km_user' => $r['km_user']));

					if($r['dict_id'] == 0){ ////szótár nem ismerü külön másoljuk, hogy lehessen okosítani
						copy($dir."/".$c['name'], $copy_ismeretlen."/".$c['name']);
					}else{
						copy($dir."/".$c['name'], $copy."/".$c['name']);
					}

				}

			}else{ ////nem tudtuk parsolni

				if(isset($r['km_user']) && $r['km_user'] == "autoreply"){
					copy($dir."/".$c['name'], $copy_autoreply."/".$c['name']);
				}else{
					$message .= "error:".$c['name'].$this->br."<br/>";
					copy($dir."/".$c['name'], $copy_error."/".$c['name']);
				}
			}
				
				
			unlink($dir."/".$c['name']); ////töröljük az inboxból
		}


		//////////////
		/////////////userek megnézik van-e nekik új bounce - db


		$res = $db->from("omm_accounts")->where('status','1')->get();

		foreach ($res as $u){
			if($u->km_user == "admin") continue;

			//$message .= ''.$u->km_user.':'.$this->br;

			$out = array();

			exec("php /var/www/kampanymenedzser/".$this->versionstring."/cronapp/index.php api/cron/userBounceFetch/".$u->km_user,$out);

			foreach ($out as $o){
				$message .= "$o<br/>";
			}

			unset($out);
		}




		$from = 'noreply@kmsrv1.hu';
		$subject = "[KM BOUNCEDETECT CRON] $day $hour";
		$to = array('support@aktivhonlapok.hu');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");

		jobs::sendEmail($from,$subject,$message,$to);

		jobs::logCron($db,"KM BOUNCEDETECT CRON $day $hour",getmypid(),"end",$process_id);
	}


	private function readErrorHeaderFromMail3($file, $db){
		$return = array();

		$split1 = "Content-Description: Delivery report";
		$delivery_report = false;
		$header = "";

		$main = false;
		$handle = @fopen($file, "r");

		if ($handle) {
			while (!feof($handle)) {
				$buffer = fgets($handle);
				if(	strstr($buffer,"X-Original-To")){
					$line = trim($buffer);
						
					$lines = explode(":", $line);
					$returnpath = trim($lines[1]);
						
					$d = explode("@", $returnpath);
					$data = trim($d[0]);
						
					$dd = explode("_", $data);
						
					$return['km_user'] = $this->getKmUserFromCode(trim($dd[0]));
					
					if($return['km_user'] == "undefined" || !isset($dd[1])){
						$return['job_id'] = 0;	
					}else{
						$return['job_id'] = trim($dd[1]);
					}
					
					
					$return['type'] = "";
					$return['dict_id'] = 0;
					$return['message_id'] = "";
						
					unset($line);
					unset($lines);
					unset($d);
					unset($data);
					unset($dd);
						
				}
					
				if( strstr($buffer,"Content-Description: Delivery report")){
					$delivery_report = true;
				}

				if( strstr($buffer,"Content-Description: Undelivered Message")){
					$delivery_report = false;
					
					$foundtype = $this->getTypeFromHeader($header,$db);
					$return['type'] = $foundtype['type'];
					$return['dict_id'] = $foundtype['dict_id'];
					break;					
				}
					

				if($delivery_report){
					$header .= $buffer;
				}


			}
				
			fclose($handle);
			unset($handle);
		}

		return $return;
	}

	/**
	 * A return-path ból megállapítja a km_usert és a message-id-t kiszedi, és a type-ot megállapítja a omm_bounces_dict tábla alapján
	 */
	private function readErrorHeaderFromMail($file,$db){

		$return = array();

		$line = "Return-Path:";
		$message_id = "Message-ID:";
		$split = " ";

		$header = "";

		$main = false;
		$handle = @fopen($file, "r");

		if ($handle) {
			//echo "file: $file,  handle: $handle".$this->br;

			while (!feof($handle)) {
				$buffer = fgets($handle, 4096);

				//Auto-Submitted: auto-replied

				if(	strstr($buffer,"X-Autoreply: yes") || strstr($buffer,"Auto-Submitted: auto-replied") ){
					$return['km_user'] = 'autoreply';
					break;
				}

				//echo $buffer.$this->br;
				if(strstr($buffer,$line) != false){
					$main = true;

					$r = explode($split,$buffer);

					if(sizeof($r == 2)){

						$r = explode("@",$r[1]);

						if(sizeof($r) == 2){
							$r = explode("_",$r[0]);
							if(sizeof($r) == 2){
								$return['km_user'] = string::trim_kmuser($r[0]);
							}else{
								$return['km_user'] = "";
							}
						}else{
							$return['km_user'] = "";
						}
					}


				}else{
					$header .= $buffer;
				}

				if($main){

					if(strstr($buffer,$message_id) != false){

						$r = explode($split,$buffer);

						if(sizeof($r == 2)){
							$return['message_id'] = string::replaceInMessageId($r[1]);
						}else{
							$return['message_id'] = "";
						}
						$foundtype = $this->getTypeFromHeader($header,$db);
						$return['type'] = $foundtype['type'];
						$return['dict_id'] = $foundtype['dict_id'];
						break;
					}

				}

			}
			fclose($handle);
			unset($handle);

		}

		return $return;
	}

	/**
	 * A return-path ból megállapítja a km_usert és a message-id-t kiszedi, és a type-ot megállapítja a omm_bounces_dict tábla alapján
	 *
	 * 2-es varia
	 *
	 */
	private function readErrorHeaderFromMail2($file,$db){

		$return = array();

		$line = "To:";
		$message_id = "Message-ID:";
		$split = " ";

		$header = "";

		$main = false;
		$handle = @fopen($file, "r");

		if ($handle) {
			//echo "file: $file \n";

			while (!feof($handle)) {
				$buffer = fgets($handle, 4096);
				$header .= $buffer;

				if(	strstr($buffer,"X-Autoreply: yes") ||
				strstr($buffer,"Auto-Submitted: auto-replied") ){
					$return['km_user'] = 'autoreply';
					break;
				}

				//echo $buffer.$this->br;
				if(strstr($buffer,"To:") != false && strstr($buffer,"_bounceaddress@kampanymenedzser.hu") != false){

					$r = explode($split,$buffer);

					if(sizeof($r == 2)){

						$r = explode("@",$r[1]);

						if(sizeof($r) == 2){
							$r = explode("_",$r[0]);
							if(sizeof($r) == 2){
								$return['km_user'] = string::trim_kmuser($r[0]);
							}else{
								$return['km_user'] = "";
							}
						}else{
							$return['km_user'] = "";
						}
					}
				}

				if(strstr($buffer,"swift@kampanymenedzser.hu") != false){
					//echo $buffer;
					$r = explode($split,$buffer);

					if(sizeof($r == 2)){
						$return['message_id'] = trim($r[1]);
					}else{
						$return['message_id'] = "";
					}
				}

			}

			$foundtype = $this->getTypeFromHeader($header,$db);
			$return['type'] = $foundtype['type'];
			$return['dict_id'] = $foundtype['dict_id'];


			unset($handle);

		}

		return $return;
	}

	private function getTypeFromHeader($header,$db){
		$return = array();
		$return['type'] = "temp";
		$return['dict_id'] = 0;

		$res = $db->from("omm_bounces_dict")->where('status','1')->orderby('ord')->get();

		foreach($res as $r){

			if(strstr($r->keyphrase,"+")){

				$keys = explode("[+]",$r->keyphrase);
				$van = true;
				foreach($keys as $key){
					if(strstr($header,$key) === false){
						$van = false;
						break;
					}
				}


				if($van){
					$return['type'] = $r->type;
					$return['dict_id'] = $r->id;
					$return['dict_reason'] = $r->reason;
					$return['dict_keyphrase'] = $r->keyphrase;
					return $return;
				}


			}else{
				if(strstr($header,$r->keyphrase) != false){
					$return['type'] = $r->type;
					$return['dict_id'] = $r->id;
					$return['dict_reason'] = $r->reason;
					$return['dict_keyphrase'] = $r->keyphrase;					
					return $return;
				}
			}
		}
		return $return;
	}


	public function testBounceParse(){
		$db = new Database();
		$test = "/var/kampanymenedzser/bouncemails/test";
		$siker = "/var/kampanymenedzser/bouncemails/test/siker";
		$ismeretlen = "/var/kampanymenedzser/bouncemails/test/ismeretlen";
		$autoreply = "/var/kampanymenedzser/bouncemails/test/autoreply";

		$cur = fileutils::dir_list($test, true);
		$i = 0;
		foreach ($cur as $c){

			$r = $this->readErrorHeaderFromMail($test."/".$c['name'],$db);

			if(isset($r['message_id']) && $r['message_id'] !="" && isset($r['km_user']) && $r['km_user'] != "" ){

				if($r['km_user'] == "autoreply"){
					copy($test."/".$c['name'], $autoreply."/".$c['name']);
				}else{
					if($r['dict_id'] == 0){
						copy($test."/".$c['name'], $ismeretlen."/".$c['name']);
					}else{
						copy($test."/".$c['name'], $siker."/".$c['name']);
					}
				}
					
				unlink($test."/".$c['name']);

			}else{

				$r2 = $this->readErrorHeaderFromMail2($test."/".$c['name'],$db);

				if(isset($r2['message_id']) && $r2['message_id'] !="" && isset($r2['km_user']) && $r2['km_user'] != "" ){

					if($r2['km_user'] == "autoreply"){
						copy($test."/".$c['name'], $autoreply."/".$c['name']);
					}else{

						if($r2['dict_id'] == 0){
							copy($test."/".$c['name'], $ismeretlen."/".$c['name']);
						}else{
							copy($test."/".$c['name'], $siker."/".$c['name']);
						}

					}

				}else{

					if($r2['km_user'] == "autoreply"){
						copy($test."/".$c['name'], $autoreply."/".$c['name']);
						unlink($test."/".$c['name']);
					}else{
						echo "$i:"."--2H--\n";
						print_r($r2);
						echo "$i:"."--2H--\n";
					}

				}

			}

		}


	}


	private function sendMail($subject,$msg){

		$from = 'noreply@kmsrv2.hu';
		$to = array('support@aktivhonlapok.hu');

		//Kohana::config_set('core.mail_smtp_username',"visszapattano@kampanymenedzser.hu");
		//Kohana::config_set('core.mail_smtp_password',"piszlicsare");


		jobs::sendEmail($from,$subject,$msg,$to);

	}


	private function checkCrons($km_user,$day,$hour,$cron,$msg){



	}


	private function exportSch(){	

		$user = "schilling";
		
		
		$dbown = Kohana::config('database.own');
		$dbown['connection']['database']="km_".$user;

		Kohana::config_set('database.own',$dbown);		

		
		$letters = ORM::factory("omm_letter")->where('status','draft')->find_all();
		
		foreach($letters as $l){
			//
			$ourFileName = "/var/kampanymenedzser/temp/sch/".$l->id.".html";
			$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
			fclose($ourFileHandle);			
			file_put_contents($ourFileName, $l->html_content);

			$ourFileName = "/var/kampanymenedzser/temp/sch/".$l->id.".txt";
			$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
			fclose($ourFileHandle);			
			file_put_contents($ourFileName, $l->text_content);			
			
		}
		
		
	}
	
}
?>