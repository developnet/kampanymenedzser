<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."vendors".DIRECTORY_SEPARATOR."php-ofc-library".DIRECTORY_SEPARATOR."open-flash-chart.php";

class Charts_Controller extends Javascript_Controller {

	
	
	public function index(){
	}

	public function listActivity(){
		
		$view_file = Kohana::config('admin.theme')."/"."reports/list_report_01_data.php";
		$this->auto_render = FALSE;
		
		$listId = $this->uri->segment(4,"");
		$today = date("Y-m-d");
		$from = $this->uri->segment(5,date( "Y-m-d", strtotime( $today." -1 month" )));
		$to = $this->uri->segment(5,date( "Y-m-d", strtotime( $today." -1 day" )));
		
		
		if($listId == ""){
			//url::redirect("/pages/listoverview");
		}else{
			
			$list = ORM::factory("omm_list")->find($listId);
			
			if($list->loaded){
				
				setlocale(LC_NUMERIC, 'en_US.UTF8');
					
				$data = array();
				$data2 = array();
				$labels = array();

				$db = new Database("own");	
				
				$act = $from;
				$max_sub = 0;
				$max_unsub = 0;
				$i = 1;
				while($act != $to){
					
					$stat = $db->from('omm_stat_lists')->where('day',$act)->where('omm_list_id',$list->id)->get();
					
					//print_r($stat[0]);

					if(sizeof($stat) == 1){

						$data[$i] = (int)$stat[0]->subscribed_today;
						$data2[$i] = (int)$stat[0]->unsubscribed_today;							

						if((int)$stat[0]->subscribed_today > $max_sub){
							$max_sub = (int)$stat[0]->subscribed_today;
						}

						if((int)$stat[0]->unsubscribed_today > $max_unsub){
							$max_unsub = (int)$stat[0]->unsubscribed_today;
						}
						
					}else{
						$data[$i] = 0;
						$data2[$i] = 0;						
					}
					$labels[$i] = substr($act,5);
					
					$act = date( "Y-m-d", strtotime( $act." +1 day" ) );
				}
				
			
        
				$this->template->data = $data;
				$this->template->data2 = $data2;
				$this->template->labels = $labels;
        		
				$this->template->render(TRUE);
					

			}else{
				//url::redirect("/pages/listoverview");	
			}
			
		}		
		
	}	
	
}
?>