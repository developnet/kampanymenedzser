<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    admin
 * @author     mberta
 * @copyright  (c) 2008
 */
abstract class Adminapicontroller_Controller extends Controller {

   	// Template view name


	public function __construct()
	{
		parent::__construct();
		$this->session = Session::instance();
			
		$this->auth = Auth::instance();
		$this->_checkLogin();		
		
	}

	

	private function _checkLogin(){
		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
			url::redirect('login');
		}
	}
	

} // End Template_Controller