<?php
class Apicontroller_Controller extends Controller{
	
	public function __construct(){
		parent::__construct();
		
		if(Kohana::config('admin.debug')){
			//$profiler = new Profiler;	
		}		
		
		Event::add('member.pre_subscription', array('api', 'preSubscription'));
		Event::add('member.post_subscription', array('api', 'postSubscription'));

		Event::add('member.pre_unsubscription', array('api', 'preUnsubscription'));
		Event::add('member.post_unsubscription', array('api', 'postUnsubscription'));
		
		Event::add('member.pre_activate', array('api', 'preActivate'));
		Event::add('member.post_activate', array('api', 'postActivate'));		
		
		Event::add('error.subscribe_email_error', array('apierrors', 'error_subscribe_email'));
		Event::add('error.subscribe_invalid_domain', array('apierrors', 'error_subscribe_invalid_domain'));
		Event::add('error.subscribe_missing_list_code', array('apierrors', 'error_subscribe_missing_list_code'));
		Event::add('error.subscribe_list_error', array('apierrors', 'error_subscribe_missing_list_code'));
		
		Event::add('error.subscribe_second_subscribe', array('apierrors', 'error_subscribe_second_subscribe'));
		Event::add('error.unsubscribe_second_unsubscribe', array('apierrors', 'error_unsubscribe_second_unsubscribe'));
		
		
		Event::add('error.unsubscribe_missingorinvalid_code', array('apierrors', 'error_unsubscribe_missingorinvalid_code'));
		Event::add('error.unsubscribe_db_error', array('apierrors', 'error_unsubscribe_db_error'));

		Event::add('error.activate_missingorinvalid_code', array('apierrors', 'error_unsubscribe_missingorinvalid_code'));
		Event::add('error.activate_db_error', array('apierrors', 'error_unsubscribe_db_error'));		
		
	}


	

	
}

?>