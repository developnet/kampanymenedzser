<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Accounts_Controller extends Ommadminpage_Controller {

	var $_pageview = "accounts";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "admin_view";
		parent::__construct();

		if (!$this->auth->logged_in(Admin_Controller::$superadmin_role)){
			url::redirect('/');
		}

	}

	public function index($package = "nincs")	{

		$this->pageview = $this->_pageview;
		$this->init();


		$db = new Database("");
		
		
		if($package == "nincs"){
			$res = $db->from("omm_accounts")->notin("omm_package_id", array(8,12))->orderby('km_user', 'asc')->get();
		}else{
			$res = $db->from("omm_accounts")->where("omm_package_id",$package)->orderby('km_user', 'asc')->get();
		}
		
		$numbers = array(
			"elofizetok" => $db->notin("omm_package_id", array(8,12))->count_records('omm_accounts'),
			"demosok" => $db->count_records('omm_accounts', array('omm_package_id=' => 8)),
			"tesztesek" => $db->count_records('omm_accounts', array('omm_package_id=' => 12)),
		);

		$packages = array();

		foreach($res as $key => $acc){
			$packages[$acc->km_user] = meta::getEmailCountAndPackageKm($db,$acc->km_user);
		}


		$this->pagetemplate->accounts = $res;
		$this->pagetemplate->numbers = $numbers;
		$this->pagetemplate->packages = $packages;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		$this->render();
	}

	public function detail($acc_id){
		$db = new Database("");

		$res = $db->from("omm_accounts")->where("id",$acc_id)->get();		
		
		if(isset($_POST['note_user'])){
			$note = array(
				"user" => $_POST['note_user'],
				"type" => $_POST['note_type'],
				"note" => $_POST['note_note'],
				"omm_account_id" => $acc_id
			);
			$status = $db->insert('omm_account_notes', $note);
			url::redirect("/admin/accounts/detail/".$acc_id);
		}
		
		
		if(isset($_POST['company'])){
			//$_POST['code'] = string::random_string();
			$status = $db->update('omm_accounts', $_POST, array('id' => $acc_id));
			url::redirect("/admin/accounts/detail/".$acc_id);
		}
		
		if(isset($_POST['user_lastname'])){

		if($_POST['user_username'] == "" || $_POST['user_password'] == ""){
			die("Empty username or password!");
		}

		$user = ORM::factory('user');

		if ( ! $user->username_exists($_POST['user_username'])){
			$user->km_user = $res[0]->km_user;
			$user->username = $_POST['user_username'];
			$user->lastname = $_POST['user_lastname'];
			$user->email = $_POST['user_email'];
			$user->firstname = $_POST['user_firstname'];
			$user->password = $_POST['user_password'];
			
			$user->status = 1;
			$user->datum = date("Y-m-d");

			if ($user->save() AND $user->add(ORM::factory('role', 'login'))){
				
			if(isset($_POST['user_admin']) && $_POST['user_admin'] == '1'){
					$user->add(ORM::factory('role', 'accountadmin'));
				}				
				
				url::redirect("/admin/accounts/detail/".$acc_id."#users");
			}
		}else{
			die("User ".$_POST['user_username']."  already exist!  ");
		}
					
			
			
			
			
		}
				
		
		
		$this->pageview = "accounts_detail";
		$this->init();

		$this->pagetemplate->notes = $db->from("omm_account_notes")->where("omm_account_id",$res[0]->id)->orderby("timestamp","desc")->get();
		$this->pagetemplate->packages = $db->from("omm_packages")->orderby("base_price","asc")->get();
		$this->pagetemplate->account = $res[0];
		
		$this->pagetemplate->users = ORM::factory('user')->where("km_user", $res[0]->km_user)->find_all();
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		
		
		$this->render();
	}

	public function add(){
		$db = new Database("");

		if(isset($_POST['company'])){

			$db->query("CREATE DATABASE km_".$_POST['km_user']." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
			
			$go = "mysql -ukm_core -p:BVFzu.sJSuWn8Bx km_".$_POST['km_user']." < /var/www/kampanymenedzser/application/new_user.sql";
			
			exec($go);
			
			$status = $db->insert('omm_accounts', $_POST);
			
			url::redirect("/admin/accounts");
			
		}
		
		$this->pageview = "accounts_add";
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError
		
		$this->render();
	}	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}