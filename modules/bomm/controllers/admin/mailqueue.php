<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Mailqueue_Controller extends Ommadminpage_Controller {

	var $_pageview = "mailqueue";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "admin_view";
		parent::__construct();

		if (!$this->auth->logged_in(Admin_Controller::$superadmin_role)){
			url::redirect('/');
		}

	}

	public function index()	{

		$this->pageview = $this->_pageview;
		$this->init();

		$out = array();
		exec("qshape",$out);//		
		
		
		$this->pagetemplate->lines = $out;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}