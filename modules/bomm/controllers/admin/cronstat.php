<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Cronstat_Controller extends Ommadminpage_Controller {

	var $_pageview = "cronstat";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "admin_view";
		parent::__construct();

		if (!$this->auth->logged_in(Admin_Controller::$superadmin_role)){
			url::redirect('/');
		}

	}

	public function index()	{

		$this->pageview = $this->_pageview;
		$this->init();

		
		$db = new Database("");
		$res = $db->from("omm_cronjobs")->orderby('start', 'desc')->limit(100)->get();		
		
		$this->pagetemplate->cronjobs = $res;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}