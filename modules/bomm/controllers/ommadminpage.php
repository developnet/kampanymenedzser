<?php
class Ommadminpage_Controller extends Controller{

	var $pageview = "clients_overview";
	var $pagetemplate;
	var $maintemplate;

	// Template view name

	public $auth;

	public $javascripts = array();
	public $stylesheets = array();

	// Default to do auto-rendering
	public $auto_render = TRUE;

	public function __construct(){
		parent::__construct();

		parent::__construct();
		$this->session = Session::instance();
			
		$this->auth = Auth::instance();
		$this->_checkLogin();


		$this->template = new View(Kohana::config('admin.theme')."/admin_view");

		$metainfo = array(
        	"username" => $_SESSION['auth_user']->username,
            "logoutlink" => html::anchor(url::base()."login/logout" ,'<span>'.Kohana::lang('barendszer.logout').'</span>',array('class' => 'logoutlink'))
		);

		$this->template->metainfo = $metainfo;

		$this->template->title = Kohana::config('admin.title');
		$this->template->base = Kohana::config('core.viewpath');
		$this->template->viewpath = Kohana::config('core.viewpath');
		$this->template->js = Kohana::config('admin.js');
		$this->template->img = Kohana::config('admin.img');
		$this->template->css = Kohana::config('admin.css');
		$this->template->assets = Kohana::config('core.assetspath');
		$this->template->swf = Kohana::config('admin.swf');
		$this->template->appname = Kohana::config('admin.appname');		
		
		if ($this->auto_render == TRUE){
			// Render the template immediately after the controller method
			Event::add('system.post_controller', array($this, '_render'));
		}

	}

	public function init(){
		$this->pagetemplate = new View(Kohana::config('admin.theme')."/admin/".$this->pageview);

		$this->pagetemplate->css = $this->template->css;
		$this->pagetemplate->img = $this->template->img;
		$this->pagetemplate->js = $this->template->js;
		$this->pagetemplate->swf = $this->template->swf;
		$this->pagetemplate->base = $this->template->base;

		$this->pagetemplate->assets = $this->template->assets;


	}

	/**
	 * Render the loaded template.
	 */
	public function _render(){

		if ($this->auto_render == TRUE){

			$this->template->render(TRUE);
		}
	}

	public function render(){
		$this->template->pageContent = $this->pagetemplate->render(FALSE,FALSE);
	}


	protected function _checkLogin(){

		if (!$this->auth->logged_in(Admin_Controller::$superadmin_role)){
			url::redirect('login');
		}
	}

}

?>