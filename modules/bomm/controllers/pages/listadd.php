<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listadd_Controller extends Ommpage_Controller {
	
	var $_pageview_1 = "lists_add_1";
	var $_pageview_2 = "lists_add_2";
	var $_pageview_3 = "lists_add_3";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		
		$this->tamplet->bodyClass = "listindex";
	}
	
	public function index()	{
		url::redirect("/pages/listadd/first");
	}


	public function first()	{
		
		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];	
		}else{
			$list = ORM::factory("omm_list");
		}
		
		
		$this->template->bodyClass = "";
		$this->pageview = $this->_pageview_1; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError		
		
		$form = array(
	        'name'      	=> $list->name,
	        'type'   		=> $list->type,
			'note'   		=> $list->note,
	        'unique_email'  => $list->unique_email,
	    );
		
		$errors = array(
	        'name'      	=> "",
	        'type'   		=> "",
			'note'   		=> "",
	        'unique_email'  => "",
	    );
	    
		$classes = $form;
		
		
		 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim', TRUE);
		        //$post->pre_filter('ucfirst', 'name');
		 		
		        $post->add_rules('name','required', 'length[0,255]');
	 
	 
	        // Test to see if things passed the rule checks
	        if ($post->validate())  {
				
	        	
	        	//$list = ORM::factory("omm_list");
	        	
	        	if(!$list->loaded){
		        	$list->code = string::random_string('unique');
		        	$list->omm_client_id = $this->client->id;
		        	$list->created_date = date("Y-m-d");	        		
	        	}

	        	
	        	$list->note = $post->note;
	        	if(empty($post->note))
	        		$list->note = null;
	        		
	        	$list->name = $post->name;
	        	$list->type = $post->type;
	        	$list->unique_email = $post->unique_email;
	           	
	           	
	           	
	        	$list->saveObject();

	        	if(isset($_SESSION['selected_list'])){
		        	url::redirect("/pages/listdetail");	        			        		
	        	}else{
		        	$_SESSION['selected_list'] = $list;
		        	url::redirect("/pages/listadd/second");	        		
	        	}
	        	

	           
	        } // No! We have validation errors, we need to show the form again, with the errors
	        else {
	            // repopulate the form fields
	            $form = arr::overwrite($form, $post->as_array());
	 
	            // populate the error fields, if any
	    		// We need to already have created an error message file, for Kohana to use
	            // Pass the error message file name to the errors() method
	            
	            $errors = arr::overwrite($errors, $post->errors('form_errors_lists'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }		
		
		$this->pagetemplate->listTypes = meta::getCodeDict(1);
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;		
		$this->render();
	}
	
	
	public function second(){
		$fieldId = $this->uri->segment(4,"");
		
		$this->pageview = $this->_pageview_2; 
		$this->init();
				
		if($fieldId != ""){
			$field = ORM::factory("omm_list_field",$fieldId);
			$this->pagetemplate->postUrl = url::base()."pages/listadd/second/".$fieldId;
			$this->pagetemplate->headermsg = "Mező szerkesztése";
			$this->pagetemplate->buttonmsg = "Mező mentése";
			
			
		}else{
			$field = ORM::factory("omm_list_field");
			$this->pagetemplate->postUrl = url::base()."pages/listadd/second";
			$this->pagetemplate->headermsg = "Új mező hozzáadása";
			$this->pagetemplate->buttonmsg = "Mező hozzáadása";
		}
		
		$this->template->bodyClass = "";
		
		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];	
		}else{
			url::redirect("/pages/listoverview");
		}
		
		

		$this->template->clientname = $this->client->name;
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		
		$this->pagetemplate->errors = ""; //clearfixError
		
		$form = array(
	        'name'      	=> $field->name,
	        'type'   		=> $field->type,
			'grid'   		=> $field->grid
	    );
		
		$errors = $form;
		$classes = $form;		
		
		
			 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim', TRUE);
		        $post->pre_filter('ucfirst', 'name');
		 		
		        $post->add_rules('name','required', 'length[0,255]');
		        $post->add_rules('type', 'required');
	 			
		        
	 
	        // Test to see if things passed the rule checks
	        if ($post->validate())  {
	
	        	$field->omm_list_id = $list->id;
	        	$field->name = $post->name;
	        	$field->type = $post->type;
	           	
	        	if(isset($post->grid)){
	        		$field->grid = $post->grid;
	        	}else{
	        		$field->grid = 0;
	        	}
	        		
	        			
				$blacklist = Kohana::config('blacklist.fields');
	        	
	        	if($fieldId == ""){
	        	   	$field->reference = string::create_reference_string($post->name);
	           		$i=2;
	           		$ref = $field->reference;
	           		
	           		while($field->isReferenceUnique() != 0 || in_array(strtoupper($field->reference), $blacklist)){
	           			if(in_array(strtoupper($field->reference), $blacklist)){
	           				$field->reference = $ref."_mezo";
	           			}else{
		           			$field->reference = $ref."_".$i;
		           			$i++;	           			
	           			}
	           		}
	        	}
	        	
	           	
	        	$field->field_id = string::random_string('unique');
	           	
	        	if(!$field->loaded){
	        		$field->ord = $field->getOrder();
	        		$ujmezo = true;	
	        	}else{
	        		$ujmezo = false;
	        	}
	        	
	        	
	        	$field->saveObject();
	        	
	        	if($field->type == "singleselectradio" || $field->type == "singleselectdropdown" || $field->type == "multiselect"){
					$field->grid = 0;
	        		$field->deleteOptions();
	        		
	        		$options = $_POST['options'];
	        		$codes = $_POST['codes'];
	        		
	        		$i=0;
	        		foreach ($options as $o){
	        			
	        			$fv = ORM::factory("omm_list_field_value");
	        			$fv->omm_list_field_id = $field->id;
	        			$fv->value = $o;
	        			
	        			if($codes[$i] == "nincscode"){
	        				$fv->code = string::random_string();	
	        			}else{
	        				$fv->code = $codes[$i];	
	        			}
	        			
	        			$fv->save();
	        			
	        			$i++;	
	        		}
	        		
	        	}
	        	$field->saveObject();
	        	unset($_POST);
	        	
	        	if($ujmezo){
					meta::createAlert("succes","Sikeres hozzáadás!","A mező sikeresen hozzá lett adva.");
	        	}else{
		    		meta::createAlert("succes","Sikeres módosítás!","A mező sikeresen módosítva lett.");
	        	}
		        	
	        	url::redirect("/pages/listadd/second");
	           
	        } 
	        else {
	            $form = arr::overwrite($form, $post->as_array());
	 
	            $errors = arr::overwrite($errors, $post->errors('form_errors_fields'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }		
		
		
		$this->pagetemplate->fields = $list->fields();
		$this->pagetemplate->fieldTypes = meta::getCodeDict(2);
		
		if($field->loaded){
			$this->pagetemplate->fieldvalues = $field->getValues();	
		}else{
			$this->pagetemplate->fieldvalues = array();
		}
		
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;	
		
		$this->render();
	}
	
	public function deleteField(){
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];
				
		$fieldId = $this->uri->segment(4,0);
		
		$db = new Database("own");
		
		$db->query("DELETE from ".$tp."omm_list_field_values WHERE omm_list_field_id = ".$fieldId." ");
		$db->query("DELETE from ".$tp."omm_list_fields  WHERE id = ".$fieldId." LIMIT 1");
		$db->query("DELETE from ".$tp."omm_list_member_datas  WHERE omm_list_field_id = ".$fieldId." ");
		$db->query("DELETE from ".$tp."omm_list_form_fields  WHERE omm_list_field_id = ".$fieldId." ");
		
		meta::createAlert("succes","Sikeres törlés!","A mező sikeresen törölve lett.");
		
	    url::redirect("/pages/listadd/second");		
	}
	
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}