<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listdetail_Controller extends Ommpage_Controller {
	
	var $_pageview = "lists_detail";
	var $_pagetemplate;
	var $client;
	var $list;
	var $rowperpage = 30;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}

		if(isset($_SESSION['selected_member'])){
			unset($_SESSION['selected_member']);
		}		

		if(isset($_SESSION['selected_form'])){
			unset($_SESSION['selected_form']);
		}			
		
		if(isset($_SESSION['selected_group'])){
			unset($_SESSION['selected_group']);
		}			

		if(isset($_SESSION['selected_campaign'])){
			unset($_SESSION['selected_campaign']);
		}		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
//	public function generateTestData(){
//		
//		for ($i=0;$i<999;$i++){
//		$this->list->addTestMember();	
//		}
//		
//		
//		
//	}
	
	public function index()	{
		
		if(!isset($_GET['showing'])){
			url::redirect("/pages/listdetail/index?showing=all");
		}elseif($_GET['showing'] != "all" && $_GET['showing'] != "year" && $_GET['showing'] != "month" && $_GET['showing'] != "week" && $_GET['showing'] != "day"){
			url::redirect("/pages/listdetail/index?showing=month");
		}
		
		
		$showing = $_GET['showing'];

		
		switch ($showing){
			
			case "all": $defparam = "all";break;
			case "year": $defparam = date("Y");break;
			case "month": $defparam = date("Y-m");break;
			case "week": { 
				$defparam = dateutils::nrweekday(date("Y-m-d"));
				$defparam = date( "Y-m-d", strtotime( $defparam." -1 week" )); 
				break;
			}
			case "day": $defparam = date("Y-m-d");break;
			
			
		}
		
		
		if(!isset($_GET['param'])){
			url::redirect("/pages/listdetail/index?showing=".$showing."&param=".$defparam);
		}				
		
		$param = $_GET['param'];		
		
		
		
		$memberStatus = $this->uri->segment(4,"active");
		$orderby = $this->uri->segment(5,"reg_date");
		$order = $this->uri->segment(6,"desc");
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		
		$this->pagetemplate->showing = $showing;
		$this->pagetemplate->param = $param;
		
		$this->pagetemplate->note = $this->list->note;
		$this->pagetemplate->listName = $this->list->name;
		$this->pagetemplate->list_id = $this->list->id;
		

		$listfilter = $this->list->id;
		$groupfilter = 'all';
		$tagfilter = 'all';
		$productfilter = 'all';		
		
		$allfields = array();
		
		$searchfields = $this->client->getFields();
		$fields = $this->client->gridFields();		
		
		$searchfield = "none";
		$keyword = "";		
		
		$this->pagetemplate->memberStatus = $memberStatus;
		
		$this->pagetemplate->allActiveMember = $this->list->membersNumber('active');
		
		if($showing == "all"){
			
			$this->pagetemplate->activeMemberCount = $this->list->membersNumber('active');
			$this->pagetemplate->unsubscribedMemberCount = $this->list->membersNumber('unsubscribed');
			$this->pagetemplate->deletedMemberCount = $this->list->membersNumber('deleted');	
			$this->pagetemplate->preregMemberCount = $this->list->membersNumber('prereg');
			$this->pagetemplate->errorMemberCount = $this->list->membersNumber('error');
			
			
				$this->pagetemplate->allActiveMember = $this->client->membersNumber($allfields,'active',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);
			
				
				$this->pagetemplate->activeMemberCount = $this->pagetemplate->allActiveMember;
				$this->pagetemplate->unsubscribedMemberCount = $this->client->membersNumber($allfields,'unsubscribed',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);
				$this->pagetemplate->deletedMemberCount = $this->client->membersNumber($allfields,'deleted',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);	
				$this->pagetemplate->preregMemberCount = $this->client->membersNumber($allfields,'prereg',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);
				$this->pagetemplate->errorMemberCount = $this->client->membersNumber($allfields,'error',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields);			
			
			$pages = Pagination::factory(array
			(
			    'items_per_page' => $this->rowperpage,
			    'query_string' => 'page',
			    'total_items' => $this->list->membersNumber($memberStatus)
			));				
			
		}else{
			$stat = $this->list->getMemberStat($showing,$param);
			
			$this->pagetemplate->activeMemberCount = $stat['active'];
			$this->pagetemplate->unsubscribedMemberCount = $stat['unsubscribed'];
			$this->pagetemplate->deletedMemberCount = $stat['deleted'];
			$this->pagetemplate->preregMemberCount = 0;			
			
			
			
			
			$pages = Pagination::factory(array
			(
			    'items_per_page' => $this->rowperpage,
			    'query_string' => 'page',
			    'total_items' => $stat[$memberStatus]
			));				
			
		}
		
		$this->pagetemplate->gridFields = $this->client->gridFields();
		
		$this->pagetemplate->orderby = $orderby;
		$this->pagetemplate->order = $order;

		
			if(isset($_GET['page']))
				$page = $_GET['page'];
			else
				$page = 1;		
		
			if($page == 1){
				$offset = 0;
			}else{
				$page = $page-1;
				$offset = $page*$this->rowperpage;
			}
				
		
		
		
		
		if($this->list->membersNumber($memberStatus) > $this->rowperpage){
			$this->pagetemplate->pagination = $pages->render("ommpagination");		
		}else{
			$this->pagetemplate->pagination = "";
		}
		
		
		
		$this->pagetemplate->checkUnsubProcess = $this->list->checkUnsubProcess();
		$this->pagetemplate->checkSubProcess = $this->list->checkSubProcess();
		
					if(isset($_POST['searchkeyword'])) $keyword = $_POST['searchkeyword'];
					else $keyword = "none"; 

					if(isset($_POST['searchfield'])) $searchfield = $_POST['searchfield'];
					else $searchfield = "none";			
		
		if($searchfield != "none"){
			$this->pagetemplate->pagination = "";
		}

				
		$this->pagetemplate->members = $this->client->getMembersForSubscribersTable($allfields,$memberStatus,$orderby,$order,$offset,$this->rowperpage,$searchfield,$keyword,$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfields);			
		//$this->pagetemplate->members = $this->list->getMembersForListTable($memberStatus,$orderby,$order,$offset,$this->rowperpage,$showing,$param,$searchfield,$keyword);
		
		$this->render();
	}

	
	public function deleteAllMember(){
		
		$count = $this->list->deleteAll();
		
		meta::createAlert("succes","Sikeres törlés!","A feliratkozók törölve lettek a listáról! (".$count.")");
		
		url::redirect("/pages/listdetail");
	}
	
	public function selectmember(){
		
		$_SESSION['listdetailreferrer'] = $_SERVER['HTTP_REFERER'];
		
		
		$mid = $this->uri->segment(4,"");

		if($mid == ""){
			url::redirect("/pages/listdetail");
		}else{
			
			$_SESSION['selected_member'] = $mid;
			url::redirect("/pages/memberdetail/index/".$mid);	
		}
		
	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		KOHANA::show_404(FALSE,FALSE);
	}

}