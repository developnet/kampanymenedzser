<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Productoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "products_overview";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		$this->template->bodyClass = "listindex";
	}
	
	public function index()	{
		
		if(isset($_SESSION['selected_product'])){
			unset($_SESSION['selected_product']);
		}
		
		if(isset($_SESSION['selected_campaign'])){
			unset($_SESSION['selected_campaign']);
		}		

		if(isset($_SESSION['selected_letter'])){
			unset($_SESSION['selected_letter']);
		}
		if(isset($_SESSION['selected_member'])){
			unset($_SESSION['selected_member']);
		}
		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		//
		$this->pagetemplate->archived_products = $this->client->products("name","asc","archive");
		$this->pagetemplate->products = $this->client->products("name");
		$this->pagetemplate->membersSum = 0;
		$this->pagetemplate->archived_membersSum = 0;
		
		
		$this->render();
	}

	public function deleteList(){
		
		$listId = $this->uri->segment(4,"");

		if($listId == ""){
			url::redirect("/pages/listoverview");
		}else{
			
			$list = ORM::factory("omm_list")->find($listId);
			
			if($list->loaded){
				
				$list->deleteList();

			    meta::createAlert("succes","Sikeres törlés!","A lista és a hozzá kapcsolódó adatok törölve lettek a rendszerből.");
				
			}else{
				url::redirect("/pages/listoverview");	
			}
			
		}
		
		
		
		url::redirect("/pages/listdetail");
	}	
	
	public function selectproduct(){
		$pid = $this->uri->segment(4,"");

		if($pid == ""){
			url::redirect("/pages/productoverview");
		}else{
			url::redirect("/pages/productdetail/index/".$pid);	
		}
		
	}
	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}