<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Letterdetail_Controller extends Ommpage_Controller {

	var $_pageview_1 = "letters_detail_1";
	var $_pageview_2 = "letters_detail_2";
	var $_pageview_3 = "letters_detail_3";
	var $_pageview_4 = "letters_detail_4";
	var $_pageview_5 = "letters_detail_5";
	var $_pageview_draft = "letters_detail_draft";
	var $_pageview_active = "letters_detail_active";
	var $_pageview_statistic = "statistic_letter_overview";

	var $_pagetemplate;
	var $client;
	var $campaign;

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();

		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_campaign'])){
			$this->campaign = $_SESSION['selected_campaign'];
		}else{
			url::redirect("/pages/campaignoverview");
		}

		$this->template->bodyClass = "subscriberlist";
	}


	public function index(){
		url::redirect('/pages/letterdetail/first');
	}


	public function draft(){
		$this->pageview = $this->_pageview_draft;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letterdetail/first');
		}

		$this->pagetemplate->campaign = $this->campaign;
		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function stat(){
		$this->pageview = $this->_pageview_statistic;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->letter = $letter;
		}else{
			url::redirect('/pages/statisticoverview');
		}


		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function active(){
		$this->pageview = $this->_pageview_active;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letterdetail/first');
		}


		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function first()	{
		$this->pageview = $this->_pageview_1;
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";


		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			$letter = ORM::factory("omm_letter");
			$this->pagetemplate->name = "Új levél";
		}


		if($letter->loaded){

			$form = array(
		        'name'  		=> $letter->name,
				'note'  		=> $letter->note,
		        'subject'  		=> $letter->subject,
				'sender_name'  	=> $letter->sender_name,
		        'sender_email'	=> $letter->sender_email,
				'sender_replyto'=> $letter->sender_replyto
			);

		}else{

			$form = array(
		        'name'  		=> "",
				'note'  		=> "",
				'subject'  		=> "",
		        'sender_name'  	=> $this->campaign->sender_name,
		        'sender_email'	=> $this->campaign->sender_email,
				'sender_replyto'=> $this->campaign->sender_replyto
			);

		}


		$errors = array(
		        'name'  		=> "",
				'note'  		=> "",
		        'subject' 	 	=> "",
		        'sender_name'  	=> "",
		        'sender_email'	=> "",
				'sender_replyto'=> ""
				);
					
				$classes = $errors;



				if ($_POST)
				{
					// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
					$post = new Validation($_POST);

					//  Add some filters
					$post->pre_filter('trim', TRUE);

					// Add some rules, the input field, followed by a list of checks, carried out in order
					$post->add_rules('name','required');
					$post->add_rules('subject','required');
					$post->add_rules('sender_name','required');
					$post->add_rules('sender_email','required');
					//$post->add_rules('sender_replyto',);
					//$post->add_callbacks('sender_email', array($this, '_domain_email'));


					// Test to see if things passed the rule checks
					if ($post->validate())  {
							
						if(!$letter->loaded){
							$letter->type = "html";
							$letter->omm_campaign_id = $this->campaign->id;
							$letter->created_date = date("Y-m-d H:i:s");

						}

						$letter->status = 'draft';
						$letter->subject = $post->subject;
						$letter->name = $post->name;
						
						$letter->note = $post->note;
						
						if(empty($post->note))
							$letter->note = null;
						
						$letter->sender_name = $post->sender_name;
						$letter->sender_email = $post->sender_email;
						$letter->sender_replyto = $post->sender_replyto;

						$letter->saveObject();
							

							
							
							
						if(isset($_POST['exitinput']) && $_POST['exitinput']=="exit"){

							unset($_POST);
							url::redirect("/pages/letteroverview/selectletter/".$letter->id);
						}
							
							
						if(isset($_POST['exitinput']) && $_POST['exitinput']=="tonextpage"){
							$_SESSION['selected_letter'] = $letter->id;
							unset($_POST);
							url::redirect("/pages/letterdetail/second");
						}
							
							
							
							
					} // No! We have validation errors, we need to show the form again, with the errors
					else {
						// repopulate the form fields
						$form = arr::overwrite($form, $post->as_array());

						// populate the error fields, if any
						// We need to already have created an error message file, for Kohana to use
						// Pass the error message file name to the errors() method

						$errors = arr::overwrite($errors, $post->errors('form_errors_letter'));

						$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
						$errorTempl->errors = $errors;

						$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

						foreach ($errors as $key => $error){
							if($error != ""){
								$classes[$key] = "clearfixError";
							}
						}

					}
				}

				$this->pagetemplate->lists = ORM::factory("omm_list")->where("omm_client_id",$this->client->id)->find_all();
				$this->pagetemplate->classes = $classes;
				$this->pagetemplate->form = $form;
				$this->render();
	}


	public function second(){


		$this->pageview = $this->_pageview_2;
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
			

		$this->pagetemplate->errors = ""; //clearfixError
			
			
		if(isset($_SESSION['htmlerror'])){
			$this->pagetemplate->htmlerrors = $_SESSION['htmlerror'];
			unset($_SESSION['htmlerror']);
		}else{
			$this->pagetemplate->htmlerrors = "";
		}
			
			
		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}
			
			
			
		if($letter->loaded){

			$form = array(
						'text_content' => $letter->text_content,
						'html_content' => $letter->html_content
			);

		}else{
			url::redirect('/pages/letterdetail/');

		}

			
			
			
						$errors = array(
						'text_content' => '',
						'html_content' => ''
						);

						$classes = $errors;
							
							
							
						if ($_POST){

							$post = new Validation($_POST);

							$post->pre_filter('trim', TRUE);

							//$post->add_callbacks('text_content', array($this, '_check_fields'));
							//$post->add_callbacks('html_content', array($this, '_check_fields'));


							if ($post->validate())  {



								$letter->type = $post->lettertype;
								$letter->text_content = $_POST['text_content'];

								$letter->setHtmlContent($_POST['html_content']);

								//$letter->realhtml_content = $letter->getRealHtml();

								if(isset($_POST['copyText'])){
									$letter->text_content = $letter->extractText($letter->html_content);
								}
								
								$letter->saveObject();

								if(isset($_POST['copyTemplate'])){
									$letter->text_content = $letter->extractText($letter->html_content);
									
									$template = ORM::factory("omm_letter_template");
					        		$template->omm_client_id = $this->client->id;
					        		$template->name = $letter->name."_sablon";		
					        		
					        		if((bool)$template->trySetHtml($_POST['html_content'])){
	        							$template->saveObject();
	        							$_SESSION['fromletter'] = true;
	        							meta::createAlert("succes","Sikeres hozzáadás!","A sablon hozzáadása sikeres volt.");
	        							url::redirect('pages/lettertemplates/selecttemplate/'.$template->id);
					        		}else{
					        			////nem jó a html
				        			}
									
								}								
								

								if($letter->type=="html" && $letter->html_content == ""){
									meta::createAlert("error","Hiányos tartalom!","A levél típusa html, de nincs feltöltve html tartalom!");
								}

								if($_POST['exit'] == "true"){
									unset($_POST);
									url::redirect('/pages/letterdetail/draft');
								}else if($_POST['exit'] == "tonextpage"){
									unset($_POST);
									url::redirect('/pages/letterdetail/test');
								}else if($_POST['exit'] == "toprevpage"){
									unset($_POST);
									url::redirect('/pages/letterdetail/first');
								}else{
									unset($_POST);
									url::redirect('/pages/letterdetail/second');
								}

									
									
							}
							else {
								$form = arr::overwrite($form, $post->as_array());
									
									
								$errors = arr::overwrite($errors, $post->errors('form_errors_letter'));
									
								$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
								$errorTempl->errors = $errors;
									
								$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
									
								foreach ($errors as $key => $error){
									if($error != ""){
										$classes[$key] = "clearfixError";
									}
								}
									
							}
						}else{
							if($letter->type=="html" && $letter->html_content == ""){
								meta::createAlert("error","Hiányos tartalom!","A levél típusa html, de nincs feltöltve html tartalom!");
							}
						}
						$this->pagetemplate->client = $this->client;
						$this->pagetemplate->cid = $this->client->id;
						$this->pagetemplate->files = $letter->getUploadedFiles();
							
						$this->pagetemplate->lists = ORM::factory("omm_list")->where("omm_client_id",$this->client->id)->find_all();
							
						$this->pagetemplate->templates = $this->client->tempaltes();
							
						if(isset($_SESSION['alert'])){
							$this->pagetemplate->errors = $_SESSION['alert'];
							unset($_SESSION['alert']);
						}else{
							$this->pagetemplate->errors = ""; //clearfixError
						}						
						
						
						$this->pagetemplate->letter = $letter;
						$this->pagetemplate->classes = $classes;
						$this->pagetemplate->form = $form;
						$this->render();


	}

	public function usetemplate(){
		$this->template->clientname = $this->client->name;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		$id = $this->uri->segment(4,"");

		if($id != ""){

			$templ = ORM::factory('omm_letter_template')->find($id);

			if($templ->loaded){

				$templ->replaceUrl(url::base()."files/".$this->client->id."/images/");

				$letter->trySetHtml($templ->html);

				$letter->saveObject();

				url::redirect("/pages/letterdetail/second");
			}

		}

		url::redirect("/pages/letterdetail/second");

	}

	public function recipients(){
		$this->pageview = $this->_pageview_3;
		$this->init();
		$this->template->clientname = $this->client->name;



		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		if($_POST){
			
			if(isset($_POST['btnAddAllList'])){//// ÖSSZES LISTA POZ

				$lists = $this->client->lists("name");
				$letter->clearIncludeLists();
				
				foreach($lists as $l){
					$letter->addListForInclude($l->id);	
					
				}
				url::redirect('/pages/letterdetail/recipients');
			}elseif(isset($_POST['btnAddAllListNeg'])){//// ÖSSZES LISTA NEG
			
				$lists = $this->client->lists("name");
				$letter->clearExcludeLists();
				
				foreach($lists as $l){
					$letter->addListForExclude($l->id);	
					
				}				
				url::redirect('/pages/letterdetail/recipients');
				
			}elseif(isset($_POST['btnClearAllList'])){//// ÖSSZES LISTA TÖRLÉSE
				
				$letter->clearIncludeLists();
				url::redirect('/pages/letterdetail/recipients');
				
			}elseif(isset($_POST['btnClearAllListNeg'])){//// ÖSSZES LISTA TÖRLÉSE NEG
				
				$letter->clearExcludeLists();
				url::redirect('/pages/letterdetail/recipients');
				
			}else{ //// egyenként
				
				if(isset($_POST['productsForInclude'])){
					$letter->addProductsForInclude($_POST['productsForInclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');					
				}
				
				if(isset($_POST['productsForExclude'])){
					$letter->addProductsForExclude($_POST['productsForExclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');					
				}				
				
				if(isset($_POST['tagsForInclude'])){
					$letter->addTagsForInclude($_POST['tagsForInclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');					
				}
				
				if(isset($_POST['tagsForExclude'])){
					$letter->addTagsForExclude($_POST['tagsForExclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');					
				}					
				
				if(isset($_POST['listsForInclude']) && $_POST['listsForInclude'] != ""){
					$letter->addListForInclude($_POST['listsForInclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');
				}
	
				if(isset($_POST['listsForExclude']) && $_POST['listsForExclude'] != ""){
					$letter->addListForExclude($_POST['listsForExclude']);
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');
				}
	
	
				///////////////////////////////////////////////////////////INCLUDE
				
				if(isset($_POST['includeProducts'])){
					$letter->clearProducts('plus');
					
					foreach ($_POST['includeProducts'] as $lid){
	
						if(isset($_POST['incallinproduct_'.$lid]) && $_POST['incallinproduct_'.$lid] == '1'){
							$letter->addProductsForInclude($lid);
						}else{
							$letter->addProductsForInclude($lid,'0');
						}
	
					}					
				}else{
					$letter->clearProducts('plus');
				}
				
				if(isset($_POST['includeProductStat'])){
					$letter->clearProductStats('plus');

					foreach($_POST['includeProductStat'] as $pid){
						$_pid = explode("_", $pid);
						$letter->addProductStat($_pid[0],$_pid[1],'plus');//
					}
					
				}else{
					$letter->clearProductStats('plus');
				}
				
				
				
				
				
				if(isset($_POST['includeLists'])){
	
					$letter->clearIncludeLists();
	
					foreach ($_POST['includeLists'] as $lid){
	
						if(isset($_POST['incallinlist_'.$lid]) && $_POST['incallinlist_'.$lid] == '1'){
							$letter->addListForInclude($lid);
						}else{
							$letter->addListForInclude($lid,'0');
						}
	
					}
				}else{
					$letter->clearIncludeLists();
				}
	
				if(isset($_POST['includeGroups'])){
	
					$letter->clearIncludeGroups();
	
					foreach ($_POST['includeGroups'] as $id){
						$letter->addGroupForInclude($id);
					}
				}else{
					$letter->clearIncludeGroups();
				}
	
				//////////////////////////////////////////////////////////EXCLUDE
				
				if(isset($_POST['excludeProducts'])){
					$letter->clearProducts('minus');
					
					foreach ($_POST['excludeProducts'] as $lid){
	
						if(isset($_POST['excallinproduct_'.$lid]) && $_POST['excallinproduct_'.$lid] == '1'){
							$letter->addProductsForExclude($lid);
						}else{
							$letter->addProductsForExclude($lid,'0');
						}
	
					}					
				}else{
					$letter->clearProducts('minus');
				}
				
				if(isset($_POST['excludeProductStat'])){
					$letter->clearProductStats('minus');

					foreach($_POST['excludeProductStat'] as $pid){
						$_pid = explode("_", $pid);
						$letter->addProductStat($_pid[0],$_pid[1],'minus');//
					}
					
				}else{
					$letter->clearProductStats('minus');
				}				
				
				
				if(isset($_POST['excludeLists'])){
	
					$letter->clearExcludeLists();
	
					foreach ($_POST['excludeLists'] as $lid){
	
						if(isset($_POST['exallinlist_'.$lid]) && $_POST['exallinlist_'.$lid] == '1'){
							$letter->addListForExclude($lid);
						}else{
							$letter->addListForExclude($lid,'0');
						}
	
					}
				}else{
					$letter->clearExcludeLists();
				}
	
				if(isset($_POST['excludeGroups'])){
	
					$letter->clearExcludeGroups();
	
					foreach ($_POST['excludeGroups'] as $id){
						$letter->addGroupForExclude($id);
					}
				}else{
					$letter->clearExcludeGroups();
				}
	
	
	
				if($_POST['exit'] == "true"){
					unset($_POST);
					url::redirect('/pages/letterdetail/draft');
				}else if($_POST['exit'] == "tonextpage"){
					unset($_POST);
					url::redirect('/pages/letterdetail/test');
				}else if($_POST['exit'] == "toprevpage"){
					unset($_POST);
					url::redirect('/pages/letterdetail/timing');
				}else{
					unset($_POST);
					url::redirect('/pages/letterdetail/recipients');
				}				
			
			}


		}

	
		
		$form = array();
		$errors = array();

		$classes = $errors;
			

		$this->pagetemplate->productsIncludes = $letter->productsIncludes();
		$this->pagetemplate->productsExcludes = $letter->productsExcludes();			
		
		$this->pagetemplate->tagsIncludes = $letter->tagsIncludes();
		$this->pagetemplate->tagsExcludes = $letter->tagsExcludes();		

		$this->pagetemplate->listsIncludes = $letter->listsIncludes();
		$this->pagetemplate->listsExcludes = $letter->listsExcludes();
		
		$this->pagetemplate->emailConditions = $letter->emailConditions();

		$this->pagetemplate->groupsPlus = $letter->groupCondArray('plus');
		$this->pagetemplate->groupsMinus = $letter->groupCondArray('minus');
		
		$this->pagetemplate->client = $this->client;
		$this->pagetemplate->lists = $this->client->lists();

		//$this->pagetemplate->recipNumber = $letter->getRecipientsNumber();
		$this->pagetemplate->recipNumber = $letter->getRecipientsNumberFilter();
		

		$this->pagetemplate->letter = $letter;
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;
		$this->render();
	}


	public function test(){
		$this->pageview = $this->_pageview_4;
		$this->init();
		$this->template->clientname = $this->client->name;

		

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		$this->pagetemplate->fields = $letter->getFieldsInLetter();
		
		
		
		if($_POST){
			$post = new Validation($_POST);

			$post->pre_filter('trim', TRUE);
			$post->add_rules('email','required');

			if ($post->validate())  {


				$emails = explode(",",$post->email);
				$jobs = array();

				foreach($emails as $e){

					$m = ORM::factory('omm_list_member');

					$regdate =  date("Y-m-d H:i:s");
					
					if(isset($_POST['testdetails']) && $_POST['testdetails'] != ""){
						$data = json_decode($_POST['testdetails']);
					}

					$m->createTestUser($e,0,$regdate,$_POST['testdetails']);

					$job = ORM::factory('omm_job');
					$job->create('test', $letter->id, NULL, $m->id, NULL );
					$jobs[] = $job;
				}


				jobs::prepareJobs($jobs);

				$swift = jobs::getSwift(true,false,'188.227.227.172');
				//$swift = jobs::getSwift(true);

				$db = new Database("own");
				jobs::sendJobs($jobs,$letter->type,$swift, $db);

				$swift->disconnect();

				$alertTempl = new View(Kohana::config('admin.theme')."/common/succes");
				$alertTempl->alertTitle = "Teszt elküldve";
				$alertTempl->alertMessage = "".$post->email."";
				$this->pagetemplate->alert = $alertTempl->render(FALSE,FALSE);
				unset($_POST);
			}
			else {
				$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
				$errorTempl->errors = array('Kérem adja meg vesszővel elválasztva az e-mail címeket!');
				$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
				unset($_POST);
			}
		}
		
		$this->pagetemplate->lastTestMember = $this->client->getLastTestMember();

		$this->pagetemplate->recents = array();

		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function timing(){
		$this->pageview = $this->_pageview_5;
		$this->init();
		$this->template->clientname = $this->client->name;

		if(isset($_SESSION['errors'])){
			$this->pagetemplate->errors = $_SESSION['errors'];
			unset($_SESSION['errors']);
		}else{
			$this->pagetemplate->errors = ""; //clearfixError
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;

			$form = array(
				'fromsubscribe_timing_event_value' => '1',
				'base_time' => date("Y-m-d"),
				'_time' => 'false',
				'timing_hour' => 0
			);

			if($letter->timing_type == "relative"){

				$form['fromsubscribe_timing_event_value'] = $letter->timing_event_value;

			}elseif($letter->timing_type == "absolute"){

				$form['base_time'] = $letter->timing_date;


				if($letter->timing_hour == NULL){
					$form['_time'] = 'false';
					$form['timing_hour'] = 0;
				}else{
					$form['_time'] = 'true';
					$form['timing_hour'] = $letter->timing_hour;
				}





			}elseif($letter->timing_type == "sendnow"){

			}


			if($letter->timingCheck(true)){

				$alertTempl = new View(Kohana::config('admin.theme')."/common/succes");

				if($letter->timing_type == "relative"){
					$tim = "Relatív";
				}elseif($letter->timing_type == "absolute"){
					$tim = "Abszolút";
				}elseif($letter->timing_type == "sendnow"){
					$tim = "Azonnali";
				}

				$alertTempl->alertTitle = "A levél sikeresen be lett időzítve! (".$tim.")";
				$alertTempl->alertMessage = "Az időzítés megváltoztatásához állítsa be a kívánt időzítést és nyomja meg mentés gombot!";
				$this->pagetemplate->alert = $alertTempl->render(FALSE,FALSE);

			}

			if(isset($_SESSION['alert'])){
				$this->pagetemplate->alert = $_SESSION['alert'];
				unset($_SESSION['alert']);
			}else{
				$this->pagetemplate->alert = "";
			}

			if ($_POST)	{

				$post = new Validation($_POST);

				$post->pre_filter('trim', TRUE);
					
				$post->add_rules('timing_type','required');
					

				if ($post->validate())  {

					$letter->timing_type = $post->timing_type;

					if($letter->timing_type == "relative"){

						if($post->fromsubscribe_timing_event_value == 0){
							meta::createAlert("error","Hiba az időzítés beállításában!","A relatív időzítésnél nem adhat meg 0 napot!");
							unset($_POST);
							url::redirect('/pages/letterdetail/timing');
						}

						$letter->timing_event_type = "fromsubscribe";
						$letter->timing_event_value = $post->fromsubscribe_timing_event_value;



					}elseif($letter->timing_type == "absolute"){

						$letter->timing_event_type = "";
						$letter->timing_event_value = "";
						$letter->timing_date = $post->base_time;

						if(isset($post->timing_hour) && $post->timing_hour == "0"){
							$letter->timing_hour = NULL;
						}else{
							
							if(isset($post->timing_hour)){
								$letter->timing_hour = $post->timing_hour;
							}else{
								$letter->timing_hour = NULL;
							}

						}



					}elseif($letter->timing_type == "sendnow"){

						$letter->timing_event_type = "";
						$letter->timing_event_value = "";
						$letter->timing_date = "";

					}


					if(!isset($_POST['timing_type'])){
						////öööööö
							
					}else{
						$letter->saveObject();
					}

					if($_POST['exit'] == "true"){
						unset($_POST);
						url::redirect('/pages/letterdetail/draft');
					}else if($_POST['exit'] == "tonextpage"){
						unset($_POST);
						url::redirect('/pages/letterdetail/recipients');
					}else if($_POST['exit'] == "toprevpage"){
						unset($_POST);
						url::redirect('/pages/letterdetail/test');
					}else{
						unset($_POST);
						url::redirect('/pages/letterdetail/timing');
					}

				}else{
					meta::createAlert("error","Hiba az időzítés beállításában!","Adja meg az időzítés típusát!");
					unset($_POST);
					url::redirect('/pages/letterdetail/timing');
				}
			}

		}else{
			url::redirect('/pages/letteroverview'); 
		}

		$this->pagetemplate->form = $form;
		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function deleteProductCondition($id,$type){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}

		if($id != ""){
			$letter->deleteProductCondition($id,$type);
		}
		
		url::redirect('/pages/letterdetail/recipients');
	}		
	
	public function deleteTagCondition($id,$type){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}

		if($id != ""){
			$letter->deleteTagCondition($id,$type);
		}
		
		url::redirect('/pages/letterdetail/recipients');
	}	

	public function deleteListConditionInclude(){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}


		$id = $this->uri->segment(4,"");

		if($id != ""){

			$list = ORM::factory('omm_list')->find($id);

			if($list->loaded){
				foreach($list->groups() as $g){
					$letter->deleteGroupConditionInclude($g->id);
				}
				$letter->deleteListConditionInclude($list->id);
			}
		}
		url::redirect('/pages/letterdetail/recipients');
	}

	public function deleteListConditionExclude(){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}


		$id = $this->uri->segment(4,"");

		if($id != ""){

			$list = ORM::factory('omm_list')->find($id);

			if($list->loaded){
				foreach($list->groups() as $g){
					$letter->deleteGroupConditionExclude($g->id);
				}
				$letter->deleteListConditionExclude($list->id);
			}
		}
		url::redirect('/pages/letterdetail/recipients');
	}

	public function addemailconditions(){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}


		if(isset($_POST['emailadd'])){
			$letter->addEmailCondition($_POST['emailadd']);
			unset($_POST);
		}


			
			
		url::redirect('/pages/letterdetail/recipients');

	}

	public function deleteemailconditions(){
		$this->pageview = $this->_pageview_3;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
		}else{
			url::redirect('/pages/letteroverview');
		}


		$condid = $this->uri->segment(4,"");

		if($condid != ""){

			$letter->deleteEmailCondition($condid);

		}
		url::redirect('/pages/letterdetail/recipients');

	}

	public function deletefile(){

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		$filen = $this->uri->segment(4,"");

		if($filen == ''){
			url::redirect('/pages/letterdetail/second');
		}

		$this->template->clientname = $this->client->name;
		$file = DOCROOT.'files/'.$letter->id.'/'.$filen;

		if(is_file("$file")) {
			unlink("$file");
		}

		$letter->realhtml_content = $letter->getRealHtml();
		$letter->save();

		url::redirect('/pages/letterdetail/second');
	}

	public function fileupload(){
		$this->template->clientname = $this->client->name;
		$files = Validation::factory($_FILES)->add_rules('upload::valid', 'upload::reqired', 'upload::type[gif,jpg,png,doc,xls,pdf]', 'upload::size[8M]');

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}


		if ($files->validate()){

			foreach($_FILES as $key => $f){
				upload::save($key, $_FILES[$key]['name'] ,DOCROOT.'files/'.$this->client->id);
				$letter->realhtml_content = $letter->getRealHtml();
				$letter->save();
			}
		}

		unset($_POST);
		url::redirect("/pages/letterdetail/second");
	}

	public function uploadhtml(){

		$this->template->clientname = $this->client->name;

		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		if (upload::valid($_FILES['htmlFile']) && upload::type($_FILES['htmlFile'],array('html'))){
			$filename = upload::save('htmlFile');

			$content = fileutils::file_get_contents_utf8($filename);

			if((bool)$letter->trySetHtml($content)){
				$letter->saveObject();
			}else{
				$_SESSION['htmlerror'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban nincsen body tag.", true);
			}

			if($filename != ""){
				unlink($filename);
			}else{
				$_SESSION['htmlerror'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
			}



		}else{
			$_SESSION['htmlerror'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
		}

		unset($_POST);
		unset($_FILES);
		url::redirect("/pages/letterdetail/second");

	}

	public function duplicateletter(){
		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		$letter->duplicate();
		url::redirect('/pages/letteroverview');
	}

	public function settodraft(){

		if(isset($_SESSION['selected_letter'])){

			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;

		}else{
			url::redirect('/pages/campaignoverview');
		}

		if($letter->timingCheckForHour(true)){

			$letter->status='draft';
			$letter->saveObject();
			url::redirect('/pages/letteroverview/selectletter/'.$letter->id);

		}else{
			meta::createAlert("error","A levelet már nem lehet szerkeszteni!","A levelet már nem lehet szerkeszteni, mert a kiküldés folyamatban van.");
			url::redirect('/pages/letteroverview/selectletter/'.$letter->id);
		}
	}


	public function editletter(){
		$id = $this->uri->segment(4,"");

		if($id == ""){
			url::redirect("/pages/campaignoverview");
		}else{

			$l = ORM::factory("omm_letter")->find($id);

			if($l->loaded && $l->status == 'active' && !$l->isSent()){

				if($l->timingCheckForHour(true)){

					$l->status='draft';
					$l->saveObject();

					meta::createAlert("alert","Levél inaktiválva lett és szerkeszthető!","A levél inaktiválása sikeres volt, elkezdheti az adatok szerkesztését. A levelet nem lehet kiküldeni addíg míg újra nem aktiválja!");
					url::redirect('/pages/letteroverview/selectletter/'.$l->id);
				}else{
					meta::createAlert("error","A levelet már nem lehet szerkeszteni!","A levelet már nem lehet szerkeszteni, mert a kiküldés folyamatban van.");
					url::redirect('/pages/letteroverview/selectletter/'.$l->id);
				}

			}else{
				url::redirect("/pages/campaignoverview");
			}
		}
	}

	public function activate(){
		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->name = $letter->name;
		}else{
			url::redirect('/pages/letteroverview');
		}

		$err = array();

		if(!$letter->contentCheck(true)){
			$err[] = 'Hiányos tartalom!';
		}

		if(!$letter->timingCheck(true)){
			$err[] = 'A levél nincs időzítve';
		}

		if(!$letter->recipCheck(true)){
			$err[] = 'Nincsenek címzettek!';
		}

		if(!$letter->testCheck(true)){
			$err[] = 'A levél még nem lett tesztelve!';
		}

		if(!$letter->timingCheckForHour(true)){
			$err[] = 'A levél a múltba van időzítve!';
		}

		$ret = $letter->fieldsCheck();

		if(sizeof($ret['html'])>0){
			$e = 'A levél html tartalmában olyan behelyettesítő tag van, ami egyik címzett lista mezőjének sem felel meg:<br/>';

			foreach($ret['html'] as $h){

				$e .= "&nbsp;&nbsp;&nbsp;&nbsp; ".html::specialchars($h)."<br/>";
			}

			$err[] = $e;
		}

		if(sizeof($ret['text'])>0){
			$e = 'A levél text tartalmában olyan behelyettesítő tag van, ami egyik lista mezőjének sem felel meg:<br/>';

			foreach($ret['text'] as $h){

				$e .= "&nbsp;&nbsp;&nbsp;&nbsp; ".html::specialchars($h)."<br/>";
			}

			$err[] = $e;
		}

		if(sizeof($ret['subject'])>0){
			$e = 'A levél tárgyában olyan behelyettesítő tag van, ami egyik lista mezőjének sem felel meg:<br/>';

			foreach($ret['subject'] as $h){

				$e .= "&nbsp;&nbsp;&nbsp;&nbsp; ".html::specialchars($h)."<br/>";
			}

			$err[] = $e;
		}

		if(sizeof($err)>0){
			$errmsg = "";
			foreach ($err as $e){
				$errmsg .= $e."<br/>";
			}

			meta::createAlert("error","Hiba az aktiválás során!",$errmsg);

			url::redirect('/pages/letterdetail/draft');
		}else{
			$letter->status='active';
			$letter->saveObject();

			meta::createAlert("succes","Levél sikeresen aktiválva lett!","A levél adatait most nem szerkesztheti!");

			url::redirect('/pages/letterdetail/draft');
		}

			


	}

	public function _domain_email(Validation $array, $field){

		$domain = preg_split("[@]",$array['sender_email']);

		if ($domain[1] != $this->client->site_domain)  {
			$array->add_error($field, 'invalid_domain_email');
		}

	}


	public function _check_fields(Validation $array, $field){
		//text_content
		//html_content

		$lists = $this->client->lists();
			
		$fields = array();
		$fields[] = "regdatum";
		$fields[] = "email";
		foreach ($lists as $l){

			foreach ($l->fields() as $f){
				$fields[] = $f->reference;
			}

		}
			
		$sepStart = Kohana::config('core.fieldSeparatorStart');
		$sepEnd = Kohana::config('core.fieldSeparatorEnd');
		$unsubLink = Kohana::config('core.unsubscribe_link_name');
		$actLink = Kohana::config('core.activation_link_name');
		$modLink = Kohana::config('core.datamod_link_name');
			
		$pattern = '/'.$sepStart.'[^'.$sepStart.']+?'.$sepEnd.'/i';

		$fldEmp = Kohana::config('core.fieldEmpty');
		$fldEmpSep = Kohana::config('core.fieldEmptySeparator');
		$fldUresSep = Kohana::config('core.fieldUresSeparator');
			
		$startSepSize = strlen($sepStart);
		$endSepSize = strlen($sepEnd);
			
			
			
		$matches = array();
		preg_match_all($pattern, $array[$field], $matches);

		$ok = 0;
		$error = 0;
		$i = 0;
		foreach ($matches as $match){

			foreach($match as $m){
				$i++;
				if($m == $sepStart.$unsubLink.$sepEnd){////leiratkozo link

					$ok++;

				}elseif($m == $sepStart.$actLink.$sepEnd){////ha aktivációs link

					$ok++;

				}elseif($m == $sepStart.$modLink.$sepEnd){////ha adatmódosító link

					$ok++;

				}else{/////ha sime mező hivatkozás

					$ref = string::processFieldTag($m, $startSepSize, $endSepSize, $fldUresSep, $fldEmpSep);


					$reference = $ref['reference'];
					$empty = $ref['empty'];
					if(in_array($reference,$fields )){
						$ok++;
					}else{
						$error++;
					}

				}
			}
		}
			

		if ($error > 0 && $i > 0)  {
			$array->add_error($field, 'invalid_field_found');
		}

	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}