<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Formoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "forms_overview";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function index()	{
		
		if(isset($_SESSION['selected_form'])){
			unset($_SESSION['selected_form']);
		}		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->listDatamodForm = $this->list->datamod_form_id;
		$this->pagetemplate->checkUnsubProcess = $this->list->checkUnsubProcess();
		$this->pagetemplate->checkSubProcess = $this->list->checkSubProcess();
		
		$this->pagetemplate->forms = $this->list->forms();
		$this->pagetemplate->membersSum = $this->list->membersNumber();
		
		
		$this->render();
	}

	
	public function selectform(){
		$gId = $this->uri->segment(4,"");

		if($gId == ""){
			url::redirect("/pages/formoverview");
		}else{
			
			$g = ORM::factory("omm_list_form")->find($gId);
			
			if($g->loaded){
				$_SESSION['selected_form'] = $gId;
				url::redirect("/pages/formdetail");	
			}else{
				url::redirect("/pages/formoverview");	
			}
		}
	}
	
	public function deleteform(){
		$gId = $this->uri->segment(4,"");

		if($gId == ""){
			url::redirect("/pages/formoverview");
		}else{
			
			$g = ORM::factory("omm_list_form")->find($gId);
			
			if($g->loaded && $g->omm_list_id == $_SESSION['selected_list']->id){
				$g->deleteForm();
				url::redirect("/pages/formoverview");	
			}else{
				url::redirect("/pages/formoverview");	
			}
		}
	}
	
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}