<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Groupmembers_Controller extends Ommpage_Controller {
	
	var $_pageview = "groups_members";
	var $_pagetemplate;
	var $client;
	var $list;
	var $group;
	var $rowperpage = 30;
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		$this->template->bodyClass = "subscriberlist";
	}
	
	public function index($groupid)	{
		
		$group = ORM::factory("omm_list_group")->find($groupid);
		
		if($group->loaded){
			$this->group = $group;
			
			$this->list = ORM::factory("omm_list")->find($group->omm_list_id);
			
		}else{
			url::redirect("/pages/groupoverview");
		}
		
		$memberStatus = "active";
		$orderby = $this->uri->segment(5,"reg_date");
		$order = $this->uri->segment(6,"desc");		//
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->group = $this->group;
		$this->pagetemplate->list = $this->list;
		
		$this->pagetemplate->alert = "";
		
		
		
		$this->pagetemplate->membersSum = $this->group->getMemberCount($this->client->getAllFields($this->list->id));
		
		$this->pagetemplate->membercount = $this->pagetemplate->membersSum;
		
		$this->pagetemplate->orderby = $orderby;
		$this->pagetemplate->order = $order;

		
			if(isset($_GET['page']))
				$page = $_GET['page'];
			else
				$page = 1;		
		
			if($page == 1){
				$offset = 0;
			}else{
				$page = $page-1;
				$offset = $page*$this->rowperpage;
			}
				
		
		
			$pages = Pagination::factory(array
			(
			    'items_per_page' => $this->rowperpage,
			    'query_string' => 'page',
			    'total_items' => $this->pagetemplate->membersSum
			));			
		
		if($this->pagetemplate->membersSum > $this->rowperpage)	{
			$this->pagetemplate->pagination = $pages->render("ommpagination");	
		}else{
			$this->pagetemplate->pagination = "";
		}
		

		$fields = $this->client->gridFields();
		
		$this->pagetemplate->gridFields = $fields;//
		
		$allfields = $this->client->getAllFields($this->list->id);
		$this->pagetemplate->members = $this->group->getMembers($allfields,$memberStatus,$orderby,$order,$offset,$this->rowperpage,true, "all" , "all", "all", "none", "none",$allfields);	//
		
		$this->render();
	}

	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}