<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Lettertemplates_Controller extends Ommpage_Controller {

	var $_pageview = "lettertemplates_overview";
	var $_pageview_detail = "lettertemplates_add";
	var $_pagetemplate;
	var $client;

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();

		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}
		$this->template->clientname = $this->client->name;
		$this->template->bodyClass = "clientdashboard";
	}

	//TODO a módosítást megcsinálni
	public function index()	{
		url::redirect("/lettertemplates/overview");
	}

	public function selecttemplate($id){
		$template = ORM::factory('omm_letter_template')->where("omm_client_id", $this->client->id)->find($id);
		
		if($template->loaded){
			$_SESSION['selected_template'] = $template;
			url::redirect('pages/lettertemplates/add');
		}else{
			url::redirect('pages/lettertemplates/overview');
		}
		
	}

	public function deletetemplate($id){
		$template = ORM::factory('omm_letter_template')->where("omm_client_id", $this->client->id)->find($id);
		
		if($template->loaded){
			$template->delete();
			meta::createAlert("succes","Sikeres törlés!","A sablon törölve lett!");
			url::redirect('pages/lettertemplates/overview');
		}else{
			url::redirect('pages/lettertemplates/overview');
		}
		
		
		
	}
	
	public function overview()	{

		$this->pageview = $this->_pageview;
		$this->init();

		if(isset($_SESSION['selected_template'])){
			unset($_SESSION['selected_template']);
		}
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);
		}else{
			$this->pagetemplate->alert = ""; 
		}		
		
		$this->pagetemplate->errors = ""; 

		$templates = ORM::factory('omm_letter_template')->where("omm_client_id", $this->client->id)->find_all();
			

		$this->pagetemplate->templates = $templates;
			
			
			
		$this->render();
	}

	public function add()	{

		$this->pageview = $this->_pageview_detail;
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['htmlerror'])){
			$this->pagetemplate->htmlerror = $_SESSION['htmlerror'];
			unset($_SESSION['htmlerror']);
		}else{
			$this->pagetemplate->htmlerror = ""; //clearfixError
		}

		if(isset($_SESSION['errors'])){
			$this->pagetemplate->errors = $_SESSION['errors'];
			unset($_SESSION['errors']);
		}else{
			$this->pagetemplate->errors = ""; //clearfixError
		}

		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);
		}else{
			$this->pagetemplate->alert = ""; //clearfixError
		}		
		
		//$templates = ORM::factory('omm_letter_template')->where("omm_client_id", $this->client->id)->find_all();
			
		$form = array(
	        'name'      	=> '',
	        'thumbnail'   => '',
	        'html'  	=> ''
	        );

	        $errors = $form;
	        $classes = $form;


	        if(isset($_SESSION['selected_template'])){
	        	$this->pagetemplate->mod = "mod";
	        	$template = $_SESSION['selected_template'];

	        	$form = array(
		        'name'      	=> $template->name,
	        	'thumbnail'   => '',
	        	'html'  	=> ''			
	        	);
	        		
	        }else{/////új felvitel
	        	$this->pagetemplate->mod = "";
	        	$template = ORM::factory("omm_letter_template");
	        }

	        if ($_POST){
	        	// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
	        	$post = new Validation($_POST);

	        	//  Add some filters
	        	$post->pre_filter('trim', TRUE);
	        		
	        	// Add some rules, the input field, followed by a list of checks, carried out in order
	        	$post->add_rules('name','required', 'length[0,255]');
	        	//$post->add_rules('html', 'required');

	        	// Test to see if things passed the rule checks
	        	if ($post->validate())  {
	        		// Yes! everything is valid
	        		//$this->pagetemplate->errors = 'Form validated and submitted correctly. <br />';
	        		$template->omm_client_id = $this->client->id;
	        		$template->name = $post->name;	        		 
	        		$template->saveObject();

	        		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
		        	$_errors = array();
	        		
	        		if (upload::valid($_FILES['html']) && upload::type($_FILES['html'],array('html'))){
	        			$filename = upload::save('html');

	        			
	        			
	        			$content = fileutils::file_get_contents_utf8($filename);
	        				
	        				
	        				
	        			if((bool)$template->trySetHtml($content)){

	        				$template->saveObject();
	        					
	        			}else{
							if($this->pagetemplate->mod != "mod"){
		        				$_errors[] = 'Hibás html fájl! Nincs body tag!';
							}
	        			}

	        			if($filename != ""){
	        				unlink($filename);
	        			}else{
	        				if($this->pagetemplate->mod != "mod"){
								$_errors[] = 'Hibás html fájl!';
	        				}	        				
	        			}

	        				
	        				
	        		}else{
	        			if($this->pagetemplate->mod != "mod"){
							$_errors[] = 'Hibás html fájl!';
	        			}	        				
	        		}
	        		 
	        		
	        		if(isset($_FILES['thumbnail']) && $_FILES['thumbnail'] != ""){
						
	        			echo $_FILES['thumbnail']."___";
	        			print_r($_FILES['thumbnail']);
	        			
	        			if (upload::valid($_FILES['thumbnail']) && upload::type($_FILES['thumbnail'],array('jpg','jpeg'))){///thumbnail
	        				
	        				$_fname = string::random_string().".jpg";
	        				$filename = upload::save('thumbnail', $_fname , './files');
	        				
	        				if(file_exists($filename)){
		        				$image = new Image($filename);
		        				$image->resize(100, 100, Image::HEIGHT);
		        				$image->crop(100, 100);
		        				
		        				$image->save();
		        				
		        				$template->thumbnail = $_fname;	        		 
		        				$template->saveObject();	        				
	        				}
	        				
	        				
	        				
	        				
		        		}else{
		        			$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
		        			$_errors[] = 'Hibás thumbnail fájl! Csak jpg és jpeg lehet!';
		
		        		}
	        		
	        		}
	        		

	        		if(sizeof($_errors) > 0){
	        			$errorTempl->errors = $_errors;
	        			$_SESSION['htmlerror'] = $errorTempl->render(FALSE,FALSE);
		        		url::redirect('pages/lettertemplates/add');	        		
	        		}
		        	
	        		 
	        		if($post->mod == "mod"){
	        			meta::createAlert("succes","Sikeres módosítás!","A sablon adatainak módosítása sikeres volt.");
	        		}else{
	        			meta::createAlert("succes","Sikeres hozzáadás!","Az új sablon sikeresen hozzá lett adva.");
	        		}
					
	        		if(isset($_SESSION['fromletter']) && $_SESSION['fromletter']){
	        			unset($_SESSION['fromletter']);
	        			unset($_SESSION['alert']);
	        			url::redirect('pages/letterdetail/second');
	        		}else{
						url::redirect('pages/lettertemplates/overview');
	        		}
	        		

	        		 
	        		 
	        	} // No! We have validation errors, we need to show the form again, with the errors
	        	else {
	        		// repopulate the form fields
	        		$form = arr::overwrite($form, $post->as_array());

	        		// populate the error fields, if any
	        		// We need to already have created an error message file, for Kohana to use
	        		// Pass the error message file name to the errors() method

	        		$errors = arr::overwrite($errors, $post->errors('form_errors_lettertemplates'));

	        		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
	        		$errorTempl->errors = $errors;

	        		$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

	        		foreach ($errors as $key => $error){
	        			if($error != ""){
	        				$classes[$key] = "clearfixError";
	        			}
	        		}



	        	}
	        }



	        $this->pagetemplate->template = $template;

	        $this->pagetemplate->classes = $classes;
	        $this->pagetemplate->form = $form;
	        	
	        	
	        $this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}