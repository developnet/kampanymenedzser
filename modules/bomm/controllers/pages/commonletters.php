<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Commonletters_Controller extends Ommpage_Controller {
	
	var $_pageview = "common_letters";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		$this->template->bodyClass = "listindex";
	}
	
	public function index($letterId = 0)	{
		

		$form = array(
	        'name'      	=> '',
	        'sender_name'  	=> '',
	        'sender_email'  => '',
			'subject'  		=> '',
			'html_content'  => '',
			'text_content'  => '',
			'note' => ''
	    );
		
		$errors = $form;
		$classes = $form;

		$letter = ORM::factory("omm_common_letter")->find($letterId);
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;		
		
		if($letter->loaded){
			$form = array(
		        'name'      	=> $letter->name,
		        'sender_name'  	=> $letter->sender_name,
		        'sender_email'  => $letter->sender_email,
				'subject'  		=> $letter->subject,
				'html_content'  => $letter->html_content,
				'text_content'  => $letter->text_content,
				'note'  => $letter->note
		    );			 
			$this->pagetemplate->selectedletter = $letter;
		}else{/////új felvitel
			$letter = ORM::factory("omm_common_letter");
		}		

		
		

		
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->form = $form;
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->errors = $errors;
		
		$this->pagetemplate->letters = ORM::factory("omm_common_letter")->where('status','active')->orderby('name','asc')->find_all();
		$this->pagetemplate->templates = $this->client->tempaltes();
		$this->pagetemplate->lists = ORM::factory("omm_list")->where("omm_client_id",$this->client->id)->find_all();
		
		$this->render();
	}
	
	public function duplicate(){
			$letterId = $this->uri->segment(4,"");
		$this->template->clientname = $this->client->name;
		if($letterId == ""){
			url::redirect("/pages/commonletters");
		}else{
			
			$c = ORM::factory("omm_common_letter")->find($letterId);
			
			if($c->loaded){
				
				$_SESSION['selected_common_letter'] = $c->duplicate();
				url::redirect("/pages/commonletters");	
				
			}else{
				url::redirect("/pages/commonletters");	
			}
		}	
	}
	
	public function newLetter(){
		if(isset($_SESSION['selected_common_letter'])){
			unset($_SESSION['selected_common_letter']);
		}
		url::redirect("/pages/commonletters");
	}
	
	public function select(){
		$letterId = $this->uri->segment(4,"");
		url::redirect("/pages/commonletters/index/".$letterId);	
	}

	public function deleteLetter(){
		$letterId = $this->uri->segment(4,"");
		$this->template->clientname = $this->client->name;
		if($letterId == ""){
			url::redirect("/pages/commonletters");
		}else{
			
			$c = ORM::factory("omm_common_letter")->find($letterId);
			
			if($c->loaded){
				$c->deleteLetter();
				if(isset($_SESSION['selected_common_letter'])){
					unset($_SESSION['selected_common_letter']);
				}				
				url::redirect("/pages/commonletters");	
				
			}else{
				url::redirect("/pages/commonletters");	
			}
		}
	}		
		
	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}