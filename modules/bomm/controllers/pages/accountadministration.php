<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Accountadministration_Controller extends Ommpage_Controller {

	var $_pageview = "account_administration";
	var $_pageview_actmonth = "account_actmonth";
	var $_pageview_prevmonth = "account_prevmonth";
	var $_pageview_billing = "account_billing";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "main";
		parent::__construct();
		$this->template->bodyClass = "clientdashboard";

		if (!$this->auth->logged_in(Admin_Controller::$superadmin_role)){
			url::redirect('/');
		}

	}

	public function index()	{

		$this->pageview = $this->_pageview;
		$this->init();

		if(isset($_GET['km_user'])){
			$km_user = $_GET['km_user'];
		}else{
			$km_user = USERNAME;
		}

		if(!isset($_GET['showing'])){
			url::redirect("/pages/accountadministration/index?showing=month&km_user=".$km_user);
		}elseif($_GET['showing'] != "all" && $_GET['showing'] != "year" && $_GET['showing'] != "month" && $_GET['showing'] != "week" && $_GET['showing'] != "day"){
			url::redirect("/pages/accountadministration/index?showing=month&km_user=".$km_user);
		}


		$showing = $_GET['showing'];


		switch ($showing){

			case "all": $defparam = "all";break;
			case "year": $defparam = date("Y");break;
			case "month": $defparam = date("Y-m");break;
			case "week": {
				$defparam = dateutils::nrweekday(date("Y-m-d"));
				$defparam = date( "Y-m-d", strtotime( $defparam." -1 week" ));
				break;
			}
			case "day": $defparam = date("Y-m-d");break;


		}


		if(!isset($_GET['param'])){
			url::redirect("/pages/accountadministration/index?showing=".$showing."&param=".$defparam."&km_user=".$km_user);
		}

		$param = $_GET['param'];

		$this->pagetemplate->showing = $showing;
		$this->pagetemplate->param = $param;




		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];

		$db = new Database("");
		$res = $db->from("users")->where('status','1')->where('km_user !=','admin')->groupby('km_user')->get();



		$this->pagetemplate->km_user = $km_user;
		$this->pagetemplate->km_users = $res;


		$sql = "SELECT sum(count) as ossz FROM omm_sendcounts WHERE km_user='".$km_user."'";

		$subscribed = $db->query($sql);
		$ossz = $subscribed[0]->ossz;
		$this->pagetemplate->ossz = $ossz;

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}