<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Clientoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "clients_overview";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "main";
		parent::__construct();
		$this->template->bodyClass = "clientdashboard";
	}
	
	public function index()	{
		//throw new Kohana_Exception("kmerrors.akarmi");
		if(isset($_SESSION['selected_client'])){
			unset($_SESSION['selected_client']);
		}
		
		if(isset($_SESSION['selected_template'])){
			unset($_SESSION['selected_template']);
		}		
		
		$this->pageview = $this->_pageview; 
		$this->init();
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		
		
		
		//TODO Majd ha jogosultság kezelés lesz akkor kell máshogy szűrni
		//$this->pagetemplate->clients = ORM::factory("omm_client")->where('user_id', $_SESSION['auth_user']->id)->orderby("name","asc")->find_all();
		
		$this->pagetemplate->clients = ORM::factory("omm_client")->where('status', 'active')->orderby("name","asc")->find_all();
		
		$this->render();
	}

	public function selectclient(){
		$clientId = $this->uri->segment(4,"");

		if($clientId == ""){
			url::redirect("/pages/clientoverview");
		}else{
			
			$client = ORM::factory("omm_client")->find($clientId);
			
			if($client->loaded){
				$_SESSION['selected_client'] = $client;
				
				$accid = meta::getAccId(); 
				
				fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$client->id);
				fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$client->id.'/images');
				fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$client->id.'/images/_thumbs');
				fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$client->id.'/files');
				fileutils::makedir(DOCROOT.'files/'.$accid.'/'.$client->id.'/media');
				
				$_SESSION['client_id'] = $client->id;
				
				url::redirect("/pages/subscribers");	
				
			}else{
				url::redirect("/pages/clientoverview");	
			}
			
		}
		
	}
	
	
	public function deleteClient(){
			$clientId = $this->uri->segment(4,"");

		if($clientId == ""){
			url::redirect("/pages/clientoverview");
		}else{
			
			$client = ORM::factory("omm_client")->find($clientId);
			
			if($client->loaded){

				$client->deleteClient();
				
				url::redirect("/pages/clientoverview");
			}else{
				url::redirect("/pages/clientoverview");	
			}
			
		}
	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}