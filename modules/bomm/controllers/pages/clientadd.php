<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Clientadd_Controller extends Ommpage_Controller {
	
	var $_pageview = "clients_add";
	var $_pagetemplate;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		$this->template->bodyClass = "clientdashboard";
	}	
	
	public function index()	{
		$this->template->clientname = "";
		$mod = $this->uri->segment(4,"");
		
		if($mod == "mod"){
			$this->maintemplate = "main_withmenu";
		}
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError
		
		$this->pagetemplate->mod = $mod;
		
		 if(isset($_SERVER['HTTP_REFERER']) && $mod == "mod"){
	     		$this->pagetemplate->cancelLink = $_SERVER['HTTP_REFERER'];
	       	}else{
	       		$this->pagetemplate->cancelLink = url::base()."pages/clientoverview";	
	       	}		
		///átmenetileg ezt most felülírom...felkerült a tabmenü szal nem para
	    $this->pagetemplate->cancelLink = url::base()."pages/clientoverview";	
	       	
		//$this->pagetemplate->clients = ORM::factory("omm_client")->orderby("name","asc")->find_all();
		
		
		$form = array(
	        'name'      	=> '',
	        'site_domain'   => '',
	        'sender_name'  	=> '',
	        'sender_email'  => '',
			'sender_replyto'  => '',
			'note' => ''
	    );
		
		$errors = $form;
		$classes = $form;
		
		
		if(isset($_SESSION['selected_client'])){
			$client = $_SESSION['selected_client'];
			$this->template->clientname = $client->name;
			$form = array(
		        'name'      	=> $client->name,
		        'site_domain'   => $client->site_domain,
		        'sender_name'  	=> $client->sender_name,
		        'sender_email'  => $client->sender_email,
				'sender_replyto'  => $client->sender_replyto,
				'note'  => $client->note
		    );			
			
		}else{/////új felvitel
			$clients = ORM::factory("omm_client")->where('status', 'active')->orderby("name","asc")->count_all();
			if($clients >= KOHANA::config('core.max_client_per_user')){/////limit check
				url::redirect($this->pagetemplate->cancelLink);
			}			
			
			$client = ORM::factory("omm_client");
		}
		
		
		
    if ($_POST)
    {
         // Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
        $post = new Validation($_POST);
 
         //  Add some filters
        $post->pre_filter('trim', TRUE);
        //$post->pre_filter('ucfirst', 'name');
 		$post->pre_filter('ucfirst', 'sender_name');
 		
        // Add some rules, the input field, followed by a list of checks, carried out in order
        $post->add_rules('name','required', 'length[0,255]');
        $post->add_rules('site_domain', 'required');
        $post->add_rules('sender_name', 'length[0,255]','required');
        $post->add_rules('sender_email', 'required','email');
        $post->add_rules('sender_replyto', 'email');
 		//$post->add_callbacks('sender_email', array($this, '_domain_email'));	
        //$post->add_callbacks('site_domain', array($this, '_unique_domain'));	
 
        // Test to see if things passed the rule checks
        if ($post->validate())  {
            // Yes! everything is valid
           $this->pagetemplate->errors = 'Form validated and submitted correctly. <br />';
           
           
           $client->user_id = $_SESSION['auth_user']->id;
           $client->name = $post->name;
           $client->site_domain = $post->site_domain;
           $client->sender_name = $post->sender_name;
           $client->sender_email = $post->sender_email;
           $client->sender_replyto = $post->sender_replyto;
           $client->note = $post->note;
           
           $client->saveObject();

           
           	if($post->mod == "mod"){
           		meta::createAlert("succes","Sikeres módosítás!","A honlap adatainak módosítása sikeres volt.");
           	}else{
           		meta::createAlert("succes","Sikeres hozzáadás!","Az új honlap sikeresen hozzá lett adva.");
           	}

	    	
	    	
           		
	       url::redirect($post->referer);
           	
           		
           
        } // No! We have validation errors, we need to show the form again, with the errors
        else {
            // repopulate the form fields
            $form = arr::overwrite($form, $post->as_array());
 
            // populate the error fields, if any
    		// We need to already have created an error message file, for Kohana to use
            // Pass the error message file name to the errors() method
            
            $errors = arr::overwrite($errors, $post->errors('form_errors_client'));
            
            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
            $errorTempl->errors = $errors;
            
            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
            
            foreach ($errors as $key => $error){
            	if($error != ""){
            		$classes[$key] = "clearfixError";
            	}
            }
            
            
            $this->pagetemplate->cancelLink = $post->referer;
            
        }
    }
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;
		$this->render();
	}


	public function _unique_domain(Validation $array, $field){
	   
			
			$domain_exists = (bool) ORM::factory('omm_client')->where('status','active')->where('site_domain', $array[$field])->count_all();
	 
		   if ($domain_exists)  {
		       $array->add_error($field, 'domain_exists');
		   }else{
		   		
		   $pos = strrpos($array[$field], "http://");
			if ($pos === false) { // note: three equal signs
			    
				$pieces = explode(".", $array[$field]);
				
				if($pieces[0] == "www"){
				 	$array->add_error($field, 'invalid_domain_http');
				}else{
				
					if(!string::checkDomain($array[$field])){
						$array->add_error($field, 'invalid_domain');
					}
				
				}
				
				
				
			    
			}else{
			   $array->add_error($field, 'invalid_domain_http');
			}
		   		
		   	
		   }			
		
	}

	public function _domain_email(Validation $array, $field){
	   
			$domain = preg_split("[@]",$array['sender_email']);

			if ($domain[1] != $array['site_domain'])  {
		       $array->add_error($field, 'invalid_domain_email');
		   }			
		
	}	
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}