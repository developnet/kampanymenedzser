<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Statisticoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "statistic_overview";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		$this->template->bodyClass = "listindex";
	}
	
	public function index()	{
		

		if(isset($_SESSION['selected_list'])){
			unset($_SESSION['selected_list']);
		}
		
		if(isset($_SESSION['selected_letter'])){
			unset($_SESSION['selected_letter']);
		}
		if(isset($_SESSION['selected_member'])){
			unset($_SESSION['selected_member']);
		}		
		
		if(isset($_SESSION['selected_form'])){
			unset($_SESSION['selected_form']);
		}			
		
		if(isset($_SESSION['selected_group'])){
			unset($_SESSION['selected_group']);
		}			
		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		$this->pagetemplate->alert = "";
		
		
		$this->pagetemplate->campaigns = $this->client->campaigns();
		$this->pagetemplate->archived_campaigns = $this->client->campaigns('archived');
		
		$this->render();
	}

	
	public function selectcampaign(){
		$campId = $this->uri->segment(4,"");

		if($campId == ""){
			url::redirect("/pages/statisticoverview");
		}else{
			
			$c = ORM::factory("omm_campaign")->find($campId);
			
			if($c->loaded){
				$_SESSION['selected_campaign'] = $c;
				url::redirect("/pages/statisticoverview");	
				
			}else{
				url::redirect("/pages/statisticoverview");	
			}
		}
	}


	
	public function deselect(){
		$campId = $this->uri->segment(4,"");

		if($campId == ""){
			url::redirect("/pages/statisticoverview");
		}else{

			if(isset($_SESSION['selected_campaign']) && $campId == $_SESSION['selected_campaign']){
				unset($_SESSION['selected_campaign']);
				url::redirect("/pages/statisticoverview");
			}
			
		}
	}	
	
	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}