<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listunsubscribeprocess_Controller extends Ommpage_Controller {
	
	var $_pageview = "lists_unsubscribeprocess";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}

		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
//	public function generateTestData(){
//		
//		for ($i=0;$i<999;$i++){
//		$this->list->addTestMember();	
//		}
//		
//		
//		
//	}
	
	public function index()	{
		
		
		
		$memberStatus = $this->uri->segment(4,"active");
		$orderby = $this->uri->segment(5,"reg_date");
		$order = $this->uri->segment(6,"desc");
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError		
		
		
		
		$form = array(
			'unsubscribe_landing_page'					=> "",
			'second_unsubscribe_page'					=> "",
			'error_unsubscribe_page'					=> ""
	    );
		
		$errors  = $form;
		$classes = $form;

		 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim');
		        
		        
		        $post->add_rules('unsubscribe_landing_page','required','url');
		        $post->add_rules('second_unsubscribe_page','required','url');
		        $post->add_rules('error_unsubscribe_page','required','url');
	 
	        ///////////////////////////////MENTÉS
	        if ($post->validate())  {
					
				$this->list->unsubscribe_landing_page = $post->unsubscribe_landing_page;   
				$this->list->second_unsubscribe_page = $post->second_unsubscribe_page;       	
		        $this->list->error_unsubscribe_page = $post->error_unsubscribe_page;   
				$this->list->saveObject();
		        
		        
				unset($_POST);
				
				meta::createAlert("succes","A leiratkozási folyamat sikeresen elmentve!","");
				url::redirect("/pages/listdetail");
	           
	        } // HIBA
	        else {
	            $form = arr::overwrite($form, $post->as_array());
	 
	            $errors = arr::overwrite($errors, $post->errors('form_errors_listsubscribe'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }else{////////////////////////ha nincs post
	    	
		    if($this->list->unsubscribe_landing_page != NULL){
				$form['unsubscribe_landing_page'] = $this->list->unsubscribe_landing_page;
				$form['second_unsubscribe_page'] = $this->list->second_unsubscribe_page;
				$form['error_unsubscribe_page'] = $this->list->error_unsubscribe_page;
			}
	
	    }
	    	

	    	
	    $this->pagetemplate->form = $form;
	    $this->pagetemplate->classes = $classes;
	    $this->pagetemplate->listName = $this->list->name;
		
 		$this->pagetemplate->lists = ORM::factory("omm_list")->where("omm_client_id",$this->client->id)->find_all();
		
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		KOHANA::show_404(FALSE,FALSE);
	}

}