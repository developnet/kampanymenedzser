<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Campaigndetail_Controller extends Ommpage_Controller {
	
	var $_pageview = "campaigns_detail";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function _unique_email(Validation $array, $field){
	   
		if($this->mo != null && $this->mo->email != $array[$field]){
			$email_exists = (bool) ORM::factory('omm_list_member')->where('omm_list_id',$this->list->id)->where('email', $array[$field])->count_all();
	 
		   if ($email_exists)  {
		       $array->add_error($field, 'email_exists');
		   }			
		}elseif($this->mo == null){
			
			$email_exists = (bool) ORM::factory('omm_list_member')->where('omm_list_id',$this->list->id)->where('email', $array[$field])->count_all();
	 
		   if ($email_exists)  {
		       $array->add_error($field, 'email_exists');
		   }
			
		}
	}
	
	
	public function index()	{
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		

		$this->pagetemplate->errors = ""; //clearfixError
		
		
		
		if(isset($_SESSION['selected_campaign'])){
			$camp = $_SESSION['selected_campaign'];
			$this->pagetemplate->name = $camp->name;	
		}else{
			$camp = ORM::factory("omm_campaign");
			$this->pagetemplate->name = "Új kampány";
		}		
		
		
        if($camp->loaded){

			$form = array(
		        'name'  		=> $camp->name,
				'note'  		=> $camp->note,
				'status'  		=> $camp->status,
		        'sender_name'  	=> $camp->sender_name,
		        'sender_email'	=> $camp->sender_email,
				'sender_replyto'=> $camp->sender_replyto
		    );        	
        	
        }else{
        	
			$form = array(
		        'name'  		=> "",
				'note'  		=> "",
				'status'  		=> "active",
		        'sender_name'  	=> $this->client->sender_name,
		        'sender_email'	=> $this->client->sender_email,
				'sender_replyto'=> $this->client->sender_replyto
		    );         	
        	
        }

		
		$errors = array(
		        'name'  		=> "",
		 		'note'  		=> "",
		        'status' 	 	=> "",
		        'sender_name'  	=> "",
		        'sender_email'	=> "",
				'sender_replyto'=> ""
	    );
	    
		$classes = $errors;
		
		
		
    if ($_POST)
    {
         // Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
        $post = new Validation($_POST);
 
         //  Add some filters
        $post->pre_filter('trim', TRUE);
 		
        // Add some rules, the input field, followed by a list of checks, carried out in order
        $post->add_rules('name','required');
        $post->add_rules('status','required');
        $post->add_rules('sender_name','required');
        $post->add_rules('sender_email','required', 'email');
        $post->add_rules('sender_replyto', 'email');
        
    	//$post->add_callbacks('sender_email', array($this, '_domain_email'));

 
        // Test to see if things passed the rule checks
        if ($post->validate())  {
			
        	if(!$camp->loaded){
        		$camp->omm_client_id = $this->client->id; 
        		$camp->created_date = date("Y-m-d H:i:s");	
        	}

        	$camp->status = $post->status;	
        	$camp->name = $post->name;
        	
        	$camp->note = $post->note;
        	
        	if(empty($post->note))
        		$camp->note = null;
        	
        	$camp->sender_name = $post->sender_name;
        	$camp->sender_email = $post->sender_email;
        	$camp->sender_replyto = $post->sender_replyto; 

        	$camp->saveObject();
        	
        	unset($_POST);
        	
        	$_SESSION['selected_campaign'] = $camp;
        	
        	url::redirect("/pages/campaignoverview");
        	
           
        } // No! We have validation errors, we need to show the form again, with the errors
        else {
            // repopulate the form fields
            $form = arr::overwrite($form, $post->as_array());
 
            // populate the error fields, if any
    		// We need to already have created an error message file, for Kohana to use
            // Pass the error message file name to the errors() method
            
            $errors = arr::overwrite($errors, $post->errors('form_errors_campaign'));
            
            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
            $errorTempl->errors = $errors;
            
            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
            
            foreach ($errors as $key => $error){
            	if($error != ""){
            		$classes[$key] = "clearfixError";
            	}
            }
            
        }
    }
    
	$this->pagetemplate->statuses = meta::getCodeDict(6);
    $this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;
		$this->render();
	}

	public function _domain_email(Validation $array, $field){
	   
			$domain = preg_split("[@]",$array['sender_email']);

			if ($domain[1] != $this->client->site_domain)  {
		       $array->add_error($field, 'invalid_domain_email');
		   }			
		
	}


	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}