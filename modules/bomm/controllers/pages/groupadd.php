<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Groupadd_Controller extends Ommpage_Controller {
	
	var $_pageview_1 = "groups_add_1";
	var $_pageview_2 = "groups_add_2";
	var $_pageview_3 = "groups_add_3";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		
		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}			
		
		$this->template->bodyClass = "listindex";
	}
	
	public function index()	{
		url::redirect("/pages/groupadd/first");
	}


	public function first($groupid = 0)	{
		
		$group = ORM::factory("omm_list_group")->find($groupid);
		
		if($group->loaded){
			$this->group = $group;
			
			$this->list = ORM::factory("omm_list")->find($group->omm_list_id);
			$_SESSION['selected_group'] = $group;
			
		}else{
			$group = ORM::factory("omm_list_group");
		}		
		
		
		
		$this->template->bodyClass = "subscriberlist";
		$this->pageview = $this->_pageview_1; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError		
		
		$form = array(
	        'name'      	=> $group->name,
			'note'			=> $group->note
	    );
		
		$errors = array(
	        'name'      	=> "",
			'note' 			=> ""
	    );
	    
		$classes = $form;
		
		
		 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim', TRUE);
		        //$post->pre_filter('ucfirst', 'name');
		 		
		        $post->add_rules('name','required', 'length[0,250]');
	 
	 
	        // Test to see if things passed the rule checks
	        if ($post->validate())  {
				
	        	if(!$group->loaded){
	        		$group->omm_list_id = $this->list->id;
	        		$group->code = $list->code = string::random_string('unique');
	        	}
	
	        		$group->note = $post->note;

	        		if(empty($post->note))
	        			$group->note = null;
	        		
	        		$group->name = $post->name;
	        		
	        	$group->saveObject();
	        	
	        	if(isset($_SESSION['selected_group'])){
		        	url::redirect("/pages/groupdetail");	        			        		
	        	}else{
		        	$_SESSION['selected_group'] = $group;
		        	url::redirect("/pages/groupadd/second/".$group->id);	        		
	        	}

	           
	        } // No! We have validation errors, we need to show the form again, with the errors
	        else {
	            // repopulate the form fields
	            $form = arr::overwrite($form, $post->as_array());
	 
	            // populate the error fields, if any
	    		// We need to already have created an error message file, for Kohana to use
	            // Pass the error message file name to the errors() method
	            
	            $errors = arr::overwrite($errors, $post->errors('form_errors_groups'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }		
		
		$this->pagetemplate->listTypes = meta::getCodeDict(1);
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;		
		$this->render();
	}
	
	
	public function second($groupid){
		
		$group = ORM::factory("omm_list_group")->find($groupid);
		
		if($group->loaded){
			$this->group = $group;
			$_SESSION['selected_group'] = $group;
			$this->list = ORM::factory("omm_list")->find($group->omm_list_id);
			
		}else{
			url::redirect("/pages/groupoverview");
		}		
		
		$this->pageview = $this->_pageview_2; 
		$this->init();
				
		$this->template->bodyClass = "subscriberlist";
		
		if(isset($_SESSION['selected_group'])){
			$group = $_SESSION['selected_group'];	
		}else{
			url::redirect("/pages/groupoverview");
		}
		
		$fields = $this->client->getAllFields($this->list->id);
		
		$this->template->clientname = $this->client->name;
		
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError
		
		$form = array(
	        'name' => $group->name,
	    );

	    $errors['name'] = "";
		
	    $condCount = 0;
		
	    $this->pagetemplate->group = $group;
		$condlevels = $group->getConditionLevels();
		foreach($condlevels as $lev){
				$level = $lev->level;
	    		$emailConds = $group->getFixedConditions("email",$level);
	    		$regdConds = $group->getFixedConditions("reg_date",$level);		
		
			    foreach ($emailConds as $c){
				       	$form['conditionType_'.$c->id] = $c->type; 
				       	$form['conditionValue_'.$c->id] = $c->value; 
				       	$errors['conditionValue_'.$c->id] = "";	    	
			    }
			    
				foreach ($regdConds as $c){
				       	$form['conditionType_'.$c->id] = $c->type; 
				       	$form['conditionValue_'.$c->id] = $c->value; 
				       	$errors['conditionValue_'.$c->id] = "";	    	
			    }
			    	    
			    foreach ($fields as $f){
				       	$conditions = $f->getConditions($group->id,$level);
				       	if(sizeof($conditions)>0){
				       		foreach ($conditions as $c){
									
				       			$form['conditionType_'.$c->id] = $c->type; 
				       			$form['conditionValue_'.$c->id] = $c->value;
				       			$form['second_conditionValue_'.$c->id] = $c->value; 
				       			//$post->add_rules('conditionnValue_'.$c->id,'required');
				        			
				       			$errors['conditionValue_'.$c->id] = "";
				        		$errors['second_conditionValue_'.$c->id] = "";
				        			
				       			$condCount++;
				        					        			
				      		}
				   		}
				  }		    		
	    		
		} 
	    
	    
	    
	    //$this->pagetemplate->emailConds = $emailConds;
	    //$this->pagetemplate->regdConds = $regdConds;
	    
    
	    
			$classes = $errors;
		
		
			 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim', TRUE);
		        $post->pre_filter('ucfirst', 'name');
		 		
		        $post->add_rules('name','required', 'length[0,250]');

		        
		        foreach($condlevels as $lev){
					$level = $lev->level;
		    		$emailConds = $group->getFixedConditions("email",$level);
		    		$regdConds = $group->getFixedConditions("reg_date",$level);			        	

			 	 	foreach ($emailConds as $c){
				    	$post->add_rules('conditionValue_'.$c->id,'required');
			 	 	}
		    
					foreach ($regdConds as $c){
						$post->add_rules('conditionValue_'.$c->id,'required');
					}			    		

		        	foreach ($fields as $f){
			        	$conditions = $f->getConditions($group->id,$level);
			        	if(sizeof($conditions)>0){
			        		foreach ($conditions as $c){
								
			        			$post->add_rules('conditionValue_'.$c->id,'required');
			        			
			        		}
			        	}
			        }					
					
		        }		        
		        
	        
		        
		        

		        
	 
	        // Test to see if things passed the rule checks
	        if ($post->validate())  {
	
	        	
		        foreach($condlevels as $lev){
					$level = $lev->level;
		    		$emailConds = $group->getFixedConditions("email",$level);
		    		$regdConds = $group->getFixedConditions("reg_date",$level);	

		    		
	        	
		        	  foreach ($emailConds as $c){
	
		        	  	$key ='conditionValue_'.$c->id; 
			        	$tkey ='conditionType_'.$c->id;
			        			
			        	$c->value = $post->$key;	
	        			$c->type = $post->$tkey;
	        			$c->save();
		    		}
		        	
		        	 foreach ($regdConds as $c){
	
		        	  	$key ='conditionValue_'.$c->id; 
			        	$tkey ='conditionType_'.$c->id;
			        			
			        	$c->value = $post->$key;	
	        			$c->type = $post->$tkey;
	        			$c->save();
		    		}	    		
	
		        	foreach ($fields as $f){
			        	$conditions = $f->getConditions($group->id,$level);
			        	if(sizeof($conditions)>0){
			        		foreach ($conditions as $c){
								
			        			$key ='conditionValue_'.$c->id; 
			        			$tkey ='conditionType_'.$c->id;
			        			
			        			if($f->type = "multiselect" && ($post->$tkey == "checkedatleast" || $post->$tkey == "checkedless" || $post->$tkey == "checkedequal")){
			        				$key ='second_conditionValue_'.$c->id;	
			        			}
			        			
						        $c->value = $post->$key;	
			        			
			        			$c->type = $post->$tkey;
			        			$c->save();
			        		}
			        	}
			        }	        	
		        }
		        $group->name = $post->name;
		        $group->saveObject();
	 			       	
	        	unset($_POST);
	        	url::redirect("/pages/groupadd/second/".$this->group->id);
	        	
	           
	        } // No! We have validation errors, we need to show the form again, with the errors
	        else {
	            // repopulate the form fields
	            $form = arr::overwrite($form, $post->as_array());
	 
	            // populate the error fields, if any
	    		// We need to already have created an error message file, for Kohana to use
	            // Pass the error message file name to the errors() method
	            
	            $errors = arr::overwrite($errors, $post->errors('form_errors_conditions'));
	            $ee = $errors;
	            foreach ($ee as $key => $e){
	            	
	            	if(strpos  ( $e ,  "conditionValue" ) !== false ){
	            		unset($ee[$key]);
	            		$ee[$key] = "Adja meg a feltétel értékét.";
	            	}
	            	
	            }
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $ee;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }		
		
	    $this->pagetemplate->membercount = $group->getMemberCount($fields);
	    //$this->pagetemplate->membercount = 0;
		
	    $this->pagetemplate->listfields = $this->list->fields();
	    $this->pagetemplate->clientfields = $this->client->getFields();
	    
	    $this->pagetemplate->fields = $fields;
		
		$this->pagetemplate->groupConditionTypes = meta::getCodeDict(3);
		
		$this->pagetemplate->condlevels = $condlevels;
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;	
		
		$this->render();
	}

	public function delcondition($groupid){
		
		if(isset($_SESSION['selected_group'])){
			$group = $_SESSION['selected_group'];	
		}else{
			url::redirect("/pages/groupoverview/".$groupid);
		}

		$id = $_GET['condId'];
		
		$cond = ORM::factory('omm_list_group_condition',$id);
		
		if($cond->loaded && $group->loaded){
			$cond->delete();
			url::redirect("/pages/groupadd/second/".$groupid);
		}else{
			url::redirect("/pages/groupoverview/".$groupid);
		}
		
	}
	
	public function addcondition($groupid){
		
		if(isset($_SESSION['selected_group'])){
			$group = $_SESSION['selected_group'];	
		}else{
			url::redirect("/pages/groupoverview/".$groupid);
		}

		$fieldId = $_GET['conditionfield'];
		$level = $_GET['level'];
		
		$field = ORM::factory('omm_list_field',$fieldId);
		
		
		if($fieldId == "email" || $fieldId == "reg_date"){
			
			$cond = ORM::factory('omm_list_group_condition');
			$cond->omm_list_group_id = $group->id;
			$cond->omm_list_field_id = 0;
			$cond->type = "ispresent";
			$cond->value = "ispresent";
			$cond->level = $level;
			$cond->field_reference = $fieldId;
			$cond->saveObject();

			url::redirect("/pages/groupadd/second/".$groupid);
			
		}else{
			
			if($field->loaded && $group->loaded){
				
				$cond = ORM::factory('omm_list_group_condition');
				$cond->omm_list_group_id = $group->id;
				$cond->omm_list_field_id = $field->id;
				$cond->type = "ispresent";
				$cond->value = "ispresent";
				$cond->level = $level;
				$cond->field_reference = $field->reference;
				$cond->saveObject();			
				
				url::redirect("/pages/groupadd/second/".$groupid);
			}else{
				url::redirect("/pages/groupoverview/".$groupid);
			}
			
			
		}
		
		

		
	}
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}