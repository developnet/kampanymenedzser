 <?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Formdetail_Controller extends Ommpage_Controller {
	
	var $_pageview = "forms_detail";
	var $_pagetemplate;
	var $client;
	var $list;
	var $mo;
	
	var $form_templates = array('form_layout_1' => 'Cimkék a mezőktől jobbra',
								'form_layout_2' => 'Cimkék a mezők felett',
								'form_layout_3' => 'Cimkék a mezőktől balra',
								'form_layout_4' => 'Modern'
								);

	var $form_styles = array('form_style_2' => 'Szürke',
							 'form_style_3' => 'Piros',
							 'form_style_4' => 'Zöld',
							 'form_style_5' => 'Kék',
							 'form_style_6' => 'Barna',
							 'form_style_7' => 'Modern'	
							);		//szürke, piros, zöld, kék, barna
	
	var $form_button_styles = array('form_style_2' => 'Szürke',
							 'color-red' => 'Piros',
							 'color-orange' => 'Narancssárga',
							 'color-yellow' => 'Citromsárga',
							 'color-lime' => 'Zöldcitrom',
							 'color-green' => 'Zöld',	
							 'color-cyan' => 'Cián',
							 'color-blue' => 'Kék',
							 'color-purple' => 'Lila',
							 'color-pink' => 'Rózsaszín'							 
							);			



	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}
		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	
	public function index()	{
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		
		$this->pagetemplate->listName = $this->list->name;

		$this->pagetemplate->errors = ""; //clearfixError
		
		
		
		if(isset($_SESSION['selected_form'])){
			$formObj = ORM::factory("omm_list_form")->find($_SESSION['selected_form']);
			$this->pagetemplate->formName = $formObj->name;	
		}else{
			$formObj = ORM::factory("omm_list_form");
			$this->pagetemplate->formName = "Új űrlap";
		}		
		
		
        if($formObj->loaded){

			$form = array(
		        'name'  		=> $formObj->name,
				'submit_button'	=> $formObj->submit_button,
		    	'form_template' => $formObj->form_template,
				'form_style' => $formObj->form_style,
				'note' => $formObj->note,
			);        	
        	
		    
        }else{
        	
			$form = array(
		        'name'  		=> '',
				'submit_button'	=> '',
				'form_template' => '',
				'form_style' 	=> '',
				'form_button_styles' => '',
				'note' 	=> ''
		    );         	
        	
        }

		
		$errors = array(
	        'name'  		=> '',
			'submit_button'	=> '',
			'form_template' => '',
			'form_style' 	=> '',
			'form_button_styles' => '',			
			'note' 	=> ''
	    );
	    
		$classes = $errors;
		
		
		
    if ($_POST)
    {
         // Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
        $post = new Validation($_POST);
 
         //  Add some filters
        $post->pre_filter('trim', TRUE);
 		
        // Add some rules, the input field, followed by a list of checks, carried out in order
        $post->add_rules('name','required');
        $post->add_rules('submit_button','required');
        $post->add_rules('form_template','required');
        $post->add_rules('form_style','required');
        
 
        // Test to see if things passed the rule checks
        if ($post->validate())  {
			
        	if(!$formObj->loaded){
        		$formObj->omm_list_id = $this->list->id;
        		$formObj->code = string::random_string('unique');
        	}

        	$formObj->form_template = $post->form_template;
        	$formObj->form_style = $post->form_style;

        	
        	$formObj->name = $post->name;
        	
        	$formObj->note = $post->note;
        	
        	if(empty($post->note))
        		$formObj->note = null;
        	
        	$formObj->submit_button = $post->submit_button;
        	
        	$formObj->saveObject();
        	$formObj->deleteFields();
        	
        	$i = 1;
        	foreach($post->formfields as $pf){
        		
        		$ff = ORM::factory('omm_list_form_field');
        		$ff->omm_list_form_id = $formObj->id;
        		$ff->field_reference = $pf;
        		
        		$req = $pf.'_required';
        		$id = $pf.'_id';
        		
        		if(isset($post->$req)){
        			$ff->required = $post->$req;
        		}

        		if($pf == 'email'){
        			$ff->required = 1;
        		}
        		
        		$ff->omm_list_field_id = $post->$id;
        		$ff->ord = $i;
        		
        		$ff->saveObj();
        		
        		$i++;
        	}
        	
        	if(isset($post->adatmodForm) && $post->adatmodForm == '1'){
        		$this->list->datamod_form_id = $formObj->id;
        		$this->list->saveObject();
        	}else{
        		if($this->list->datamod_form_id == $formObj->id){
        			$this->list->datamod_form_id = null;
        			$this->list->saveObject();
        		}
        	}
        	
			
        	$_SESSION['selected_form'] = $formObj;
        	
        	unset($_POST);
        	
        	url::redirect("/pages/formdetail");
        	
           
        } // No! We have validation errors, we need to show the form again, with the errors
        else {
            // repopulate the form fields
            $form = arr::overwrite($form, $post->as_array());
 
            // populate the error fields, if any
    		// We need to already have created an error message file, for Kohana to use
            // Pass the error message file name to the errors() method
            
            $errors = arr::overwrite($errors, $post->errors('form_fields_errors'));
            
            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
            $errorTempl->errors = $errors;
            
            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
            
            foreach ($errors as $key => $error){
            	if($error != ""){
            		$classes[$key] = "clearfixError";
            	}
            }
            
        }
    }
    

    	if($formObj->loaded){
    		$this->pagetemplate->formFields = $formObj->fields();
    		$this->pagetemplate->formHtml = $formObj->getHtml();
    		$this->pagetemplate->formid = $formObj->id;

    		$this->pagetemplate->datamodForm = ($this->list->datamod_form_id == $formObj->id);
    		
    	}else{
    		$this->pagetemplate->formFields = array();
    		$this->pagetemplate->formHtml = "";
			$this->pagetemplate->formid = "";
			$this->pagetemplate->datamodForm = false;
    	}
    	
		$this->pagetemplate->form_templates = $this->form_templates;
		$this->pagetemplate->form_styles = $this->form_styles;    	
		$this->pagetemplate->form_button_styles = $this->form_button_styles;    	    	

		$this->pagetemplate->globalfields = $this->client->getFields();
		$this->pagetemplate->listfields = $this->list->fields();		
		
		$this->pagetemplate->fields = $this->list->fields();
		
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;
		$this->render();
	}




	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}