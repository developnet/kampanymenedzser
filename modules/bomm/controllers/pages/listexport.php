<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."_preload.php";

/**
 */
class Listexport_Controller extends Ommpage_Controller {

	var $_pageview = "lists_export";
	var $_pagetemplate;
	var $client;
	var $list;

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();

		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}




		$this->template->bodyClass = "subscriberlist";
	}

	public function index($param = "")	{
		$this->pageview = $this->_pageview;
		$this->init();
		$this->template->clientname = $this->client->name;

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];
		}

		$form = array(
	        'method'      			=> "import",
	        'separator_import'      => ";",
	        'separator_export'      => ";",
	        'quote_export'      	=> "&quot;",
			'quote_import'      	=> "&quot;"	 	 			        
			);

			$errors = array(
	        'method'      	=> "",
	        'separator_import'      	=> "",
	        'separator_export'      	=> "",
	        'quote_export'      		=> "",
			'quote_import'      		=> ""	 					
			);

			$classes = $form;



		 if ($_POST){
		 	 
		 	$post = new Validation($_POST);
		 	$post->pre_filter('trim');


		 	///////////////////////////////MENTÉS
		 	if ($post->validate())  {

		 		unset($_POST);
		 		url::redirect("/pages/listexport");

		 	} // HIBA
		 	else {
		 		$form = arr::overwrite($form, $post->as_array());

		 		$errors = arr::overwrite($errors, $post->errors('form_errors_listexport'));

		 		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
		 		$errorTempl->errors = $errors;

		 		$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

		 		foreach ($errors as $key => $error){
		 			if($error != ""){
		 				$classes[$key] = "clearfixError";
		 			}
		 		}

		 	}
		 }

		 $this->pagetemplate->param = $param;
		 $this->pagetemplate->classes = $classes;
		 $this->pagetemplate->form = $form;
		 



		 $this->render();
	}


	public function export2($list_id){

		$this->template->clientname = $this->client->name;

		$this->auto_render = FALSE;

		
		$list = ORM::factory('omm_list')->find($list_id);
		
		$fields = $list->fields();
		header('Content-type: text/csv; charset=ISO-8859-2');
		header('Content-disposition: attachment;filename='."export_".$list->id."_".date("YmdHis")."_".$_SESSION['auth_user']->username.".csv");

		$csv = "regdatum;email;statusz;";
		foreach ($fields as $f){
			$csv .= $f->reference.';';
		}
		$csv .= "\n";


		foreach($list->getMembersForImport() as $m){

			$csv .= $m->regdatum.";".$m->email.";";

			switch ($m->statusz){
				case 'active':
					$csv .= '1;';
					break;
				case 'unsubscribed':
					$csv .= '2;';
					break;
				case 'deleted':
					$csv .= '3;';
					break;
				case 'error':
					$csv .= '4;';
					break;
				case 'prereg':
					$csv .= '5;';
					break;
				default:
					$csv .= '1;';
					break;
			}

			foreach ($fields as $f){

				$ref = $f->reference;

				if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){

					$code = $m->$ref;

					$csv .= $f->getValueFromCode($code).';';

				}elseif($f->type == "multiselect"){

					//EePg4fd7,q82ikgWE,Hhe7hC7l,

					$codes = $m->$ref;

					$codesArray = explode(",",$codes);

					$tocsv = '';
					foreach ($codesArray as $c){
						if($c != ""){
							$tocsv .= $f->getValueFromCode($c).",";
						}
							
					}

					$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
					$csv .= $tocsv.';';
				}else{
					$csv .= $m->$ref.';';
				}



			}
			$csv .= "\n";

		}


		//	    $csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
		$csv = $this->translateUTF8ToWindowsCP1252($csv);
		$csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
		echo $csv;
	}

	public function export(){

		$this->template->clientname = $this->client->name;

		$this->auto_render = FALSE;

		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];
		}else{
			url::redirect('/pages/listdetail');
		}
		$fields = $this->client->getAllFields($list->id);
		//$fields = $list->fields();//
		

		$csv = "regdatum;list_regdatum;email;statusz;tag;";
		foreach ($fields as $f){
			$csv .= $f->reference.';';
		}
		
		$csv .= "\n";


		foreach($list->getMembersForImport("", "",$fields) as $m){

			$csv .= $m->regdatum.";".$m->list_reg_date.";".$m->email.";";

			switch ($m->statusz){
				case 'active':
					$csv .= '1;';
					break;
				case 'unsubscribed':
					$csv .= '2;';
					break;
				case 'deleted':
					$csv .= '3;';
					break;
				case 'error':
					$csv .= '4;';
					break;
				case 'prereg':
					$csv .= '5;';
					break;
				default:
					$csv .= '1;';
					break;
			}
			
			//cimkék
			$member = ORM::factory('omm_list_member')->find($m->id);
			$csv .= $member->getTags(true).";";		
			unset($member);
			
			//
			
			foreach ($fields as $f){

				$ref = $f->reference;

				if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){

					$code = $m->$ref;

					$csv .= $f->getValueFromCode($code).';';

				}elseif($f->type == "multiselect"){

					//EePg4fd7,q82ikgWE,Hhe7hC7l,

					$codes = $m->$ref;

					$codesArray = explode(",",$codes);

					$tocsv = '';
					foreach ($codesArray as $c){
						if($c != ""){
							$tocsv .= $f->getValueFromCode($c).",";
						}
							
					}

					$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
					$csv .= $tocsv.';';
				}else{
					$csv .= $m->$ref.';';
				}



			}
			$csv .= "\n";

		}


		//	    $csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
		$csv = string::translateUTF8ToWindowsCP1252($csv);
		$csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
		
		header('Content-type: text/csv; charset=ISO-8859-2');
		header('Content-disposition: attachment;filename='."export_".$list->id."_".date("YmdHis")."_".$_SESSION['auth_user']->username.".csv");
		
		
		echo $csv;
	}


	function translateUTF8ToWindowsCP1252($string) {
		$utf8 = array(
        '‚Ç¨', // ‚Ç¨
        '‚Äô', // ‚Äô
        '¬£', // ¬£
        '√Ä', // √Ä
        '√Å', // √Å
        '√Ç', // √Ç
        '√É', // √É
        '√Ñ', // √Ñ
        '√Ö', // √Ö
        '√Ü', // √Ü
        '√á', // √á
        '√à', // √à
        '√â', // √â
        '√ä', // √ä
        '√ã', // √ã
        '√å', // √å
        '√ç', // √ç
        '√é', // √é
        '√è', // √è
        '√ê', // √ê
        '√ë', // √ë
        '√í', // √í
        '√ì', // √ì
        '√î', // √î
        '√ï', // √ï
        '√ñ', // √ñ
        '√ó', // √ó
        '√ò', // √ò
        '√ô', // √ô
        '√ö', // √ö
        '√õ', // √õ
        '√ú', // √ú
        '√ù', // √ù
        '√û', // √û
        '√ü', // √ü
        '√†', // √†
        '√°', // √°
        '√¢', // √¢
        '√£', // √£
        '√§', // √§
        '√•', // √•
        '√¶', // √¶
        '√ß', // √ß
        '√®', // √®
        '√©', // √©
        '√™', // √™
        '√´', // √´
        '√¨', // √¨
        '√≠', // √≠
        '√Æ', // √Æ
        '√Ø', // √Ø
        '√∞', // √∞
        '√±', // √±
        '√≤', // √≤
        '√≥', // √≥
        '√¥', // √¥
        '√µ', // √µ
        '√∂', // √∂
        '√∑', // √∑
        '√∏', // √∏
        '√π', // √π
        '√∫', // √∫
        '√ª', // √ª
        '√º', // √º
        '√Ω', // √Ω
        '√æ', // √æ
        '√ø', // √ø
		);

		$cp1252 = array(
		chr(128), // ‚Ç¨
		chr(146), // ‚Äô
		chr(163), // ¬£
		chr(192), // √Ä
		chr(193), // √Å
		chr(194), // √Ç
		chr(195), // √É
		chr(196), // √Ñ
		chr(197), // √Ö
		chr(198), // √Ü
		chr(199), // √á
		chr(200), // √à
		chr(201), // √â
		chr(202), // √ä
		chr(203), // √ã
		chr(204), // √å
		chr(205), // √ç
		chr(206), // √é
		chr(207), // √è
		chr(208), // √ê
		chr(209), // √ë
		chr(210), // √í
		chr(211), // √ì
		chr(212), // √î
		chr(213), // √ï
		chr(214), // √ñ
		chr(215), // √ó
		chr(216), // √ò
		chr(217), // √ô
		chr(218), // √ö
		chr(219), // √õ
		chr(220), // √ú
		chr(221), // √ù
		chr(222), // √û
		chr(223), // √ü
		chr(224), // √†
		chr(225), // √°
		chr(226), // √¢
		chr(227), // √£
		chr(228), // √§
		chr(229), // √•
		chr(230), // √¶
		chr(231), // √ß
		chr(232), // √®
		chr(233), // √©
		chr(234), // √™
		chr(235), // √´
		chr(236), // √¨
		chr(237), // √≠
		chr(238), // √Æ
		chr(239), // √Ø
		chr(240), // √∞
		chr(241), // √±
		chr(242), // √≤
		chr(243), // √≥
		chr(244), // √¥
		chr(245), // √µ
		chr(246), // √∂
		chr(247), // √∑
		chr(248), // √∏
		chr(249), // √π
		chr(250), // √∫
		chr(251), // √ª
		chr(252), // √º
		chr(253), // √Ω
		chr(254), // √æ
		chr(255), // √ø
		);

		return str_replace($utf8, $cp1252, $string);
	}

	public function preimport(){

		$this->pageview = $this->_pageview;
		$this->init();
		$this->template->clientname = $this->client->name;
		
		$this->pagetemplate->param = "";
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

			
		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];
		}

		$form = array(
	        'method'      			=> "import",
	        'separator_import'      => ";",
	        'separator_export'      => ";",
	        'quote_export'      	=> "&quot;",
			'quote_import'      	=> "&quot;"	 	 			        
			);

			$errors = array(
	        'method'      	=> "",
	        'separator_import'      	=> "",
	        'separator_export'      	=> "",
	        'quote_export'      		=> "",
			'quote_import'      		=> ""	 					
			);

			$classes = $form;

			if(!isset($_FILES['file_import']) || !isset($_POST)){
					
				$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
				$errorTempl->errors = array('Hibás csv fájl!');
				$_SESSION['csverror'] = $errorTempl->render(FALSE,FALSE);

				unset($_POST);
				unset($_FILES);
				url::redirect("/pages/listexport");


			}

			if (upload::valid($_FILES['file_import']) && upload::type($_FILES['file_import'],array('csv'))){
				
				$filename = upload::save('file_import');
				//var_dump($filename);					
				$this->pagetemplate->importfile = $filename;

				if($filename == ""){

					$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
					$errorTempl->errors = array('Hibás csv fájl!');
					$_SESSION['csverror'] = $errorTempl->render(FALSE,FALSE);

					unset($_POST);
					unset($_FILES);
					url::redirect("/pages/listexport");

				}

				$content = file_get_contents($filename);
				
				if(mb_detect_encoding($content, 'UTF-8, ISO-8859-1, ISO-8859-2', true) != "UTF-8"){
					fileutils::convert_file_to_utf8($filename);
				}


				$csvData = fileutils::getCsvContent($filename,0,5);

				$i = 0;
				$header = array();
				$available_headers = array();
				$multivalue_headers = array();
				$multivalue_fields = array();
				
				/*
				foreach($list->fields() as $f){

					if($f->type == "singleselectradio" || $f->type == "singleselectdropdown" || $f->type == "multiselect"){
						$multivalue_headers[] = $f->reference;
						$multivalue_fields[$f->reference] = $f;
					}

					$available_headers[] = $f->reference;
				}*/


				$regdatum_key = -1;
				$email_key = -1;
				$statusz_key = -1;

				$rows = array();
				foreach($csvData as $row) {//////////header begyüjtése
					$r = array();
					$j = 0;
					foreach($row as $key => $col) {

						if($i == 0){ /// header sor
							$header['header_'.$j] = $col;
						}

						if($i < 4){/// mint gyűjtés
							$r['header_'.$j] = $col;
						}
						$j++;
					}

					$rows[] = $r;

					if($i == 3) break;

					$i++;
				}

				$this->pagetemplate->headernum = sizeof($header);

				$fields = $this->client->getFields();///
				$fielddrop = array('skip'=>'Ne importálja',
				 				   'email'=>'E-mail cím',					
				 				   'regdatum'=>'Feliratkozás dátuma',
				 				   'statusz'=>'Státusz');

				foreach($fields as $f){
					$fielddrop[$f->reference] = $f->name;
				}
				
				
				
				$this->pagetemplate->globalfields = $fields;
				$this->pagetemplate->listfields = array();
				
				$this->pagetemplate->fieldchoose = true;

				$this->pagetemplate->header = $header;

				$this->pagetemplate->rows = $rows;

					
			}else{

				$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
				$errorTempl->errors = array('Hibás csv fájl!');
				$_SESSION['csverror'] = $errorTempl->render(FALSE,FALSE);

				unset($_POST);
				unset($_FILES);
				url::redirect("/pages/listexport");

			}


		 $this->pagetemplate->classes = $classes;
		 $this->pagetemplate->form = $form;
		 $this->pagetemplate->lists = $this->client->lists();


		 $this->render();

	}


	public function import(){

		$this->template->clientname = $this->client->name;

		if(isset($_SESSION['selected_list'])){
			$list = $_SESSION['selected_list'];
		}else{
			url::redirect('/pages/listdetail');
		}

		if (isset($_POST['import_file'])){
			$filename = $_POST['import_file'];
			$headernum = $_POST['headernum'];

			if(isset($_POST['first_line_header']) && $_POST['first_line_header'] == 1){
				$firstline = false;
			}else{
				$firstline = true;
			}

			/*
			 $content = file_get_contents($filename);

			 if(mb_detect_encoding($content, 'UTF-8, ISO-8859-1, ISO-8859-2', true) != "UTF-8"){
				fileutils::convert_file_to_utf8($filename);
				}
				*/
			$csvData = fileutils::getCsvContent($filename);

			$i = 0;
			$header = array();
			$fields = array();

			foreach($list->fields() as $f){
				$fields[$f->reference] = $f;
			}


			$regdatum_key = -1;
			$email_key = -1;
			$statusz_key = -1;
			$headerreferences = array();
			for ($i=0;$i<$headernum;$i++){

				if($_POST['header_'.$i] == "email"){
					$email_key = $i;
				}elseif($_POST['header_'.$i] == "regdatum"){
					$regdatum_key = $i;
				}elseif($_POST['header_'.$i] == "statusz"){
					$statusz_key = $i;
				}elseif($_POST['header_'.$i] != "skip"){
					$headerreferences[$_POST['header_'.$i]] = $i;
				}

			}

			if($email_key == -1){
				$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
				$errorTempl->errors = array('Nincs e-mail oszlop vagy hiányzik a fejléc!');
				$_SESSION['csverror'] = $errorTempl->render(FALSE,FALSE);
				url::redirect("/pages/listexport");
			}

			$notuniquemail = array();
			$errors = array();
			$all = 0;
			$imported_active = array();
			$imported_unsubscribed = array();
			$imported_deleted = array();

			$modified_user = array();
			$new_user = array();


			$i = 0;
			foreach($csvData as $row) {//////////adatok begyüjtése
				if(($i == 0 && $firstline) || $i > 0){////header skip

					if(isset($row[$email_key])){
						$email = $row[$email_key];
					}else{
						$email = "nincs email:$i";
					}

					//$email = trim($email);
					if(valid::email($email) == false){
						$errors[] = $email;
						continue;
					}
					$all++;
					$member = ORM::factory('omm_list_member')->where('omm_list_id',$list->id)->where('email',$email)->find();

					if(!$member->loaded){ ///új user létrehozzuk

						$member->omm_list_id = $list->id;
						$member->manual = 2;

						if($regdatum_key == -1){
							$member->reg_date = date("Y-m-d H:i:s");
						}else{
							$member->reg_date = $row[$regdatum_key];
						}

						if($statusz_key == -1){
							$member->status = 'active';
							$member->activation_date = $member->reg_date;
						}else{

							switch ($row[$statusz_key]){

								case 1:
									$member->status = 'active';
									$member->activation_date = $member->reg_date;
									break;
								case 2:
									$member->status = 'unsubscribed';
									$member->activation_date = $member->reg_date;
									break;
								case 3:
									$member->status = 'deleted';
									break;
								default:
									$member->status = 'active';
									$member->activation_date = $member->reg_date;
									break;
							}
						}

						$member->code = string::random_string('unique');
						$member->email = $email;
						$member->unsubscribe_date = null;

						$new_user[] = $email;
							
					}else{////meglévő user updatelünk

						if($regdatum_key != -1){
							$member->reg_date = $row[$regdatum_key];
						}


						if($statusz_key != -1){

							switch ($row[$statusz_key]){

								case 1:{
									if($member->status != 'active'){
										$member->status = 'active';
										//$member->unsubscribe_date = null;

										if($member->activation_date == null){
											$member->activation_date = date("Y-m-d H:i:s");
										}
											
									}
									break;
								}
								case 2:{
									if($member->status != 'unsubscribed'){
										$member->status = 'unsubscribed';

										if($member->unsubscribe_date == null){
											$member->unsubscribe_date = date("Y-m-d H:i:s");
										}
									}
									break;
								}
								case 3:{
									if($member->status != 'deleted'){
										$member->status = 'deleted';
									}
									break;
								}
								default:///nem csinálunk semmit
									break;
							}
						}

						$modified_user[] = $email;
					}

					$member->save();
					$post = new Object();
					foreach ($headerreferences as $key => $h){
						$ff = $fields[$key];
						//echo $key."<br>";
						//echo $ff->type;
						if(isset($row[$h])){
							if($ff->type == "singleselectradio" || $ff->type == "singleselectdropdown" || $ff->type == "multiselect"){
								//$post->$h = $field->getCodeFromValue($row[$key]);
								$array = explode(",",$row[$h]);
								$atad = array();
								foreach ($array as $k => $a){
									$b = trim($a);
									if($b != "") {
										$code = $ff->getCodeFromValue($b);
										if($code != '-'){
											$atad[] = $code;
										}
									}
								}
								$post->$key = $atad;
								unset($array);
							}else{
								$post->$key = string::replaceChars($row[$h]);
							}
								
						}else{
							$post->$key = "";
						}
						//$post->$key = $this->replaceChars($row[$h]);
					//echo "<br>".$key." = ".$post->$key."<br/>";
				}

				try {
					$member->saveData($post,$list->fields());
					switch ($member->status){
						case 'active':
							$imported_active[] = $email;
							break;
						case 'unsubscribed':
							$imported_unsubscribed[] = $email;
							break;
						case 'deleted':
							$imported_deleted[] = $email;
							break;
						default:
							break;
					}
					unset($member);
					unset($post);
				}catch (Exception $e){
					KOHANA::log("error","prereg member adatok mentés, member_id=".$member->id." ::: ".$e);
					$errors[] = $email;
				}
			}
			$i++;
		}/////////////adatok vége

		$message = "<strong>Összes sor:</strong> ".$all." "."<br/>";
		$message .= "<strong>Sikeres (aktív):</strong> ".sizeof($imported_active).""."<br/>";
		$message .= "<strong>Sikeres (leiratkozott):</strong> ".sizeof($imported_unsubscribed).""."<br/>";
		$message .= "<strong>Sikeres (törölt):</strong> ".sizeof($imported_deleted).""."<br/>";
		$message .= "<strong>-----------------------------<br/>";
		$message .= "<strong>Módosított:</strong> ".sizeof($modified_user).""."<br/>";
		$message .= "<strong>Új:</strong> ".sizeof($new_user).""."<br/>";
		$message .= "<strong>Helytelen e-mail cím:</strong> ".sizeof($errors).""."<br/>";

		//meta::createAlert("succes","Importálás eredménye:",$message);
		
		//$alertTempl->alertMessage .= "<strong>Log:</strong>"."<br/>";
		$log = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head><body style=\"font-family: sans-serif;\">";
		$log .= "Importálás a \"".$list->name."\" listába "."\n"."<br>";
		$log .= "Dátum: ".date("Y-m-d H:i:s")."\n"."<br>";
		$log .= "Felhasználó:".$_SESSION['auth_user']->username." (".$_SESSION['auth_user']->email.")"."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Sikeres e-mailek - aktív: "."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($imported_active as $m  ){
			$log .= $m."\n"."<br>";
		}
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Sikeres e-mailek - leiratkozott: "."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($imported_unsubscribed as $m  ){
			$log .= $m."\n"."<br>";
		}
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Sikeres e-mailek - törölt: "."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($imported_deleted as $m  ){
			$log .= $m."\n"."<br>";
		}

		$log .= "\n\n"."<br>"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Módosított e-mailek: "."\n"."<br>"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($modified_user as $m  ){
			$log .= $m."\n"."<br>";
		}
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Új e-mailek: "."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($new_user as $m  ){
			$log .= $m."\n"."<br>";
		}
		$log .= "\n\n"."<br>"."<br>";

		$log .= "Hibás e-mail: "."\n"."<br>";
		$log .= "-----------------------------"."\n"."<br>";
		foreach($errors as $m  ){
			$log .= $m."\n"."<br>";
		}
		$log .= "</body></html>";

		file_put_contents("./upload/importlog_".date("YmdHis")."_".$_SESSION['auth_user']->username.".html", $log);

		$message .= '<a target="_blank" href="'.url_Core::base().'upload/importlog_'.date("YmdHis").'_'.$_SESSION['auth_user']->username.'.html">Import eredménye</a>';

		meta::createAlert("succes","Importálás eredménye:",$message);
		
		unlink($filename);
	}else{
		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
		$errorTempl->errors = array('Hibás csv fájl!');
		$_SESSION['csverror'] = $errorTempl->render(FALSE,FALSE);
	}

	unset($_POST);
	unset($_FILES);
	url::redirect("/pages/listexport");
}

public function _unique_email($email){
	////van-e ilyen aktív user?
	return  (bool) ORM::factory('omm_list_member')->where('omm_list_id',$this->list->id)->where('email',$email)->where('status','active')->count_all();
}

/**
 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
 */
public function __call($method, $arguments)
{
	$this->auto_render = FALSE;
	KOHANA::show_404(FALSE,FALSE);
}

}