<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Groupoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "groups_overview";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function index()	{
		
		if(isset($_SESSION['selected_group'])){
			unset($_SESSION['selected_group']);
		}		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		$this->pagetemplate->alert = "";
		
		$this->pagetemplate->fields = $this->client->getAllFields($this->list->id);
		$this->pagetemplate->groups = $this->list->groups();
		
		$this->pagetemplate->membersSum = $this->list->membersNumber();
		
		
		$this->render();
	}

	
	public function selectgroup(){
		$gId = $this->uri->segment(4,"");

		if($gId == ""){
			url::redirect("/pages/listoverview");
		}else{
			
			$g = ORM::factory("omm_list_group")->find($gId);
			
			if($g->loaded){
				$_SESSION['selected_group'] = $g;
				url::redirect("/pages/groupmembers");	
			}else{
				url::redirect("/pages/groupoverview");	
			}
			
		}
		
	}
	
	public function deleteGroup(){
		
		$groupId = $this->uri->segment(4,"");

		if($groupId == ""){
			url::redirect("/pages/groupoverview");
		}else{
			
			$g = ORM::factory("omm_list_group")->find($groupId);
			
			if($g->loaded){
				
				$g->deleteGroup();
				
				meta::createAlert("succes","Csoport törlése sikeres volt!","");
				
			}else{
				url::redirect("/pages/groupoverview");	
			}
			
		}
		
		
		
		url::redirect("/pages/groupoverview");
	}	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}