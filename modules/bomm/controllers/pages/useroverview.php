<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Useroverview_Controller extends Ommpage_Controller {

	var $_pageview = "user_overview";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "main";
		parent::__construct();
		$this->template->bodyClass = "clientdashboard";
	}

	public function index()	{

		$this->pageview = $this->_pageview;
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SERVER['HTTP_REFERER'])){
			$this->pagetemplate->cancelLink = $_SERVER['HTTP_REFERER'];
		}else{
			$this->pagetemplate->cancelLink = url::base()."pages/clientoverview";
		}

		//$this->pagetemplate->clients = ORM::factory("omm_client")->orderby("name","asc")->find_all();


		$form = array(
	        'email'   => '',
	        'firstname'  	=> '',
	        'lastname'  => '',
			'newpassword'  => '',
			'newpassword2'  => ''
	        );

	        $errors = $form;
	        $classes = $form;


	        $user = $_SESSION['auth_user'];

	        $form = array(
		        'email'   => $user->email,
		        'firstname'  	=> $user->firstname,
		        'lastname'  => $user->lastname
	        );
	        	



	        if ($_POST)
	        {
	        	// Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
	        	$post = new Validation($_POST);

	        	//  Add some filters
	        	$post->pre_filter('trim', TRUE);
	        	//$post->pre_filter('ucfirst', 'name');
	        	// Add some rules, the input field, followed by a list of checks, carried out in order
	        	$post->add_rules('email', 'required','email');
	        	$post->add_rules('firstname','required', 'length[0,255]');
	        	$post->add_rules('lastname','required', 'length[0,255]');
				
	        	if(isset($_POST['newpassword']) && $_POST['newpassword'] != ""){
	        		$post->add_rules('newpassword','required','alpha_dash', 'matches[newpassword2]','length[6,255]');
	        		$post->add_rules('newpassword2','matches[newpassword]');
	        	}
	        	

	        	// Test to see if things passed the rule checks
	        	if ($post->validate())  {
	        		// Yes! everything is valid
	        		$this->pagetemplate->errors = 'Form validated and submitted correctly. <br />';
	        		 
	        		 
	        		
	        		
	        		$user->email = $post->email;
	        		$user->firstname = $post->firstname;
	        		$user->lastname = $post->lastname;
	        		
	        		if(isset($_POST['newpassword']) && $_POST['newpassword'] != ""){
	        			$user->password = $post->newpassword;
	        		}
	        		
	        		$user->save();

	        		$_SESSION['auth_user'] = $user;
	        		 
					meta::createAlert("succes","Sikeres módosítás!","A felhasználó adatainak módosítása sikeres volt.");

	        		if(isset($_POST['newpassword']) && $_POST['newpassword'] != ""){
	        			url::redirect('/login/logout');
	        		}else{
	        			url::redirect($post->referer);
	        		}
	        		
	        		
	        		

	        		 
	        		 
	        	} // No! We have validation errors, we need to show the form again, with the errors
	        	else {
	        		// repopulate the form fields
	        		$form = arr::overwrite($form, $post->as_array());

	        		// populate the error fields, if any
	        		// We need to already have created an error message file, for Kohana to use
	        		// Pass the error message file name to the errors() method

	        		$errors = arr::overwrite($errors, $post->errors('form_errors_user'));

	        		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
	        		$errorTempl->errors = $errors;

	        		$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

	        		foreach ($errors as $key => $error){
	        			if($error != ""){
	        				$classes[$key] = "clearfixError";
	        			}
	        		}


	        		$this->pagetemplate->cancelLink = $post->referer;

	        	}
	        }

	        $this->pagetemplate->classes = $classes;
	        $this->pagetemplate->form = $form;
	        $this->render();
	}




	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}