<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Memberdetail_Controller extends Ommpage_Controller {
	
	var $_pageview = "member_detail";
	var $_pagetemplate;
	var $client;
	var $list;
	var $mo;
	

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		
		
		
		/*


		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}
		*/
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function _unique_email(Validation $array, $field){
	   
		if($this->mo != null && $this->mo->email != $array[$field]){
			$email_exists = (bool) ORM::factory('omm_list_member')->where('omm_client_id', $this->client->id)->where('email', $array[$field])->count_all();
	 
		   if ($email_exists)  {
		       $array->add_error($field, 'email_exists');
		   }			
		}elseif($this->mo == null){
			
			$email_exists = (bool) ORM::factory('omm_list_member')->where('omm_client_id', $this->client->id)->where('email', $array[$field])->count_all();
	 
		   if ($email_exists)  {
		       $array->add_error($field, 'email_exists');
		   }
			
		}
	}
	
	public function detail($member_id,$list_id)	{
		
	    if ($_POST){
	        $post = new Validation($_POST);
	        $post->pre_filter('trim', TRUE);

	        $member = ORM::factory("omm_list_member")->find($member_id);
	        $list = ORM::factory('omm_list')->find($list_id);
	        
	        if($member->loaded){
	        		
                	////megnézni, hogy van-e kicsekkelt checkboxos mező
		        	$mfields = explode(",",$post->multiselect_fields); 
		        	
		        	foreach($mfields as $f){
		        		if($f != ""){
		        			if(!isset($post->$f)){
		        				$post->$f = array();
		        			}
		        		}
		        	}	 

		        $member->saveObject();
				$member->saveData($post,$list->fields());	
		        	
		        unset($_POST);
		       	meta::createAlert("succes","Sikeres módosítás!","A feliratkozó adatai módosítva!");
		       		        	
	        }
	    }

        url::redirect("/pages/memberdetail/index/".$member->id);		
	}	
	
	public function index($member_id = 0)	{
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->client = $this->client;
		
		$this->pagetemplate->listName = "{{Lista neve nem értelmezett}}";

		$this->pagetemplate->errors = ""; //clearfixError
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->errors = $_SESSION['alert'];
			unset($_SESSION['alert']);
		}else{
			$this->pagetemplate->errors = ""; //clearfixError
		}			
		
		
		
		if(isset($member_id) || $member_id = 0){
			$member = ORM::factory("omm_list_member")->find($member_id);	
			
			
			
		}else{
			$member = ORM::factory("omm_list_member");
		}		
		
		
        if($member->loaded){
        	$mo = $member->getData($this->client->getFields());
        	$this->mo = $mo;		

			$form = array(
		        'email'  		=> $mo->email,
		        'reg_date'  	=> $mo->reg_date,
		        'status'  		=> $mo->status
		    );        	
        	
        }else{
        	
			$form = array(
		        'email'  		=> '',
		        'reg_date'  	=> '',
		        'status'  		=> ''
		    );         	
        	
        }

		
		$errors = array(
	        'email'  		=> '',
	        'reg_date'  	=> '',
	        'status'  		=> ''
	    );
	    
		$classes = $errors;
		 
		
	    foreach ($this->client->getFields() as $f) {
				
        	$r = $f->reference;
        	
        	$form[$r] = '';
        	$errors[$r] = '';
        	
	        if($member->loaded){
				
	        	if(isset($mo->$r))
	        		$form[$r] = $mo->$r;
	        	else
	        		$form[$r] = "";
	        			 
				$rrr = $r."_code";
				
				if(isset($mo->$rrr)){
					$form[$rrr] = $mo->$rrr; 	
				}

	        }    
        	
//	        if($f->required){
//	    		$post->add_rules($r,'required');    	
//	        }
	        
        }		
		
    if ($_POST)
    {
    	if(isset($_POST['listadd_list_id']) && $member->loaded){
    		$post = new Validation($_POST);
    		
    		$regdate = $post->listadd_regdate;
    		$list_id = $post->listadd_list_id;
    		
    		$member->addList($list_id,$regdate);
    		meta::createAlert("succes","Sikeres hozzáadás!","Az új feliratkozó felkerült a listára!");	
    		url::redirect("/pages/memberdetail/index/".$member->id);
    	}
    	
    	if(isset($_POST['product_connect_id']) && $member->loaded){
    		$post = new Validation($_POST);
    		
    		$pc = ORM::factory("omm_product_member_connect")->find($post->product_connect_id);
    		
    		$pc->status = $post->product_mod_status;
    		$pc->order_date = $post->product_order_date;
    		$pc->purchase_date = $post->product_purchase_date;
    		$pc->failed_date = $post->product_failed_date;
    		$pc->saveObject();
    		meta::createAlert("succes","Sikeres módosítás!","A termék adatai módosítva lettek!");	
    		url::redirect("/pages/memberdetail/index/".$member->id);
    	}
    	
    	if(isset($_POST['product_id']) && isset($_POST['product_status']) && $member->loaded){
			$post = new Validation($_POST);
			
			$member->addProduct($post->product_id,$post->product_status);
			meta::createAlert("succes","Sikeres hozzáadás!","A termék hozzá lett adva a feliratkozónak!");			
    		url::redirect("/pages/memberdetail/index/".$member->id);
    	}
         // Instantiate Validation, use $post, so we don't overwrite $_POST fields with our own things
        $post = new Validation($_POST);
 		
         //  Add some filters
        $post->pre_filter('trim', TRUE);
 		
        // Add some rules, the input field, followed by a list of checks, carried out in order
        $post->add_rules('email','required', 'email');
        $post->add_rules('status','required');
		$post->add_callbacks('email', array($this, '_unique_email'));
        
        
 
        // Test to see if things passed the rule checks
        if ($post->validate())  {
			$new = false;
        	if(!$member->loaded){
				$new = true;
        		$code1 = string::random_string('unique');
				$reg_date =  date("Y-m-d H:i:s");

				$member->reg_date = $reg_date;
				$member->code = $code1;
				$member->omm_client_id = $this->client->id;
				$member->manual = 1;
        	}
        	
        	
        	if($post->status == 'active' && $member->status != 'active' && $member->loaded){
        		$member->unsubscribe_date = null;
        		
        		if(!$member->loaded)
       				$member->activation_date = date("Y-m-d H:i:s");
        		
       			//url::redirect("/pages/memberdetail/index/".$member->id);
        	}

             if($post->status == 'prereg' && $member->status != 'prereg' && $member->loaded){
        		$member->unsubscribe_date = null;
        		
        		if(!$member->loaded)
        			$member->activation_date = null;
        		
        		$member->reg_date = date("Y-m-d H:i:s");
        	}        	
        	
        	if($post->status == 'unsubscribed' && $member->status != 'unsubscribed' && $member->loaded){
        		
        		$member->unsubscribe_date = date("Y-m-d H:i:s");
        		
        	}
        	
        	////megnézni, hogy van-e kicsekkelt checkboxos mező
        	
        	$mfields = explode(",",$post->multiselect_fields); 
        	
        	foreach($mfields as $f){
        		if($f != ""){
        			if(!isset($post->$f)){
        				$post->$f = array();
        			}
        		}
        	}
        	
        	
        	
        	$member->status = $post->status;
        	$member->email = $post->email;
        	$member->saveObject(); //
        	$member->saveData($post,$this->client->getFields());
        	
        	$tags = explode(",", $post->as_values_tags);
        	$_tags = array();
        	$tags[] = 'Mindenki';
        	foreach($tags as $tag){
        		if (is_numeric($tag)){
        			$_tags[] = array('id' => $tag);
        		}elseif($tag != ""){//
        			$tago = ORM::factory("omm_tag");
        			$tag_id = $tago->createIfNotExist($tag,'active');
        			
        			$_tags[] = array('id' => $tag_id);
        		}
        	}
        	
        	$member->addTags($_tags);
        	
        	
			
        	
        	
        	unset($_POST);
        	
        	if($new){
	        	meta::createAlert("succes","Sikeres hozzáadás!","Az új feliratkozó elmentve!");	
        	}else{
        		meta::createAlert("succes","Sikeres módosítás!","A feliratkozó adatai módosítva!");
        	}
        	
        	url::redirect("/pages/memberdetail/index/".$member->id);
        	
        	
           
        }else {
            $form = arr::overwrite($form, $post->as_array());
            
        	$form['email'] = $post->email;
        	
            $errors = arr::overwrite($errors, $post->errors('form_errors_member'));
            
            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
            $errorTempl->errors = $errors;
            
            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
            
            foreach ($errors as $key => $error){
            	if($error != ""){
            		$classes[$key] = "clearfixError";
            	}
            }
            
        }
    }
    
		$nameref = $this->client->getNameField();
		
		if($member->loaded){
			$this->pagetemplate->mo = $mo;
			
			if($nameref == "bothnames"){
			
				$first = $this->client->searchReferenceByType("firstname");
				$last = $this->client->searchReferenceByType("lastname");
				
				$name = $mo->$last." ".$mo->$first;
			}else{
				
				if($nameref != ""){
					$name = $mo->$nameref;	
				}else{
					$name = $mo->email;
				}
				
				
			}			
			
		}else{
			$this->pagetemplate->mo = "";
			
			$name = KOHANA::lang("bomm.newsubscriber");
		}
		

    	$this->pagetemplate->member = $member;
		
		$this->pagetemplate->stat = $member->getMemberStat();
		$this->pagetemplate->allletters = $member->getLetters();
		
		
		
		$this->pagetemplate->fields = $this->client->getFields();
    	$this->pagetemplate->name = $name;
		
    	$this->pagetemplate->statuses = meta::getCodeDict(4);
		$this->pagetemplate->classes = $classes;
		$this->pagetemplate->form = $form;
		$this->render();
	}
	
	public function delFromList($memberid){
		
		if(isset($_POST['list_id'])){
			$member = ORM::factory('omm_list_member')->find($memberid);
			
			$member->delFromList($_POST['list_id']);
			
			meta::createAlert("succes","Sikeres törlés!","A feliratkozó lekerült a listáról.");
		}else{
			meta::createAlert("error","Nincs kiválasztva lista!","Nincs lista kiválasztva a törléshez.");
			
		}
		
		url::redirect("/pages/memberdetail/index/".$memberid);
		
	}
	
	public function sendCommonLetter($memberid){
		$this->template->clientname = $this->client->name;
		if(isset($_POST['common_letter_id'])){
			
			$letter = ORM::factory('omm_common_letter')->find($_POST['common_letter_id']);

			if($letter->loaded){////////elküldjük a feliratozást megköszönö mailt
				$job = ORM::factory('omm_job');
				$job->create("common", null, $letter->id, $memberid, null);
					
				$dom = new simple_html_dom();
				$job->prepare($dom);
				unset($dom);

				$job->execute();
				//var_dump($job);
				
				if($job->status == 'sent'){
					meta::createAlert("succes","Sikeres küldés!","A levél sikeresen ki lett küldve.");	
				}else{
					meta::createAlert("succes","Sikeres küldés!","A levél sikeresen ki lett küldve.");
				}
				
				
			}
			
			
			
		}else{
			meta::createAlert("error","Nincs kiválasztva levél!","Nincs levél kiválasztva a küldéshez.");
		}
		
		url::redirect("/pages/memberdetail/index/".$memberid);
	}	
	

	public function selectothermember(){
		
		$_SESSION['listdetailreferrer'] = $_SERVER['HTTP_REFERER'];
		
		
		$mid = $this->uri->segment(4,"");
		$lid = $this->uri->segment(5,"");
		
		if($mid == "" || $lid == ""){
			url::redirect("/pages/listdetail");
		}else{
			$_SESSION['selected_list'] = ORM::factory("omm_list")->find($lid);
			$_SESSION['selected_member'] = $mid;
			url::redirect("/pages/memberdetail");	
		}		
		
	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}