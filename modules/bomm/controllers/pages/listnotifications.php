<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listnotifications_Controller extends Ommpage_Controller {
	
	var $_pageview = "lists_notifications";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}

		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function index()	{
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError		
		
		$notifs = $this->list->notifications();
		

		
		$form = array(
			'subscribe_email'					=> "",
			'unsubscribe_email'					=> "",
			'daily_email'						=> "",
			'subscribe_email_check'				=> "",
			'unsubscribe_email_check'			=> "",
			'daily_email_check'					=> ""
	    );
		
		$errors  = $form;
		$classes = $form;

		foreach ($notifs as $notif){
		
			if($notif->type == "subscribe" && $notif->value != NULL){
				$form['subscribe_email'] = $notif->value;
				$form['subscribe_email_check'] = "1";
			}
		
			if($notif->type == "unsubscribe" && $notif->value != NULL){
				$form['unsubscribe_email'] = $notif->value;
				$form['unsubscribe_email_check'] = "1";
			}			

			if($notif->type == "daily" && $notif->value != NULL){
				$form['daily_email'] = $notif->value;
				$form['daily_email_check'] = "1";
			}			
		}		
		
		
		 if ($_POST){
		    	
		        $post = new Validation($_POST);
		 
		        $post->pre_filter('trim');
		        
		        
		        $post->add_rules('subscribe_email','email');
		        $post->add_rules('unsubscribe_email','email');
		        $post->add_rules('daily_email','email');

		        if(isset($post->subscribe_email_check) && $post->subscribe_email_check == "1"){
		        	$post->add_rules('subscribe_email','required');
		        }

		 		if(isset($post->unsubscribe_email_check) && $post->unsubscribe_email_check == "1"){
		        	$post->add_rules('unsubscribe_email','required');
		        }

		 		if(isset($post->daily_email_check) && $post->daily_email_check == "1"){
		        	$post->add_rules('daily_email','required');
		        }		

		        
	        ///////////////////////////////MENTÉS
	        if ($post->validate())  {
					
	        	$this->list->clearNotifications();
	        	
		        if(isset($post->subscribe_email_check) && $post->subscribe_email_check == "1"){
		        	$not = ORM::factory("omm_list_notification");
		        	$not->omm_list_id = $this->list->id;
		        	$not->type = "subscribe";
		        	$not->value = $post->subscribe_email;
					$not->saveObject();
		        }

		 		if(isset($post->unsubscribe_email_check) && $post->unsubscribe_email_check == "1"){
		        	$not = ORM::factory("omm_list_notification");
		        	$not->omm_list_id = $this->list->id;
		        	$not->type = "unsubscribe";
		        	$not->value = $post->unsubscribe_email;
					$not->saveObject();
		        }

		 		if(isset($post->daily_email_check) && $post->daily_email_check == "1"){
		        	$not = ORM::factory("omm_list_notification");
		        	$not->omm_list_id = $this->list->id;
		        	$not->type = "daily";
		        	$not->value = $post->daily_email;
					$not->saveObject();
		        }		        	
	        	
		        
				unset($_POST);

				meta::createAlert("succes","A beállítások sikeresen elmentve!","");
				url::redirect("/pages/listdetail");
	           
	        } // HIBA
	        else {
	            $form = arr::overwrite($form, $post->as_array());
	 
	            $errors = arr::overwrite($errors, $post->errors('form_errors_listnotifications'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }else{////////////////////////ha nincs post
	    	

	    	
	    }
	    	

	    	
		
	    $this->pagetemplate->form = $form;
	    $this->pagetemplate->classes = $classes;
	    $this->pagetemplate->listName = $this->list->name;
		

		
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		KOHANA::show_404(FALSE,FALSE);
	}

}