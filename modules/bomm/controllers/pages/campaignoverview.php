<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Campaignoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "campaigns_overview";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		$this->template->bodyClass = "listindex";
	}
	
	public function index()	{
		

		if(isset($_SESSION['selected_list'])){
			unset($_SESSION['selected_list']);
		}
		
		if(isset($_SESSION['selected_letter'])){
			unset($_SESSION['selected_letter']);
		}
		if(isset($_SESSION['selected_member'])){
			unset($_SESSION['selected_member']);
		}		
		
		if(isset($_SESSION['selected_form'])){
			unset($_SESSION['selected_form']);
		}			
		
		if(isset($_SESSION['selected_group'])){
			unset($_SESSION['selected_group']);
		}			
		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		$this->pagetemplate->alert = "";
		
		
		$this->pagetemplate->campaigns = $this->client->campaigns();
		$this->pagetemplate->archived_campaigns = $this->client->campaigns('archived');
		
		
		$this->render();
	}

	public function archiveCampaign($id){
		$camp = ORM::factory('omm_campaign')->find($id);
		
		if($camp->loaded){
			$camp->status = 'archived';
			$camp->saveObject();
		}
		
		unset($_SESSION['selected_campaign']);
		url::redirect("/pages/campaignoverview");
		
	}
	
	public function activateCampaign($id){
		$camp = ORM::factory('omm_campaign')->find($id);
		
		if($camp->loaded){
			$camp->status = 'active';
			$camp->saveObject(); 
		}
		
		unset($_SESSION['selected_campaign']);
		url::redirect("/pages/campaignoverview");
		
	}	
	
	public function selectcampaign(){
		$campId = $this->uri->segment(4,"");
		$this->template->clientname = $this->client->name;
		if($campId == ""){
			url::redirect("/pages/campaignoverview");
		}else{
			
			$c = ORM::factory("omm_campaign")->find($campId);
			
			if($c->loaded){
				$_SESSION['selected_campaign'] = $c;
				url::redirect("/pages/campaignoverview");	
				
			}else{
				url::redirect("/pages/campaignoverview");	
			}
		}
	}

	public function deleteLetter(){
		$letterId = $this->uri->segment(4,"");
		$this->template->clientname = $this->client->name;
		if($letterId == ""){
			url::redirect("/pages/campaignoverview");
		}else{
			
			$c = ORM::factory("omm_letter")->find($letterId);
			
			if($c->loaded && $c->omm_campaign_id == $_SESSION['selected_campaign']->id){
				$c->deleteLetter();
				url::redirect("/pages/campaignoverview");	
				
			}else{
				url::redirect("/pages/campaignoverview");	
			}
		}
	}		
	
	
	public function deleteCampaign(){
		$campId = $this->uri->segment(4,"");
		$this->template->clientname = $this->client->name;
		if($campId == ""){
			url::redirect("/pages/campaignoverview");
		}else{
			
			$c = ORM::factory("omm_campaign")->find($campId);
			
			if($c->loaded && $c->omm_client_id == $this->client->id){
				$c->deleteCampaign();
				unset($_SESSION['selected_campaign']);
				url::redirect("/pages/campaignoverview");	
			}else{
				url::redirect("/pages/campaignoverview");	
			}
		}
	}		
	
	public function selectcampaignforedit(){
		$campId = $this->uri->segment(4,"");

		if($campId == ""){
			url::redirect("/pages/campaignoverview");
		}else{
			
			$c = ORM::factory("omm_campaign")->find($campId);
			
			if($c->loaded){
				$_SESSION['selected_campaign'] = $c;
				url::redirect("/pages/campaigndetail");	
				
			}else{
				url::redirect("/pages/campaignoverview");	
			}
		}
	}	

	public function deselect(){
		$campId = $this->uri->segment(4,"");

		if($campId == ""){
			url::redirect("/pages/campaignoverview");
		}else{

			if(isset($_SESSION['selected_campaign']) && $campId == $_SESSION['selected_campaign']){
				unset($_SESSION['selected_campaign']);
				url::redirect("/pages/campaignoverview");
			}
			
		}
	}		
	
	
	
	public function newcampaign(){
		$campId = $this->uri->segment(4,"");

		if(isset($_SESSION['selected_campaign'])){
			unset($_SESSION['selected_campaign']);
		}
		
		url::redirect("/pages/campaigndetail");	
	}	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}