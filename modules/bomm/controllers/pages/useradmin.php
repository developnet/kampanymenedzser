<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Useradmin_Controller extends Ommpage_Controller {

	var $_pageview = "useradmin_overview";
	var $_pageview_detail = "useradmin_add";
	var $_pagetemplate;
	var $client;

	public function __construct(){
		$this->maintemplate = "main";
		parent::__construct();
	
		
		if (!$this->auth->logged_in(Admin_Controller::$accadmin_role)){
			url::redirect('/');
		}
		
		$this->template->bodyClass = "clientdashboard";
	}

	public function index()	{
		url::redirect("pages/useradmin/overview");
	}

	public function selectuser($id){
		$u = ORM::factory('user')->where("km_user", meta::getKMuser())->find($id);
		
		if($u->loaded){
			$_SESSION['selected_user'] = $u;
			url::redirect('pages/useradmin/add');
		}else{
			url::redirect('pages/useradmin/overview');
		}
		
	}

	public function deleteuser($id){
		$template = ORM::factory('user')->where("km_user", meta::getKMuser())->find($id);
		
		if($template->loaded){
			$template->delete();
			meta::createAlert("succes","Sikeres törlés!","A felhasználó törölve lett!");
			url::redirect('pages/useradmin/overview');
		}else{
			url::redirect('pages/useradmin/overview');
		}
		
		
		
	}
	
	public function overview()	{

		$this->pageview = $this->_pageview;
		$this->init();

		if(isset($_SESSION['selected_user'])){
			unset($_SESSION['selected_user']);
		}
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);
		}else{
			$this->pagetemplate->alert = ""; 
		}		
		
		$this->pagetemplate->errors = ""; 

		$users = ORM::factory('user')->where("km_user", meta::getKMuser())->orderby("lastname","asc")->orderby("firstname","asc")->find_all();
			

		$this->pagetemplate->users = $users;
			
			
			
		$this->render();
	}

	public function add()	{

		$this->pageview = $this->_pageview_detail;
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['errors'])){
			$this->pagetemplate->errors = $_SESSION['errors'];
			unset($_SESSION['errors']);
		}else{
			$this->pagetemplate->errors = ""; //clearfixError
		}

		//$templates = ORM::factory('omm_letter_template')->where("omm_client_id", $this->client->id)->find_all();
			
		$form = array(
	        'username'      => '',
			'lastname'      => '',
	        'firstname'   	=> '',
	        'email'  		=> '',
	        'jelszo'  	=> '',
		 	'password2'  	=> '',
			'note'			=> '',
			'status'		=> ''
	        );

	        $errors = $form;
	        $classes = $form;


	        if(isset($_SESSION['selected_user'])){
	        	$this->pagetemplate->mod = "mod";
	        	$user = $_SESSION['selected_user'];

	        	$form = array(
		        'username'      => $user->username,
	        	'lastname'      => $user->lastname,
		        'firstname'   	=> $user->firstname,
		        'email'  		=> $user->email,
		        'jelszo'  	=> '',
	        	'password2'  	=> '',
	        	'note'  		=> $user->note,
	        	'status'		=> $user->status			
	        	);
	        		
	        }else{/////új felvitel
	        	$this->pagetemplate->mod = "";
	        	$user = ORM::factory("user");
	        	$user->code = string::random_string('unique');
	        }

	        if ($_POST){
	        	$post = new Validation($_POST);

	        	$post->pre_filter('trim', TRUE);
	        		
	        	$post->add_rules('username','required', 'length[4,255]');
	        	$post->add_rules('lastname','required', 'length[0,255]');
	        	$post->add_rules('firstname','required', 'length[0,255]');
	        	$post->add_rules('email','required', 'length[0,255]','email');
	        	$post->add_rules('jelszo','matches[password2]', 'length[8,255]');
	        	//$post->add_rules('html', 'required');

	        	if ($post->validate())  {

	        		$user->km_user = USERNAME;
	        		$user->username = $post->username;
	        		$user->lastname = $post->lastname;
	        		$user->firstname = $post->firstname;
	        		$user->email = $post->email;
	        		$user->status = $post->status;
	        		
	        		if($post->jelszo != ""){
	        			$user->password = $post->jelszo;
	        		}
	        		
	        		$user->note = $post->note;	        		 
	        		
	        		$user->save();
	        		
	        		if(sizeof($user->roles)==0){
	        			$user->add(ORM::factory('role', 'login'));
	        			$user->save();
	        		}
					

					
					
	        		if($post->mod == "mod"){
	        			meta::createAlert("succes","Sikeres módosítás!","A felhasználó adatainak módosítása sikeres volt.");
	        		}else{
	        			meta::createAlert("succes","Sikeres hozzáadás!","Az új felhasználó sikeresen hozzá lett adva.");
	        		}


	        		url::redirect('pages/useradmin/overview');

	        		 
	        		 
	        	} // No! We have validation errors, we need to show the form again, with the errors
	        	else {
	        		// repopulate the form fields
	        		$form = arr::overwrite($form, $post->as_array());

	        		// populate the error fields, if any
	        		// We need to already have created an error message file, for Kohana to use
	        		// Pass the error message file name to the errors() method

	        		$errors = arr::overwrite($errors, $post->errors('form_errors_useradmin'));

	        		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
	        		$errorTempl->errors = $errors;

	        		$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

	        		foreach ($errors as $key => $error){
	        			if($error != ""){
	        				$classes[$key] = "clearfixError";
	        			}
	        		}



	        	}
	        }



	        $this->pagetemplate->user = $user;

	        $this->pagetemplate->classes = $classes;
	        $this->pagetemplate->form = $form;
	        	
	        	
	        $this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}