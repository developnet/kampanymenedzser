<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Subscribers_Controller extends Ommpage_Controller {
	
	var $_pageview = "subscribers_overview";
	var $_pagetemplate;
	var $client;
	var $list;
	var $rowperpage = 100;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		$this->template->bodyClass = "subscriberlist";
	}
	
//	public function generateTestData(){
//		
//		for ($i=0;$i<999;$i++){
//		$this->list->addTestMember();	
//		}
//		
//		
//		
//	}
	
	
	public function index()	{
		set_time_limit(0);
		$selected = false;
		$uriparam = "";
		
		if(isset($_GET['show']) && $_GET['show'] == 'selected'){
			$uriparam = '?show=selected';
			
			if(isset($_SESSION['selected_members'])){
				$memids = $_SESSION['selected_members'];
				$selected = true;
			}else{
				$memids = array();
			}
		
		}
		
		$memberStatus = $this->uri->segment(4,"active");
		$orderby = $this->uri->segment(5,"reg_date");
		$order = $this->uri->segment(6,"desc");
		$listfilter = $this->uri->segment(7,"all");
		$groupfilter = $this->uri->segment(8,"all");
		$tagfilter = $this->uri->segment(9,"all");
		$productfilter = $this->uri->segment(10,"all");
		$export = $this->uri->segment(11,"none");

		$rdate_from = "";
		if(isset($_GET['rdate_from']) && $_GET['rdate_from'] != ''){
			$rdate_from = $_GET['rdate_from'];
		}

		$rdate_to = "";
		if(isset($_GET['rdate_to']) && $_GET['rdate_to'] != ''){
			$rdate_to = $_GET['rdate_to'];
		}		
		
//
		
		if(isset($_GET['show']) && $_GET['show'] == 'delselected'){
			unset($_SESSION['selected_members']);
			url::redirect("/pages/subscribers/index/$memberStatus/$orderby/$order/$listfilter/$groupfilter/$tagfilter/$productfilter");
		}		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		
		$this->pagetemplate->rdate_from = $rdate_from;
		$this->pagetemplate->rdate_to = $rdate_to;		
		
		//var_dump($_GET);
		
		$this->template->clientname = $this->client->name;
		
		$this->pagetemplate->uriparam = $uriparam;
		
		$this->pagetemplate->memberStatus = $memberStatus;
		$this->pagetemplate->orderby = $orderby;
		$this->pagetemplate->order = $order;		
		$this->pagetemplate->listfilter = $listfilter;		
		$this->pagetemplate->groupfilter = $groupfilter;
		$this->pagetemplate->tagfilter = $tagfilter;
		$this->pagetemplate->productfilter = $productfilter;			
		
		if($productfilter != "all"){
			$filteredproducts = explode("_", $productfilter);//
		}else{
			$filteredproducts = array();
		} 
		$this->pagetemplate->filteredproducts = $filteredproducts;		
		
		
		if($listfilter != "all"){
			$filteredlists = explode("_", $listfilter);//
		}else{
			$filteredlists = array();
		} 
		$this->pagetemplate->filteredlists = $filteredlists;

		if($groupfilter != "all"){
			$filteredgroups = explode("_", $groupfilter);//
		}else{
			$filteredgroups = array();
		} 
		$this->pagetemplate->filteredgroups = $filteredgroups;		
		
		if($tagfilter != "all"){
			$filteredtags = explode("_", $tagfilter);//
		}else{
			$filteredtags = array();
		} 
		$this->pagetemplate->filteredtags = $filteredtags;	

		
		
		
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		$this->pagetemplate->client = $this->client; //		
		
		$this->pagetemplate->memberStatus = $memberStatus;
		
		if($groupfilter != "all"){
			$allfields = $this->client->getAllFields($filteredlists[0]);
		}else{
			$allfields = array();
		} 		
		
			
		$searchfields = $this->client->getFields();
		
		$fields = $this->client->gridFields();
		$this->pagetemplate->gridFields = $fields;		
		
		$keyword = "";		
		$searchfield = "none";
		if(isset($_GET['searchfield'])){///keresés
				
				if($_GET['searchfield'] == 'detail'){
					$searchfield = 'detail';
					$uriparam = '?searchfield=detail';
					
					$keyword = array();
					
					foreach($searchfields as $f){
						
						if(isset($_GET[$f->reference])){
							$uriparam .= '&'.$f->reference.'='.$_GET[$f->reference];
							$keyword[$f->reference] = urldecode($_GET[$f->reference]); 
						}
						
					}
					
				}else{

					if($_GET['keyword'] != ""){
			 		
				 		$keyword = urldecode($_GET['keyword']);
				 		
				 		$searchfield = "all";
				 		
				 		$uriparam = '?searchfield=all&keyword='.$_GET['keyword'];
				 		
				 	}					
					
				}
			
			

		}
		
		$this->pagetemplate->uriparam = $uriparam;
		if(!is_array($keyword)){
			$this->pagetemplate->main_searchkeyword = urldecode($keyword);
		}else{
			
			if($searchfield == 'detail'){
				$this->pagetemplate->main_searchkeyword = "<Részletes keresés>";	
			}else{
				$this->pagetemplate->main_searchkeyword = "";
			}
				
		}
		
		
		
			if($selected){
				
				$this->pagetemplate->allActiveMember = $this->client->membersNumberSelected('active',$memids);
			
				
				$this->pagetemplate->activeMemberCount = $this->pagetemplate->allActiveMember;
				$this->pagetemplate->unsubscribedMemberCount = $this->client->membersNumberSelected('unsubscribed',$memids);
				$this->pagetemplate->deletedMemberCount = $this->client->membersNumberSelected('deleted',$memids);	
				$this->pagetemplate->preregMemberCount = $this->client->membersNumberSelected('prereg',$memids);
				$this->pagetemplate->errorMemberCount = $this->client->membersNumberSelected('error',$memids);				
				
				
			}else{
				$this->pagetemplate->allActiveMember = $this->client->membersNumber($allfields,'active',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields,$rdate_from,$rdate_to);
			
				
				$this->pagetemplate->activeMemberCount = $this->pagetemplate->allActiveMember;
				$this->pagetemplate->unsubscribedMemberCount = $this->client->membersNumber($allfields,'unsubscribed',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields,$rdate_from,$rdate_to);
				$this->pagetemplate->deletedMemberCount = $this->client->membersNumber($allfields,'deleted',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields,$rdate_from,$rdate_to);	
				$this->pagetemplate->preregMemberCount = $this->client->membersNumber($allfields,'prereg',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields,$rdate_from,$rdate_to);
				$this->pagetemplate->errorMemberCount = $this->client->membersNumber($allfields,'error',$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfield,$keyword,$searchfields,$rdate_from,$rdate_to);				
				
			}
		

			
			if($memberStatus == 'active'){
				$allnum = $this->pagetemplate->activeMemberCount;
			}elseif($memberStatus == 'unsubscribed'){
				$allnum = $this->pagetemplate->unsubscribedMemberCount;
			}elseif($memberStatus == 'deleted'){
				$allnum = $this->pagetemplate->deletedMemberCount;
			}elseif($memberStatus == 'prereg'){
				$allnum = $this->pagetemplate->preregMemberCount;
			}elseif($memberStatus == 'error'){
				$allnum = $this->pagetemplate->errorMemberCount;
			}
			
			if($selected){
				$allnum = sizeof($memids);
			}
			
			
			$pages = Pagination::factory(array
			(
			    'items_per_page' => $this->rowperpage,
			    'query_string' => 'page',
			    'total_items' => $allnum
			));				
			

		


		
			if(isset($_GET['page']))
				$page = $_GET['page'];
			else
				$page = 1;		
		
			if($page == 1){
				$offset = 0;
			}else{
				$page = $page-1;
				$offset = $page*$this->rowperpage;
			}
				
		
	
							
		
		if($this->client->membersNumber($memberStatus) > $this->rowperpage){
			$this->pagetemplate->pagination = $pages->render("ommpagination");		
		}else{
			$this->pagetemplate->pagination = "";
		}
		
		if($export != "none"){
			$rowperpage = 50000;
			$offset = 0;
			$exp = true;
		}else{
			$rowperpage = $this->rowperpage;
			$exp = false;
		}
		
		if($selected){
			$_members = $this->client->getMembersForSubscribersTableSelected($allfields,$memberStatus,$orderby,$order,$offset,$rowperpage,$memids,$searchfields,$exp);
		}else{
			$_members = $this->client->getMembersForSubscribersTable($allfields,$memberStatus,$orderby,$order,$offset,$rowperpage,$searchfield,$keyword,$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfields,$rdate_from,$rdate_to,$exp);	
		}
		
		
		
		if($export != "none"){
			
			$csv = "regdatum;email;statusz;tag;";
			foreach ($searchfields as $f){
				$csv .= $f->reference.';';
			}
			
			$csv .= "\n";			
			$db = new Database('own');
			foreach($_members as $m){
				
				$csv .= $m->reg_date.";".$m->email.";";
	
				switch ($m->status){
					case 'active':
						$csv .= '1;';
						break;
					case 'unsubscribed':
						$csv .= '2;';
						break;
					case 'deleted':
						$csv .= '3;';
						break;
					case 'error':
						$csv .= '4;';
						break;
					case 'prereg':
						$csv .= '5;';
						break;
					default:
						$csv .= '1;';
						break;
				}
				
				
				
				//cimkék
				$csv .= Omm_list_member_Model::_getTags(true,$justTag = NULL,$m->id,$db).";";		
			
				foreach ($searchfields as $f){
	
					$ref = $f->reference;
	
					if($f->type == "singleselectradio" || $f->type == "singleselectdropdown"){
	
						$code = $m->$ref;
	
						$csv .= $f->getValueFromCode($code).';';
	
					}elseif($f->type == "multiselect"){
	
						//EePg4fd7,q82ikgWE,Hhe7hC7l,
	
						$codes = $m->$ref;
	
						$codesArray = explode(",",$codes);
	
						$tocsv = '';
						foreach ($codesArray as $c){
							if($c != ""){
								$tocsv .= $f->getValueFromCode($c).",";
							}
								
						}
	
						$tocsv = substr($tocsv,0,sizeof($tocsv)-2);
						$csv .= $tocsv.';';
					}else{
						$csv .= $m->$ref.';';
					}
	
	
	
				}
				$csv .= "\n";
	
			}
	
	
			//	    $csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
			$csv = string::translateUTF8ToWindowsCP1252($csv);
			$csv = mb_convert_encoding($csv, 'ISO-8859-2',  'UTF-8');
			
			header('Content-type: text/csv; charset=ISO-8859-2');
			header('Content-disposition: attachment;filename='."export_subscribers_".date("YmdHis")."_".$_SESSION['auth_user']->username.".csv");
			
			
			echo $csv;				
			die();
			
				
			
		}else{
			$this->pagetemplate->members = $_members;
		}		
		
		
		if(isset($_POST['selection_tag'])){
			$post = new Validation($_POST);
			$selection = $post->selection_tag;
			
			$memids = array();
			
			if($selection == 'selected'){
				
				if(isset($_SESSION['selected_members'])){
					
					foreach($_SESSION['selected_members'] as $m){
						$memids[] = $m;
					}
					
					
				}else{
					$memids = array();	
				}
				
								
			}elseif($selection == 'all'){
				$mms = $this->client->getMembersForSubscribersTable($allfields,$memberStatus,$orderby,$order,0,100000,$searchfield,$keyword,$listfilter,$groupfilter,$tagfilter,$productfilter,$searchfields);
			   	foreach($mms as $m){
					$memids[] = $m->id;
        		}				
				
			}
			
        	$tags = explode(",", $post->as_values_tags);
        	$_tags = array();
        	foreach($tags as $tag){
        		if (is_numeric($tag)){
        			$_tags[] = array('id' => $tag);
        		}elseif($tag != ""){//
        			
					$tago = ORM::factory("omm_tag");		
					$tago->create($tag,'active');
					$tago->saveObject();        			
        			
        			$_tags[] = array('id' => $tago->id);
        		}
        	}
        	

        	
        	
        	foreach($memids as $m){
				
        		if($post->mod_tag == 'add'){
        			
        			Omm_list_member_Model::addTagsJusIds($this->client->getDB(),$m, $_tags);
        				
        		}elseif($post->mod_tag == 'del'){
        			
        			Omm_list_member_Model::delTagsByJustId($this->client->getDB(),$m, $_tags);
        			
        		}
        		
        			
        	}
        	
        	
        	//$member->addTags($_tags);			
			
			
		
		}
		
		
		$this->render();
	}

	
	public function deleteAllMember(){
		
		$count = $this->list->deleteAll();
		
		meta::createAlert("succes","Sikeres törlés!","A feliratkozók törölve lettek a listáról! (".$count.")");
		
		url::redirect("/pages/listdetail");
	}
	
	public function selectmember(){
		
		$_SESSION['listdetailreferrer'] = $_SERVER['HTTP_REFERER'];
		
		
		$mid = $this->uri->segment(4,"");

		if($mid == ""){
			url::redirect("/pages/listdetail");
		}else{
			
			$_SESSION['selected_member'] = $mid;
			url::redirect("/pages/memberdetail/index/".$mid);	
		}
		
	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		KOHANA::show_404(FALSE,FALSE);
	}

}