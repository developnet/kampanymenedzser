<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Statisticdetails_Controller extends Ommpage_Controller {

	var $_pageview_letter_statistic = "statistic_letter_overview";
	var $_pageview_letter_statistic_recip = "statistic_letter_recipients";
	var $_pageview_letter_statistic_link = "statistic_letter_links";

	var $_pagetemplate;
	var $client;
	var $campaign;

	var $rowperpage = 30;

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();

		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_campaign'])){
			$this->campaign = $_SESSION['selected_campaign'];
		}else{
			url::redirect("/pages/campaignoverview");
		}

		$this->template->bodyClass = "subscriberlist";
	}


	public function index(){
		url::redirect('/pages/statisticoverview');
	}



	public function letter(){
		$this->pageview = $this->_pageview_letter_statistic;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->letter = $letter;
		}else{
			url::redirect('/pages/statisticoverview');
		}
		
		
		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function recipients(){
		
		if(isset($_POST['selection_tag'])){
			
			if(isset($_SESSION['selected_letter'])){
				$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			}else{
				url::redirect('/pages/statisticoverview');
			}			
			
			$post = new Validation($_POST);
			$selection = $post->selection_tag;			
			
			$memids = array();
			
			$mms = $this->pagetemplate->members = $letter->getMembersBySending($selection,"email","asc" ,0,0, TRUE);
			
			foreach($mms as $m){
				$memids[] = $m->id;
        	}				
			
		    $tags = explode(",", $post->as_values_tags);
        	$_tags = array();
        	foreach($tags as $tag){
        		if (is_numeric($tag)){
        			$_tags[] = array('id' => $tag);
        		}elseif($tag != ""){//
        			
					$tago = ORM::factory("omm_tag");		
					$tago->create($tag,'active');
					$tago->saveObject();        			
        			
        			$_tags[] = array('id' => $tago->id);
        		}
        	}
        	

        	
        	
        	foreach($memids as $m){
				
        		if($post->mod_tag == 'add'){
        			
        			Omm_list_member_Model::addTagsJusIds($this->client->getDB(),$m, $_tags);
        				
        		}elseif($post->mod_tag == 'del'){
        			
        			Omm_list_member_Model::delTagsByJustId($this->client->getDB(),$m, $_tags);
        			
        		}
        	}			
			
        	meta::createAlert("succes","Sikeres szerkesztés!","A címkék szerkesztése megtörtént! (".sizeof($memids)." db feliratkozó módosítva)");
        	
			url::redirect('/pages/statisticdetails/recipients');
		}//////post selection_tag		
		
		
		$this->pageview = $this->_pageview_letter_statistic_recip;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}


		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->letter = $letter;
			
			//meta::createLetterSystemMessage($letter);
			
		}else{
			url::redirect('/pages/statisticoverview');
		}


		$memberStatus = $this->uri->segment(4,"all");
		$orderby = $this->uri->segment(5,"email");
		$order = $this->uri->segment(6,"asc");


		$this->pagetemplate->memberStatus = $memberStatus;
		$this->pagetemplate->orderby = $orderby;
		$this->pagetemplate->order = $order;



		$stat = $letter->getLetterLinkStat();

		if($memberStatus == "all"){
			$allmember = $stat["sentNumber"];
		}else if($memberStatus == "opened"){
			$allmember = $stat["uniqueOpens"];
		}else if($memberStatus == "clicked"){
			$allmember = $stat["uniqueClicks"];
		}else if($memberStatus == "unsubscribed"){
			$allmember = $stat["unsubscribeClicks"];
		}else if($memberStatus == "temperror"){
			$allmember = $stat["tempErrors"];
		}else if($memberStatus == "permerror"){
			$allmember = $stat["permErrors"];
		}
		
		$allmember = $allmember - sizeof($stat['deletedRecipients']);
		
		if($allmember<0) $allmember = 0;
		
		$this->pagetemplate->deletedClicks = $stat['allClicks_deleted'];

		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}
		else{
			$page = 1;
		}
		if($page == 1){
			$offset = 0;
		}else{
			$page = $page-1;
			$offset = $page*$this->rowperpage;
		}
			
		$pages = Pagination::factory(array
		(
			    'items_per_page' => $this->rowperpage,
			    'query_string' => 'page',
			    'total_items' => $allmember
		));

		if($allmember > $this->rowperpage){
			$this->pagetemplate->pagination = $pages->render("ommpagination");
		}else{
			$this->pagetemplate->pagination = "";
		}
		$this->pagetemplate->bounceDict = meta::getBounceDict();
		$this->pagetemplate->allmember = $allmember;
		$this->pagetemplate->members = $letter->getMembersBySending($memberStatus,$orderby,$order,$offset,$this->rowperpage);
		
		$this->pagetemplate->deletedRecipients = $stat['deletedRecipients'];


		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function link(){
		$this->pageview = $this->_pageview_letter_statistic_link;
		$this->init();
		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";

		$this->pagetemplate->errors = ""; //clearfixError

		if(isset($_SESSION['error'])){
			$this->pagetemplate->errors = $_SESSION['error'];
			unset($_SESSION['error']);
		}


		if(isset($_SESSION['selected_letter'])){
			$letter = ORM::factory('omm_letter')->find($_SESSION['selected_letter']);
			$this->pagetemplate->letter = $letter;
		}else{
			url::redirect('/pages/statisticoverview');
		}


		$this->pagetemplate->letter = $letter;
		$this->render();
	}

	public function selectmember(){
		
		$_SESSION['listdetailreferrer'] = $_SERVER['HTTP_REFERER'];
		$mid = $this->uri->segment(4,"");
		url::redirect("/pages/memberdetail/index/".$mid);

	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}