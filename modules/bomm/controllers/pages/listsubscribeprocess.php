<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listsubscribeprocess_Controller extends Ommpage_Controller {

	var $_pageview = "lists_subscribeprocess";
	var $_pagetemplate;
	var $client;
	var $list;

	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();

		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}



		$this->template->bodyClass = "subscriberlist";
	}

	//	public function generateTestData(){
	//
	//		for ($i=0;$i<999;$i++){
	//		$this->list->addTestMember();
	//		}
	//
	//
	//
	//	}

	public function index()	{

		if(isset($_FILES['htmlFile_1'])){
			if (upload::valid($_FILES['htmlFile_1']) && upload::type($_FILES['htmlFile_1'],array('html'))){//////HTML upload
				$filename = upload::save('htmlFile_1');

				if($filename != ""){
					$content = fileutils::file_get_contents_utf8($filename);

					if($this->list->verifycation_email_id == NULL){
						$id = 0;
					}else{
						$id = $this->list->verifycation_email_id;
					}

					$verifycation_email = ORM::factory("omm_common_letter")->find($id);

					if((bool)$verifycation_email->trySetHtml($content)){

						$verifycation_email->type = "html";
						$verifycation_email->saveObject();
						$this->list->verifycation_email_id = $verifycation_email->id;
						$this->list->saveObject();
						$_SESSION['htmlerror1'] = meta::createAlert("succes","Sikeres feltöltés!","A HTML fájl betöltése sikeres volt.", true);
					}else{
						$_SESSION['htmlerror1'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban nincsen body tag.", true);
					}



					unlink($filename);
					url::redirect("/pages/listsubscribeprocess#html_verifycation");
				}else{
					//$_SESSION['htmlerror1'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
				}



			}else{
				//$_SESSION['htmlerror1'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
			}
		}

		if(isset($_FILES['htmlFile_2'])){
			if (upload::valid($_FILES['htmlFile_2']) && upload::type($_FILES['htmlFile_2'],array('html'))){//////HTML upload
				$filename = upload::save('htmlFile_2');

				if($filename != ""){

					$content = fileutils::file_get_contents_utf8($filename);

					if($this->list->subscribe_conf_email_id == NULL){
						$id = 0;
					}else{
						$id = $this->list->subscribe_conf_email_id;
					}

					$subscribe_conf_email = ORM::factory("omm_common_letter")->find($id);

					if((bool)$subscribe_conf_email->trySetHtml($content)){

						$subscribe_conf_email->type = "html";
						$subscribe_conf_email->saveObject();
						$this->list->subscribe_conf_email_id = $subscribe_conf_email->id;
						$this->list->saveObject();
						$_SESSION['htmlerror2'] = meta::createAlert("succes","Sikeres feltöltés!","A HTML fájl betöltése sikeres volt.", true);
					}else{
						$_SESSION['htmlerror2'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban nincsen body tag.", true);
					}

					unlink($filename);
					url::redirect("/pages/listsubscribeprocess#html_subscribe");
				}else{
					//$_SESSION['htmlerror1'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
				}



			}else{
				//$_SESSION['htmlerror1'] = meta::createAlert("error","Hibás html fájl!","A feltöltött html fájlban hiba van.", true);
			}
		}

		///////html upload 1

		$memberStatus = $this->uri->segment(4,"active");
		$orderby = $this->uri->segment(5,"reg_date");
		$order = $this->uri->segment(6,"desc");

		$this->pageview = $this->_pageview;
		$this->init();

		$this->template->clientname = $this->client->name;
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		$this->pagetemplate->errors = ""; //clearfixError



		$form = array(
			'initial_conf_page'					=> "",

	        'verifycation_type'   				=> "",
			
			'verifycation_sender_name_text'		=> "",
			'verifycation_sender_email_text'	=> "",
			'verifycation_subject_text'			=> "",
			'verifycation_text_content_text'	=> "",

			'verifycation_sender_name_html'		=> "",
			'verifycation_sender_email_html'	=> "",
			'verifycation_subject_html'			=> "",
			'verifycation_html_content_html'	=> "",
			'verifycation_text_content_html'	=> "",
			
			'second_conf_page'					=> "",

	        'subscribe_conf_type'   			=> "",
			
			'subscribe_conf_sender_name_text'	=> "",
			'subscribe_conf_sender_email_text'	=> "",
			'subscribe_conf_subject_text'		=> "",
			'subscribe_conf_text_content_text'	=> "",

			'subscribe_conf_sender_name_html'	=> "",
			'subscribe_conf_sender_email_html'	=> "",
			'subscribe_conf_subject_html'		=> "",
			'subscribe_conf_html_content_html'	=> "",
			'subscribe_conf_text_content_html'  => "",
			'second_subscribe_page'				=> "",
			'error_subscribe_page'				=> ""
			);

			$errors  = $form;
			$classes = $form;

		 if ($_POST){
		 	 
		 	$post = new Validation($_POST);
		 	 
		 	$post->pre_filter('trim');


		 	$post->add_rules('initial_conf_page','required','url');


		 	$post->add_rules('second_subscribe_page','required','url');
		 	$post->add_rules('error_subscribe_page','required','url');


		 	if($this->list->type == "double"){
		 		$post->add_rules('verifycation_type','required');
		 		$post->add_rules('second_conf_page','required','url');
		 	}

		 	$post->add_rules('subscribe_conf_type','required');


		 	///////////////////////////////MENTÉS
		 	if ($post->validate())  {

		 		$this->list->initial_conf_page = $post->initial_conf_page;
		 		$this->list->second_conf_page = $post->second_conf_page;

		 		$this->list->second_subscribe_page = $post->second_subscribe_page;
		 		$this->list->error_subscribe_page = $post->error_subscribe_page;

		 		if($post->verifycation_type == "text"){
		 			$this->list->verifycation_email_id = $post->verifycation_email_id;
		 				
		 		}elseif($post->verifycation_type == "html" && isset($post->verifycation_email_id)){
					$this->list->verifycation_email_id = $post->verifycation_email_id;
		 				
		 		}else{
		 			$this->list->verifycation_email_id = NULL;
		 		}

		 		$subscribe_conf_email->type = $post->subscribe_conf_type;

		 		if($post->subscribe_conf_type == "text" && isset($post->subscribe_conf_email_id)){
		 			$this->list->subscribe_conf_email_id = $post->subscribe_conf_email_id;
		 				
		 		}elseif($post->subscribe_conf_type == "html" && isset($post->subscribe_conf_email_id)){
		 			$this->list->subscribe_conf_email_id = $post->subscribe_conf_email_id;
		 				
		 		}else{
		 			$this->list->subscribe_conf_email_id = NULL;
		 		}

		 		$this->list->saveObject();


		 		unset($_POST);

		 		meta::createAlert("succes","A felirtkozási folyamat sikeresen elmentve!","");
		 		url::redirect("/pages/listsubscribeprocess");

		 	} // HIBA
		 	else {
		 		$form = arr::overwrite($form, $post->as_array());

		 		$errors = arr::overwrite($errors, $post->errors('form_errors_listsubscribe'));

		 		$errorTempl = new View(Kohana::config('admin.theme')."/common/errors");
		 		$errorTempl->errors = $errors;

		 		$this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);

		 		foreach ($errors as $key => $error){
		 			if($error != ""){
		 				$classes[$key] = "clearfixError";
		 			}
		 		}

		 	}
		 }else{////////////////////////ha nincs post

		 	$form['verifycation_type'] = "text";
		 	$form['subscribe_conf_type'] = "nothing";
		 	 
		 	$form['verifycation_sender_name_text'] = $this->client->sender_name;
		 	$form['verifycation_sender_name_html'] = $this->client->sender_name;

		 	$form['subscribe_conf_sender_name_text'] = $this->client->sender_name;
		 	$form['subscribe_conf_sender_name_html'] = $this->client->sender_name;
		 	 

		 	$form['verifycation_sender_email_text'] = $this->client->sender_email;
		 	$form['verifycation_sender_email_html'] = $this->client->sender_email;

		 	$form['subscribe_conf_sender_email_text'] = $this->client->sender_email;
		 	$form['subscribe_conf_sender_email_html'] = $this->client->sender_email;

		 	if($this->list->initial_conf_page != NULL){
		 		$form['initial_conf_page'] = $this->list->initial_conf_page;
		 	}
		 	 
		 	if($this->list->second_subscribe_page != NULL){
		 		$form['second_subscribe_page'] = $this->list->second_subscribe_page;
		 	}
		 	 

		 	if($this->list->error_subscribe_page != NULL){
		 		$form['error_subscribe_page'] = $this->list->error_subscribe_page;
		 	}
		 	 

		 	if($this->list->second_conf_page != NULL){
		 		$form['second_conf_page'] = $this->list->second_conf_page;
		 	}
		 	 
		 	if($this->list->verifycation_email_id != NULL){
		 		$form['verifycation_type'] = "html";
		 	}

		 	 

		 	if($this->list->subscribe_conf_email_id != NULL){
				$form['subscribe_conf_type'] = "html";

		 	}


		 }




			if(isset($_SESSION['htmlerror1'])){
				$this->pagetemplate->htmlerrors1 = $_SESSION['htmlerror1'];
				unset($_SESSION['htmlerror1']);
			}else{
				$this->pagetemplate->htmlerrors1 = "";
			}

			if(isset($_SESSION['htmlerror2'])){
				$this->pagetemplate->htmlerrors2 = $_SESSION['htmlerror2'];
				unset($_SESSION['htmlerror2']);
			}else{
				$this->pagetemplate->htmlerrors2 = "";
			}

			$this->pagetemplate->templates = $this->client->tempaltes();
		 $this->pagetemplate->form = $form;
		 $this->pagetemplate->classes = $classes;
		 $this->pagetemplate->listName = $this->list->name;

		 $this->pagetemplate->lists = ORM::factory("omm_list")->where("omm_client_id",$this->client->id)->find_all();

		 $this->render();
	}

	public function extracttext($type){
		$this->template->clientname = $this->client->name;

		if($type == "html_verifycation"){

			if($this->list->verifycation_email_id == NULL){
				$id = 0;
			}else{
				$id = $this->list->verifycation_email_id;
			}

			$verifycation_email = ORM::factory("omm_common_letter")->find($id);
			$verifycation_email->type = "html";
			$verifycation_email->text_content = $verifycation_email->extractText($verifycation_email->html_content);
			$verifycation_email->saveObject();
				
			$_SESSION['htmlerror1'] = meta::createAlert("succes","Sikeres másolás!","A HTML szöveg sikeresen át lett másolva.", true);
			url::redirect("/pages/listsubscribeprocess#html_verifycation");

		}else{

			if($this->list->subscribe_conf_email_id == NULL){
				$id = 0;
			}else{
				$id = $this->list->subscribe_conf_email_id;
			}

			$subscribe_conf_email = ORM::factory("omm_common_letter")->find($id);
			$subscribe_conf_email->type = "html";
			$subscribe_conf_email->text_content = $subscribe_conf_email->extractText($subscribe_conf_email->html_content);
			$subscribe_conf_email->saveObject();

			
			$_SESSION['htmlerror2'] = meta::createAlert("succes","Sikeres másolás!","A HTML szöveg sikeresen át lett másolva.", true);
			url::redirect("/pages/listsubscribeprocess#html_subscribe");	
	}
}

public function usetemplate($type,$id){
	$this->template->clientname = $this->client->name;

	if($id != ""){

		$templ = ORM::factory('omm_letter_template')->find($id);

		if($templ->loaded){

			$templ->replaceUrl(url::base()."files/".$this->client->id."/images/");

			if($type == "html_verifycation"){

				if($this->list->verifycation_email_id == NULL){
					$id = 0;
				}else{
					$id = $this->list->verifycation_email_id;
				}

				$verifycation_email = ORM::factory("omm_common_letter")->find($id);

				if((bool)$verifycation_email->trySetHtml($templ->html)){

					$verifycation_email->type = "html";
					$verifycation_email->saveObject();
					$this->list->verifycation_email_id = $verifycation_email->id;
					$this->list->saveObject();
					$_SESSION['htmlerror1'] = meta::createAlert("succes","Sikeres betöltés!","A sablon betöltése sikeres volt.", true);
				}else{
					$_SESSION['htmlerror1'] = meta::createAlert("error","Sikertelen!","A sablon betöltése sikertelen volt.", true);
				}

				url::redirect("/pages/listsubscribeprocess#html_verifycation");

			}else{
				if($this->list->subscribe_conf_email_id == NULL){
					$id = 0;
				}else{
					$id = $this->list->subscribe_conf_email_id;
				}

				$subscribe_conf_email = ORM::factory("omm_common_letter")->find($id);

				if((bool)$subscribe_conf_email->trySetHtml($templ->html)){

					$subscribe_conf_email->type = "html";
					$subscribe_conf_email->saveObject();
					$this->list->subscribe_conf_email_id = $subscribe_conf_email->id;
					$this->list->saveObject();
					$_SESSION['htmlerror2'] = meta::createAlert("succes","Sikeres betöltés!","A sablon betöltése sikeres volt.", true);
				}else{
					$_SESSION['htmlerror2'] = meta::createAlert("error","Sikertelen!","A sablon betöltése sikertelen volt.", true);
				}

				url::redirect("/pages/listsubscribeprocess#html_subscribe");
			}
		}
	}
}


/**
 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
 */
public function __call($method, $arguments)
{
	$this->auto_render = FALSE;
	KOHANA::show_404(FALSE,FALSE);
}

}