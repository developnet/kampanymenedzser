<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Accountoverview_Controller extends Ommpage_Controller {

	var $_pageview = "account_overview";
	var $_pageview_actmonth = "account_actmonth";
	var $_pageview_prevmonth = "account_prevmonth";
	var $_pageview_billing = "account_billing";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "main";
		parent::__construct();
		$this->template->bodyClass = "clientdashboard";
		
		if (!$this->auth->logged_in(Admin_Controller::$accadmin_role)){
			url::redirect('/');
		}		
		
	}
	//TODO a módosítást megcsinálni
	public function index()	{

		$this->pageview = $this->_pageview;
		$this->init();

		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError
		
		if($_SESSION['auth_user']->km_user == "admin"){
			$km_user = USERNAME;
		}else{
			$km_user = $_SESSION['auth_user']->km_user;
		}
		
		$account = ORM::factory('omm_account')->where("km_user",$km_user)->find();
		 
		if(!$account->loaded){
			$account->km_user = meta::getKMuser();
			$account->saveObject();
		}
		 
		$this->pagetemplate->account = $account;
		 
		 
		 
		 
		$this->render();
	}


	public function actmonth(){
		$this->pageview = $this->_pageview_actmonth;
		$this->init();
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError

		$account = ORM::factory('omm_account')->where("km_user",meta::getKMuser())->find();
		 
		if(!$account->loaded){
			url::redirect("/pages/accountoverview");
		}

		
		$db = new Database("own");
		
		$actmonth = date( "Y-m" );
		
		$sql = "SELECT count(j.id) as ossz
					FROM `omm_day_jobs` dj, omm_jobs j
					WHERE dj.DAY
					BETWEEN '$actmonth-01'
					AND '$actmonth-31'
					AND dj.STATUS = 'sent'
					AND j.omm_day_job_id = dj.id
					AND j.status = 'sent'
					AND j.type != 'test'
					";
		
		$mails = $db->query($sql);

		$sql = "SELECT count(distinct email) as ossz
					FROM omm_list_members WHERE status = 'active'";		
		$emails = $db->query($sql);
		
		$package = meta::getEmailCountAndPackage();
		
		$year = date( "Y" );
		$month = date( "m" );
		
		$this->pagetemplate->month = $year.". ".KOHANA::lang("barendszer.".$month);
		
		$this->pagetemplate->mailcount = $mails[0]->ossz;
		$this->pagetemplate->emailcount = $package['emailcount'];
		$this->pagetemplate->package = $package['package'];
		
		$this->pagetemplate->account = $account;
		

		$this->render();
	}

	public function prevmonth(){
		$this->pageview = $this->_pageview_prevmonth;
		$this->init();
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError


		$this->render();
	}	

	public function billing(){
		$this->pageview = $this->_pageview_billing;
		$this->init();
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError


		$this->pagetemplate->log = meta::getAccountLog();
		$this->pagetemplate->eventdict = meta::getCodeDictArray(23);
		
		$this->render();
	}		
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}