<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Redirect_Controller extends Ommpage_Controller {

	var $_pageview = "user_overview";
	var $_pagetemplate;

	public function __construct(){
		$this->maintemplate = "empty";
		parent::__construct();
		$this->template->bodyClass = "";
		$this->pageview = "redirect";
		$this->init();		
	}
	
	protected function _checkLogin(){
		 
		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
				
			$_SESSION['auth_user'] = ORM::factory('user');
				
		}		
		
		
		 
	}
		
	public function index(){
		$this->pagetemplate->message = "";
		$this->pagetemplate->description = "";
	    $this->render();
	}
	
	/**
	 * Feliratkozás köszönő, megerősítésre figyelmeztető oldal címe
	 * Ez az oldal fog megjelenni sikeres regisztráció után. Kettős Opt-in es lista esetén aktivációs e-mail kiküldése után, aktiváció elött megjelenő oldal.
	 *
	 */
	public function subscribe($page){
		$this->pagetemplate->message = Kohana::lang("kmredirect.$page.message");
		$this->pagetemplate->description = Kohana::lang("kmredirect.$page.description");
	    $this->render();
	}
	
	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)	{
		throw new Kohana_404_Exception("$method");
	}	
	
}

?>