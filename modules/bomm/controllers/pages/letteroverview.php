<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Letteroverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "letters_overview";
	var $_pagetemplate;
	var $client;
	var $campaign;
	
	public function __construct(){
//		url::redirect("/pages/campaignoverview");
		
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		

		if(isset($_SESSION['selected_campaign'])){
			$this->campaign = $_SESSION['selected_campaign'];
		}else{
			url::redirect("/pages/campaignoverview");
		}		
		
		$this->template->bodyClass = "listindex";
	}
	
	public function index()	{
		url::redirect("/pages/campaignoverview");
		
		if(isset($_SESSION['selected_letter'])){
			unset($_SESSION['selected_letter']);
		}
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		
		$this->pagetemplate->alert = "";
		
		
		
		
		$this->pagetemplate->letters = $this->campaign->letters();		
		$this->pagetemplate->drafts = $this->campaign->drafts();
		
		
		$this->render();
	}

	
	public function selectletter(){
		$this->template->clientname = $this->client->name;
		$liId = $this->uri->segment(4,"");

		if($liId == ""){
			url::redirect("/pages/campaignoverview");
		}else{
			
			//$letter = ORM::factory("omm_letter")->find($liId);
			
			if($liId != ""){
				$_SESSION['selected_letter'] = $liId;
				
				$letter = ORM::factory("omm_letter")->find($liId);
				
				url::redirect("/pages/letterdetail/draft");
				
			}else{
				url::redirect("/pages/campaignoverview");	
			}
			
		}
		
	}
	
	public function selectletterforstat(){
		$this->template->clientname = $this->client->name;
		$liId = $this->uri->segment(4,"");

		if($liId == ""){
			url::redirect("/pages/statisticoverview");
		}else{
			
			//$letter = ORM::factory("omm_letter")->find($liId);
			
			if($liId != ""){
				$_SESSION['selected_letter'] = $liId;
				
				$letter = ORM::factory("omm_letter")->find($liId);
				
				url::redirect("/pages/statisticdetails/letter");
				
			}else{
				url::redirect("/pages/statisticoverview");	
			}
			
		}
		
	}	
	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}