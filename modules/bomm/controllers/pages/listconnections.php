<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Listconnections_Controller extends Ommpage_Controller {
	
	var $_pageview = "lists_connections";
	var $_pagetemplate;
	var $client;
	var $list;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}

		if(isset($_SESSION['selected_list'])){
			$this->list = $_SESSION['selected_list'];
		}else{
			url::redirect("/pages/listoverview");
		}

		
		
		$this->template->bodyClass = "subscriberlist";
	}
	
	public function index()	{
		
		
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		$this->pagetemplate->alert = "";
		$this->pagetemplate->errors = ""; //clearfixError		
		
		
		

		 if ($_POST){
		    	
		        $post = new Validation($_POST);
		        $post->pre_filter('trim');
		        
	 
	        ///////////////////////////////MENTÉS
	        if ($post->validate())  {
				
	        	$this->list->deleteConnections("subscribe");
				$this->list->deleteConnections("unsubscribe");
				
	        	if($post->subscribe == "only"){
	        		$this->list->subscribe_connection_type = "only";
	        	}elseif($post->subscribe == "all_lists"){
	        		$this->list->subscribe_connection_type = "all_lists";
	        	}else{
	        		$this->list->subscribe_connection_type = "all";

	        		if(isset($post->subscribeLists)){

	        			 foreach ($post->subscribeLists as $l){
		        			$conn = ORM::factory("omm_list_connection");
		        			$conn->omm_list_id = $this->list->id;
		        			$conn->type = "subscribe";
		        			$conn->other_list_id = $l;
							$conn->save();
		        		}	        			
	        			
	        		}else{
	        			$this->list->subscribe_connection_type = "only";	
	        		}
	        		
	        		
	        	}
	        	
	        	if($post->unsubscribe == "only"){
	        		$this->list->unsubscribe_connection_type = "only";
	        	}elseif($post->unsubscribe == "all_lists"){
	        		$this->list->unsubscribe_connection_type = "all_lists";
	        	}else{
	        		$this->list->unsubscribe_connection_type = "all";
	        		
	        		if(isset($post->unsubscribeLists)){

	        			 foreach ($post->unsubscribeLists as $l){
		        			$conn = ORM::factory("omm_list_connection");
		        			$conn->omm_list_id = $this->list->id;
		        			$conn->type = "unsubscribe";
		        			$conn->other_list_id = $l;
							$conn->save();
		        		}	        			
	        			
	        		}else{
	        			$this->list->unsubscribe_connection_type = "only";	
	        		}
	        		
	        	}
		        
		        $this->list->saveObject();
				unset($_POST);
				url::redirect("/pages/listdetail");
	           
	        } // HIBA
	        else {
	            $form = arr::overwrite($form, $post->as_array());
	 
	            $errors = arr::overwrite($errors, $post->errors('form_errors_listsubscribe'));
	            
	            $errorTempl = new View(Kohana::config('admin.theme')."/common/errors");	
	            $errorTempl->errors = $errors;
	            
	            $this->pagetemplate->errors = $errorTempl->render(FALSE,FALSE);
	            
	            foreach ($errors as $key => $error){
	            	if($error != ""){
	            		$classes[$key] = "clearfixError";
	            	}
	            }
	            
	        }
	    }
	    	

	    	
		$this->pagetemplate->lists = $this->client->lists("name","asc");
		$this->pagetemplate->subscribeConnections = $this->list->connections("subscribe");
		$this->pagetemplate->unsubscribeConnections = $this->list->connections("unsubscribe");
	    $this->pagetemplate->listName = $this->list->name;
		

		
		$this->render();
	}



	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		KOHANA::show_404(FALSE,FALSE);
	}

}