<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Riportsoverview_Controller extends Ommpage_Controller {
	
	var $_pageview = "riports_overview";
	var $_pagetemplate;
	var $client;
	
	public function __construct(){
		$this->maintemplate = "main_withmenu";
		parent::__construct();
		
		if(isset($_SESSION['selected_client'])){
			$this->client = $_SESSION['selected_client'];
		}else{
			url::redirect("/pages/clientoverview");
		}		
		$this->template->bodyClass = "listindex";
	}
	
	public function index($riportid = 0,$arg1 = null, $arg2 = null, $arg3 = null, $arg4 = null)	{
		
		$this->pageview = $this->_pageview; 
		$this->init();

		$this->template->clientname = $this->client->name;
		
		//
		if(isset($_SESSION['alert'])){
			$this->pagetemplate->alert = $_SESSION['alert'];
			unset($_SESSION['alert']);	
		}else{
			$this->pagetemplate->alert = "";
		}
		
		
		$this->pagetemplate->riports = $this->client->riports();
		
		if($riportid != 0){
			$riport = ORM::factory('omm_riport')->find($riportid);
			$this->pagetemplate->riport = $riport;
			$args = func_get_args(); 
			$this->pagetemplate->riportd = $riport->run($arg1,$arg2,$arg3,$arg4);
			
			if($arg1 == null || $arg2 == null){
				$to = date('Y-m-d');
				$from = strtotime ( '-7 day' , strtotime ( $to ) ) ;
				$from = date ( 'Y-m-d' , $from );	
			
			}else{
				$from = $arg1;
				$to = $arg2;				
			
			}
			

			
			$back_from = strtotime ( '-7 day' , strtotime ( $from ) ) ;
			$back_from = date ( 'Y-m-d' , $back_from );	
			
			$this->pagetemplate->back_from = $back_from;
			$this->pagetemplate->back_to = $from;

			$forward_to = strtotime ( '+7 day' , strtotime ( $to ) ) ;
			$forward_to = date ( 'Y-m-d' , $forward_to );	
			
			$this->pagetemplate->forward_from = $to;
			$this->pagetemplate->forward_to = $forward_to;			
			
			
		}
		
		$this->render();
	}

	
	

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo "";
	}

}