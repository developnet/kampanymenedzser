<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    admin
 * @author     mberta
 * @copyright  (c) 2008
 */
abstract class Admin_Controller extends Controller {

	// Template view name
	public static $admin_role = "login";
	public static $accadmin_role = "accountadmin";
	public static $superadmin_role = "superadmin";
	public $main_template = "main";
	public $template = 'main_view';
	public $auth;

	public $javascripts = array();
	public $stylesheets = array();

	// Default to do auto-rendering
	public $auto_render = TRUE;


	public function __construct()
	{
		parent::__construct();
		$this->session = Session::instance();
			
		$this->auth = Auth::instance();
		$this->_checkLogin();

		//$profiler = new Profiler;



		$this->template = new View(Kohana::config('admin.theme')."/".$this->main_template);

		$this->template->title = Kohana::config('admin.title');
		$this->template->base = Kohana::config('core.viewpath');
		$this->template->viewpath = Kohana::config('core.viewpath');
		$this->template->js = Kohana::config('admin.js');
		$this->template->img = Kohana::config('admin.img');
		$this->template->css = Kohana::config('admin.css');
		$this->template->assets = Kohana::config('core.assetspath');
		$this->template->swf = Kohana::config('admin.swf');
		$this->template->appname = Kohana::config('admin.appname');
		$this->template->bodyClass = "";

		$this->template->pageContent = "";

		//$this->javascripts[] = array("link" => $this->template->base.$this->template->assets."tinymce/tiny_mce.js");
		$this->javascripts[] = array("link" => $this->template->assets."js/json2.js");
		$this->javascripts[] = array("link" => $this->template->assets."js/swfobject.js");
		//$this->javascripts[] = array("link" => $this->template->assets."jquery/js/jquery-1.3.2.min.js");
		$this->javascripts[] = array("link" => "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
		
		$this->javascripts[] = array("link" => $this->template->assets."jquery/js/jquery-ui-1.7.1.custom.min.js");
		$this->javascripts[] = array("link" => $this->template->assets."js/jquery.qtip-1.0.0-rc3.min.js");
		$this->javascripts[] = array("link" => $this->template->assets."js/jquery.pagination.js");
		$this->javascripts[] = array("link" => $this->template->assets."js/jquery.autoSuggest.packed.js");
		$this->javascripts[] = array("link" => $this->template->assets."DataTables-1.9.4/media/js/jquery.dataTables.js");
		
		
		$this->stylesheets[] = array("link" => $this->template->assets."DataTables-1.9.4/media/css/jquery.dataTables_themeroller.css");
		
		$this->stylesheets[] = array("link" => $this->template->assets."js/pagination.css");
		$this->stylesheets[] = array("link" => $this->template->assets."css/autoSuggest.css");
		
		$this->stylesheets[] = array("link" => $this->template->viewpath.$this->template->css."buttons.css");
		
		$this->stylesheets[] = array("link" => $this->template->assets."jquery/css/km_theme/jquery-ui-1.7.2.custom.css");
		
		if(defined('BASE') && BASE == 'hireso.garbaroyal.hu'){
			$this->stylesheets[] = array("link" => $this->template->viewpath.$this->template->css."hireso.css");
		}
		

		$this->javascripts[] = array("link" => $this->template->assets."js/ui/datepicker-hu.js");
		$this->javascripts[] = array("link" => $this->template->viewpath.$this->template->js."common.js");
		$this->javascripts[] = array("link" => $this->template->viewpath.$this->template->js."subscribers.js");
		//
		//        $this->stylesheets[] = array("link" => $this->template->base.$this->template->assets."js/themes/base/ui.all.css");
		//        $this->stylesheets[] = array("link" => $this->template->base.$this->template->assets."js/themes/base/ui.allplugins.css");
		//
		//
		//
		//        $this->javascripts[] = array("link" => $this->template->base.$this->template->assets."js/jquery.hotkeys-0.7.8-packed.js");
		//        $this->javascripts[] = array("link" => $this->template->base.$this->template->assets."js/jquery.cookie.js");
		//
		//
		//        $this->stylesheets[] = array("link" => $this->template->base.$this->template->css."template.css");
		 



		$this->template->message = "";
		$this->template->adminNavs = $this->getAdminNavs();
		$this->template->primaryNavs = $this->getPrimaryNavs();
		$this->template->secondaryNavs = $this->getSecondaryNavs();



		$metainfo = array(
        	"username" => $_SESSION['auth_user']->username,
            "logoutlink" => html::anchor(url::base()."login/logout" ,'<span>'.Kohana::lang('barendszer.logout').'</span>',array('class' => 'logoutlink'))
		);

		$this->template->metainfo = $metainfo;

		if ($this->auto_render == TRUE)
		{
			// Render the template immediately after the controller method
			Event::add('system.post_controller', array($this, '_render'));
		}
	}


	/**
	 * Render the loaded template.
	 */
	public function _render()
	{
		if ($this->auto_render == TRUE)
		{
			 
			$this->template->javascripts = $this->javascripts;
			$this->template->stylesheets = $this->stylesheets;
				
			if(isset($_SESSION['message'])){

				$this->template->message = $_SESSION['message'];
				unset($_SESSION['message']);

			}
				
			// Render the template when the class is destroyed
			$this->template->render(TRUE);
		}
	}

	protected function _checkLogin(){

		if (!$this->auth->logged_in(Admin_Controller::$admin_role)){
				
			url::redirect('login');
				
		}
	}

	private function getPrimaryNavs(){

		$links = array();

		$links["listsTab"] = array(
			"listoverview","listadd","listdetail",
			"groupoverview","groupadd",
			"groupmembers","listsubscribeprocess","listconnections","listunsubscribeprocess",
			"formoverview","formdetail","listnotifications");

		
		$links["subscribersTab"] = array(
			"subscribers",'memberdetail',"listexport");				
		
		$links["campaignsTab"] = array(
			"campaignoverview","campaigndetail","letteroverview","letterdetail");		

		$links["statisticsTab"] = array(
			"statisticoverview","statisticdetails");		
		
		$links["riportsTab"] = array(
			"riportsoverview");		

		$navs = array(
			"subscribersTab" => array("name"=>"Feliratkozók","link"=> url::base()."pages/subscribers","current"=>""),	
			"listsTab" => array("name"=>"Hírlevél-listák","link"=> url::base()."pages/listoverview","current"=>""),
			"campaignsTab" => array("name"=>"Kampányok","link"=>url::base()."pages/campaignoverview","current"=>""),
			"statisticsTab" => array("name"=>"Statisztika","link"=>url::base()."pages/statisticoverview","current"=>""),
			"riportsTab" => array("name"=>"Riportok","link"=>url::base()."pages/riportsoverview","current"=>"")
		);

		$page = $this->uri->segment(2,"");

		foreach($navs as $key => $n){

			if (in_array($page, $links[$key])) {
				$navs[$key]["current"] = "current";
			}					
		
		}
		

		return $navs;
	}

	private function getSecondaryNavs(){
		$links = array();
		$page = $this->uri->segment(2,"");
		
		$links["clientSettingTab"] = array(
			"clientadd","lettertemplates","commonletters","productoverview","productdetail","productadd","fields"
		);	
		
			

		$navs = array(
			"clientSettingTab" => array("name"=>"Honlapbeállítások","link"=>url::base()."pages/clientadd/index/mod","current"=>""),		
		);


		if (in_array($page, $links["clientSettingTab"])) {
			$navs["clientSettingTab"]["current"] = "current";
		}

		
		return $navs;
	}

	private function getAdminNavs(){

		$navs = array();

		$nav = array();
		$nav['name'] = "Honlapválasztó";

		if($this->uri->segment(2,"") == "clientoverview" || $this->uri->segment(1,"") == ""){
			$nav['id'] = "adminNavsOn";
		}else{
			$nav['id'] = "";
		}
		$nav['link'] = url::base();

		$navs[] = $nav;



		$nav = array();
		
		if(isset($_SESSION['auth_user'])){
			$userinfo = $_SESSION['auth_user']->lastname." ".$_SESSION['auth_user']->firstname."";
		}else{
			$userinfo = "";
		}
		

		$nav['name'] = "Felhasználói profil : ".$userinfo;

		if($this->uri->segment(2,"") == "useroverview" || $this->uri->segment(1,"") == ""){
			$nav['id'] = "adminNavsOn";
		}else{
			$nav['id'] = "";
		}
		$nav['link'] = url::base()."pages/useroverview";
		$navs[] = $nav;


		if ($this->auth->logged_in(Admin_Controller::$accadmin_role)){

			$nav = array();
			$nav['name'] = "Felhasználók kezelése";

			if($this->uri->segment(2,"") == "useradmin" || $this->uri->segment(1,"") == ""){
				$nav['id'] = "adminNavsOn";
			}else{
				$nav['id'] = "";
			}
			$nav['link'] = url::base()."pages/useradmin";
			$navs[] = $nav;
				
			
			$nav = array();
			$nav['name'] = "Előfizetői profil";
	
			if($this->uri->segment(2,"") == "accountoverview" || $this->uri->segment(1,"") == ""){
				$nav['id'] = "adminNavsOn";
			}else{
				$nav['id'] = "";
			}
			$nav['link'] = url::base()."pages/accountoverview";
			$navs[] = $nav;			
			
		}

		
		if ($this->auth->logged_in(Admin_Controller::$superadmin_role)){
			
			$nav = array();
			$nav['name'] = "Adminisztrációs felület";
	
			if($this->uri->segment(2,"") == "accountoverview" || $this->uri->segment(1,"") == ""){
				$nav['id'] = "adminNavsOn";
			}else{
				$nav['id'] = "";
			}
			$nav['link'] = url::base()."admin/userstat";
			$navs[] = $nav;			
			
		}
		
		
		return $navs;
	}


} // End Template_Controller