<?php defined('SYSPATH') or die('No direct script access.');
/**
 */
class Login_Controller extends Controller {

	
	
	private $main_template = "login";
	private $template;
	private $auth;
	
	public function index()	{
        
//		$user = ORM::factory('user');
//		
//		$user->username="admin";
//		$user->password="admin";
//		
//		$user->add(ORM::factory('role', 'login'));
//		$user->save();
		
		$this->auth = Auth::instance();
		
		if ($this->auth->logged_in(Admin_Controller::$admin_role)){
			url::redirect('/');
		}
		
		if(isset($_POST['username'])){
			
			if($this->auth->login($_POST['username'],$_POST['password'])){
				
				$km_user = $_SESSION['auth_user']->km_user;

				$_SESSION['km_user'] = $km_user;
				
				if($_SESSION['auth_user']->status == 0){
					Auth::instance()->logout(TRUE);
					$_SESSION['message'] = Kohana::lang('barendszer.badlogin');
					//url::redirect("/");
				}else{
					
					if($km_user == "admin"){
						$_SESSION['km_user'] = $_POST['km_user'];
					}
					
					//define('USERNAME',$_SESSION['km_user']);	

					cookie::set("km_user", $_SESSION['km_user']);
					
					$dbcore = new Database("");
					$acc = $dbcore->query("SELECT code FROM omm_accounts WHERE km_user = '".$_SESSION['km_user']."' ");					

					if(sizeof($acc) > 0){
						$_SESSION['km_user_code'] = $acc[0]->code;
						cookie::set("km_user_code", $acc[0]->code);
					}
					
					
					url::redirect("/");
				}
				
				
				
			}else{
				$_SESSION['message'] = Kohana::lang('barendszer.badlogin'); 
				//appmodules::getMessage("warning",Kohana::lang('barendszer.badlogin').'');
			}
			
		}
		
		
		$this->template = new View(Kohana::config('admin.theme')."/".$this->main_template);
        $this->template->title = Kohana::config('admin.title');
        $this->template->appname = Kohana::config('admin.appname');
        $this->template->base = url::base();
        $this->template->js = Kohana::config('admin.js');
        $this->template->img = Kohana::config('admin.img');  
        $this->template->css = Kohana::config('admin.css');  
        $this->template->assets = Kohana::config('admin.assets');  		

		    if(isset($_SESSION['message'])){
	        	
	        	$this->template->message = $_SESSION['message'];
	        	unset($_SESSION['message']);
	        	
	        }else{
	        	$this->template->message = "";
	        }
	        
        $this->template->pagetitle = Kohana::lang('barendszer.tologin');
        $this->template->loginform = $this->getForm();
        
		$this->template->render(TRUE);
	}


	public function logout(){
		Auth::instance()->logout(TRUE);
		
		cookie::delete('km_user');
		
		url::redirect("/");
	}

	/**
	 * ha olyan function-t akarnak hívni ami nincs akkor ez hívódik meg
	 */
	public function __call($method, $arguments)	{
		$this->auto_render = FALSE;
		echo "";
	}

	
	
	private function getForm() {
		
		$form = '<div id="form-block">';
		
		$form .= '<form method="post" action="'.url::site().'/login" id="login_form">';
		
		$attr = array(
			"class" => "username",
			"name" => "username",
			"id" => "username"
			);
		
		$form .= '<label for="username">'.Kohana::lang('barendszer.username').'</label>';
		$form .= form::input($attr,"");

		$attr = array(
			"class" => "password",
			"name" => "password",
			"id" => "password",
			"type" => "password"
			);
		
		$form .= '<label for="password">'.Kohana::lang('barendszer.password').'</label>';
		$form .= form::input($attr,"");
		
		$form .= form::submit(array("class" => "submit", "id" => "submit"), Kohana::lang('barendszer.tologin'));
		
		$form .= '</form></div>';
		
		return $form;
	}
	
}