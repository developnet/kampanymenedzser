<?php
$lang = array
(
	'name' => Array	(
			'required' 	=> 'Adja meg a honlap nevét!',
			'default'  	=> 'Invalid Input.'
		),

	'site_domain' => Array	(
			'required' 	=> 'Adja meg a domaint!',
			'default'  	=> 'Invalid Input.',
			'domain_exists' => 'Ez a domain már létezik!',
		 	'invalid_domain_http' => 'A domain nevet http:// és www nélkül adja meg!',
			'invalid_domain' => 'Érvénytelen domain!'
		),

	'sender_name' => Array	(
			'required' 	=> 'Adja meg a küldő nevét!',
			'default'  	=> 'Invalid Input.'
		),

	'sender_replyto' => Array	(
			'required' 	=> 'A reply-to mező kitöltése kötelező.',
			'default'  	=> 'Szabályos e-mail címet adjon meg!'
		),		
		
	'sender_email' => Array	(
			'required'  => 'Adja meg a küldő e-mail címét!',
			'email' 	=> 'Szabályos e-mail címet adjon meg!',
			'default' 	=> 'Invalid Input.',
			'invalid_domain_email' => 'A feladó e-mail címe nem a megadott domain-en van!'
		),		
		
		
);
?>