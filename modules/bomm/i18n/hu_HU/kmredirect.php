<?php
$lang = array
(
	'initialsingle' => array( 
		'message' => 'Sikeres regisztráció!',
 		'description' => 'A regisztrációja sikeres volt.',
	),
	'initialdouble' => array( 
		'message' => 'Sikeres regisztráció!Aktivációs e-mail kiment!',
 		'description' => 'A regisztrációja sikeres volt.',
	),
	'confirmdouble' => array( 
		'message' => 'Sikeres aktiváció/regisztráció!',
 		'description' => 'A regisztrációja sikeres volt.',
	),
	'second' => array( 
		'message' => 'Ez az e-mail cím már regisztrálva van!',
 		'description' => 'A regisztrációja sikeres volt.',
	),
	'error' => array( 
		'message' => 'A feliratkozás sikertelen volt!',
 		'description' => 'A regisztrációja sikertelen volt.',
	),
	'unsubscribe' => array( 
		'message' => 'Sikeres leiratkozás!',
 		'description' => 'A leiratkozás sikeres volt.',
	),
	'secondunsub' => array( 
		'message' => 'Ez az e-mail cím már nem szerepel a listán!',
 		'description' => 'A leiratkozás sikeres volt.',
	),
	'errorunsub' => array( 
		'message' => 'A leiratkozás sikertelen volt!',
 		'description' => 'Kérjük próbálja meg újra!',
	),	
	'nomodify' => array( 
		'message' => 'Az adatmódosítás nem engedélyezett!',
 		'description' => 'Jelenleg a listán nincs engedélyezve az adatmódosítás! Az adatmódosításhoz lépjen kapcsolatba a lista üzemeltetőjével!',
	)	
	
	
	
);
?>