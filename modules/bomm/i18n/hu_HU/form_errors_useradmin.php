<?php
$lang = array
(
	'lastname' => Array	(
			'required' 	=> 'Adja meg a vezetéknevet!',
			'default'  	=> 'Invalid Input.'
		),

	'firstname' => Array	(
			'required' 	=> 'Adja meg a keresztnevet!',
			'default'  	=> 'Invalid Input.'
		),

	'email' => Array	(
			'required' 	=> 'Adja meg az e-mailt!',
			'default'  	=> 'Szabályos e-mail címet adjon meg!'
		),

	'jelszo' => Array	(
			'matches' 	=> 'A két jelszó nem egyezik meg!',
			'length'  	=> 'A jelszó minimum 8 karakter legyen.'
		),
	'username' => Array	(
			'matches' 	=> 'Adja meg a felhasználónevet!',
			'length'  	=> 'A felhasználónév minimum 4 karakter legyen.'
		)	
		
);
?>