<?php
$lang = array
(
		'email' => Array	(
				'default' 		=> 'Hibás e-mail cím!'
			),

		'initial_conf_page'					=> Array	('default' 		=> 'Adja meg a feliratkozást köszönő, megerősítésre figyelmeztető oldal címét'	, 
															"url" 		=> 'A feliratkozás köszönő, megerősítésre figyelmeztető oldal címe nem url'),
		
	    'verifycation_type'   				=> Array	('default' 		=> 'HIBASZÖVEG2'	),
	        
		'verifycation_sender_name_text'		=> Array	('default' 		=> 'Adja meg a megerősítő e-mail feladó nevét!'	),
		'verifycation_sender_email_text'	=> Array	('default' 		=> 'Adja meg a megerősítő e-mail feladó e-mail címét!'	),
		
		'verifycation_subject_text'			=> Array	('default' 		=> 'Adja meg a levél tárgyát'	),
			
		'verifycation_text_content_text'	=> Array	('default' 		=> 'Adja meg a levél tartalmát'	),
		'verifycation_sender_name_html'		=> Array	('default' 		=> 'Adja meg a megerősítő e-mail feladó nevét!'	),
		'verifycation_sender_email_html'	=> Array	('default' 		=> 'Adja meg a megerősítő e-mail feladó e-mail címét!'	),
		'verifycation_subject_html'			=> Array	('default' 		=> 'Adja meg a megerősítő e-mail tárgyát!'	),
		'verifycation_html_content_html'	=> Array	('default' 		=> 'Adja meg a megerősítő e-mail tartalmát!'	),
		'verifycation_text_content_html'	=> Array	('default' 		=> 'Adja meg a megerősítő e-mail tartalmát!'	),
	       
		'second_conf_page'					=> Array	('default' 		=> 'Ajda meg sikeres regisztrációt köszönő linket' , 
															"url" => 'A sikeres regisztrációt köszönő link nem szabályos url!'	),

	    'subscribe_conf_type'   			=> Array	('default' 		=> 'HIBASZÖVEG13'	),
	        
		'subscribe_conf_sender_name_text'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail feladó nevét!'	),
		'subscribe_conf_sender_email_text'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail feladó e-mail címét!'	),
		'subscribe_conf_subject_text'		=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail tárgyát!'	),
		'subscribe_conf_text_content_text'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail tartalmát!'	),

		'subscribe_conf_sender_name_html'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail feladó nevét!'	),
		'subscribe_conf_sender_email_html'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail feladó e-mail címét!'	),
		'subscribe_conf_subject_html'		=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail tárgyát!'	),
		'subscribe_conf_html_content_html'	=> Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail tartalmát!'	),
		'subscribe_conf_text_content_html'  => Array	('default' 		=> 'Adja meg a feliratkozás megerősítő e-mail tartalmát!'	),
		
		
		'unsubscribe_landing_page'		  	=> Array	('default' 		=> 'Adja meg a leiratkozás utáni oldal címét',
															"url" => 'A leiratkozás utáni oldal címe nem url'		),
			
		'second_unsubscribe_page'			=> Array	('default' 		=> 'Adja meg a ismételt leiratkozás utáni oldal címét' , 
															"url" => 'Az ismételt leiratkozás utáni oldal címe nem url'	),	

		'error_unsubscribe_page'			=> Array	('default' 		=> 'Adja meg a sikertelen leiratkozás utáni oldal címét' , 
															"url" => 'A sikertelen leiratkozás utáni oldal címe nem url'	),
			
		'second_subscribe_page'			=> Array	('default' 		=> 'Ajda meg az ismételt feliratkozás linket' , 
															"url" => 'Az ismételt feliratkozás utáni oldal címe nem url'	),
			
		'error_subscribe_page'			=> Array	('default' 		=> 'Ajda meg "Sikeres kettős Opt-in feliratkozás megköszönő oldal címe" mezőt' , 
															"url" => 'A "Sikeres kettős Opt-in feliratkozás megköszönő oldal címe" hibás url!'	)			
		
			
			
		
);
?>