<?php
$lang = array
(
	'name' => Array	(
			'required' 	=> 'Adja meg a kampány nevét!',
		),	
	'sender_name' => Array	(
			'required' 	=> 'Adja meg a küldő nevét!',
		),
	'sender_email' => Array	(
			'required' 	=> 'Adja meg a küldő e-mail címét!',
			'invalid_domain_email' => 'Az e-mail nem a honlap domain-jéhez tartozik!'
		)
		
);
?>