<?php
$lang = array
(
	'email' => Array	(
			'required' 	=> 'Adja meg az e-mail címét!',
			'email'  	=> 'Érvényes e-mail címet adjon meg!'
		),

	'firstname' => Array	(
			'required' 	=> 'Adja meg a keresztnevét!',
			'default'  	=> 'Invalid Input.'
		),

	'lastname' => Array	(
			'required' 	=> 'Adja meg a vezetéknevét!',
			'default'  	=> 'Invalid Input.'
		),

	'newpassword' => Array	(
			'required' 	=> 'Adja meg a jelszavat!.',
			'length'  	=> 'A jelszó legalább 6 karakter kell, hogy legyen!',
			'alpha_dash' => 'A jelszó csak ékezet nélküli betüt, számot, per-jelet és aláhúzást tartalmazhat!',
			'matches'  	=> 'A két jelszó nem egyezik meg!',
		),			
		
	'newpassword2' => Array	(
			'required' 	=> 'Írja be még egyszer a jelszót!',
			'length'  	=> 'A jelszó legalább 6 karakter kell, hogy legyen!',
			'alpha_dash' => 'A jelszó csak ékezet nélküli betüt, számot, per-jelet és aláhúzást tartalmazhat!',
			'matches'  	=> 'A két jelszó nem egyezik meg!',
		),					
		
);
?>