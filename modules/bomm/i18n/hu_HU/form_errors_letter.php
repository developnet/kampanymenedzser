<?php
$lang = array
(
	'name' => Array	(
			'required' 	=> 'Adja meg a levél nevét!',
		),	
	'sender_name' => Array	(
			'required' 	=> 'Adja meg a küldő nevét!',
		),
	'sender_email' => Array	(
			'required' 	=> 'Adja meg a küldő e-mail címét!',
			'invalid_domain_email' => 'A feladó e-mail címe nem a honlap domain-jéhez tartozik!'
		),
	'subject' => Array	(
			'required' 	=> 'Adja meg a levél tárgyát!',
		),
	'text_content' => Array	(
			'invalid_field_found' => 'A text tartalom olyan behelyettesítő tag-et tartalmaz ami nem szerepel egy listában sem!'
		),
	'html_content' => Array(
			'invalid_field_found' => 'A html tartalom olyan behelyettesítő tag-et tartalmaz ami nem szerepel egy listában sem!'
		)	
		
		
);
?>