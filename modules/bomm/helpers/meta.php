<?php
class meta_Core {

	public static function getKMuser(){
		return $_COOKIE['km_user'];
	}

	public static function getAccId(){
		$db = new Database("default");
		$res = $db->from("omm_accounts")->where("km_user",meta::getKMuser())->limit(1)->get();
		return $res[0]->id;
	}

	public static function getAccountLog(){
		$db = new Database("default");
		$logs = $db->from("omm_account_log")->where("km_user",meta::getKMuser())->orderby("date", "desc")->orderby("mod_date", "desc")->get();
		return $logs;
	}

	public static function getEmailCountAndPackageKm($db, $km_user){

		$sql = "SELECT count(distinct email) as ossz
					FROM km_".$km_user.".omm_list_members m WHERE m.status = 'active' ";		
		$emails = $db->query($sql);
		$ossz = $emails[0]->ossz;


		$ret = array();
		$ret['emailcount'] = $ossz;

		$acc = $db->from("omm_accounts")->where("km_user",$km_user)->limit(1)->get();

		if($acc[0]->omm_package_id == 0){
			$sql = "SELECT name,base_price,emailfrom,emailto FROM km_core.omm_packages WHERE dinamic = 1 AND $ossz >= emailfrom AND $ossz <= emailto LIMIT 1";
		}else{
			$sql = "SELECT name,base_price,emailfrom,emailto FROM km_core.omm_packages WHERE id = ".$acc[0]->omm_package_id." LIMIT 1";
		}

		$package = $db->query($sql);
		$p = $package[0];

		$ret['package'] = $p;

		return $ret;		
	}
	
	public static function getEmailCountAndPackage(){
		
		$db = new Database("own");

		$sql = "SELECT count(distinct email) as ossz
					FROM omm_list_members m WHERE m.status = 'active'";		
		$emails = $db->query($sql);
		$ossz = $emails[0]->ossz;


		$ret = array();
		$ret['emailcount'] = $ossz;


		$db->query("use km_core");

		$acc = $db->from("omm_accounts")->where("km_user",meta::getKMuser())->limit(1)->get();

		if($acc[0]->omm_package_id == 0){
			$sql = "SELECT name,base_price,emailfrom,emailto FROM `omm_packages` WHERE dinamic = 1 AND $ossz >= emailfrom AND $ossz <= emailto LIMIT 1";
		}else{
			$sql = "SELECT name,base_price,emailfrom,emailto FROM `omm_packages` WHERE id = ".$acc[0]->omm_package_id." LIMIT 1";
		}

		$package = $db->query($sql);
		$p = $package[0];

		$ret['package'] = $p;

		return $ret;
	}

	public static function getCodeDictArray($codetype){

		$dict = meta::getCodeDict($codetype);

		$dict_a = array();

		foreach($dict as $d){
			$dict_a[$d->code] = $d->value;
		}

		return $dict_a;
	}


	public static function getVersionString(){
		return KOHANA::config("bomm.major_version").".".KOHANA::config("bomm.minor_version")."-".KOHANA::config("bomm.build");
	}

	public static function getCodeDict($codetype){
		$db = new Database("default");
		return $db->from("omm_codedicts")->where("codetype",$codetype)->orderby("value")->get();
	}


	public static function getBounceDict(){
		$db = new Database("default");
		$res = $db->query("SELECT id,reason FROM omm_bounces_dict WHERE status = 1");

		$return = array();
		$return[0] = "Ismeretlen visszapattanó.";
		foreach($res as $r){
			$return[$r->id] = $r->reason;
		}

		return $return;
	}

	public static function getCodeDictRow($codetype,$value){
		$db = new Database("default");
		return $result = $db->from("omm_codedicts")->where("codetype",$codetype)->where("code",$value)->get();
	}

	public static function getTitle($page){
		$titles = array(
			"campaigndetail/" => "Kampány létrehozás/módosítás",
			"campaignoverview/" => "Kampányok böngészése",
			"clientadd/" => "Honlap hozzáadása",
			"clientadd/index" => "Honlap módosítása",
			"clientoverview/" => "Honlap kiválasztása",
			"formdetail/" => "Űrlap hozzáadása/módosítása",
			"formoverview/" => "Űrlapok karbantartása",
			"groupadd/first" => "Csoport hozzáadása/szereksztése - Csoport adatai",		
			"groupadd/second" => "Csoport hozzáadása/szereksztése - Csoport feltételei",	
			"groupmembers/" => "Csoport tagjainak tallózása",
			"groupmembers/index" => "Csoport tagjainak tallózása",
			"groupoverview/" => "Csoportok tallózása",
			"letterdetail/first" => "Levél hozzáadása/szerkesztése - Levél adatai",
			"letterdetail/second" => "Levél hozzáadása/szerkesztése - Levél tartalma",
			"letterdetail/recipients" => "Levél hozzáadása/szerkesztése - Levél címzési feltételek",
			"letterdetail/test" => "Levél hozzáadása/szerkesztése - Tesztküldés",
			"letterdetail/timing" => "Levél hozzáadása/szerkesztése - Időzítés beállítása",
			"letterdetail/draft" => "Levél hozzáadása/szerkesztése - Levél előnézet",
			"letteroverview/" => "Levelek tallózása",
			"listadd/first" => "Lista hozzáadása/szerkesztése - Lista adatai",
			"listadd/second" => "Lista hozzáadása/szerkesztése - Lista mezői",
			"listconnections/" => "Listák összekapcsolása",
			"listdetail/index" => "Lista előnézete",
			"listexport/" => "Lista importálása/exportálása",
			"listoverview/" => "Hírlevél-listák tallózása",
			"listsubscribeprocess/" => "Lista feliratkozási folyamatának beállítása",
			"listunsubscribeprocess/" => "Lista leiratkozási folyamatának beállítása",
			"memberdetail/" => "Feliratkozó szerkesztése",
			"memberdetail/index" => "Feliratkozó szerkesztése",
			"statisticdetails/letter" => "Levél statisztikájának előnézet",
			"statisticdetails/recipients" => "Levél statisztikája - címzettek tevékenységei",
			"statisticdetails/link" => "Levél statisztikája - linkek statisztikája",
			"statisticoverview/" => "Levelek statisztikáinak tallózása",		
			"ures/" => "Honlapok tallózása",
			"useroverview/" => "Felhasználói profil szerkesztése",
			"accountoverview/" => "Előfizetői profil",
			"accountoverview/actmonth" => "Előfizetői profil - Aktuális hónap egyenlege",
			"accountoverview/billing"	=> "Előfizetői profil - Kiküldött számlák",
			"listnotifications/" => "Értesítések beállításai",
			"lettertemplates/overview" => "Levél sablonok tallózása",
			"lettertemplates/add" => "Levél sablonok hozzáadása/módosítása",
			"listexport/preimport" => "Lista importálása/exportálása",
			"useradmin/overview" => "Felhasználók kezelése",
			"useradmin/add" => "Felhasználó hozzáadása/szerkesztése",
			"accountadministration/" => "Előfizető adminisztráció",
			"accountadministration/index" => "Előfizető adminisztráció",
		    "commonletters/" => "Általános levelek",
			"subscribers/" => "Feliratkozók áttekintése",
			"fields/index" => "Globális mezők",
			"productoverview/" => "Termékek áttekintése",
			"productadd/first" => "Termék hozzáadása",
			"productdetail/index" => "Termék előnézete"
		);
		if(isset($titles[$page])){
			return $titles[$page];
		}
		else{
			return "<<undefined>>";
		}
	}


	public static function createAlert($type,$title,$msg,$returnHtml = false){
		$errorTempl = new View(Kohana::config('admin.theme')."/common/".$type);
		$errorTempl->alertTitle = $title;
		$errorTempl->alertMessage = $msg;
		if($returnHtml){
			return $errorTempl->render(FALSE,FALSE);
		}else{
			$_SESSION['alert'] = $errorTempl->render(FALSE,FALSE);
		}

	}

	public static function clearAlert(){
		unset($_SESSION['alert']);
	}


	public static function createMessage($recipient,$type,$subject,$preview,$body = ""){
		$message = ORM::factory('omm_message');
		$message->create($recipient,$type,$subject,$preview,$body);
	}

	public static function createLetterSystemMessage($letter){

		try {
			$dbown = Kohana::config('database.own');
			$kmuserdb = $dbown['connection']['database'];
			list($km, $user) = explode('_', $kmuserdb);
				
			$recipient = $user;
				
			$type = "system";
			$subject = "Levél kiküldés";

			$ret = $letter->getSystemMessage();

			$preview = $ret['preview'];
			$body = $ret['body'];
				
			meta::createMessage($recipient,$type,$subject,$preview,$body);
				
		} catch (Exception $e) {
			KOHANA::log("error", $e);
		}

	}
	
	/**
	 * Nyelvi téma, ide kell majd a nyelvi adatbázis
	 *
	 * @param String $string
	 */
	public static function _($string){
		return $string;
	}
	

}

?>