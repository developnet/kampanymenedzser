<?php
class apierrors_Core {
	
	/***************** HIBÁK ************************************/
	
	public static function error_unsubscribe_db_error(){
		$data = Event::$data;
		if(1==2 && $data->error_unsubscribe_page != null AND $data->error_unsubscribe_page != ""){
			
			url::redirect($data->error_unsubscribe_page);
		}else{
			url::redirect('/pages/redirect/subscribe/errorunsub');
		}		
	}
	
	public static function error_unsubscribe_missingorinvalid_code(){
		throw new Kohana_User_Exception('Invalid or missing code!', "Invalid or missing code!");
	}
	
	public static function error_subscribe_email(){
		$data = Event::$data;
		if($data->error_subscribe_page != null AND $data->error_subscribe_page != ""){
			
			url::redirect($data->error_subscribe_page);
		}else{
			url::redirect('/pages/redirect/subscribe/error');
		}		
	}	
	
	public function error_subscribe_invalid_domain(){
		throw new Kohana_User_Exception('Invalid domain!', "Invalid domain: ".Event::$data);
	}
	
	public function error_subscribe_missing_list_code(){
		throw new Kohana_User_Exception('Invalid or missing list code', "Invalid or missing list code");
	}
	

	public function error_subscribe_second_subscribe(){
		$data = Event::$data;
		if($data->second_subscribe_page != null AND $data->second_subscribe_page != ""){
			
			url::redirect($data->second_subscribe_page);
		}else{
			url::redirect('/pages/redirect/subscribe/second');
		}
	}

	public function error_unsubscribe_second_unsubscribe(){
		$data = Event::$data;
		$member = $data['member'];
		$km_user = $data['km_user'];
				
		if(1==2 && $data->second_unsubscribe_page != null AND $data->second_unsubscribe_page != ""){
			
			url::redirect($data->second_unsubscribe_page);
		}else{
			
			if($km_user == 'bezchoroby'){
				url::redirect('http://bezchoroby.sk/7-1-odhlasenie-opatovne');
			}else{
				url::redirect('/pages/redirect/subscribe/secondunsub');	
			}			
			
			
		}		
	}

	
	
	/***************** HIBÁK ************************************/
	
}
?>