<?php
if ( ! class_exists('Swift', FALSE)){
	// Load SwiftMailer
	require Kohana::find_file('vendor', 'swift/Swift');

	// Register the Swift ClassLoader as an autoload
	spl_autoload_register(array('Swift_ClassLoader', 'load'));
}

class api_Core {

	/**
	 * feliratkozás elött fut le...Event::data = $_POST
	 *
	 */
	public static function preSubscription(){

	}

	/**
	 * Feliratkozás után fut le..Event:data = $member, $list array-ban
	 *
	 */
	public static function postSubscription(){

		$data = Event::$data;

		$kmuser = $data['kmuser'];
		$list = $data['list'];
		$member = $data['member'];
		$api = 	$data['api'];
		if($list->type == "single"){///////single optin egyből mehet aktív-ba a user

			$member->status = 'active';
			$member->activation_date = date("Y-m-d H:i:s");
			try {
				$member->save();
			}catch (Exception $e){
				KOHANA::log("error","single optin listánál aktívvá állítás prereg után, member_id=".$member->id." ::: ".$e);
			}




			////
			api::sendSubscribeCofirmMail($member,$list,$kmuser);///////sikeres felriatkozás mail küldés

			////////////affiliate log, amelyik kódhoz volt már logja ahhoz nyomunk (prereg létrejött ha a formban volt kód)
			//				$log = ORM::factory('omm_aff_log');
			//		        $oldlogs = $log->getLogForEmailLists($member->email);
			//
			//		        $ids = array();
			//        foreach ($oldlogs as $l){
			//		        	$_log = ORM::factory('omm_aff_log');
			//			        $_log->create($l->code_id,'subscribe',$l->list_id,$member->email);
			//			     	$ids[] = $l->id;
			//		        }
			//		        $log->deletePreregLogs($ids);
			///////////////////

		}else{
			api::sendActivationMail($member,$list,$kmuser);///////aktiváló mail küldés
		}

		if($api){
			$ret = array('REQUEST' => 'OK',
	        			 'STATUS'  => 'SUBSCRIBED',
	        			 'MSG'  => 'Sikeres feliratkozás.'	
	        						 );

	        echo json_encode($ret);
	        KOHANA::shutdown();
	        die();
	        
		}else{
			if($list->initial_conf_page != null AND $list->initial_conf_page != ""){
				
				url::redirect(replace::replaceFields($member,$list->initial_conf_page,TRUE,$list->id));
			}else{
				url::redirect('/pages/redirect/subscribe/initial'.$list->type);
			}
		}




	}

	private static function subscribeToConnectedLists($list,$member){


	}

	public static function sendSubscribeCofirmMail($member,$list,$kmuser = ""){
		if($list->subscribe_conf_email_id != NULL){///////ha van email kiküldjük

			$subscribe_conf_email = ORM::factory("omm_common_letter")->find($list->subscribe_conf_email_id);

			if($subscribe_conf_email->loaded){////////elküldjük a feliratozást megköszönö mailt
				$job = ORM::factory('omm_job');
				$job->create("common", null, $subscribe_conf_email->id, $member->id, null);
					
				$dom = new simple_html_dom();
				$job->prepare($dom,$kmuser);
				unset($dom);

				$job->execute();
			}
		}


		api::sendNotification($list,$member,'subscribe');

	}

	public static function sendNotification($list,$member,$type){

		try {


			////értesítő kiküldése, amennyiben be van állítva
			if($list->getNotifMail($type) != ""){
					
				if($type == 'subscribe'){
					$subject = '[KM ÉRTESÍTŐ]['.$list->name.'] Feliratkozásról';
				}else{
					$subject = '[KM ÉRTESÍTŐ]['.$list->name.'] Leiratkozásról';
				}
					
					
				$message =	"";
					
				$fields = $list->fields(true);
				$mess = new View(Kohana::config('admin.theme')."/common/".$type."_notification");
					
				$mess->list = $list;
				$mess->member = $member;
				$mess->mo = $member->getFullData($fields);
				$mess->fields = $fields;

				$message = $mess->render(FALSE,FALSE);
					
					
				$m = new Swift_Message($subject);
				$m->headers->setEncoding("Q");
					
				$part_html = new Swift_Message_Part($message, "text/html");
				$part_html->setCharset("utf-8");
				$m->attach($part_html);
					
				//$swift = jobs::getSwift();
				$swift = jobs::getSwift(false,false,'188.227.227.172');
					
				$sender = new Swift_Address("ertesito@kampanymenedzser.hu", "KampányMenedzser értesítő");
					
				//$from = 'noreply@aktivhonlapok.hu';
				KOHANA::log("info","Értesítő, member_id=".$member->id." :".$list->getNotifMail('subscribe').":: ");
				$swift->send($m, $list->getNotifMail($type), $sender);

					
				$swift->disconnect();
			}


		} catch (Exception $e) {
			KOHANA::log("error","Értesító küldés hiba, $type, member_id=".$member->id." ::: ".$e);
		}

	}

	public static function sendActivationMail($member,$list,$kmuser=""){
		if($list->verifycation_email_id != NULL){///////ha van email kiküldjük
			$verifycation_email = ORM::factory("omm_common_letter")->find($list->verifycation_email_id);

			if($verifycation_email->loaded){
				$job = ORM::factory('omm_job');
				$job->create("common", null, $verifycation_email->id, $member->id, null);
					
				$dom = new simple_html_dom();
				$job->prepare($dom,$kmuser,null,$list->code);
				unset($dom);
					
				$job->execute();
			}
		}
	}


	public static function preActivate(){

	}


	public static function postActivate(){
		$data = Event::$data;
		$member = $data['member'];
		$kmuser = $data['kmuser'];
		$list = $data['list'];

		try {////////és a kapcsolódó listákon is activ-ra állítjuk
			$connectedLists = $list->connectedLists('subscribe');
			$email = $member->email;

			foreach($connectedLists as $l){
				$l->activateMembersByEmail($member->email);
			}
		}catch(Exception $e){
			KOHANA::log("error","double optin listánál aktívvá állítás postactivate, member_id=".$member->id." ::: ".$e);
		}

		api::sendSubscribeCofirmMail($member,$list,$kmuser);


		//			////////////affiliate log, amelyik kódhoz volt már logja ahhoz nyomunk (prereg létrejött ha a formban volt kód)
		//				$log = ORM::factory('omm_aff_log');
		//		        $oldlogs = $log->getLogForEmailLists($member->email,'prereg');
		//
		//		        $ids = array();
		//		        foreach ($oldlogs as $l){
		//		        	$_log = ORM::factory('omm_aff_log');
		//			        $_log->create($l->code_id,'subscribe',$l->list_id,$member->email);
		//			     	$ids[] = $l->id;
		//		        }
		//		        $log->deletePreregLogs($ids);
		//			///////////////////

		if($list->second_conf_page != null AND $list->second_conf_page != ""){
			
			url::redirect(replace::replaceFields($member,$list->second_conf_page,TRUE));
		}else{
			url::redirect('/pages/redirect/subscribe/confirmdouble');
		}
	}


	/**
	 * leliratkozás elött fut le...
	 *
	 */
	public static function preUnsubscription(){

	}

	public static function globalUnsubscription(){
	
		
	}
	
	/**
	 * leliratkozás után fut le..Event:data = $member_ORM
	 *
	 */
	public static function postUnsubscription(){
		$data = Event::$data;
		$member = $data['member'];
		$km_user = $data['km_user'];
		//$list = $member->omm_list;

		
			
		//			////////////affiliate log, amelyik kódhoz volt már logja ahhoz nyomunk
		//			try {
		//				$log = ORM::factory('omm_aff_log');
		//		        $oldlogs = $log->getLogForEmailLists($member->email,'subscribe');
		//
		//		        $ids = array();
		//		        foreach ($oldlogs as $l){
		//		        	$_log = ORM::factory('omm_aff_log');
		//			        $_log->create($l->code_id,'unsubscribe',$l->list_id,$member->email);
		//			        $ids[] = $l->id;
		//		        }
		//
		//		        $log->deletePreregLogs($ids);
		//			}catch(Exception $e){
		//				KOHANA::log("error","affiliate error leiratkozásnál".$member->id." ::: ".$e);
		//			}
		//			///////////////////
		
		//api::sendNotification($list,$member,'unsubscribe');


			
		
		
		if(1==2 && $list->unsubscribe_landing_page != null AND $list->unsubscribe_landing_page != ""){
			
			
			url::redirect(replace::replaceFields($member,$list->unsubscribe_landing_page,TRUE));
		}else{
			
			if($km_user == 'bezchoroby'){
				url::redirect('http://bezchoroby.sk/7-1-odhlasenie');
			}else{
				url::redirect('/pages/redirect/subscribe/unsubscribe');	
			}
			
			
			
			
		}
	}



}
?>