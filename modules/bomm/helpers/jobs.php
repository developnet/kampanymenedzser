<?php defined('SYSPATH') or die('No direct script access.');

require_once LIBROOT.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR."bomm".DIRECTORY_SEPARATOR."libraries".DIRECTORY_SEPARATOR."simple_html_dom.php";

		if ( ! class_exists('Swift', FALSE)){
			// Load SwiftMailer
			require Kohana::find_file('vendor', 'swift/Swift');

			// Register the Swift ClassLoader as an autoload
			spl_autoload_register(array('Swift_ClassLoader', 'load'));
		}	

class jobs_Core {
	
	public static function prepareJobs($jobs){
		$dom = new simple_html_dom();
		foreach ($jobs as $job){
			
			$job->prepare($dom);
			unset($job);
		}
		unset($dom);
		
	}
	
	public static function runLostSql($db){
		$dir = Kohana::log_directory()."send/";
		$files = filesystem::get_files($dir);
		
		foreach($files as $file){
			if ( is_file($dir.$file)){
				$sql = file_get_contents($dir.$file);
				//echo $sql."<br/>";
				$db->query($sql);
				unlink($dir.$file);	
			}
		}
		return $files;
	}
	
	public static function clean($file){
		$dir = Kohana::log_directory()."send/";
		$filename = $dir.$file;
		unlink($filename);		
	}
	
	public static function startLogFile($file){
		$dir = Kohana::log_directory()."send/";
		$filename = $dir.$file;
				
		if ( ! is_file($filename)){
			
			// Write the SYSPATH checking header
			file_put_contents($filename,' ');

			// Prevent external writes
			chmod($filename, 0644);
		}		
	}
	
	public static function writeLogFile($sql,$file){
		
		if ( ! is_file($file)){
			$dir = Kohana::log_directory()."send/";
			$filename = $dir.$file;
			file_put_contents($filename,  $sql.PHP_EOL, FILE_APPEND);			
		}		
	}

	
	/**
	 * mega küldő
	 * @param omm_job array $jobs
	 * @param string $type
	 */
	public static function sendJobs($jobs,$type,$swift,$db, $useDomainLimit = true, $km_user = ""){
		return jobs::advancedSendJobs($jobs,$type,$swift,$db, $useDomainLimit, $km_user);
		
		/*
		$tablename = Kohana::config("database.default");
		$tp = $tablename['table_prefix'];				
		
		$jobids = array();
		
		if ( ! class_exists('Swift', FALSE)){
			// Load SwiftMailer
			require Kohana::find_file('vendor', 'swift/Swift');

			// Register the Swift ClassLoader as an autoload
			spl_autoload_register(array('Swift_ClassLoader', 'load'));
		}		
		
		$recipients = new Swift_RecipientList();
		$replacements = array();

		if(Kohana::config("core.swift_log_enabled")){
			$log =& Swift_LogContainer::getLog();
			$log->setLogLevel(Kohana::config("core.swift_log_level"));			
		}	


		
		 
		//////////////dummy message
		$message = new Swift_Message("{subject}");
		$message->headers->setEncoding("Q");
		
		$message->setReturnPath(Kohana::config("core.mail_return_path"));

		if(sizeof($jobs) > 0){
			if($jobs[0]->email_sender_replyto != NULL && $jobs[0]->email_sender_replyto != ""){
				$message->setReplyTo($jobs[0]->email_sender_replyto);
			}else{
				$message->setReplyTo(NULL);
			}			
		}

			
		$part_text = new Swift_Message_Part("{text}");
		$part_text->headers->setEncoding("Q");
		
		$part_text->setCharset("utf-8");
		$message->attach($part_text);
			
		if($type == "html"){
				
			$part_html = new Swift_Message_Part("{body}", "text/html");
			$part_html->setCharset("utf-8");
			$message->attach($part_html);	
				
		}		
		///////////////////dummy
		
		
		////////////////////////címzettek, tartalom		
		foreach($jobs as $job){
			
			$message->job_id = $job->id;
			
			$replacements[$job->email_recipient_email] = array("{subject}" => $job->email_subject, "{body}" => $job->email_content_html, "{text}" => $job->email_content_text); 
			
			if($job->email_recipient_name != ""){
				$recipients->addTo(new Swift_Address($job->email_recipient_email, $job->email_recipient_name));
				$jobids[] = $job->id;
			}else{
				$recipients->addTo($job->email_recipient_email);
				$jobids[] = $job->id;
			}
		}
		////////////////////címzettek, tartalom
		
		
		$swift->attachPlugin(new Swift_Plugin_Decorator($replacements), "decorator");
		$batch = new Swift_BatchMailer($swift);
			
		///////////küldés
		
		if($job->email_sender_name != ""){
			$sender = new Swift_Address($job->email_sender_email, $job->email_sender_name);	
		}else{
			$sender = $job->email_sender_email;
		}		
		
		$num_sent = 0;
		$faileds = array();
		
		
		$num_sent = $batch->send($message, $recipients, $sender);

		
		$faileds = $batch->getFailedRecipients();
		
		/////////////////eredmény
		if(sizeof($faileds) <= 0){
			
			$file = date('Y_m_d_U').'.log'.EXT;
			self::startLogFile($file);			
			
			$sql = "UPDATE ".$tp."omm_jobs SET status='sent', sent_day = NOW(), sent_time = NOW() WHERE id IN (".implode(",", $jobids).",0);";
			self::writeLogFile($sql,$file);

			try {
				$db->query($sql);
				self::clean($file);	
			} catch (Exception $e) {}			
			
		}else{
	
			$jobids = array();			
			$jobidserror = array();
			
			
			foreach ($jobs as $job){
			
				if(in_array($job->email_recipient_email,$faileds)){
					$jobidserror[] = $job->id;
				}else{
					$jobids[] = $job->id;
				}
			}
				/////sikeres
				$file = date('Y_m_d_U').'.log'.EXT;
				self::startLogFile($file);				
				$sql = "UPDATE ".$tp."omm_jobs SET status='sent', sent_day = NOW(), sent_time = NOW() WHERE id IN (".implode(",", $jobids).",0);";
				self::writeLogFile($sql,$file);	

				try {
					$db->query($sql);
					self::clean($file);	
				} catch (Exception $e) {}				

				
				////sikertelen
				$file = date('Y_m_d_U').'.log'.EXT;
				self::startLogFile($file);				
				$sql = "UPDATE ".$tp."omm_jobs SET status='error', sent_day = NOW(), sent_time = NOW() WHERE id IN (".implode(",", $jobidserror).",0);";
				self::writeLogFile($sql,$file);				
				
				try {
					$db->query($sql);
					self::clean($file);	
				} catch (Exception $e) {}				
		}
		
		
		
		

		/////////////////eredmény
		
		
		 
		if(Kohana::config("core.swift_log_enabled")){
			file_put_contents(Kohana::log_directory()."swift/"."swift.".date('Y_m_d_U').".log", $log->dump(TRUE));	
		}	
		
		
		return $num_sent;*/
	}
	
	
	/**
	 * mega küldő
	 * @param omm_job array $jobs
	 * @param string $type
	 */
	public static function advancedSendJobs($jobs,$type,$swift,$db, $useDomainLimit = true, $km_user = ""){
		
		
		$jobids = array();
		
		if ( ! class_exists('Swift', FALSE)){
			// Load SwiftMailer
			require Kohana::find_file('vendor', 'swift/Swift');

			// Register the Swift ClassLoader as an autoload
			spl_autoload_register(array('Swift_ClassLoader', 'load'));
		}		
		
		$recipients = new Swift_RecipientList();

		if(Kohana::config("core.swift_log_enabled")){
			$log =& Swift_LogContainer::getLog();
			$log->setLogLevel(Kohana::config("core.swift_log_level"));			
		}	

		if(sizeof($jobs) > 0){
			if($jobs[0]->email_sender_replyto != NULL && $jobs[0]->email_sender_replyto != ""){
				$replyto = $jobs[0]->email_sender_replyto;
			}else{
				$replyto = NULL;
			}		

			if($jobs[0]->email_sender_name != ""){
				$sender = new Swift_Address($jobs[0]->email_sender_email, $jobs[0]->email_sender_name);	
			}else{
				$sender = $jobs[0]->email_sender_email;
			}				
		}

			

			

		
		
		////////////////////////címzettek, tartalom		
		$i = 0;
		foreach($jobs as $job){
			
			$send = true;
			if($useDomainLimit){
				$send = false;
				$_db = new Database();
				$check = jobs::checkDomainLimit($job->email_recipient_email,$_db);
				if(is_null($check)){ ////van-e limit bejegyzés a domainre
					$send = true;				
				}else{////van limit, megnézzük, hogy belül vagyunk-e
					
					#  $datum1 = date (" y-m-d "); // 06-10-30  
					#  $datum2 = date (" y-m-d", filemtime("feltolt.php")); // 06-09-27  
					#  $deltat = strtotime($datum1)-strtotime($datum2); //Másodpercben
	
					$datum2 = time();
					
					$deltat =  $datum2 - strtotime($check->lastsend); //Másodpercben
					
					if($deltat < $check->seconds) {
						$send = false;
						//KOHANA::log("alert", $job->email_recipient_email . " limiten belul, nem kuldjuk ki. Last: ". strtotime($check->lastsend)." Now: ".$datum2." (".$deltat." < ".$check->seconds.")");
						sleep(Kohana::config("core.waitafterlimitedfound"));
					}else{
						$send = true;
						//KOHANA::log("alert", $job->email_recipient_email . " mehet.");
						jobs::updateDomainLastSend($check->domain,$datum2,$_db);
					}
				}
				
				unset($_db);				
			}

			
			if($send){
				$message = new Swift_Message($job->email_subject);
				$message->headers->setEncoding("Q");
				$message->setReplyTo($replyto);
				
				if($km_user != ""){
					$message->setReturnPath($km_user."_".$job->id."@kmsrv2.hu");//
				}else{
					$message->setReturnPath("teszt_".$job->id."@kmsrv2.hu");	
				}
				
							
				$message->job_id = $job->id;
				
				$part_text = new Swift_Message_Part($job->email_content_text);
				$part_text->setEncoding("q");
				$part_text->setCharset("utf-8");
				$message->attach($part_text);			
				
				if($type == "html"){
					$part_html = new Swift_Message_Part($job->email_content_html, "text/html");
					$part_html->setEncoding("q");
					$part_html->setCharset("utf-8");
					$message->attach($part_html);	
						
				}				
				
				
				if($job->email_recipient_name != ""){
					$recipient = new Swift_Address($job->email_recipient_email, $job->email_recipient_name);
				}else{
					$recipient = $job->email_recipient_email;
				}
				
				
				$sent = $swift->send($message,$recipient, $sender);
				
				unset($message);
				unset($recipient);		
				$i++;		
			}
		}
		////////////////////címzettek, tartalom
		
		return $i;
	}	
	
	public static function updateDomainLastSend($domain,$lastsend,$db,$ip = ""){
		
		if($ip != "") $ip = " ".$ip;		
		
		$db->query("UPDATE omm_domain_limits SET lastsend = NOW() WHERE domain = '".$domain.$ip."' ");
	}
	
	public static function checkDomainLimit($email,$db,$ip = ""){
		$domain = preg_split("[@]",$email);
		
		if($ip != "") $ip = " ".$ip;
		
		$res = $db->query("SELECT domain,lastsend,seconds FROM omm_domain_limits WHERE domain = '".$domain[1].$ip."' LIMIT 1");
		
		if(sizeof($res) > 0){
			return $res[0];
		}else{
			return NULL;
		}
		
	}

	
	public static function logCron($dbcore, $type, $pid, $stat = "start", $id = NULL ){
		//getmypid()
		$now = date('Y-m-d H:i:s');
		
		if($stat == "start"){
			$res = $dbcore->insert("omm_cronjobs", array( "type" => $type, "pid" => $pid, "start" => $now ) );
			$id = $res->insert_id();
			
			return $id;
			
		}else{
			$dbcore->update("omm_cronjobs", array("end" => $now, "pid_end" => $pid), array('id' => $id));
		}
	}	
	
	/**
	 * sima e-mailt küld, mindengtől függetlenül
	 * Enter description here...
	 * @return unknown_type
	 */
	public static function sendEmail($from,$subject,$message,$to = array(),$smtp = '188.227.227.172'){
			
		
		
			$m = new Swift_Message($subject);
			$m->headers->setEncoding("Q");			
			
			$part_html = new Swift_Message_Part($message, "text/html");
			$part_html->setCharset("utf-8");
			$m->attach($part_html);				
			
			$swift = jobs::getSwift(false,false,$smtp);
			
			foreach($to as $t){
				$swift->send($m, $t, $from);	
			}
			
			$swift->disconnect();			
	}
	
	public static function getSwift($antiflood = false, $common = false, $smtphost = "", $km_user_code = ""){
		
		if ( ! class_exists('Swift', FALSE)){
			// Load SwiftMailer
			require Kohana::find_file('vendor', 'swift/Swift');

			// Register the Swift ClassLoader as an autoload
			spl_autoload_register(array('Swift_ClassLoader', 'load'));
		}			
		
		//////////////connection
		if(Kohana::config("core.mail_method") == 'smtp'){
			
			if($smtphost == ""){
				//$smtphost = Kohana::config("core.mail_smtp_host");
				$smtphost = "188.227.227.171";
				
			}
			
			if($km_user_code == "U1JtoHr1" || $km_user_code == "teszt1"){//
				$smtphost = "188.227.227.174";		
			}

			if($km_user_code == "J1KbTCkS" || $km_user_code == "epitkezem"){//
				$smtphost = "188.227.227.173";		
			}			
			
			if($km_user_code == "40bigNbQ" || $km_user_code == "legends"){//
				$smtphost = "188.227.227.178";		
			}				
			
			if($km_user_code == "fhdCBfEu" || $km_user_code == "antikvar"){//
				$smtphost = "188.227.227.179";		
			}			

			if($km_user_code == "HprqcpVA" || $km_user_code == "regikonyv"){//
				$smtphost = "188.227.227.180";		
			}				
			
			if($km_user_code == "4UyNehPQ" || $km_user_code == "krizobaltina"){//
				$smtphost = "188.227.227.174";		
			}				
			
			$conn = new Swift_Connection_SMTP($smtphost, 
											  Kohana::config("core.mail_smtp_port"), 
											  Swift_Connection_SMTP::AUTO_DETECT);	
												  										 
			$conn->setUsername(Kohana::config("core.mail_smtp_username"));
			$conn->setpassword(Kohana::config("core.mail_smtp_password"));
			
		}elseif(Kohana::config("core.mail_method") == 'mail'){
			$conn = new Swift_Connection_NativeMail();
		}
		//////////connection			

		$swift = new Swift($conn);
		
		if($antiflood){
			//Reconnect after 100 emails
			$swift->attachPlugin(new Swift_Plugin_AntiFlood(KOHANA::config("core.sendthereshold"), KOHANA::config("core.waitaftersendthereshold")), "anti-flood");

			//Load the plugin, and give it a name
			$swift->attachPlugin(new OmmSendListener(), "OmmSendListener");				
			
		}

		if($common){
			$swift->attachPlugin(new CommonSendListener(), "CommonSendListener");
		}
		
		return $swift;
	}
	
	
	
	
	
	
}

/////////////////////////
class OmmSendListener implements Swift_Events_SendListener
{
	var $db = NULL;
	
    public function sendPerformed(Swift_Events_SendEvent $e){
		
    	if($this->db == NULL){
    		$this->db = new Database("own");
    	}

    	try {
    		
    		if(sizeof($e->getFailedRecipients()) > 0 ) $stat = 'error';
    		else $stat = 'sent';
    		
    		$status = $this->db->update('omm_jobs', array('message_id' => $e->getMessage()->getId(), 
    												 'status' => $stat,
    												 'sent_day' =>  date('Y-m-d'),
    												 'sent_time' =>  date('Y-m-d H:i:s')		
    													), array('id' => $e->getMessage()->job_id));
    		
    		KOHANA::log("alert", "sent:".$e->getMessage()->getId()." - job:".$e->getMessage()->job_id);
    		//KOHANA::log_save();
    										
    	} catch (Exception $ee) {
    		KOHANA::log("error", "sent:".$e->getMessage()->getId()." - job:".$e->getMessage()->job_id);
    		KOHANA::log("error", $ee);
    		//KOHANA::log_save();
    	}
		
		usleep(KOHANA::config("core.waitaftersend"));
    }
}


class CommonSendListener implements Swift_Events_SendListener
{
	var $db = NULL;
	
    public function sendPerformed(Swift_Events_SendEvent $e){
		
    	if($this->db == NULL){
    		$this->db = new Database("own");
    	}

    	try {
    		
    		if(sizeof($e->getFailedRecipients()) > 0 ) $stat = 'error';
    		else $stat = 'sent';
    		
    		$status = $this->db->update('omm_jobs', array('message_id' => $e->getMessage()->getId(), 
    												 'status' => $stat,
    												 'sent_day' =>  date('Y-m-d'),
    												 'sent_time' =>  date('Y-m-d H:i:s')		
    													), array('id' => $e->getMessage()->job_id));
    		
    		KOHANA::log("alert", "sent:".$e->getMessage()->getId()." - job:".$e->getMessage()->job_id);
    		//KOHANA::log_save();
    										
    	} catch (Exception $ee) {
    		KOHANA::log("error", "sent:".$e->getMessage()->getId()." - job:".$e->getMessage()->job_id);
    		KOHANA::log("error", $ee);
    		//KOHANA::log_save();
    	}
		
    }
    
}

?>