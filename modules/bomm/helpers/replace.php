<?php

class replace_Core{


	public static function replaceFields($member,$text, $base64 = FALSE, $listid = null){

		$matches = array();
		
		$pattern = '/'.Kohana::config('core.fieldSeparatorStart').'[^'.Kohana::config('core.fieldSeparatorStart').']+?'.Kohana::config('core.fieldSeparatorEnd').'/i';
		
		preg_match_all($pattern, $text, $matches);
		
		$client = $member->omm_client;
		$fields = $client->getAllFields($listid);
		
		$mo = $member->getData($fields);

		foreach($matches as $match){
			foreach($match as $m){
				replace::replaceMatch($m,$mo,$text,$base64);
			}
		}

		 
		return $text;
	}


	public static function replaceMatch($m,$mo, &$text, $base64 = FALSE){

		$ref = string::processFieldTag($m,  strlen(Kohana::config('core.fieldSeparatorStart')), strlen(Kohana::config('core.fieldSeparatorEnd')), Kohana::config('core.fieldUresSeparator'), Kohana::config('core.fieldEmptySeparator'));

		$reference = $ref['reference'];
		$empty = $ref['empty'];
		
		
		if(!isset($mo->$reference) || $mo->$reference == NULL || $mo->$reference == "" ){
			$newtext = $empty;
		}else{
			$newtext = $mo->$reference;
		}
		
		if($base64){
			$newtext = base64_encode($newtext);
		}
		
		$text = str_ireplace($m, $newtext, $text);
		
	}

}

?>