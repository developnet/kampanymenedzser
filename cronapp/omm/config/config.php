<?php defined('SYSPATH') or die('No direct script access.');
$user = 'new';

//max honlap userenknt
$config['max_client_per_user'] = 100;

if (substr($_SERVER["PHP_SELF"], 0, 5) == "https") {
	$config['protocol'] = "https";
} else {
	$config['protocol'] = "http";
}


$prot = 'http';
if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
	$prot .= "s";
}

$config['protocol'] = $prot;
	 

//AFA
$config['afe_szazalek'] = 27;

$base = $user.'.kmsrv2.hu/';
//$base = 'mfpvip.local/';

$core_base = $user.'.kmsrv2.hu/core/';
//$core_base = 'mfpvip.local/';


$config['site_domain'] = $base;
$config['formvalidatorpath'] = $config['protocol'].'://'.$core_base.'validator/'; 
$config['assetspath'] = $config['protocol'].'://'.$core_base.'assets/'; 
$config['report_xml'] = $config['protocol'].'://'.$core_base.'views/omm/reports/';
//$config['viewpath'] = 'http://'.$core_base.'modules/bomm/views/';

$config['viewpath'] = $config['protocol'].'://'.$core_base.'views/';


/**
 * lehet: mail, smtp
 */
$config['mail_method'] = 'smtp';

$config['mail_smtp_host'] = '127.0.0.1'; 
$config['mail_smtp_port'] = '25';

$config['mail_smtp_username'] = '';
$config['mail_smtp_password'] = '';

//kampanymenedzser.hu
//$config['mail_smtp_username'] = 'mailgyujto';
//$config['mail_smtp_password'] = 'Mailgy12';

//$config['mail_return_path'] = 'hirlevel@aktivhonlapok.hu';

//kampanymenedzser.hu
//$config['mail_return_path'] = 'visszapattano@kampanymenedzser.hu';
$config['mail_return_path'] =  'mail@kmsrv2.hu';

$config['fieldSeparatorStart'] = "{{";
$config['fieldSeparatorEnd'] = "}}";
$config['fieldUresSeparator'] = ",";
$config['fieldEmpty'] = "ures";
$config['fieldEmptySeparator'] = "=";
$config['unsubscribe_link_name'] = "leiratkozo_link";	
$config['activation_link_name'] = "aktivacios_link";		
$config['datamod_link_name'] = "adatmodosito_link";			
	
$config['job_create_break_limit'] = 10000;
$config['job_prepare_break_limit'] = 10000;
$config['job_send_break_limit'] = 10000;
	

$config['job_create_break_limit_sendnow'] = 500;
$config['job_prepare_break_limit_sendnow'] = 100;
$config['job_send_break_limit_sendnow'] = 100;
	
	
$config['open_timeout'] = 1800; ///30 perc másodpercben

////vgleges bounce error limit ...ha elri ezt akkor a member hibs lesz
$config['perm_error_limit'] = 2; 
//// # hnap alatt kell a perm_error_limit-et meghaladni
$config['perm_error_limit_timeout'] = 12; 

////tmenet bounce error limit ...ha elri ezt akkor a member hibs lesz
$config['temp_error_limit'] = 5;
//// # hnap alatt kell a temp_error_limit-et meghaladni
$config['temp_error_limit_timeout'] = 2; 


//		0 = Off (Swift_Log::LOG_NOTHING or SWIFT_LOG_NOTHING in PHP4)
//		1 = Errors only (Swift_Log::LOG_ERRORS or SWIFT_LOG_ERRORS in PHP4)
//		2 = Failed deliveries (Swift_Log::LOG_FAILURES or SWIFT_LOG_FAILURES in PHP4)
//		3 = Network commands (Swift_Log::LOG_NETWORK or SWIFT_LOG_NETWORK in PHP4)
//		4 = Everything (Swift_Log::LOG_EVERYTHING or SWIFT_LOG_EVERYTHING in PHP4)
$config['swift_log_level'] = 2; 
$config['swift_log_enabled'] = TRUE;

$config['cron_create_limit'] = 200000; //db e-mail
$config['cron_prepare_limit'] = 10000; //db e-mail
$config['cron_send_limit'] = 100; //db e-mai userenkentl

$config['waitafteruseroperation'] = 2; //seconds

$config['sendthereshold'] = 500; //db e-mail
$config['waitaftersend'] = 100; // microsecond
$config['waitaftersendthereshold'] = 0; //second
$config['waitafterlimitedfound'] = 1; //second, ennyit vr ha tallegy mailt amit nem lehet kikldeni limit miatt

$config['resend_email_after_bounce'] = 1; //hnyszor kldje jra a jobot ha jn 'resend' visszapattan ugyan arra a jobra
$config['waitaftersend_atresend'] = 10000000; // microsecond 10000000 = 10 s

/**
 * Force a default protocol to be used by the site. If no site_protocol is
 * specified, then the current protocol is used, or when possible, only an
 * absolute path (with no protocol/domain) is used.
 */
$config['site_protocol'] = '';

/**
 * Name of the front controller for this application. Default: index.php
 *
 * This can be removed by using URL rewriting.
 */
$config['index_page'] = '';

/**
 * Fake file extension that will be added to all generated URLs. Example: .html
 */
$config['url_suffix'] = '';

/**
 * Length of time of the internal cache in seconds. 0 or FALSE means no caching.
 * The internal cache stores file paths and config entries across requests and
 * can give significant speed improvements at the expense of delayed updating.
 */
$config['internal_cache'] = FALSE;

/**
 * Enable or disable gzip output compression. This can dramatically decrease
 * server bandwidth usage, at the cost of slightly higher CPU usage. Set to
 * the compression level (1-9) that you want to use, or FALSE to disable.
 *
 * Do not enable this option if you are using output compression in php.ini!
 */
$config['output_compression'] = FALSE;

/**
 * Enable or disable global XSS filtering of GET, POST, and SERVER data. This
 * option also accepts a string to specify a specific XSS filtering tool.
 */
$config['global_xss_filtering'] = FALSE;

/**
 * Enable or disable hooks. Setting this option to TRUE will enable
 * all hooks. By using an array of hook filenames, you can control
 * which hooks are enabled. Setting this option to FALSE disables hooks.
 */
$config['enable_hooks'] = FALSE;

/**
 * Log thresholds:
 *  0 - Disable logging
 *  1 - Errors and exceptions
 *  2 - Warnings
 *  3 - Notices
 *  4 - Debugging
 */
$config['log_threshold'] = 2;

/**
 * Message logging directory.
 */
$config['log_directory'] = APPPATH.'logs';

/**
 * Enable or disable displaying of Kohana error pages. This will not affect
 * logging. Turning this off will disable ALL error pages.
 */
$config['display_errors'] = TRUE;

/**
 * Enable or disable statistics in the final output. Stats are replaced via
 * specific strings, such as {execution_time}.
 *
 * @see http://docs.kohanaphp.com/general/configuration
 */
$config['render_stats'] = TRUE;

/**
 * Filename prefixed used to determine extensions. For example, an
 * extension to the Controller class would be named MY_Controller.php.
 */
$config['extension_prefix'] = 'MY_';

/**
 * Additional resource paths, or "modules". Each path can either be absolute
 * or relative to the docroot. Modules can include any resource that can exist
 * in your application directory, configuration files, controllers, views, etc.
 */
$config['modules'] = array
(
	 MODPATH.'auth',     
	 MODPATH.'bomm',     
	 MODPATH.'barendszer',
 	 MODPATH.'unit_test'
);



