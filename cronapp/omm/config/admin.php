<?php defined('SYSPATH') or die('No direct script access.');


$config['debug'] = TRUE;

$config['theme'] = "omm";

$config['title'] = "KampányMenedzser";
$config['appname'] = "KampányMenedzser";

$config['css'] = "".$config['theme']."/css/";
$config['js'] = "".$config['theme']."/js/";
$config['img'] = "".$config['theme']."/img/";
$config['swf'] = "".$config['theme']."/swf/";

$config['assets'] = "assets/";

$config['appmodules'] = array(
	'barendszer','bcms'
);

?>
